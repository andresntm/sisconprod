/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sissmall;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import org.zkoss.lang.Threads;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;

import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;

import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;
import sissmall.entidades.SmallUsuarios;
import sissmall.implementaciones.UsuariosImpl;

/**
 *
 * @author exesilr
 */
public class UsuariosController extends SelectorComposer<Component> {

    Session session;

    @Wire
    Grid BanEntrClientee;
    @Wire
    Grid BanEntrJuducial;

    @Wire
    Bandbox buscarinputRutSmallCore;

    @Wire
    Button btn_condona;
    @Wire
    Button btn_sacabop;
    //MvcConfig mmmm = new MvcConfig();

    //busqueda usuarios SmallCore
    final UsuariosImpl usr;
    SmallUsuarios sUsr;
    MvcConfig mmmm = new MvcConfig();
    @Wire
    Textbox smallusruaios_alias;

    @Wire
    Textbox smallusruaios_perfil;
    @Wire
    Textbox smallusruaios_tipousuario;
    @Wire
    Textbox smallusruaios_nombrecompleto;
    @Wire
    Textbox smallusruaios_rut;
    @Wire
    Textbox smallusruaios_regional;

    @Wire
    Textbox smallusruaios_cedente;
    @Wire
    Textbox smallusruaios_cargo;
    @Wire
    Textbox smallusruaios_estado;
    @Wire
    Textbox smallusruaios_sucnova;

    @Wire
    Button smallusruaios_btn_respassword;
    @Wire
    Tabbox smallusruaios_tabbox;
    @Wire
    Grid smallusruaios_grid;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        session = Sessions.getCurrent();

    }

    public UsuariosController() throws SQLException {
        this.usr = new UsuariosImpl(mmmm.getDataSourceLucy());

    }

    @Listen("onClick=#btn-find-rutSmallUsr; onOK=#loginWin")
    public void doLogin() throws SQLException {
        String nm = buscarinputRutSmallCore.getValue();
        if (nm == "") {
            Messagebox.show("STRINGGGG[VACIO]");
            return;
        }
        if (nm != null || nm.isEmpty() || nm.length() > 0) {

            String mm = nm.replace(".", "");
            String[] ParteEntera = mm.split("-");

            sUsr = usr.getUsuarioPorRut(Integer.parseInt(ParteEntera[0]));
            smallusruaios_alias.setValue(sUsr.getUsername());
            smallusruaios_perfil.setValue(sUsr.getPerfil());
            smallusruaios_tipousuario.setValue(sUsr.getTipo_Usuario());
            smallusruaios_nombrecompleto.setValue(sUsr.getNombre() + " " + sUsr.getApellidoPaterno() + " " + sUsr.getApellidoMaterno());
            smallusruaios_rut.setValue(sUsr.getRut());
            smallusruaios_regional.setValue(sUsr.getRegional());
            smallusruaios_cedente.setValue(sUsr.getCedente());
            smallusruaios_cargo.setValue(sUsr.getCargo());
            smallusruaios_estado.setValue(sUsr.getEstado());
            smallusruaios_sucnova.setValue(sUsr.getSucursal_Nova());
            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
            smallusruaios_grid.setVisible(true);
            smallusruaios_btn_respassword.setVisible(true);
            smallusruaios_tabbox.setVisible(true);

        } else {
            Messagebox.show("KSDJAKLDJKLSJDKL----USER[VACIO]");
        }

    }

    @Listen("onClick=#smallusruaios_btn_respassword; onOk=#loginWin")
    public void ProcesaResetearUsuarioPsw() {

        String nm = buscarinputRutSmallCore.getValue();
        Messagebox.show("ProcesandoPassword");
        Clients.showBusy("Waiting for server...");
        Clients.showBusy(buscarinputRutSmallCore, nm);

    }

    public void doLongOperation() {
        Threads.sleep(5000); //simulate a 5 sec operation
        //output.setValue("the result is 42"); //update the UI

    }

    @Listen("onClick=#btn_condona; onOK=#loginWin")
    public void simulacondonacion() throws SQLException {
        System.out.println("#------------@@@@SESIONNNNNNNNNNn" + Executions.getCurrent().getDesktop().getPages().toString() + " @@@@@@@@@---------#");
        // Include uiContent = (Include) Sessions.getCurrent().getAttribute("pageref"); 
        // uiContent.setSrc("sacabop.zul");
        //pageref.setSrc("sacabop.zul");

        // /WEB-INF/condonaciones/condonaciones/sacabop.zul
        // Window window = (Window)Executions.createComponents("sacabop.zul","contenido", null);
        //window.add
        // window.doModal();
        //window.
        //Executions.get
        String jjjjj = Executions.getCurrent().getDesktop().getPages().toString();
        //String hhhh="";
        String[] hhhh = jjjjj.split(" ");
        String xxx = hhhh[1].replace("_]]", "");
        //xxx=xxx.replace("]]","");
        //Executions.getCurrent().getDesktop().getPages().toString();
        String iddiv = xxx.concat("le");
        System.out.println("#------------@@@@SESIONNNNNNNNNNn[" + xxx.toString() + "] @@@@@@@@@---------#");

        System.out.println("#------------@@@@Intentificador del Div de Include[" + iddiv + "] @@@@@@@@@---------#");
        Div nnnn = null;
        nnnn.setId(iddiv.toString());

        //String iddiv=xxx+"le";
        Include headerPage = new Include();
        headerPage.setSrc("usuarios.zul");
        headerPage.setParent(nnnn);
        //Include headerPage = new Include();
        //  headerPage.getch

        //		headerPage.setSrc("header.zul");
        //	headerPage.setParent(contenido);
        //      headerPage.setParent(yBHQle);
//org.zkoss.zul.Panel cannot be cast to org.zkoss.zul.Window
        ////pageref.setSrc("ejecutivo.zul");
        //Messagebox.show("Imprimo:["+page.+"]");
//Executions.getCurrent().sendRedirect(locationUri);
        // include.setSrc("sacabop.zul");
        //Include incInclude = (Include)this.desktop.getPage("PrincipalID").getFellowIfAny("incContenidoPrincipal");
        //if(incInclude != null) incInclude.setSrc("protected/frm_EmisionCotizacion.jsp");
//  Include include = (Include)Selectors.iterable(fnList.getPage(), "#mainInclude").iterator().next(); 
        //   include.setSrc(locationUri);
        //Executions.sendRedirect("/chapter8/");
    }

}
