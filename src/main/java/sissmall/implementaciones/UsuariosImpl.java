/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sissmall.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import static siscore.comunes.LogController.SisCorelog;
import sissmall.entidades.SmallUsuarios;
import sissmall.interfaces.UsuariosInterfaz;

/**
 *
 * @author exesilr
 */
public class UsuariosImpl implements UsuariosInterfaz{
        private DataSource dataSource;
        private SimpleJdbcCall jdbcCall;

    public UsuariosImpl(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public void setDataSource(DataSource ds) {
        this.dataSource=ds;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public SmallUsuarios getUsuarioPorRut(int Rut){
    SmallUsuarios usr;
            usr = new SmallUsuarios();
    
    
             try {
        List<SmallUsuarios> listusr;
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_web_lista_usuario").
                returningResultSet("rs", new ParameterizedRowMapper<SmallUsuarios>() {

                    public SmallUsuarios mapRow(ResultSet rs, int i) throws SQLException {

                        SmallUsuarios aUsr = new SmallUsuarios();
                        aUsr.setId(rs.getInt("ID"));
                        aUsr.setUsername(rs.getString("Username"));
                        aUsr.setTipo_Usuario(rs.getString("Tipo_usuario"));
                        aUsr.setNombre(rs.getString("Nombre"));
                        aUsr.setApellidoPaterno(rs.getString("Apellido_P"));
                        aUsr.setApellidoMaterno(rs.getString("Apellido_M"));
                        aUsr.setRut(rs.getString("RUT"));
                        aUsr.setRegional(rs.getString("Regional"));
                        aUsr.setCedente(rs.getString("Cedente"));
                        aUsr.setCargo(rs.getString("Cargo"));
                        aUsr.setPerfil(rs.getString("Perfil"));
                        aUsr.setEstado(rs.getString("idEstado"));
                        aUsr.setSucursal_Nova(rs.getString("Sucursal_Nova"));
                        //aUsr.setUsername(rs.getString("Username"));
                        //aUsr.sesetId(rs.getInt("fld_ced"));
                        //aUsr.setOperacion(rs.getString("fld_ope"));
                        //aUsr.setOperacionOriginal(rs.getString("fld_ope_ini"));
                        //aUsr.setTipoCedente(rs.getString("fld_tip_cre"));
                        //aUsr.setDetalleCredito(rs.getString("fld_det_tip_cre_max"));


                        return aUsr;//rs.getString(1);   

                    }
                ;//           private static final Logger LOG = Logger.getLogger(.class.getName());
        });
     // SqlParameterSource in = new MapSqlParameterSource().addValue("rut", "10000165").addValue("col", 1).addValue("usuario", "jliguen");
     
             SqlParameterSource in = new MapSqlParameterSource().addValue("var", Rut);
       

            Map<String, Object> out = jdbcCall.execute(in);

            listusr = (List<SmallUsuarios>) (out.get("rs"));
            usr=listusr.get(0);

        } catch (DataAccessException ex) {
               SisCorelog("Error Ejecutando Procedure ["+ex.getMessage()+"]");
        }
        //List l = jdbcCall.executeFunction(List.class, Collections.EMPTY_MAP);
          SisCorelog("Rut[" + Rut + "]---User.alias[" + usr.getPerfil() + "]");
       // System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");
            
            
            
            
    
    return usr;
    
    }
    
    
    public boolean CambiarPassword(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }
   public boolean CambiarEstadoUsuario(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }
    public boolean CambiarNombre(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }   
    public boolean CambiarPerfil(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    } 
     public boolean CambiarTipoUsuario(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }  
     
       public boolean CambiarRegional(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }
        public boolean CambiarCedente(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }
         public boolean CambiarCargo(int Rut){
    	String QueryUpdatePassword="UPDATE DB_WEBNZA_USUARIO SET STRCONTRASENA =@pass, FECHA_UPDATE = getdate(), IDESTADO = 1, INTENTOS_INGRESO	= 0 WHERE STRUSUARIO = @usuario";
    
    
    return false;
    }
}
