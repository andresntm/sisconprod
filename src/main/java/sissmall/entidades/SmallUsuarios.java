/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sissmall.entidades;

/**
 *
 * @author exesilr
 */
public class SmallUsuarios {

    
        int id;
    String Username;
    String Tipo_Usuario;
    String Nombre;
    String ApellidoPaterno;
    String ApellidoMaterno;
    String Rut;
    String Regional;
    String Cedente;
    String Cargo;
    String Perfil;
    String Estado;
    String Sucursal_Nova;
    
    
        public SmallUsuarios(int id, String Username, String Tipo_Usuario, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String Rut, String Regional, String Cedente, String Cargo, String Perfil, String Estado, String Sucursal_Nova) {
        this.id = id;
        this.Username = Username;
        this.Tipo_Usuario = Tipo_Usuario;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.Rut = Rut;
        this.Regional = Regional;
        this.Cedente = Cedente;
        this.Cargo = Cargo;
        this.Perfil = Perfil;
        this.Estado = Estado;
        this.Sucursal_Nova = Sucursal_Nova;
    }

    public SmallUsuarios() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getTipo_Usuario() {
        return Tipo_Usuario;
    }

    public void setTipo_Usuario(String Tipo_Usuario) {
        this.Tipo_Usuario = Tipo_Usuario;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getRut() {
        return Rut;
    }

    public void setRut(String Rut) {
        this.Rut = Rut;
    }

    public String getRegional() {
        return Regional;
    }

    public void setRegional(String Regional) {
        this.Regional = Regional;
    }

    public String getCedente() {
        return Cedente;
    }

    public void setCedente(String Cedente) {
        this.Cedente = Cedente;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getSucursal_Nova() {
        return Sucursal_Nova;
    }

    public void setSucursal_Nova(String Sucursal_Nova) {
        this.Sucursal_Nova = Sucursal_Nova;
    }



    
}
