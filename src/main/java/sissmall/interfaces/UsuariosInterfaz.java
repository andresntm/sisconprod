/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sissmall.interfaces;

import javax.sql.DataSource;
import sissmall.entidades.SmallUsuarios;

/**
 *
 * @author exesilr
 */
public interface UsuariosInterfaz {
      public void setDataSource(DataSource ds);
      
     public SmallUsuarios getUsuarioPorRut(int Rut);
      
      
}
