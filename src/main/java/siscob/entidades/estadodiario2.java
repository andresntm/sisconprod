/**
 * Condonacion.java - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscob.entidades;

/**
 *
 * @author exesilr
 */
public class estadodiario2 {

    public estadodiario2() {
    }

    public estadodiario2(String jusgado, String cliente, String id_tribunal_plataformma, String nro_ingreso, String partes, String nw_rol, String nw_rol2, String ju_rol, String Abogado, String cl_cliente, String cl_rut) {
        this.jusgado = jusgado;
        this.cliente = cliente;
        this.id_tribunal_plataformma = id_tribunal_plataformma;
        this.nro_ingreso = nro_ingreso;
        this.partes = partes;
        this.nw_rol = nw_rol;
        this.nw_rol2 = nw_rol2;
        this.ju_rol = ju_rol;
        this.Abogado = Abogado;
        this.cl_cliente = cl_cliente;
        this.cl_rut = cl_rut;
    }

    public String getJusgado() {
        return jusgado;
    }

    public void setJusgado(String jusgado) {
        this.jusgado = jusgado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getId_tribunal_plataformma() {
        return id_tribunal_plataformma;
    }

    public void setId_tribunal_plataformma(String id_tribunal_plataformma) {
        this.id_tribunal_plataformma = id_tribunal_plataformma;
    }

    public String getNro_ingreso() {
        return nro_ingreso;
    }

    public void setNro_ingreso(String nro_ingreso) {
        this.nro_ingreso = nro_ingreso;
    }

    public String getPartes() {
        return partes;
    }

    public void setPartes(String partes) {
        this.partes = partes;
    }

    public String getNw_rol() {
        return nw_rol;
    }

    public void setNw_rol(String nw_rol) {
        this.nw_rol = nw_rol;
    }

    public String getNw_rol2() {
        return nw_rol2;
    }

    public void setNw_rol2(String nw_rol2) {
        this.nw_rol2 = nw_rol2;
    }

    public String getJu_rol() {
        return ju_rol;
    }

    public void setJu_rol(String ju_rol) {
        this.ju_rol = ju_rol;
    }

    public String getAbogado() {
        return Abogado;
    }

    public void setAbogado(String Abogado) {
        this.Abogado = Abogado;
    }

    public String getCl_cliente() {
        return cl_cliente;
    }

    public void setCl_cliente(String cl_cliente) {
        this.cl_cliente = cl_cliente;
    }

    public String getCl_rut() {
        return cl_rut;
    }

    public void setCl_rut(String cl_rut) {
        this.cl_rut = cl_rut;
    }

    String jusgado;
    String cliente;
    String id_tribunal_plataformma;
    String nro_ingreso;
    String partes;
    String nw_rol;
    String nw_rol2;
    String ju_rol;
    String Abogado;
    String cl_cliente;
    String cl_rut;

    
    
}
