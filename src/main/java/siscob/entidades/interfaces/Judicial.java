/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscob.entidades.interfaces;

import siscon.entidades.interfaces.*;
import java.util.List;
import siscob.entidades.estadodiario2;
import siscon.entidades.APPResumenCondonacionesInforme;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.BandejaCondonacionesInforme;
import siscon.entidades.ConDetOper;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionAprobadasJoin;
import siscon.entidades.CondonacionReparoJoin;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.GlosaENC;
import siscon.entidades.MarcaBanco;
import siscon.entidades.Reparos;
import siscon.entidades.ResumenCondonacionesInforme;
import siscon.entidades.Tabla_Condonacion;
import siscon.entidades.UsuarioPermiso;

/**
 *
 * @author exesilr
 */
public interface Judicial {

    public int consulta_id();

    public boolean Guardar();

    public Condonacion getCurrentCondonacion();

    public void setCurrentCondonacion(Condonacion CurrentCondonacion);

    public void setDettaleOPeraciones(List<DetalleCliente> detOpers);

    public int GuardarCondonacion(int estado, int tipocondonacion, String Tipocon);

    public List<CondonacionTabla2> GetCondonacionesAplicada();

    public List<CondonacionTabla> GetCondonacionesAplicada(String cuenta);

//    public List<CondonacionTabla> GetCondonacionesPendientes();
    public List<CondonacionTabla2> GetCondonacionesPendientes2();

    public List<CondonacionTabla2> GetCondonacionesPendientes2(String cuenta);

    public boolean SetCondonacionesAprobadaAnalista(int idcondonacion);

    public List<CondonacionTabla2> GetCondonacionesReparadMotivos();

    public int GuardarCondonacion(final int estado, final int tipoOperacion, final String TipoCon, final int rechazo);

    public List<CondonacionTabla2> GetCondonacionesReparads();

    public List<CondonacionTabla2> GetCondonacionesReparads(String cuenta);

    public List<CondonacionTabla> GetCondonacionesAprobadas();

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginated();

    public List<CondonacionTabla2> GetCondonacionesAprobadas(String cuenta);

    public boolean SetCondonacionesReparadaAnalista(int idcondonacion);

//    public boolean SetCondonacionesAprobadaAnalistaApliEjecutico(int idcondonacion);
    public boolean insertMarca(MarcaBanco marBan);

    public MarcaBanco selectMarca(int id_marcaBanco);

    public boolean isClinete(String rut);

    public List<CondonacionTabla2> GetCondonacionesCerradasAproAnalista(String cuenta);
//    public List<CondonacionTabla> GetCondonacionesPendientes(String Modulo);

    public boolean SetCondonacionesAprobadaZonal(int idcondonacion);

    public int getSumaInterezPersonalizado(int rutcolaborador, int rutcliente);

    public boolean SetCondonacionIntoRegla(int idcondonacion, int id_colaborador, int id_regla);

    public List<ConDetOper> getListDetOperCondonacion(int idCondonacion);

    public int getClienteEnCondonacion(int condonacion);

    public int insertUsrProvision(final int id_colaborador, final int idCondonacion, final float monto, final String dv_desc, final String rutcliente);

    public int insertUsrProvision(final int id_colaborador, final int idCondonacion, final float monto, String dv_desc, final String rutcliente, int diid);

    public AdjuntarENC GetAdjuntoCond(int id_cond);

    public GlosaENC GetGlosaCond(int id_cond);

    public boolean SetCambiaEstadoCondonaciones(
            final String ModuloOrigen,
            final String ModuloDestino,
            final UsuarioPermiso UsuarioActual,
            final int Condonacion,
            final String EstadoOrigen,
            final String EstadoDestino);

    public List<Reparos> GetReparosXcondonacion(int id_condonacion);

    public List<CondonacionTabla> GetCondonacionesPendientesCountReparo();

    public List<CondonacionTabla2> GetCondonacionesPendientes2CountReparo();

    public Boolean GetUltimoReparosXcondonacion(int id_condonacion, String cuenta);

    public List<Reparos> GetReparosXcondonacion(int id_condonacion, String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAplicada2();

    public List<CondonacionTabla2> GetCondonacionesAplicada2(String cuenta);

    public int GuardarCondonacionPM(final String estado, final String tipoOperacion, final String TipoCon);

    public List<CondonacionTabla2> GetCondonacionesPendientes2Pm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesPendientes2Pm();

    public List<CondonacionTabla2> GetCondonacionesPendientes2CountReparoPm();

    public boolean SetCondonacionesReparadaZonal(int idcondonacion);

    public List<CondonacionTabla2> GetCondonacionesReparadMotivosPm();

    public List<CondonacionTabla2> GetCondonacionesReparadsPm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAplicada2Pm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAprobadasPm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginatedPm();

    public List<CondonacionTabla2> GetCondonacionesReparadMotivosPm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginatedPm(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesCerradasPm(String cuenta);

    public List<Reparos> GetReparosXcondonacion_ZonalPyme(int id_condonacion);

    public List<CondonacionTabla2> GetCondonacionesCerradasAproZonal(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacion(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesAplicadas(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesRechazadas(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesProrrogadas(String cuenta);

    public List<CondonacionTabla2> GetCondonacionesRechazadas();

    public List<CondonacionTabla2> GetCondonacionesProrrogadas();

    public List<CondonacionTabla2> GetCondonacionesRechazadasAprobadores(String cuenta, int cedente);

    public List<CondonacionTabla2> GetCondonacionesProrrogadasAprobadores(String cuenta, int cedente);

    public List<Tabla_Condonacion> GetCondonaciones();

    public int isCliente(String rut);
    
    
    public boolean SetCondonacionIntoReglaV2Decimal(int id_colaborador, int idcondonacion, int id_regla);
    
    
    public List<BandejaCondonacionesInforme> GetCondonacionesAplicadasForContab(String cuenta);
    public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInforme(String cuenta);
    public List<APPResumenCondonacionesInforme> GetCondonacionesAplicadasAndResumenInforme(String cuenta);
    
    
    
    
    
    public List<estadodiario2> GetEstadoDiario(String cuenta);
    
}
