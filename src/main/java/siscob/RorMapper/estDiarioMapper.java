/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscob.RorMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscob.entidades.estadodiario2;

/**
 *
 * @author esilves
 */
public class estDiarioMapper implements RowMapper<estadodiario2>{
    
    
                public estadodiario2 mapRow(ResultSet rs, int rowNum) throws SQLException {
        estadodiario2 estdia = new estadodiario2();
        
        estdia.setJusgado(rs.getString("descripcion_podel_judicial"));
        estdia.setCl_cliente(rs.getString("cl_cliente"));
        estdia.setId_tribunal_plataformma(rs.getString("id_tribunal"));
        estdia.setNro_ingreso(rs.getString("nro_ingreso"));
        estdia.setPartes(rs.getString("partes"));
        estdia.setNw_rol(rs.getString("NW_ROL"));
        estdia.setNw_rol2(rs.getString("nw_rol_2"));
        estdia.setJu_rol(rs.getString("ju_rol"));
        estdia.setAbogado(rs.getString("ab_abogado"));
        estdia.setCliente(rs.getString("cl_cliente"));
        estdia.setCl_rut(rs.getString("cl_rut"));
        return estdia;
    }
    
    
    
}
