/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscob.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.ext.Selectable;
import siscob.entidades.estadodiario2;
import siscob.entidades.implementaciones.JudicialImpl;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.ConDetOper;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author exesilr
 */
public class EstadosDiariosController extends SelectorComposer<Component> {

    @Wire
    Grid grd_BanEstDia;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<estadodiario2> lEstDiaFilter;

//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<estadodiario2> lEstDiaFinal;
    ////////////////////////////////////////////////////////
    final CondonacionInterfaz cond;
    JudicialImpl jud;
    ListModelList<estadodiario2> bandEstDia;
    Condonacion CurrentCondonacion;
    private List<estadodiario2> listEstDia;
    Window window;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    int idCondonacion;
    Session sess;
    UsuarioPermiso permisos;
    String cuenta;
    String Nombre;
    MetodosGenerales MT;
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private static List<estadodiario2> DataGridList = new ArrayList<estadodiario2>();
    Session session;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();

    public EstadosDiariosController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());
        listEstDia = null;
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.jud = new JudicialImpl(mmmm.getDataSourceCob());
       
       
       
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();
        Nombre = permisos.getNombre();
        session = Sessions.getCurrent();
        cargaPag();

    }

    private void cargaPag() {

        try {
            sTextLocal = null;
            sTextLocalResp = null;
          //  eventos();
            lEstDiaFinal = new ArrayList<estadodiario2>();
            lEstDiaFilter = new ArrayList<estadodiario2>();
            lEstDiaFinal = this.jud.GetEstadoDiario(cuenta);
            lEstDiaFilter = lEstDiaFinal;
            
            cargaGrid(lEstDiaFilter);
           // muestraCol();
        } catch (Exception e) {
            System.out.print("ErrorEjePendientes Linea 157 Error:["+e+"]");
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones cerradas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<estadodiario2> lCondT2) {

        listEstDia = lCondT2;
        bandEstDia = new ListModelList<estadodiario2>(listEstDia);

        grd_BanEstDia.setModel(bandEstDia);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_BanEstDia.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lEstDiaFilter = filterGrid();
                cargaGrid(lEstDiaFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lEstDiaFilter = filterGrid();
                cargaGrid(lEstDiaFilter);
                txt_filtra.select();

            }
        });

    }

    private List<estadodiario2> filterGrid() {
        List<estadodiario2> lFilterGrid = new ArrayList<estadodiario2>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lEstDiaFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lEstDiaFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (estadodiario2 cT2 : lEstDiaFinal) {
                        String cadena = cT2.getAbogado()+ ";"
                                + cT2.getCl_cliente() + ";"
                                + cT2.getCl_rut() + ";"
                                + cT2.getId_tribunal_plataformma() + ";"
                                + cT2.getJu_rol()+ ";"
                                + cT2.getJusgado()+ ";"
                                + cT2.getNro_ingreso() + ";"
                                + cT2.getNw_rol() + ";"
                                + cT2.getNw_rol2() + ";"
                                + cT2.getPartes();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lEstDiaFinal;
                }
            }
        }

        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lEstDiaFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }

        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }

            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        String template = "Analista/Poput/AnSacabopMostrar.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
