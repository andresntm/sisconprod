/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.genral;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import javax.sql.DataSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
/**
 *
 * @author excosoc
 */
public class MetodosGenerales {
    Session session = Sessions.getCurrent();

    //varibles locales
    int validaDet;
    int validaSegDet;
    //Variables cantidad de hitos
    int[] Entradas;
    int[] EntradasErr;
    String[] Bandeja2;
    int TotalEntradas;
    private SimpleJdbcCall execSp;
    private DataSource data;
    public MetodosGenerales() {
    }

    public boolean validarRut(String rut) {

        boolean validacion = false;

        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            String rutAux = rut.substring(0, rut.length() - 1);
            String dv = rut.substring(rut.length() - 1, rut.length());
            int cantidad = rutAux.length();
            int factor = 2;
            int suma = 0;
            String verificador = "";

            for (int i = cantidad; i > 0; i--) {
                if (factor > 7) {
                    factor = 2;
                }
                suma += (Integer.parseInt(rut.substring((i - 1), i))) * factor;
                factor++;

            }

            verificador = String.valueOf(11 - suma % 11);
            if (verificador.equals(dv)) {
                validacion = true;
            } else {
                if ((verificador.equals("10")) && (dv.toLowerCase().equals("k"))) {
                    validacion = true;
                } else {
                    if ((verificador.equals("11") && dv.equals("0"))) {
                        validacion = true;
                    } else {
                        validacion = false;
                    }
                }
            }
            return validacion;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

      public char CalculaDv(int rut) {

        char dvCorrecto = ' ';

        try {
            int cantidad = Integer.toString(rut).length();
            int factor = 2;
            int suma = 0;
            String rutV = "";
            rutV = Integer.toString(rut);
            String verificador = "";

            for (int i = cantidad; i > 0; i--) {
                if (factor > 7) {
                    factor = 2;
                }
                suma += (Integer.parseInt(rutV.substring((i - 1), i))) * factor;
                factor++;

            }

            verificador = String.valueOf(11 - suma % 11);

            if ((verificador.equals("10"))) {
                dvCorrecto = 'k';
            } else {
                if ((verificador.equals("11"))) {
                    dvCorrecto = '0';
                } else {
                    dvCorrecto = verificador.charAt(0);
                }
            }

            return dvCorrecto;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }

        return dvCorrecto;
    }

    /**
     * Permite convertir un String en fecha (Date).
     *
     * @param fecha Cadena de fecha dd/MM/yyyy
     * @return Objeto Date
     */
    public Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }
    

    public boolean like(String toBeCompare, String by) {
        if (by != null) {
            if (toBeCompare != null) {
                if (by.startsWith("%") && by.endsWith("%")) {
                    int index = toBeCompare.toLowerCase().indexOf(by.replace("%", "").toLowerCase());
                    if (index < 0) {
                        return false;
                    } else {
                        return true;
                    }
                } else if (by.startsWith("%")) {
                    return toBeCompare.endsWith(by.replace("%", ""));
                } else if (by.endsWith("%")) {
                    return toBeCompare.startsWith(by.replace("%", ""));
                } else {
                    return toBeCompare.equals(by.replace("%", ""));
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


}
