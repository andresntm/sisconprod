/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.genral;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import siscore.genral.Contacto;

import org.springframework.dao.DataAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

/**
 * An implementation of the ContactDAO interface.
 * @author www.codejava.net
 *
 */
public class ContactoDAOImpl implements ContactoDAO {

	private JdbcTemplate jdbcTemplate;
	
	public ContactoDAOImpl(DataSource dataSource) {
              System.out.println("#-------Mostrando DatosDB--CONTACTOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA-----------@@@");
		jdbcTemplate = new JdbcTemplate(dataSource);
                String sql = "select * from contact";
                
                
                
                		List<Contacto> listContact = jdbcTemplate.query(sql, new RowMapper<Contacto>() {
                   
			@Override
			public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
				Contacto aContact = new Contacto();
	
				aContact.setId(rs.getInt("contact_id"));
				aContact.setName(rs.getString("name"));
				aContact.setEmail(rs.getString("email"));
				aContact.setAddress(rs.getString("address"));
				aContact.setTelephone(rs.getString("telephone"));
				
				return aContact;
			}
			
		});
               // List<Contacto> listContact = jdbcTemplate.queryForList(sql);
               // List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
                //rows.size();
                     /*  for(Map<String, Object> row : rows){
          String id = row.get("name").toString();
          String name = (String)row.get("email").toString();
         
          System.out.println("DATOS["+id + "] [" + name+"]" ); 
        }*/
                  System.out.println("#-------Mostrando DatosDB--BB"+listContact.get(0).getEmail().toString()+"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB-----------@@@");
	}

	@Override
	public void saveOrUpdate(Contacto contacto) {
		if (contacto.getId() > 0) {
			// update
			String sql = "UPDATE contacto SET name=?, email=?, address=?, "
						+ "telephone=? WHERE id_contacto=?";
			jdbcTemplate.update(sql, contacto.getName(), contacto.getEmail(),
					contacto.getAddress(), contacto.getTelephone(), contacto.getId());
		} else {
			// insert
			String sql = "INSERT INTO contacto (name, email, address, telephone)"
						+ " VALUES (?, ?, ?, ?)";
			jdbcTemplate.update(sql, contacto.getName(), contacto.getEmail(),
					contacto.getAddress(), contacto.getTelephone());
		}
		
	}

	@Override
	public void delete(int contactoId) {
		String sql = "DELETE FROM contacto WHERE id_contacto=?";
		jdbcTemplate.update(sql, contactoId);
	}

	@Override
	public List<Contacto> list() {
            System.out.println("#-------Mostrando DatosDB--CONTACTOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA-----------@@@");
		String sql = "select * from contacto";
                 System.out.println("#-------Mostrando DatosDB--CONTACTO22222222222222222222222222222222222222-----------@@@");
		List<Contacto> listContact = jdbcTemplate.query(sql, new RowMapper<Contacto>() {
                   
			@Override
			public Contacto mapRow(ResultSet rs, int rowNum) throws SQLException {
				Contacto aContact = new Contacto();
	
				aContact.setId(rs.getInt("id_contacto"));
				aContact.setName(rs.getString("name"));
				aContact.setEmail(rs.getString("email"));
				aContact.setAddress(rs.getString("address"));
				aContact.setTelephone(rs.getString("telephone"));
				
				return aContact;
			}
			
		});
		
		return listContact;
	}

	@Override
	public Contacto get(int contactoId) {
		String sql = "SELECT * FROM contact WHERE contact_id=" + contactoId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Contacto>() {

			@Override
			public Contacto extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Contacto contacto = new Contacto();
					contacto.setId(rs.getInt("contact_id"));
					contacto.setName(rs.getString("name"));
					contacto.setEmail(rs.getString("email"));
					contacto.setAddress(rs.getString("address"));
					contacto.setTelephone(rs.getString("telephone"));
					return contacto;
				}
				
				return null;
			}
			
		});
	}

}
