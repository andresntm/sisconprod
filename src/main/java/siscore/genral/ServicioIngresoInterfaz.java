/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.genral;



/**
 *
 * @author exesilr
 */
public interface ServicioIngresoInterfaz {
    
    
    
    	/**login with account and password**/
	public boolean login(String account, String password);
	
	/**logout current user**/
	public void logout();
	
	/**get current user credential**/
	public UsuarioCredential getUsuarioCredential();
        
        
        
}
