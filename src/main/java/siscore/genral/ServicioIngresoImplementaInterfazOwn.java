/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package siscore.genral;

import siscore.*;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

public class ServicioIngresoImplementaInterfazOwn extends ServicioIngresoImplementaInterfaz{
	private static final long serialVersionUID = 1L;
	
	UsuariosInterfazService userInfoService = new UsuarioImplementacionInterfaz();
	
	@Override
	public boolean login(String nm, String pd) {
		Usuarios user = userInfoService.findUsuarios(nm);
		//a simple plan text password verification
		if(user==null || !user.getPassword().equals(pd)){
			return false;
		}
		
		Session sess = Sessions.getCurrent();
		UsuarioCredential cre = new UsuarioCredential(user.getNombre(),user.getEmail());
		//just in case for this demo.
		if(cre.isAnonymous()){
			return false;
		}
		sess.setAttribute("UsuarioCredential",cre);
		
		//TODO handle the role here for authorization
		return true;
	}
	
	@Override
	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("userCredential");
	}
}
