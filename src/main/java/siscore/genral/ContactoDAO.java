/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.genral;

import java.util.List;

import siscore.genral.Contacto;

/**
 * Defines DAO operations for the contact model.
 * @author www.codejava.net
 *
 */
public interface ContactoDAO {
	
	public void saveOrUpdate(Contacto contacto);
	
	public void delete(int contacotId);
	
	public Contacto get(int contactoId);
	
	public List<Contacto> list();
}
