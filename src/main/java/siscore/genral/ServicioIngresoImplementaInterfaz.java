/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package siscore.genral ;

import sisboj.necesarias.*;
import java.io.Serializable;


import siscore.genral.ServicioIngresoInterfaz;
import siscore.genral.UsuarioCredential;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

public class ServicioIngresoImplementaInterfaz implements ServicioIngresoInterfaz,Serializable{
	private static final long serialVersionUID = 1L;

	public UsuarioCredential getUsuarioCredential(){
		Session sess = Sessions.getCurrent();
		UsuarioCredential cre = (UsuarioCredential)sess.getAttribute("userCredential");
		if(cre==null){
			cre = new UsuarioCredential();//new a anonymous user and set to session
			sess.setAttribute("UsuarioCredential",cre);
		}
		return cre;
	}
	

	public boolean login(String nm, String pd) {
		// will be implemented in chapter 8
		return false;
	}


	public void logout() {
		// will be implemented in chapter 8
	}
}
