/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.pruebas;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import org.zkoss.lang.Threads;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.DetalleCliente;
import siscon.entidades.JudicialCliente;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;

/**
 *
 * @author exesilr
 */
public class gridController extends SelectorComposer<Window> {
        Session session;

    @Wire
    Grid BanEntrClientee;
    @Wire
    Grid BanEntrJuducial;
    private boolean firstGridVisible;
    @Wire
    Grid griddos;
    @Wire
    Bandbox buscarinput;

    @Wire
    Button btn_condona;
    @Wire
    Button btn_sacabop;
    //MvcConfig mmmm = new MvcConfig();
    MvcConfig mmmm = new MvcConfig();
    
    Include pageref;
    Hlayout nn;
    @Wire
    Vlayout contenido;
    
   Div  yBHQle;
    UsuarioPermiso permisos;
    // @WireVariable
    // ListModelList<Contacto> myListModel;    

    //ContactoDAO contactDAO;   
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    final DetalleOperacionesClientes detoper;
    final JudicialClienteInterz JudCliente;
    String cuenta;
    
        @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose((Window) comp);
     //  session = Sessions.getCurrent(); 
        session = Sessions.getCurrent();
        //sess = Sessions.getCurrent();
        //permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
       
        permisos = (UsuarioPermiso) session.getAttribute("UsuarioPermisos");
        
         cuenta = permisos.getCuenta();
       
      

    }
    
    public gridController() throws SQLException {
       
               this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
         this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        //  this.contactDAO = mmmm.getContactoDAO();
        //this.informe=new InformesImpl(mmmm.getDataSourceLucy());
    }    
    
    @Listen("onClick=#btn-find-rut1; onOK=#loginWin")
    public void doLogin() throws SQLException {

        //Grid BanEntrCliente = null;
        System.out.println("#------------@@@@@@@@@@@@@@@@@[Controllador de busquedaSSSSSS]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        // ListDetOperModel.clear();
        //  this.contactDAO = mmmm.getContactoDAO();

        String nm = buscarinput.getValue();
        if (nm == "") {
            Messagebox.show("STRINGGGG[VACIO]");
            return;
        }
        if (nm != null || nm.isEmpty() || nm.length() > 0) {
            
            String mm=nm.replace(".","");
            String[] ParteEntera = mm.split("-");
            List<DetalleCliente> ListDetOper = detoper.Cliente(Integer.parseInt(ParteEntera[0]),cuenta);
            List<JudicialCliente> ListJudCliente = JudCliente.JudClie(Integer.parseInt(ParteEntera[0]), "jliguen");
            System.out.println("#------------@@@@Exito consultando SP de JudicialCliente[," + ListJudCliente.get(0).getAbogado() + "@@@@@@@@@---------#");
            if (!ListDetOper.isEmpty()) {
                System.out.println("#------------@@@@Exito consultando SP de Oper[," + ListDetOper.get(0).getCedente() + "," + ListDetOper.get(0).getOperacion() + "," + ListDetOper.get(0).getOperacionOriginal() + "," + ListDetOper.get(0).getTipoCedente() + "]@@@@@@@@@---------#");

                ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
                ListJudClienteModel=new ListModelList<JudicialCliente>(ListJudCliente);
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                System.out.println("#------------@@@@ListDetOperModel.get(0).getOperacionOriginal()[," + ListDetOperModel.get(2).getOperacionOriginal() + "]GetMora[" + ListDetOperModel.get(2).getMora() + "]GetSaldoInsoluto[" + ListDetOperModel.get(2).getSaldoinsoluto() + "] El Valor Con NumberFormat de JAVA[" + nf.format(ListDetOperModel.get(2).getMora()) + "," + nf.format(ListDetOperModel.get(2).getMora()).replaceFirst("Ch", "") + "]Ahora Imprimo El valor De la Clase[" + ListDetOperModel.get(2).getMoraEnPesosChileno() + "] @@@@@@@@@---------#");
                BanEntrClientee.setModel(ListDetOperModel);
                BanEntrJuducial.setModel(ListJudClienteModel);
                BanEntrClientee.setVisible(true);
                 BanEntrClientee.renderAll();
                System.out.println("#------------&&&&&&&&&&&&&SearchController ListDetOperModel.getSize()["+ListDetOperModel.getSize()+"]Grid_SacabopXX [" + BanEntrClientee.getModel().toString()+ "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
                //btn_condona.setVisible(true);
                
                
                for (Component row : BanEntrClientee.getRows().getChildren()) {
                    row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                        @Override
                        public void onEvent(Event arg0) throws Exception {
                            Row row = (Row) arg0.getTarget();
                            Boolean rowSelected = (Boolean) row.getAttribute("Selected");
                            String nnnnn= row.getAttributes().toString();
                            if (rowSelected != null && rowSelected) {
                                row.setAttribute("Selected", false);
                                // row.setStyle("");
                                row.setSclass("");
                                //row.setc
                            } else {
                                for (Component rownn : BanEntrClientee.getRows().getChildren()) {
                                    rownn.getClass();
                                    Row nnn=(Row) rownn;
                                 nnn.setAttribute("Selected", false);
                                 nnn.setSclass("");
                                }
                                row.setAttribute("Selected", true);
                                // row.setStyle("background-color: #CCCCCC !important");   // inline style
                                row.setSclass("z-row-background-color-on-select");         // sclass
                            }
                        }
                    });
                }
                
                
                
                btn_sacabop.setVisible(true);
                BanEntrJuducial.setVisible(true);
            } else {
                Messagebox.show("NO EXISTEN DATOS DEL CLIENTE");
            }
        } else {
            Messagebox.show("KSDJAKLDJKLSJDKL----USER[VACIO]");
            return;
        }

    }
    
    	public void doLongOperation() {
		Threads.sleep(5000); //simulate a 5 sec operation
          	//output.setValue("the result is 42"); //update the UI
                
	}
    
    
       @Listen("onClick=#btn_condona; onOK=#loginWin")
 public void simulacondonacion() throws SQLException {
         System.out.println("#------------@@@@SESIONNNNNNNNNNn"+ Executions.getCurrent().getDesktop().getPages().toString()+ " @@@@@@@@@---------#");

     String jjjjj = Executions.getCurrent().getDesktop().getPages().toString();
    String[]    hhhh = jjjjj.split(" ");
    String xxx=    hhhh[1].replace("_]]","");

     String iddiv=xxx.concat("le");
	 System.out.println("#------------@@@@SESIONNNNNNNNNNn["+ xxx +"] @@@@@@@@@---------#");

          System.out.println("#------------@@@@Intentificador del Div de Include["+ iddiv +"] @@@@@@@@@---------#");
                   Div nnnn = null;
         nnnn.setId(iddiv);
         
         //String iddiv=xxx+"le";
         Include headerPage = new Include();
         headerPage.setSrc("usuarios.zul");
         headerPage.setParent(nnnn);



 
 }    
    
    

}
