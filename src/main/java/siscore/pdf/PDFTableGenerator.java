package siscore.pdf;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

public class PDFTableGenerator {

    
  public  PDFTableGenerator(){}
    
    
    
    // Generates document from Table object
    public void generatePDF(Table table) throws IOException, COSVisitorException, WriterException {
        PDDocument doc = null;
        try {
            doc = new PDDocument();
            drawTable(doc, table);
            doc.save("sample.pdf");
        } finally {
            if (doc != null) {
                doc.close();
            }
        }
    }
    // Generates document from Table object
    public PDDocument generatePDFdoc(Table table) throws IOException, COSVisitorException, WriterException {
        PDDocument doc = null;
       
            doc = new PDDocument();
            drawTable(doc, table);
            //doc.save("sample.pdf");
            return doc;

    }
    // Configures basic setup for the table and draws it page by page
    public void drawTable(PDDocument doc, Table table) throws IOException, WriterException {
        // Calculate pagination
        Integer rowsPerPage = new Double(Math.floor(table.getHeight() / table.getRowHeight())).intValue() - 1; // subtract
        Integer numberOfPages = new Double(Math.ceil(table.getNumberOfRows().floatValue() / rowsPerPage)).intValue();

        // Generate each page, get the content and draw it
        for (int pageCount = 0; pageCount < numberOfPages; pageCount++) {
            PDPage page = generatePage(doc, table);
            
            
            // creamos el contenido de la pagina en document
            PDPageContentStream contentStream = generateContentStream(doc, page, table);
            
            
           // traemos el contenido para la pagina que se encuentra en la clase tabla
            String[][] currentPageContent = getContentForCurrentPage(table, rowsPerPage, pageCount);
            
            
            
            if (pageCount == 0) {
                 drawFirstPage(table, currentPageContent, contentStream,doc);
                
            } else {
drawCurrentPage(table, currentPageContent, contentStream);
               

            }
        }
    }

    // Draws current page table grid and border lines and content
    private void drawCurrentPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream)
            throws IOException {
        float tableTopY = table.isLandscape() ? table.getPageSize().getWidth() - table.getMargin() : table.getPageSize().getHeight() - table.getMargin();

        // Draws grid and borders
        drawTableGrid(table, currentPageContent, contentStream, tableTopY);

        // Position cursor to start drawing content
        float nextTextX = table.getMargin() + table.getCellMargin();
        // Calculate center alignment for text in cell considering font height
        float nextTextY = tableTopY - (table.getRowHeight() / 2) - ((table.getTextFont().getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * table.getFontSize()) / 4);

        // Write column headers
        writeContentLine(table.getColumnsNamesAsArray(), contentStream, nextTextX, nextTextY, table);
        nextTextY -= table.getRowHeight();
        nextTextX = table.getMargin() + table.getCellMargin();

        // Write content
        for (int i = 0; i < currentPageContent.length; i++) {
            writeContentLine(currentPageContent[i], contentStream, nextTextX, nextTextY, table);
            nextTextY -= table.getRowHeight();
            nextTextX = table.getMargin() + table.getCellMargin();
        }

        contentStream.close();
    }

    
        // Draws current page table grid and border lines and content
    private void drawFirstPage(Table table, String[][] currentPageContent, PDPageContentStream contentStream,PDDocument document)  throws IOException, WriterException {
        float tableTopY = table.isLandscape() ? table.getPageSize().getWidth() - table.getMargin() : table.getPageSize().getHeight() - table.getMargin();


            // Position cursor to start drawing content
        float nextTextX = table.getMargin() + table.getCellMargin();
        // Calculate center alignment for text in cell considering font height
        float nextTextY = tableTopY - (table.getRowHeight() / 2) - ((table.getTextFont().getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * table.getFontSize()) / 4);


        // Write column headers
        writeContentLine2(table.getColumnsNamesAsArray(), contentStream, nextTextX, nextTextY, table,document);
        nextTextX += 90;
        nextTextY -= 90;
        
   
        
        
    
        
        
                // Draws grid and borders
        drawTableGrid(table, currentPageContent, contentStream, tableTopY-90);
        
        
        writeContentLine(table.getColumnsNamesAsArray(), contentStream, nextTextX, nextTextY, table);
        
        
        
        nextTextY -= table.getRowHeight();
        nextTextX = table.getMargin() + table.getCellMargin();

        // Write content
        for (int i = 0; i < currentPageContent.length; i++) {
            writeContentLine(currentPageContent[i], contentStream, nextTextX, nextTextY, table);
            nextTextY -= table.getRowHeight();
            nextTextX = table.getMargin() + table.getCellMargin();
        }

        contentStream.close();
    }
    
       // Writes the content for one line
    private void writeContentLine2(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY,Table table,PDDocument document) throws IOException, WriterException {
                                 
        
        contentStream.beginText();
                            contentStream.setFont(PDType1Font.COURIER, 20);
                    contentStream.setNonStrokingColor(Color.BLUE);
                    contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
                    contentStream.drawString("Gu�a Despacho Documentos ");
                    contentStream.endText();
        
        
        contentStream.setFont(PDType1Font.COURIER, 10);
        contentStream.setNonStrokingColor(Color.BLACK);
        
        
                   contentStream.beginText();
                    contentStream.moveTextPositionByAmount(nextTextX, nextTextY-40);
                   contentStream.drawString(table.getCodlote());
                   contentStream.endText();
                    
                    
                     nextTextX += 150;
                    
                    BitMatrix bitMatrixXXXX = new Code128Writer().encode(table.getCodlote(), BarcodeFormat.CODE_128, 150, 50, null);          
                    BufferedImage buffImg=MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);      
                    PDXObjectImage ximage = new PDPixelMap(document, buffImg);           
                    contentStream.drawXObject(ximage, nextTextX, nextTextY-70, 150, 50);
        
        nextTextX += 90;
        nextTextY +=150;
        
        for (int i = 0; i < table.getNumberOfColumns(); i++) {
            String text = lineContent[i];
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
            contentStream.drawString(text != null ? text : "");
            contentStream.endText();
   
            nextTextX += table.getColumns().get(i).getWidth();
        }
    } 
    // Writes the content for one line
    private void writeContentLine(String[] lineContent, PDPageContentStream contentStream, float nextTextX, float nextTextY,
            Table table) throws IOException {
        for (int i = 0; i < table.getNumberOfColumns(); i++) {
            String text = lineContent[i];
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(nextTextX, nextTextY);
            contentStream.drawString(text != null ? text : "");
            contentStream.endText();
            nextTextX += table.getColumns().get(i).getWidth();
        }
    }

    private void drawTableGrid(Table table, String[][] currentPageContent, PDPageContentStream contentStream, float tableTopY)
            throws IOException {
        // Draw row lines
        float nextY = tableTopY;
        for (int i = 0; i <= currentPageContent.length + 1; i++) {
            contentStream.drawLine(table.getMargin(), nextY, table.getMargin() + table.getWidth(), nextY);
            nextY -= table.getRowHeight();
        }

        // Draw column lines
        final float tableYLength = table.getRowHeight() + (table.getRowHeight() * currentPageContent.length);
        final float tableBottomY = tableTopY - tableYLength;
        float nextX = table.getMargin();
        for (int i = 0; i < table.getNumberOfColumns(); i++) {
            contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);

            nextX += table.getColumns().get(i).getWidth();
            
        }
       
        contentStream.drawLine(nextX, tableTopY, nextX, tableBottomY);
        contentStream.setNonStrokingColor(Color.BLACK);
    }

    private String[][] getContentForCurrentPage(Table table, Integer rowsPerPage, int pageCount) {
        int startRange = pageCount * rowsPerPage;
        int endRange = (pageCount * rowsPerPage) + rowsPerPage;
        if (endRange > table.getNumberOfRows()) {
            endRange = table.getNumberOfRows();
        }
        return Arrays.copyOfRange(table.getContent(), startRange, endRange);
    }

    private PDPage generatePage(PDDocument doc, Table table) {
        PDPage page = new PDPage();
        page.setMediaBox(table.getPageSize());
        page.setRotation(table.isLandscape() ? 90 : 0);
        doc.addPage(page);
        return page;
    }

    private PDPageContentStream generateContentStream(PDDocument doc, PDPage page, Table table) throws IOException {
        PDPageContentStream contentStream = new PDPageContentStream(doc, page, false, false);
        // User transformation matrix to change the reference when drawing.
        // This is necessary for the landscape position to draw correctly
        if (table.isLandscape()) {
            contentStream.concatenate2CTM(0, 1, -1, 0, table.getPageSize().getWidth(), 0);
        }
        contentStream.setFont(table.getTextFont(), table.getFontSize());
        return contentStream;
    }
}
