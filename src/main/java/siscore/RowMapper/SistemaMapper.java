/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscore.entidades.Sistema;


/**
 *
 * @author exesilr
 */
public class SistemaMapper implements RowMapper<Sistema>{
            public Sistema mapRow(ResultSet rs, int rowNum) throws SQLException {
        Sistema _sis = new Sistema();
        _sis.setId(rs.getInt("id"));
        _sis.setDescripcion(rs.getString("sis_desc"));
        _sis.setRegistrado(rs.getString("registrado"));
        _sis.setCodigo(rs.getString("codigo"));

        return _sis;
    }
}
