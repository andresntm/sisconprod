/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.controller;

import siscon.controller.*;
import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.SisconinfoperacionV2;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.UsuariosSM;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author excosoc
 */
public class PerfilSiscore extends SelectorComposer<Window> {

    @Wire
    private Grid grd_InformeCond;
    @Wire
    private Radiogroup rdg_tip1;
    @Wire
    private Radio rdb_retail;
    @Wire
    private Radio rdb_pyme;
    ListModelList<UsuariosSM> bandeCondTerminada;
    private List<UsuariosSM> listCondonaciones;
    private List<CondonacionTabla2> listCondonaciones2;
    @Wire
    private Column clm_0;
    @Wire
    private Column clm_1;
    @Wire
    private Column clm_2;
    @Wire
    private Column clm_3;
    @Wire
    private Column clm_4;
    @Wire
    private Column clm_5;
    @Wire
    private Column clm_6;
    @Wire
    private Column clm_7;
    @Wire
    private Column clm_8;
    @Wire
    private Column clm_9;
    String modulo;
    @Wire
    Span btn_search;

    private List<UsuariosSM> lCondFinal;
    private String sTextLocal;
    @Wire
    private String sTextLocalResp;
    @Wire
    Textbox txt_filtra;
    public String AreaTrabajo;
    MetodosGenerales MT;
    ListModelList<Reparos> ListReparosCondonacionModel;
    ListModelList<SisconinfoperacionV2> ListOperacionesInformeModel;
    Session session;
    String cuenta;
    UsuarioPermiso permisos;
    ListModelList<DetalleCliente> ListDetOperModel;
    Window window;
    int idCondonacion;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    final UsuariosSMInterfaz cond;
    DetalleOperacionesClientes detoper;
    Session sess;
    private List<UsuariosSM> lCondFilter;

    UsuarioDAO _usu;
    List<UsuariosSM> lRep = new ArrayList<UsuariosSM>();
//lRep= lRep = new ArrayList<UsuariosSM>();

    MvcConfig mmmm = new MvcConfig();
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private List<UsuariosSM> lInforme;
    private ListModelList<UsuariosSM> lMLInforme;
    private UsuariosSM informe;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        cargaPag();

        txt_filtra.setText("");
        listCondonaciones = cond.getUsuariosSM();
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);

        lCondFinal = new ArrayList<UsuariosSM>();
        lCondFinal = listCondonaciones;
        lCondFilter = lCondFinal;
        grd_InformeCond.setModel(bandeCondTerminada);

        session = Sessions.getCurrent();
        sess = Sessions.getCurrent();
        //     lRep = cond.getInfClienV2();
        //muestra = new ListModelList<UsuariosSM>(lRep);
        //  grillainformes.setModel(muestra);
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount(); 
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        eventos();

    }

    public void cargaPag() {

        rdg_tip1.setSelectedItem(rdb_pyme);

        lInforme = new ArrayList<UsuariosSM>();
        informe = new UsuariosSM();
        //  lInforme = informe.list_Informe_Pyme();

        refresh_grid(lInforme);
        // eventos();
    }

    public void refresh_grid(List<UsuariosSM> lICARP) {

        lMLInforme = new ListModelList<UsuariosSM>(lICARP);
        grd_InformeCond.setModel(lMLInforme);
    }


    private void eventos() {


        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });


        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });


    }

    public PerfilSiscore() throws SQLException {

        // this.cond = new CondonacionImpl(mmmm.getDataSourceProduccionMYSQL());
        this.cond = new UsuariosSMImpl(mmmm.getDataSource());
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.modulo = "EfectividadComercial";

    }


    private List<UsuariosSM> filterGrid() {
        List<UsuariosSM> lFilterGrid = new ArrayList<UsuariosSM>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = listCondonaciones;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = listCondonaciones;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (UsuariosSM cT2 : listCondonaciones) {
                        String cadena = cT2.getStrNombre() + ";"
                                + cT2.getStrApellidoP() + ";"
                                + cT2.getStrApellidoM() + ";"
                                + cT2.getStrUsuario() + ";"
                                + cT2.getStrRut() ;

                        if (MT.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
        return lFilterGrid;
    }

    private void cargaGrid(List<UsuariosSM> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);

        grd_InformeCond.setModel(bandeCondTerminada);

    }
    
    
    
    
    
 

            
            
   
    public void PerfilSisCore(final String UsuarioNT) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", UsuarioNT);
       //arguments.put("rut", 15.014544);
        String template = "Mantenedores/Poput/PerfilPorUsuarioSisCore.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_close");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });
        
        
        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    
        public void VerPerfilSiscon(final String UsuarioNT) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", UsuarioNT);
       //arguments.put("rut", 15.014544);
        String template = "Mantenedores/Poput/PerfilPorUsuario.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_close");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });
        
        
        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
