/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Grid;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscore.entidades.AreaTrabajo_1;
import siscon.entidades.GlosaENC;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.AdjuntarAPI;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.AdjuntarInterfaz;
import siscon.entidades.interfaces.ClienteInterfaz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscore.entidades.Sistema_1;
import siscore.entidades.implementaciones.AreaTrabajoImpl;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;

/**
 *
 * @author exesilr
 */
public class PerfilPorUsuarios extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;
    @Wire
    Grid id_gridReparos;
    Window capturawin;
    Window window;
    @Wire
    Window id_poput_reparo;
    Div pag_Infor;
    @Wire
    private Window idWinGarantiasPoput;
    @Wire
    Label lbl_idCond;
    @Wire
    Button tn_ejeReparadaReparar;
    @Wire
    Button btn_closeButton2;

    @Wire
    Combobox cmb_tipoBanca; 
        
    @Wire
    Window win_PerfilXusr;

    final UsuariosSMInterfaz USSMAIMPL;
    @Wire
    Combobox cmb_tipoRechazo;

    @Wire
    Combobox cmb_sistema;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    private AdjuntarENC adjunta;
    private String modulo = "EjePopUpReparos";//cosorio
    private String modulo2 = "AnSacabopView";
    private GlosaENC glosa; //cosorio
    private EventQueue eq;//cosorio
    private float sizeFiles = 0;
    private int id_cond = 0;
    private int rutCli = 0;
    private String rutCliEntero = null;
    private int id_Cli = 0;
    private ClienteInterfaz buscaIdCliInter;
    private final AdjuntarInterfaz ai;
    final CondonacionInterfaz cond;
    private List<AreaTrabajo_1> lTR;

    AreaTrabajoImpl AT;
    private ListModelList<AreaTrabajo_1> lMLTR;
    private List<Comboitem> lItemFull;

    private List<Sistema_1> lTRS;
    private ListModelList<Sistema_1> lMLTRS;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    @Wire
    public String TTTT = "EJESICON";
    MvcConfig mmmm = new MvcConfig();
    ListModelList<Reparos> ListReparosCondonacionModel;

    public String getAreaTrabajo() {
        return AreaTrabajo;
    }

    public void setAreaTrabajo(String AreaTrabajo) {
        this.AreaTrabajo = AreaTrabajo;
    }
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        this.capturawin = (Window) comp;
        //this.window = this.capturawin;
        // pag_Infor = (Div) capturawin.getParent();
        cargaListSistema();
//ScargaListTipRechazos();

    }

    public PerfilPorUsuarios() throws SQLException {
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.AT = new AreaTrabajoImpl(mmmm.getDataSource());
        this.AreaTrabajo = "ANSISCON";
        ai = new AdjuntarAPI();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        USSMAIMPL = new UsuariosSMImpl(mmmm.getDataSource());
    }

    @Listen("onClick=#btn_sigperfil")
    public void AsignaPerfil() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", lbl_idCond.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);

        AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());

        USSMAIMPL.addPerfilSison(lbl_idCond.getValue(), at.getCod());

        // printButton.setParent(window);
        try {
            capturawin.detach();           //win_PerfilXusr
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Listen("onClick=#btn_sigperfilSisCore")
    public void AsignaPerfilSisCore() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", lbl_idCond.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);

        AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());

        USSMAIMPL.addPerfilSisCore(lbl_idCond.getValue(), at.getCod(),"SISCORE_OPERBCI");

        // printButton.setParent(window);
        try {
            capturawin.detach();           //win_PerfilXusr
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void cargaListTipRechazos() {
        lTR = new ArrayList<AreaTrabajo_1>();
        lTR = AT.listAreasTrbajo();
        lMLTR = new ListModelList<AreaTrabajo_1>(lTR);
//        cmb_tipoRechazo.setInplace(true);
        cmb_tipoRechazo.setReadonly(true);
        cmb_tipoRechazo.setAutodrop(true);
        cmb_tipoRechazo.setWidth("100%");
        cmb_tipoRechazo.setClass("input-group-block");
        cmb_tipoRechazo.setPlaceholder("Seleccione Sub Sistema.");
        cmb_tipoRechazo.setStyle("color : black !important; font-weight : bold");

        cmb_tipoRechazo.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                AreaTrabajo_1 tR = (AreaTrabajo_1) data;

                rechazo = tR.getDescripcion();
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        cmb_tipoRechazo.setModel(lMLTR);

        lItemFull = cmb_tipoRechazo.getItems();

    }

    @Listen("onChanging = combobox#cmb_sistema")
    public void changeAutorelleno3(InputEvent event) {

        Messagebox.show("cambio de comobobox VALOR:[" + event.getValue() + "]");
        /// id_autorelleno.setValue(nf.format(this.parsePesoToFloat(event.getValue())).replaceFirst("Ch", ""));

    }

    @Listen("onSelect = #cmb_sistema")
    public void onSelect$cmb_sistema(Event event) {
        cmb_sistema.getSelectedIndex();

        Sistema_1 sis = lMLTRS.get(cmb_sistema.getSelectedIndex());
        /*             for (Perfil insPer : lPerfil) {
                                        if (insPer.getDescripcion().equals(cmb_Perfil.getValue())) {
                                            relPerRel.setId_perfil(insPer.getId_perfil());
                                            break;
                                        }
                                    }*/

 /*   for (final RangoFecha lrf : lRangoFecha) {
            if (lrf.getDesc().equals(cmb_PerFechaRegla.getValue())) {
                txt_DesdeFecha.setValue(Integer.toString(lrf.getRango_ini()));
                txt_HastaFecha.setValue(Integer.toString(lrf.getRango_fin()));
            }
        }*/
        lTR = new ArrayList<AreaTrabajo_1>();
        lTR = AT.listAreasTrbajo(sis.getCodigo());
        lMLTR = new ListModelList<AreaTrabajo_1>(lTR);
//        cmb_tipoRechazo.setInplace(true);
        cmb_tipoRechazo.setReadonly(true);
        cmb_tipoRechazo.setAutodrop(true);
        cmb_tipoRechazo.setWidth("100%");
        cmb_tipoRechazo.setClass("input-group-block");
        cmb_tipoRechazo.setPlaceholder("Seleccione un tipo de rechazo.");
        cmb_tipoRechazo.setStyle("color : black !important; font-weight : bold");

        cmb_tipoRechazo.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                AreaTrabajo_1 tR = (AreaTrabajo_1) data;

                rechazo = tR.getDescripcion() + "(" + tR.getCod() + ")";
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        cmb_tipoRechazo.setModel(lMLTR);

        lItemFull = cmb_tipoRechazo.getItems();

        //  Messagebox.show("cambio de comobobox VALOR:["+event.getName()+"]   INDEX ["+cmb_sistema.getSelectedIndex()+"]Valueee :[["+cmb_sistema.getValue()+"]] SISTEMA :["+sis.getCodigo()+"]");
    }

    private void cargaListSistema() {
        lTRS = new ArrayList<Sistema_1>();
        lTRS = AT.listSistema();
        lMLTRS = new ListModelList<Sistema_1>(lTRS);
//        cmb_tipoRechazo.setInplace(true);
        cmb_sistema.setReadonly(true);
        cmb_sistema.setAutodrop(true);
        cmb_sistema.setWidth("100%");
        cmb_sistema.setClass("input-group-block");
        cmb_sistema.setPlaceholder("Seleccione un tipo de rechazo.");
        cmb_sistema.setStyle("color : black !important; font-weight : bold");

        cmb_sistema.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                Sistema_1 tR = (Sistema_1) data;

                rechazo = tR.getSisDesc() + "(" + tR.getCodigo() + ")";
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        cmb_sistema.setModel(lMLTRS);

        lItemFull = cmb_sistema.getItems();

    }

    public void eventos() {
        id_poput_reparo.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("ReparoPopUp", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", id_poput_reparo, map));

                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
            }

        });

        btn_closeButton2.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("ReparoPopUp", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_closeButton2, map));
                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
                id_poput_reparo.detach();
            }
        });
    }

    public void cargaPag() {
        final Execution exec = Executions.getCurrent();

        if (exec.getArg().get("ListAdjuntoSS") != null) {
            adjunta.setaDet((List<AdjuntarDET>) exec.getArg().get("ListAdjuntoSS"));
        }

        if (exec.getArg().get("id_condonacion") != null) {
            id_cond = (Integer) exec.getArg().get("id_condonacion");
        }
        if (exec.getArg().get("rutclienteInt") != null) {
            rutCli = (Integer) exec.getArg().get("rutclienteInt");
        }
        if (exec.getArg().get("rutcliente") != null) {
            rutCliEntero = (String) exec.getArg().get("rutcliente");
        }

        //buscar id cliente en tabla de condonaciones por la id de condonación.
        id_Cli = buscaIdCliInter.GetClienteCondonado(id_cond);

        adjunta.setFk_idCliente(id_Cli);
        adjunta.setFk_idCondonacion(id_cond);
        ///////////////////////////////////////////////////////////////////////

        eventos();

    }

    public void Adjuntar(Object[] aaa) {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        int id_condResp = 0;
        int id_CliResp = 0;
        String[][] coupleArray = new String[aaa.length][];
        AdjuntarENC adjLocal = new AdjuntarENC();

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        int id_reparo = Integer.parseInt(coupleArray[0][1]);
        id_condResp = adjunta.getFk_idCondonacion();
        id_CliResp = adjunta.getFk_idCliente();

        if (adjunta.getDi_id_reparo() != id_reparo) {
            if (adjunta.getDi_id_reparo() != 0) {
                adjunta = new AdjuntarENC();
            }
        }

        adjunta.setFk_idCliente(id_CliResp);
        adjunta.setFk_idCondonacion(id_condResp);
        adjunta.setDi_id_reparo(id_reparo);

        adjLocal = ai.api(adjunta, 2);

        if (adjLocal != null) {
            adjunta = adjLocal;
        }

        map.put("rutClienteAdjunto", rutCliEntero);
        map.put("ListAdjuntoSS", adjunta.getaDet());
        map.put("modulo", modulo);

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/AdjuntarPoput.zul", null, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void Adjuntar2(Object[] aaa) {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        int id_condResp = 0;
        int id_CliResp = 0;
        String[][] coupleArray = new String[aaa.length][];
        AdjuntarENC adjLocal = new AdjuntarENC();

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        int id_reparo = Integer.parseInt(coupleArray[0][1]);
        id_condResp = adjunta.getFk_idCondonacion();
        id_CliResp = adjunta.getFk_idCliente();

        if (adjunta.getDi_id_reparo() != id_reparo) {
            if (adjunta.getDi_id_reparo() != 0) {
                adjunta = new AdjuntarENC();
            }
        }

        adjunta.setFk_idCliente(id_CliResp);
        adjunta.setFk_idCondonacion(id_condResp);
        adjunta.setDi_id_reparo(id_reparo);

        adjLocal = ai.api(adjunta, 2);

        if (adjLocal != null) {
            adjunta = adjLocal;
        }

        map.put("rutClienteAdjunto", rutCliEntero);
        map.put("ListAdjuntoSS", adjunta.getaDet());
        map.put("modulo", modulo2);

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/AdjuntarPoput.zul", this.capturawin, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void UpdateGridReparos() {

        // _conListDOc = new MvcConfig().documentosJDBC();
        //this._listDOc = _conListDOc.listDocumentoLotePorBarcode(this._lot.lot.getDv_CodBarra()); //id_operacion
        //    _listModelDOc = new ListModelList<Documento>(_listDOc);
        //   grid_Dctos.setModel(_listModelDOc);
        //  Messagebox.show("Updated");
        //   grid_Dctos.setVisible(false);
        //    grid_Dctos.setVisible(true);
        // id_scanCode.setVisible(false);
        //   _divFridDoc.setVisible(false);
        //    _divFridDoc.setVisible(true);
    }

}
