/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.controller;

/**
 *
 * @author exaesan
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Grid;
import sisboj.entidades.Perfiles;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Banca;
import siscore.entidades.AreaTrabajo_1;
import siscon.entidades.GlosaENC;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.AdjuntarAPI;
import siscon.entidades.implementaciones.BancaJDBC;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.AdjuntarInterfaz;
import siscon.entidades.interfaces.ClienteInterfaz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscore.entidades.Sistema_1;
import siscore.entidades.implementaciones.AreaTrabajoImpl;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;

/**
 *
 * @author exesilr
 */
public class PerfilPorUsuariosSiscon extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;
    @Wire
    Grid id_gridReparos;
    Window capturawin;
    Window window;
    @Wire
    Window id_poput_reparo;
    Div pag_Infor;
    @Wire
    private Window idWinGarantiasPoput;
    @Wire
    Label lbl_idCond;
    @Wire
    Button tn_ejeReparadaReparar;
    @Wire
    Button btn_closeButton2;
    
    String perf;
    @Wire
    Label idperfiles;
    @Wire  
    Combobox idperfilesCombo;
    
    @Wire
    Button btn_deletperfil;
    
    @Wire
    Window win_PerfilXusr;
    
    
    final UsuariosSMInterfaz USSMAIMPL;
        @Wire
     Combobox cmb_tipoRechazo;
      @Wire
     Combobox cmb_tipoBanca;   
        
       @Wire  
      Combobox  cmb_sistema;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    private AdjuntarENC adjunta;
    private final String modulo = "EjePopUpReparos";//cosorio
    private final String modulo2 = "AnSacabopView";
    private GlosaENC glosa; //cosorio
    private EventQueue eq;//cosorio
    private final float sizeFiles = 0;
    private final int id_cond = 0;
    private final int rutCli = 0;
    private final String rutCliEntero = null;
    private final int id_Cli = 0;
    private ClienteInterfaz buscaIdCliInter;
    private final AdjuntarInterfaz ai;
    final CondonacionInterfaz cond;
    
    private List<AreaTrabajo_1> lTR;
    
    private List<Perfiles> lTRP;
    
    private String perfilXBanca;
    private List<String> c2TR;
    private ListModelList<String> M2LTR;
    
    AreaTrabajoImpl AT;
    private ListModelList<AreaTrabajo_1> lMLTR;
    private ListModelList<Perfiles> lMLTRP;
    private ListModelList lCmb_Perfil;
    BancaJDBC _banca;
     private List<Comboitem> lItemFull;
     
     private List<Banca> lPerfil = new ArrayList<Banca>();
     
     private List<Sistema_1> lTRS;
     private ListModelList<Sistema_1> lMLTRS;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    @Wire
    public String TTTT = "EJESICON";
    MvcConfig mmmm = new MvcConfig();
    ListModelList<Reparos> ListReparosCondonacionModel;

    public String getAreaTrabajo() {
        return AreaTrabajo;
    }

    public void setAreaTrabajo(String AreaTrabajo) {
        this.AreaTrabajo = AreaTrabajo;
    }
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        this.capturawin = (Window) comp;

       cargaListSistema();
       cargaListSistemaPerfiles();
       cargaBanca();

    }

    public PerfilPorUsuariosSiscon() throws SQLException {
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.AT= new AreaTrabajoImpl(mmmm.getDataSource());
        this.AreaTrabajo = "ANSISCON";
        ai = new AdjuntarAPI();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        USSMAIMPL=new UsuariosSMImpl(mmmm.getDataSource());
        this._banca=new BancaJDBC(mmmm.getDataSource());
    }

    @Listen("onClick=#btn_sigperfil")
    public void AsignaPerfil() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", lbl_idCond.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);

        //Perfiles at = lMLTRP.get(cmb_tipoRechazo.getSelectedIndex());

       // USSMAIMPL.addPerfilSisonPerf(lbl_idCond.getValue(), at.getDv_CodPerfil());
       
       AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());

        USSMAIMPL.addPerfilSisonPerf(lbl_idCond.getValue(), at.getCod());

        // printButton.setParent(window); 
        try {
            capturawin.detach();           //win_PerfilXusr
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    public void irDetalle(int id_det) throws SQLException {

        System.out.println("IR DETALLE --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }
    
    
    public void cargaListSistemaPerfiles(){
        
        System.out.println("CargalistaSistemaPerfiles**");
        //Map<String,String> listPerf = USSMAIMPL.listPerfilSison(lbl_idCond.getValue(),"");
        //String listPerf = USSMAIMPL.listPerfilSison(lbl_idCond.getValue(),"");
        List<String> inf_perf = USSMAIMPL.listPerfilSison(lbl_idCond.getValue(),"");
        //perf ="-"+ listPerf.get("perfil1") +" \n "+"-"+listPerf.get("perfil2");
        //System.out.println( "PerfilGET-> "+listPerf.get("perfil1")+" --- "+listPerf.get("perfil2"));
        //idperfiles.setValue(listPerf);
        
        //idperfilesCombo
//        idperfilesCombo.setReadonly(true);
//        idperfilesCombo.setAutodrop(true);
//        idperfilesCombo.setWidth("100%");
//        idperfilesCombo.setClass("input-group-block");
//        idperfilesCombo.setPlaceholder("Perfiles Asignados");
//        idperfilesCombo.setStyle("color : black !important; font-weight : bold");
        
        c2TR = inf_perf;
        String per="";
        for(String camp:c2TR ){
             System.out.println(camp);
             
             if(per !=""){
                 per +=","+camp;
             }else{
                 per +=camp;
             }
        }
        perfilXBanca = per;
        System.out.println("perfilXBanca-> "+perfilXBanca);
        idperfiles.setValue(per);
        M2LTR = new ListModelList<String>(c2TR);
//        idperfilesCombo.setModel(M2LTR);
    }
    
    
    @Listen("onClick=#btn_deletperfil")
    public void EliminaPerfil() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", lbl_idCond.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);

        //AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());
       // Perfiles at = lMLTRP.get(cmb_tipoRechazo.getSelectedIndex());
        AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());
        //USSMAIMPL.addPerfilSison(lbl_idCond.getValue(), at.getCod()); eliminaPerfilSison
        String responseDelete = USSMAIMPL.eliminaPerfilSison(lbl_idCond.getValue(), at.getCod());
//        System.out.println("Var1-> "+lbl_idCond.getValue());
//        System.out.println("Var2-> "+at.getDv_CodPerfil());

        System.out.println("***responseDelete*** -> "+responseDelete);
        
        // printButton.setParent(window);
        try {
            capturawin.detach();           //win_PerfilXusr
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    @Listen("onClick=#btn_uptperfil")
    public void UpdatePerfil() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();
System.out.println("llego update --> ");
        arguments.put("id_condonacion", lbl_idCond.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);

        //AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());
       // Perfiles at = lMLTRP.get(cmb_tipoRechazo.getSelectedIndex());
        AreaTrabajo_1 at = lMLTR.get(cmb_tipoRechazo.getSelectedIndex());
        //USSMAIMPL.addPerfilSison(lbl_idCond.getValue(), at.getCod()); eliminaPerfilSison
        String responseDelete = USSMAIMPL.updatePerfilSison(lbl_idCond.getValue(), at.getCod());
//        System.out.println("Var1-> "+lbl_idCond.getValue());
//        System.out.println("Var2-> "+at.getDv_CodPerfil());

       System.out.println("***responseDelete*** -> "+responseDelete);
        
        // printButton.setParent(window);
        try {
            capturawin.detach();           //win_PerfilXusr
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

 
    @Listen("onSelect = #cmb_sistema")
    public void onSelect$cmb_sistema(Event event) {
        cmb_sistema.getSelectedIndex();

     Sistema_1 sis= lMLTRS.get(cmb_sistema.getSelectedIndex());
     System.out.println("SIS -> "+sis);
     System.out.println("sis.getCodigo()-> "+sis.getCodigo());
       
        //lTRP = new ArrayList<Perfiles>();
        lTR = new ArrayList<AreaTrabajo_1>();
        lTR = AT.listAreasTrbajo(sis.getCodigo());
        //lTR = AT.listAreasTrbajo(sis.getCodigo());
        //lTRP = AT.listPerfiles();
        lMLTR = new ListModelList<AreaTrabajo_1>(lTR);
        //lMLTRP = new ListModelList<Perfiles>(lTRP);
//        cmb_tipoRechazo.setInplace(true);
        cmb_tipoRechazo.setReadonly(true);
        cmb_tipoRechazo.setAutodrop(true);
        cmb_tipoRechazo.setWidth("100%");
        cmb_tipoRechazo.setClass("input-group-block");
        cmb_tipoRechazo.setPlaceholder("Seleccione Perfil.");
        cmb_tipoRechazo.setStyle("color : black !important; font-weight : bold");

        cmb_tipoRechazo.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                //Perfiles tR = (Perfiles) data;
                AreaTrabajo_1 tR = (AreaTrabajo_1) data;

                //rechazo = tR.getDescripcion() +"("+tR.getDv_CodPerfil()+")";
                rechazo = tR.getDescripcion() +"("+tR.getCod()+")";
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        //cmb_tipoRechazo.setModel(lMLTRP);
         cmb_tipoRechazo.setModel(lMLTR);

        lItemFull = cmb_tipoRechazo.getItems();
        
       //  Messagebox.show("cambio de comobobox VALOR:["+event.getName()+"]   INDEX ["+cmb_sistema.getSelectedIndex()+"]Valueee :[["+cmb_sistema.getValue()+"]] SISTEMA :["+sis.getCodigo()+"]");

    }           
    
    
        private void cargaListSistema() {
        lTRS = new ArrayList<Sistema_1>();
        lTRS = AT.listSistema();
        lMLTRS = new ListModelList<Sistema_1>(lTRS);
//        cmb_tipoRechazo.setInplace(true);
        cmb_sistema.setReadonly(true);
        cmb_sistema.setAutodrop(true);
        cmb_sistema.setWidth("100%");
        cmb_sistema.setClass("input-group-block");
        cmb_sistema.setPlaceholder("Seleccione Sitema.");
        cmb_sistema.setStyle("color : black !important; font-weight : bold");

        cmb_sistema.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                Sistema_1 tR = (Sistema_1) data;

                rechazo = tR.getSisDesc()+"("+tR.getCodigo()+")";
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        cmb_sistema.setModel(lMLTRS);

        lItemFull = cmb_sistema.getItems();

    } 
        

public void cargaBanca() {

        try {
            lPerfil.clear();
            lPerfil = _banca.getBancaXPerfil(perfilXBanca);
            lCmb_Perfil = new ListModelList<Banca>(lPerfil);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_tipoBanca.setModel(lCmb_Perfil);
            cmb_tipoBanca.applyProperties();
            cmb_tipoBanca.setVisible(false);
            cmb_tipoBanca.setVisible(true);
            cmb_tipoBanca.setPlaceholder("Bancas Asignadas");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los Bacnas. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }





}
