/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades;

import java.io.Serializable;

/**
 *
 * @author esilves
 */
public class AreaTrabajo_1 implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private String cod;
    private String descripcion;

    private Integer fkDiIdFilial;

    public AreaTrabajo_1() {
    }

    public AreaTrabajo_1(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getFkDiIdFilial() {
        return fkDiIdFilial;
    }

    public void setFkDiIdFilial(int fkDiIdFilial) {
        this.fkDiIdFilial = fkDiIdFilial;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreaTrabajo_1)) {
            return false;
        }
        AreaTrabajo_1 other = (AreaTrabajo_1) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siscon.entidades.AreaTrabajo_1[ id=" + id + " ]";
    }
    
}
