/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author esilves
 */

public class Sistema_1 implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String sisDesc;

    private Date registrado;

    private String codigo;

    public Sistema_1() {
    }

    public Sistema_1(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSisDesc() {
        return sisDesc;
    }

    public void setSisDesc(String sisDesc) {
        this.sisDesc = sisDesc;
    }

    public Date getRegistrado() {
        return registrado;
    }

    public void setRegistrado(Date registrado) {
        this.registrado = registrado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sistema_1)) {
            return false;
        }
        Sistema_1 other = (Sistema_1) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siscore.entidades.Sistema_1[ id=" + id + " ]";
    }
    
}
