/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades;

public class Sistema {

    
    

    public int id;
    public String descripcion;
    public String registrado;
    public String codigo;
    
    
    
    public Sistema(int id, String descripcion, String registrado, String codigo) {
        this.id = id;
        this.descripcion = descripcion;
        this.registrado = registrado;
        this.codigo = codigo;
    }



    public Sistema() {
    }

    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRegistrado() {
        return registrado;
    }

    public void setRegistrado(String registrado) {
        this.registrado = registrado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
