/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades.implementaciones;

import siscon.entidades.implementaciones.*;
import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import siscore.entidades.AreaTrabajo_1;
import siscon.entidades.Condonacion;
import siscon.entidades.DetalleCliente;
import siscore.entidades.Sistema_1;
import siscore.entidades.interfaces.AreaTrabajoIntfz;

/**
 *
 * @author exesilr
 */
public class AreaTrabajoImpl implements AreaTrabajoIntfz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<DetalleCliente> ListaOperaciones;
    private JdbcTemplate jdbcTemplateObject;
    private int id_con = 0;
    private int estado_destino = 0;
    private String[] nombs;
    private EstadosJDBC _estados;
    private MvcConfig mmmm = new MvcConfig();
    private Condonacion CurrentCondonacion;
    private String insertSqlDetalleCondonacion,
            insertSqlEstado_Condonacion,
            insertSqlOperaciones,
            insertSqlTrakingCondonacionOperacion,
            updateSql,
            insertSqlCondonacion_retail;

    public AreaTrabajoImpl(DataSource dataSource) throws SQLException {

        this.dataSource = dataSource;
        //   this.gi = new GlosaAPI();//cosorio
        //   this.ai = new AdjuntarAPI();
        _estados = new EstadosJDBC(mmmm.getDataSource());
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<AreaTrabajo_1> listAreasTrbajo() {
        List<AreaTrabajo_1> lCR = new ArrayList<AreaTrabajo_1>();
        jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        final String sql = "select * from AreaTrabajo";

        try {
            lCR = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(AreaTrabajo_1.class));
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

    public List<AreaTrabajo_1> listAreasTrbajo(String CODIGO) {
        List<AreaTrabajo_1> lCR = new ArrayList<AreaTrabajo_1>();
        jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        final String sql = "select * from AreaTrabajo at\n"
                + "   inner join sistema_areatrabajo sisat on sisat.fk_id_areatrabajo=at.id\n"
                + "   inner join sistema sis  on sis.id=sisat.fk_id_sistema\n"
                + "   where sis.codigo='" + CODIGO + "'    ";

        try {
            lCR = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(AreaTrabajo_1.class));
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

    public List<Sistema_1> listSistema() {
        List<Sistema_1> lCR = new ArrayList<Sistema_1>();
        jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        final String sql = "select * from sistema";

        try {
            lCR = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(Sistema_1.class));
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

    public List<Sistema_1> listSistema(String CODIGO) {
        List<Sistema_1> lCR = new ArrayList<Sistema_1>();
        jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        final String sql = "select * from sistema WHERE codigo='" + CODIGO + "'";

        try {
            lCR = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(Sistema_1.class));
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

}
