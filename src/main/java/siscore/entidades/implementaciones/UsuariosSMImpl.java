/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades.implementaciones;

import siscon.entidades.implementaciones.*;
import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.AccesosEjecutivo;
import siscon.entidades.Condonacion;
import siscon.entidades.DetalleCliente;
import siscon.entidades.ReglaTiempoGrilla;
import siscon.entidades.TrackingRowTomadaUsr;
import siscon.entidades.UsuariosSM;
import siscon.entidades.usuario;
import static siscore.comunes.LogController.SisCorelog;
import siscore.entidades.interfaces.UsuariosSMInterfaz;

/**
 *
 * @author exesilr
 */
public class UsuariosSMImpl implements UsuariosSMInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<DetalleCliente> ListaOperaciones;
    private JdbcTemplate jdbcTemplateObject;
    private int id_con = 0;
    private int estado_destino = 0;
    private String[] nombs;
    private EstadosJDBC _estados;
    private MvcConfig mmmm = new MvcConfig();
    private Condonacion CurrentCondonacion;
    private String insertSqlDetalleCondonacion,
            insertSqlEstado_Condonacion,
            insertSqlOperaciones,
            insertSqlTrakingCondonacionOperacion,
            updateSql,
            insertSqlCondonacion_retail;

    public UsuariosSMImpl(DataSource dataSource) throws SQLException {

        this.dataSource = dataSource;
        //   this.gi = new GlosaAPI();//cosorio
        //   this.ai = new AdjuntarAPI();
        _estados = new EstadosJDBC(mmmm.getDataSource());
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<AccesosEjecutivo> getInfEjecutivoAccessos() {

        String sql = "select * from (\n"
                + "select HH.cuenta 'ejecutivo_condona',max(HH.fecha) 'fec',count(HH.cuenta)numero_accesos from(\n"
                + "select ht.cuenta,ht.id,count(ht.cuenta)numerointentos,max(ht.registrado)fecha from http_session  ht\n"
                + "inner join http_session_acciones hta on hta.http_sessionID=ht.id\n"
                + "\n"
                + "group by ht.cuenta,ht.id \n"
                + "\n"
                + ") as HH\n"
                + "\n"
                + "group by HH.cuenta\n"
                + "\n"
                + ") as GG\n"
                + "order by GG.fec desc";
        List<AccesosEjecutivo> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(AccesosEjecutivo.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }

    public List<UsuariosSM> getUsuariosSM() {

        String sql = "SELECT isnull([idUsuario],0) idUsuario\n"
                + "      ,isnull([idEstado],0) idEstado\n"
                + "      ,isnull([idTipoUsuario],0) idTipoUsuario\n"
                + "      ,isnull([strNombre],'null') strNombre\n"
                + "      ,isnull([strApellidoP],'null') strApellidoP\n"
                + "      ,isnull([strApellidoM],'null') strApellidoM\n"
                + "      ,isnull([strUsuario],'null') strUsuario\n"
                + "      ,isnull([strRut],0) strRut\n"
                + "      ,isnull([strDv],0) strDv\n"
                + "      ,isnull([strSexo],'null') strSexo\n"
                + "      ,isnull([strContrasena],'null') strContrasena \n"
                + "      ,isnull([dateFechaNacimiento],'1900-01-01 00:00:00.000') dateFechaNacimiento\n"
                + "      ,isnull([dateCreado],'1900-01-01 00:00:00.000') dateCreado\n"
                + "      ,isnull([strCedente],'null') strCedente\n"
                + "      ,isnull([strCargo],'null')  strCargo\n"
                + "      ,isnull([strPerfil],'null')  strPerfil\n"
                + "      ,isnull([strRegional],'null')  strRegional\n"
                + "      ,isnull([fld_nom_asi],'null')   fld_nom_asi\n"
                + "      ,isnull([fld_por_vig],0)    fld_por_vig\n"
                + "      ,isnull([fld_lead_sn],'n')   fld_lead_sn\n"
                + "      ,isnull([fld_vig_001],'n')  fld_vig_001\n"
                + "      ,isnull([fld_red_com_sn],'n')  fld_red_com_sn\n"
                + "      ,isnull([fld_listado],'n')  fld_listado\n"
                + "      ,isnull([fld_lis_vel],0)  fld_lis_vel\n"
                + "      ,isnull([fld_tip_usu_cas],'null')  fld_tip_usu_cas\n"
                + "      ,isnull([fld_vis],'null') fld_vis\n"
                + "      ,isnull([fld_nov_suc],0)  fld_nov_suc\n"
                + "      ,isnull([fld_ID_Padre],0)  fld_ID_Padre\n"
                + "      ,isnull([fld_ID_Padre2],0)  fld_ID_Padre2\n"
                + "      ,isnull([Fecha_update],'1900-01-01 00:00:00.000')   Fecha_update\n"
                + "      ,isnull([ID_padre_informe],0)  ID_padre_informe\n"
                + "      ,isnull([fld_rut_2dig_des],0) fld_rut_2dig_des\n"
                + "      ,isnull([fld_rut_2dig_has],0) fld_rut_2dig_has\n"
                + "      ,isnull([ID_INFORMES_PYME],'null') ID_INFORMES_PYME\n"
                + "      ,isnull([COD_EJEC_NOV],0) COD_EJEC_NOV\n"
                + "      ,isnull([INF_ONLINE],0) INF_ONLINE\n"
                + "      ,isnull([VIS_COL],0) VIS_COL\n"
                + "      ,isnull([USER_EXT],0) USER_EXT\n"
                + "      ,isnull([FECHA_CADUCIDAD],'1900-01-01 00:00:00.000') FECHA_CADUCIDAD\n"
                + "      ,isnull([FECHA_ULTIMO_ACCESO],'1900-01-01 00:00:00.000') FECHA_ULTIMO_ACCESO\n"
                + "      ,isnull([ULTIMA_IP_REGISTRADA],'null')  ULTIMA_IP_REGISTRADA\n"
                + "      ,isnull([URL_ACCESO_SC],'null')  URL_ACCESO_SC\n"
                + "      ,isnull([INTENTOS_INGRESO],0) INTENTOS_INGRESO\n"
                + "      ,isnull([TEAM_RECL],0) TEAM_RECL\n"
                + "      ,isnull([CAMBIA_PASSWORD],0) CAMBIA_PASSWORD\n"
                + "      ,isnull([DOMINIO],'null') DOMINIO\n"
                + "      ,isnull([OLD_USER],'null') OLD_USER\n"
                + "      ,isnull([VIS_SIM],0)  VIS_SIM\n"
                + "      ,isnull([POOL],0) POOL\n"
                + "      ,isnull([fld_ofi_bci],0) fld_ofi_bci\n"
                + "      ,isnull([ACEPTA_SIM],0) ACEPTA_SIM\n"
                + "      ,isnull([FYP],0) FYP\n"
                + "      ,isnull([OLD_IDTIPOUSUARIO],0) OLD_IDTIPOUSUARIO\n"
                + "      ,isnull([fld_user_creacion],'null') fld_user_creacion\n"
                + "      ,isnull([VIS_PR],0) VIS_PR\n"
                + "      ,isnull([fecha_desactiva],'1900-01-01 00:00:00.000') fecha_desactiva\n"
                + "      ,(case when (select count(*) from usuario usuu where usuu.alias=strUsuario)=0 then 'NO'else 'SI' end)  'issiscon'\n"
                + "      FROM [In_cbza].[dbo].[db_webnza_usuario] us"+
                "      INNER JOIN [in_cbza].[dbo].[TBL_BASE_DATOS_COLABORADORES] col ON col.Login_Name = us.strUsuario";
                
                
               
        
        
        List<UsuariosSM> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(UsuariosSM.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }

    //// SP para dar perfil y accesso a SIcORE   
          
     public String  addPerfilSison(String alias,String perfil) {
          this.jdbcCall =  new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_homologa_usuarios_app");
         //sss="METROPOLITANA CORDILLERA";
        // alias="esilves";
        // perfil="ejesiscon";
         int opcion =4; //4cREAION 1 vista
         
         
         //String 
         
         
         
      SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario",alias).addValue("perfil",perfil).addValue("col", opcion);
      Map<String, Object> out = jdbcCall.execute(in);
     // CategoryModel model;
      
  //    model = new DefaultCategoryModel();
      //private static final CategoryModel model;
      System.out.print("####### ---- aqui imprimimos el res de output -------#######");
      for (String keys : out.keySet())
{
   System.out.println(keys);
}
      
      ArrayList resultList = (ArrayList) out.get( "#result-set-1" );

Map resultlMap = (Map) resultList.get( 0 );

for (int nn=0;nn<resultList.size();nn++)
{
Map resultlMapx = (Map) resultList.get( nn );
//System.out.print("####### i:["+nn+"]----EJECUTIVO["+((String) resultlMapx.get("FLD_NOM")).toString()+"] fld_Cumpl_Recupero :["+((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString()+"]  fld_cumpl_90_vig["+((Double) resultlMapx.get("fld_cumpl_90_vig")).toString()+"]  fld_cumpl_180_vig["+((Double) resultlMapx.get("fld_cumpl_180_vig")).toString()+"] fld_por_cpl_hip["+((Double) resultlMapx.get("fld_por_cpl_hip")).toString()+"]-------#######");
}
//Double fld_Cumpl_Recupero = (Double) resultlMap.get("fld_Cumpl_Recupero");
//Double fld_cumpl_90_vig = (Double) resultlMap.get("fld_cumpl_90_vig");
//Double fld_cumpl_180_vig = (Double) resultlMap.get("fld_cumpl_180_vig");
//Double fld_por_cpl_hip = (Double) resultlMap.get("fld_por_cpl_hip");        
        
     //System.out.print("####### ---- fld_Cumpl_Recupero :["+fld_Cumpl_Recupero.toString()+"] fld_cumpl_90_vig["+fld_cumpl_90_vig.toString()+"]  fld_cumpl_180_vig["+fld_cumpl_180_vig.toString()+"] fld_por_cpl_hip["+fld_por_cpl_hip.toString()+"]-------#######");  
   //   System.out.print("####### ---- aqui imorimimos Prueba de Model[[["+model.getCategories().toString()+"]]]el ser de output FIN-------#######");
     // System.out.print("Este es el contenido del SP Ejecutado["+(String) out.get("out_fld_Cumpl_Recupero")+"]");
     // Student student = new Student();
     // student.setId(id);
     // student.setName((String) out.get("out_name"));
      //student.setAge((Integer) out.get("out_age"));

      return "BIEN";
   }   
    //// SP para dar perfil y accesso a SIcORE   
    public String addPerfilSisCore(String alias, String areatrabajo,String perfil) {
         Map<String, Object> out = null;
        this.jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_siscore_homologa_usuarios_app");
        int opcion = 4; //4cREAION 1 vista
        //String 
        SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario", alias).addValue("perfil", areatrabajo).addValue("areatrabajo", perfil).addValue("col", opcion);
        
        
        try {
        
        out= jdbcCall.execute(in);
                } catch (DataAccessException ex) {

            SisCorelog("ERROR--- :[199siscoreUsuarioIMPL][:" + ex.getMessage()+"]");

        }
        
        System.out.print("####### ---- aqui imprimimos el res de output -------#######");
        for (String keys : out.keySet()) {
            System.out.println(keys);
        }

        ArrayList resultList = (ArrayList) out.get("#result-set-1");

        Map resultlMap = (Map) resultList.get(0);

        for (int nn = 0; nn < resultList.size(); nn++) {
            Map resultlMapx = (Map) resultList.get(nn);

        }

        return "BIEN";
    }

    public List<UsuariosSM> getUsuariosSBCI() {

        String sql = "SELECT isnull([idUsuario],0) idUsuario\n"
                + "      ,isnull([idEstado],0) idEstado\n"
                + "      ,isnull([idTipoUsuario],0) idTipoUsuario\n"
                + "      ,isnull([strNombre],'null') strNombre\n"
                + "      ,isnull([strApellidoP],'null') strApellidoP\n"
                + "      ,isnull([strApellidoM],'null') strApellidoM\n"
                + "      ,isnull([strUsuario],'null') strUsuario\n"
                + "      ,isnull([strRut],0) strRut\n"
                + "      ,isnull([strDv],0) strDv\n"
                + "      ,isnull([strSexo],'null') strSexo\n"
                + "      ,isnull([strContrasena],'null') strContrasena \n"
                + "      ,isnull([dateFechaNacimiento],'1900-01-01 00:00:00.000') dateFechaNacimiento\n"
                + "      ,isnull([dateCreado],'1900-01-01 00:00:00.000') dateCreado\n"
                + "      ,isnull([strCedente],'null') strCedente\n"
                + "      ,isnull([strCargo],'null')  strCargo\n"
                + "      ,isnull([strPerfil],'null')  strPerfil\n"
                + "      ,isnull([strRegional],'null')  strRegional\n"
                + "      ,isnull([fld_nom_asi],'null')   fld_nom_asi\n"
                + "      ,isnull([fld_por_vig],0)    fld_por_vig\n"
                + "      ,isnull([fld_lead_sn],'n')   fld_lead_sn\n"
                + "      ,isnull([fld_vig_001],'n')  fld_vig_001\n"
                + "      ,isnull([fld_red_com_sn],'n')  fld_red_com_sn\n"
                + "      ,isnull([fld_listado],'n')  fld_listado\n"
                + "      ,isnull([fld_lis_vel],0)  fld_lis_vel\n"
                + "      ,isnull([fld_tip_usu_cas],'null')  fld_tip_usu_cas\n"
                + "      ,isnull([fld_vis],'null') fld_vis\n"
                + "      ,isnull([fld_nov_suc],0)  fld_nov_suc\n"
                + "      ,isnull([fld_ID_Padre],0)  fld_ID_Padre\n"
                + "      ,isnull([fld_ID_Padre2],0)  fld_ID_Padre2\n"
                + "      ,isnull([Fecha_update],'1900-01-01 00:00:00.000')   Fecha_update\n"
                + "      ,isnull([ID_padre_informe],0)  ID_padre_informe\n"
                + "      ,isnull([fld_rut_2dig_des],0) fld_rut_2dig_des\n"
                + "      ,isnull([fld_rut_2dig_has],0) fld_rut_2dig_has\n"
                + "      ,isnull([ID_INFORMES_PYME],'null') ID_INFORMES_PYME\n"
                + "      ,isnull([COD_EJEC_NOV],0) COD_EJEC_NOV\n"
                + "      ,isnull([INF_ONLINE],0) INF_ONLINE\n"
                + "      ,isnull([VIS_COL],0) VIS_COL\n"
                + "      ,isnull([USER_EXT],0) USER_EXT\n"
                + "      ,isnull([FECHA_CADUCIDAD],'1900-01-01 00:00:00.000') FECHA_CADUCIDAD\n"
                + "      ,isnull([FECHA_ULTIMO_ACCESO],'1900-01-01 00:00:00.000') FECHA_ULTIMO_ACCESO\n"
                + "      ,isnull([ULTIMA_IP_REGISTRADA],'null')  ULTIMA_IP_REGISTRADA\n"
                + "      ,isnull([URL_ACCESO_SC],'null')  URL_ACCESO_SC\n"
                + "      ,isnull([INTENTOS_INGRESO],0) INTENTOS_INGRESO\n"
                + "      ,isnull([TEAM_RECL],0) TEAM_RECL\n"
                + "      ,isnull([CAMBIA_PASSWORD],0) CAMBIA_PASSWORD\n"
                + "      ,isnull([DOMINIO],'null') DOMINIO\n"
                + "      ,isnull([OLD_USER],'null') OLD_USER\n"
                + "      ,isnull([VIS_SIM],0)  VIS_SIM\n"
                + "      ,isnull([POOL],0) POOL\n"
                + "      ,isnull([fld_ofi_bci],0) fld_ofi_bci\n"
                + "      ,isnull([ACEPTA_SIM],0) ACEPTA_SIM\n"
                + "      ,isnull([FYP],0) FYP\n"
                + "      ,isnull([OLD_IDTIPOUSUARIO],0) OLD_IDTIPOUSUARIO\n"
                + "      ,isnull([fld_user_creacion],'null') fld_user_creacion\n"
                + "      ,isnull([VIS_PR],0) VIS_PR\n"
                + "      ,isnull([fecha_desactiva],'1900-01-01 00:00:00.000') fecha_desactiva\n"
                + "      ,(case when (select count(*) from usuario usuu where usuu.alias=strUsuario)=0 then 'NO'else 'SI' end)  'issiscon'\n"
                + "      FROM [In_cbza].[dbo].[db_webnza_usuario]";
        List<UsuariosSM> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(UsuariosSM.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
        
    public List<TrackingRowTomadaUsr> getTrackingRow(int usuarioTRow) {
     System.out.println("getTrackingRow--.> "+usuarioTRow);
          String sql = "SELECT * from tracking_event  where idRow = "+usuarioTRow+"";
       List<TrackingRowTomadaUsr> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(TrackingRowTomadaUsr.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
         
         
         
         
         return inf_clie;
     }
    
     public List<TrackingRowTomadaUsr> getTrackingRow() {
     
          String sql = "SELECT * from tracking_event";
       List<TrackingRowTomadaUsr> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(TrackingRowTomadaUsr.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
         
         
         
         
         return inf_clie;
     }
    
    public int getTimeRow() {
        // int id=0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select tiempo from tracking_event_grilla_time";
        int tiempo = 0;
        try {
            tiempo = jdbcTemplate.queryForInt(sql);

        } catch (DataAccessException ex) {

            System.out.print("####### ---- ERROR de EJECUCION de Query///id tiempito/// ERR:[" + ex.getMessage() + "]----#######");

        }
        System.out.println("#------------@@@@@@@@@@@@@@@@@ tiempito [" + tiempo + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        return tiempo;

    }
    
    public boolean insertFlagRow(final TrackingRowTomadaUsr trflg,int getTime) {
        final String insertDetSol = "INSERT INTO [dbo].[tracking_event]\n"
                + "           ([id_colaborador]\n"
                + "           ,[ruta]\n"
                + "           ,[bandeja]\n"
                + "           ,[idRow]\n"
                + "           ,[estado]\n"
                + "           ,[fecha_evento]\n"
                + "           ,[fecha_evento_final])\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,getdate() \n"
                + "           ,DATEADD(minute,"+getTime+",Getdate()))\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"id_tracking"});
                    ps.setInt(1, trflg.getId_colaborador());
                    ps.setString(2, trflg.getRuta());
                    ps.setString(3, trflg.getBandeja());
                    ps.setInt(4, trflg.getIdRow());
                    ps.setInt(5, trflg.getEstado());
                    //ps.setDate(6,trflg.getFecha_evento());
                   
                    
                    
                    return ps;
                }
            }, key);

            /*insertDetSol,new Object[]{    detSol.getDi_fk_IdSolicitud()
                                                    ,detSol.getDi_fk_IdUbi()
                                                    ,detSol.getDi_fk_IdDetHito()
                                                });*/
            return true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        }

    }
    
    public List<usuario> getNomColabById(int colabId){
     
          String SQL = "select * FROM usuario where id_usuario='" + colabId + "'";
       List<usuario> tempusr = jdbcTemplateObject.query(SQL, new BeanPropertyRowMapper(usuario.class));
        return tempusr;
     }
    
    public int getIdColabByNom(String colabnom){
     
       
       jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select id_usuario FROM usuario where alias='" + colabnom + "'";
        int idCOL = 0;
        try {
            idCOL = jdbcTemplate.queryForInt(sql);

        } catch (DataAccessException ex) {

            System.out.print("####### ---- ERROR de EJECUCION de Query///getIdColabByNom/// ERR:[" + ex.getMessage() + "]----#######");

        }
        System.out.println("#------------@@@@@@@@@@@@@@@@@ getIdColabByNom [" + idCOL + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        return idCOL;
     }
    
   public boolean updateEstadoRow(int usuarioTRow,int estado) {
         
        try {
            String SQL = "update tracking_event set estado=" + estado + " where idRow= " + usuarioTRow;
            System.out.println("updateEstadoRow SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
    
    public boolean updateFlagRow(int usuarioTRow,int estado,int getTime) {
         
        try {
            String SQL = "update tracking_event set estado=" + estado + ",fecha_evento = getdate(),fecha_evento_final = DATEADD(minute,"+getTime+",Getdate()) where idRow= " + usuarioTRow;
            System.out.println("updateSolicitud SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
    
    public boolean updateFlagRow2(int usuarioTRow,int estado,int getTime,int idcola) {
         
        try {
            String SQL = "update tracking_event set estado=" + estado + ",fecha_evento = getdate(),fecha_evento_final = DATEADD(minute,"+getTime+",Getdate()),id_colaborador=" + idcola + " where idRow= " + usuarioTRow;
            System.out.println("updateSolicitud SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
    
    public List<ReglaTiempoGrilla> getReglaGrilla() {

        String sql = "select * from tracking_event_grilla_time";
        List<ReglaTiempoGrilla> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(ReglaTiempoGrilla.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
    
    public boolean updateTimeRow(int usuarioTRow,int tiempo) {
        try {
            String SQL = "update tracking_event_grilla_time set tiempo=" + tiempo + ",fecha_update = getdate(),id_colaborador =  "+ usuarioTRow;
            System.out.println("updateSolicitud SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
    
    public String  addPerfilSisonPerf(String alias,String perfil) {
        System.out.println("LLEGA addPerfilSisonPerf");
          this.jdbcCall =  new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_homologa_usuarios_app");
         //sss="METROPOLITANA CORDILLERA";
        // alias="esilves";
        // perfil="ejesiscon";
         int opcion =11; //4cREAION 1 vista
         
         
         //String  
         
         
         
      SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario",alias).addValue("perfil",perfil).addValue("col", opcion);
      Map<String, Object> out = jdbcCall.execute(in);
     // CategoryModel model;
      
  //    model = new DefaultCategoryModel();
      //private static final CategoryModel model;
      System.out.print("####### ---- aqui imprimimos el res de output -------#######");
      for (String keys : out.keySet())
{
   System.out.println(keys);
}
      
      ArrayList resultList = (ArrayList) out.get( "#result-set-1" );

Map resultlMap = (Map) resultList.get( 0 );

for (int nn=0;nn<resultList.size();nn++)
{
Map resultlMapx = (Map) resultList.get( nn );
//System.out.print("####### i:["+nn+"]----EJECUTIVO["+((String) resultlMapx.get("FLD_NOM")).toString()+"] fld_Cumpl_Recupero :["+((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString()+"]  fld_cumpl_90_vig["+((Double) resultlMapx.get("fld_cumpl_90_vig")).toString()+"]  fld_cumpl_180_vig["+((Double) resultlMapx.get("fld_cumpl_180_vig")).toString()+"] fld_por_cpl_hip["+((Double) resultlMapx.get("fld_por_cpl_hip")).toString()+"]-------#######");
}
//Double fld_Cumpl_Recupero = (Double) resultlMap.get("fld_Cumpl_Recupero");
//Double fld_cumpl_90_vig = (Double) resultlMap.get("fld_cumpl_90_vig");
//Double fld_cumpl_180_vig = (Double) resultlMap.get("fld_cumpl_180_vig");
//Double fld_por_cpl_hip = (Double) resultlMap.get("fld_por_cpl_hip");        
        
     //System.out.print("####### ---- fld_Cumpl_Recupero :["+fld_Cumpl_Recupero.toString()+"] fld_cumpl_90_vig["+fld_cumpl_90_vig.toString()+"]  fld_cumpl_180_vig["+fld_cumpl_180_vig.toString()+"] fld_por_cpl_hip["+fld_por_cpl_hip.toString()+"]-------#######");  
   //   System.out.print("####### ---- aqui imorimimos Prueba de Model[[["+model.getCategories().toString()+"]]]el ser de output FIN-------#######");
     // System.out.print("Este es el contenido del SP Ejecutado["+(String) out.get("out_fld_Cumpl_Recupero")+"]");
     // Student student = new Student();
     // student.setId(id);
     // student.setName((String) out.get("out_name"));
      //student.setAge((Integer) out.get("out_age"));

      return "BIEN Perf";
   } 
    
    public String updatePerfilSison(String alias, String perfil) {
        this.jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_homologa_usuarios_app");
        
        int opcion =13; //4cREAION 1 vista 12 elimina
        System.out.println("update parameters alias-> "+alias);
        System.out.println("update parameters perfil-> "+perfil);
        System.out.println("update parameters opcion-> "+opcion);
        //String 
        
        SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario", alias).addValue("perfil", perfil).addValue("col", opcion);
        Map<String, Object> out = jdbcCall.execute(in);
       
        System.out.print("####### ---- aqui imprimimos el res de output -------#######");
        for (String keys : out.keySet()) {
            System.out.println(keys);
        }

        return "Update perfil exitoso";
    }
    
    public String eliminaPerfilSison(String alias, String perfil) {
        this.jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_homologa_usuarios_app");
        //sss="METROPOLITANA CORDILLERA";
        // alias="esilves";
        // perfil="ejesiscon";
        int opcion =12; //4cREAION 1 vista 12 elimina

        //String 
        
        SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario", alias).addValue("perfil", perfil).addValue("col", opcion);
        Map<String, Object> out = jdbcCall.execute(in);
        // CategoryModel model;

        //    model = new DefaultCategoryModel();
        //private static final CategoryModel model;
        System.out.print("####### ---- aqui imprimimos el res de output -------#######");
        for (String keys : out.keySet()) {
            System.out.println(keys);
        }

//        ArrayList resultList = (ArrayList) out.get("#result-set-1");
//
//        Map resultlMap = (Map) resultList.get(0);
//
//        for (int nn = 0; nn < resultList.size(); nn++) {
//            Map resultlMapx = (Map) resultList.get(nn);
////System.out.print("####### i:["+nn+"]----EJECUTIVO["+((String) resultlMapx.get("FLD_NOM")).toString()+"] fld_Cumpl_Recupero :["+((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString()+"]  fld_cumpl_90_vig["+((Double) resultlMapx.get("fld_cumpl_90_vig")).toString()+"]  fld_cumpl_180_vig["+((Double) resultlMapx.get("fld_cumpl_180_vig")).toString()+"] fld_por_cpl_hip["+((Double) resultlMapx.get("fld_por_cpl_hip")).toString()+"]-------#######");
//        }

        return "Elimina perfil exitoso";
    }
    
      public List<String>  listPerfilSison(String alias,String perfil) {
          this.jdbcCall =  new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_homologa_usuarios_app");
         //sss="METROPOLITANA CORDILLERA";
        // alias="esilves";
        // perfil="ejesiscon";
         int opcion =1; //4cREAION 1 vista
         
         
         //String 
         
      List<String> inf_perf = new ArrayList();;
         
      SqlParameterSource in = new MapSqlParameterSource().addValue("aliasusuario",alias).addValue("perfil",perfil).addValue("col", opcion);
      Map<String, Object> out = jdbcCall.execute(in);
     // CategoryModel model;
      
  //    model = new DefaultCategoryModel();
      //private static final CategoryModel model;
      System.out.print("####### ---- aqui imprimimos el res de output -------#######");
      for (String keys : out.keySet())
{
    System.out.println("keys");
   System.out.println(keys);
}
      
      ArrayList resultList = (ArrayList) out.get( "#result-set-1" );
      ArrayList resultList8 = (ArrayList) out.get( "#result-set-8" );

Map resultlMap = (Map) resultList.get( 0 );

Map<String,String> listPerfiles1 = new HashMap<String, String>();

//for (int nn=0;nn<resultList.size();nn++)
//{
//Map resultlMapx = (Map) resultList.get( nn );
////System.out.print("####### i:["+nn+"]----EJECUTIVO["+((String) resultlMapx.get("FLD_NOM")).toString()+"] fld_Cumpl_Recupero :["+((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString()+"]  fld_cumpl_90_vig["+((Double) resultlMapx.get("fld_cumpl_90_vig")).toString()+"]  fld_cumpl_180_vig["+((Double) resultlMapx.get("fld_cumpl_180_vig")).toString()+"] fld_por_cpl_hip["+((Double) resultlMapx.get("fld_por_cpl_hip")).toString()+"]-------#######");
//
//System.out.print("####### i:["+nn+"]----USUARIO["+((String) resultlMapx.get("Nombres")).toString()+"] PERFIL :["+((String) resultlMapx.get("Perfil")).toString()+"] -------#######");
//
////Map<String, Object> listPerfiles1=new Map<String, String>();
//
//listPerfiles1.put("perfil1", ((String) resultlMapx.get("Perfil")).toString());
//inf_perf.add(((String) resultlMapx.get("Perfil")).toString());
//
//}
for (int nn=0;nn<resultList8.size();nn++)
{
Map resultlMapx = (Map) resultList8.get( nn );
//System.out.print("####### i:["+nn+"]----EJECUTIVO["+((String) resultlMapx.get("FLD_NOM")).toString()+"] fld_Cumpl_Recupero :["+((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString()+"]  fld_cumpl_90_vig["+((Double) resultlMapx.get("fld_cumpl_90_vig")).toString()+"]  fld_cumpl_180_vig["+((Double) resultlMapx.get("fld_cumpl_180_vig")).toString()+"] fld_por_cpl_hip["+((Double) resultlMapx.get("fld_por_cpl_hip")).toString()+"]-------#######");

System.out.print("####### i:["+nn+"]----DESCRIPCION["+((String) resultlMapx.get("descripcion")).toString()+"] PERFIL :["+((String) resultlMapx.get("dv_CodPerfil")).toString()+"] -------#######");

//Map<String, Object> listPerfiles1=new Map<String, String>();

listPerfiles1.put("perfil2", ((String) resultlMapx.get("descripcion")).toString()+"("+((String) resultlMapx.get("dv_CodPerfil")).toString()+")");
//inf_perf.add(((String) resultlMapx.get("descripcion")).toString()+"("+((String) resultlMapx.get("dv_CodPerfil")).toString()+")");***************************
inf_perf.add(((String) resultlMapx.get("descripcion")).toString());
}
//Double fld_Cumpl_Recupero = (Double) resultlMap.get("fld_Cumpl_Recupero");
//Double fld_cumpl_90_vig = (Double) resultlMap.get("fld_cumpl_90_vig");
//Double fld_cumpl_180_vig = (Double) resultlMap.get("fld_cumpl_180_vig");
//Double fld_por_cpl_hip = (Double) resultlMap.get("fld_por_cpl_hip");        
        
     //System.out.print("####### ---- fld_Cumpl_Recupero :["+fld_Cumpl_Recupero.toString()+"] fld_cumpl_90_vig["+fld_cumpl_90_vig.toString()+"]  fld_cumpl_180_vig["+fld_cumpl_180_vig.toString()+"] fld_por_cpl_hip["+fld_por_cpl_hip.toString()+"]-------#######");  
   //   System.out.print("####### ---- aqui imorimimos Prueba de Model[[["+model.getCategories().toString()+"]]]el ser de output FIN-------#######");
     // System.out.print("Este es el contenido del SP Ejecutado["+(String) out.get("out_fld_Cumpl_Recupero")+"]");
     // Student student = new Student();
     // student.setId(id);
     // student.setName((String) out.get("out_name"));
      //student.setAge((Integer) out.get("out_age"));

      return inf_perf;
   } 
    
    
    public boolean delRow() {
         
        try {
            String SQL = "delete from [dbo].[tracking_event]";
            System.out.println("delete [dbo].[tracking_event] SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
