/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades.implementaciones;

import siscon.entidades.implementaciones.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Banca;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author esilves
 */
public class LogJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public LogJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public boolean guardarReparo(final int idcondonacion, final String Motivo, final String Glosa, final String cuenta) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO reparo\n"
                + "           (di_fk_IdCondonacion\n"
                + "           ,dv_MotivoReparo\n"
                + "           ,dv_GlosaReparo"
                + "           ,di_fk_IdColaborador"
                + "           ,registrado"
                + " )\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,(select cola.id from colaborador cola  inner join usuario usu  on usu.id_usuario=cola.id_usuario   where usu.alias=?) \n"
                + "           ,getdate())\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"di_idReparo"});
                    ps.setInt(1, idcondonacion);
                    ps.setString(2, Motivo);
                    ps.setString(3, Glosa);
                    ps.setString(4, cuenta);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Reparo : 61[" + e.toString() + "]");
            ok = false;
        }
        return ok;

    }

    public List<Banca> getALLBanca(String perfil) {
        List<Banca> lBanca = new ArrayList<Banca>();

        final String SQL = "SELECT  ba.id,ba.cod_banca,concat(ba.cod_banca,'-#-',ba.descripcion)'descripcion'  FROM banca ba\n"
                + "  left join perfil_bojBanca peo on peo.fkIdBanca=ba.id\n"
                + "   where peo.fkIdBanca is null";

        try {
            lBanca = jdbcTemplate.query(SQL, new RowMapper<Banca>() {
                public Banca mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Banca ban = new Banca();

                    ban.setId(rs.getInt("id"));
                    ban.setCod_banca(rs.getString("cod_banca"));
                    ban.setDescripcion(rs.getString("descripcion"));

                    return ban;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error Mostrando Perfiles Ejecutivos ALL Error:[" + ex.getMessage() + "] Query : [ " + SQL + "]");
        }
        return lBanca;

    }

    public List<Banca> getBancaXPerfil(String Perfil) {
        List<Banca> lBanca = new ArrayList<Banca>();

        final String SQL = "select ba.* from perfil pe \n"
                + "         inner join perfil_bojBanca peo on peo.fk_id_perfil=pe.id_perfil\n"
                + "		 inner join banca ba on ba.id=peo.fkIdBanca\n"
                + "\n"
                + "		 where \n"
                + "		 pe.descripcion='" + Perfil + "'";

        try {
            lBanca = jdbcTemplate.query(SQL, new RowMapper<Banca>() {
                public Banca mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Banca ban = new Banca();

                    ban.setId(rs.getInt("id"));
                    ban.setCod_banca(rs.getString("cod_banca"));
                    ban.setDescripcion(rs.getString("descripcion"));

                    return ban;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error Mostrando Perfiles Ejecutivos ALL Error:[" + ex.getMessage() + "] Query : [ " + SQL + "]");
        }
        return lBanca;

    }

    public boolean GuardarBancaPerfil(final String perfil, final String banca) {
        boolean retorna = false;

        final String SQL = "INSERT INTO [perfil_bojBanca]\n"
                + "           ([fk_id_perfil]\n"
                + ",           [fkIdBanca]\n"
                + ",           [registrado]\n"
                + "           )"
                + "     VALUES\n"
                + "            ((select pe.id_perfil from perfil pe where pe.descripcion= ?)\n"
                + "            ,(select ba.id from banca ba where concat(ba.cod_banca,'-#-',ba.descripcion) =?)\n"
                + "            ,(CONVERT(datetime,GETDATE(),120))\n"
                + "             )";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_perfilbanca"});
                    ps.setString(1, perfil);
                    ps.setString(2, banca);
                    //ps.setInt(3, marBan.getFk_idCondonacion());
                    return ps;
                }
            }, key);

            retorna = true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = false;
        }
        return retorna;
    }

    
    
    
    public void guardaAcceso(String Codigo,String nm,String pd,String desc)
    {
           // boolean retorna = false;

        final String SQL = "INSERT INTO [tracking_accesos]\n"
                + "           ([id_usuario]\n"
                + ",           [registro]\n"
                + ",           [fk_id_accion]\n"
                + ",           [fk_id_sistema]\n"
                + ",           [descripcion]\n"
                + "           )"
                + "     VALUES\n"
                + "            ((select usu.id_usuario from usuario usu where usu.alias='SIN_USER')\n"
                + "            ,(CONVERT(datetime,GETDATE(),120))\n"
                + "            ,(select trac.id from tracking_acciones trac where trac.descripcion ='"+Codigo+"')\n"
                + "            ,(select sis.id from sistema sis where sis.codigo ='SISCON')\n" 
                + "            ,('"+Codigo+""+desc+"')\n"         
                + "             )";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() 
            {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});
                   // ps.setString(1, perfil);
                   // ps.setString(2, banca);
                    //ps.setInt(3, marBan.getFk_idCondonacion());
                    return ps;
                }
            }, key);

            //retorna = true;
        } catch (DataAccessException e) 
        {
            Messagebox.show(e.toString());
            //retorna = false;
        }
      //  return retorna;
    
    
    }
    
    
}
