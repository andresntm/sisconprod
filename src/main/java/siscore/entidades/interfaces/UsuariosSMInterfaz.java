/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.entidades.interfaces;

import java.util.List;
import siscon.entidades.AccesosEjecutivo;
import siscon.entidades.ReglaTiempoGrilla;
import siscon.entidades.TrackingRowTomadaUsr;
import siscon.entidades.UsuariosSM;
import siscon.entidades.usuario;

/**
 *
 * @author exesilr
 */
public interface UsuariosSMInterfaz {


    
public List<AccesosEjecutivo> getInfEjecutivoAccessos();
public List<UsuariosSM> getUsuariosSM();
public String  addPerfilSison(String alias,String perfil);
public String addPerfilSisCore(String alias, String areatrabajo,String perfil);

public List<String>   listPerfilSison(String alias,String perfil);
public String eliminaPerfilSison(String alias,String perfil);
public String updatePerfilSison(String alias,String perfil);
public String  addPerfilSisonPerf(String alias,String perfil);

//hola
public List<TrackingRowTomadaUsr> getTrackingRow(int usuarioTRow);
public List<TrackingRowTomadaUsr> getTrackingRow();
public int getTimeRow();
public boolean insertFlagRow(TrackingRowTomadaUsr tmrglf,int getTime);
public List<usuario> getNomColabById(int colabId);
public int getIdColabByNom(String colabnom);
public boolean updateFlagRow(int usuarioTRow,int estado,int getTime);
public boolean updateFlagRow2(int usuarioTRow,int estado,int getTime,int idcola);
public List<ReglaTiempoGrilla> getReglaGrilla();
public boolean updateTimeRow(int usuarioTRow,int tiempo);
public boolean delRow();
public boolean updateEstadoRow(int usuarioTRow,int estado);
}
