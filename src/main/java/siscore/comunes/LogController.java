/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.comunes;

/**
 *
 * @author exesilr
 */
public class LogController {

    private String fullClassName;
    private String className;
    private String methodName;
    private int lineNumber;

    public LogController() {

        this.fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        this.className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        this.methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        this.lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();

    }

    public static void SisCorelog(String message) {
        //if (DEBUG) {
        String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();

        //  Log.d("message"+message+"classname:"+className + "." + methodName + "():" +lineNumber);
        System.out.println("####### ---- LOG de EJECUCION de fullClassName[" + fullClassName + "] className [" + className + "]  methodName [" + methodName + "] lineNumber [" + lineNumber + "] message [" + message + "]----#######");
        // }

    }

    public String getFullClassName() {
        return fullClassName;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public int getLineNumber() {
        return lineNumber;
    }

}
