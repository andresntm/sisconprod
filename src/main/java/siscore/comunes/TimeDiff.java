/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscore.comunes;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author exesilr
 */
public class TimeDiff {

    String date1 = "07/15/2016";
    String time1 = "11:00 AM";
    String date2 = "07/17/2016";
    String time2 = "12:15 AM";
    String format = "MM/dd/yyyy hh:mm a";
    Date dateObj1;
    Date dateObj2;
    long diff;
    DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");
    SimpleDateFormat sdf = new SimpleDateFormat(format);

    public TimeDiff(String d1,String t1,String d2,String t2) throws ParseException {
        
        dateObj1 = sdf.parse(d1!=null? d1:date1 + " " + t1==null? time1:t1);
        dateObj2 = sdf.parse(d2!=null? d1:date2 + " " + t2==null? time1:t2);

        diff = dateObj2.getTime() - dateObj1.getTime();
        System.out.println(dateObj1);
        System.out.println(dateObj2 + "\n");
    }

    public int mintunos() {
        int diffmin = 0;

        diffmin = (int) (diff / (60 * 1000));
        //System.out.println("difference between minutues: " + crunchifyFormatter.format(diffmin));

        return diffmin;
    }

    public int segundos() {
        int diffsec = 0;

        diffsec = (int) (diff / (1000));
        //System.out.println("difference between seconds: " + crunchifyFormatter.format(diffsec));

        return diffsec;

    }

    public int horas() {
        int diffhours = 0;

        diffhours = (int) (diff / (60 * 60 * 1000));
        //System.out.println("difference between hours: " + crunchifyFormatter.format(diffhours));

        return diffhours;

    }

    public int dias() {
        int diffdias = 0;

        diffdias = (int) (diff / (24 * 60 * 60 * 1000));
        //SiscoreLog	System.out.println("difference between days: " + retorno);

        return diffdias;

    }

}
