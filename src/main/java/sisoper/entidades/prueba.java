/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisoper.entidades;

import siscon.entidades.*;
import java.io.File;
import java.util.Date;
import org.zkoss.util.media.Media;

/**
 *
 * @author excosoc
 */
public class prueba {

    /**
     * @return the fileDown
     */
    public File getFileDown() {
        return fileDown;
    }

    /**
     * @param fileDown the fileDown to set
     */
    public void setFileDown(File fileDown) {
        this.fileDown = fileDown;
    }

    /**
     * @return the tama�oNum
     */
    public float getTama�oNum() {
        return tama�oNum;
    }

    /**
     * @param tama�oNum the tama�oNum to set
     */
    public void setTama�oNum(float tama�oNum) {
        this.tama�oNum = tama�oNum;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private int fk_idColaborador;
    private int id_det_archivos;
    private int fk_id_subearchivo;
    private String patch_archivo;
    private String extension;
    private String tama�o;
    private Date registrado;
    private boolean newFile;
    private Media fileUp;
    private String fileName;
    private float tama�oNum;
    private File fileDown;

    public prueba() {
    }

    public prueba(int id_det_archivos,
            int fk_id_subearchivo,
            String patch_archivo,
            String extension,
            String tama�o,
            int fk_idColaborador,
            boolean newFile,
            String fileName,
            float tama�oNum) {

        this.fk_idColaborador = fk_idColaborador;
        this.id_det_archivos = id_det_archivos;
        this.fk_id_subearchivo = fk_id_subearchivo;
        this.patch_archivo = patch_archivo;
        this.extension = extension;
        this.tama�o = tama�o;
        this.newFile = newFile;
        this.fileName = fileName;
        this.tama�oNum = tama�oNum;
    }

    /**
     * @return the fileUp
     */
    public Media getFileUp() {
        return fileUp;
    }

    /**
     * @param fileUp the fileUp to set
     */
    public void setFileUp(Media fileUp) {
        this.fileUp = fileUp;
    }

    /**
     * @return the newFile
     */
    public boolean isNewFile() {
        return newFile;
    }

    /**
     * @param newFile the newFile to set
     */
    public void setNewFile(boolean newFile) {
        this.newFile = newFile;
    }

    /**
     * @return the id_det_archivos
     */
    public int getId_det_archivos() {
        return id_det_archivos;
    }

    /**
     * @param id_det_archivos the id_det_archivos to set
     */
    public void setId_det_archivos(int id_det_archivos) {
        this.id_det_archivos = id_det_archivos;
    }

    /**
     * @return the fk_id_subearchivo
     */
    public int getFk_id_subearchivo() {
        return fk_id_subearchivo;
    }

    /**
     * @param fk_id_subearchivo the fk_id_subearchivo to set
     */
    public void setFk_id_subearchivo(int fk_id_subearchivo) {
        this.fk_id_subearchivo = fk_id_subearchivo;
    }

    /**
     * @return the patch_archivo
     */
    public String getPatch_archivo() {
        return patch_archivo;
    }

    /**
     * @param patch_archivo the patch_archivo to set
     */
    public void setPatch_archivo(String patch_archivo) {
        this.patch_archivo = patch_archivo;
    }

    /**
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * @param extension the extension to set
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * @return the tama�o
     */
    public String getTama�o() {
        return tama�o;
    }

    /**
     * @param tama�o the tama�o to set
     */
    public void setTama�o(String tama�o) {
        this.tama�o = tama�o;
    }

    /**
     * @return the registrado
     */
    public Date getRegistrado() {
        return registrado;
    }

    /**
     * @return the fk_idColaborador
     */
    public int getFk_idColaborador() {
        return fk_idColaborador;
    }

    /**
     * @param fk_idColaborador the fk_idColaborador to set
     */
    public void setFk_idColaborador(int fk_idColaborador) {
        this.fk_idColaborador = fk_idColaborador;
    }

    /**
     * @param registrado the registrado to set
     */
    public void setRegistrado(Date registrado) {
        this.registrado = registrado;
    }

}
