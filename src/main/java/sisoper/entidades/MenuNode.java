package sisoper.entidades;

import java.util.LinkedList;
import java.util.List;

public class MenuNode
{
    private String label;

    private String iconSclass;


    private String link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
 private String id;
    private List<MenuNode> subMenus = new LinkedList<MenuNode>();

    public MenuNode(String label)
    {
        this.label = label;
    }

    public MenuNode(String label, String iconSclass)
    {
        this(label);
        this.iconSclass = iconSclass;
    }

    

    
    
    
    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getIconSclass()
    {
        return iconSclass;
    }

    public void setIconSclass(String iconSclass)
    {
        this.iconSclass = iconSclass;
    }

    public List<MenuNode> getSubMenus()
    {
        return subMenus;
    }

    public void addMenuNode(MenuNode menuNode)
    {
        this.subMenus.add(menuNode);
    }
    
    
    
        public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
    
}
