package sisoper.controller;

import java.util.LinkedList;
import java.util.List;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import sisoper.entidades.MenuNode;

public class NavbarVM
{
    private List<MenuNode> menuHierarchy;

    private MenuNode selectedMenuNode;

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    public String ruta;
    @Init
    public void init()
    {
        this.menuHierarchy = new LinkedList<MenuNode>();
        
        this.ruta="Contabilidad/InformeHonorarios.zul";
        
        MenuNode menuNodeA = new MenuNode("A", "z-icon-plus");
        menuNodeA.addMenuNode(new MenuNode("AA"));
        
        
        
        
        
        this.menuHierarchy.add(menuNodeA);
        MenuNode menuNodeB = new MenuNode("B", "z-icon-minus");
        menuNodeB.addMenuNode(new MenuNode("BA"));
        MenuNode menuNodeBB = new MenuNode("BB");
        menuNodeBB.setLink("Informes/InformeCond_Ap_Re_Pe.zul");
        
        menuNodeBB.setId("BB");
        menuNodeB.addMenuNode(menuNodeBB);
        
        
        
        
        this.menuHierarchy.add(menuNodeB);
        
        MenuNode menuNodeC = new MenuNode("C", "z-icon-money");
        
        
        
        this.menuHierarchy.add(menuNodeC);
        this.selectedMenuNode = menuNodeBB;
    }

    public List<MenuNode> getMenuHierarchy()
    {
        return this.menuHierarchy;
    }

    public MenuNode getSelectedMenuNode()
    {
        return this.selectedMenuNode;
    }
    
    
    
        
        @Command
    public void toInner2(@SelectorParam("div") LinkedList<Div> labels,@SelectorParam("#include33") Include pageref) {
       // Div gg=(Div) ("#sidebar");
       
      // Messagebox.show("Labels:["+labels.get+"]");
       
       
       
              for (int i = 0; i < labels.size(); i++) {
           // ;
             Messagebox.show("Labels:["+labels.get(i).getId()+"]");
        }
       
        pageref.setSrc("Contabilidad/InformeHonorarios.zul");
    }
    
    
}
