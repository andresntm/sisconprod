package applications2;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.Set;
import javax.servlet.http.HttpSession;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Perfil;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;

public class SidebarComposer extends SelectorComposer<Component> {

    @Wire
    Hlayout main;
    @Wire
    Div sidebar;
    @Wire
    Navbar navbar;
    @Wire
    Navitem calitem;
    @Wire
    A toggler;
    public String UsuarioCustodia;
    public String NomUsuario;
    HttpSession hses;
    String httpsession;
    int idsessionsiscon;
    HttpSessionJDBC _httpsession;
    MvcConfig mmmm = new MvcConfig();
    @Wire
    Include content;
    String Perfil;
    String Alias;
    
    @Wire            
    Navitem lbl_campana;

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }
     boolean tieneperfil;
    public SidebarComposer() throws SQLException {

        Session sess = Sessions.getCurrent();
        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        //String Account = permisos.getCuenta();//user.getAccount();
        this.Alias=permisos.getCuenta();
        String Nombre = permisos.getNombre();
        this.UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();
        this.AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        this.NomUsuario = Nombre;
        Set<Perfil> perfiless;
        
        
        //lbl_campana.setLabel("Cond.Campa�a");
//        lbl_campana.getAutag();
       // lbl_campana.getLabel()
        
        Perfil kk= new Perfil();
        
        
        // buscar perfil sistemico del jefe de aplicacoion
        for (final Perfil pe : permisos.getPerfiless()) {
  
            
            if (pe.getCodPerfil().equalsIgnoreCase("JEFAPPSISCON"))
            {
            tieneperfil=true;
            kk=pe;
            
            }
            
            
        }
        if(this.tieneperfil){this.Perfil=kk.getCodPerfil();}
        else {this.Perfil="NOPERFIL";}
        
      //  kk.setCodPerfil("JEFAPPSISCON");
        //kk.setId_perfil(20);
        //kk.setDescripcion("JEFE Modulo APlicacion");
        //kk.setId_jefe(0);
        // definamos Perfiles y usuarios
        //this.Perfil= permisos.get
        //perfiless = permisos.getPerfiless();
         //this.Perfil = perfiless.
        //tieneperfil=kk.equals();
        //this.tieneperfil = permisos.TienePerfil(kk);
        // tieneperfil=perfiless.isEmpty();
       // Aqui filtramos a  el permiso de HErnan
        //
        
        _httpsession = new HttpSessionJDBC(mmmm.getDataSource());
        hses = (HttpSession) sess.getNativeSession();
        httpsession = hses.getId();
        idsessionsiscon = _httpsession.GetSisConIdSession(httpsession);

    }

    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        // message
    }

    // Toggle sidebar to collapse or expand
    @Listen("onClick = #toggler")
    public void toggleSidebarCollapsed() {

        _httpsession.setSessionAccion("onClick = #toggler", idsessionsiscon, 2);

        if (navbar.isCollapsed()) {
            sidebar.setSclass("sidebar");
            navbar.setCollapsed(false);
            //calitem.setTooltip("calpp, position=end_center, delay=0");
            toggler.setIconSclass("z-icon-angle-double-left");
        } else {
            sidebar.setSclass("sidebar sidebar-min");
            navbar.setCollapsed(true);
            //calitem.setTooltip("");
            toggler.setIconSclass("z-icon-angle-double-right");
        }
        // Force the hlayout contains sidebar to recalculate its size
        Clients.resize(sidebar.getRoot().query("#main"));
    }

    public String getAreaTrabajo() {
        return AreaTrabajo;
    }
    public String getAlias() {
        return Alias;
    }

    
    public void setAreaTrabajo(String AreaTrabajo) {
        this.AreaTrabajo = AreaTrabajo;
    }
    public String AreaTrabajo;

    public String getUsuarioCustodia() {
        return UsuarioCustodia;
    }

    public void setUsuarioCustodia(String UsuarioCustodia) {
        this.UsuarioCustodia = UsuarioCustodia;
    }

    public String getNomUsuario() {
        return NomUsuario;
    }

    public void setNomUsuario(String NomUsuario) {
        this.NomUsuario = NomUsuario;
    }

    public void clickmenu(String _menuclick) {
        // Messagebox.show("fdsfsdfsd");

        _httpsession.setSessionAccion(_menuclick, idsessionsiscon, 2);
    }
    
    
    
        @Listen("onClick = #BB")
    public void menuList(){
            try {
                Messagebox.show("enelLIST");
        content.setSrc("page1.zul");
        }catch(Exception e){
             Messagebox.show("ErrorList");
            System.out.println("Error Btn Clicked, " +e);
        }
    }
    
    
    
    @NotifyChange("visible")
    @GlobalCommand
    public void show(){
    
    Messagebox.show("EscondeMensaje");
    }
    
    

    
    
}
