package configuracion;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.Query;
import org.zkoss.zul.Messagebox;
import static siscore.comunes.LogController.SisCorelog;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author esilves
 */
public class GeneralConf {

    private static String modo;
    String internalIPs = "192\\.168\\.X\\.X" + "|";
    String My_ProduccionIP = "10\\.0\\.2\\.X" + "|" + "127\\.0\\.0\\.X" + "|";
    String MyDesarrollo = "161\\.131\\.189\\.X" + "|";
    String MyQA = "161\\.131\\.219\\.X" + "|";
    String QAProduccion = "161\\.131\\.189\\.X" + "|";
    String Produccion = "172\\.16\\.26\\.X" + "|" + "172\\.16\\.27\\.X" + "|";
    String host_source_prod = "";
    String db_source_prod = "";
    String IpMatch = "X\\.X\\.X\\.X" + "|";
    String host;

    GeneralConf(String input) {

        //   SisBojConf.modo = input == null ? "debug" : input;
    }

    /**
     *
     */
    public GeneralConf() {

        // SisBojConf.modo = "debugddd";
    }

    /**
     *
     * @param input
     */
    public static void print(String input) {

        if (modo.equals("debug")) {

            Messagebox.show(input);

        }

    }

    public void initIt() throws Exception {

        //  SisBojConf.modo = "debugff";
        //System.out.println(" metodo de inicio antes de propiedades : [[[[" +SisBojConf.modo);
    }

    public String getIpAddressAndPort1() throws MalformedObjectNameException, NullPointerException, UnknownHostException {
        MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
        Set<ObjectName> objectNames = beanServer.queryNames(new ObjectName("*:type=Connector,*"),
                Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
        String host = InetAddress.getLocalHost().getHostAddress();
        String port = objectNames.iterator().next().getKeyProperty("port");
        String ipadd = "http" + "://" + host + ":" + port;
        System.out.println(ipadd);
        return ipadd;
    }

    public String _determinaAmbiente() {

        String ambiente = "Noseterminado";
        String ip;
        Pattern ippatern = Pattern.compile("^(?:" + IpMatch.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Matcher mm;
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    ip = addr.getHostAddress();
                    String Name = iface.getDisplayName();
                    if (!Name.matches("(?i).*VirtualBox.*")) {
                        mm = ippatern.matcher(ip);
                        if (mm.matches()) {
                            host = ip;
                        }

                    }
                    System.out.println(Name + " " + ip);
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        //Pattern p = Pattern.compile("^(?:"+internalIPs.replaceAll("X", "(?:\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])")+")$");      7
        Pattern p = Pattern.compile("^(?:" + internalIPs.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern Developer = Pattern.compile("^(?:" + MyDesarrollo.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern My_Produccion = Pattern.compile("^(?:" + My_ProduccionIP.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern AltaDisponibilidad = Pattern.compile("^(?:" + Produccion.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");

        Matcher m = p.matcher(host);
        Matcher m2dev = Developer.matcher(host);
        Matcher m3Prod = My_Produccion.matcher(host);

        Matcher MatcherAltaDisponibilidad = AltaDisponibilidad.matcher(host);

        SisCorelog("MccConfig: [" + host + "]");

        if (m.matches()) {     //// house Instance

            ambiente = "desarrollo";

        } else if (m2dev.matches()) {    // Local Work Instance
            //this.host_source_prod = "161.131.230.214";

            ambiente = "produccion";

        } else if (m3Prod.matches()) {  //Production Instance

            ambiente = "produccion";
            // this.DondeEstoy = "161.131.230.214";
        } else if (MatcherAltaDisponibilidad.matches()) {   // Alta Disponibilidad Instance
            ambiente = "produccion";
        }

        return ambiente;
    }
    
    


}
