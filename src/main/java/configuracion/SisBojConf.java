package configuracion;

import org.zkoss.zul.Messagebox;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author esilves
 */
public class SisBojConf {

    private static String modo;
    public GeneralConf _general;

    SisBojConf(String input) {

        SisBojConf.modo = input == null ? "debug" : input;

    }

    /**
     *
     */
    public SisBojConf() {
        _general = new GeneralConf();
        //SisBojConf.modo = "produccion";
        SisBojConf.modo = _general._determinaAmbiente();
    }

    /**
     *
     * @param input
     */
    public static void print(String input) {

        if (modo.equals("debug")) {

            Messagebox.show(input);

        }

    }

    public static String getModo() {
        return modo;
    }

    public static void setModo(String modo) {
        SisBojConf.modo = modo;
    }

    public void initIt() throws Exception {

        SisBojConf.modo = "debugff";
        System.out.println("metodo de inicio antes de propiedades : [[[[" + SisBojConf.modo);
    }

    public void cleanUp() throws Exception {
        System.out.println("Spring Container is destroy! Customer clean up");
    }

    public String Nombresp_detClie() {
        String nombre_sp = "";

        if (modo.equals("produccion")) {
            nombre_sp = "sp_web_syscon_det_ope_cli";
        } else if (modo.equals("qa")) {
            nombre_sp = "sp_web_det_ope_cli_SISCON";
        } else if (modo.equals("desarrollo")) {
            //nombre_sp = "sp_web_syscon_det_ope_cli_dev";/// esto se actualizo por la adicio del servidor de certificacion
            nombre_sp = "sp_web_syscon_det_ope_cli";
        }

        //   return ("debug".equals(modo))?"sp_web_det_ope_cli_SISCON":"sp_web_syscon_det_ope_cli";
        return nombre_sp;
    }

    public String Nombresp_detClie2() {
        String nombre_sp = "";

        if (modo.equals("produccion")) {
            nombre_sp = "sp_web_syscon_con_cli_V2";
        } else if (modo.equals("qa")) {
            nombre_sp = "sp_web_syscon_con_cli_SISCON";
        } else if (modo.equals("desarrollo")) {
           // nombre_sp = "sp_web_syscon_con_cli_dev";
            nombre_sp = "sp_web_syscon_con_cli_V2"; /// esto se actualizo por la adicio del servidor de certificacion
        }

        //   return ("debug".equals(modo))?"sp_web_det_ope_cli_SISCON":"sp_web_syscon_det_ope_cli";
        return nombre_sp;
    }

}
