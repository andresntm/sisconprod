/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.utilidades;

import java.util.ResourceBundle;

/**
 *
 * @author EXAESAN
 */
public class DiccionarioCodeZpl {
    
    public String diccionarioCodeZpl(String code) {

        String codeFinal = "";
        ResourceBundle myResources = ResourceBundle.getBundle("zpldiccionario");
       

        for (int n = 0; n < code.length(); n++) {
            char chard = code.charAt(n);
            String charToString = Character.toString(chard);
            

            /*Logica Diccionario*/
            if (charToString.equals("A")) {
                codeFinal += myResources.getString("CharA");
            } else if (charToString.equals("B")) {
                codeFinal += myResources.getString("CharB");
            } else if (charToString.equals("C")) {
                codeFinal += myResources.getString("CharC");
            } else if (charToString.equals("D")) {
                codeFinal += myResources.getString("CharD");
            } else if (charToString.equals("E")) {
                codeFinal += myResources.getString("CharE");
            } else if (charToString.equals("0")) {
                codeFinal += myResources.getString("Char0");
            } else if (charToString.equals("1")) {
                codeFinal += myResources.getString("Char1");
            } else if (charToString.equals("2")) {
                codeFinal += myResources.getString("Char2");
            } else if (charToString.equals("3")) {
                codeFinal += myResources.getString("Char3");
            } else if (charToString.equals("4")) {
                codeFinal += myResources.getString("Char4");
            } else if (charToString.equals("5")) {
                codeFinal += myResources.getString("Char5");
            } else if (charToString.equals("6")) {
                codeFinal += myResources.getString("Char6");
            } else if (charToString.equals("7")) {
                codeFinal += myResources.getString("Char7");
            } else if (charToString.equals("8")) {
                codeFinal += myResources.getString("Char8");
            } else if (charToString.equals("9")) {
                codeFinal += myResources.getString("Char9");
            }

        }

        return codeFinal;
    }
}
