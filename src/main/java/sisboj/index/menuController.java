/* BooksController.java

	Purpose:
		
	Description:
		
	History:
		5:53 PM 11/9/15, Created by jumperchen

Copyright (C) 2015 Potix Corporation. All Rights Reserved.
*/
package sisboj.index;




import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author jumperchen
 */

@Controller ("menuIndexController") 

@RequestMapping(value = {"/sisboj/*"})
//@SessionAttributes({"booksVM"})
public class menuController {
	@RequestMapping(value = {"navbar"}, method = RequestMethod.GET)
	public String navbar(ModelMap model) {
  
		return "applications2/navbar.zul";
	}
        	@RequestMapping(value = {"sidebar"}, method = RequestMethod.GET)
	public String sidebar(ModelMap model) {
  
		return "applications2/sidebar.zul";
	} 
        
        
             	@RequestMapping(value = {"page"}, method = RequestMethod.GET)
	public String page(ModelMap model) {
  
		return "applications2/page.zul";
	} 
	/*@RequestMapping(value = {"bootstrap.min.css","WEB-INF/Index/bootstrap.min.css","/WEB-INF/Index/bootstrap.min.css"}, method = RequestMethod.GET)
	public String css(ModelMap model) {

		return "Index/bootstrap.min.css";
	}
*/

}
