/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.esencial;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import sisboj.entidades.UsuariosLdapEnt;
import sisboj.entidades.implementaciones.UsuariosLdapJDBC;

/**
 *
 * @author EXVGUBA
 */
public class LdapController {

//    DirLdapJDBC usuLdap = new DirLdapJDBC();
    UsuariosLdapJDBC usuJDBC = new UsuariosLdapJDBC();

  

    public String getUsuarioPorIP(String ip) throws SQLException {        

        System.out.println(" ESTOY AQUI getUsuarioPorIP->");
        usuJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        System.out.println( usuJDBC.getClass().toString());
        List<UsuariosLdapEnt> usuldap = new ArrayList<UsuariosLdapEnt>();
        String usuario="";

        try {
            usuldap = usuJDBC.listUsuariosLdap();
            for(int i=0;i<usuldap.size();i++){
                if(usuldap.get(i).getIp().equals(ip)){
                    usuario = usuldap.get(i).getUsuario();
                    System.out.println("Usuario : "+usuario+" IP: "+ip+"  => Encontrado");
                    i=usuldap.size();
                }
            }
            
        } catch (Exception e) {
            System.out.println("SUCEDIO ALGO EN LdapController.getUsuarioPorIP =>" + e.getStackTrace());
            e.getStackTrace();
        }

        return usuario;
    }

}
