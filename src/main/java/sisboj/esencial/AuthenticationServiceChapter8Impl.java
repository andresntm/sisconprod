/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package sisboj.esencial; 

import java.util.List;
import sisboj.necesarias.AuthenticationServiceChapter5Impl;
import sisboj.necesarias.UserInfoServiceChapter5Impl;
import sisboj.entidades.User;
import sisboj.necesarias.UserCredential;
import sisboj.necesarias.UserInfoService;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

public class AuthenticationServiceChapter8Impl extends AuthenticationServiceChapter5Impl{
	private static final long serialVersionUID = 1L;
	
	UserInfoService userInfoService = new UserInfoServiceChapter5Impl();

	@Override
	public boolean login(String nm, String pd) {
		User user = userInfoService.findUser(nm);
		//a simple plan text password verification
		if(user==null || !user.getPassword().equals(pd)){
			return false;
		}
		
		Session sess = Sessions.getCurrent();
		UserCredential cre = new UserCredential(user.getAccount(),user.getFullName());
		//just in case for this demo.
		if(cre.isAnonymous()){
			return false;
		}
		sess.setAttribute("userCredential",cre);
		
		//TODO handle the role here for authorization
		return true;
	}
	
	@Override
	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("userCredential");
	}
        
        
        @Override
	public  String UserAcees(String nm, String pd) {
            User user = userInfoService.findUser(nm);
           // List<Contacto> listContact = contactoDAO.list();
           		if(user==null || !user.getPassword().equals(pd)){
			return "";
		}
                        
                     if(user.getuserAcces()!=null)return    user.getuserAcces();
                     else return "No Existe";
      
           // System.out.println("#-------Mostrando Datos en Nuevo Metodo Usuarios:::["+listContact+"]-----------@@@");
         // return listContact;
	}        

        

        
        
}
