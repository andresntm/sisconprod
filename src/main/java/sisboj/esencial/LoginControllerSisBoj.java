/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
 */
package sisboj.esencial;


import config.MvcConfig;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpSession;
import sisboj.necesarias.AuthenticationService;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;
import javax.servlet.http.HttpServletRequest;
import org.zkoss.zul.Window;

public class LoginControllerSisBoj extends SelectorComposer<Component> {

    private static final long serialVersionUID = 1L;

    //wire components
    @Wire
    Textbox txtSBoj;
    @Wire
    Textbox account;
    @Wire
    Textbox password;
    @Wire
    Label messageiniciologin;
    @Wire
    Window winLoginboj;
    @Wire
    Textbox loginusername;
    @Wire
    Textbox loginpassword;
    @Wire
    Textbox loginpasswordboj;
    @Wire
    Textbox loginpasswordoper;
    @Wire
    Textbox loginpasswordcob;
    MvcConfig mmmm = new MvcConfig();
    //services
    AuthenticationService authService = new AuthenticationServiceChapter8Impl();
    UsuarioPermisoJDBC usrPermiso;
    HttpSessionJDBC _httpsession;
    Session sess;
    HttpSession hses;
    String sistema = "";
    String UsuarioIP="";

    String remoteIP;

    public String getRemoteIP() {
        return remoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }

    LdapController ldpCtrl = new LdapController();

    public LoginControllerSisBoj() throws SQLException {
      //  this.setRemoteIP("0.0.0.0");

        sess = Sessions.getCurrent(true);
        hses = (HttpSession) sess.getNativeSession();

        usrPermiso = new MvcConfig().usuarioPermisoJDBC();
        _httpsession = new HttpSessionJDBC(mmmm.getDataSource());

        HttpServletRequest requestXX = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
       // remoteIP ="161.131.189.197"; 
      requestXX.getRemoteAddr();
       
      
        // collectInfo();

        // doLoginDB2();
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        doLoginDB2();
    }

    // aca inicia session buscando usuarios de sisboj
    @Listen("onLoad=#winLoginboj; onClick=#btn-loginboj; onOK=#btn-loginWin; onOK=#loginpasswordboj")
    public boolean doLoginDB2() throws SQLException, InterruptedException, IOException {
        String nm2 = "";
        String pd2 = ""; //loginpassword.getValue();
        String jsessionid = Executions.getCurrent().getHeader("cookie");
        sistema = "sisboj";
        
        System.out.println("remoteIP => "+this.remoteIP);
        
        //se obtiene usuario por ip
          UsuarioIP = ldpCtrl.getUsuarioPorIP(this.remoteIP);
         if(!UsuarioIP.equals("")){
             nm2=UsuarioIP;
             
                if (!usrPermiso.login(nm2, pd2, sistema)) {
                   Messagebox.show(String.format("Usuario sin acceso al sistema"));
                   return false;
               }
         }else{
             Messagebox.show(String.format("Usuario no encontrado"));
            return false;
         }
        
        // nm2 = "RGUTIEP";

        // boolean ldp = ldpCtrl.autenticacionLDAP(nm2);
       // boolean ldp = true;
       // System.out.println(":::::::::::::::::::::::::: ldp :::::::::::::::::::::: => " + ldp);
        // boolean ldp=true;
        
       /*
        if (!ldp) {
            Messagebox.show(String.format("Usuario no encontrado"));
            return false;
        } else {
            if (!usrPermiso.login(nm2, pd2, sistema)) {
                Messagebox.show(String.format("Usuario sin acceso al sistema"));
                return false;
            }
        }
        */
        //Messagebox.show(String.format("jksdhas####{username:{%s}-----password:{%s}", nm2,pd2));
/*
        if (!usrPermiso.login(nm2, pd2)) {
            Messagebox.show(String.format("Password Incorrecta"));
            //   messageiniciologin.setValue("Password Incorrecta.");
            return;

        }
         */
        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
        //UserCredential cre= authService.getUserCredential();
        // Executions.sendRedirect("/SysBoj/Principal");

        //guardamos session en base de datos
        //int keyId= _httpsession.setSession(permiso.getCuenta(),hses.getId(),"000.000.000");
//        String jsessionid = Executions.getCurrent().getHeader("cookie");
        String httpsession = hses.getId();
        int keyId = _httpsession.setSession(permiso.getCuenta(), httpsession, this.getRemoteIP(), jsessionid, 2);
        _httpsession.setSessionAccion("Inicio de Session SisBoj", keyId, 2);

        Executions.sendRedirect("/sisboj/index");
        Messagebox.setTemplate("../windowconfirm/windowconfirm.zul");
        return true;
    }

}
