/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
 */
package sisboj.esencial;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import sisboj.necesarias.AuthenticationService;
import java.util.Date;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.ClientInfoEvent;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;
import siscore.entidades.Sistema;
import javax.servlet.http.HttpServletRequest;
import org.zkoss.zul.Window;

public class LoginController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 1L;

    //wire components
    @Wire
    Textbox txtSBoj;
    @Wire
    Textbox account;
    @Wire
    Textbox password;
    @Wire
    Label messageiniciologin;
    @Wire
    Window winLoginboj;
    @Wire
    Textbox loginusername;
    @Wire
    Textbox loginpassword;
    @Wire
    Textbox loginpasswordboj;
    @Wire
    Textbox loginpasswordoper;
    @Wire
    Textbox loginpasswordcob;
    MvcConfig mmmm = new MvcConfig();
    //services
    AuthenticationService authService = new AuthenticationServiceChapter8Impl();
    UsuarioPermisoJDBC usrPermiso;
    HttpSessionJDBC _httpsession;
    Session sess;
    HttpSession hses;
    String sistema="Otro";

    public String getRemoteIP() {
        return remoteIP;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }
    String remoteIP;

    LdapController ldpCtrl = new LdapController();

    String userWin = System.getProperty("user.name");

    public LoginController() throws SQLException {
        this.setRemoteIP("0.0.0.0");
        // requestXX = new HttpServletRequest();

        sess = Sessions.getCurrent(true);
        hses = (HttpSession) sess.getNativeSession();

        usrPermiso = new MvcConfig().usuarioPermisoJDBC();
        _httpsession = new HttpSessionJDBC(mmmm.getDataSource());

        HttpServletRequest requestXX = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        this.setRemoteIP(requestXX.getRemoteAddr());
        // collectInfo();

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
//            doLoginDB2();

    }

    @Listen("onClick=#btn-loginDB; onOK=#btn-loginWin; onOK=#loginpassword")
    public void doLoginDB() {
        System.out.println("Entre a LoginController.doLoginDB linea 110");
        String nm2 = loginusername.getValue();
        String pd2 = loginpassword.getValue();
        
        if (!usrPermiso.login(nm2, pd2,sistema)) {
            Messagebox.show(String.format("Password Incorrecta"));
            return;

        }

        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
        List<Sistema> _sis = permiso.getSistema();

        if (_sis.size() < 1) {
            Messagebox.show("Usuario no Tiene Perfil en SISCORE");
            return;
        }

        if (permiso.isInvitado()) {
            Messagebox.show("Usuario no Tiene Perfil para SisCon");
            return;
        }

//if (_sis.toString().matches(".*\\bSISCON\\b.*"))
        String listastring = _sis.toString();
        if (_sis.toString().matches(".*\\bSISCON\\b.*")) {
            Messagebox.show("Usuario no Tiene Perfil en SISCON");
            return;
        }
//        collectInfo();

        //guardamos session en base de datos
        //int keyId= _httpsession.setSession(permiso.getCuenta(),hses.getId(),"000.000.000");
        String jsessionid = Executions.getCurrent().getHeader("cookie");
        String httpsession = hses.getId();
        int keyId = _httpsession.setSession(permiso.getCuenta(), httpsession, this.getRemoteIP(), jsessionid, 1);
        _httpsession.setSessionAccion("Inicio de Session SisCon", keyId, 2);
        //UserCredential cre= authService.getUserCredential();
        // Executions.sendRedirect("/SysBoj/Principal");
        Executions.sendRedirect("/siscon/index");
        Messagebox.setTemplate("../windowconfirm/windowconfirm.zul");

    }

    // aca inicia session buscando usuarios de sisboj
    @Listen("onLoad=#winLoginboj; onClick=#btn-loginboj; onOK=#btn-loginWin; onOK=#loginpasswordboj")
    public boolean doLoginDB2() throws SQLException {

        //boolean ldp = ldpCtrl.autenticacionLDAP("exvguba");
        boolean ldp=true;
        sistema="sisboj";
        String nm2 ="ucc";//userWin;//loginusername.getValue();
        String pd2 =""; //loginpassword.getValue();
        
         if(!ldp){
           Messagebox.show(String.format("Usuario no encontrado"));
           return false;
       }else{
            if (!usrPermiso.login(nm2, pd2,sistema)) {
                Messagebox.show(String.format("Usuario sinacceso al sistema"));
                //   messageiniciologin.setValue("Password Incorrecta.");
                return false;

            }
       }

        //Messagebox.show(String.format("jksdhas####{username:{%s}-----password:{%s}", nm2,pd2));
/*
        if (!usrPermiso.login(nm2, pd2)) {
            Messagebox.show(String.format("Password Incorrecta"));
            //   messageiniciologin.setValue("Password Incorrecta.");
            return;

        }
*/
        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
        //UserCredential cre= authService.getUserCredential();
        // Executions.sendRedirect("/SysBoj/Principal");

        //guardamos session en base de datos
        //int keyId= _httpsession.setSession(permiso.getCuenta(),hses.getId(),"000.000.000");
        String jsessionid = Executions.getCurrent().getHeader("cookie");
        String httpsession = hses.getId();
        int keyId = _httpsession.setSession(permiso.getCuenta(), httpsession, this.getRemoteIP(), jsessionid, 2);
        _httpsession.setSessionAccion("Inicio de Session SisBoj", keyId, 2);

        Executions.sendRedirect("/sisboj/index");
        Messagebox.setTemplate("../windowconfirm/windowconfirm.zul");
      return true;
    }

    @Listen("onClick=#btn-loginoper; onOK=#btn-loginWin; onOK=#loginpasswordoper")
    public void doLoginOper() {

        //  Messagebox.show("Autentixation SisOper");
        String nm2 = loginusername.getValue();
        String pd2 = loginpasswordboj.getValue();
        //Messagebox.show(String.format("jksdhas####{username:{%s}-----password:{%s}", nm2,pd2));

        if (!usrPermiso.login(nm2, pd2,sistema)) {
            Messagebox.show(String.format("Password Incorrecta"));
            //   messageiniciologin.setValue("Password Incorrecta.");
            return;

        }
        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
        //UserCredential cre= authService.getUserCredential();
        // Executions.sendRedirect("/SysBoj/Principal");

        //guardamos session en base de datos
        //int keyId= _httpsession.setSession(permiso.getCuenta(),hses.getId(),"000.000.000");
        String jsessionid = Executions.getCurrent().getHeader("cookie");
        String httpsession = hses.getId();
        int keyId = _httpsession.setSession(permiso.getCuenta(), httpsession, this.getRemoteIP(), jsessionid, 2);
        _httpsession.setSessionAccion("Inicio de Session SisOper", keyId, 2);

        Executions.sendRedirect("/sisoper/index");
        Messagebox.setTemplate("../windowconfirm/windowconfirm.zul");

    }

    @Listen("onClick=#btn-logincob; onOK=#btn-loginWin; onOK=#loginpasswordcob")
    public void doLoginCob() {

        //  Messagebox.show("Autentixation BOJ");
        String nm2 = loginusername.getValue();
        String pd2 = loginpasswordcob.getValue();
        //Messagebox.show(String.format("jksdhas####{username:{%s}-----password:{%s}", nm2,pd2));

        if (!usrPermiso.login(nm2, pd2,sistema)) {
            Messagebox.show(String.format("Password Incorrecta"));
            //   messageiniciologin.setValue("Password Incorrecta.");
            return;

        }
        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
        String jsessionid = Executions.getCurrent().getHeader("cookie");
        String httpsession = hses.getId();
        int keyId = _httpsession.setSession(permiso.getCuenta(), httpsession, this.getRemoteIP(), jsessionid, 2);
        _httpsession.setSessionAccion("Inicio de Session SisCob", keyId, 2);

        Executions.sendRedirect("/siscob/index");
        Messagebox.setTemplate("../windowconfirm/windowconfirm.zul");

    }

    private Textbox txtSessionInfo;
    private StringBuilder clientInfo = new StringBuilder();

    public void onClientInfo(ClientInfoEvent evt) {
        clientInfo.append("ZK ClientInfo: \r");
        clientInfo.append("getScreenWidth():\t\t").append(evt.getScreenWidth()).append(" x ").append(evt.getScreenHeight()).append("\r");
        clientInfo.append("getColorDepth():\t\t").append(evt.getColorDepth()).append("bit\r");
        clientInfo.append("getDesktopWidth():\t\t").append(evt.getDesktopWidth()).append(" x ").append(evt.getDesktopHeight()).append("\r");
        clientInfo.append("getTimeZone():\t\t\t").append(evt.getTimeZone().getDisplayName()).append("\r");
        clientInfo.append("getName():\t\t\t").append(evt.getName()).append("\r");
        clientInfo.append("--------------------------------------------------------------------------------------------------\r");

    }

    private void collectInfo() {
        //  RequestContextHolder nn= new RequestContextHolder;

        //String ipAddress = request.getRemoteAddr();
        StringBuilder result = new StringBuilder();
        String datos = null;
        try {

            datos = "Session\n"
                    + "getLocalAddr():" + sess.getLocalAddr() + "\n"
                    + "getLocalName():" + sess.getLocalName() + "\n"
                    + "getRemoteAddr():" + sess.getRemoteAddr() + "\n"
                    + "getRemoteHost():" + sess.getRemoteHost() + "\n"
                    + "getServerName():" + sess.getServerName() + "\n"
                    + "getWebApp().getAppName():" + sess.getWebApp().getAppName() + "\n";

            datos += "--------------------------------------------------------------------------------------------------\n"
                    + "HttpSession\n"
                    + ".getId():" + hses.getId() + "\n"
                    + ".getCreationTime():" + new Date(hses.getCreationTime()).toString() + "\n"
                    + ".getLastAccessedTime():" + new Date(hses.getLastAccessedTime()).toString() + "\n"
                    + "--------------------------------------------------------------------------------------------------\n";

            ServletContext sCon = hses.getServletContext();
            datos += "ServletContext\n"
                    + ".getServerInfo():" + sCon.getServerInfo() + "\n"
                    + ".getContextPath():" + sCon.getContextPath() + "\n"
                    + ".getServletContextName():" + sCon.getServletContextName() + "\n"
                    + "--------------------------------------------------------------------------------------------------\n"
                    + "ZK Executions\n"
                    + ".getHeader('user-agent'):" + Executions.getCurrent().getHeader("user-agent") + "\n"
                    + ".getHeader('accept-language'):" + Executions.getCurrent().getHeader("accept-language") + "\n"
                    + ".getHeader('referer'):" + Executions.getCurrent().getHeader("referer") + "\n"
                    + ".getHeader('connection'):" + Executions.getCurrent().getHeader("connection") + "\n"
                    + ".getHeader('zk-sid'):" + Executions.getCurrent().getHeader("zk-sid") + "\n"
                    + ".getHeader('origin'):" + Executions.getCurrent().getHeader("origin") + "\n"
                    + ".getHeader('host'):" + Executions.getCurrent().getHeader("host") + "\n"
                    + ".getHeader('cookie'):" + Executions.getCurrent().getHeader("cookie") + "\n"
                    + ".getHeader(\"X-FORWARDED-FOR\"):" + Executions.getCurrent().getHeader("X-FORWARDED-FOR") + "\n"
                    + ".ip:" + this.getRemoteIP() + "\n"
                    // + ".ip:" + requestXX.getHeader("X-FORWARDED-FOR")+ "\n"
                    + "--------------------------------------------------------------------------------------------------\r";

            Messagebox.show(datos);
            System.out.println(datos);

        } catch (Exception ex) {
            System.out.println(datos);
        }
    }
}
