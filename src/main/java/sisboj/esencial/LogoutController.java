/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
 */
package sisboj.esencial;

import config.MvcConfig;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import sisboj.necesarias.AuthenticationService;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;

public class LogoutController extends SelectorComposer<Component> {

    private static final long serialVersionUID = 1L;
    UsuarioPermisoJDBC usrPermiso;
    HttpSessionJDBC _httpsession;
    Session sess;
    HttpSession hses;
MvcConfig mmmm = new MvcConfig();
    public LogoutController() throws SQLException {
                sess = Sessions.getCurrent();
        hses=(HttpSession) sess.getNativeSession();
        
        usrPermiso = new MvcConfig().usuarioPermisoJDBC();
        _httpsession= new  HttpSessionJDBC(mmmm.getDataSource());
//        collectInfo();
        
    }
    
    
    
    
    //services
    AuthenticationService authService = new AuthenticationServiceChapter8Impl();

    @Listen("onClick=#logout")
    public void doLogout() {
        authService.logout();
        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos();
         String httpsession =hses.getId();
        int idsessionsiscon=_httpsession.GetSisConIdSession(httpsession);
        
         _httpsession.setSessionAccion("Fin de Session SisCon",idsessionsiscon,2);
        //_httpsession.setSessionAccion(hses.getId(), permiso.getCuenta(), "000.000.000");
      //          _httpsession.setSessionAccion("Inicio de Session SisCon",keyId,2);
        Sessions.getCurrent().invalidate();
        Executions.sendRedirect("/Login2");
    }
}
