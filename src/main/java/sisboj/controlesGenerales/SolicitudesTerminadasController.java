/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controlesGenerales;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Operaciones;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;


/**
 *
 * @author esilvestre
 */
@SuppressWarnings("serial")
public class SolicitudesTerminadasController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;

    @Wire
    Panel id_addDocPanel;

    @Wire
    Div id_detalleSolicitud;
    Window capturawin;
    @Wire
    Div _divFridDoc;

    @Wire
    private List<String> cmb_TipDcto;

    String ValueIdOperaciones;
    String unicacion;

//    @Wire
//    Button id_addDocumentos;
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private EventQueue eq;
    private int id_solicitud = 0;

    ///Informacion del Usuario
    String UsuarioCustodia;

    public SolicitudesTerminadasController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        Session sess = Sessions.getCurrent();

        capturawin = (Window) comp;
        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        String Account = permisos.getCuenta();//user.getAccount();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();
        // String rutcliente = Executions.getCurrent().getParameter("rutcliente");
        // String mm = rutcliente.replace(".", "");
        // String[] ParteEntera = mm.split("-");

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }
        /// cust ={1,2,3}  indica la pesta�a 
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {

                final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
//                int cust = Integer.parseInt(session.getAttribute("custodias").toString());

                //Qr Imagen 
                listSol = conexSol.getSolTerminadas( UsuarioCustodia);

                bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

                grid.setModel(bandejacustodia);
                grid.renderAll();

            }
        });

//        int cust = Integer.parseInt(session.getAttribute("custodias").toString());

        //Qr Imagen 
        this.listSol = conexSol.getSolTerminadas(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

    }

    public void updateGridDoc(int id_operacion, int countNumDoc) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);

        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);
//        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);

    }

//    public void updateDetalleGrid(int id_sol) {
//
//        conexDetSol = new MvcConfig().detSolicitudJDBC();
//        this.listDet = conexDetSol.listDetSolicitud(id_sol);
//        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
//        grid_DetSol.setModel(DetSolModelList);
//        grid_DetSol.setVisible(false);
//        grid_DetSol.setVisible(true);
//
//    }
//
//    public void setValuesDocs(int id_sol) {
//        boolean flag = false;
//        _divFridDoc.setVisible(false);
//        id_addDocPanel.setVisible(false);
//        BandejaDetalle detSeleccion = null;//=(BandejaDetalle)listDet.get(id_sol);
//
//        for (final BandejaDetalle banddettt : listDet) {
//            if (banddettt.getIdDetSol() == id_sol) {
//                detSeleccion = banddettt;
//            }
//        }
//
//        id_addDocPanel.setVisible(true);
//        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
//        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
//        txt_OperDcto.setValue(detSeleccion.getnOperacion());
////        id_addDocumentos.setVisible(true);
//
//        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
//            // Messagebox.show(detSeleccion.getnOperacion());
//            ope = _op.getOperaciones(detSeleccion.getnOperacion());
//            //  Messagebox.show(Integer.toString(ope.getDi_IdOperacion()));
//            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs());
//        }
//
//        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
//    }
    public void sendBackOffice(final int numerosolicitud) {

        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("solicitud", numerosolicitud);

        arguments.put("rut", 15.014544);
        String template = "boveda/EnvioBojPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
