/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controlesGenerales;

import config.MvcConfig;
import siscore.pruebas.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Operaciones;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;


/**
 *
 * @author esilvestre
 */
public class SolicituddesEnviadasController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;

    @Wire
    Panel id_addDocPanel;

    @Wire
    Div id_detalleSolicitud;

    @Wire
    Div _divFridDoc;

    @Wire
    private List<String> cmb_TipDcto;

    String ValueIdOperaciones;
    String unicacion;

    @Wire
    Button id_addDocumentos;

    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;

    private int id_solicitud = 0;

    ///Informacion del Usuario
    String UsuarioCustodia;

    public SolicituddesEnviadasController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();
        // String rutcliente = Executions.getCurrent().getParameter("rutcliente");
        // String mm = rutcliente.replace(".", "");
        // String[] ParteEntera = mm.split("-");

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }
        /// cust ={1,2,3}  indica la pesta�a 
//        int cust = Integer.parseInt(session.getAttribute("custodias").toString());
        // cust =3;
        //Qr Imagen 

        this.listSol = conexSol.getSolEnviadas(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();
        //this.updateGridDoc();
//        int cuantos = grid.getRows().getChildren().size();
//
//        for (Component row : grid.getRows().getChildren()) {
//            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
//
//                public void onEvent(Event arg0) throws Exception {
//                    Row row = (Row) arg0.getTarget();
//                    Row row3 = (Row) arg0.getTarget();
//                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();
//
//                    String value = banc.getProducto();
//                    Component c = row.getChildren().get(1);
//                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");
//                    //Capturo todos los atributos de llegada
//                    String nnnnn = row.getAttributes().toString();
//                    // Messagebox.show("Estos Son los Atributos Capturado:["+nnnnn+"] index:["+value+"]");
//                    List<BandejaDetalle> listDet1 = getListDet(banc.getNumsol());
//                    id_solicitud=banc.getNumsol();
//                    DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
//                    grid_DetSol.setModel(DetSolModelList);
//                    id_addDocPanel.setVisible(false);
//                    _divFridDoc.setVisible(false);
//                    id_detalleSolicitud.setVisible(true);
//                    if (rowSelected != null && rowSelected) {
//                        row.setAttribute("Selected", false);
//                        // row.setStyle("");
//                        row.setSclass("");
//                        //row.setc
//                    } else {
//                        for (Component rownn : grid.getRows().getChildren()) {
//                            rownn.getClass();
//                            Row nnn = (Row) rownn;
//                            nnn.setAttribute("Selected", false);
//                            nnn.setSclass("");
//                        }
//                        row.setAttribute("Selected", true);
//                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
//                        row.setSclass("z-row-background-color-on-select");         // sclass
//                    }
//                }
//            });
//        }

    }

    public void updateGridDoc(int id_operacion, int countNumDoc) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);

        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);
        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);

    }

    public void updateDetalleGrid(int id_sol) throws SQLException {

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
        grid_DetSol.setVisible(false);
        grid_DetSol.setVisible(true);

    }

    /*public void replaceRow(){
                            conexDetSol = new MvcConfig().detSolicitudJDBC();
         this.listDet = conexDetSol.listDetSolicitud(1);
                    DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
			Map datas = new HashMap();
			datas.put("data", listDet);		
			Rows rows = grid_DetSol.getRows();
			rows.getChildren().clear();
			Executions.createComponents("boveda/UpdateRowDetSolicitud.zul", rows, datas);
			grid_DetSol.invalidate();
                        grid_DetSol.renderAll();
		}*/
    public void setValuesDocs(int id_sol) throws SQLException {
        boolean flag = false;
        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);
        BandejaDetalle detSeleccion = null;//=(BandejaDetalle)listDet.get(id_sol);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        id_addDocumentos.setVisible(true);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            // Messagebox.show(detSeleccion.getnOperacion());
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            //  Messagebox.show(Integer.toString(ope.getDi_IdOperacion()));
            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs());
        }

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setValuesDocsNew(int id_sol, int countNumDoc) throws SQLException {
        boolean flag = false;

        //Messagebox.show("Numerode Documentos["+countNumDoc+"]");
        numerodocsOperActual = countNumDoc;
        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);
        BandejaDetalle detSeleccion = null;//=(BandejaDetalle)listDet.get(id_sol);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            // Messagebox.show(detSeleccion.getnOperacion());
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            //  Messagebox.show(Integer.toString(ope.getDi_IdOperacion()));
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc);
        }

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setButtonVisible() {

        id_addDocumentos.setVisible(true);

    }

    public String setButtonNoVisible() {

        id_addDocumentos.setVisible(false);
        return "WENAAAA";
    }

    public void setValuesDocsComplete(int id_sol) throws SQLException {
        boolean flag = false;
        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);
        BandejaDetalle detSeleccion = null;//=(BandejaDetalle)listDet.get(id_sol);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        id_addDocumentos.setVisible(false);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            // Messagebox.show(detSeleccion.getnOperacion());
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            //  Messagebox.show(Integer.toString(ope.getDi_IdOperacion()));
            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs());
        }

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();

        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        return listDet;
    }

    @Listen("onClick = #_buttonEnviarBoj")
    public void envio_boj() {
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_ubicaciondoc", 54545);

        arguments.put("rut", 15.014544);
        String template = "boveda/EnvioBojPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  } else {
        //     Messagebox.show("Please read the terms of service and accept it before you submit the order.");
        //  }
    }

    public void sendBackOffice(final int numerosolicitud) {

        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_solicitud", numerosolicitud);

        arguments.put("rut", 15.014544);
        String template = "boveda/EnvioBojPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                if (conexSol.EnviarBojSolicitud(numerosolicitud)) {
                    Messagebox.show("Se Envio correctamente la Solicitud ", "Enviar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                                //OK is clicked
                                // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);

                                //  Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                                //  Events.postEvent("onRefresh", self, null); 
                                // self.detach();   
                                //resultWin.detach();
                                //  Executions.sendRedirect("/sisboj/index");
                            } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                //Cancel is clicked
                                // Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                                //((Div)((Window)resultWin.getParent()).getFellow("_divFridDoc")).setVisible(true);
                                // txt_hiddenIdExito.setValue("false");
                                // resultWin.detach();
                                // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                            }
                        }
                    }
                    );
                } else {
                    Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                                //OK is clicked
                                // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                Executions.sendRedirect("/sisboj/index");
                            } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                //Cancel is clicked
                                // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                            }
                        }
                    }
                    );

                }
                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
