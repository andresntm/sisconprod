package sisboj.controlesGenerales;

import config.MvcConfig;
import java.sql.SQLException;
import org.zkoss.chart.Charts;
import org.zkoss.chart.Legend;
import org.zkoss.chart.PlotLine;
import org.zkoss.chart.model.CategoryModel;
import org.zkoss.chart.model.DefaultCategoryModel;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Grid;

import siscore.genral.Informes;
import siscore.genral.InformesImpl;

public class BandEntrController extends SelectorComposer<Component> {
        
    @Wire
    Grid BanEntr;
    @Wire
    Charts chart;
    //ListModelList<Contacto> myListModel;

    
    
    MvcConfig mmmm = new MvcConfig();
   
    
   // final ContactoDAO contactDAO;
    
    final Informes informe;
      CategoryModel model;
      
     
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        // List<Contacto> listContact = contactDAO.list();
          model = new DefaultCategoryModel();
          model=informe.getModelInformes("fdfsd");
         
       //  System.out.println("#------------@@@@Exito Implantacion de BEans["+listContact.get(0).getName().toString()+"]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
       
         System.out.println("#------------@@@@Exito Trayendo el MODEL["+model.getCategories().toString()+"]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        chart.setModel(model);
        
        chart.getTitle().setX(-20);
        chart.setTitle("Informe Total Metropolitana Cordillera");
        chart.setSubtitle("Periodo 20-12-2016");
        chart.getSubtitle().setX(-20);
    
        chart.getYAxis().setTitle("Porcentaje(%)");
        PlotLine plotLine = new PlotLine();
        plotLine.setValue(0);
        plotLine.setWidth(1);
        plotLine.setColor("#808080");
        chart.getYAxis().addPlotLine(plotLine);

        chart.getTooltip().setValueSuffix("Porcentaje(%)");

        Legend legend = chart.getLegend();
        legend.setLayout("vertical");
        legend.setAlign("right");
        legend.setVerticalAlign("middle");
        legend.setBorderWidth(0);
         
         
         
         //myListModel = new ListModelList<Contacto>(listContact);
        
        //BanEntr.setModel(myListModel);         

//get data from service and wrap it to list-model for the view
     /*   List<Mail> mList = new ArrayList<Mail>();
        if (!mList.isEmpty())
			mList.clear();
        mList.add(new Mail(1, "zk Spreadsheet RC Released Check this out", "2010/10/17 20:37:12", 24));
        mList.add(new Mail(2, "[zk 5 - Help] RE: SelectedItemConverter Question 3", "2010/10/17 18:31:12", 24));
        mList.add(new Mail(3, "[zk 5 - Help] RE: SelectedItemConverter Question 2", "2010/10/17 17:30:12", 24));
        mList.add(new Mail(4, "Times_Series Chart help", "2010/10/17 15:26:37", 24));
        mList.add(new Mail(5, "RE: Times_Series Chart help", "2010/10/17 14:22:37", 24));
        mList.add(new Mail(6, "RE: Times_Series Chart help(Updated)", "2010/10/17 13:26:37", 24));
        mList.add(new Mail(7, "[zk 5 - General] Grid Rendering problem", "2010/10/17 10:41:33", 24));
        mList.add(new Mail(8, "[zk 5 - Help] RE: SelectedItemConverter Question", "2010/10/17 10:14:27", 24));
        mList.add(new Mail(9, "[Personal] RE: requirement of new project", "2010/10/16 13:34:37", 24));		
        mList.add(new Mail(10, "[zk 3 - Feature] Client programming Question", "2010/10/15 04:31:12", 24));		
        mList.add(new Mail(11, "[zk 5 - Feature] Hlayout/Vlayout Usage", "2010/10/15 04:31:12", 24));
        mList.add(new Mail(12, "RE: Times_Series Chart help(Updated)", "2010/10/15 03:26:37", 24));
        mList.add(new Mail(13, "[zk 3 - Feature] JQuery support", "2010/10/14 04:31:12", 24));
        mList.add(new Mail(14, "[zk 5 - Help] RE: Times_Series Chart help", "2010/10/14 02:43:34", 24));
        mList.add(new Mail(15, "[Personal] requirement of new project", "2010/10/14 02:44:35", 24));
        mList.add(new Mail(16, "[zk 5 - Help] RE: SelectedItemConverter Question", "2010/10/13 02:14:27", 24));
        myListModel = new ListModelList<Mail>(mList);
        BanEntr.setModel(myListModel);*/
    }

    
       public BandEntrController() throws SQLException {
//        this.contactDAO = mmmm.getContactoDAO();
        this.informe=new InformesImpl(mmmm.getDataSourceLucy());
    }
    @Listen("onClick = #BanEntr > rows > row")
    public void click(MouseEvent event) {
        Clients.showNotification("Row selected!");
    }


}
