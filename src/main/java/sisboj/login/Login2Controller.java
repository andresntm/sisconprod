/* BooksController.java

	Purpose:
		
	Description:
		
	History:
		5:53 PM 11/9/15, Created by jumperchen

Copyright (C) 2015 Potix Corporation. All Rights Reserved.
 */
package sisboj.login;

import config.MvcConfig;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import sisboj.esencial.AuthenticationServiceChapter8Impl;
import sisboj.necesarias.AuthenticationService;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;

/**
 * @author
 */
@Controller
@RequestMapping(value = {"/Login2", "/AA", "/AutoLogin", "/sisboj","/sisoper","/siscob"})
//@SessionAttributes({"booksVM"})
public class Login2Controller {

    //wire components
    @Wire
    Textbox account;
    @Wire
    Textbox password;
    @Wire
    Label message;

    //services
    AuthenticationService authService = new AuthenticationServiceChapter8Impl();
    UsuarioPermisoJDBC usrPermiso;

    public Login2Controller() throws SQLException {
        usrPermiso = new MvcConfig().usuarioPermisoJDBC();
        
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String login2(ModelMap model) {
        Sessions.getCurrent();

        //model.get
        System.err.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        return "Login/index2.zul";

    }

    @RequestMapping(value = {"A", "/A"}, method = RequestMethod.GET)
    public String LoginGantt(ModelMap model) {
        Sessions.getCurrent();

        System.err.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        return "project/gantt/index.zul";

    }

    @RequestMapping(value = {"auth", "/auth"}, method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView test(HttpServletRequest request) {
        //return "hi this is a test";
        Map<String, String[]> parameters = request.getParameterMap();
        String userName = "";
        String userPass = "";

        for (String key : parameters.keySet()) {
            System.out.println(key);
            if (key.equals("user_name")) {
                //userName=key;
                String[] vals = parameters.get(key);

                for (String val : vals) {
                    userName = val;
                    System.out.println(" -> " + val);
                }
            }

            if (key.equals("password")) {
                // userName=key;
                String[] vals = parameters.get(key);

                for (String val : vals) {
                    userPass = val;
                    System.out.println(" -> " + val);
                }
            }

        }

        if (!usrPermiso.login(userName, userPass, request.getSession(true))) {

            ModelAndView mv = new ModelAndView();
            mv.setViewName("/Error/index.zul");
            return mv;
        }
        
        
        

        UsuarioPermiso permiso = usrPermiso.getUsuarioPermisos(request.getSession(true));

        //  Messagebox.show;
        ModelAndView mv = new ModelAndView(new RedirectView("/siscore/siscon/index"));

        return mv;

    }

    @RequestMapping(value = {"ZZ", "/ZZ"}, method = RequestMethod.POST)
    public ModelAndView testZ(HttpServletRequest request, HttpServletResponse response) {
        //return "hi this is a test";

        String userName = request.getParameter("data[Admin][user_name]");
        String userPass = request.getParameter("data[Admin][password]");

        Messagebox.show("LEO PARAMETRO FOR LOGIN + userName={" + userName + "} userPass = {" + userPass + "}");

        /*  int userId=userDAO.getUser(userName, userPass);
      if(userId!=0){  
        String message = "welcome!!!"; 
        return new ModelAndView("result", "message", message);  
      }  
      else{  
        String message = "fail";
        return new ModelAndView("index", "message",message);  
      } 
  
         */
        this.message.setValue("sdasdasdsa");
        return new ModelAndView("project/gantt/index.zul", "message", message);
    }

    @RequestMapping(value = {"Login", "/Login"}, method = RequestMethod.GET)
    public String login_boj(ModelMap model) {
        Sessions.getCurrent();
      System.out.println("os.name: "+System.getProperty("os.name"));
       System.out.println("####################### ---  USER: "+System.getProperty("user.name"));
       
        Map<String, String> map = System.getenv();
       
   for (String key: map.keySet()){
    System.out.println("KDJHKLDJHKLDJKDLJD####"+key);
}
    for (Map.Entry<String, String> entry : map.entrySet()) {
    System.out.println(entry.getKey() + "/" + entry.getValue());
}
    //   map.entrySet().forEach(System.out::println);
     // Messagebox.show("[["+System.getProperty("os.name")+"]]");
        //model.get
        System.err.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        return "Login/indexboj.zul";

    }
    
    
    
  @RequestMapping(value = {"Loginx", "/Loginx"}, method = RequestMethod.GET)
  public String printUser(ModelMap model) {
 
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      String name = auth.getName(); //get logged in username
		
      model.addAttribute("username", name);
      return "hello";
 
  }
    
    
        @RequestMapping(value = {"LoginOper", "/LoginOper"}, method = RequestMethod.GET)
    public String login_Oper(ModelMap model) {
        Sessions.getCurrent();

        //model.get
        System.err.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        return "Login/indexoper.zul";

    }
    
    
    
        @RequestMapping(value = {"LoginCob", "/LoginCob"}, method = RequestMethod.GET)
    public String login_cob(ModelMap model) {
        Sessions.getCurrent();

        //model.get
        System.err.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        return "Login/indexcob.zul";

    }
    
    

 
    @RequestMapping(value = {"Loginz", "/Loginz"}, method = RequestMethod.GET)
    @ResponseBody
    public String currentUserName(Principal principal) {
        return principal.getName();
    }

    @RequestMapping(value = {"Loginb", "/Loginb"}, method = RequestMethod.GET)
    @ResponseBody
    public String currentUserNameSimple(HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        return principal.getName();
    } 
   
    
    
   /*s @RequestMapping(value = "/headerTest", method = RequestMethod.GET)
        public @ResponseBody DummyAPI dummyApi(@RequestHeader ("User-Agent") String userAgent,HttpServletRequest request,  HttpServletResponse response)
{
    System.out.println("User-Agent : " + userAgent);

    String browserName = request.getHeader("User-Agent");
    System.out.println("BrowserName : " + browserName);

    return "test";
}*/

}
