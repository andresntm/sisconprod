/* BooksController.java
	Purpose:		
	Description:		
	History:
        5:53 PM 11/9/15, Created by jumperchen
Copyright (C) 2015 Potix Corporation. All Rights Reserved.
 */
package sisboj.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

/**
 * @author jumperchen
 */
@Controller
@RequestMapping(value = {"/Login", "", "/"})
//@SessionAttributes({"booksVM"})
public class LoginController {

    //wire components
    @Wire
    Textbox account;
    @Wire
    Textbox password;
    @Wire
    Label message;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String index(ModelMap model) {
        Sessions.getCurrent();
        System.out.println("::::::::::::::::::::::::::::PASE POR ACA => sisboj.login.LoginController:::::::::::::::::::::::::::::::::");
        System.err.print("\n\n\n\t\t--- {" + Sessions.getCurrent() + "} ---\n\n\n");

        return "Login/index2.zul";
        //return "Login/indexboj.zul";

    }

}
