/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Solicitud;


/**
 *
 * @author excosoc
 */
public class SolicitudMapper implements RowMapper<Solicitud>{

    public Solicitud mapRow(ResultSet rs, int rowNum) throws SQLException {
        Solicitud sol = new Solicitud();
        
        sol.setDi_IdSolicitud(rs.getInt("di_IdSolicitud"));
        sol.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
        sol.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
        sol.setDi_fk_IdHito(rs.getInt("di_fk_IdHito"));
        sol.setDdt_FechaCreacion(rs.getString("ddt_FechaCreacion"));
        sol.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
        //sol.setDi_cantOpeInh(rs.getInt("CANTIDADINH"));
        //sol.setDi_fk_Prod(rs.getInt("di_fk_Produc"));
        
        return sol;
    }
    
}
