/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.SetDocumentosEnt;

/**
 *
 * @author EXVGUBA
 */
public class DetSetDocumentosMapper implements RowMapper<SetDocumentosEnt>{

    public SetDocumentosEnt mapRow(ResultSet rs, int rowNum) throws SQLException {
        SetDocumentosEnt dstd = new SetDocumentosEnt();
        dstd.setDi_IdSetDoc(rs.getInt("di_IdSetDoc"));
        dstd.setDv_NomSetDoc(rs.getString("dv_NomSetDoc"));
        dstd.setDdt_FechaCrea(rs.getDate("ddt_FechaCrea"));
        dstd.setDdt_FechaMod(rs.getDate("ddt_FechaMod"));
        dstd.setDv_Descripcion(rs.getString("dv_Descripcion"));
        dstd.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
        dstd.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
        dstd.setDi_fk_IdTipDcto(rs.getInt("di_IdTipDcto"));
        return dstd;

    }
}
