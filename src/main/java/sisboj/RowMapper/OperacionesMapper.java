/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Operaciones;

/**
 *
 * @author excosoc
 */
public class OperacionesMapper implements RowMapper<Operaciones>{

    public Operaciones mapRow(ResultSet rs, int rowNum) throws SQLException {
        Operaciones oper = new Operaciones();
        oper.setDi_IdOperacion(rs.getInt("di_IdOperacion")); 
        oper.setDv_CodOperacion(rs.getString("dv_CodOperacion")); 
        oper.setDv_CodOperRelacionada(rs.getString("dv_CodOperRelacionada"));
        oper.setDi_fk_IdCliente(rs.getInt("di_fk_IdCliente")); 
        oper.setDi_fk_IdProduc(rs.getInt("di_fk_IdProduc"));
        oper.setDi_IdEstado(rs.getInt("di_IdEstado"));
        oper.setDi_DiasMora(rs.getInt("di_DiasMora"));
        oper.setDm_SaldoInsoluto(rs.getBigDecimal("dm_SaldoInsoluto"));
        oper.setDf_SaldoInsoUF(rs.getFloat("df_SaldoInsoUF"));
        oper.setDv_TipoMoneda(rs.getString("dv_TipoMoneda"));
        oper.setDv_fk_IdTipOpe(rs.getString("dv_fk_IdTipOpe"));
        oper.setDi_fk_IdOficina(rs.getInt("di_fk_IdOficina"));
        oper.setDi_fk_IdCedente(rs.getInt("di_fk_IdCedente"));
        oper.setDv_fk_TipRegion(rs.getString("dv_fk_TipRegion"));
        oper.setDdt_FechVencAct(rs.getString("ddt_FechVencAct"));
        oper.setDdt_FechCurse(rs.getString("ddt_FechCurse"));
        oper.setDdt_FechUltimoPago(rs.getString("ddt_FechUltimoPago"));
        oper.setDv_fk_IdBanca(rs.getString("dv_fk_IdBanca"));
        oper.setDv_ApodoBanca(rs.getString("dv_ApodoBanca"));
        oper.setDv_Garantia(rs.getString("dv_Garantia"));
        oper.setDv_Origen(rs.getString("dv_Origen"));
        oper.setDf_DtdD00(rs.getFloat("df_DtdD00"));
        oper.setDv_ReneNza(rs.getString("dv_ReneNza"));
        oper.setDv_EjecutivoReneNza(rs.getString("dv_EjecutivoReneNza"));
        oper.setDi_SucursalNova(rs.getInt("di_SucursalNova"));
        oper.setDi_CanalVentaNova(rs.getInt("di_CanalVentaNova"));
        oper.setDv_CodTipCreditoNova(rs.getString("dv_CodTipCreditoNova"));
        oper.setDv_4012Nova(rs.getString("dv_4012Nova"));
        oper.setDi_FaseNova(rs.getInt("di_FaseNova"));
        oper.setDv_CodeBci(rs.getString("dv_CodeBci"));
        oper.setDv_CodeGlosaBci(rs.getString("dv_CodeGlosaBci"));
        oper.setDdt_FechaIngreso(rs.getString("ddt_FechaIngreso"));
     //   oper.setCantidadEsperadaRegistros(rs.getInt("di_CantDctosEsperados"));
        return oper;
   }
}
