/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.ProductosTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class ProductosTblHitosMapper implements RowMapper<ProductosTblHitosEnt>{
    
    public ProductosTblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        
//        System.out.println("TblHitosMapper.mapRow estoy aqui 1");
        ProductosTblHitosEnt productos = new ProductosTblHitosEnt();
        productos.setProducto(rs.getString("producto"));
       
        
//        System.out.println("TblHitosMapper.mapRow estoy aqui 2");
        return productos;
    }
    
}
