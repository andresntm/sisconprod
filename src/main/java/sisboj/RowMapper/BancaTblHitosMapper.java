/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.BancaTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class BancaTblHitosMapper implements RowMapper<BancaTblHitosEnt>{
    
    public BancaTblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        
//        System.out.println("TblHitosMapper.mapRow estoy aqui 1");
        BancaTblHitosEnt banca = new BancaTblHitosEnt();
        banca.setBanca(rs.getString("banca"));
       
        
//        System.out.println("TblHitosMapper.mapRow estoy aqui 2");
        return banca;
    }
    
}
