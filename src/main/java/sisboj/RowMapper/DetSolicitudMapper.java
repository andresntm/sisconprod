/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.DetSolicitud;


/**
 *
 * @author excosoc
 */
public class DetSolicitudMapper implements RowMapper<DetSolicitud>{

    @Override
    public DetSolicitud mapRow(ResultSet rs, int rowNum) throws SQLException {
        DetSolicitud dSol = new DetSolicitud();
       
        dSol.setDi_IdDetSol(rs.getInt("di_IdDetSol"));
        dSol.setDi_fk_IdSolicitud(rs.getInt("di_fk_IdSolicitud"));
        dSol.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
        dSol.setDi_fk_IdDetHito(rs.getInt("di_fk_IdDetHito"));
        dSol.setDi_fk_IdDcto(rs.getInt("di_fk_IdDcto"));
        dSol.setDi_fk_IdOper(rs.getInt("di_fk_IdOper"));
        dSol.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
        dSol.setDv_EstadoActual(rs.getString("dv_EstadoActual"));
        //dSol.setContador_documentos(rs.getInt("NumeroDoctos"));
        return dSol;
    }
    
}
