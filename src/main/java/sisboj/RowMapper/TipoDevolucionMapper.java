/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.TipoDevolucionEnt;

/**
 *
 * @author exvguba
 */
public class TipoDevolucionMapper implements RowMapper<TipoDevolucionEnt> {

    public TipoDevolucionEnt mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoDevolucionEnt td = new TipoDevolucionEnt();
        td.setDi_IdTipoDev(rs.getInt("di_IdTipoDev"));
        td.setDv_NomTipoDev(rs.getString("dv_NomTipoDev"));
        td.setDv_CodTipoDev(rs.getString("dv_CodTipoDev"));
        td.setDv_Descripcion(rs.getString("dv_Descripcion"));
        td.setDdt_FechaCreacion(rs.getDate("ddt_FechaCreacion"));
        td.setDdt_FechaActualiza(rs.getDate("ddt_FechaActualiza"));
        td.setBit_Activo(rs.getInt("bit_Activo"));

        return td;
    }

}
