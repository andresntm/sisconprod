/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.TblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class TblHitosMapper implements RowMapper<TblHitosEnt>{
    
    public TblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        
//        System.out.println("TblHitosMapper.mapRow estoy aqui 1");
        TblHitosEnt tlbHitos = new TblHitosEnt();
        tlbHitos.setRut_completo(rs.getString("rut_completo"));
        tlbHitos.setFecha_ejecucion(rs.getDate("fecha_ejecucion"));
        tlbHitos.setUbicacion(rs.getString("ubicacion"));
        tlbHitos.setOficina(rs.getString("oficina"));
        tlbHitos.setN_of(rs.getInt("n_of"));
        tlbHitos.setReg(rs.getString("reg"));
        tlbHitos.setOp_orig(rs.getString("op_orig"));
        tlbHitos.setOperacion2(rs.getString("operacion_2"));
        tlbHitos.setTipo_op(rs.getString("tipo_op"));
        tlbHitos.setMonto(rs.getDouble("monto"));
        tlbHitos.setBanca(rs.getString("banca"));        
        tlbHitos.setRut(rs.getLong("rut"));
        tlbHitos.setDv(rs.getString("dv"));
        tlbHitos.setNombre(rs.getString("nombre"));
        tlbHitos.setFan(rs.getDate("fan"));
        tlbHitos.setFcurse(rs.getString("fcurse"));
        tlbHitos.setDias_mora(rs.getInt("dias_mora"));
        tlbHitos.setCode(rs.getString("code"));
        tlbHitos.setGlosa(rs.getString("glosa"));
        tlbHitos.setReneg_por_nza(rs.getString("reneg_por_nza"));
        tlbHitos.setEjecutivo_neg(rs.getString("ejecutivo_neg"));
        tlbHitos.setPrioridad(rs.getInt("prioridad"));
        tlbHitos.setNom_banca(rs.getString("nom_banca"));
        tlbHitos.setProducto(rs.getString("producto"));
        tlbHitos.setSolicitud(rs.getString("solicitud"));
        tlbHitos.setNombre_region(rs.getString("nombre_region"));        
        tlbHitos.setPrioridad_pyme(rs.getString("prioridad_pyme"));
        tlbHitos.setUsuario_genera(rs.getString("usuario_genera"));
        tlbHitos.setHito_especial(rs.getInt("hito_especial"));
        tlbHitos.setAcelerado(rs.getInt("acelerado"));
        tlbHitos.setEstado(rs.getString("estado"));
        return tlbHitos;
    }
    
}
