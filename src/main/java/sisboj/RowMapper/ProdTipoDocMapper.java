/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.ProdTipoDoc;

/**
 * ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo
 * @author exvguba
 */
public class ProdTipoDocMapper implements RowMapper<ProdTipoDoc> {
    public ProdTipoDoc mapRow(ResultSet rs, int rowNum) throws SQLException {
        ProdTipoDoc ptd = new ProdTipoDoc();
        ptd.setId_ProdTipDoc(rs.getInt("id_prodtipdoc"));
        ptd.setDv_NomProduc(rs.getString("dv_NomProduc"));
        ptd.setDi_IdProduc(rs.getInt("di_IdProduc"));
        ptd.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
        ptd.setDi_IdTipDcto(rs.getInt("di_IdTipDcto"));
        ptd.setBit_activo(Integer.toString(rs.getInt("bit_Activo")));
        ptd.setObligatorio(String.valueOf(rs.getInt("obligatorio")));
        return ptd;
    }

}
