/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Estado;


/**
 *
 * @author excosoc
 */
public class EstadoMapper implements RowMapper<Estado>{

    public Estado mapRow(ResultSet rs, int rowNum) throws SQLException {
        Estado est = new Estado();
        
        /*
            private int di_IdEstado;
            private String dv_NomEstado;
            private String dv_DetEstado;
            private String dv_CodEstado;
            private String dv_Lugar;
        */
        
        est.setDi_IdEstado(rs.getInt("di_IdEstado"));
        est.setDv_NomEstado(rs.getString("dv_NomEstado"));
        est.setDv_DetEstado(rs.getString("dv_DetEstado"));
        est.setDv_CodEstado(rs.getString("dv_CodEstado"));
        est.setDv_Lugar(rs.getString("dv_Lugar"));
        return est;
    }
    
}
