/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.BandejaCustodia;

/**
 *
 * @author excosoc
 */
public class principalMapper implements RowMapper<BandejaCustodia> {

    public BandejaCustodia mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaCustodia sol = new BandejaCustodia();

        sol.setHito(rs.getString("di_IdHito"));
        sol.setNumDctos(rs.getInt("cantidad"));
        sol.setNumsol(rs.getInt("di_IdSolicitud"));
        sol.setEstado(rs.getString("dv_NomEstado"));
        sol.setProducto(rs.getString("dv_NomProduc"));
        sol.setUbacion(rs.getString("dv_DetUbi"));
        sol.setFechaRegistro(rs.getString("ddt_fechaIngreso"));
        sol.setFechaCierre(rs.getString("ddt_FechaCierre"));
        sol.setFechaEnvio(rs.getString("ddt_FechaEnvio"));
        sol.setCodeBar(rs.getString("dv_CodeBar"));
        sol.setNumOpeInh(rs.getInt("CANTIDADINH"));
        return sol;
    }
}
