/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.RegionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class RegionTblHitosMapper implements RowMapper<RegionTblHitosEnt>{
    
    public RegionTblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        
        RegionTblHitosEnt region = new RegionTblHitosEnt();
        region.setReg(rs.getString("reg"));
        return region;
    }
    
}
