/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Documento;


/**
 *
 * @author excosoc
 */
public class DocumentoMapper implements RowMapper<Documento>{

    public Documento mapRow(ResultSet rs, int rowNum) throws SQLException {
        Documento doc = new Documento();
            
        doc.setDi_IdDcto(rs.getInt("di_IdDcto"));
        doc.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
        doc.setDi_fk_TipoDcto(rs.getInt("di_fk_TipoDcto"));
        doc.setDdt_FechaSalidaBov(rs.getString("ddt_FechaSalidaBov"));
        doc.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
        doc.setDv_CodBarra(rs.getString("dv_CodBarra"));
        doc.setDi_fk_IdOper(rs.getInt("di_IdOperacion"));
        doc.setDb_checkOk(rs.getBoolean("db_checkOk"));
        doc.setDv_Glosa(rs.getString("dv_Glosa"));
        doc.setDb_ApoderadoBanco(rs.getBoolean("db_ApoderadoBanco"));
        doc.setEstadoGlosa(rs.getString("dv_NomEstado"));
        doc.setTipoDocumentoGlosa(rs.getString("dv_NomTipDcto"));
        doc.setCodigoOperacion(rs.getString("dv_CodOperacion"));
        return doc;
    }
    
    
    
}
