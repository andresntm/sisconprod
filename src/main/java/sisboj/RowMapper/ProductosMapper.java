/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Estado;
import sisboj.entidades.Productos;


/**
 *
 * @author excosoc
 */
public class ProductosMapper implements RowMapper<Productos>{

    public Productos mapRow(ResultSet rs, int rowNum) throws SQLException {
        Productos prod = new Productos();
        prod.setDi_IdProducto(rs.getInt("di_IdProduc"));
        prod.setDv_NomProducto(rs.getString("dv_NomProduc"));
        prod.setDv_CodProducto(rs.getString("dv_CodProducto"));
        prod.setDv_Activo(Integer.toString(rs.getInt("dv_Activo")));       
        return prod;
    }
    
}
