/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.UsuariosLdapEnt;

/**
 *
 * @author EXVGUBA
 */
 public class UsuariosLdapMapper implements RowMapper<UsuariosLdapEnt>{

    public UsuariosLdapEnt mapRow(ResultSet rs, int i) throws SQLException {
          UsuariosLdapEnt usuLdap = new UsuariosLdapEnt();
        usuLdap.setId(rs.getLong("id"));
        usuLdap.setIp(rs.getString("ip"));
        usuLdap.setUsuario(rs.getString("usuario"));
        return usuLdap;
    }
    
}
