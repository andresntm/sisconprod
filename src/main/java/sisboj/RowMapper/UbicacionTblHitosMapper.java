/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.UbicacionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class UbicacionTblHitosMapper implements RowMapper<UbicacionTblHitosEnt>{
    
    public UbicacionTblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        UbicacionTblHitosEnt ubicaciontlbHitos = new UbicacionTblHitosEnt();
        ubicaciontlbHitos.setUbicacion(rs.getString("ubicacion"));      
        return ubicaciontlbHitos;
    }
    
}
