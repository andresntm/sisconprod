/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.MotivoDevolucionEnt;

/**
 *
 * @author EXVGUBA
 */
public class MotivoDevolucionMapper implements RowMapper<MotivoDevolucionEnt> {
    public MotivoDevolucionEnt mapRow(ResultSet rs, int rowNum) throws SQLException {

        MotivoDevolucionEnt md = new MotivoDevolucionEnt();
        md.setDi_IdMotivoDev(rs.getInt("di_IdMotivoDev"));
        md.setDv_NomMotivoDev(rs.getString("dv_NomMotivoDev"));
        md.setDv_CodMotivoDev(rs.getString("dv_CodMotivoDev"));
        md.setDi_fk_TipoDev(rs.getInt("di_fk_TipoDev"));
        md.setDv_Descripcion(rs.getString("dv_Descripcion"));
        //md.setDdt_FechaCreacion(rs.getDate("ddt_FechaCreacion"));
        md.setDdt_FechaActualiza(rs.getDate("ddt_FechaActualiza"));
        md.setBit_Activo(rs.getInt("bit_Activo"));
        return md;
    }

}
