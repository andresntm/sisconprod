/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.RutasLdap;


/**
 *
 * @author excosoc
 */
public class RutasLdapMapper implements RowMapper<RutasLdap>{

    public RutasLdap mapRow(ResultSet rs, int rowNum) throws SQLException {
        RutasLdap rts = new RutasLdap();
           rts.setId(rs.getInt("id"));
           rts.setDir(rs.getString("dir"));
           rts.setDescripcion(rs.getString("descripcion"));
       
        return rts;
    }
    
    
    
}
