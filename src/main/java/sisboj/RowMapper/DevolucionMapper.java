/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.DevolucionEnt;

/**
 *
 * @author exvguba
 */
public class DevolucionMapper implements RowMapper<DevolucionEnt> {

    public DevolucionEnt mapRow(ResultSet rs, int rowNum) throws SQLException {
        DevolucionEnt dev = new DevolucionEnt();

        dev.setDi_fk_IdOper(rs.getInt("di_fk_IdOper"));
        dev.setDi_fk_IdTipoDev(rs.getInt("di_fk_IdTipoDev"));
        dev.setDi_fk_IdMotivoDev(rs.getInt("di_fk_IdMotivoDev"));
        dev.setDi_fk_IdSolOrigen(rs.getInt("di_fk_IdSolOrigen"));
        dev.setDi_fk_IdSolActual(rs.getInt("di_fk_IdSolActual"));
        dev.setDi_fk_IdEstadoDev(rs.getInt("di_fk_IdEstadoDev"));
        dev.setDdt_FechaCreacion(rs.getDate("ddt_FechaCreacion"));
        dev.setDdt_FechaActualiza(rs.getDate("ddt_FechaActualiza"));
        dev.setDv_ObsOperacion(rs.getString("dv_ObsOperacion"));
        dev.setDi_fk_IdUbiOrigen(rs.getInt("di_fk_IdUbiOrigen"));
        dev.setDi_fk_IdUbiDestino(rs.getInt("di_fk_IdUbiDestino"));
        dev.setDv_ObsSolicitud(rs.getString("dv_ObsSolicitud"));
        return dev;
    }

}
