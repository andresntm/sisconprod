/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.BandejaDetalle;


/**
 *
 * @author excosoc
 */
public class bandDetMapper implements RowMapper<BandejaDetalle> {

    public BandejaDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaDetalle doc = new BandejaDetalle();

        doc.setnOperacion(rs.getString("dv_CodOperacion"));
        doc.setRut(rs.getInt("di_Rut"));
        doc.setDigVer(rs.getString("dc_DigVer").charAt(0));
        doc.setNombrecomp(rs.getString("Nombre"));
        doc.setIdDetSol(rs.getInt("di_IdDetSol"));
        doc.setNumero_docs(rs.getInt("NumeroDoctos"));
        doc.setRutCompleto(rs.getInt("di_Rut"), rs.getString("dc_DigVer").charAt(0));
        doc.setNumero_docs_esperado(rs.getInt("di_CantDctosEsp"));
        doc.setDv_IdTipOpe(rs.getString("dv_IdTipOpe"));
        doc.setDdt_fechaIngreso(rs.getDate("DDT_FECHAINGRESO"));
        doc.setIdOperacion(rs.getInt("di_idoperacion"));
        doc.setDi_fk_idestado(rs.getInt("di_fk_idestado"));
        doc.setDv_codestado(rs.getString("dv_codestado"));
        doc.setDv_estadoOpe(rs.getString("dv_estadoope"));
        doc.setDv_EstadoActual(rs.getString("dv_EstadoActual"));
        doc.setOpe_inhibida(rs.getString("OPE_INHIBIDA"));
        return doc;
    }
}
