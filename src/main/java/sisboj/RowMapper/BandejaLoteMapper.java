/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.BandejaLote;


/**
 *
 * @author esilves
 */
public class BandejaLoteMapper implements RowMapper<BandejaLote>{
  
    
        public BandejaLote mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaLote doc = new BandejaLote();
            
       // doc.setHito(rs.getString("di_IdHito"));
        doc.setNumDctos(rs.getInt("cantidad"));
        doc.setNumLote(rs.getString("dv_CodBarra"));
        doc.setEstado(rs.getString("dv_NomEstado"));
       // doc.setProducto(rs.getString("dv_NomProduc"));    
        doc.setUbacion(rs.getString("dv_DetUbi"));
        return doc;
    }
    
    
}
