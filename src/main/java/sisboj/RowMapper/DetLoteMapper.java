/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.DetLote;


/**
 *
 * @author excosoc
 */
public class DetLoteMapper implements RowMapper<DetLote>{

    public DetLote mapRow(ResultSet rs, int rowNum) throws SQLException {
        DetLote dLote = new DetLote();
        dLote.setDi_IdDetLote(rs.getInt("di_IdDetLote"));
        dLote.setDi_fk_IdLote(rs.getInt("di_fk_IdLote"));
        dLote.setDi_fk_IdDcto(rs.getInt("di_fk_IdDcto"));
        dLote.setDv_GlosaLote(rs.getString("dv_GlosaLote"));
        return dLote;
    }
    
}
