/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.OficinaTblHitosEnt;
import sisboj.entidades.UbicacionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class OficinaTblHitosMapper implements RowMapper<OficinaTblHitosEnt>{
    
    public OficinaTblHitosEnt mapRow(ResultSet rs, int i) throws SQLException {
        OficinaTblHitosEnt OficinaTblHitosEnt = new OficinaTblHitosEnt();
        OficinaTblHitosEnt.setOficina(rs.getString("oficina"));      
        return OficinaTblHitosEnt;
    }
    
}
