/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import sisboj.entidades.TipDcto;


/**
 *
 * @author excosoc
 */
public class TipoDctoMapper implements RowMapper<TipDcto>{

    public TipDcto mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipDcto tipodcto = new TipDcto();
      //  tipodcto.setDi_IdLote(rs.getInt("di_IdLote"));
        
        tipodcto.setDi_IdTipDcto(rs.getInt("di_IdTipDcto"));
        tipodcto.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
        tipodcto.setDi_IdTipDctoPJ(rs.getInt("di_IdTipDctoPJ"));
        //lote.setDv_CodBarra(rs.getString("dv_CodBarra"));
   
        //lote.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
        //lote.setDdt_FechaCreacion(rs.getString("ddt_FechaCreacion"));
        return tipodcto;
   
    
    
    
    }
    
}