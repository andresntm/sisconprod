/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.TipoDocumento;

/**
 *
 * @author excosoc
 */
public class TipoDocumentoMapper implements RowMapper<TipoDocumento> {
    public TipoDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoDocumento td = new TipoDocumento();
        td.setDi_IdTipDcto(rs.getInt("di_IdTipDcto"));
        td.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
        td.setDi_IdTipDctoPJ(rs.getInt("di_IdTipDctoPJ"));
//      td.setDdt_FecIngreso(rs.getDate("ddt_FecIngreso");
        td.setBit_Activo(Integer.toString(rs.getByte("bit_Activo")));
        return td;
    }

}
