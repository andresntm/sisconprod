/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Lote;


/**
 *
 * @author excosoc
 */
public class LoteMapper implements RowMapper<Lote>{

    public Lote mapRow(ResultSet rs, int rowNum) throws SQLException {
        Lote lote = new Lote();
        lote.setDi_IdLote(rs.getInt("di_IdLote"));
        lote.setDv_CodBarra(rs.getString("dv_CodBarra"));
   
        lote.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
        lote.setDdt_FechaCreacion(rs.getString("ddt_FechaCreacion"));
        return lote;
    }
    
}