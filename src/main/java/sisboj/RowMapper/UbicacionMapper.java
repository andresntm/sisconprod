/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.Ubicacion;


/**
 *
 * @author excosoc
 */
public class UbicacionMapper implements RowMapper<Ubicacion>{

    public Ubicacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ubicacion ubi = new Ubicacion();
        ubi.setDi_IdUbicacion(rs.getInt("di_IdUbicacion"));
        ubi.setDv_DetUbicacion(rs.getString("dv_DetUbi"));
        return ubi;
    }
    
}