/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.SetDocumentosEnt;

/**
 *
 * @author EXVGUBA
 */
public class SetDocumentosMapper implements RowMapper<SetDocumentosEnt>{

    public SetDocumentosEnt mapRow(ResultSet rs, int rowNum) throws SQLException {
        SetDocumentosEnt std = new SetDocumentosEnt();
        std.setDi_IdSetDoc(rs.getInt("di_IdSetDoc"));
        std.setDv_NomSetDoc(rs.getString("dv_NomSetDoc"));
        std.setDdt_FechaCrea(rs.getDate("ddt_FechaCrea"));
        std.setDdt_FechaMod(rs.getDate("ddt_FechaMod"));
        std.setDv_Descripcion(rs.getString("dv_Descripcion"));
        std.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
//        std.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
//        std.setDi_fk_IdTipDcto(rs.getInt("di_IdTipDcto"));
        return std;

    }
}
