/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import config.MvcConfig;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;


/**
 *
 * @author esilvestre
 */
@SuppressWarnings("serial")
public class SolicitudesTerminadasController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
 
    @Wire
    Div id_addDocNew;
    
    @Wire
    Div codebarDiv;
    
    @Wire
    Textbox glosaNom;
    
    @Wire
    Panel id_addPenPanel;
    
    @Wire
    Panel id_addDocPanel;
    @Wire
    Combobox cmb_EstDcto;
    
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_TioAux2;
    
    @Wire
    Div coder;
    
    @Wire
    Image _idCodeBarRear2;
    
    @Wire
    Label _yyyy;
    
    @Wire
    Label _yyyyOp;
    
    
    @Wire
    Label barcodeOper;
    
    @Wire
    Combobox cmb_TipoDocumento;

    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;
    
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    
    @Wire
    Textbox txt_textObs;
    
    @Wire
    Div id_detalleSolicitud;
    Window capturawin;
    @Wire
    Div _divFridDoc;

    @Wire
    Image _idCodeBarOpe;
    
    @Wire
    Paging pg;
    
    
    @Wire
    private List<String> cmb_TipDcto;

    String ValueIdOperaciones;
    String unicacion;

//    @Wire
//    Button id_addDocumentos;
    
    BitMatrix encodeCode128 = null;
    BitMatrix encodeCode128Oper = null;
    BufferedImage bufferedImage128 = null;
    BufferedImage bufferedImage128Oper = null;
    
    
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    List<Ubicacion> lUbi;
    private EventQueue eq;
    UbicacionJDBC ubicacionJDBC;
    List<BandejaDetalle> nn = null;
    private int id_solicitud = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    ///Informacion del Usuario
    String UsuarioCustodia;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private EstadoJDBC _estado;

    public SolicitudesTerminadasController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        lUbi = new ArrayList<Ubicacion>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        CargaPag();
        CargaEventos();

    }

    public void CargaPag() throws SQLException {

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        try{
        this.listSol = conexSol.getSolTerminadas(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        /*Se setea paginador paging id="pg"*/
        pg.setTotalSize(listSol.size());
        pg.setPageSize(10);
        pg.setDetailed(true);

        /*Se setea grid mold y se asigna paging id="pg" a grid*/
        grid.setMold("paging");
        grid.setPaginal(pg);

        /*Se setea model*/
        grid.setModel(bandejacustodia);

        /*Se crea evento onCreate para el grid asi genera los eventos click para los rows*/
        grid.addEventListener("onCreate", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                System.out.println("ONCREATE");
                for (Component row : grid.getRows().getChildren()) {
                    row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                        public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                    String value = banc.getProducto();
                    String codeBar = "";
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                    id_solicitud = banc.getNumsol();

                    codeBar = banc.getCodeBar();

                    System.out.println("CODEBAR TERMINADA : " + codeBar);

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    grid_DetSol.setModel(DetSolModelList);
                    //                 id_addDocPanel.setVisible(false);
                    //                 _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                   
                   
                   /*CODIGO DE BARRA*/
                   codeBarGenerate(codeBar);

                    Clients.scrollIntoView(id_detalleSolicitud);
                    
                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);
                        coder.setVisible(false);
                        codebarDiv.setVisible(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }
                    });
                }
            }
        });

        /*Se genera evento onPaging para cargar eventos click al paginar y cambiar de pagina del grid*/
        grid.addEventListener("onPaging", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                PagingEvent evt = (PagingEvent) event;
                CargaEventos();
            }
        });
        
        }catch(Exception ex){
            System.out.println("Exception Carga Inicial Solicitudes SolicitudesTerminadasController --> " + ex);
        }
        
        int cuantos = grid.getRows().getChildren().size();

    }
    
    public void irSort(int id_label) throws SQLException {

        System.out.println("ID_LABEL --> " + id_label);

        switch (id_label) {
            case 1:
                CargaPagSort(1);
                break;
            case 2:
                CargaPagSort(2);
                break;
            case 3:
                CargaPagSort(3);
                break;
            case 4:
                CargaPagSort(4);
                break;
            case 5:
                CargaPagSort(5);
                break;
            /*else if(id_label==7){
        CargaPagSort(7);
        }*/
            case 6:
                CargaPagSort(6);
                break;
            case 8:
                CargaPagSort(8);
                break;
            case 9:
                CargaPagSort(9);
                break;
            default:
                break;
        }

    }
    
    
    public void CargaPagSort(int label_cont) throws SQLException {
        for (Component column : grid.getColumns().getChildren()) {

            column.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Column row = (Column) arg0.getTarget();

                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    if (rowSelected != null && rowSelected) {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", true);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", false);
                        row.setSclass("z-row-background-color-on-select-sort");

                    }else {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        row.setSclass("z-row-background-color-on-select-sort");

                    }

                }
            });
        }

        this.listSol = conexSol.getSolNuev_TermSort(UsuarioCustodia, label_cont);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

        CargaEventos();

    }
    
    
    private void CargaEventos() throws SQLException {
        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                    String value = banc.getProducto();
                    String codeBar = "";
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                    id_solicitud = banc.getNumsol();

                    codeBar = banc.getCodeBar();

                    System.out.println("CODEBAR TERMINADA : " + codeBar);

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    grid_DetSol.setModel(DetSolModelList);
                    //                 id_addDocPanel.setVisible(false);
                    //                 _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                   
                   
                   /*CODIGO DE BARRA*/
                   codeBarGenerate(codeBar);

                    Clients.scrollIntoView(id_detalleSolicitud);
                    
                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);
                        coder.setVisible(false);
                        codebarDiv.setVisible(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }
            });
        }
    }
    
    
    public void buscarOper() throws SQLException{
    
        String glosanom = glosaNom.getValue();
        System.out.println("glosanom: "+glosanom);
        
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        
        if(glosanom == "" || glosanom.isEmpty()){
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
        grid_DetSol.setModel(DetSolModelList);
        }else{
        this.listDet = conexDetSol.listDetBuscaOperSolicitud(glosanom,id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
        }
        
    }
    
    public void codeBarGenerate(String barCode) throws WriterException, SQLException {
        
        encodeCode128 = new Code128Writer().encode(barCode, BarcodeFormat.CODE_128, 150, 80, null);
        bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
        _idCodeBarRear2.setContent(bufferedImage128);
        _yyyy.setValue(barCode);
        coder.setVisible(true);
        
        
    }
    
    public void  codeBarGenerateOper (String nOper) throws SQLException, WriterException{
    
        codebarDiv.setVisible(true);
        
        encodeCode128Oper = new Code128Writer().encode(nOper, BarcodeFormat.CODE_128, 150, 80, null);
        bufferedImage128Oper = MatrixToImageWriter.toBufferedImage(encodeCode128Oper);
       _idCodeBarOpe.setContent(bufferedImage128Oper);
       _yyyyOp.setValue(nOper);
      
    }
    
    public void updateGridDoc(int id_operacion, int countNumDoc, int cantDocsEsperados) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

        id_addDocNew.setVisible(countNumDoc >= cantDocsEsperados ? false : true); // se debe corregir al nuevo proceso

    }

//    public void updateDetalleGrid(int id_sol) {
//
//        conexDetSol = new MvcConfig().detSolicitudJDBC();
//        this.listDet = conexDetSol.listDetSolicitud(id_sol);
//        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
//        grid_DetSol.setModel(DetSolModelList);
//        grid_DetSol.setVisible(false);
//        grid_DetSol.setVisible(true);
//
//    }
//
//    public void setValuesDocs(int id_sol) {
//        boolean flag = false;
//        _divFridDoc.setVisible(false);
//        id_addDocPanel.setVisible(false);
//        BandejaDetalle detSeleccion = null;//=(BandejaDetalle)listDet.get(id_sol);
//
//        for (final BandejaDetalle banddettt : listDet) {
//            if (banddettt.getIdDetSol() == id_sol) {
//                detSeleccion = banddettt;
//            }
//        }
//
//        id_addDocPanel.setVisible(true);
//        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
//        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
//        txt_OperDcto.setValue(detSeleccion.getnOperacion());
////        id_addDocumentos.setVisible(true);
//
//        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
//            // Messagebox.show(detSeleccion.getnOperacion());
//            ope = _op.getOperaciones(detSeleccion.getnOperacion());
//            //  Messagebox.show(Integer.toString(ope.getDi_IdOperacion()));
//            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs());
//        }
//
//        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
//    }
    
    
    

    public void updateDetalleGrid(int id_sol) throws SQLException {

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        
         for (final BandejaDetalle nnn : listDet) {

            if (nnn.getOpe_inhibida().endsWith("NO")) {
                System.out.println("INH--> " + nnn.getnOperacion());
            } else {
                System.out.println("INH--> " + nnn.getnOperacion());
                nnn.setDv_estadoOpe("Inhibida");
                nnn.setNumero_docs(0);
                
            }
        }
        
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);

        grid_DetSol.setModel(DetSolModelList);

        grid_DetSol.setVisible(false);
        grid_DetSol.setVisible(true);

    }

    public void updateDetalle(int id_sol) throws SQLException {

        int id_soli = id_sol;
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_soli));

        grid_DetSol.setModel(DetSolModelList);
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
        id_addPenPanel.setVisible(false);
        id_detalleSolicitud.setVisible(true);

        Clients.scrollIntoView(id_detalleSolicitud);

    }
    
     public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        
        for (final BandejaDetalle nnn : listDet) {

            if (nnn.getOpe_inhibida().endsWith("NO")) {
                System.out.println("INH--> " + nnn.getnOperacion());
            } else {
                System.out.println("INH--> " + nnn.getnOperacion());
                nnn.setDv_estadoOpe("Inhibida");
                nnn.setNumero_docs(0);
            }
        }
        
        return listDet;
    }
    
     public void setValuesDocsComplete(int id_sol, int NumDocEsperados, int NumDocumentos,String nOpe) throws SQLException, WriterException {

        CargaBandejaDetalle(id_sol, NumDocEsperados);

//        id_addDocumentos.setVisible(false);
        //id_addDocNew.setVisible(false);
        
        codeBarGenerateOper (nOpe);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());

            System.out.println("ope.getDi_IdOperacion => " + ope.getDi_IdOperacion());
            System.out.println("detSeleccion.getNumero_docs() => " + ope.getDi_IdOperacion());
            System.out.println("NumDocEsperados => " + NumDocEsperados);

            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs(), NumDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);
        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }
     public void CargaBandejaDetalle(int id_sol, int numDocEsperados) {

        id_addPenPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        txt_TioAux.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }
     
    public List<ProdTipoDoc> getListTipDoc() {
        List<ProdTipoDoc> ptd = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(detSeleccion.getnOperacion()));
        return ptd;
    }

    public void sendBackOffice(final int numerosolicitud) {

        System.out.println("***LLEGA sendBackOffice*** Enviar Lote N�Solicitud: " + numerosolicitud);
//        int id_operacion_documento = 0;
//        Map<String, Object> arguments = new HashMap<String, Object>();
//
//        arguments.put("solicitud", numerosolicitud);
//
//        arguments.put("rut", 15.014544);
//        String template = "boveda/EnvioBojPoput.zul";
//        window = (Window) Executions.createComponents(template, null, arguments);
//
//        try {
//            window.doModal();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        //updateSolicitud
        boolean updEstadoEnv = false;

        updEstadoEnv = conexSol.CerrarOCambiarEstadoSolicitud(numerosolicitud, "enviado");
        
        if (updEstadoEnv) {
            Messagebox.show("Solicitud Enviada Correctamente", "", Messagebox.OK, Messagebox.INFORMATION);
            this.listSol = conexSol.getSolTerminadas(UsuarioCustodia);

            bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

            grid.setModel(bandejacustodia);
            grid.renderAll();

            id_detalleSolicitud.setVisible(false);
            id_addDocPanel.setVisible(false);
            coder.setVisible(false);
            
            codebarDiv.setVisible(false);
            
        } else {
            Messagebox.show("Problemas al enviar Solicitud", "", Messagebox.OK, Messagebox.ERROR);
        }

    }
    
     public void sendBackNueva(final int numerosolicitud) {

        System.out.println("***LLEGA sendBackNueva*** Re Abrir N�Solicitud: "+numerosolicitud);
        
        //updateSolicitud NSCUS
        
                  Messagebox.show("Confirma Re-Abrir Solicitud ", "Re-Abrir Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                 new org.zkoss.zk.ui.event.EventListener() {
             public void onEvent(Event e) {
                 if (Messagebox.ON_OK.equals(e.getName())) {
                     boolean updEstadoEnv = false;

                     
                     updEstadoEnv = conexSol.CerrarOCambiarEstadoSolicitud(numerosolicitud, "nuevo");
                     if(updEstadoEnv){
                     Messagebox.show("Solicitud Re-Abierta Correctamente", "", Messagebox.OK, Messagebox.INFORMATION);
                     listSol = conexSol.getSolTerminadas(UsuarioCustodia);

                     bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

                     grid.setModel(bandejacustodia);
                     grid.renderAll();

                     id_detalleSolicitud.setVisible(false);
                     id_addDocPanel.setVisible(false);
                     coder.setVisible(false);
                     codebarDiv.setVisible(false);
                     }else{
                     Messagebox.show("Problemas al Re-Abrir Solicitud", "", Messagebox.OK, Messagebox.ERROR);
                     }

                 } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                 }
             }
         }
         );
            
            
            
            
        
        
        
        
        
        
        
        
        
//        int id_operacion_documento = 0;
//        Map<String, Object> arguments = new HashMap<String, Object>();
//
//        arguments.put("solicitud", numerosolicitud);
//
//        arguments.put("rut", 15.014544);
//        String template = "boveda/EnvioBojPoput.zul";
//        window = (Window) Executions.createComponents(template, null, arguments);
//
//        try {
//            window.doModal();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

}
