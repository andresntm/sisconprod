/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;


import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import config.MvcConfig;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;

/**
 *
 * @author aescobar
 */
@SuppressWarnings("serial")
public class BandBojRecepController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;

    @Wire
    Textbox glosaNom;
    
    @Wire
    Div id_addDocNew;

    @Wire
    Panel id_addPenPanel;

    @Wire
    Panel id_addDocPanel;
    @Wire
    Combobox cmb_EstDcto;

    @Wire
    Textbox txt_TioAux;
    
    @Wire
    Button btnCerrarAll;
    
    @Wire
    Textbox txt_TioAux2;

    @Wire
    Combobox cmb_TipoDocumento;

    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;

    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;

    @Wire
    Textbox txt_textObs;

    @Wire
    Div id_detalleSolicitud;
    Window capturawin;
    @Wire
    Div _divFridDoc;

    @Wire
    private List<String> cmb_TipDcto;

    String ValueIdOperaciones;
    String unicacion;

    BitMatrix encodeCode128 = null;
    private BufferedImage bufferedImage128 = null;
    private Image img123 = null;
//    @Wire
//    Button id_addDocumentos;
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    private BandejaCustodia baj;
    List<Ubicacion> lUbi;
    List<BandejaCustodia> nn = null;
    private EventQueue eq;
    UbicacionJDBC ubicacionJDBC;
    private int id_solicitud = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    ///Informacion del Usuario
    String UsuarioCustodia;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private EstadoJDBC _estado;

    public BandBojRecepController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        lUbi = new ArrayList<Ubicacion>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        CargaPag();
        CargaEventos();

    }

    public void CargaPag() throws SQLException {

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        try{
        this.listSol = conexSol.getSolRecepcionadas(UsuarioCustodia);

        nn = conexSol.getSolRecepcionadas(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

//        for (final BandejaCustodia nnn : bandejacustodia) {
//            nnn.setEstado("Recibido");
//        }

        grid.setModel(bandejacustodia);
        grid.renderAll();
        }catch(Exception ex){
            System.out.println("Exception Carga Inicial Solicitudes BandBojRecepController --> " + ex);
        }
        int cuantos = grid.getRows().getChildren().size();

    }
    
    public void buscarOper() throws SQLException{
    
        String glosanom = glosaNom.getValue();
        System.out.println("glosanom: "+glosanom);
        
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        
        if(glosanom == "" || glosanom.isEmpty()){
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
        grid_DetSol.setModel(DetSolModelList);
        }else{
        this.listDet = conexDetSol.listDetBuscaOperSolicitud(glosanom,id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
        }
        
    }
    
    

    public void irSort(int id_label) throws SQLException {

        System.out.println("ID_LABEL --> " + id_label);

        switch (id_label) {
            case 1:
                CargaPagSort(1);
                break;
            case 2:
                CargaPagSort(2);
                break;
            case 3:
                CargaPagSort(3);
                break;
            case 4:
                CargaPagSort(4);
                break;
            case 5:
                CargaPagSort(5);
                break;
            /*else if(id_label==7){
        CargaPagSort(7);
        }*/
            case 6:
                CargaPagSort(6);
                break;
            case 8:
                CargaPagSort(8);
                break;
            case 9:
                CargaPagSort(9);
                break;
            default:
                break;
        }

    }

    public void CargaPagSort(int label_cont) throws SQLException {
        for (Component column : grid.getColumns().getChildren()) {

            column.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Column row = (Column) arg0.getTarget();

                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    if (rowSelected != null && rowSelected) {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", true);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", false);
                        row.setSclass("z-row-background-color-on-select-sort");

                    } else {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        row.setSclass("z-row-background-color-on-select-sort");

                    }

                }
            });
        }

        this.listSol = conexSol.getSolNuev_EnvSort(UsuarioCustodia, label_cont);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

        CargaEventos();

    }

    private void CargaEventos() throws SQLException {
        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                    id_solicitud = banc.getNumsol();

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    
                    for (final BandejaDetalle nnns : DetSolModelList) {
                        System.out.println("LALALAL: "+nnns.getIdOperacion()); 
                        
                        //generateCodeOper(nnns.getnOperacion());
                       // nnns.setBufferedImage128(generateCodeOper(nnns.getnOperacion()));
                      
                      
                        _conListDOc = new MvcConfig().documentosJDBC();
                       _listDOc = _conListDOc.listDocumento(nnns.getIdOperacion());
                       _listModelDOc = new ListModelList<Documento>(_listDOc);
                      
                         for (final Documento dco : _listModelDOc) {
                             System.out.println("Documento GLOSA : "+dco.getTipoDocumentoGlosa());
                            //nnns.setDv_estadoOpe(dco.getTipoDocumentoGlosa());
                            nnns.setTipoDoc(dco.getTipoDocumentoGlosa());
                            //nnns.setDv_EstadoActual("");
                    }
                    }
                    
                    grid_DetSol.setModel(DetSolModelList);
                    //                 id_addDocPanel.setVisible(false);
                    //                 _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                    btnCerrarAll.setVisible(true);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);
                        
                        btnCerrarAll.setVisible(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }
            });
        }
    }

    public void irDetalle(String id_det) throws SQLException {

        System.out.println("IR DETALLE --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }
    
    
    public BufferedImage generateCodeOper(String code) throws SQLException, WriterException{
        
       DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
        
       // for (final BandejaDetalle nnn : DetSolModelList) {
       
       /*Encode */
       encodeCode128 = new Code128Writer().encode(code, BarcodeFormat.CODE_128, 150, 80, null);
       bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
       /*fin encode*/
       
       //BandejaDetalle bjdet = new BandejaDetalle();
      //bjdet.setBufferedImage128(bufferedImage128);
            
        //}
        return bufferedImage128;
    
    }
    
    public void updateGridDoc(int id_operacion, int countNumDoc, int cantDocsEsperados) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

        id_addDocNew.setVisible(countNumDoc >= cantDocsEsperados ? false : true); // se debe corregir al nuevo proceso

    }


    public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        return listDet;
    }

    public void setValuesDocsComplete(int id_sol, int NumDocEsperados, int NumDocumentos) throws SQLException {

        CargaBandejaDetalle(id_sol, NumDocEsperados);

//        id_addDocumentos.setVisible(false);
        //id_addDocNew.setVisible(false);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());

            System.out.println("ope.getDi_IdOperacion => " + ope.getDi_IdOperacion());
            System.out.println("detSeleccion.getNumero_docs() => " + ope.getDi_IdOperacion());
            System.out.println("NumDocEsperados => " + NumDocEsperados);

            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs(), NumDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);
        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void CargaBandejaDetalle(int id_sol, int numDocEsperados) {

        id_addPenPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        txt_TioAux.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public List<ProdTipoDoc> getListTipDoc() {
        List<ProdTipoDoc> ptd = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(detSeleccion.getnOperacion()));
        return ptd;
    }

    public void sendBackOffice(final int numerosolicitud) {

        System.out.println("***LLEGA sendBackOffice*** N�Solicitud: " + numerosolicitud);
//        int id_operacion_documento = 0;
//        Map<String, Object> arguments = new HashMap<String, Object>();
//
//        arguments.put("solicitud", numerosolicitud);
//
//        arguments.put("rut", 15.014544);
//        String template = "boveda/EnvioBojPoput.zul";
//        window = (Window) Executions.createComponents(template, null, arguments);
//
//        try {
//            window.doModal();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    public void sendBackNueva(final int numerosolicitud) {

        System.out.println("***LLEGA sendBackNueva*** N�Solicitud: " + numerosolicitud);
//        int id_operacion_documento = 0;
//        Map<String, Object> arguments = new HashMap<String, Object>();
//
//        arguments.put("solicitud", numerosolicitud);
//
//        arguments.put("rut", 15.014544);
//        String template = "boveda/EnvioBojPoput.zul";
//        window = (Window) Executions.createComponents(template, null, arguments);
//
//        try {
//            window.doModal();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

}
