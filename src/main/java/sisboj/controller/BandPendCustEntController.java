/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import config.MvcConfig;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zul.event.PagingEvent;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;

/**
 *
 * @author esilvestre
 */
@SuppressWarnings("serial")
public class BandPendCustEntController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
    @Wire
    Panel id_addDocPanel;
    @Wire
    Div id_detalleSolicitud;
    @Wire
    Textbox aux;
    @Wire
    Div _divFridDoc;
    @Wire
    Button id_addDocumentos;
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_cantDocs;
    @Wire
    Div id_addDocNew;
    @Wire
    Combobox cmb_TipoDocumento;
    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    @Wire
    Textbox txt_docsEsperados;
    
    @Wire
    Textbox glosaNom;
    
    @Wire
    Paging pg;

//    int idOpe2=0;
    String ValueIdOperaciones;
    String unicacion;
    Window capturawin;

    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private DetSolicitudJDBC conexDetSol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipoDocumentoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private int id_solicitud = 0;
    private EventQueue eq;
    private boolean bNuevo;
    private int idOperRetorno = 0;
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    private EstadoJDBC _estado;
    private int Id_Operacion;
    private int numDctsOpAct = 0;
    private List<ProdTipoDoc> prTipoDoc;

    ///Informacion del Usuario
    String UsuarioCustodia;
    private Documento _nuevodocto;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    
    private String param_url;
    

    public BandPendCustEntController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {

        super.doAfterCompose(comp);
        capturawin = (Window) comp;

        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        String param="";
        Execution exec = Executions.getCurrent();
       if(exec.getParameter("idSolicitud") == null || exec.getParameter("idSolicitud").equals("null") ){
           
            CargaPag();
            CargaEventos();
           // CargaPagSort(0);
       }else{
        //CargaPag();
        //CargaEventos();
        
            param_url=exec.getParameter("idSolicitud");
            id_solicitud=Integer.parseInt(param_url);
            System.out.println("***GetUrlParam ---> "+param_url);
            CargaPagByUrl(param_url);
       }
        
       
        

    }

    private void CargaPagByUrl(String param) throws SQLException {

        final int urlParam = Integer.parseInt(param);	
        
        System.out.println("###--->>CargaPagByUrl --> " + urlParam);
        
        /*CARGA PAGINA*/
         Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        this.listSol = conexSol.getSolPendientes(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

        /*CARGA EVENTOS*/
        
        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CREATE, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();
                  
                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();

                    
                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(urlParam));

                    grid_DetSol.setModel(DetSolModelList);
                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }
                }
            });
        }
        
        
    }
    
    
    
    public void CargaPag() throws SQLException {

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        this.listSol = conexSol.getSolPendientes(UsuarioCustodia);

        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);
        
         /*Se setea paginador paging id="pg"*/
        pg.setTotalSize(listSol.size());
        pg.setPageSize(10);
        pg.setDetailed(true);

        /*Se setea grid mold y se asigna paging id="pg" a grid*/
        grid.setMold("paging");
        grid.setPaginal(pg);

        /*Se setea model*/
        grid.setModel(bandejacustodia);

        /*Se crea evento onCreate para el grid asi genera los eventos click para los rows*/
        grid.addEventListener("onCreate", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                System.out.println("ONCREATE");
                for (Component row : grid.getRows().getChildren()) {
                    row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                        public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();
                  
                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();

                    id_solicitud = banc.getNumsol();
                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    grid_DetSol.setModel(DetSolModelList);
                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }
                }
                    });
                }
            }
        });

        /*Se genera evento onPaging para cargar eventos click al paginar y cambiar de pagina del grid*/
        grid.addEventListener("onPaging", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                PagingEvent evt = (PagingEvent) event;
                CargaEventos();
            }
        });

        int cuantos = grid.getRows().getChildren().size();

    }
public void irSort(int id_label) throws SQLException{
        
        System.out.println("ID_LABEL --> "+ id_label);
        
        switch (id_label) {
            case 1:
                CargaPagSort(1);
                break;
            case 2:
                CargaPagSort(2);
                break;
            case 3:
                CargaPagSort(3);
                break;
            case 4:
                CargaPagSort(4);
                break;
            case 5:
                CargaPagSort(5);
                break;
        /*else if(id_label==7){
        CargaPagSort(7);
        }*/
            case 6:
                CargaPagSort(6);
                break;
            case 8:
                CargaPagSort(8);
                break;
            default:
                break;
        }
        
 }

public void CargaPagSort(int label_cont) throws SQLException {


         for (final Component column : grid.getColumns().getChildren()) {
              
            column.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
 
                @Override
                public void onEvent(Event arg0) throws Exception {
                    Column row = (Column) arg0.getTarget();
                    System.out.println("****ROWo1 -->> "+ row);
                   
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");
                   
                    if ( rowSelected != null && rowSelected) {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", true);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", false);
                        row.setSclass("z-row-background-color-on-select-sort");  
                   
                    }else{
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        row.setSclass("z-row-background-color-on-select-sort");  
                    
                    }
                    
                }
            });
        }
         
         
            this.listSol = conexSol.getSolPendientesSort(UsuarioCustodia,label_cont);
            bandejacustodia = new ListModelList<BandejaCustodia>(listSol);
         
            grid.setModel(bandejacustodia);
            grid.renderAll();
        
            CargaEventos();
      
    }

    private void CargaEventos() {

        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();
                  
                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();

                    id_solicitud = banc.getNumsol();
                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    grid_DetSol.setModel(DetSolModelList);
                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }
                }
            });
        }
    }
    
    
    
    public void buscarOper() throws SQLException{
    
        String glosanom = glosaNom.getValue();
        System.out.println("glosanom: "+glosanom);
        
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        
        if(glosanom == "" || glosanom.isEmpty()){
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
        grid_DetSol.setModel(DetSolModelList);
        }else{
        this.listDet = conexDetSol.listDetBuscaOperSolicitud(glosanom,id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
        }
        
    }

     public void irDetalle(int id_det) throws SQLException {

        System.out.println("IR DETALLE --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }

    
    public void updateGridDoc(int id_operacion, int countNumDoc, int cantDocsEsperados) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

        id_addDocNew.setVisible(countNumDoc >= cantDocsEsperados ? false : true); // se debe corregir al nuevo proceso

    }

    public void updateDetalleGrid(int id_sol) throws SQLException {

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
        grid_DetSol.setVisible(false);
        grid_DetSol.setVisible(true);

    }
    
    
     public void updateDetalle(int id_sol) throws SQLException {
     
        int id_soli=id_sol;
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_soli));

        grid_DetSol.setModel(DetSolModelList);
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
        id_detalleSolicitud.setVisible(true);

   
        Clients.scrollIntoView(id_detalleSolicitud);
 }
   
    public void updateCustodia(int id_sol) throws SQLException {
     
        String param_id= String.valueOf(id_solicitud);
        CargaPagByUrl(param_id);
 }

    /**
     * Se utiliza para cargar el detalle y trabajarlo con el hijo de esta
     * ventana
     *
     * @param id_sol
     */
    public void CargaBandejaDetalle(int id_sol, int numDocEsperados) {

        detSeleccion = new BandejaDetalle();

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        txt_TioAux.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public void setValuesDocsNew(int id_sol, int countNumDoc, int cantDocEsperados) throws SQLException {

        CargaBandejaDetalle(id_sol, cantDocEsperados);

//        id_addDocNew.setVisible(countNumDoc >= 2 ? false : true);
//        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc, cantDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setValuesDocsComplete(int id_sol, int NumDocEsperados) throws SQLException {

        CargaBandejaDetalle(id_sol, NumDocEsperados);

//        id_addDocumentos.setVisible(false);
        id_addDocNew.setVisible(false);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());

            System.out.println("ope.getDi_IdOperacion => " + ope.getDi_IdOperacion());
            System.out.println("detSeleccion.getNumero_docs() => " + ope.getDi_IdOperacion());
            System.out.println("NumDocEsperados => " + NumDocEsperados);

            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs(), NumDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);
        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setButtonVisible() {

//        id_addDocumentos.setVisible(true);
        id_addDocNew.setVisible(true);

    }

    public void deleteDocs(final int id_operacion) throws SQLException {
        final List<BandejaDetalle> bandeja = getListDet(id_solicitud);
//        idOpe2=id_operacion;
        Messagebox.show("�Desea eliminar documento? ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    boolean eliminado = false;
                    List<BandejaDetalle> bandeja2 = bandeja;
                    eliminado = _conListDOc.eliminarDocumento(id_operacion);
                    if (eliminado) {
                        Messagebox.show("Documento eliminado correctamente", "", Messagebox.OK, Messagebox.INFORMATION);
                        refrescaGridDetalle();
                    }

                    int numDocs = 0;
                    int numDocsEsp = 0;

                    for (BandejaDetalle b : bandeja2) {
                        if (id_operacion == b.getIdOperacion()) {
                            numDocs = b.getNumero_docs();
                            numDocsEsp = b.getNumero_docs_esperado();
                        }
                    }
//                    setValuesDocsNew(id_solicitud, numDocs, numDocsEsp);
                    grid_DetSol.setModel(DetSolModelList);
                    updateGridDoc(id_operacion, numDocs, numDocsEsp);
                    getListDet(id_solicitud);
                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {

                }
            }
        }
        );

    }

    public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
//        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        this.listDet = conexDetSol.listDetSolPendientes(id_sol);
        System.out.println("BandPendCustEntController.getListDet conexDetSol.listDetSolPendientes COUNT => " + listDet.size());
        return listDet;
    }

    public void refrescaGridDetalle() throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
//               detSeleccion 
    }

    public void irBanPendientes(int id_sol, String numOpe) throws SQLException {
//        Map<String, Object> arguments = new HashMap<String, Object>();

        System.out.println("BandCustEntController.irBanPendientes id_solicitud => " + id_solicitud);
        System.out.println("BandCustEntController.irBanPendientes numOpe => " + numOpe);

//        detSeleccion.setDi_idSolicitud(id_solicitud);
//        detSeleccion.setnOperacion(numOpe);
//        arguments.put("seleccion", detSeleccion);
//        arguments.put("id_solicitud", id_solicitud);
        String template = "custodia/BandejaPendientes.zul?idSolicitud=" + toString().valueOf(id_solicitud);
        //   System.out.println("Prueba fellow{" + capturawin.getParent().getParent().getFellows().toString() + "}");

        Include inc = ((Include) capturawin.getParent().getParent().getFellow("pageref"));

        //  inc.setSrc(template);
        try {
            inc.setSrc(template);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    @Listen("onClick = #id_addDocumentos")
    public void submit() {

        Map<String, Object> arguments = new HashMap<String, Object>();

        detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("Seleccion", detSeleccion);

        String template = "custodia/detallesPoput.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

     */
    // este metodo Permite Refrescar de Forma Arcaida, Forzando la ejecucion del boton por comando
//    @Listen("onClick = #btnrefresh")
//    public void refresh() {
//        _divFridDoc.setVisible(true);
//        //this.updateGridDoc();
//    }
    @Listen("onClick = #id_procesarLotes")
    public void readCode() {

        Map<String, Object> arguments = new HashMap<String, Object>();

        ListModelList<BandejaDetalle> DetSolModelList2;
        DetSolModelList2 = new ListModelList<BandejaDetalle>(listDet);

        // arguments.put("orderItems", DetSolModelList2);
        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("txt_RutDcto", txt_RutDcto.getValue());
        arguments.put("txt_NombreDcto", txt_NombreDcto.getValue());
        arguments.put("txt_OperDcto", txt_OperDcto.getValue());

        arguments.put("rut", 15.014544);
        String template = "custodia/BarCodeLotes.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonGuardarDoc");
//        final Textbox nn = (Textbox) window.getFellow("txt_hiddenIdOper");
        //      final Textbox CodOper = (Textbox) window.getFellow("txt_OperDcto");
        //    final Textbox AddNewValue = (Textbox) window.getFellow("txt_hiddenIdExito");
        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws SQLException {

                // String idoper=
                window.detach();
                //  int operWinValue = Integer.parseInt(nn.getValue());
                //String operWinValue_str = nn.getValue();
                //String _CodOper = CodOper.getValue();
                int NumeroDocs = 0;
                //updateDetalleGrid();
                // Messagebox.show("Este Es el Valor de La Solicitud["+id_solicitud+"]");

                updateDetalleGrid(id_solicitud);

                /*        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getnOperacion() == _CodOper ) {
                NumeroDocs=banddettt.getNumero_docs();
                //detSeleccion = banddettt;
            }
        }*/
                //  Messagebox.show("AddNewValue.getValue()["+AddNewValue.getValue()+"]operWinValue["+_CodOper+"]NumeroDocs["+NumeroDocs+"]Nedosc["+numerodocsOperActual+"");
                // se cuenta solo la ultima suma de n+1 para determinar el final de  la accion de agreggar
                //  numerodocsOperActual = AddNewValue.getValue() == "true" ? numerodocsOperActual = numerodocsOperActual + 1 : numerodocsOperActual;
                // si foy el ultimo documento entonces termino y cierro solicitud
                int numdocsagregados = conexDetSol.NumeroDocumentosAgregadosSolicitud(id_solicitud);
                // ojo aca el valor de getNumeroDocumentosSolicitud se agrego de forma estatica,se debe arreglar

                int numerototaldedocs = conexSol.getNumeroDocumentosSolicitud(id_solicitud);

                // Messagebox.show("NumerodeDocs:[" + numdocsagregados + "]numerototaldedocs[" + numerototaldedocs + "]");
                //             Messagebox.show("AddNewValue.getValue()["+AddNewValue.getValue()+"]operWinValue["+_CodOper+"]NumeroDocs["+NumeroDocs+"]Nedosc["+numerodocsOperActual+"");
                // updateGridDoc(operWinValue, numerodocsOperActual);
                if (numdocsagregados == numerototaldedocs) {

                    if (conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerrado")) {
                        Messagebox.show("Se Cerro correctamente la Solicitud ", "Terminar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {

                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {

                                }
                            }
                        }
                        );
                    } else {
                        Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                    //OK is clicked
                                    // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                    Executions.sendRedirect("/sisboj/index");
                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                    //Cancel is clicked
                                    // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                }
                            }
                        });

                    }

                }

                //  window.print(buildParameters(window),planner);
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    @Listen("onClick = #id_reprocesarLotes")
    public void codereader() throws SQLException {

        int getIdEst=275;
        //cantOpera
        int countPen=0;
        
            /*CODIGO PROCESAR POR LOTES TODAS LAS OPERACIONES EN PENDIENTE*/
            final List<Estado> codEstado;
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
            listDet = conexDetSol.listDetSolPendientes(id_solicitud);
            
                 Messagebox.show("Operaciones Pendientes seran Reprocesadas, Desea Reprocesar Lote?", "Re Procesar Lote para Solicitud: "+id_solicitud, Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                           System.out.println("Entra a Reprocesar");
                           /*List<BandejaDetalle> nne;*/
                           for (final BandejaDetalle bdj : listDet) {
                                int coda = bdj.getDi_fk_idestado();
                                System.out.println("Cambiando Estado Operacion: "+ bdj.getnOperacion() +" --> Estado actual: "+bdj.getDi_fk_idestado() );
                                //conexDetSol.updateOpeSol(id_solicitud,"di_fk_IdEstado",141);
                                 if (coda == codEstado.get(0).getDi_IdEstado()) {
                                     conexSol.cambioEstadoSolDet(bdj.getIdDetSol(), "R", "");
                                }
                                
                            }
                           //conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);
                           updateDetalleGrid(id_solicitud);
                          
                           updateDetalle(id_solicitud);
                           updateCustodia(id_solicitud);

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                           System.out.println("Cancela");
                        }
                    }
                });
                
    }
    

    public void setCambioEstadoDocDet(int id_sol, int countNumDoc, String accion) throws SQLException {
        boolean resp = false;

        detSeleccion = new BandejaDetalle();

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        try {
            System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
           
            
            
            resp = conexSol.cambioEstadoSolDet(detSeleccion.getIdDetSol(), accion,"En Reproceso");
            if (resp) {
                Messagebox.show("Documento enviado a Reprocesar.", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        }
                    }
                });
//                updateDetalleGrid(id_sol);
                //Actualiza la bandeja de Solicitudes
//                updateGridSolicitudes();
            updateDetalleGrid(id_sol);
            updateDetalle(id_solicitud);
            updateCustodia(id_solicitud);
            
            
            } else {
                Messagebox.show("Problemas al Modificar el estado de la operacion. ", null, Messagebox.OK, Messagebox.ERROR,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            //OK is clicked
                            Executions.sendRedirect("/sisboj/index");
                        }
                    }
                }
                );

            }

        } catch (SQLException e) {
            e.getMessage();
        }
    }

    /**
     * Metodo que agrega documentos pendientes para poder cerrar el lote
     */
    public boolean GuardarDocumentosPendientes(int id_sol) throws SQLException {

        List<Estado> codEstado = new ArrayList<Estado>();
        List<Estado> codEstado2 = new ArrayList<Estado>();

        try {
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        } catch (SQLException ex) {
            Logger.getLogger(DocumentoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        codEstado = estJDBC.getIdEstadoByCodigo("DLPENCUS");
        codEstado2 = estJDBC.getIdEstadoByCodigo("SOLOCUS");

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        boolean resp = false;
        try {
            this.listDet = conexDetSol.listDetSolPendientes(id_sol);

            final HashMap<String, Object> map = new HashMap<String, Object>();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date();

            // ope = _op.getOperaciones(txt_OperDcto.getValue());
            for (final BandejaDetalle detOpPen : listDet) {

                String GlosaReparo = "Pendiente Cierre(Estado Lote - Custodia)";
                this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
                this._nuevodocto.setDb_ApoderadoBanco(false);
                this._nuevodocto.setDb_checkOk(true);
                this._nuevodocto.setDi_fk_IdUbi(2);
                this._nuevodocto.setDv_Glosa(GlosaReparo);
                this._nuevodocto.setDdt_FechaSalidaBov(null);
                this._nuevodocto.setDi_fk_IdEstado(codEstado.get(0).getDi_IdEstado());
                this._nuevodocto.setDi_fk_IdOper(detOpPen.getIdOperacion());
                this._nuevodocto.setDi_fk_TipoDcto(codEstado2.get(0).getDi_IdEstado());

                System.out.println("GuardarDocumentosPendientes _nuevodocto => " + _nuevodocto.toString());

                resp = _docto.insert(_nuevodocto);
            }

        } catch (WrongValueException ex) {

            ex.getMessage();
            return false;
        }

        return resp;
    }

    @Listen("onClick = #ButtonGuardarDoc")
    public void GuardarDocumento() throws SQLException {

        try {

            final HashMap<String, Object> map = new HashMap<String, Object>();

            int id_tipDcto = 0;
            int id_EstDcto = 0;
            System.out.println("cmb_TipoDocumento.getValue() => " + cmb_TipoDocumento.getValue());
            System.out.println("cmb_EstDcto.getValue() => " + cmb_EstDcto.getValue());
            System.out.println("txt_docsEsperados.getValue() => " + txt_docsEsperados.getValue());
            String tipodoctodesc = cmb_TipoDocumento.getValue();
            String estado = cmb_EstDcto.getValue();
            int docsEsp = Integer.parseInt(txt_docsEsperados.getValue());
            this._nuevodocto = new Documento();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date();

            ope = _op.getOperaciones(txt_OperDcto.getValue());

            //se retorna a la pag principal
            Id_Operacion = ope.getDi_IdOperacion();
            boolean checkapbanco = id_chechApoderado.isChecked();

            // Agregagos el Nuevo Documento encontrado
            for (final TipoDocumento tipodocto : _listtipoDocto) {
                if (tipodocto.getDv_NomTipDcto().equals(tipodoctodesc)) {
                    id_tipDcto = tipodocto.getDi_IdTipDcto();
                }
            }

            if (id_tipDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar un tipo de documento para continuar.");
                cmb_TipoDocumento.setFocus(true);
                return;
            }

            for (final Estado est : _estadoDocto) {
                if (est.getDv_NomEstado().equals(estado) && est.getDv_DetEstado().equals("Operaci�n C/Documento en custodia")) {
                    id_EstDcto = est.getDi_IdEstado();
                }
            }

            if (id_EstDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar el estado del documento para continuar.");
                cmb_EstDcto.setFocus(true);
                return;
            }

            if (checkapbanco) {
                Messagebox.show("Sr(a). Usuario(a), recuerde que los pagares firmados por apoderado banco deben ir con su contrato anexado.");
            }

            this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
            this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
            this._nuevodocto.setDb_checkOk(true);
            this._nuevodocto.setDi_fk_IdUbi(2);
            this._nuevodocto.setDv_Glosa(txt_GlosaDcto2.getValue());
            this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
            this._nuevodocto.setDi_fk_IdEstado(id_EstDcto);
            this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());
            this._nuevodocto.setDi_fk_TipoDcto(id_tipDcto);

            // Messagebox.show("DatosCamputados::::checkapbanco[" + checkapbanco + "]txt_OperDcto.getValue()[" + txt_OperDcto.getValue() + "]txt_GlosaDcto2.getValue()[" + txt_GlosaDcto2.getValue() + "]cmb_TipoDocumento.getValue()[" + cmb_TipoDocumento.getValue() + "]id_documento[" + id_documento + "]ope.id[" + ope.getDi_IdOperacion() + "]");
            boolean insert = _docto.insert(_nuevodocto);

            // Messagebox.show("DatosCamputados:::");
            if (insert) {
                id_addDocPanel.setVisible(false);
//                window.setVisible(false);
                bNuevo = true;
                Messagebox.show("Se Agreg� Correctamente el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            bNuevo = false;
                        }
                    }
                });

                if (bNuevo) {
                    numDctsOpAct += 1;
                } else {
                    numDctsOpAct = numDctsOpAct;
                }

                System.out.println("PoputAddDOcumento.GuardarDocumento bNuevo => " + bNuevo);
                System.out.println("PoputAddDOcumento.GuardarDocumento Id_Operacion => " + Id_Operacion);

                //Nuevo
                updateDetalleGrid(id_solicitud);
                updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);

            } else {
                Messagebox.show("Problemas al Agregar el Documento. ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            //OK is clicked
                            Executions.sendRedirect("/sisboj/index");
                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            //Cancel is clicked
                        }
                    }
                }
                );

            }

        } catch (WrongValueException ex) {
            Messagebox.show("Problemas al Agregar el Documento. \n" + ex.toString(), "Guardar Documento", Messagebox.OK, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        Executions.sendRedirect("/sisboj/index");
                    }
                }
            }
            );
        }
        listCountProdTipoDoc();

    }

    @Listen("onClick = #BtnCancelarGuardarDoc")
    public void cancelaGuardarDocumento() throws SQLException {

        updateDetalleGrid(id_solicitud);
        //  updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);

        id_addDocPanel.setVisible(false);

    }

    /**
     * Metodo que retorna cantidad de documentos por producto
     *
     * @return List<ProdTipoDoc>
     */
    public List<ProdTipoDoc> listCountProdTipoDoc() {
        return prodTipoDocConex.getListCountProdTipoDoc();
    }

    public List<ProdTipoDoc> getListTipDoc() {
        List<ProdTipoDoc> ptd = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(detSeleccion.getnOperacion()));
        return ptd;
    }

    public void updateGridSolicitudes() throws SQLException {
        this.listSol = conexSol.getSolPendientes(UsuarioCustodia);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);
        grid.setModel(bandejacustodia);
        grid.renderAll();

    }

    /*
            @SuppressWarnings("unused")
            public void afterSaveDocs() throws Exception {

//                final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                detSeleccion = new BandejaDetalle();
                detSeleccion = (BandejaDetalle) map.get("detSeleccion");
                bNuevo = (Boolean) map.get("bNuevo");
                numerodocsOperActual = (Integer) map.get("numerodocsOperActual");
                idOperRetorno = (Integer) map.get("idOepracion");
                boolean resp = false;

                updateDetalleGrid(detSeleccion.getDi_idSolicitud());

                int numdocsagregados = conexDetSol.NumeroDocumentosAgregadosSolicitud(detSeleccion.getDi_idSolicitud());
                // ojo aca el valor de getNumeroDocumentosSolicitud se agrego de forma estatica,se debe arreglar

                int numerototaldedocs = conexSol.getNumeroDocumentosSolicitud(detSeleccion.getDi_idSolicitud());
                updateGridDoc(idOperRetorno, numerodocsOperActual);

                //Se obitenen la cantidad de documentos solicitados y pendientes
                int arrNumDocs[];
                arrNumDocs = new int[2];
                arrNumDocs = conexSol.getNumeroDocSOLYPEN(detSeleccion.getDi_idSolicitud());
                int sumaDocs = arrNumDocs[1] + arrNumDocs[0];

                System.out.println("(numdocsagregados) => " + numdocsagregados);
                System.out.println("(numerototaldedocs) => " + numerototaldedocs);

                if (sumaDocs != 0) {
                    if (numdocsagregados == sumaDocs || numdocsagregados == arrNumDocs[0]) {

                        //Se cambiar de estado documentos pendientes para poder cerrar solicitud
                        if (arrNumDocs[1] != 0) {
                            resp = GuardarDocumentosPendientes(detSeleccion.getDi_idSolicitud());
                        }

                        if (conexSol.CerrarSolicitud(detSeleccion.getDi_idSolicitud()) && resp) {
                            Messagebox.show("Se Cerro correctamente la Solicitud ", "Terminar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                                    new org.zkoss.zk.ui.event.EventListener() {
                                public void onEvent(Event e) {
                                    if (Messagebox.ON_OK.equals(e.getName())) {
                                        // Messagebox.show("IncludePagPrin["+capturawin.getParent().getParent().getFellows().toString()+"]");

                                        Include inc = ((Include) capturawin.getParent().getParent().getFellow("PagPrin"));
                                        inc.setSrc(null);
                                        inc.setSrc("boveda/pagCont.zul");
                                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                    }
                                }
                            }
                            );
                        } else {
                            Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                                    new org.zkoss.zk.ui.event.EventListener() {
                                public void onEvent(Event e) {
                                    if (Messagebox.ON_OK.equals(e.getName())) {
                                        //OK is clicked
                                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                        Executions.sendRedirect("/sisboj/index");
                                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                        //Cancel is clicked
                                        // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                    }
                                }
                            }
                            );

                        }

                    }

                }
            }
     */
}
