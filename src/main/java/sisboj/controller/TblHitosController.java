/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import sisboj.entidades.implementaciones.TblHitosJDBC;
import sisboj.entidades.TblHitosEnt;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import config.MvcConfig;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Filedownload;
import sisboj.entidades.BancaTblHitosEnt;
import sisboj.entidades.EstOpeHitoCount;
import sisboj.entidades.OficinaTblHitosEnt;
import sisboj.entidades.ProductosTblHitosEnt;
import sisboj.entidades.RegionTblHitosEnt;
import sisboj.entidades.UbicacionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class TblHitosController extends SelectorComposer<Window> {

    @Wire
    Date fIni;

    @Wire
    Date fFin;

    @Wire
    Textbox txtRut;

    @Wire("txtDMI")
    Textbox txtDMI;

    @Wire("txtDMF")
    Textbox txtDMF;

    @Wire
    Textbox txtNOp;

//    @Wire
//    Textbox comboUbicacion; 
    @Wire("comboOficinas")
    Combobox comboOficinas;

    Date formatFIniD;
    Date formatFFinD;
    //Variable para Filtros
    Date fechaI;
    Date fechaF;
    List<UbicacionTblHitosEnt> ubicacion;
    String oficina = "";
    String region = "";
    String banca = "";
    String rut = "";
    int dmi;
    int dmf;
    String n_of;
    String n_op;
    String n_sol;
    String prod;
    String estadoOp;

    private List<TblHitosEnt> listTblHitos2;

    private final List<UbicacionTblHitosEnt> listTblComboUbicacion;
    private final List<UbicacionTblHitosEnt> listTblComboUbicacionFiltro;
    List<UbicacionTblHitosEnt> listTblComboUbicacionFiltro2;
    private final List<OficinaTblHitosEnt> listTblComboOficina;
    private final List<RegionTblHitosEnt> listTblComboRegion;
    private final List<BancaTblHitosEnt> listTblComboBanca;
    private final List<ProductosTblHitosEnt> listTblComboProductos;
//    private List<TblHitosEnt> listTblHitos = new ArrayList<TblHitosEnt>();
//    MvcConfig conexTblH = new MvcConfig();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final TblHitosJDBC conexTblHitos;
    ListModelList<TblHitosEnt> listModelTblHitos;
    ListModelList<UbicacionTblHitosEnt> listModelUbicacionTblHitos;
    ListModelList<OficinaTblHitosEnt> listModelOficinasTblHitos;
    ListModelList<RegionTblHitosEnt> listModelRegionTblHitos;
    ListModelList<BancaTblHitosEnt> listModelBancaTblHitos;
    ListModelList<ProductosTblHitosEnt> listModelProductosTblHitos;
    //private TblHitosEnt tblHitosEnt;

    private static final String footerMessage = "Un Total de %d Registros";
    // private int cantH;

//    private ListModelList<UbicacionTblHitosEnt> _model;
    private Set<UbicacionTblHitosEnt> _selectedObjects;

    public TblHitosController() throws SQLException {
        conexTblHitos = new MvcConfig().tblHitosJDBC();
        listTblHitos2 = conexTblHitos.getListTblHitos(formatFIniD, formatFIniD, ubicacion, oficina, region, banca, rut, dmi, dmf, n_of, n_op, prod, estadoOp,n_sol);
        // cantH = listTblHitos2.size();
        listTblComboUbicacion = conexTblHitos.getUbicacionListTblHitos();
        listTblComboOficina = conexTblHitos.getOficinaListTblHitos();
        listTblComboRegion = conexTblHitos.getRegionListTblHitos();
        listTblComboBanca = conexTblHitos.getBancaListTblHitos();
        listTblComboProductos = conexTblHitos.getProductoListTblHitos();
        listTblComboUbicacionFiltro = new ArrayList<UbicacionTblHitosEnt>();
        listTblComboUbicacionFiltro2 = new ArrayList<UbicacionTblHitosEnt>();
//        comboOficinas.setModel(getListOficinasTblHitos());
//        txtDMF.setValue("");
//        txtDMI.setValue("");

    }

    /**
     * **************** Fin Filtros Grilla Usuarios
     *
     **************************
     * @param fini
     * @param ffin
     * @param ofi
     * @param reg
     * @param ban
     * @param txRut
     * @param txDMI
     * @param txDMF
     * @param txtNOF
     * @param txtNOp
     * @param product
     */
    @Command
    @NotifyChange({"listTblHitos", "listTblHitosListbox", "footer", "listUbicacionTblHitos", "listOficinasTblHitos", "listRegionTblHitos", "listBancaTblHitos", "listProductosTblHitos", "marcas", "cantEstadoOp"})
    public void buscarConFiltros(@BindingParam("fini") final Datebox fini, @BindingParam("ffin") final Datebox ffin, @BindingParam("ofi") final Combobox ofi, @BindingParam("reg") final Combobox reg, @BindingParam("ban") final Combobox ban, @BindingParam("txRut") final Textbox txRut, @BindingParam("txtDMI") final Textbox txDMI, @BindingParam("txtDMF") final Textbox txDMF, @BindingParam("txtNOF") final Textbox txtNOF, @BindingParam("txtNOp") final Textbox txtNOp, @BindingParam("CBProd") final Combobox product, @BindingParam("chkbxEstN") final Checkbox chbxEstN, @BindingParam("chkbxEstP") final Checkbox chbxEstP, @BindingParam("chkbxEstF") final Checkbox chbxEstF,@BindingParam("txtNSol") final Textbox txNSol) {
        estadoOp = "";
        fechaI = fini.getValue();
        fechaF = ffin.getValue();
        //ubicacion = ubi.getVflex();
        oficina = ofi.getValue().trim();
        region = reg.getValue().trim();
        banca = ban.getValue().trim();
        rut = txRut.getValue().trim();
        String dmiStr = txDMI.getValue().trim();
        String dmfStr = txDMF.getValue().trim();
        n_of = txtNOF.getValue().trim();
        n_op = txtNOp.getValue().trim();
        prod = product.getValue().trim();
        n_sol = txNSol.getValue().trim();

        if (chbxEstN.isChecked()) {
            estadoOp = "'NUEVO'";
        }
        if (chbxEstP.isChecked()) {
            if (chbxEstN.isChecked()) {
                estadoOp += ",'PENDIENTE'";
            } else {
                estadoOp += "'PENDIENTE'";
            }
        }

        if (chbxEstF.isChecked()) {
            if (chbxEstN.isChecked() || chbxEstP.isChecked()) {
                estadoOp += ",'CON_SALIDA'";
            } else {
                estadoOp += "'CON_SALIDA'";
            }
        }

        for (UbicacionTblHitosEnt ubic : getSelectedObjects()) {
            System.out.println("ubic ====>>>>> " + ubic.getUbicacion());
            listTblComboUbicacionFiltro.add(ubic);
        }

        System.out.println("TblHitosController.buscarConFiltros dmiStr => " + dmiStr);
        System.out.println("TblHitosController.buscarConFiltros dmfStr => " + dmfStr);

        //dias de mora a entero
        if (dmiStr.equals("")) {
            dmi = 0;
        } else {
            dmi = Integer.parseInt(dmiStr);
        }
        if (dmfStr.equals("")) {
            dmf = 0;
        } else {
            dmf = Integer.parseInt(dmfStr);
        }

        System.out.println("TblHitosController.buscarConFiltros fechaI => " + fechaI);
        System.out.println("TblHitosController.buscarConFiltros fechaF => " + fechaF);
//        System.out.println("TblHitosController.buscarConFiltros ubicacion => " + ubicacion);
        System.out.println("TblHitosController.buscarConFiltros oficina => " + oficina);
        System.out.println("TblHitosController.buscarConFiltros region => " + region);
        System.out.println("TblHitosController.buscarConFiltros banca => " + banca);
        System.out.println("TblHitosController.buscarConFiltros producto => " + prod);

        int ResResta = 1;
        if (fechaI != null && fechaF != null) {
            String formatFIni = simpleDateFormat.format(fechaI);
            String formatFFin = simpleDateFormat.format(fechaF);
            formatFIni = formatFIni.replace("-", "");
            formatFFin = formatFFin.replace("-", "");
            Long FIni = Long.parseLong(formatFIni);
            Long FFin = Long.parseLong(formatFFin);
            ResResta = (int) (FFin - FIni);
        }

        System.out.println("ResResta => " + ResResta);

        try {

            if (ResResta < 0) {
                Messagebox.show("Fecha de Inicio no debe ser mayor a fecha de Fin del periodo a consultar", "Mensaje Administrador", Messagebox.OK, Messagebox.QUESTION, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_OK.equals(event.getName())) {
                            fini.setFocus(true);
                        }
                    }
                });

            } else {

                listTblHitos2 = conexTblHitos.getListTblHitos(fechaI, fechaF, listTblComboUbicacionFiltro, oficina, region, banca, rut, dmi, dmf, n_of, n_op, prod, estadoOp,n_sol);
                listTblComboUbicacionFiltro.clear();
                getCantEstadoOp();
                // getListUbicacionTblHitos();
                /* }*/
            }
        } catch (WrongValueException e) {
            Messagebox.show("ha ocurrido un error al realizar la busqueda. \n Error: " + e.toString());
        } catch (Exception e) {
            Messagebox.show("ha ocurrido un error al realizar la busqueda. \n Error: " + e.toString());
        } finally {

        }

    }

    public void getMarcas() {
        System.out.println("TblHitosController.marcas ubicacion => " + ubicacion);
        System.out.println("TblHitosController.marcas oficina => " + oficina);
        System.out.println("TblHitosController.marcas region => " + region);
        System.out.println("TblHitosController.marcas banca => " + banca);
        System.out.println("TblHitosController.marcas Rut => " + rut);
    }

    public ListModel<TblHitosEnt> getListTblHitos() {
        return listModelTblHitos = new ListModelList<TblHitosEnt>(listTblHitos2);

    }

    public List<TblHitosEnt> getListTblHitosListbox() {
        getCantEstadoOp();
        return listTblHitos2;

    }

    public ListModel<UbicacionTblHitosEnt> getListUbicacionTblHitos() {
        listModelUbicacionTblHitos = new ListModelList<UbicacionTblHitosEnt>(conexTblHitos.getUbicacionListTblHitos());
        return listModelUbicacionTblHitos;
    }

    public ListModel<OficinaTblHitosEnt> getListOficinasTblHitos() {
        return listModelOficinasTblHitos = new ListModelList<OficinaTblHitosEnt>(conexTblHitos.getOficinaListTblHitos());
    }

    public ListModel<RegionTblHitosEnt> getListRegionTblHitos() {
        return listModelRegionTblHitos = new ListModelList<RegionTblHitosEnt>(conexTblHitos.getRegionListTblHitos());
    }

    public ListModel<BancaTblHitosEnt> getListBancaTblHitos() {
        return listModelBancaTblHitos = new ListModelList<BancaTblHitosEnt>(conexTblHitos.getBancaListTblHitos());
    }

    public ListModel<ProductosTblHitosEnt> getListProductosTblHitos() {
        return listModelProductosTblHitos = new ListModelList<ProductosTblHitosEnt>(conexTblHitos.getProductoListTblHitos());
    }

    public EstOpeHitoCount getCantEstadoOp() {
        EstOpeHitoCount cantEstOP = new EstOpeHitoCount();
        cantEstOP = conexTblHitos.getCountEstadoOp();
        return cantEstOP;
    }

    /**
     *
     * @param win
     * @throws Exception
     */
    @Override
    public void doAfterCompose(Window win) throws Exception {
        super.doAfterCompose(win);
    }

    /**
     * ****************** Inicio Filtros Grilla
     *
     * @return
     */
    public String getFooter() {
        return String.format(footerMessage, listTblHitos2.size());
    }

    public ListModel<TblHitosEnt> getTblHitosListModel() {
        return getListTblHitos();
    }

    public ListModel<TblHitosEnt> getListTblHitosGrilla() throws Exception {
        return new ListModelList<TblHitosEnt>(listTblHitos2);
    }

    /**
     * *************** INICIO EXPORT GRID
     *
     ********************
     * @param gridList
     * @throws java.lang.Exception
     */
    @Command
    public void exportGridToExcel(@BindingParam("gridList") final Grid gridList) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024 * 10);
        gridList.renderAll();
        String fNew = df.format(new Date());
        System.out.println(gridList.getListModel().getSize() + "  size data");
        ExcelExporter exporter = new ExcelExporter();
        exporter.export(gridList, out);
        AMedia amedia = new AMedia("Informe_Custodia_" + fNew + ".xlsx", "xls", "application/file", out.toByteArray());
        Filedownload.save(amedia);
        out.close();
    }

    /*------------- Inicio Seleccion multiple Filtro Ubicacion --------------------------*/
    public void setSelectedObjects(Set<UbicacionTblHitosEnt> selectedObjects) {
        System.out.println("setSelectedObjects => " + selectedObjects);
        _selectedObjects = selectedObjects;
    }

    public Set<UbicacionTblHitosEnt> getSelectedObjects() {
        if (_selectedObjects == null) {
            _selectedObjects = new HashSet<UbicacionTblHitosEnt>();
//			_selectedObjects.add(new UbicacionTblHitosEnt("item 1"));
//			_selectedObjects.add(new UbicacionTblHitosEnt("item 3"));
        }
        return _selectedObjects;
    }

    /**
     * Filtros Dinamicos
     *
     * @param fini
     * @param ffin
     * @param ofi
     * @param reg
     * @param ban
     * @param txRut
     * @param txDMI
     * @param txDMF
     * @param txtNOF
     * @param txtNOp
     * @param product
     */
    @Command
    @NotifyChange({"listUbicacionTblHitos", "listOficinasTblHitos", "listRegionTblHitos", "listBancaTblHitos", "listProductosTblHitos", "cantEstadoOp"})
    public void filtrosDinamicos(@BindingParam("fini") final Datebox fini, @BindingParam("ffin") final Datebox ffin, @BindingParam("ofi") final Combobox ofi, @BindingParam("reg") final Combobox reg, @BindingParam("ban") final Combobox ban, @BindingParam("txRut") final Textbox txRut, @BindingParam("txtDMI") final Textbox txDMI, @BindingParam("txtDMF") final Textbox txDMF, @BindingParam("txtNOF") final Textbox txtNOF, @BindingParam("txtNOp") final Textbox txtNOp, @BindingParam("CBProd") final Combobox product) {
        fechaI = fini.getValue();
        fechaF = ffin.getValue();
        //ubicacion = ubi.getVflex();
        oficina = ofi.getValue().trim();
        region = reg.getValue().trim();
        banca = ban.getValue().trim();
        rut = txRut.getValue().trim();
        String dmiStr = txDMI.getValue().trim();
        String dmfStr = txDMF.getValue().trim();
        n_of = txtNOF.getValue().trim();
        n_op = txtNOp.getValue().trim();
        prod = product.getValue().trim();

        for (UbicacionTblHitosEnt ubic : getSelectedObjects()) {
            System.out.println("ubic ====>>>>> " + ubic.getUbicacion());
            listTblComboUbicacionFiltro.add(ubic);
        }

        //dias de mora a entero
        if (dmiStr.equals("")) {
            dmi = 0;
        } else {
            dmi = Integer.parseInt(dmiStr);
        }
        if (dmfStr.equals("")) {
            dmf = 0;
        } else {
            dmf = Integer.parseInt(dmfStr);
        }

        System.out.println("TblHitosController.filtrosDinamicos fechaI => " + fechaI);
        System.out.println("TblHitosController.filtrosDinamicos fechaF => " + fechaF);
        System.out.println("TblHitosController.filtrosDinamicos oficina => " + oficina);
        System.out.println("TblHitosController.filtrosDinamicos region => " + region);
        System.out.println("TblHitosController.filtrosDinamicos banca => " + banca);
        System.out.println("TblHitosController.filtrosDinamicos producto => " + prod);

        int ResResta = 1;
        if (fechaI != null && fechaF != null) {
            String formatFIni = simpleDateFormat.format(fechaI);
            String formatFFin = simpleDateFormat.format(fechaF);
            formatFIni = formatFIni.replace("-", "");
            formatFFin = formatFFin.replace("-", "");
            Long FIni = Long.parseLong(formatFIni);
            Long FFin = Long.parseLong(formatFFin);
            ResResta = (int) (FFin - FIni);
        }

        System.out.println("ResResta => " + ResResta);

        try {
            conexTblHitos.getListTblHitos(fechaI, fechaF, listTblComboUbicacionFiltro, oficina, region, banca, rut, dmi, dmf, n_of, n_op, prod, estadoOp,n_sol);
            listTblComboUbicacionFiltro.clear();

        } catch (WrongValueException e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } catch (Exception e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } finally {

        }

    }


    /*
    @Command
    @NotifyChange({"listTblHitos", "listTblHitosListbox", "footer", "listUbicacionTblHitos", "listOficinasTblHitos", "listRegionTblHitos", "listBancaTblHitos", "listProductosTblHitos"})
    public void limpiaFiltros() {
        Date fechaI2 = null;
        Date fechaF2 = null;
        String oficina2 = "";
        String region2 = "";
        String banca2 = "";
        String rut2 = "";
        int dmi2 =0;
        int dmf2=0;
        String n_of2 = "";
        String n_op2 = "";
        String prod2 = "";
        
// SelectedIndex
//        ofi.getSelectedIndex();
//        ofi.setSelectedIndex(-1);
                
        System.out.println("TblHitosController.limpiaFiltros fechaI => " + fechaI2);
        System.out.println("TblHitosController.limpiaFiltros fechaF => " + fechaF2);
        System.out.println("TblHitosController.limpiaFiltros oficina => " + oficina2);
        System.out.println("TblHitosController.limpiaFiltros region => " + region2);
        System.out.println("TblHitosController.limpiaFiltros banca => " + banca2);
        System.out.println("TblHitosController.limpiaFiltros producto => " + prod2);
        


        try {
//            System.out.println("comboOficinas.getValue(); => "+comboOficinas.getSelectedItem().getLabel());
            listTblHitos2 = conexTblHitos.getListTblHitos(fechaI2, fechaF2, listTblComboUbicacionFiltro2, oficina2, region2, banca2, rut2, dmi2, dmf2, n_of2, n_op2, prod2);
//           txtDMF.setText("");
//           txtDMI.setText("");
            //listTblComboUbicacionFiltro.clear();
//            comboOficinas.setSelectedIndex(-1);

        } catch (WrongValueException e) {
            Messagebox.show("ha ocurrido un error al realizar la busqueda. \n Error: " + e.toString());
        } catch (Exception e) {
            Messagebox.show("ha ocurrido un error al realizar la busqueda. \n Error: " + e.toString());
        } finally {

        }

    }
     */
}
