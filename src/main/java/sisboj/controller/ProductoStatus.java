/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import sisboj.entidades.Productos;

/**
 *
 * @author EXVGUBA
 */
public class ProductoStatus {

    private Productos prod;
    private boolean editingStatus;

    public ProductoStatus(Productos lc, boolean editingStatus) {
        this.prod = lc;
        this.editingStatus = editingStatus;
    }

    public Productos getProductosStatus() {
        return prod;
    }

    public boolean getEditingStatus() {
        return editingStatus;
    }

    public void setEditingStatus(boolean editingStatus) {
        this.editingStatus = editingStatus;
    }
}
