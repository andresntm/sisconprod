/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import java.util.List;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import config.MvcConfig;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Window;
import sisboj.entidades.ActInact;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.UbicacionTblHitosEnt;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;

/**
 *
 * @author excosoc
 */
public class TipoDocumentoController extends SelectorComposer<Window> {
//public class TipoDocumentoController extends SelectorComposer<Window> {

    @Wire
    Textbox txNvoProd;
    
    @Wire
    Textbox txNvoCodProd;
    
    @Wire
    Combobox cmbNvoTDrod;
    
    Date formatFIniD;
    Date formatFFinD;
    //Variable para Filtros
    Date fechaI;
    Date fechaF;
    List<UbicacionTblHitosEnt> ubicacion;
    String oficina = "";
    String region = "";
    String banca = "";
    String rut = "";
    int dmi;
    int dmf;
    String n_of;
    String n_op;
    String prod;
    
    List<ActInact> listEstAct;
    private ActInact act1;
    private ActInact act2;
    private ListModelList<TipoDocumentoStatus> listTipoDocumentoStatus;
    private ListModel<TipoDocumentoStatus> listTipoDocumentoStatus2;
    private final TipoDocumentoJDBC conexTD;
    private static final String footerMessage = "Un Total de %d Registros";
    private boolean displayEdit;
    String filtroNombre;
    
    public String getFiltroNombre() {
        return filtroNombre;
    }
    
    public void setFiltroNombre(String filtroNombre) {
        this.filtroNombre = filtroNombre;
    }    
    
    public TipoDocumentoController() throws SQLException {
        conexTD = new MvcConfig().tipoDocumentoJDBC();
        this.listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento(getFiltroNombre()));//new ListModelList<TipoDocumentoStatus>();
//        this.listTipoDocumentoStatus2 = generateStatusList(conexTD.getListTipoDocumento());
        this.displayEdit = true;
        this.act1 = new ActInact();
        this.act2 = new ActInact();
        this.listEstAct = new ArrayList<ActInact>();
    }
    
    @Command
    @NotifyChange({"listTipoDocumento", "footer"})
    public void buscarConFiltros(@BindingParam("glosaNom") Textbox glosaNom) {
        if (!"".equals(glosaNom.getValue().trim())) {
            setFiltroNombre(glosaNom.getValue().trim());
        } else {
            setFiltroNombre("");
        }
        listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento(getFiltroNombre()));        
    }
    
    public List<ActInact> getEstAct() {
        listEstAct.clear();
        act1.setNombreEst("Activo");
        act1.setVal(1);
        act2.setNombreEst("Inactivo");
        act2.setVal(0);
        this.listEstAct.add(0, act1);
        this.listEstAct.add(1, act2);
        return listEstAct;
    }
    
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window win) throws Exception {
        super.doAfterCompose(win);
        this.listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento(getFiltroNombre()));
        getListTipoDocumento();
    }
    
    public String getFooter() {
        return String.format(footerMessage, conexTD.getListTipoDocumento(getFiltroNombre()).size());
    }

    /**
     * ************************ Editando
     * *******************************************
     */
    public boolean isDisplayEdit() {
        return displayEdit;
    }
    
    @NotifyChange({"listTipoDocumento", "displayEdit"})
    public void setDisplayEdit(boolean displayEdit) {
        this.displayEdit = displayEdit;
    }
    
    public List<TipoDocumentoStatus> getListTipoDocumento() {
        // listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento());
        return listTipoDocumentoStatus;
    }
    
    @Command
    public void changeEditableStatus(@BindingParam("prodStatus") TipoDocumentoStatus lcs) {
        lcs.setEditingStatus(!lcs.getEditingStatus());
        refreshRowTemplate(lcs);
    }
    
    @Command
    @NotifyChange({"listTipoDocumento", "footer"})
    public void confirm(@BindingParam("prodStatus") final TipoDocumentoStatus p) {
        boolean response = false;
        List<TipoDocumento> tdoc = new ArrayList<TipoDocumento>();
        List<TipoDocumento> tdoc2 = new ArrayList<TipoDocumento>();
        
        tdoc = conexTD.nomTipoDocumentoExiste(p.getTipoDocumentoStatus().getDv_NomTipDcto());
        tdoc2 = conexTD.codTipoDocumentoExiste(p.getTipoDocumentoStatus().getDi_IdTipDctoPJ());
        
        if (tdoc.size() > 0 && (tdoc.get(0).getDi_IdTipDcto() != p.getTipoDocumentoStatus().getDi_IdTipDcto())) {
            Messagebox.show("Ya existe un Tipo de Documento con ese nombre, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
                        
                        List<TipoDocumento> td3 = conexTD.selectTipoDocumentoByID(p.getTipoDocumentoStatus().getDi_IdTipDcto());
                        p.getTipoDocumentoStatus().setDv_NomTipDcto(td3.get(0).getDv_NomTipDcto());
                        p.getTipoDocumentoStatus().setDi_IdTipDctoPJ(td3.get(0).getDi_IdTipDctoPJ());
                        p.getTipoDocumentoStatus().setBit_Activo(td3.get(0).getBit_Activo());
                        changeEditableStatus(p);
                    }
                }
            });
        } else if (tdoc2.size() > 0 && (tdoc2.get(0).getDi_IdTipDcto() != p.getTipoDocumentoStatus().getDi_IdTipDcto())) {
            Messagebox.show("Ya existe un Tipo de Documento con ese codigo, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {                        
                        List<TipoDocumento> td3 = conexTD.selectTipoDocumentoByID(p.getTipoDocumentoStatus().getDi_IdTipDcto());
                        p.getTipoDocumentoStatus().setDv_NomTipDcto(td3.get(0).getDv_NomTipDcto());
                        p.getTipoDocumentoStatus().setDi_IdTipDctoPJ(td3.get(0).getDi_IdTipDctoPJ());
                        p.getTipoDocumentoStatus().setBit_Activo(td3.get(0).getBit_Activo());
                        changeEditableStatus(p);
                    }
                }
            });
        } else {
            TipoDocumento td = new TipoDocumento();
            td.setDi_IdTipDcto(p.getTipoDocumentoStatus().getDi_IdTipDcto());
            td.setDv_NomTipDcto(p.getTipoDocumentoStatus().getDv_NomTipDcto());
            td.setDi_IdTipDctoPJ(p.getTipoDocumentoStatus().getDi_IdTipDctoPJ());
            td.setBit_Activo(p.getTipoDocumentoStatus().getBit_Activo());
            response = conexTD.updateTipoDocumento(td);
            if (response) {
                
                listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento(getFiltroNombre()));
                getListTipoDocumento();
//            changeEditableStatus(p);
//            refreshRowTemplate(p);
            }
        }
    }
    
    public void refreshRowTemplate(TipoDocumentoStatus lcs) {
        //replace the element in the collection by itself to trigger a model update
        listTipoDocumentoStatus.set(listTipoDocumentoStatus.indexOf(lcs), lcs);
    }
    
    public String getLimpiaCamposNvoProd() {
        String clean = "";
        return clean;
    }
    
    public int getLimpiaCamposNvoNumerico() {
        int clean = 0;
        return clean;
    }
    
    private static ListModelList<TipoDocumentoStatus> generateStatusList(List<TipoDocumento> contributions) {
        ListModelList<TipoDocumentoStatus> prodSt = new ListModelList<TipoDocumentoStatus>();
        for (TipoDocumento lc : contributions) {
            prodSt.add(new TipoDocumentoStatus(lc, false));
        }
        return prodSt;
    }
    
    @Command
    @NotifyChange({"listTipoDocumento", "footer", "limpiaCamposNvoProd", "limpiaCamposNvoNumerico"})
    public void addNuevoProducto(@BindingParam("txNvoTD") Textbox txNTD, @BindingParam("txNvoCodTD") Spinner txNCodTD, @BindingParam("cmbNvoEstTD") Combobox cbNEstTD) {
        boolean response = false;
        TipoDocumento td = new TipoDocumento();
        final String nvoTD = txNTD.getValue().trim();
        int nvoCodTD = 0;
        if (txNCodTD.getValue() != null && txNCodTD.getValue() != 0) {
            nvoCodTD = txNCodTD.getValue();
        }
        
        String estadoTD = "";
        
        if ("Activo".equals(cbNEstTD.getValue().trim())) {
            estadoTD = "1";
        }
        if ("Inactivo".equals(cbNEstTD.getValue().trim())) {
            estadoTD = "0";
        }
        
        List<TipoDocumento> tdoc = new ArrayList<TipoDocumento>();
        List<TipoDocumento> tdoc2 = new ArrayList<TipoDocumento>();
        
        tdoc = conexTD.nomTipoDocumentoExiste(nvoTD);
        tdoc2 = conexTD.codTipoDocumentoExiste(nvoCodTD);
        if (tdoc.size() > 0) {
            Messagebox.show("Ya existe un Tipo de Documento con ese nombre, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
//                        fini.setFocus(true);
                    }
                }
            });
        } else if (tdoc2.size() > 0) {
            Messagebox.show("Ya existe un Tipo de Documento con ese codigo, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
//                        fini.setFocus(true);
                    }
                }
            });
        } else {
            
            if ("".equals(nvoTD) || nvoCodTD == 0 || (!"1".equals(estadoTD) && !"0".equals(estadoTD))) {
                Messagebox.show("Debe completar todos los campos(Tipo de Documento, codigo, estado). \n�Desea Continuar?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_YES.equals(event.getName())) {
                            if ("".equals(nvoTD)) {
//                            txNvoProd.setFocus(true);
                            } else if ("".equals(nvoTD)) {
//                            txNvoCodProd.setFocus(true);
                            }
                        } else if (Messagebox.ON_NO.equals(event.getName())) {
                            getLimpiaCamposNvoProd();
                            getLimpiaCamposNvoNumerico();
                        }
                    }
                });
                
            } else {
                
                td.setDv_NomTipDcto(nvoTD);
                td.setDi_IdTipDctoPJ(nvoCodTD);
                td.setBit_Activo(estadoTD);
                
                try {
                    
                    response = conexTD.insertTipoDocumento(td);
                    
                    if (response) {
                        Messagebox.show("Tipo de Documento agregado correctamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ON_OK, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_OK.equals(event.getName())) {
                                    if ("".equals(nvoTD)) {
//                                    txNvoProd.setFocus(true);
                                    } else if ("".equals(nvoTD)) {
//                                    txNvoCodProd.setFocus(true);
                                    }
                                    
                                }
                            }
                        });
                        
                        listTipoDocumentoStatus = generateStatusList(conexTD.getListTipoDocumento(getFiltroNombre()));
                    }
                } catch (Exception e) {
                    Messagebox.show("Error al agregar un Tipo de Documento, intentelo nuevamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ERROR, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            if (Messagebox.ON_OK.equals(event.getName())) {
                                getLimpiaCamposNvoProd();
                                getLimpiaCamposNvoNumerico();
                                
                            }
                        }
                    });
                }
                
            }
        }
        
    }

    /**
     * *************** INICIO EXPORT GRID
     *
     ********************
     * @param gridList
     * @throws java.lang.Exception
     */
    @Command
    public void exportGridToExcel(@BindingParam("gridList") final Grid gridList) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024 * 10);
        gridList.renderAll();
        String fNew = df.format(new Date());
        System.out.println(gridList.getListModel().getSize() + "  size data");
        ExcelExporter exporter = new ExcelExporter();
        exporter.export(gridList, out);
        AMedia amedia = new AMedia("Informe_Custodia_" + fNew + ".xlsx", "xls", "application/file", out.toByteArray());
        Filedownload.save(amedia);
        out.close();
    }

    /*
    @Command
    @NotifyChange({"listUbicacionTblHitos", "listOficinasTblHitos", "listRegionTblHitos", "listBancaTblHitos", "listTipoDocumentoTblHitos"})
    public void filtrosDinamicos(@BindingParam("fini") final Datebox fini, @BindingParam("ffin") final Datebox ffin, @BindingParam("ofi") final Combobox ofi, @BindingParam("reg") final Combobox reg, @BindingParam("ban") final Combobox ban, @BindingParam("txRut") final Textbox txRut, @BindingParam("txtDMI") final Textbox txDMI, @BindingParam("txtDMF") final Textbox txDMF, @BindingParam("txtNOF") final Textbox txtNOF, @BindingParam("txtNOp") final Textbox txtNOp, @BindingParam("CBProd") final Combobox product) {
        fechaI = fini.getValue();
        fechaF = ffin.getValue();
        //ubicacion = ubi.getVflex();
        oficina = ofi.getValue().trim();
        region = reg.getValue().trim();
        banca = ban.getValue().trim();
        rut = txRut.getValue().trim();
        String dmiStr = txDMI.getValue().trim();
        String dmfStr = txDMF.getValue().trim();
        n_of = txtNOF.getValue().trim();
        n_op = txtNOp.getValue().trim();
        prod = product.getValue().trim();

        for (UbicacionTblHitosEnt ubic : getSelectedObjects()) {
            System.out.println("ubic ====>>>>> " + ubic.getUbicacion());
            listTblComboUbicacionFiltro.add(ubic);
        }

        //dias de mora a entero
        if (dmiStr.equals("")) {
            dmi = 0;
        } else {
            dmi = Integer.parseInt(dmiStr);
        }
        if (dmfStr.equals("")) {
            dmf = 0;
        } else {
            dmf = Integer.parseInt(dmfStr);
        }

        System.out.println("TblHitosController.filtrosDinamicos fechaI => " + fechaI);
        System.out.println("TblHitosController.filtrosDinamicos fechaF => " + fechaF);
        System.out.println("TblHitosController.filtrosDinamicos oficina => " + oficina);
        System.out.println("TblHitosController.filtrosDinamicos region => " + region);
        System.out.println("TblHitosController.filtrosDinamicos banca => " + banca);
        System.out.println("TblHitosController.filtrosDinamicos producto => " + prod);

        int ResResta = 1;
        if (fechaI != null && fechaF != null) {
            String formatFIni = simpleDateFormat.format(fechaI);
            String formatFFin = simpleDateFormat.format(fechaF);
            formatFIni = formatFIni.replace("-", "");
            formatFFin = formatFFin.replace("-", "");
            Long FIni = Long.parseLong(formatFIni);
            Long FFin = Long.parseLong(formatFFin);
            ResResta = (int) (FFin - FIni);
        }

        System.out.println("ResResta => " + ResResta);

        try {
            conexTblHitos.getListTipoDocumento(fechaI, fechaF, listTblComboUbicacionFiltro, oficina, region, banca, rut, dmi, dmf, n_of, n_op, prod);
            listTblComboUbicacionFiltro.clear();

        } catch (WrongValueException e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } catch (Exception e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } finally {

        }

    }
     */
}
