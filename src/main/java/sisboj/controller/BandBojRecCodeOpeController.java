/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;


import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import config.MvcConfig;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.*;
import org.zkoss.zul.*;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;


/**
 *
 * @author aescobar
 */
@SuppressWarnings("serial")
public class BandBojRecCodeOpeController extends SelectorComposer<Window> {
    
    @Wire
    Grid grid;
    
    @Wire
    Grid grid_DetSol;
    
    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
    
    @Wire
    Textbox glosaNom;
    
    @Wire
    Div id_addDocNew;
    
    @Wire
    Panel id_addPenPanel;
    
    @Wire
    Panel id_addDocPanel;
    @Wire
    Combobox cmb_EstDcto;
    
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_TioAux2;
    
    @Wire
    Combobox cmb_TipoDocumento;
    
    @Wire
    Image _idCodeBarRear2;
    
    @Wire
    Label _yyyy;
    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;
    
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    
    @Wire
    Textbox txt_textObs;
    
    @Wire
    Div id_detalleSolicitud;
    Window capturawin;
    @Wire
    Div _divFridDoc;
    
    @Wire
    private List<String> cmb_TipDcto;
    
    String ValueIdOperaciones;
    String unicacion;
    
    BitMatrix encodeCode128 = null;
    private BufferedImage bufferedImage128 = null;
    private Image img123 = null;
//    @Wire
//    Button id_addDocumentos;
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    private BandejaCustodia baj;
    List<Ubicacion> lUbi;
    List<BandejaCustodia> nn = null;
    private EventQueue eq;
    UbicacionJDBC ubicacionJDBC;
    private int id_solicitud = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    ///Informacion del Usuario
    String UsuarioCustodia;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private EstadoJDBC _estado;
    
    public BandBojRecCodeOpeController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        lUbi = new ArrayList<Ubicacion>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();
    }
    
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);
        
        Execution execution = Executions.getCurrent();
        String ed = execution.getParameter("nOpera");
        String ed2 = execution.getParameter("callback2");
        System.out.println("ED: " + ed);
        System.out.println("ED2: " + ed2);

        // CargaPag();
        //CargaEventos();
        _idCodeBarRear2.setContent(generateCodeOper(ed));
        _yyyy.setValue(ed);
        
        
        
        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(Integer.parseInt(ed2));
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        
        
        
    }
    
    public BufferedImage generateCodeOper(String code) throws SQLException, WriterException {

        // for (final BandejaDetalle nnn : DetSolModelList) {
        /*Encode */
        encodeCode128 = new Code128Writer().encode(code, BarcodeFormat.CODE_128, 150, 80, null);
        bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
        /*fin encode*/

        //BandejaDetalle bjdet = new BandejaDetalle();
        //bjdet.setBufferedImage128(bufferedImage128);
        //}
        return bufferedImage128;
        
    }
    
}
