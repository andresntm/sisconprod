/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import sisboj.entidades.ProdTipoDoc;

/**
 *
 * @author EXVGUBA
 */
public class ProdTipoDctoStatus {

    private ProdTipoDoc ptd;
    private boolean editingStatus;

    public ProdTipoDctoStatus(ProdTipoDoc lc, boolean editingStatus) {
        this.ptd = lc;
        this.editingStatus = editingStatus;
    }

    public ProdTipoDoc getProdTipoDocStatus() {
        return ptd;
    }

    public boolean getEditingStatus() {
        return editingStatus;
    }

    public void setEditingStatus(boolean editingStatus) {
        this.editingStatus = editingStatus;
    }
}
