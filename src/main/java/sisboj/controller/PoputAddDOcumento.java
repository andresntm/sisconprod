/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.TipDcto;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;


/**
 *
 * @author esilves
 */
@SuppressWarnings("serial")
public class PoputAddDOcumento extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

    @Wire
    Window win_AdjuntaDcto;

    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_FecDetSolIng;
    @Wire
    Button ButtonGuardarDoc;
    @Wire
    String FlagFirmaDelegado;
    @Wire
    Checkbox id_chechApoderado;
    @Wire
    Combobox cmb_TipDcto;
    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Vlayout btn_idvlayoutinfodoc;
    @Wire
    Vlayout _idvlayoutinfodoc;

    private EventQueue eq;
    private TipDctoJDBC _tipoDoc;
    private EstadoJDBC _estado;
    private ListModelList<TipDcto> _tipoDoctoList;
    private List<TipDcto> _listtipoDocto = new ArrayList<TipDcto>();
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private Documento _nuevodocto;
    private final DocumentoJDBC _docto;
    private final OperacionesJDBC _op;
    private BandejaDetalle bandDet;
    private int numDctsOpAct = 0;
    private boolean bNuevo = false;
    private Operaciones ope;
    private int Id_Operacion;

    public PoputAddDOcumento() throws SQLException {
        _tipoDoc = new MvcConfig().tipDctoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        _docto = new MvcConfig().documentosJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        try {
            CreaPag();
        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            win_AdjuntaDcto.detach();
        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            win_AdjuntaDcto.detach();
        }
    }

    private void CreaPag() {
        final Execution exec = Executions.getCurrent();

        bandDet = new BandejaDetalle();

        if (exec.getArg().get("Seleccion") != (null)) {
            bandDet = (BandejaDetalle) exec.getArg().get("Seleccion");
        } else {
            return;
        }

        txt_OperDcto.setValue(bandDet.getnOperacion());
        txt_RutDcto.setValue(bandDet.getRutCompleto());
        txt_NombreDcto.setValue(bandDet.getNombrecomp());
        txt_TioAux.setValue(bandDet.getDv_IdTipOpe());
        txt_FecDetSolIng.setValue(bandDet.getDdt_fechaIngreso().toString());
        List list2;
        List list3;
        ListModelList lm2;
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.listTipDcto();
        _tipoDoctoList = new ListModelList<TipDcto>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        //   try{
        list2 = new ArrayList();
        list3 = new ArrayList();
        // country.setModel(ListModels.toListSubModel(new ListModelList(getAllItems())));
        for (final TipDcto tipodocto : _listtipoDocto) {
            list2.add(tipodocto.getDv_NomTipDcto());
        }

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        lm2 = new ListModelList(list2);
        lm3 = new ListModelList(list3);

        cmb_EstDcto.setModel(lm3);
        cmb_TipDcto.setModel(lm2);
        cmb_TipDcto.setPlaceholder("Seleccione Tipo dcto.");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");

    }

    private void logicaTioAux() {
        String dv_idTipOpe;

        try {

            dv_idTipOpe = bandDet.getDv_IdTipOpe();

        } catch (Exception Ex) {

        }

    }

    @Listen("onClick = #closeButton")
    public void close() {
        win_AdjuntaDcto.detach();
    }

    @Listen("onClick = #ButtonGuardarDoc")
    public void GuardarDocumento() {

        try {

            final HashMap<String, Object> map = new HashMap<String, Object>();

            int id_tipDcto = 0;
            int id_EstDcto = 0;
            String tipodoctodesc = cmb_TipDcto.getValue();
            String estado = cmb_EstDcto.getValue();
            this._nuevodocto = new Documento();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date();

            ope = _op.getOperaciones(txt_OperDcto.getValue());

            //se retorna a la pag principal
            Id_Operacion = ope.getDi_IdOperacion();
            boolean checkapbanco = id_chechApoderado.isChecked();

            // Agregagos el Nuevo Docuemnto encontrado
            for (final TipDcto tipodocto : _listtipoDocto) {
                if (tipodocto.getDv_NomTipDcto().equals(tipodoctodesc)) {
                    id_tipDcto = tipodocto.getDi_IdTipDcto();
                }
            }

            if (id_tipDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar un tipo de documento para continuar.");
                cmb_TipDcto.setFocus(true);
                return;
            }

            for (final Estado est : _estadoDocto) {
                if (est.getDv_NomEstado().equals(estado) && est.getDv_DetEstado().equals("Operaci�n C/Documento en custodia")) {
                    id_EstDcto = est.getDi_IdEstado();
                }
            }

            if (id_EstDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar el estado del documento para continuar.");
                cmb_EstDcto.setFocus(true);
                return;
            }

            if (checkapbanco) {
                Messagebox.show("Sr(a). Usuario(a), recuerde que los pagares firmados por apoderado banco deben ir con su contrato anexado.");
            }

            this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
            this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
            this._nuevodocto.setDb_checkOk(true);
            this._nuevodocto.setDi_fk_IdUbi(2);
            this._nuevodocto.setDv_Glosa(txt_GlosaDcto2.getValue());
            this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
            this._nuevodocto.setDi_fk_IdEstado(id_EstDcto);
            this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());
            this._nuevodocto.setDi_fk_TipoDcto(id_tipDcto);

            // Messagebox.show("DatosCamputados::::checkapbanco[" + checkapbanco + "]txt_OperDcto.getValue()[" + txt_OperDcto.getValue() + "]txt_GlosaDcto2.getValue()[" + txt_GlosaDcto2.getValue() + "]cmb_TipDcto.getValue()[" + cmb_TipDcto.getValue() + "]id_documento[" + id_documento + "]ope.id[" + ope.getDi_IdOperacion() + "]");
            boolean insert = _docto.insert(_nuevodocto);

            // Messagebox.show("DatosCamputados:::");
            if (insert) {
                bNuevo = true;
                //window.setVisible(false);

                //Apoderado Banco
                //Messagebox.setTemplate("windowconfirm/windowconfirm.zul");
                Messagebox.show("Se Agreg� Correctamente el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            bNuevo = false;
                        }
                    }
                });

                if (bNuevo) {
                    numDctsOpAct += 1;
                } else {
                    numDctsOpAct = numDctsOpAct;
                }
                
                System.out.println("PoputAddDOcumento.GuardarDocumento bNuevo => "+bNuevo);
                System.out.println("PoputAddDOcumento.GuardarDocumento numDctsOpAct => "+numDctsOpAct);
                System.out.println("PoputAddDOcumento.GuardarDocumento Id_Operacion => "+Id_Operacion);
                System.out.println("PoputAddDOcumento.GuardarDocumento bandDet => "+bandDet);

                map.put("bNuevo", bNuevo);
                map.put("numerodocsOperActual", numDctsOpAct);
                map.put("idOepracion", Id_Operacion);
                map.put("detSeleccion", bandDet);
                eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", ButtonGuardarDoc, map));

                win_AdjuntaDcto.detach();

            } else {
                Messagebox.show("Problemas al Agregar el Documento. ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            //OK is clicked
                            Executions.sendRedirect("/sisboj/index");
                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            //Cancel is clicked
                        }
                    }
                }
                );

            }

        } catch (WrongValueException ex) {
            Messagebox.show("Problemas al Agregar el Documento. \n" + ex.toString(), "Guardar Documento", Messagebox.OK, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        Executions.sendRedirect("/sisboj/index");
                    }
                }
            }
            );
        }

    }

}
