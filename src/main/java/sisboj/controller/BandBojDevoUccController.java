/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;



import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Auxheader;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Detail;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;




import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.util.Clients;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import config.MvcConfig;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.System.out;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.PrinterStateReason;
import javax.print.attribute.standard.PrinterStateReasons;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.zkoss.util.media.AMedia;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.JerarquiaSolEnt;
import sisboj.entidades.Operaciones;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.Productos;
import sisboj.entidades.Solicitud;
import sisboj.entidades.TipDcto;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.JerarquiaSolJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.ProductosJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;
import sisboj.utilidades.DiccionarioCodeZpl;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.PrinterInfo;
import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.attribute.standard.PrinterLocation;
import javax.print.attribute.standard.PrinterMakeAndModel;
import javax.print.attribute.standard.PrinterState;

/**
 *
 * @author esilvestre
 */
@SuppressWarnings("serial")
public class BandBojDevoUccController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid gridPadre;

    @Wire
    Grid grid_DetSol;

    @Wire
    Auxheader auxColDet;

    @Wire
    Grid grid2;
    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;

    @Wire
    Textbox id_codebar;

    @Wire
    Textbox glosaNom;

//    @Wire
//    Detail onOpenFal;
    @Wire(".folder")
    Detail onOpenFal; // wire to button 2

    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
    @Wire
    Textbox txt_RutDcto2;
    @Wire
    Textbox txt_NombreDcto2;
    @Wire
    Textbox txt_OperDcto2;
    @Wire
    Panel id_addDocPanel;
    @Wire
    Panel id_addPenPanel;
    @Wire
    Div id_detalleSolicitud;
    @Wire
    Textbox aux;
    @Wire
    Div _divFridDoc;
    @Wire
    Button id_addDocumentos;
    @Wire
    Div coder;

    @Wire
    Image _idCodeBarRear2;

    @Wire
    Image _idCodeBarRear23;

    @Wire
    Button btnrefresh;

    @Wire
    Button btnCerrarAll;

    @Wire
    Label _yyyy;

    @Wire
    Label _yyyyy;

    @Wire
    Label viewrutcliente1;
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_TioAux2;
    @Wire
    Textbox txt_cantDocs;
    @Wire
    Div id_addDocNew;
    @Wire
    Combobox cmb_TipoDocumento;
    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;
    @Wire
    Textbox txt_textObs;
    //Inicio pendientes
    @Wire
    Textbox txtObservaciones;

//    int idOpe2=0;
    String ValueIdOperaciones;
    String unicacion;
    @Wire
    Window capturawin;

    @Wire
    Iframe iframe;

    @Wire
    private Window idWinController;
    BitMatrix encodeCode128 = null;
    BitMatrix encodeCode128Sol = null;
    BitMatrix encodeCode128SolP = null;

    BufferedImage bufferedImage128 = null;
    BufferedImage bufferedImage128Sol = null;
    BufferedImage bufferedImage128SolP = null;

    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    List<TipDcto> _listtipoDoctox = new ArrayList<TipDcto>();
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private DetSolicitudJDBC conexDetSol;
    private ProductosJDBC conexProdCod;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaCustodia> bandejacustodiaPad;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;

    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipoDocumentoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private int id_solicitud = 0;
    private String nameProductCode;
    private String fecSolicCode;
    private EventQueue eq;
    private boolean bNuevo;
    private int idOperRetorno = 0;
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    private EstadoJDBC _estado;
    private int Id_Operacion;
    private int numDctsOpAct = 0;
    private List<ProdTipoDoc> prTipoDoc;
    List<BandejaDetalle> nn = null;
    List<BandejaDetalle> nnCierra = null;
    private int oPer;
    private int nDoc;
    private int docEs;
    private int activePage;
    private int cantOpera;
    ///Informacion del Usuario
    String UsuarioCustodia;
    private Documento _nuevodocto;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    boolean cambioEstado = false;
    Solicitud crearHijo;
    Solicitud newHijosol;
    boolean updPadre = false;
    boolean updOperNvaSol = false;
    UbicacionJDBC ubicacionJDBC;
    int solConPendientes;
    List<Ubicacion> lUbi;
    List<Productos> lprod;
    int contSolCompletada;
    boolean responseCierreSol;
    String SolCompletada2;
    int cantOpeComplet2;
    int cantOpePend2;
    int docsEsp;
    int veri = 0;
    int idSolHijo;
    JerarquiaSolJDBC jerarquiaSolJDBC;
    PDStream pss;

    float rimpagString;
    float rimpagImage;

    public BandBojDevoUccController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        conexProdCod = new MvcConfig().productosJDBC();
        lUbi = new ArrayList<Ubicacion>();
        lprod = new ArrayList<Productos>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();
        jerarquiaSolJDBC = new MvcConfig().jerarquiaSolJDBC();

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {

        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        CargaPag();
        CargaEventos();
        CargaPagSort(0);

    }

    public void CargaPag() throws SQLException {

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        try{
        this.listSol = conexSol.getSolDevueltas(UsuarioCustodia);

        iframe.setVisible(false);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();
        }catch(Exception ex){
            System.out.println("Exception Carga Inicial Solicitudes BandBojDevoUccController  --> " + ex);
        }
        int cuantos = grid.getRows().getChildren().size();

    }

    public void irSort(int id_label) throws SQLException {

        System.out.println("ID_LABEL --> " + id_label);

        switch (id_label) {
            case 1:
                CargaPagSort(1);
                break;
            case 2:
                CargaPagSort(2);
                break;
            case 3:
                CargaPagSort(3);
                break;
            case 4:
                CargaPagSort(4);
                break;
            case 5:
                CargaPagSort(5);
                break;
            /*else if(id_label==7){
        CargaPagSort(7);
        }*/
            case 6:
                CargaPagSort(6);
                break;
            case 8:
                CargaPagSort(8);
                break;
            default:
                break;
        }

    }

    /*Metodo Carga BandejaNuevaPadre Inicial*/
    public ListModel<BandejaCustodia> getCustodia() {

        try {
            bandejacustodiaPad = new ListModelList<BandejaCustodia>(conexSol.getSolDevueltas(UsuarioCustodia));
            //gridPadre.setModel(bandejacustodiaPad);
            List<BandejaCustodia> bandeja = conexSol.getSolDevueltas(UsuarioCustodia);
            List<BandejaCustodia> bandeja2 = bandeja;
            int num_sol;

            for (BandejaCustodia b : bandeja2) {

                num_sol = b.getNumsol();
                System.out.println("**BandejPadre " + num_sol);

                bandejacustodiaPad = new ListModelList<BandejaCustodia>(conexSol.getSolNuev_Pad(UsuarioCustodia, num_sol));
            }

        } catch (SQLException ex) {
            Logger.getLogger(BandCustEntController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ListModelList<BandejaCustodia>(bandejacustodiaPad);
    }

    public void buscarOper() throws SQLException {

        String glosanom = glosaNom.getValue();
        System.out.println("glosanom: " + glosanom);

        conexDetSol = new MvcConfig().detSolicitudJDBC();

        if (glosanom == "" || glosanom.isEmpty()) {
            DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
            grid_DetSol.setModel(DetSolModelList);
        } else {
            this.listDet = conexDetSol.listDetBuscaOperSolicitud(glosanom, id_solicitud);
            DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
            grid_DetSol.setModel(DetSolModelList);
        }

    }

    public void irDetalle(int id_det) throws SQLException {

        System.out.println("IR DETALLE --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }

    public void CargaPagSort(int label_cont) throws SQLException {
        for (Component column : grid.getColumns().getChildren()) {

            column.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Column row = (Column) arg0.getTarget();

                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    if (rowSelected != null && rowSelected) {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", true);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", false);
                        row.setSclass("z-row-background-color-on-select-sort");

                    } else {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        row.setSclass("z-row-background-color-on-select-sort");

                    }

                }
            });
        }

        this.listSol = conexSol.getSolDevueltas_Sort(UsuarioCustodia, label_cont);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

        CargaEventos();

    }

    private void CargaEventos() throws SQLException {
        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                    id_solicitud = banc.getNumsol();
                    nameProductCode = banc.getProducto();
                    fecSolicCode = banc.getFechaRegistro();
                    cantOpera = banc.getNumDctos();

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    grid_DetSol.setModel(DetSolModelList);
                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                    coder.setVisible(false);
                    _idCodeBarRear2.setVisible(false);
                    _yyyy.setValue("");

                    /*Metodo boton cerrar solicitud*/
                    cierreSolok(id_solicitud);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);

                        btnCerrarAll.setVisible(false);

                        onOpenFal.setOpen(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");

                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }
            });
        }
    }

    @Listen("onClick = #btnCerrarAll")
    public void cerrarAllSol() throws SQLException {

        Messagebox.show("Operacion: " + id_solicitud + " sera Cerrada y Enviada a Bandeja Terminadas.Desea Continuar?", "Cerrar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    System.out.println("Entra a cerrar solicitud");
                    /*Metodo cerrar solicitud*/
                    System.out.println("Cierra Solicitud n�: " + id_solicitud);
                    String SolCompletada = "";

                    /**/
                    List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
                    lbd = getListDet(id_solicitud);

                    solConPendientes = 0;
                    contSolCompletada = 0;
                    for (int i = 0; i < lbd.size(); i++) {
                        if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                            // SolCompletada = "No";
                        } else {
                            if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                                solConPendientes = solConPendientes + 1;
                            }
                            if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                                // SolCompletada = "Si";
                                contSolCompletada = contSolCompletada + 1;
                            }
                        }

                    }

                    if ((solConPendientes + contSolCompletada) == lbd.size()) {
                        SolCompletada = "Si";

                    } else {
                        SolCompletada = "No";
                    }

                    String[] respStr = new String[3];
                    respStr = new String[3];
                    respStr[0] = SolCompletada;
                    respStr[1] = String.valueOf(contSolCompletada);
                    respStr[2] = String.valueOf(solConPendientes);

                    cierraSolicitud(respStr);

                    // updateDetalleGrid(id_solicitud);
                    solConPendientes = 0;
                    contSolCompletada = 0;
                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                    System.out.println("Cancela");
                }
            }
        });

    }

    public void cierreSolok(int id_sol) throws SQLException {

        int countPen = 0;
        List<Estado> codEstadoPencus;
        List<Estado> codEstadoCdscus;

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

        codEstadoPencus = estJDBC.getIdEstadoByCodigo("PENCUS");
        codEstadoCdscus = estJDBC.getIdEstadoByCodigo("CDSCUS");

        nnCierra = conexDetSol.listDetSolicitud(id_sol);
        int solConPendientess = 0;
        int contSolCompletadass = 0;

        for (final BandejaDetalle nnn : nnCierra) {
            int coda = nnn.getDi_fk_idestado();

//            if (coda == codEstadoPencus.get(0).getDi_IdEstado() || coda == codEstadoCdscus.get(0).getDi_IdEstado()) {
//               
//                countPen++;
//                
//                
//            }
            /**/
            if (coda == codEstadoPencus.get(0).getDi_IdEstado()) {
                solConPendientess = solConPendientess + 1;
            }
            if (coda == codEstadoCdscus.get(0).getDi_IdEstado()) {
                // SolCompletada = "Si";
                contSolCompletadass = contSolCompletadass + 1;
            }

        }

        System.out.println("cierreSolok contSolCompletadass : " + contSolCompletadass);
        System.out.println("cierreSolok solConPendientess : " + solConPendientess);
        System.out.println("cierreSolok cantOpera :" + cantOpera);

        if ((solConPendientess + contSolCompletadass) == cantOpera) {

            if (solConPendientess == cantOpera) {
                btnCerrarAll.setVisible(false);
            } else {
                btnCerrarAll.setVisible(true);
            }

        } else {
            btnCerrarAll.setVisible(false);
        }

//        if (countPen == cantOpera) {
//            btnCerrarAll.setVisible(true);
//        } else {
//            btnCerrarAll.setVisible(false);
//        }
    }

    public void MatchDoc(int id_sol, final int Oper, int numDoc, int docEsp) throws SQLException {

        final List<BandejaDetalle> bandeja = getListDet(id_solicitud);

        boolean resp = false;
        List<BandejaDetalle> bandeja2 = bandeja;

        int numDocs = 0;
        int numDocsEsp = 0;
        String estProc = "Procesada";

        for (BandejaDetalle b : bandeja2) {
            if (Oper == b.getIdOperacion()) {
                numDocs = b.getNumero_docs();
                numDocsEsp = b.getNumero_docs_esperado();

                if (numDocs == numDocsEsp) {
                    id_addDocNew.setVisible(false);
                } else {
                    id_addDocNew.setVisible(true);
                }

            }

        }
        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(Oper);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

//        _conListDOc = new MvcConfig().documentosJDBC();
//        this._listDOc = _conListDOc.listDocumento(Oper);
//        _listModelDOc = new ListModelList<Documento>(_listDOc);
//
//        grid_Dctos.setModel(_listModelDOc);
//        _divFridDoc.setVisible(true);
        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

    }

    public void updateGridDoc(int id_operacion, int countNumDoc, int cantDocsEsperados) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

        id_addDocNew.setVisible(countNumDoc >= cantDocsEsperados ? false : true); // se debe corregir al nuevo proceso

    }

    public void updateDetalleGrid(int id_sol) throws SQLException {

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);

        grid_DetSol.setModel(DetSolModelList);

        grid_DetSol.setVisible(false);
        grid_DetSol.setVisible(true);

    }

    public void updateDetalle(int id_sol) throws SQLException {

        int id_soli = id_sol;
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_soli));

        grid_DetSol.setModel(DetSolModelList);
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
        id_addPenPanel.setVisible(false);
        id_detalleSolicitud.setVisible(true);

        Clients.scrollIntoView(id_detalleSolicitud);

    }

    /**
     * Se utiliza para cargar el detalle y trabajarlo con el hijo de esta
     * ventana
     *
     * @param id_sol
     */
    public void CargaBandejaDetalle(int id_sol, int numDocEsperados) {

        id_addPenPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        txt_TioAux.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public void CargaBandejaDetallePend(int id_sol, int numDocEsperados) {

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();
        id_addPenPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addPenPanel.setVisible(true);
        txt_RutDcto2.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto2.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto2.setValue(detSeleccion.getnOperacion());
        txt_TioAux2.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados2.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public void setValuesDocsNew(int id_sol, int countNumDoc, int cantDocEsperados) throws SQLException {

        CargaBandejaDetalle(id_sol, cantDocEsperados);

//        id_addDocNew.setVisible(countNumDoc >= 2 ? false : true);
//        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc, cantDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);

        oPer = ope.getDi_IdOperacion();
        nDoc = countNumDoc;
        docEs = cantDocEsperados;

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setValuesDocsPend(int id_sol, int countNumDoc, int cantDocEsperados) throws SQLException {

        CargaBandejaDetallePend(id_sol, cantDocEsperados);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc, cantDocEsperados);
        }

        Clients.scrollIntoView(id_addPenPanel);
    }

    public void setValuesDocsComplete(int id_sol, int NumDocEsperados, int NumDocumentos) throws SQLException {

        CargaBandejaDetalle(id_sol, NumDocEsperados);

//        id_addDocumentos.setVisible(false);
        id_addDocNew.setVisible(false);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());

            System.out.println("ope.getDi_IdOperacion => " + ope.getDi_IdOperacion());
            System.out.println("detSeleccion.getNumero_docs() => " + ope.getDi_IdOperacion());
            System.out.println("NumDocEsperados => " + NumDocEsperados);

            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs(), NumDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);
        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setButtonVisible() {

//        id_addDocumentos.setVisible(true);
        id_addDocNew.setVisible(true);

    }

    public void deleteDocs(final int id_operacion) throws SQLException {
        final List<BandejaDetalle> bandeja = getListDet(id_solicitud);
//        idOpe2=id_operacion;
        Messagebox.show("�Desea eliminar documento? ", "Eliminar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    boolean eliminado = false;
                    List<BandejaDetalle> bandeja2 = bandeja;
                    eliminado = _conListDOc.eliminarDocumento(id_operacion);
                    if (eliminado) {
                        Messagebox.show("Documento eliminado correctamente", "", Messagebox.OK, Messagebox.INFORMATION);
                        refrescaGridDetalle();
                        btnCerrarAll.setVisible(false);
                    }

                    int numDocs = 0;
                    int numDocsEsp = 0;
                    int idDetalleOpe = 0;

                    for (BandejaDetalle b : bandeja2) {
                        if (id_operacion == b.getIdOperacion()) {
                            numDocs = b.getNumero_docs();
                            numDocsEsp = b.getNumero_docs_esperado();
                            idDetalleOpe = b.getIdDetSol();
                        }
                    }

                    if (eliminado) {
                        conexSol.cambioEstadoSolDet(idDetalleOpe, "R", "Incompleta");
                    }
//                    setValuesDocsNew(id_solicitud, numDocs, numDocsEsp);
                    grid_DetSol.setModel(DetSolModelList);
                    updateGridDoc(id_operacion, numDocs, numDocsEsp);
                    getListDet(id_solicitud);
                    coder.setVisible(false);
                    _idCodeBarRear2.setVisible(false);
                    _yyyy.setValue("");
                    id_codebar.setValue("");
                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {

                }
            }
        }
        );

    }

    public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        return listDet;
    }

    public void refrescaGridDetalle() throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
//               detSeleccion 
    }

    public void irBanPendientes(int id_sol, String numOpe) throws SQLException {
        Map<String, Object> arguments = new HashMap<String, Object>();

        System.out.println("BandCustEntController.irBanPendientes id_solicitud => " + id_solicitud);
        System.out.println("BandCustEntController.irBanPendientes numOpe => " + numOpe);

//        detSeleccion.setDi_idSolicitud(id_solicitud);
//        detSeleccion.setnOperacion(numOpe);
        arguments.put("idSolicitud", id_solicitud);
//        arguments.put("id_solicitud", id_solicitud);
        String template = "custodia/BandejaPendientes.zul?idSolicitud=" + String.valueOf(id_solicitud);
        //String template = "custodia/BandejaPendientes.zul";
        //  System.out.println("Prueba fellow{" + capturawin.getParent().getParent().getFellows().toString() + "}");

        Include inc = ((Include) capturawin.getParent().getParent().getFellow("pageref"));

        try {
            inc.setSrc(template);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
    @Listen("onClick = #id_addDocumentos")
    public void submit() {

        Map<String, Object> arguments = new HashMap<String, Object>();

        detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("Seleccion", detSeleccion);

        String template = "custodia/detallesPoput.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

     */
    // este metodo Permite Refrescar de Forma Arcaida, Forzando la ejecucion del boton por comando
//    @Listen("onClick = #btnrefresh")
//    public void refresh() {
//        _divFridDoc.setVisible(true);
//        //this.updateGridDoc();
//    }
    @Listen("onClick = #id_procesarLotesss")
    public void readCode() {

        System.out.println("PROCESA POR LOTES");
        Map<String, Object> arguments = new HashMap<String, Object>();

        ListModelList<BandejaDetalle> DetSolModelList2;
        DetSolModelList2 = new ListModelList<BandejaDetalle>(listDet);

        System.out.println("ID SOL A MANDAR " + id_solicitud);
        // arguments.put("orderItems", DetSolModelList2);
        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("txt_RutDcto", txt_RutDcto.getValue());
        arguments.put("txt_NombreDcto", txt_NombreDcto.getValue());
        arguments.put("txt_OperDcto", txt_OperDcto.getValue());
        arguments.put("rut_Cli", 15.014544);
        arguments.put("id_solicitud", id_solicitud);

        String template = "custodia/BarCodeLotes.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonGuardarDoc");
        //final Textbox nn = (Textbox) window.getFellow("txt_hiddenIdOper");
        //final Textbox CodOper = (Textbox) window.getFellow("txt_OperDcto");
        //final Textbox AddNewValue = (Textbox) window.getFellow("txt_hiddenIdExito");

//        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
//            @Override
//            public void onEvent(Event event) throws SQLException {
//
//                // String idoper=
//                window.detach();
//                int NumeroDocs = 0;
//                updateDetalleGrid(id_solicitud);
//                int numdocsagregados = conexDetSol.NumeroDocumentosAgregadosSolicitud(id_solicitud);
//                // ojo aca el valor de getNumeroDocumentosSolicitud se agrego de forma estatica,se debe arreglar
//                int numerototaldedocs = conexSol.getNumeroDocumentosSolicitud(id_solicitud);
//
//                if (numdocsagregados == numerototaldedocs) {
//
//                    if (conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerrado")) {
//                        Messagebox.show("Se Cerro correctamente la Solicitud ", "Terminar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
//                                new org.zkoss.zk.ui.event.EventListener() {
//                            public void onEvent(Event e) {
//                                if (Messagebox.ON_OK.equals(e.getName())) {
//
//                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
//
//                                }
//                            }
//                        }
//                        );
//                    } else {
//                        Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
//                                new org.zkoss.zk.ui.event.EventListener() {
//                            public void onEvent(Event e) {
//                                if (Messagebox.ON_OK.equals(e.getName())) {
//                                    //OK is clicked
//                                    // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
//                                    Executions.sendRedirect("/sisboj/index");
//                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
//                                    //Cancel is clicked
//                                    // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
//                                }
//                            }
//                        });
//
//                    }
//
//                }
//
//                //  window.print(buildParameters(window),planner);
//            }
//        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick = #id_procesarLotes")
    public void codereader() throws SQLException {

        int getIdEst = 275;
        //cantOpera
        int countPen = 0;

        if (coder.setVisible(true)) {

            coder.setVisible(false);
            _idCodeBarRear2.setVisible(false);
            _yyyy.setValue("");
            id_codebar.setValue("");
        } else if (coder.setVisible(false)) {
            coder.setVisible(true);
            coder.setFocus(true);

            /*CODIGO PROCESAR POR LOTES TODAS LAS OPERACIONES EN PENDIENTE*/
            List<Estado> codEstado;
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
            nn = conexDetSol.listDetSolicitud(id_solicitud);

            for (final BandejaDetalle nnn : nn) {
                int coda = nnn.getDi_fk_idestado();

                if (coda == codEstado.get(0).getDi_IdEstado()) {
                    countPen++;
                }
            }
            System.out.println("CountPent: " + countPen);
            System.out.println("CantOpera: " + cantOpera);

            if (countPen == cantOpera) {

                Messagebox.show("Operaciones en Pendiente, Desea Reprocesar Lote?", "ReProcesar lote", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            System.out.println("Entra a Reprocesar");
                            /*List<BandejaDetalle> nne;*/
                            for (final BandejaDetalle bdj : nn) {
                                System.out.println("Cambiando Estado Operacion: " + bdj.getnOperacion() + " --> Estado actual: " + bdj.getDi_fk_idestado());
                                //conexDetSol.updateOpeSol(id_solicitud,"di_fk_IdEstado",141);
                                conexSol.cambioEstadoSolDet(bdj.getIdDetSol(), "R", "");
                            }
                            //conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);
                            updateDetalleGrid(id_solicitud);

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            System.out.println("Cancela");
                        }
                    }
                });

            }

        }/*Else If*/

    }

    public void codereaderChange(String Barcode) throws WriterException, SQLException, FormatException, ChecksumException, NotFoundException, IOException, ParseException {

        System.out.println("Barcode: " + Barcode);

        try {

            int idFkEstado = 0;
            String idOperx = "";
            List<Estado> codEstado;
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

            nn = conexDetSol.listDetSolicitud(id_solicitud);

            boolean result = conexDetSol.getDetSolicitud(id_solicitud, Barcode);

            if (result == false) {
                Messagebox.show("Puede Asociar e imprimir Codigo para asociar a una del listado o descartarla.", "Operacion No Encontrada !!", Messagebox.OK, Messagebox.ERROR);
                return;
            }

            System.out.println("**RESULT " + result);
            System.out.println("**BARCODE " + Barcode);

            //Preguntar si existe el documentos en documentos
            int _numDocs = _docto.getDocumentoPorOperacion(Barcode);

            System.out.println("_NUMDOCS " + _numDocs);

            int _numdoctoEsperado = 0;
            for (final BandejaDetalle nnn : nn) {
                if (nnn.getnOperacion().equals(Barcode)) {
                    _numdoctoEsperado = nnn.getNumero_docs_esperado();
                    idFkEstado = nnn.getDi_fk_idestado();
                    idOperx = nnn.getnOperacion();

                }
            }

            if (_numdoctoEsperado == _numDocs) {
                //Messagebox.show("Operacion Completa Para esta Solicitud", "Operacion Completada", Messagebox.OK, Messagebox.INFORMATION);

                _idCodeBarRear2.setVisible(false);

                _yyyy.setValue("");

                _idCodeBarRear23.setVisible(false);

                _yyyyy.setValue("");

                id_codebar.setFocus(true);
                return;
            }

            if (idFkEstado == codEstado.get(0).getDi_IdEstado()) {
                Messagebox.show("N� Operacion en estado Pendiente", "Operacion Pendiente N�: " + idOperx, Messagebox.OK, Messagebox.INFORMATION);
                _idCodeBarRear2.setVisible(false);

                _yyyy.setValue("");

                _idCodeBarRear23.setVisible(false);

                _yyyyy.setValue("");

                id_codebar.setFocus(true);

                id_addDocPanel.setVisible(false);
                _divFridDoc.setVisible(false);
                return;
            }

            procesoDocumentoCode(Barcode);

            _idCodeBarRear2.setVisible(true);

            String CodeTemp = Barcode;
            /*Visualizacion codigo */
            String codeBard = generaCodigoBarra();
            encodeCode128SolP = new Code128Writer().encode(codeBard, BarcodeFormat.CODE_128, 150, 80, null);
            bufferedImage128SolP = MatrixToImageWriter.toBufferedImage(encodeCode128SolP);
            //_idCodeBarRear23.setContent(bufferedImage128SolP);
            //_yyyyy.setValue(codeBard);
            System.out.println("CODIGO BARRA SOLICITUD: " + codeBard);

            //Messagebox.show("BarCode Escaneado :[" + Barcode + "] existe En solicitud?[" + result + "]_numDocs:[" + _numDocs + "]_numdoctoEsperado:[" + _numdoctoEsperado + "]");
            encodeCode128 = new Code128Writer().encode(CodeTemp, BarcodeFormat.CODE_128, 150, 80, null);

            bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
            _idCodeBarRear2.setContent(bufferedImage128);
            _yyyy.setValue(Barcode);

            readQR(bufferedImage128);

            id_codebar.setFocus(true);

        } catch (Exception ex) {
            System.out.println("Problemas al leer codigo de barra Exception--> " + ex);
        }

    }

    public String generaCodigoBarra() throws ParseException, WriterException {
        /*GENERACION DE CODIGO BARRA PARA SOLICITUD*/
        int idProdv = 0;
        String idProdS = "";

        int cantOperI = 0;
        String cantOperS = "";

        String fecCode = "";

        String CodeSol = "";
        CodeSol += id_solicitud;

        System.out.println("nameProductCode " + nameProductCode);

        //_conListDOc = new MvcConfig().documentosJDBC();
        lprod = conexProdCod.selectProductoByName(nameProductCode);

        for (Productos lmx : lprod) {
            idProdv = lmx.getDi_IdProducto();
        }
        idProdS = String.valueOf(idProdv);
        //CodeSol += idProdS;

        cantOperI = conexSol.getNumeroDocumentosSolicitud(id_solicitud);
        cantOperS = String.valueOf(cantOperI);

        //CodeSol += cantOperS;
        String dateStr = fecSolicCode;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse(dateStr);
        sdf = new SimpleDateFormat("yyyyMMdd");
        dateStr = sdf.format(date);

        CodeSol = "SOL" + id_solicitud + dateStr + cantOperS;
        //CodeSol += dateStr;

        /*encodeCode128SolP = new Code128Writer().encode(CodeSol, BarcodeFormat.CODE_128, 150, 80, null);
        bufferedImage128SolP = MatrixToImageWriter.toBufferedImage(encodeCode128SolP);
        _idCodeBarRear23.setContent(bufferedImage128SolP);
        _yyyyy.setValue(CodeSol);*/

 /*GENERACION DE CODIGO BARRA PARA SOLICITUD*/
        return CodeSol;
    }

    /*Metodo Decode para BufferedImage*/
    public void readQR(BufferedImage img) throws FormatException, ChecksumException, NotFoundException, IOException {

        LuminanceSource source = new BufferedImageLuminanceSource(img);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Reader reader = new MultiFormatReader();
        Result stringBarCode = reader.decode(bitmap);

        System.out.println("Encode128 ----> " + stringBarCode.getText());
    }

    public void printCodeBarOper(String code) throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        System.out.println("PRINT: " + code);
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("ZE"+code);
        
        selecImpresoraInstalada(id_solicitud, codes);
        
        //imprimeCodBarra2(code);/*Se reemplaza por selecImpresoraInstalada */
    }

    /*Metodo en desuso reemplazado por metodo selecImpresoraInstalada() y popup*/
    public void imprimeCodBarra2(String codeBar) throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService psZebra = null;
        String bac = "\\";
        boolean verificaService = false;
        ResourceBundle myResources = ResourceBundle.getBundle("zpldiccionario");
        String ipZebra = myResources.getString("ipZebraConnection");

        for (PrintService printService : ps) {
            System.out.println("printService.getName() " + printService.getName());
            //System.out.println("RED :" + bac + bac + "161.131.230.132" + bac + "ZDesigner GT800 (EPL)"); ZDesigner GT800 (ZPL)
            PrintServiceAttributeSet printServiceAttributes = printService.getAttributes();
            PrinterState prnState = (PrinterState) printService.getAttribute(PrinterState.class);

            //             PrintService printer = PrintServiceLookup.lookupDefaultPrintService();
//             AttributeSet att = printer.getAttributes();
//             for (Attribute a : att.toArray()) {
//                 String attributeName="";
//                 String attributeValue="";
//                 
//                 attributeName = a.getName();
//                 attributeValue = att.get(a.getClass()).toString();
//                 
//                 System.out.println("ATRIBUTES "+attributeName + ":" +attributeValue);
//             }
            if (printService.getName().toString().contains("ZDesigner")) {

                System.out.println("ZDesigner PrintServices : --> " + printService.getName());

                //0,29
                //0,00
//                if (printService.getName().equals("ZDesigner GT800 (ZPLcs)") ) {
//                    psZebra = printService;
//                    
//                    Attribute[] a = printServiceAttributes.toArray();
//            for (Attribute unAtribute : a) {
//                System.out.println("atributo: " + unAtribute.getName());
//            }
//
//            System.out.println("--- viendo valores especificos de los atributos ");
//
//            // valor especifico de un determinado atributo de la impresora
//            System.out.println("PrinterLocation: " + printServiceAttributes.get(PrinterLocation.class));
//            System.out.println("PrinterInfo: " + printServiceAttributes.get(PrinterInfo.class));
//            System.out.println("PrinterState: " + printServiceAttributes.get(PrinterState.class));
//            System.out.println("Destination: " + printServiceAttributes.get(Destination.class));
//            System.out.println("PrinterMakeAndModel: " + printServiceAttributes.get(PrinterMakeAndModel.class));
//            System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributes.get(PrinterIsAcceptingJobs.class));
//
//        
//                    
//                    /*VERIFICA*/
//                    if (prnState == PrinterState.STOPPED) {
//                    PrinterStateReasons prnStateReasons = (PrinterStateReasons) printService.getAttribute(PrinterStateReasons.class);
//                    if ((prnStateReasons != null) && (prnStateReasons.containsKey(PrinterStateReason.SHUTDOWN) )) {
//                      
//                    Messagebox.show("PrintService is no longer available.", "", Messagebox.OK, null);
//                    //throw new PrintException("PrintService is no longer available.");
//                        
//                    }
//                      }else{
//                    
//                        if ((PrinterIsAcceptingJobs) (printService.getAttribute(PrinterIsAcceptingJobs.class)) == PrinterIsAcceptingJobs.NOT_ACCEPTING_JOBS) {
//                        
//                        Messagebox.show("Printer is not accepting job.", "", Messagebox.OK, null);
//                    }else{
//                    System.out.println("ZDesigner Local: --> " + printService.getName());
//                    verificaService = true;
//                    break;
//                        }
//                    }
//                    
//                    
//                    
//                    
//                    
//                    
//                } else
                if (printService.getName().equals(bac + bac + ipZebra + bac + "ZDesigner GT800 (EPL)")) {
                    psZebra = printService;

                    Attribute[] a = printServiceAttributes.toArray();
                    for (Attribute unAtribute : a) {
                        System.out.println("atributo: " + unAtribute.getName());
                    }

                    System.out.println("--- viendo valores especificos de los atributos ");

                    // valor especifico de un determinado atributo de la impresora
                    System.out.println("PrinterLocation: " + printServiceAttributes.get(PrinterLocation.class));
                    System.out.println("PrinterInfo: " + printServiceAttributes.get(PrinterInfo.class));
                    System.out.println("PrinterState: " + printServiceAttributes.get(PrinterState.class));
                    System.out.println("Destination: " + printServiceAttributes.get(Destination.class));
                    System.out.println("PrinterMakeAndModel: " + printServiceAttributes.get(PrinterMakeAndModel.class));
                    System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributes.get(PrinterIsAcceptingJobs.class));

                    /*VERIFICA*/
                    if (prnState == PrinterState.STOPPED) {
                        PrinterStateReasons prnStateReasons = (PrinterStateReasons) printService.getAttribute(PrinterStateReasons.class);
                        if ((prnStateReasons != null) && (prnStateReasons.containsKey(PrinterStateReason.SHUTDOWN))) {

                            Messagebox.show("PrintService is no longer available.", "", Messagebox.OK, null);
                            //throw new PrintException("PrintService is no longer available.");

                        }
                    } else {

                        if ((PrinterIsAcceptingJobs) (printService.getAttribute(PrinterIsAcceptingJobs.class)) == PrinterIsAcceptingJobs.NOT_ACCEPTING_JOBS) {

                            Messagebox.show("Printer is not accepting job.", "", Messagebox.OK, null);
                        } else {
                            System.out.println("ZDesigner Network: --> " + printService.getName());
                            verificaService = true;
                            break;
                        }

                    }

                }
            }

        }
        // aca obtenemos la printer default  
        // ******PrintService printService = PrintServiceLookup.lookupDefaultPrintService();

//        String zplCommand = "^XA\n"
//                //+ "^FO10,0^ARN,11,7^FD SOME TEXT ^FS\n"
//                //+ "^FO300,0^ARN,11,7^FD SOME VALUE ^FS\n"
//                //+ "^FO10,35^ARN,11,7^FD SOME TEXT ^FS\n"
//               // + "^FO300,35^ARN,11,7^FD SOME VALUE ^FS\n"
//                + "^FO10,70^ARN,11,7^FD D41599999433 ^FS\n"
//                //+ "^FO10,115^ARN,11,7^BCN,60,Y,Y,N^FD 23749237439827 ^FS\n"
//                + "^XZ";
//                String zplCommand = "^XA\n"
//                                +"^FO90,200^BY4^BCN,256,Y,N,Y,N^FD>;>D41599999433^FS\n"
//                                +"^XZ";
        //D41599999433sdsd
        DiccionarioCodeZpl dicc = new DiccionarioCodeZpl();
        String codeFinal = dicc.diccionarioCodeZpl(codeBar);

        String zplCommand = "^XA^F090,75^BY3^BCN,100,Y,N,N^FD>9" + codeFinal + "\n"
                + "^XZ";

        // convertimos el comando a bytes  
        byte[] by = zplCommand.getBytes();
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(by, flavor, null);

        if (verificaService) {

            // creamos el printjob  
            DocPrintJob job = psZebra.createPrintJob();
            // imprimimos  
            //job.print(doc, null);

        } else {

            Messagebox.show("No se Encontro Impresora Zebra Conectada ", "", Messagebox.OK, null);
        }

    }

    //Este Metodo levanta popup impresora
    public void selecImpresoraInstalada(int idsol, ArrayList<String> codeBar) {

        String iddsol = String.valueOf(idsol);

        ArrayList<String> Idsol = new ArrayList<String>();
        Idsol.add(iddsol);

        Map<String, ArrayList<String>> arguments = new HashMap<String, ArrayList<String>>();

        //detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("IdSolicitud", Idsol);
        arguments.put("Codes", codeBar);

        String template = "custodia/detallesPrint.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    @Listen("onClick = #id_printCod")
    public void codePrints() throws SQLException, IOException, WriterException, PrintException, PrintException, COSVisitorException {
        ArrayList<String> codes = new ArrayList<String>();
        nn = conexDetSol.listDetSolicitud(id_solicitud);

        for (final BandejaDetalle nnn : nn) {

            codes.add(nnn.getnOperacion());

        }
        selecImpresoraInstalada(id_solicitud, codes);
        //imprimeCodBarra(codes);/*Este ya no se usa por que lo hacemos desde el popup*/
        iframe.setVisible(false);

    }

    /*Metodo en desuso reemplazado por metodo selecImpresoraInstalada() y popup*/
    public void imprimeCodBarra(ArrayList<String> codeBar) throws IOException, WriterException, PrintException, COSVisitorException, SQLException {
        // resultWin.detach();
        BitMatrix bitMatrixXXXX;
        PDDocument document = new PDDocument();
        PDPage emptyPage = null;
        int line = 0;
        PDFont font = PDType1Font.HELVETICA;
        PDFont fontMono = PDType1Font.COURIER;

        // emptyPage = new PDPage(PDPage.);
        emptyPage = new PDPage();
        PDPage blankPage = new PDPage();
        PDRectangle rect = emptyPage.getMediaBox();
        // emptyPage.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));
        document.addPage(emptyPage);
        //document.addPage( blankPage );
        //document.save("EmptyPage.pdf");

        //PDFRenderer renderer = new PDFRenderer(document);
        //BufferedImage img = renderer.renderImage(12 - 1, 4f);
        //ImageIOUtil.writeImage(img, new File(RESULT_FOLDER, "ENG-US_NMATSCJ-1.103-0330-page12cropped.jpg").getAbsolutePath(), 300);
        PDPageContentStream content = new PDPageContentStream(document, emptyPage);

        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLACK);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
        content.drawString("Codigos Operaciones Solicitud N� " + id_solicitud);
        content.endText();
        int cont = 0;

//        codeBar.add("D11810036326");
//        codeBar.add("D11810036327");
//        codeBar.add("D11810036328");
//        codeBar.add("D11810036329");
//        codeBar.add("D11810036320");
//         codeBar.add("D1181007363201");
//          codeBar.add("D118100376322");
//           codeBar.add("D1181003667323");
//            codeBar.add("D11810036324");
//             codeBar.add("D11810036rt325");
//              codeBar.add("D118100356326");
//               codeBar.add("D118100367327");
//                codeBar.add("D1181003667328");
//                 codeBar.add("D11810036567329");
        System.out.println("CODEBARSIZE: " + codeBar.size());

        for (int x = 0; x < codeBar.size(); x++) {

            rimpagString = rect.getHeight() - (49 + 1) * (++line);
            rimpagImage = rect.getHeight() - 50 * (++line);

            content.beginText();
            content.setFont(fontMono, 15);
            content.setNonStrokingColor(Color.BLACK);
            content.moveTextPositionByAmount(175, rimpagString);
            content.drawString(codeBar.get(x));
            content.endText();

            bitMatrixXXXX = new Code128Writer().encode(codeBar.get(x), BarcodeFormat.CODE_128, 150, 80, null);
            BufferedImage buffImgt = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
            PDXObjectImage ximaget = new PDPixelMap(document, buffImgt);
            content.drawXObject(ximaget, 150, rimpagImage, 150, 50);
            cont++;

            
             //if number of results exceeds what can fit on the first page
            if (cont%6==0) {

                content.close();

                line = 0;

               
                emptyPage = new PDPage(PDPage.PAGE_SIZE_LETTER);
                document.addPage(emptyPage);
                content = new PDPageContentStream(document, emptyPage);

                //generate data for second page
            }

        }

        //// agregando codigos de los Documentos del Lote
        conexDetSol = new MvcConfig().detSolicitudJDBC();

        String CompiladoNumeros = "";

        content.close();
        //contente.close();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        pss = new PDStream(document);
        // document.close();

        // PDStream contents = emptyPage.getContents();
        String path = "C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf";
        File f = new File(path);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            iframe.setContent(mymedia);
        }

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            //Samsung M536x Series
            if (printService.getName().equals("Samsung M536x Series")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            //Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());
            Messagebox.show("Codigos Impresos Correctamente.");

        }

        InputStream is = pss.createInputStream();

        //Messagebox.show("is" + is.toString());
        final byte[] byteStream = out.toByteArray();
        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());
    }

    public void procesoDocumentoCode(String _codigoOperOrigen) throws SQLException, WriterException, ParseException {
        String codeBar = _codigoOperOrigen;
        String[] resp1 = new String[3];
        resp1 = GuardarDocumentoCode(codeBar);

        if ("Si".equals(resp1[0])) {
            cierraSolicitud(resp1);
        }
        solConPendientes = 0;
        contSolCompletada = 0;
        cantOpeComplet2 = 0;
        cantOpePend2 = 0;
        SolCompletada2 = "";
        SolCompletada2 = resp1[0];
        cantOpeComplet2 = Integer.parseInt(resp1[1]);
        cantOpePend2 = Integer.parseInt(resp1[2]);

    }

    public String[] GuardarDocumentoCode(String _codigoOperOrigen) throws SQLException {
        boolean insert = false;
        String[] respStr = new String[3];
        String Glosa = "Documento Escaneado Por C�digo de Barra";

//        try {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        int id_tipDcto = 0;
        int id_EstDcto = 0;

        System.out.println("cmb_TipoDocumento.getValue() => " + cmb_TipoDocumento.getValue());
        System.out.println("cmb_EstDcto.getValue() => " + cmb_EstDcto.getValue());
        System.out.println("txt_docsEsperados.getValue() => " + txt_docsEsperados.getValue());

        String lm2 = "";
        int tipoDocInt = 0;
        List<ProdTipoDoc> dp = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(_codigoOperOrigen));;

        for (ProdTipoDoc lmx : dp) {
            lm2 = lmx.getDv_NomTipDcto();
            tipoDocInt = lmx.getDi_IdTipDcto();
        }

        System.out.println("String NOM DOC " + lm2 + " " + tipoDocInt);

        String tipodoctodesc = lm2;//cmb_TipoDocumento.getValue();
        //String estado = cmb_EstDcto.getValue();
        //docsEsp = Integer.parseInt(txt_docsEsperados.getValue());
        this._nuevodocto = new Documento();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();

        ope = _op.getOperaciones(_codigoOperOrigen);

        //se retorna a la pag principal
        Id_Operacion = ope.getDi_IdOperacion();
        //boolean checkapbanco = id_chechApoderado.isChecked();

        boolean checkapbanco = false;
        // Agregagos el Nuevo Documento encontrado
//        for (final TipoDocumento tipodocto : _listtipoDocto) {
//            if (tipodocto.getDv_NomTipDcto().equals(tipodoctodesc)) {
//                id_tipDcto = tipodocto.getDi_IdTipDcto();
//            }
//        }

//        if (id_tipDcto == 0) {
//            Messagebox.show("Sr(a). Usuario(a), debe seleccionar un tipo de documento para continuar.");
//            cmb_TipoDocumento.setFocus(true);
//            // return;
//        }
//        for (final Estado est : _estadoDocto) {
//            if (est.getDv_NomEstado().equals(estado) && est.getDv_DetEstado().equals("Operaci�n C/Documento en custodia")) {
//                id_EstDcto = est.getDi_IdEstado();
//            }
//        }
//        if (id_EstDcto == 0) {
//            Messagebox.show("Sr(a). Usuario(a), debe seleccionar el estado del documento para continuar.");
//            cmb_EstDcto.setFocus(true);
//            // return;
//        }
//        if (checkapbanco) {
//            Messagebox.show("Sr(a). Usuario(a), recuerde que los pagares firmados por apoderado banco deben ir con su contrato anexado.");
//        }
        this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
        this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
        this._nuevodocto.setDb_checkOk(true);
        this._nuevodocto.setDi_fk_IdUbi(2);
        this._nuevodocto.setDv_Glosa(Glosa);
        this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
        this._nuevodocto.setDi_fk_IdEstado(152);
        this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());
        this._nuevodocto.setDi_fk_TipoDcto(tipoDocInt);

        insert = _docto.insert(_nuevodocto);

        boolean respCambEst = false;
        if (insert) {

            //Se cambia el estado a la operacion (Completa)
            respCambEst = conexSol.cambioEstadoSolDet(Id_Operacion, "C", "");

            bNuevo = true;
            Messagebox.show("El Documento fue agregado Correctamente", "Guardar Documento", Messagebox.OK, Messagebox.INFORMATION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                    }
                }
            }
            );

            updateDetalleGrid(id_solicitud);

            if (bNuevo) {
                numDctsOpAct += 1;
            } else {
                numDctsOpAct = numDctsOpAct;
            }

        } else {
            Messagebox.show("Problemas al Agregar el Documento. \n", "Guardar Documento", Messagebox.OK, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        //Executions.sendRedirect("/sisboj/index");
                    }
                }
            }
            );

        }

        /*Limpia datos info documento*/
        cmb_TipoDocumento.setValue(
                null);
        cmb_EstDcto.setValue(
                null);
        //  txt_docsEsperados.setValue(null);
        id_chechApoderado.setChecked(
                false);
        txt_GlosaDcto2.setValue(
                "");

        // Messagebox.show("DatosCamputados:::");
        if (insert && respCambEst) {
            //se verifica si estan agregados todos los documentos en las operaciones de la solicitud                
            List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
            lbd = getListDet(id_solicitud);
            String SolCompletada = "";
            solConPendientes = 0;
            contSolCompletada = 0;
            for (int i = 0; i < lbd.size(); i++) {
                if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "No";
                } else {
                    if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                        solConPendientes = solConPendientes + 1;
                    }
                    if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                        // SolCompletada = "Si";
                        contSolCompletada = contSolCompletada + 1;
                    }
                }

            }

            if ((solConPendientes + contSolCompletada) == lbd.size()) {
                SolCompletada = "Si";

            } else {
                SolCompletada = "No";
            }

            System.out.println("GuardarDocumento contSolCompletada2 => " + contSolCompletada);
            System.out.println("GuardarDocumento solConPendientes2 => " + solConPendientes);

            respStr = new String[3];
            respStr[0] = SolCompletada;
            respStr[1] = String.valueOf(contSolCompletada);
            respStr[2] = String.valueOf(solConPendientes);

        }

        return respStr;

    }

    @Listen("onClick = #ButtonGuardarPend")
    public void setCambioEstadoDocDet() throws SQLException, WriterException, ParseException {
        boolean resp = false;
        detSeleccion = new BandejaDetalle();
        if (!"".equals(txtObservaciones.getValue())) {
            for (final BandejaDetalle banddettt : listDet) {
                if (banddettt.getnOperacion() == txt_OperDcto2.getValue()) {
                    detSeleccion = banddettt;
                }
            }

            try {
                System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
                //Se deja como pendiente la operacion
                resp = conexSol.cambioEstadoSolDet(detSeleccion.getIdDetSol(), "P", txtObservaciones.getValue());

                if (resp) {
                    Messagebox.show("Documento enviado a Pendientes Correctamente ", "", Messagebox.OK, null,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                            }
                        }
                    });

                    activePage = grid_DetSol.getPaginal().getActivePage();
                    //grid_DetSol.setActivePage(activePage);
                    System.out.println("-->*#ACTIVE PAGE " + activePage);
                    //updateDetalleGrid(detSeleccion.getDi_idSolicitud());
                    //updateDetalle(id_solicitud);
                    updateDetalleGrid(id_solicitud);
                    id_addPenPanel.setVisible(false);
                    //updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);
                    //listCountProdTipoDoc();

                    txtObservaciones.setValue(null);

                    //Envia a cierrre de Solicitud
                    String[] EvaluaCierre = validaCantOperaciones(id_solicitud);

                    if ("Si".equals(EvaluaCierre[0])) {
                        cierraSolicitud(EvaluaCierre);
                    }
                    solConPendientes = 0;
                    contSolCompletada = 0;
                    cantOpeComplet2 = 0;
                    cantOpePend2 = 0;
                    SolCompletada2 = "";
                    SolCompletada2 = EvaluaCierre[0];
                    cantOpeComplet2 = Integer.parseInt(EvaluaCierre[1]);
                    cantOpePend2 = Integer.parseInt(EvaluaCierre[2]);

                } else {
                    Messagebox.show("Problemas al Mover el archivo el Documento. ", null, Messagebox.OK, Messagebox.ERROR,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                                //OK is clicked
                                Executions.sendRedirect("/sisboj/index");
                            }
                        }
                    }
                    );

                }

            } catch (SQLException e) {
                e.getMessage();
            }
        } else {
            Messagebox.show("Debe Agregar Observacion. ", null, Messagebox.OK, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        txtObservaciones.focus();
                    }
                }
            }
            );
        }
    }

    public String[] validaCantOperaciones(int idSol) throws SQLException {

        String[] respStr = new String[3];
        //se verifica si estan agregados todos los documentos en las operaciones de la solicitud                
        List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
        lbd = getListDet(idSol);
        String SolCompletada = "";
        solConPendientes = 0;
        contSolCompletada = 0;
        for (int i = 0; i < lbd.size(); i++) {
            if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                // SolCompletada = "No";
            } else {
                if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                    solConPendientes = solConPendientes + 1;
                }
                if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "Si";
                    contSolCompletada = contSolCompletada + 1;
                }
            }

        }

        if ((solConPendientes + contSolCompletada) == lbd.size()) {

            if (solConPendientes == lbd.size()) {
                SolCompletada = "No";
            } else {
                SolCompletada = "Si";
            }

        } else {
            SolCompletada = "No";
        }

        System.out.println("GuardarDocumento contSolCompletada2 => " + contSolCompletada);
        System.out.println("GuardarDocumento solConPendientes2 => " + solConPendientes);

        respStr = new String[3];
        respStr[0] = SolCompletada;
        respStr[1] = String.valueOf(contSolCompletada);
        respStr[2] = String.valueOf(solConPendientes);

        updateDetalleGrid(idSol);
        listCountProdTipoDoc();

        return respStr;

    }

    /**
     * Metodo que agrega documentos pendientes para poder cerrar el lote
     */
    public boolean GuardarDocumentosPendientes(int id_sol) throws SQLException {

        List<Estado> codEstado = new ArrayList<Estado>();
        List<Estado> codEstado2 = new ArrayList<Estado>();

        try {
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        } catch (SQLException ex) {
            Logger.getLogger(DocumentoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        codEstado = estJDBC.getIdEstadoByCodigo("DLPENCUS");
        codEstado2 = estJDBC.getIdEstadoByCodigo("SOLOCUS");

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        boolean resp = false;
        try {
            this.listDet = conexDetSol.listDetSolPendientes(id_sol);

            final HashMap<String, Object> map = new HashMap<String, Object>();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date();

            // ope = _op.getOperaciones(txt_OperDcto.getValue());
            for (final BandejaDetalle detOpPen : listDet) {

                String GlosaReparo = "Pendiente Cierre(Estado Lote - Custodia)";
                this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
                this._nuevodocto.setDb_ApoderadoBanco(false);
                this._nuevodocto.setDb_checkOk(true);
                this._nuevodocto.setDi_fk_IdUbi(2);
                this._nuevodocto.setDv_Glosa(GlosaReparo);
                this._nuevodocto.setDdt_FechaSalidaBov(null);
                this._nuevodocto.setDi_fk_IdEstado(codEstado.get(0).getDi_IdEstado());
                this._nuevodocto.setDi_fk_IdOper(detOpPen.getIdOperacion());
                this._nuevodocto.setDi_fk_TipoDcto(codEstado2.get(0).getDi_IdEstado());

                System.out.println("GuardarDocumentosPendientes _nuevodocto => " + _nuevodocto.toString());

                resp = _docto.insert(_nuevodocto);
            }

        } catch (WrongValueException ex) {

            ex.getMessage();
            return false;
        }

        return resp;
    }

    @Listen("onClick = #ButtonGuardarDoc")
    public void procesoDocumento() throws SQLException, WriterException, ParseException {
        String[] resp1 = new String[3];
        resp1 = GuardarDocumento();

        if ("Si".equals(resp1[0])) {
            cierraSolicitud(resp1);
        }

        try {
            solConPendientes = 0;
            contSolCompletada = 0;
            cantOpeComplet2 = 0;
            cantOpePend2 = 0;
            SolCompletada2 = "";
            SolCompletada2 = resp1[0];
            cantOpeComplet2 = Integer.parseInt(resp1[1]);
            cantOpePend2 = Integer.parseInt(resp1[2]);
        } catch (Exception ex) {
            System.out.println("Error ProcesaDocumento()");
        }

    }

    public String[] GuardarDocumento() throws SQLException {
        boolean insert = false;
        String[] respStr = new String[3];

//        try {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        int id_tipDcto = 0;
        int id_EstDcto = 0;

        System.out.println("cmb_TipoDocumento.getValue() => " + cmb_TipoDocumento.getValue());
        System.out.println("cmb_EstDcto.getValue() => " + cmb_EstDcto.getValue());
        System.out.println("txt_docsEsperados.getValue() => " + txt_docsEsperados.getValue());

        String tipodoctodesc = cmb_TipoDocumento.getValue();
        String estado = cmb_EstDcto.getValue();
        docsEsp = Integer.parseInt(txt_docsEsperados.getValue());
        this._nuevodocto = new Documento();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();

        ope = _op.getOperaciones(txt_OperDcto.getValue());

        //se retorna a la pag principal
        Id_Operacion = ope.getDi_IdOperacion();
        boolean checkapbanco = id_chechApoderado.isChecked();
        boolean validateNull = true;
        int valida = 0;

        if (estado.isEmpty() || estado.equals(null) || estado.equals("") || estado.equals("null") || tipodoctodesc.isEmpty() || tipodoctodesc.equals(null) || tipodoctodesc.equals("") || tipodoctodesc.equals("null")) {
            validateNull = false;
            valida = 1;
        }

        if (valida == 1) {
            Messagebox.show("Sr(a). Usuario(a), debe seleccionar un Tipo y un Estado de documento para continuar.", "", Messagebox.OK, Messagebox.INFORMATION);
        } else {

            // Agregagos el Nuevo Documento encontrado
            for (final TipoDocumento tipodocto : _listtipoDocto) {
                if (tipodocto.getDv_NomTipDcto().equals(tipodoctodesc)) {
                    id_tipDcto = tipodocto.getDi_IdTipDcto();
                }
            }

            if (id_tipDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar un tipo de documento para continuar.");
                cmb_TipoDocumento.setFocus(true);
                // return;
            }

            for (final Estado est : _estadoDocto) {
                if (est.getDv_NomEstado().equals(estado) && est.getDv_DetEstado().equals("Operaci�n C/Documento en custodia")) {
                    id_EstDcto = est.getDi_IdEstado();
                }
            }

            if (id_EstDcto == 0) {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar el estado del documento para continuar.");
                cmb_EstDcto.setFocus(true);
                //return;
            }

            if (checkapbanco) {
                Messagebox.show("Sr(a). Usuario(a), recuerde que los pagares firmados por apoderado banco deben ir con su contrato anexado.");
            }
        }

        this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
        this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
        this._nuevodocto.setDb_checkOk(true);
        this._nuevodocto.setDi_fk_IdUbi(2);
        this._nuevodocto.setDv_Glosa(txt_GlosaDcto2.getValue());
        this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
        this._nuevodocto.setDi_fk_IdEstado(id_EstDcto);
        this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());
        this._nuevodocto.setDi_fk_TipoDcto(id_tipDcto);

        boolean respCambEst = false;

        if (validateNull == true) {
            insert = _docto.insert(_nuevodocto);

            if (insert) {

                //Se cambia el estado a la operacion (Completa)
                respCambEst = conexSol.cambioEstadoSolDet(Id_Operacion, "C", "Completa");

                bNuevo = true;
                Messagebox.show("El Documento fue agregado Correctamente", "", Messagebox.OK, Messagebox.INFORMATION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        }
                    }
                }
                );

                updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);
                MatchDoc(id_solicitud, ope.getDi_IdOperacion(), nDoc, docEs);

                if (bNuevo) {
                    numDctsOpAct += 1;
                } else {
                    numDctsOpAct = numDctsOpAct;
                }

            } else {
                Messagebox.show("Problemas al Agregar el Documento. \n", "Guardar Documento", Messagebox.OK, Messagebox.ERROR,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            //OK is clicked
                            // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                            // Executions.sendRedirect("/sisboj/index");
                        }
                    }
                }
                );

            }

            /*Limpia datos info documento*/
            cmb_TipoDocumento.setValue(
                    null);
            cmb_EstDcto.setValue(
                    null);
            //  txt_docsEsperados.setValue(null);
            id_chechApoderado.setChecked(
                    false);
            txt_GlosaDcto2.setValue(
                    "");

            // Messagebox.show("DatosCamputados:::");
            if (insert && respCambEst) {
                //se verifica si estan agregados todos los documentos en las operaciones de la solicitud                
                List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
                lbd = getListDet(id_solicitud);
                String SolCompletada = "";
                solConPendientes = 0;
                contSolCompletada = 0;
                for (int i = 0; i < lbd.size(); i++) {
                    if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                        // SolCompletada = "No";
                    } else {
                        if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                            solConPendientes = solConPendientes + 1;
                        }
                        if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                            // SolCompletada = "Si";
                            contSolCompletada = contSolCompletada + 1;
                        }
                    }

                }

                if ((solConPendientes + contSolCompletada) == lbd.size()) {
                    SolCompletada = "Si";

                } else {
                    SolCompletada = "No";
                }

                System.out.println("GuardarDocumento contSolCompletada2 => " + contSolCompletada);
                System.out.println("GuardarDocumento solConPendientes2 => " + solConPendientes);

                respStr = new String[3];
                respStr[0] = SolCompletada;
                respStr[1] = String.valueOf(contSolCompletada);
                respStr[2] = String.valueOf(solConPendientes);

                updateDetalleGrid(id_solicitud);
                listCountProdTipoDoc();

            }

        }

        return respStr;

    }

    public boolean cierraSolicitud(String[] Str) throws SQLException, WriterException, ParseException {

        System.out.println(" cierraSolicitud SolCompletada => " + Str[0]);
        System.out.println(" cierraSolicitud cantOpeComplet => " + Str[1]);
        System.out.println(" cierraSolicitud cantOpePend => " + Str[2]);

        SolCompletada2 = Str[0];
        cantOpeComplet2 = Integer.parseInt(Str[1]);
        cantOpePend2 = Integer.parseInt(Str[2]);

        //Si estan completas todas las operaciones se cambia de estado la solicitud  
//        try {
        if ("Si".equals(SolCompletada2) && cantOpePend2 == 0) {

            //Cerra Solcicitud para ser enviada a bandeja recepcion de BOJ(Cambiar estado a la solicitud)
            cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerrado");
            if (cambioEstado) {

                boolean updCodeBarSol;
                String codbar = generaCodigoBarra();

                updCodeBarSol = conexSol.updateSolicitudCodeBar(id_solicitud, "dv_CodeBar", codbar);

                System.out.println("Solicitud Completada Correctamente => Generar codigo de barra");

            }
            if (cambioEstado) {
                responseCierreSol = true;
                Messagebox.show("Solicitud Completada Correctamente", "Fue Movida a Solicitudes Terminadas.", Messagebox.OK, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            System.out.println("Solicitud Terminada Correctamente");

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            responseCierreSol = false;
                        }

                        gridPrincipal();
                    }

                });
            } else {
                responseCierreSol = false;
            }
        }
        if ("Si".equals(SolCompletada2) && cantOpePend2 > 0) {
            System.out.println("estoy aqui 11111111111111111111");
            // responseCierreSol = cierreConPendientes();
            Messagebox.show("Solicitud Completada Con Operaciones Pendientes.", "�Desea Cerrar esta solicitud para ser enviada?.", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                    if (Messagebox.ON_OK.equals(e.getName())) {

                        //Cerrar Solcicitud(Cambiar estado a la solicitud)
                        cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerradoConP");
                        //Actualizar cantidad de documentos (Resta documentos Pendientes)

                        boolean updCantidadDocs = false;
                        int restaDocs = 0;
                        int suma2 = cantOpeComplet2 + cantOpePend2;
                        restaDocs = suma2 - cantOpePend2;

                        updCantidadDocs = conexSol.updateSolicitud(id_solicitud, "di_CantDctosSolicitados", restaDocs);

                        boolean cambEstOp = false;
                        //Cambia de estado ooperaciones en tabla tb_Sys_DetSolicitud(Completa)                                    
                        cambEstOp = conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);

                        if (cambioEstado && cambEstOp && updCantidadDocs) {
//                          if (cambioEstado && cambEstOp) {
                            // Se crea hijo de esta para las operaciones con estado Pendiente y se obtiene hijo creado

                            List<Estado> ListEstado = new ArrayList();
                            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

                            ListEstado = estJDBC.getIdEstadoByCodigo("NUPECUS");

                            //se obtiene id de Ubicacion
//                                        List<Ubicacion> lUbi = new ArrayList<Ubicacion>();
                            lUbi = ubicacionJDBC.getUbicacion("dv_DetUbi", "UCC");

                            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Date dat = new Date();
                            System.out.println("df.format(dat) => " + df.format(dat).toString());
                            //Se obtiene la solicitud padre
                            Solicitud sol = new Solicitud();
                            sol = conexSol.getSolicitud(id_solicitud);

                            newHijosol = new Solicitud();
                            // newHijosol.setDi_IdSolicitud(id_solicitud);
                            newHijosol.setDi_fk_IdUbi(lUbi.get(0).getDi_IdUbicacion());
                            newHijosol.setDi_fk_IdHito(sol.getDi_fk_IdHito());
                            newHijosol.setDi_fk_IdEstado(ListEstado.get(0).getDi_IdEstado());
                            newHijosol.setDdt_FechaCreacion(df.format(dat));
                            newHijosol.setDi_cantidadDocs(cantOpePend2);
                            //newHijosol.setDi_SolPadre(sol.getDi_IdSolicitud());

                            crearHijo = conexSol.InsertHijoSol(newHijosol);

                            //  idSolHijo = crearHijo.getDi_IdSolicitud();
                            //  idSolHijo = crearHijo.getDi_IdSolicitud();
                            System.out.println("idSolHijo ======>>> " + crearHijo.getDi_IdSolicitud());

                            System.out.println("idSolHijo ======>>> " + crearHijo.getDi_IdSolicitud());

                            //Si el hijo se creo correctamente, se viculan padre e hijo
                            //Si el hijo se creo correctamente, se viculan padre e hijo
                            //Si el hijo se creo correctamente, se vicula el padre con el hijo
                            if (crearHijo != null) {
                                // updPadre = conexSol.updateSolicitud(id_solicitud, "di_SolHijo", crearHijo.getDi_IdSolicitud());
                                JerarquiaSolEnt jerEnt = new JerarquiaSolEnt();
                                jerEnt.setDi_fk_IdSolicitudPadre(sol.getDi_IdSolicitud());
                                jerEnt.setDi_fk_IdSolicitudHijo(crearHijo.getDi_IdSolicitud());

                                updPadre = jerarquiaSolJDBC.insertJerarquiaSol(jerEnt);
                                System.out.println("updPadre => " + updPadre);
                            }

                            //Una vez actualizado el padre se vinculan las operaciones pendientes a la solic. hijo creada.
                            if (updPadre) {
                                System.out.println("crearHijo.getDi_IdSolicitud() => " + crearHijo.getDi_IdSolicitud());
                                updOperNvaSol = conexDetSol.updateIdSolOperacion(id_solicitud, "di_fk_IdSolicitud", crearHijo.getDi_IdSolicitud());
                            }

                            if (updOperNvaSol) {

                                boolean updCodeBarSol;
                                String codbar = generaCodigoBarra();

                                updCodeBarSol = conexSol.updateSolicitudCodeBar(id_solicitud, "dv_CodeBar", codbar);

                                System.out.println("Solicitud Completada Con Operaciones Pendientes => Generar codigo de barra");
                            }

//                             SolCompletada2 = "";
//                             cantOpeComplet2 = 0;
//                             cantOpePend2 = 0;
                            responseCierreSol = true;
                        }

                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        responseCierreSol = false;

                    }

                    gridPrincipal();

                }
            });

            SolCompletada2 = "";
            cantOpeComplet2 = 0;
            cantOpePend2 = 0;

        }

//        } catch (Exception e) {
        responseCierreSol = false;
//        }

//        solConPendientes = 0;
//        contSolCompletada = 0;
        return responseCierreSol;
    }

    public void gridPrincipal() throws SQLException {
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
//        this.listSol = conexSol.getSolDevueltas(UsuarioCustodia);
//
//        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);
//
//        grid.setModel(bandejacustodia);
        id_detalleSolicitud.setVisible(false);
        coder.setVisible(false);
        _idCodeBarRear2.setVisible(false);
        _yyyy.setValue("");
        btnCerrarAll.setVisible(false);
        CargaPag();
        CargaEventos();
    }

    /*
    public boolean cierreConPendientes() {
        System.out.println("entre a cierreConPendientes => ");
        Messagebox.show("Solicitud Completada Con Operaciones Pendientes.", "�Desea Cerrar esta solicitud para ser enviada?.", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException {
                if (Messagebox.ON_OK.equals(e.getName())) {

                    //Cerrar Solcicitud(Cambiar estado a la solicitud)
                    cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerradoConP");
                    //Actualizar cantidad de documentos (Resta documentos Pendientes)
                    boolean updCantidadDocs = false;
                    int restaDocs = 0;
                    int suma2 = cantOpeComplet2 + cantOpePend2;
                    restaDocs = suma2 - cantOpePend2;

                    updCantidadDocs = conexSol.updateSolicitud(id_solicitud, "di_CantDctosSolicitados", restaDocs);

                    boolean cambEstOp = false;
                    //Cambia de estado ooperaciones en tabla tb_Sys_DetSolicitud(Completa)                                    
                    cambEstOp = conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);

                    if (cambioEstado && cambEstOp && updCantidadDocs) {
                        // Se crea hijo de esta para las operaciones con estado Pendiente y se obtiene hijo creado

                        List<Estado> ListEstado = new ArrayList();
                        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

                        ListEstado = estJDBC.getIdEstadoByCodigo("NUPECUS");

                        //se obtiene id de Ubicacion
//                                        List<Ubicacion> lUbi = new ArrayList<Ubicacion>();
                        lUbi = ubicacionJDBC.getUbicacion("dv_DetUbi", "UCC");

                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                        Date dat = new Date();
                        System.out.println("df.format(dat) => " + df.format(dat).toString());
                        //Se obtiene la solicitud padre
                        Solicitud sol = new Solicitud();
                        sol = conexSol.getSolicitud(id_solicitud);

                        newHijosol = new Solicitud();
                        // newHijosol.setDi_IdSolicitud(id_solicitud);
                        newHijosol.setDi_fk_IdUbi(lUbi.get(0).getDi_IdUbicacion());
                        newHijosol.setDi_fk_IdHito(sol.getDi_fk_IdHito());
                        newHijosol.setDi_fk_IdEstado(ListEstado.get(0).getDi_IdEstado());
                        newHijosol.setDdt_FechaCreacion(df.format(dat));
                        newHijosol.setDi_cantidadDocs(cantOpePend2);
                        newHijosol.setDi_SolPadre(sol.getDi_IdSolicitud());

                        crearHijo = conexSol.InsertHijoSol(newHijosol);

                        //Si el hijo se creo correctamente, se agrega hijo al padre
                        if (crearHijo != null) {
                            updPadre = conexSol.updateSolicitud(id_solicitud, "di_SolHijo", crearHijo.getDi_IdSolicitud());
                            System.out.println("updPadre => " + updPadre);
                        }

                        //Una vez actualizado el padre se vinculan las operaciones pendientes a la solic. hijo creada.
                        if (updPadre) {
                            System.out.println("crearHijo.getDi_IdSolicitud() => " + crearHijo.getDi_IdSolicitud());
                            updOperNvaSol = conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdSolicitud", crearHijo.getDi_IdSolicitud());
                        }

                        if (updOperNvaSol) {
                            //Generar Codigo de barra
                            System.out.println("Solicitud Completada Con Operaciones Pendientes => Generar codigo de barra");
                        }

//                             SolCompletada2 = "";
//                             cantOpeComplet2 = 0;
//                             cantOpePend2 = 0;
                        responseCierreSol = true;
                    }

                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                    responseCierreSol = false;
                }
            }
        });
        return responseCierreSol;
    }
     */
    @Listen("onClick = #BtnCancelarGuardarDoc")
    public void cancelaGuardarDocumento() throws SQLException {

        updateDetalleGrid(id_solicitud);
        //  updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);

        id_addDocPanel.setVisible(false);

    }

    @Listen("onClick = #BtnCancelarPend")
    public void cancelaEnviaPend() throws SQLException {

        updateDetalleGrid(id_solicitud);
        //  updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);

        id_addDocPanel.setVisible(false);
        id_addPenPanel.setVisible(false);
        txtObservaciones.setValue(null);

    }

    /**
     * Metodo que retorna cantidad de documentos por producto
     *
     * @return List<ProdTipoDoc>
     */
    public List<ProdTipoDoc> listCountProdTipoDoc() {
        return prodTipoDocConex.getListCountProdTipoDoc();
    }

    public List<ProdTipoDoc> getListTipDoc() {
        List<ProdTipoDoc> ptd = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(detSeleccion.getnOperacion()));
        return ptd;
    }

    //esta funcionalidad levanta popup
    public void agregarObservacion(int op) {

        detSeleccion = new BandejaDetalle();

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == op) {
                detSeleccion = banddettt;
            }
        }

        Map<String, Object> arguments = new HashMap<String, Object>();

        detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("Seleccion", detSeleccion);

        String template = "custodia/detallesPoput.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
}
