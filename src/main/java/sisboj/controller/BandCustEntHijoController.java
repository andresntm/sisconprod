package sisboj.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import config.MvcConfig;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Operaciones;
import sisboj.entidades.Solicitud;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;

/**
 *
 * @author aescobar
 */
@SuppressWarnings("serial")
public class BandCustEntHijoController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid gridPadre;

    @Wire
    Grid grid_DetSol;

    @Wire
    Grid grid2;
    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
    @Wire
    Textbox txt_RutDcto2;
    @Wire
    Textbox txt_NombreDcto2;
    @Wire
    Textbox txt_OperDcto2;
    @Wire
    Panel id_addDocPanel;
    @Wire
    Panel id_addPenPanel;
    @Wire
    Div id_detalleSolicitud;
    @Wire
    Textbox aux;
    @Wire
    Div _divFridDoc;
    @Wire
    Button id_addDocumentos;
    @Wire
    Textbox txt_TioAux;
    @Wire
    Textbox txt_TioAux2;
    @Wire
    Textbox txt_cantDocs;
    @Wire
    Div id_addDocNew;
    @Wire
    Combobox cmb_TipoDocumento;
    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;
    @Wire
    Textbox txt_textObs;
    @Wire
    Textbox txtObservaciones;

    String ValueIdOperaciones;
    String unicacion;
    Window capturawin;

    private final SolicitudJDBC conexSol;
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaCustodia> bandejacustodiaPad;
    ListModelList<BandejaDetalle> DetSolModelList;
    ListModelList<Documento> _listModelDOc;
    Window window;
    Operaciones ope = null;
    TipoDocumentoJDBC tipodocumentos;
    private EventQueue eq;

    ///Informacion del Usuario
    String UsuarioCustodia;
    EstadoJDBC estJDBC;
    boolean cambioEstado = false;
    Solicitud crearHijo;
    Solicitud newHijosol;
    boolean updPadre = false;
    boolean updOperNvaSol = false;
    UbicacionJDBC ubicacionJDBC;
    int solConPendientes;
    List<Ubicacion> lUbi;
    int contSolCompletada;
    boolean responseCierreSol;
    String SolCompletada2;
    int cantOpeComplet2;
    int cantOpePend2;
    int docsEsp;
    int veri = 0;

    private List<BandejaCustodia> quarters;

    public BandCustEntHijoController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        estJDBC = new EstadoJDBC();
        lUbi = new ArrayList<Ubicacion>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {

        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        CargaPag();

    }

    public void CargaPag() throws SQLException {

        Execution execution = Executions.getCurrent();
        String ed = execution.getParameter("numsol_pad");

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }

        int edint = Integer.parseInt(ed);
        System.out.println("--->EDINT " + edint);
        this.quarters = conexSol.getSolNuev_Pad(UsuarioCustodia, edint);
        bandejacustodiaPad = new ListModelList<BandejaCustodia>(quarters);

        gridPadre.setModel(bandejacustodiaPad);
        gridPadre.renderAll();

    }

}
