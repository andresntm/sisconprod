/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import java.util.List;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import config.MvcConfig;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import sisboj.entidades.ActInact;
import sisboj.entidades.Productos;
import sisboj.entidades.UbicacionTblHitosEnt;
import sisboj.entidades.implementaciones.ProductosJDBC;

/**
 *
 * @author excosoc
 */
@SuppressWarnings("serial")
public class ProductosController extends SelectorComposer<Window> {
//public class ProductosController extends SelectorComposer<Window> {

    @Wire
    Textbox txNvoProd;

    @Wire
    Textbox txNvoCodProd;

    @Wire
    Combobox cmbNvoEstProd;

    Date formatFIniD;
    Date formatFFinD;
    //Variable para Filtros
    Date fechaI;
    Date fechaF;
    List<UbicacionTblHitosEnt> ubicacion;
    String oficina = "";
    String region = "";
    String banca = "";
    String rut = "";
    int dmi;
    int dmf;
    String n_of;
    String n_op;
    String prod;
    String filtroNombre;
    
    private List<Productos> listaProductos;
    public String getFiltroNombre() {
        return filtroNombre;
    }

    public void setFiltroNombre(String filtroNombre) {
        this.filtroNombre = filtroNombre;
    }
    List<ActInact> listEstAct;
    private ActInact act1;
    private ActInact act2;
    private ListModelList<ProductoStatus> listProductosStatus;
    private final ProductosJDBC conexProd;
    private static final String footerMessage = "Un Total de %d Registros";
    private boolean displayEdit;

    public ProductosController() throws SQLException {
        conexProd = new MvcConfig().productosJDBC();
        this.listaProductos = conexProd.getListProductos(getFiltroNombre());
        this.listProductosStatus = generateStatusList(conexProd.getListProductos(getFiltroNombre()));// new ListModelList<ProductoStatus>(); //generateStatusList(listaProductos);
        this.displayEdit = true;
        this.act1 = new ActInact();
        this.act2 = new ActInact();
        this.listEstAct = new ArrayList<ActInact>();
    }
    
    
    @Command
    @NotifyChange({"listProductos", "footer"})
    public void buscarConFiltros(@BindingParam("glosaNom") Textbox glosaNom) {
        if (!"".equals(glosaNom.getValue().trim())) {
            setFiltroNombre(glosaNom.getValue().trim());
        } else {
            setFiltroNombre("");
        }
        listProductosStatus = generateStatusList(conexProd.getListProductos(getFiltroNombre()));        
    }
    

    public List<ActInact> getEstAct() {
        listEstAct.clear();
        act1.setNombreEst("Activo");
        act1.setVal(1);
        act2.setNombreEst("Inactivo");
        act2.setVal(0);
        this.listEstAct.add(0, act1);
        this.listEstAct.add(1, act2);
        return listEstAct;
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window win) throws Exception {
        super.doAfterCompose(win);

        this.listaProductos = conexProd.getListProductos(getFiltroNombre());
        getListProductos();
//        this.listProductosStatus = new ListModelList<ProductoStatus>(); //generateStatusList(listaProductos);
    }

    public String getFooter() {
        return String.format(footerMessage, listaProductos.size());
    }

    /**
     * ************************ Editando
     * *******************************************
     */
    public boolean isDisplayEdit() {
        return displayEdit;
    }

    @NotifyChange({"listProductos", "displayEdit"})
    public void setDisplayEdit(boolean displayEdit) {
        this.displayEdit = displayEdit;
    }

    public List<ProductoStatus> getListProductos() {
        // listProductosStatus = generateStatusList(conexProd.getListProductos());
        return listProductosStatus;
    }

    @Command
    public void changeEditableStatus(@BindingParam("prodStatus") ProductoStatus lcs) {
        lcs.setEditingStatus(!lcs.getEditingStatus());
        refreshRowTemplate(lcs);
    }

    @Command
    @NotifyChange({"listProductos", "footer"})
    public void confirm(@BindingParam("prodStatus") final ProductoStatus p) {
        boolean response = false;

        List<Productos> produ = new ArrayList<Productos>();
        List<Productos> produ2 = new ArrayList<Productos>();

        produ = conexProd.nomProductosExiste(p.getProductosStatus().getDv_NomProducto());
        produ2 = conexProd.codProductosExiste(p.getProductosStatus().getDv_CodProducto());

        if (produ.size() > 0 && (produ.get(0).getDi_IdProducto()!= p.getProductosStatus().getDi_IdProducto())) {
            Messagebox.show("Ya existe un Producto con ese nombre, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {

                        List<Productos> td3 = conexProd.selectProductoByID(p.getProductosStatus().getDi_IdProducto());
                        p.getProductosStatus().setDv_NomProducto(td3.get(0).getDv_NomProducto());
                        p.getProductosStatus().setDv_CodProducto(td3.get(0).getDv_CodProducto());
                        p.getProductosStatus().setDv_Activo(td3.get(0).getDv_Activo());
                        changeEditableStatus(p);
                    }
                }
            });
        } else if (produ2.size() > 0 && (produ2.get(0).getDi_IdProducto() != p.getProductosStatus().getDi_IdProducto())) {
            Messagebox.show("Ya existe un Producto con ese codigo, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
                        List<Productos> td3 = conexProd.selectProductoByID(p.getProductosStatus().getDi_IdProducto());
                        p.getProductosStatus().setDv_NomProducto(td3.get(0).getDv_NomProducto());
                        p.getProductosStatus().setDv_CodProducto(td3.get(0).getDv_CodProducto());
                        p.getProductosStatus().setDv_Activo(td3.get(0).getDv_Activo());
                        changeEditableStatus(p);
                    }
                }
            });
        } else {
            Productos producto = new Productos();
            producto.setDi_IdProducto(p.getProductosStatus().getDi_IdProducto());
            producto.setDv_NomProducto(p.getProductosStatus().getDv_NomProducto());
            producto.setDv_CodProducto(p.getProductosStatus().getDv_CodProducto());
            producto.setDv_Activo(p.getProductosStatus().getDv_Activo());

            response = conexProd.updateProducto(producto);
            if (response) {
                listProductosStatus = generateStatusList(conexProd.getListProductos(getFiltroNombre()));
                getListProductos();
//            changeEditableStatus(p);
//            refreshRowTemplate(p);
            }
        }
    }

    public void refreshRowTemplate(ProductoStatus lcs) {
        listProductosStatus.set(listProductosStatus.indexOf(lcs), lcs);

    }

    public String getLimpiaCamposNvoProd() {
        String clean = "";
        return clean;
    }

    private static ListModelList<ProductoStatus> generateStatusList(List<Productos> contributions) {
        ListModelList<ProductoStatus> prodSt = new ListModelList<ProductoStatus>();
        for (Productos lc : contributions) {
            prodSt.add(new ProductoStatus(lc, false));
        }
        return prodSt;
    }

    @Command
    @NotifyChange({"listProductos", "footer", "limpiaCamposNvoProd"})
    public void addNuevoProducto(@BindingParam("txNvoProd") Textbox txNProd, @BindingParam("txNvoCodProd") Textbox txNCodProd, @BindingParam("cmbNvoEstProd") Combobox cbNEstProd) {
        boolean response = false;
        Productos prod = new Productos();
        final String nvoProd = txNProd.getValue().trim();
        final String nvoCodProd = txNCodProd.getValue().trim();
        final String cmbNvoEstP = cbNEstProd.getValue().trim();
        String estadoProd = "";

        if ("Activo".equals(cmbNvoEstP)) {
            estadoProd = "1";
        }
        if ("Inactivo".equals(cmbNvoEstP)) {
            estadoProd = "0";
        }

        List<Productos> nom = conexProd.nomProductosExiste(nvoProd);
        List<Productos> cod = conexProd.codProductosExiste(nvoCodProd);

        if (nom.size() > 0) {
            Messagebox.show("Ya existe un producto con ese nombre, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
//                        fini.setFocus(true);
                    }
                }
            });
        } else if (cod.size() > 0) {
            Messagebox.show("Ya existe un producto con ese codigo, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
//                        fini.setFocus(true);
                    }
                }
            });
        } else {

            if ("".equals(nvoProd) || "".equals(nvoCodProd) || (!"1".equals(estadoProd) && !"0".equals(estadoProd))) {
                Messagebox.show("Debe completar todos los campos(Producto, codigo, estado). \n�Desea Continuar?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_YES.equals(event.getName())) {
                            if ("".equals(nvoProd)) {
//                            txNvoProd.setFocus(true);
                            } else if ("".equals(nvoCodProd)) {
//                            txNvoCodProd.setFocus(true);
                            } else if ("".equals(cmbNvoEstP)) {
//                            cmbNvoEstProd.setFocus(true);
                            }
                        } else if (Messagebox.ON_NO.equals(event.getName())) {
                            getLimpiaCamposNvoProd();
                        }
                    }
                });

            } else {

                prod.setDv_NomProducto(nvoProd);
                prod.setDv_CodProducto(nvoCodProd);
                prod.setDv_Activo(estadoProd);

                try {

                    response = conexProd.insertProducto(prod);

                    if (response) {
                        Messagebox.show("Producto agregado correctamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ON_OK, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_OK.equals(event.getName())) {
                                    if ("".equals(nvoProd)) {
//                                    txNvoProd.setFocus(true);
                                    } else if ("".equals(nvoCodProd)) {
//                                    txNvoCodProd.setFocus(true);
                                    } else if ("".equals(cmbNvoEstP)) {
//                                    cmbNvoEstProd.setFocus(true);
                                    }

                                }
                            }
                        });
                        listProductosStatus = generateStatusList(conexProd.getListProductos(getFiltroNombre()));

                    }
                } catch (Exception e) {
                    Messagebox.show("Error al agregar un producto, intentelo nuevamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ERROR, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            if (Messagebox.ON_OK.equals(event.getName())) {
                                getLimpiaCamposNvoProd();

                            }
                        }
                    });
                }

            }
        }

    }

    /**
     * *************** INICIO EXPORT GRID
     *
     ********************
     * @param gridList
     * @throws java.lang.Exception
     */
    @Command
    public void exportGridToExcel(@BindingParam("gridList") final Grid gridList) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024 * 10);
        gridList.renderAll();
        String fNew = df.format(new Date());
        System.out.println(gridList.getListModel().getSize() + "  size data");
        ExcelExporter exporter = new ExcelExporter();
        exporter.export(gridList, out);
        AMedia amedia = new AMedia("Informe_Custodia_" + fNew + ".xlsx", "xls", "application/file", out.toByteArray());
        Filedownload.save(amedia);
        out.close();
    }

    /*
    @Command
    @NotifyChange({"listUbicacionTblHitos", "listOficinasTblHitos", "listRegionTblHitos", "listBancaTblHitos", "listProductosTblHitos"})
    public void filtrosDinamicos(@BindingParam("fini") final Datebox fini, @BindingParam("ffin") final Datebox ffin, @BindingParam("ofi") final Combobox ofi, @BindingParam("reg") final Combobox reg, @BindingParam("ban") final Combobox ban, @BindingParam("txRut") final Textbox txRut, @BindingParam("txtDMI") final Textbox txDMI, @BindingParam("txtDMF") final Textbox txDMF, @BindingParam("txtNOF") final Textbox txtNOF, @BindingParam("txtNOp") final Textbox txtNOp, @BindingParam("CBProd") final Combobox product) {
        fechaI = fini.getValue();
        fechaF = ffin.getValue();
        //ubicacion = ubi.getVflex();
        oficina = ofi.getValue().trim();
        region = reg.getValue().trim();
        banca = ban.getValue().trim();
        rut = txRut.getValue().trim();
        String dmiStr = txDMI.getValue().trim();
        String dmfStr = txDMF.getValue().trim();
        n_of = txtNOF.getValue().trim();
        n_op = txtNOp.getValue().trim();
        prod = product.getValue().trim();

        for (UbicacionTblHitosEnt ubic : getSelectedObjects()) {
            System.out.println("ubic ====>>>>> " + ubic.getUbicacion());
            listTblComboUbicacionFiltro.add(ubic);
        }

        //dias de mora a entero
        if (dmiStr.equals("")) {
            dmi = 0;
        } else {
            dmi = Integer.parseInt(dmiStr);
        }
        if (dmfStr.equals("")) {
            dmf = 0;
        } else {
            dmf = Integer.parseInt(dmfStr);
        }

        System.out.println("TblHitosController.filtrosDinamicos fechaI => " + fechaI);
        System.out.println("TblHitosController.filtrosDinamicos fechaF => " + fechaF);
        System.out.println("TblHitosController.filtrosDinamicos oficina => " + oficina);
        System.out.println("TblHitosController.filtrosDinamicos region => " + region);
        System.out.println("TblHitosController.filtrosDinamicos banca => " + banca);
        System.out.println("TblHitosController.filtrosDinamicos producto => " + prod);

        int ResResta = 1;
        if (fechaI != null && fechaF != null) {
            String formatFIni = simpleDateFormat.format(fechaI);
            String formatFFin = simpleDateFormat.format(fechaF);
            formatFIni = formatFIni.replace("-", "");
            formatFFin = formatFFin.replace("-", "");
            Long FIni = Long.parseLong(formatFIni);
            Long FFin = Long.parseLong(formatFFin);
            ResResta = (int) (FFin - FIni);
        }

        System.out.println("ResResta => " + ResResta);

        try {
            conexTblHitos.getListProductos(fechaI, fechaF, listTblComboUbicacionFiltro, oficina, region, banca, rut, dmi, dmf, n_of, n_op, prod);
            listTblComboUbicacionFiltro.clear();

        } catch (WrongValueException e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } catch (Exception e) {
            Messagebox.show("ha ocurrido un error en TblHitosController.filtrosDinamicos. \n Error: " + e.toString());
        } finally {

        }

    }
     */
}
