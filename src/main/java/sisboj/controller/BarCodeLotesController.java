/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.oned.Code128Reader;
import config.MvcConfig;
import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.zkoss.bind.BindUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.TipDcto;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;

/**
 *
 * @author esilves
 */
public class BarCodeLotesController extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

    @Wire
    Window resultWin;
    Window capturawin;
    Grid grid_DetSol;
    TipDctoJDBC _tipoDoc;
    EstadoJDBC _estado;
    ListModelList<TipDcto> _tipoDoctoList;
    List<TipDcto> _listtipoDocto = new ArrayList<TipDcto>();
    List<Estado> _estadoDocto = new ArrayList<Estado>();
    @Wire
    Combobox cmb_TipDcto;
    @Wire
    Textbox txt_OperDcto;
    @Wire
    Documento _nuevodocto;

    @Wire
    Textbox txt_GlosaDcto2;

    @Wire
    Textbox txt_GlosaDcto3;

    @Wire
    Textbox id_codebar;

    @Wire
    Textbox pueba;

    @Wire
    Textbox txt_hiddenIdOper;

    @Wire
    String FlagFirmaDelegado;

    @Wire
    Checkbox id_chechApoderado;

    @Wire
    Div _idCodBar;
    @Wire
    Image _idCodeBarRear;

    @Wire
    Image _idCodeBarRear2;

    @Wire
    Image _idCodeBarRear3;
    @Wire
    Label _idCodBarLabel;

    @Wire
    Label _yyyy;

    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Vlayout btn_idvlayoutinfodoc;

    @Wire
    Vlayout _idvlayoutinfodoc;
    @Wire
    Textbox txt_hiddenIdExito;
    private EventQueue eq;
    private final DocumentoJDBC _docto;
    private final OperacionesJDBC _op;
    private final DetSolicitudJDBC _detSol;
    private final SolicitudJDBC conexSol;
    private DetSolicitudJDBC conexDetSol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    Operaciones ope;
    int Id_Operacion;
    BitMatrix encodeCode128 = null;
    BufferedImage bufferedImage128 = null;
    List<BandejaDetalle> nn = null;
    BandCustEntController bcec=new BandCustEntController();
    private Object id_solicitud;
    private int id_sol;

    ListModelList<BandejaDetalle> DetSolModelList;
    
    public BarCodeLotesController() throws SQLException {
        _tipoDoc = new MvcConfig().tipDctoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        _docto = new MvcConfig().documentosJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        _detSol = new MvcConfig().detSolicitudJDBC();
        conexSol = new MvcConfig().solicitudJDBC();
         conexDetSol = new MvcConfig().detSolicitudJDBC();
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        resultWin = (Window) comp;
        
        //eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);
        Execution execution = Executions.getCurrent();
        capturawin = (Window) execution.getArg().get("idWinController");
        grid_DetSol = (Grid) execution.getArg().get("grid_DetSol");
       
        System.out.println("LOTES IDSOL " + execution.getArg().get("id_solicitud"));
        id_solicitud = execution.getArg().get("id_solicitud");
        id_sol = (Integer) id_solicitud;

        
        List list2;
        List list3;
        ListModelList lm2;
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.listTipDcto();
        _tipoDoctoList = new ListModelList<TipDcto>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        //   try{
        list2 = new ArrayList();
        list3 = new ArrayList();

        // country.setModel(ListModels.toListSubModel(new ListModelList(getAllItems())));
        for (final TipDcto tipodocto : _listtipoDocto) {
            list2.add(tipodocto.getDv_NomTipDcto());
        }

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        lm2 = new ListModelList(list2);
        lm3 = new ListModelList(list3);
        lm2.addSelection(lm2.get(0));
        lm3.addSelection(lm3.get(0));

        //remplazar por el numero de solicitud
        //nn = _detSol.listDetSolicitud(1);
        nn = _detSol.listDetSolicitud(id_sol);

        id_codebar.setFocus(true);

    }

    @Listen("onClick = #closeButton")
    public void close() {
        resultWin.detach();
    }

    public boolean GuardarDocumento(String _codigoOperOrigen) {
        int id_documento = 0;
        boolean valuereturn = false;
        String tipodoctodesc = "PAGARE";

        // Agregagos el Nuevo Docuemnto encontrado
        this._nuevodocto = new Documento();
        boolean checkapbanco = false;
        this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
        this._nuevodocto.setDb_checkOk(true);
        this._nuevodocto.setDi_fk_IdUbi(2);
        this._nuevodocto.setDv_Glosa("Documento Escaneado Por C�digo de Barra");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        // dateFormat.format(date)
        this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
        this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
        this._nuevodocto.setDi_fk_IdEstado(152);
        // resultWin.getParent().
        ope = _op.getOperaciones(_codigoOperOrigen);

        Id_Operacion = ope.getDi_IdOperacion();
        //txt_hiddenIdOper.setValue(Integer.toString(Id_Operacion));
        this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());

        for (final TipDcto tipodocto : _listtipoDocto) {
            if (tipodocto.getDv_NomTipDcto().equals(tipodoctodesc)) {
                id_documento = tipodocto.getDi_IdTipDcto();
            }
        }
        this._nuevodocto.setDi_fk_TipoDcto(id_documento);

        // Messagebox.show("DatosCamputados::::checkapbanco[" + checkapbanco + "]txt_OperDcto.getValue()[" + txt_OperDcto.getValue() + "]txt_GlosaDcto2.getValue()[" + txt_GlosaDcto2.getValue() + "]cmb_TipDcto.getValue()[" + cmb_TipDcto.getValue() + "]id_documento[" + id_documento + "]ope.id[" + ope.getDi_IdOperacion() + "]");
        boolean insert = _docto.insert(_nuevodocto);

        // Messagebox.show("DatosCamputados:::");
        if (insert) {
            //  txt_hiddenIdExito.setValue("true");
            //window.setVisible(false);
            Messagebox.show("Se Agreg� Correctamente el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) throws SQLException {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        // Events.postEvent("onRefresh", resultWin, null);
                        
                        // Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                        // bcec.refreshId(id_sol);
                         //Events.postEvent("onRefresh",resultWin, null);
                         //BindUtils.postGlobalCommand(null, null, "refreshDropDown", null);
                         
                         
                          //resultWin.detach();   
                        // resultWin.detach();
                        //  Executions.sendRedirect("/sisboj/index");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        //Cancel is clicked
                        // Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                        //((Div)((Window)resultWin.getParent()).getFellow("_divFridDoc")).setVisible(true);
                        // txt_hiddenIdExito.setValue("false");
                        // resultWin.detach();

                        // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                    }
                }
            }
            );

            // si foy el ultimo documento entonces termino y cierro solicitud
            int numdocsagregados = 0;//conexDetSol.NumeroDocumentosAgregadosSolicitud(id_solicitud);
            // ojo aca el valor de getNumeroDocumentosSolicitud se agrego de forma estatica,se debe arreglar

            int numerototaldedocs = 0; //conexSol.getNumeroDocumentosSolicitud(id_solicitud);     
            for (final BandejaDetalle nnn : nn) {
                if (nnn.getnOperacion().equals(_codigoOperOrigen)) {
                    //_numdoctoEsperado=nnn.getNumero_docs_esperado();
                    numdocsagregados = numdocsagregados + nnn.getNumero_docs();
                    numerototaldedocs = numerototaldedocs + nnn.getNumero_docs_esperado();
                    //id_documento = tipodocto.getDi_IdTipDcto();
                }
            }
            numdocsagregados = numdocsagregados + 1;

            if (numdocsagregados == numerototaldedocs) {

                if (conexSol.CerrarOCambiarEstadoSolicitud(1, "cerrado")) {
                    Messagebox.show("Se Cerro correctamente la Solicitud ", "Terminar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) throws SQLException {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                                //OK is clicked
                                // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);

                                /*conexDetSol = new MvcConfig().detSolicitudJDBC();
                                listDet = conexDetSol.listDetSolicitud(id_sol);
                                
                                DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
                                ((Grid)((Window)resultWin.getParent()).getFellow("grid_DetSol")).setModel(DetSolModelList);*/
                                
                               // bcec.updateDetalleGrid(id_sol);
                               
                                
                                //  Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                                // Events.postEvent("onRefresh", self, null); 
                                // self.detach();   
                                //resultWin.detach();
                                //  Executions.sendRedirect("/sisboj/index");
                            } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                //Cancel is clicked
                                // Events.sendEvent(new Event("onClick", (Button)((Window)resultWin.getParent()).getFellow("btnrefresh") ));
                                //((Div)((Window)resultWin.getParent()).getFellow("_divFridDoc")).setVisible(true);
                                // txt_hiddenIdExito.setValue("false");
                                // resultWin.detach();
                                // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                            }
                        }
                    }
                    );
                } else {
                    Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                            new org.zkoss.zk.ui.event.EventListener() {
                        public void onEvent(Event e) {
                            if (Messagebox.ON_OK.equals(e.getName())) {
                                //OK is clicked
                                // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                                Executions.sendRedirect("/sisboj/index");
                            } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                                //Cancel is clicked
                                // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                            }
                        }
                    }
                    );

                }

            }

            return true;

        } else {
            Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        Executions.sendRedirect("/sisboj/index");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        //Cancel is clicked
                        // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                    }
                }
            }
            );
            return false;
        }

    }

    @Listen("onClick = #FocusTextBox")
    public void Focus() {

        txt_GlosaDcto2.setFocus(true);

    }

    
    
    public void generaCambios(String Barcode) throws WriterException, SQLException {
        //  pueba.setValue("");
        //  pueba.setText("");
        boolean result = _detSol.getDetSolicitud(id_sol, Barcode);
        
        System.out.println("**RESULT "+result);
        System.out.println("**BARCODE "+Barcode);
        
        //Preguntar si existe el documentos en documentos
        int _numDocs = _docto.getDocumentoPorOperacion(Barcode);
        

        System.out.println("_NUMDOCS " + _numDocs);
        //List<BandejaDetalle> nn=null;
        // recorremos la lista de operacion de la solicitus en busca de la cantidad de documentos esperados de la solicitus
        int _numdoctoEsperado = 0;
        for (final BandejaDetalle nnn : nn) {
            if (nnn.getnOperacion().equals(Barcode)) {
                _numdoctoEsperado = nnn.getNumero_docs_esperado();
                //id_documento = tipodocto.getDi_IdTipDcto();
            }
        }

        if (_numdoctoEsperado == _numDocs) {
            Messagebox.show("Operacion Completa Ingrese Otro Documento");

            _idCodeBarRear2.setVisible(false);

            _yyyy.setValue("");
            id_codebar.setFocus(true);
            return;
        }

        
        /*Metodo Comentado*/
        GuardarDocumento(Barcode);

        
        _idCodeBarRear2.setVisible(true);
        //comsultar la cantidad esperada de  documento
        //int _numeroDocEsperado=
        //String input;
        //input=Barcode.replaceAll("\\r?\\n", "");
        String CodeTemp = Barcode;
        Messagebox.show("BarCode Escaneado :[" + Barcode + "] existe En solicitud?[" + result + "]_numDocs:[" + _numDocs + "]_numdoctoEsperado:[" + _numdoctoEsperado + "]");
        encodeCode128 = new Code128Writer().encode(CodeTemp, BarcodeFormat.CODE_128, 150, 80, null);
        
       
        bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);

        //_idCodeBarRear.
        //_idCodeBarRear.setContent(bufferedImage128);
        _idCodeBarRear2.setContent(bufferedImage128);
        // _idCodeBarRear3.setContent(bufferedImage128);
        //_idCodBarLabel.setValue("");
        //_idCodBarLabel.setValue(Barcode);

        _yyyy.setValue(Barcode);
        id_codebar.setFocus(true);

        
         /*conexDetSol = new MvcConfig().detSolicitudJDBC();
                                listDet = conexDetSol.listDetSolicitud(id_sol);
                                
                                DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
                                ((Grid)((Window)resultWin.getParent()).getFellow("grid_DetSol")).setModel(DetSolModelList);*/
                                
           // bcec.updateDetalleGrid(id_sol);
           
             
           
       //Events.postEvent("onRefresh",resultWin, null);   
       
       
        //this.resultWin.detach();
        //pueba.setValue("");  
    }
    
    public void onChanging$txt_GlosaDcto3() {
        Messagebox.show("BarCode Escaneado :[" + txt_GlosaDcto3.getValue() + "]");
//System.out.println("Username3 : " + userName.getValue()); 
    }

    @Listen("onChanging = #txt_GlosaDcto3")
    public void onChangingTextbox(Event event) {

        Textbox row = (Textbox) event.getTarget();
        String byEvent = row.getValue();
        Messagebox.show("�???? :[" + event.toString() + "]value[" + byEvent + "]");
        String input;
        input = byEvent.replaceAll("\\r?\\n", "");
        txt_GlosaDcto3.setValue(row.getValue());

        if (input.equals("")) {
            row.setValue("");
            txt_GlosaDcto3.setValue("");
            Messagebox.show("RETORNANDO...");
            return;
        }
        // Textbox nn=(Textbox)event.;
        //   byEvent = event.getValue();
        Messagebox.show("BarCode Escaneado :[" + row.getValue() + "]");
        row.setValue("");
        txt_GlosaDcto3.setValue("");
    }

}
