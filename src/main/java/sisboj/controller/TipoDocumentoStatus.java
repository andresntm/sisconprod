/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import sisboj.entidades.TipoDocumento;

/**
 *
 * @author EXVGUBA
 */
public class TipoDocumentoStatus {

    private TipoDocumento td;
    private boolean editingStatus;

    public TipoDocumentoStatus(TipoDocumento lc, boolean editingStatus) {
        this.td = lc;
        this.editingStatus = editingStatus;
    }

    public TipoDocumento getTipoDocumentoStatus() {
        return td;
    }

    public boolean getEditingStatus() {
        return editingStatus;
    }

    public void setEditingStatus(boolean editingStatus) {
        this.editingStatus = editingStatus;
    }
}
