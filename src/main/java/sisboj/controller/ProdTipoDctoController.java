/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.controller;

import java.util.List;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import config.MvcConfig;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import sisboj.entidades.ActInact;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.Productos;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.UbicacionTblHitosEnt;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.ProductosJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;

public class ProdTipoDctoController extends SelectorComposer<Window> {

    @Wire
    Textbox txNvoProd;

    @Wire
    Textbox txNvoCodProd;

    @Wire
    Combobox cmbNvoTDrod;

    Date formatFIniD;
    Date formatFFinD;
    //Variable para Filtros
    Date fechaI;
    Date fechaF;
    List<UbicacionTblHitosEnt> ubicacion;
    String oficina = "";
    String region = "";
    String banca = "";
    String rut = "";
    int dmi;
    int dmf;
    String n_of;
    String n_op;
    String prod;

    List<ActInact> listEstAct;
    private ActInact act1;
    private ActInact act2;
    private ListModelList<ProdTipoDctoStatus> listProdTipoDocStatus;
    private ListModel<ProdTipoDctoStatus> listProdTipoDocStatus2;
    private final ProdTipoDocJDBC conexTD;
    private static final String footerMessage = "Un Total de %d Registros";
    private boolean displayEdit;
    String filtroNombre;

    public String getFiltroNombre() {
        return filtroNombre;
    }

    public void setFiltroNombre(String filtroNombre) {
        this.filtroNombre = filtroNombre;
    }

    private final TipoDocumentoJDBC conexTD2;

    private final ProductosJDBC conexProd;

    private List<Productos> listaProductos;
    private List<TipoDocumento> listaTipoDoc;
    private List<TipoDocumento> listaTipoDoc2;

    public ProdTipoDctoController() throws SQLException {
        conexTD = new MvcConfig().prodTipoDocJDBC();
        this.listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc(getFiltroNombre()));//new ListModelList<ProdTipoDctoStatus>();
        conexTD2 = new MvcConfig().tipoDocumentoJDBC();
        conexProd = new MvcConfig().productosJDBC();
        this.listaProductos = conexProd.getListProductos(getFiltroNombre());
        this.listaTipoDoc = conexTD2.getListTipoDocumento("");
        this.listaTipoDoc2 = new ArrayList<TipoDocumento>();
//        this.listProdTipoDocStatus2 = generateStatusList(conexTD.getListProdTipoDoc());
        this.displayEdit = true;
        this.act1 = new ActInact();
        this.act2 = new ActInact();
        this.listEstAct = new ArrayList<ActInact>();
    }

    @Command
    @NotifyChange({"listProdTipoDoc", "footer"})
    public void buscarConFiltros(@BindingParam("glosaNom") Textbox glosaNom) {
        if (!"".equals(glosaNom.getValue().trim())) {
            setFiltroNombre(glosaNom.getValue().trim());
        } else {
            setFiltroNombre("");
        }
        listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc(getFiltroNombre()));
    }

    public List<ActInact> getEstAct() {
        listEstAct.clear();
        act1.setNombreEst("Activo");
        act1.setVal(1);
        act2.setNombreEst("Inactivo");
        act2.setVal(0);
        this.listEstAct.add(0, act1);
        this.listEstAct.add(1, act2);
        return listEstAct;
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window win) throws Exception {
        super.doAfterCompose(win);
        this.listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc(getFiltroNombre()));
        getListProdTipoDoc();
    }

    public String getFooter() {
        return String.format(footerMessage, conexTD.getListProdTipoDoc(getFiltroNombre()).size());
    }

    /**
     * ************************ Editando
     * *******************************************
     */
    public boolean isDisplayEdit() {
        return displayEdit;
    }

    @NotifyChange({"listProdTipoDoc", "displayEdit"})
    public void setDisplayEdit(boolean displayEdit) {
        this.displayEdit = displayEdit;
    }

    public List<ProdTipoDctoStatus> getListProdTipoDoc() {
        // listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc());
        return listProdTipoDocStatus;
    }

    public List<Productos> getListProd() {
        this.listaProductos = conexProd.getListProductos(getFiltroNombre());
//        listaProductos.get(0).getDv_NomProducto();
//        listaProductos.get(0).getDi_IdProducto();
        return listaProductos;
    }

    public List<TipoDocumento> getListTipoDoc() {
        this.listaTipoDoc = conexTD2.getListTipoDocumento("");
        return listaTipoDoc;
    }

    @Command
    public void changeEditableStatus(@BindingParam("prodStatus") ProdTipoDctoStatus lcs) {
        lcs.setEditingStatus(!lcs.getEditingStatus());
        refreshRowTemplate(lcs);
    }

    @Command
    @NotifyChange({"listProdTipoDoc", "footer"})
    public void confirm(@BindingParam("prodStatus") final ProdTipoDctoStatus p) {
        boolean response = false;
        List<ProdTipoDoc> tdoc = new ArrayList<ProdTipoDoc>();

        tdoc = conexTD.prodTipoDocExiste(p.getProdTipoDocStatus().getDi_IdProduc(), p.getProdTipoDocStatus().getDi_IdTipDcto());
        System.out.println("tdoc.size() => " + tdoc.size());
        if (tdoc.size() > 0 && (tdoc.get(0).getId_ProdTipDoc() != p.getProdTipoDocStatus().getId_ProdTipDoc())) {
            Messagebox.show("Ya existe este Tipo de Documento para al producto seleccionado, favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {

                        List<ProdTipoDoc> td3 = conexTD.selectProdTipoDctoByID(p.getProdTipoDocStatus().getId_ProdTipDoc());
                        p.getProdTipoDocStatus().setDi_IdProduc(td3.get(0).getDi_IdProduc());
                        p.getProdTipoDocStatus().setDi_IdTipDcto(td3.get(0).getDi_IdTipDcto());
                        p.getProdTipoDocStatus().setBit_activo(td3.get(0).getBit_activo());
                        p.getProdTipoDocStatus().setObligatorio(td3.get(0).getObligatorio());
                        changeEditableStatus(p);
                    }
                }
            });
        } else {
            ProdTipoDoc td = new ProdTipoDoc();
            td.setId_ProdTipDoc(p.getProdTipoDocStatus().getId_ProdTipDoc());
            td.setDi_IdProduc(p.getProdTipoDocStatus().getDi_IdProduc());
            td.setDi_IdTipDcto(p.getProdTipoDocStatus().getDi_IdTipDcto());
            td.setBit_activo(p.getProdTipoDocStatus().getBit_activo());
            td.setObligatorio(p.getProdTipoDocStatus().getObligatorio());
            response = conexTD.updateProdTipoDoc(td);
            if (response) {

                listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc(getFiltroNombre()));
                getListProdTipoDoc();
//            changeEditableStatus(p);
//            refreshRowTemplate(p);
            }
        }
    }

    public void refreshRowTemplate(ProdTipoDctoStatus lcs) {
        //replace the element in the collection by itself to trigger a model update
        listProdTipoDocStatus.set(listProdTipoDocStatus.indexOf(lcs), lcs);
    }

    public String getLimpiaCamposNvoProd() {
        String clean = "";
        return clean;
    }

    public int getLimpiaCamposNvoNumerico() {
        int clean = 0;
        return clean;
    }

    private static ListModelList<ProdTipoDctoStatus> generateStatusList(List<ProdTipoDoc> contributions) {
        ListModelList<ProdTipoDctoStatus> prodSt = new ListModelList<ProdTipoDctoStatus>();
        for (ProdTipoDoc lc : contributions) {
            prodSt.add(new ProdTipoDctoStatus(lc, false));
        }
        return prodSt;
    }

    
    
     @Command
    @NotifyChange({"listClearDoc"})
    public void getListClearDoc(@BindingParam("cmb_doc") final Combobox doc) {
       doc.setValue(null);
    }
            
    
    @Command
    @NotifyChange({"listTipoDoc3"})
    public List<TipoDocumento> getListTipoDoc2(@BindingParam("cmb_prod") final Combobox prod) {
        String prd = "";

        getListTipoDoc3().clear();
        if (!"".equals(prod.getValue()) && prod.getValue() != null) {
            prd = prod.getValue();
            this.listaTipoDoc2 = conexTD2.getListTipoDocumentoPorProd(prd);
        }

        return listaTipoDoc2;
    }

    public List<TipoDocumento> getListTipoDoc3() {
        System.out.println("Cantidad de documentos disponibles (ProdTipoDctoController.getListTipoDoc3)=> " + listaTipoDoc2.size());
        return listaTipoDoc2;
    }

    //cmb_prod=cmb_prod,cmb_doc=cmb_doc,cmb_obl=cmb_obl,cmbNvoEstTD=cmbNvoEstTD
    @Command
    @NotifyChange({"listProdTipoDoc","footer"})
    public void addNuevoProdTipoDoc(@BindingParam("cmb_prod") final Combobox cmb_prod, @BindingParam("cmb_doc") final Combobox cmb_doc, @BindingParam("cmb_obl") final Combobox cmb_obl, @BindingParam("cmbNvoEstTD") final Combobox cmbNvoEstTD) {
        boolean response = false;

        ProdTipoDoc ptd = new ProdTipoDoc();
        String prod = cmb_prod.getValue().trim();
        String doc = cmb_doc.getValue().trim();
        String obl = cmb_obl.getValue().trim();
        String cbNvoEstTD = cmbNvoEstTD.getValue().trim();

        System.out.println(" prod => " + prod);
        System.out.println(" doc => " + doc);
        System.out.println(" obl => " + obl);
        System.out.println(" cbNvoEstTD => " + cbNvoEstTD);

        String estadoTD = "";

        if ("Activo".equals(cbNvoEstTD)) {
            estadoTD = "1";
        }
        if ("Inactivo".equals(cbNvoEstTD)) {
            estadoTD = "0";
        }

        List<ProdTipoDoc> tdoc = new ArrayList<ProdTipoDoc>();
//        List<ProdTipoDoc> tdoc2 = new ArrayList<ProdTipoDoc>();

        tdoc = conexTD.prodTipoDocExiste2(prod, doc);
//        tdoc2 = conexTD.codProdTipoDocExiste(nvoCodTD);
        if (tdoc.size() > 0) {
            Messagebox.show("El documento seleccionado ya esta asociado al producto , favor intente con otro", "Mensaje Administrador", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_OK.equals(event.getName())) {
//                        fini.setFocus(true);
                    }
                }
            });
        }

        if ("".equals(prod) || "".equals(doc) || "".equals(obl) || "".equals(cbNvoEstTD)) {
            Messagebox.show("Debe completar todos los campos. \n�Desea Continuar?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION, new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    if (Messagebox.ON_YES.equals(event.getName())) {

                    } else if (Messagebox.ON_NO.equals(event.getName())) {
                        getLimpiaCamposNvoProd();
                        getLimpiaCamposNvoNumerico();
                        
                        cmb_prod.setValue(null);
                        cmb_doc.setValue(null);
                        cmb_obl.setValue(null);
                        cmbNvoEstTD.setValue(null);
                    }
                }
            });

        } else {

            ptd.setDv_NomProduc(prod);
            ptd.setDv_NomTipDcto(doc);
            ptd.setBit_activo(estadoTD);
            ptd.setObligatorio(obl);

            try {

                response = conexTD.insertProdTipoDoc(ptd);

                if (response) {
                    Messagebox.show("Registro Agregado correctamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ON_OK, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            if (Messagebox.ON_OK.equals(event.getName())) {
//                                if ("".equals(nvoTD)) {
////                                    txNvoProd.setFocus(true);
//                                } else if ("".equals(nvoTD)) {
////                                    txNvoCodProd.setFocus(true);
//                                }

                            }
                        }
                    });

                     this.listProdTipoDocStatus = generateStatusList(conexTD.getListProdTipoDoc(getFiltroNombre()));
                     listaTipoDoc2.clear();
                }
            } catch (Exception e) {
                Messagebox.show("Error al agregar un Tipo de Documento, intentelo nuevamente.", "Mensaje Administrador", Messagebox.OK, Messagebox.ERROR, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_OK.equals(event.getName())) {
                            getLimpiaCamposNvoProd();
                            getLimpiaCamposNvoNumerico();

                        }
                    }
                });
            }

        }
    }

    /**
     * *************** INICIO EXPORT GRID
     *
     ********************
     * @param gridList
     * @throws java.lang.Exception
     */
    @Command
    public void exportGridToExcel(@BindingParam("gridList") final Grid gridList) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024 * 10);
        gridList.renderAll();
        String fNew = df.format(new Date());
        System.out.println(gridList.getListModel().getSize() + "  size data");
        ExcelExporter exporter = new ExcelExporter();
        exporter.export(gridList, out);
        AMedia amedia = new AMedia("Informe_DocumentosPorProducto_" + fNew + ".xlsx", "xls", "application/file", out.toByteArray());
        Filedownload.save(amedia);
        out.close();
    }

}
