package sisboj.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import config.MvcConfig;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.System.out;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.RadioButton;
import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.attribute.standard.PrinterState;
import javax.print.attribute.standard.PrinterStateReason;
import javax.print.attribute.standard.PrinterStateReasons;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.zkoss.util.media.AMedia;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.JerarquiaSolEnt;
import sisboj.entidades.Operaciones;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.Productos;
import sisboj.entidades.Solicitud;
import sisboj.entidades.TipDcto;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.JerarquiaSolJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.ProductosJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;
import sisboj.utilidades.DiccionarioCodeZpl;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.Attribute;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.PrinterInfo;
import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.attribute.standard.PrinterLocation;
import javax.print.attribute.standard.PrinterMakeAndModel;
import javax.print.attribute.standard.PrinterName;
import javax.print.attribute.standard.PrinterState;
import org.zkoss.zul.event.PagingEvent;
import sisboj.entidades.DevolucionEnt;
import sisboj.entidades.MotivoDevolucionEnt;
import sisboj.entidades.TipoDevolucionEnt;
import sisboj.entidades.implementaciones.DevolucionJDBC;
import sisboj.entidades.implementaciones.MotivoDevolucionJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;

/**
 *
 * @author esilvestre
 */
@SuppressWarnings("serial")
public class BandBojRecController extends SelectorComposer<Window> {

    @Wire
    Grid grid;

    @Wire
    Grid gridPadre;

    @Wire
    Grid grid_DetSol;

    @Wire
    Auxheader auxColDet;

    @Wire
    Grid grid2;
    @Wire
    Grid grid_Dctos;
    @Wire
    Textbox txt_RutDcto;
    @Wire
    Textbox txt_NombreDcto;

    @Wire
    Textbox id_codebar;

    @Wire
    Textbox glosaNom;

    @Wire
    Textbox glosaNom2;

    @Wire
    Combobox cmb_motivo_dev;

    @Wire
    Combobox cmb_motivo_dev3;

    @Wire
    Combobox cmb_motivo_rep;

//    @Wire
//    Detail onOpenFal;
    @Wire(".folder")
    Detail onOpenFal; // wire to button 2

    @Wire
    Textbox txt_OperDcto;
    @Wire
    Textbox txt_GlosaDcto;
    @Wire
    Textbox txt_RutDcto2;
    @Wire
    Textbox txt_NombreDcto2;
    @Wire
    Textbox txt_OperDcto2;

    @Wire
    Textbox motDevGen;

    @Wire
    Panel id_addDocPanel;
    @Wire
    Panel id_addDevPanel;
    @Wire
    Div id_detalleSolicitud;
    @Wire
    Textbox aux;
    @Wire
    Div _divFridDoc;
    @Wire
    Button id_addDocumentos;
    @Wire
    Div coder;

    @Wire
    Div codeBarSoldiv;

    @Wire
    Image _idCodeBarRear2;

    @Wire
    Image _idCodeBarRear23;

    @Wire
    Button btnrefresh;

    @Wire
    Button btnCerrarAll;

    @Wire
    Label _yyyy;

    @Wire
    Label _yyyyy;

    @Wire
    Label viewrutcliente1;
    @Wire
    Textbox txt_TioAux;

    @Wire
    Textbox txt_GobsSol;

    @Wire
    Textbox txt_TioAux2;
    @Wire
    Textbox txt_cantDocs;

    @Wire
    Div id_addDocNew;

    @Wire
    Div obsSolRep;

    @Wire
    Combobox cmb_TipoDocumento;
    @Wire
    Combobox cmb_EstDcto;
    @Wire
    Textbox txt_GlosaDcto2;
    @Wire
    Checkbox id_chechApoderado;
    @Wire
    Textbox txt_docsEsperados;
    @Wire
    Textbox txt_docsEsperados2;
    @Wire
    Textbox txt_textObs;
    //Inicio pendientes
    @Wire
    Textbox txtObservaciones;
    @Wire
    Radio rb_dev;
    @Wire
    Radio rb_rep;

    @Wire
    Div devolv_div;

//    @Wire
//    Radio rb_devol;
    @Wire
    Radio rb_dev3;
    @Wire
    Radio rb_rep3;

    @Wire
    Radio rb_devol;
    @Wire
    Radio rb_recep;

    @Wire
    Radiogroup sv2;

    @Wire
    Radiogroup sv3;

//    int idOpe2=0;
    String ValueIdOperaciones;
    String unicacion;
    @Wire
    Window capturawin;

    @Wire
    Iframe iframe;

    @Wire
    Paging pg;

    @Wire
    Button btnCerrarAllTerm;

    @Wire
    Button id_printCodSol;

    @Wire
    Button btnCerrarAllPend;

    @Wire
    private Window idWinController;
    BitMatrix encodeCode128 = null;
    BitMatrix encodeCode128Sol = null;
    BitMatrix encodeCode128SolP = null;

    BufferedImage bufferedImage128 = null;
    BufferedImage bufferedImage128Sol = null;
    BufferedImage bufferedImage128SolP = null;

    private List<TipoDocumento> _listtipoDocto = new ArrayList<TipoDocumento>();
    List<TipDcto> _listtipoDoctox = new ArrayList<TipDcto>();
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private final ProdTipoDocJDBC prodTipoDocConex;
    private DetSolicitudJDBC conexDetSol;
    private ProductosJDBC conexProdCod;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    ListModelList<BandejaCustodia> bandejacustodia;
    ListModelList<BandejaCustodia> bandejacustodiaPad;
    ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    private final MotivoDevolucionJDBC motivoDevolucionJDBC;

    Window window;
    private final OperacionesJDBC _op;
    Operaciones ope = null;
    TipoDocumentoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private BandejaDetalle detSeleccion = new BandejaDetalle();
    private int id_solicitud = 0;
    private int idDetSolGlobal = 0;
    private String nameProductCode;
    private String fecSolicCode;
    private EventQueue eq;
    private boolean bNuevo;
    private int idOperRetorno = 0;
    private ListModelList<TipoDocumento> _tipoDoctoList;
    private TipoDocumentoJDBC _tipoDoc;
    private EstadoJDBC _estado;
    private int Id_Operacion;
    private int numDctsOpAct = 0;
    private List<ProdTipoDoc> prTipoDoc;

    List<BandejaDetalle> nnn = null;
    List<BandejaCustodia> nn = null;

    List<BandejaDetalle> nnCierra = null;
    private int oPer;
    private int nDoc;
    private int docEs;
    private int activePage;
    private int cantOpera;
    ///Informacion del Usuario
    String UsuarioCustodia;
    private Documento _nuevodocto;
    private final DocumentoJDBC _docto;
    EstadoJDBC estJDBC;
    boolean cambioEstado = false;
    Solicitud crearHijo;
    Solicitud newHijosol;
    boolean updPadre = false;
    boolean updOperNvaSol = false;
    UbicacionJDBC ubicacionJDBC;
    int solConPendientes;
    List<Ubicacion> lUbi;
    List<Productos> lprod;
    int contSolCompletada;
    int contSolRecepcionada;
    int contSolReparo;
    boolean responseCierreSol;
    boolean responseCierreSolBoj;
    String SolCompletada2;
    String SolCompletadaBoj;
    int cantOpeComplet2;
    int cantOpePend2;

    int contSolRecepcionada2;
    int contSolReparo2;
    int cantOpDev;
    int cantOpRep;
    int cantTotalOp;

    int docsEsp;
    int veri = 0;
    int idSolHijo;
    JerarquiaSolJDBC jerarquiaSolJDBC;
    PDStream pss;

    float rimpagString;
    float rimpagImage;
    private String devrepchange;
    boolean responseVal2;
    private String idSolicitudDecodeFinal = "";

    DevolucionJDBC devolucionJDBC;

    public BandBojRecController() throws SQLException {
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        _docto = new MvcConfig().documentosJDBC();
        estJDBC = new EstadoJDBC();
        _tipoDoc = new MvcConfig().tipoDocumentoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        prodTipoDocConex = new MvcConfig().prodTipoDocJDBC();
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        conexProdCod = new MvcConfig().productosJDBC();
        lUbi = new ArrayList<Ubicacion>();
        lprod = new ArrayList<Productos>();
        ubicacionJDBC = new MvcConfig().ubicacionJDBC();
        jerarquiaSolJDBC = new MvcConfig().jerarquiaSolJDBC();
        motivoDevolucionJDBC = new MvcConfig().motivoDevolucionJDBC();
        devolucionJDBC = new MvcConfig().devolucionJDBC();

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {

        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, true);

        CargaPag();
        CargaEventos();
        // CargaPagSort(0);

    }

    public void CargaPag() throws SQLException {

        Session sess = Sessions.getCurrent();

        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();

        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }

        //si es usuario de boj custodia seteamos la Ubicacion en UCC
        if (UsuarioCustodia.equals("BOJ_CUSTODIAUCC")) {
            UsuarioCustodia = "UCC";
        }
        //iframe.setVisible(false);

        try{
        this.listSol = conexSol.getSolEnviadasBojs(UsuarioCustodia, "");

// this.listSol = conexSol.getSolEnviadas(UsuarioCustodia);
//
//        this.listSol = conexSol.getSolEnviadas(UsuarioCustodia);
        //nn = conexSol.getSolEnviadasBojs(UsuarioCustodia);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        for (final BandejaCustodia nnn : bandejacustodia) {

            System.out.println("Estado Soli Recibidas 01--> " + nnn.getEstado());
            //Se cambia estado de enviado a recibido
            nnn.setEstado("Recibido");

        }

        /*Se setea paginador paging id="pg"*/
        pg.setTotalSize(listSol.size());
        pg.setPageSize(10);
        pg.setDetailed(true);

        /*Se setea grid mold y se asigna paging id="pg" a grid*/
        grid.setMold("paging");
        grid.setPaginal(pg);

        /*Se setea model*/
        grid.setModel(bandejacustodia);

        /*Se crea evento onCreate para el grid asi genera los eventos click para los rows*/
        grid.addEventListener("onCreate", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                System.out.println("ONCREATE");
                for (Component row : grid.getRows().getChildren()) {
                    row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                        public void onEvent(Event arg0) throws Exception {
                            Row row = (Row) arg0.getTarget();
                            Row row3 = (Row) arg0.getTarget();
                            BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                            String value = banc.getProducto();
                            Component c = row.getChildren().get(1);
                            Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                            //Capturo todos los atributos de llegada
                            String nnnnn = row.getAttributes().toString();
                            //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                            id_solicitud = banc.getNumsol();
                            nameProductCode = banc.getProducto();
                            fecSolicCode = banc.getFechaRegistro();
                            cantOpera = banc.getNumDctos();

                            String bard = banc.getCodeBar();

                            encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);

                            bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
                            _idCodeBarRear23.setContent(bufferedImage128);
                            _yyyyy.setValue(bard);
                            idSolicitudDecodeFinal = bard;

                            _idCodeBarRear23.setVisible(true);
                            _yyyyy.setVisible(true);

//                    String bard = banc.getCodeBar();
//                    
//                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);
//
//                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
//                    _idCodeBarRear2.setContent(bufferedImage128);
//                    _yyyy.setValue(bard);
//                    
//                    
//                    _idCodeBarRear2.setVisible(true);
//                    _yyyy.setVisible(true);
//                    String bard = banc.getCodeBar();
//                    
//                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);
//
//                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
//                    _idCodeBarRear2.setContent(bufferedImage128);
//                    _yyyy.setValue(bard);
//                    
//                    
//                    _idCodeBarRear2.setVisible(true);
//                    _yyyy.setVisible(true);
                            codeBarSoldiv.setVisible(true);

                            DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                            for (final BandejaDetalle nnns : DetSolModelList) {
                                System.out.println("LALALAL: " + nnns.getIdOperacion());

                                //generateCodeOper(nnns.getnOperacion());
                                // nnns.setBufferedImage128(generateCodeOper(nnns.getnOperacion()));
                                _conListDOc = new MvcConfig().documentosJDBC();
                                _listDOc = _conListDOc.listDocumento(nnns.getIdOperacion());
                                _listModelDOc = new ListModelList<Documento>(_listDOc);

                                for (final Documento dco : _listModelDOc) {
                                    System.out.println("Documento GLOSA : " + dco.getTipoDocumentoGlosa());
                                    //nnns.setDv_estadoOpe(dco.getTipoDocumentoGlosa());
                                    nnns.setTipoDoc(dco.getTipoDocumentoGlosa());
                                }
                            }

                            grid_DetSol.setModel(DetSolModelList);

                            btnCerrarAllTerm.setVisible(true);
                            id_printCodSol.setVisible(true);
                            id_addDocPanel.setVisible(false);
                            _divFridDoc.setVisible(false);
                            id_detalleSolicitud.setVisible(true);
                            coder.setVisible(false);

                            devolv_div.setVisible(false);

                            //_yyyy.setValue("");

                            /*Metodo boton cerrar solicitud*/
                            cierreSolok(id_solicitud);

                            Clients.scrollIntoView(id_detalleSolicitud);

                            if (rowSelected != null && rowSelected) {
                                row.setAttribute("Selected", false);
                                // row.setStyle("");
                                row.setSclass("");
                                id_detalleSolicitud.setVisible(false);
                                id_addDocPanel.setVisible(false);

                                codeBarSoldiv.setVisible(false);
                                btnCerrarAllTerm.setVisible(false);
                                id_printCodSol.setVisible(false);
                                btnCerrarAll.setVisible(false);

                                onOpenFal.setOpen(false);
                                //row.setc
                            } else {
                                for (Component rownn : grid.getRows().getChildren()) {
                                    rownn.getClass();
                                    Row nnn = (Row) rownn;
                                    nnn.setAttribute("Selected", false);
                                    nnn.setSclass("");

                                }
                                row.setAttribute("Selected", true);
                                // row.setStyle("background-color: #CCCCCC !important");   // inline style
                                row.setSclass("z-row-background-color-on-select");         // sclass
                            }

                        }
                    });
                }
            }
        });

        /*Se genera evento onPaging para cargar eventos click al paginar y cambiar de pagina del grid*/
        grid.addEventListener("onPaging", new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                PagingEvent evt = (PagingEvent) event;
                CargaEventos();
            }
        });
        }catch(Exception ex){
            System.out.println("Exception Carga Inicial Solicitudes BandBojRecController --> " + ex);
        }
        int cuantos = grid.getRows().getChildren().size();

        glosaNom2.setFocus(true);

    }

    public void irSort(int id_label) throws SQLException {

        System.out.println("ID_LABEL --> " + id_label);

        switch (id_label) {
            case 1:
                CargaPagSort(1);
                break;
            case 2:
                CargaPagSort(2);
                break;
            case 3:
                CargaPagSort(3);
                break;
            case 4:
                CargaPagSort(4);
                break;
            case 5:
                CargaPagSort(5);
                break;
            /*else if(id_label==7){
        CargaPagSort(7);
        }*/
            case 6:
                CargaPagSort(6);
                break;
            case 8:
                CargaPagSort(8);
                break;
            default:
                break;
        }

    }

    /*Metodo Carga BandejaNuevaPadre Inicial*/
    public ListModel<BandejaCustodia> getCustodia() {

        try {
            bandejacustodiaPad = new ListModelList<BandejaCustodia>(conexSol.getSolNuev_Pend(UsuarioCustodia));
            //gridPadre.setModel(bandejacustodiaPad);
            List<BandejaCustodia> bandeja = conexSol.getSolNuev_Pend(UsuarioCustodia);
            List<BandejaCustodia> bandeja2 = bandeja;
            int num_sol;

            for (BandejaCustodia b : bandeja2) {

                num_sol = b.getNumsol();
                System.out.println("**BandejPadre " + num_sol);

                bandejacustodiaPad = new ListModelList<BandejaCustodia>(conexSol.getSolNuev_Pad(UsuarioCustodia, num_sol));
            }

        } catch (SQLException ex) {
            Logger.getLogger(BandCustEntController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return new ListModelList<BandejaCustodia>(bandejacustodiaPad);
    }

    public void buscarOper() throws SQLException {

        String glosanom = glosaNom.getValue();
        System.out.println("glosanom: " + glosanom);

        conexDetSol = new MvcConfig().detSolicitudJDBC();

        if (glosanom == "" || glosanom.isEmpty()) {
            DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_solicitud));
            grid_DetSol.setModel(DetSolModelList);
        } else {
            this.listDet = conexDetSol.listDetBuscaOperSolicitud(glosanom, id_solicitud);
            DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
            grid_DetSol.setModel(DetSolModelList);
        }

    }

    public void irDetalle(int id_det) throws SQLException {

        System.out.println("IR DETALLE --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }

    public void irDetalleString(String id_det) throws SQLException {

        System.out.println("IR irDetalleString --> " + id_det);
        //gridPadre.setModel(bandejacustodiaPad);
    }

    public void irReprocesarBoj(int idDetsol, String nOper) throws SQLException {

        System.out.println("IR irReprocesarBoj --> " + idDetsol);
        //gridPadre.setModel(bandejacustodiaPad);

        boolean resp = false;
        detSeleccion = new BandejaDetalle();
        // System.out.println("id_Detsols--> : " +idDetsol +" "+ "nOper--> : "+nOper +" "+ "idSol--> : "+id_Sol);

//
        try {
            System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
            //Se deja como pendiente la operacion

            // resp = conexSol.cambioEstadoSolDet(idDetsol, "RECP", "Recepcionada");
            resp = conexSol.cambioEstadoSolDet(idDetsol, "C", "Completa");

            if (resp) {
                Messagebox.show("Operacion Reprocesada Correctamente ", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        }
                    }
                });

                activePage = grid_DetSol.getPaginal().getActivePage();
                //grid_DetSol.setActivePage(activePage);
                System.out.println("-->*#ACTIVE PAGE " + activePage);
                //updateDetalleGrid(detSeleccion.getDi_idSolicitud());
                //updateDetalle(id_solicitud);
                updateDetalleGrid(id_solicitud);
                id_addDevPanel.setVisible(false);
                //updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);
                //listCountProdTipoDoc();

                txtObservaciones.setValue(null);
////
////                    //Envia a cierrre de Solicitud
////                    String[] EvaluaCierre = validaCantOperaciones(id_solicitud);
////
////                    if ("Si".equals(EvaluaCierre[0])) {
////                        cierraSolicitud(EvaluaCierre);
////                    }
////                    solConPendientes = 0;
////                    contSolCompletada = 0;
////                    cantOpeComplet2 = 0;
////                    cantOpePend2 = 0;
////                    SolCompletada2 = "";
////                    SolCompletada2 = EvaluaCierre[0];
////                    cantOpeComplet2 = Integer.parseInt(EvaluaCierre[1]);
////                    cantOpePend2 = Integer.parseInt(EvaluaCierre[2]);
//
//                } else {
//                    Messagebox.show("Problemas al Mover el archivo el Documento. ", null, Messagebox.OK, Messagebox.ERROR,
//                            new org.zkoss.zk.ui.event.EventListener() {
//                        public void onEvent(Event e) {
//                            if (Messagebox.ON_OK.equals(e.getName())) {
//                                //OK is clicked
//                                Executions.sendRedirect("/sisboj/index");
//                            }
//                        }
//                    }
//                    );
//
            }

        } catch (SQLException e) {
            e.getMessage();
        }

    }

    public void irRecepcion(final int id_Detsol, final String nOper) throws SQLException {

        System.out.println("IR DETALLE --> " + id_solicitud + " " + id_Detsol + " " + nOper);
        Messagebox.show("Desea Recepcionar esta Operacion? ", "Recepcion de Operaciones", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    System.out.println("Si");
                    setCambioEstadoBojDet(id_Detsol, nOper, id_solicitud);

                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                    System.out.println("No");

                }
            }
        }
        );
        //gridPadre.setModel(bandejacustodiaPad);
    }

    public void setCambioEstadoBojDet(int id_Detsols, String nOper, int id_Sol) throws SQLException, WriterException, ParseException {
        boolean resp = false;
        detSeleccion = new BandejaDetalle();
        System.out.println("id_Detsols--> : " + id_Detsols + " " + "nOper--> : " + nOper + " " + "idSol--> : " + id_Sol);

//
        try {
            System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
            //Se deja como pendiente la operacion

            resp = conexSol.cambioEstadoSolDet(id_Detsols, "RECP", "Recepcionada");

            if (resp) {
                Messagebox.show("Operacion Recepcionada Correctamente ", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                        }
                    }
                });

                activePage = grid_DetSol.getPaginal().getActivePage();
                //grid_DetSol.setActivePage(activePage);
                System.out.println("-->*#ACTIVE PAGE " + activePage);
                //updateDetalleGrid(detSeleccion.getDi_idSolicitud());
                //updateDetalle(id_solicitud);
                updateDetalleGrid(id_solicitud);
                id_addDevPanel.setVisible(false);
                //updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);
                //listCountProdTipoDoc();

                txtObservaciones.setValue(null);

                //Evalua cierre de lote operaciones
                //Envia a cierrre de Solicitud
                String[] EvaluaCierre = validaCantOperaciones(id_solicitud);

                if ("Si".equals(EvaluaCierre[0])) {
                    cierraSolicitudBoj(EvaluaCierre);
                    System.out.println("Solicitud Evalua cierre si");
                    for (String str : EvaluaCierre) {
                        System.out.print("EK --> " + str);
                    }

                } else {
                    System.out.println("Solicitud evalua cierre no");
                }

                contSolRecepcionada2 = 0;
                contSolReparo2 = 0;
                cantOpDev = 0;
                cantOpRep = 0;
                cantTotalOp = 0;

                //SolCompletada2 = "";
                //solConPendientes = 0;
                // contSolCompletada = 0;
                contSolRecepcionada = 0;
                contSolReparo = 0;

//                    SolCompletada2 = EvaluaCierre[0];
//                    cantOpeComplet2 = Integer.parseInt(EvaluaCierre[1]);
//                    cantOpePend2 = Integer.parseInt(EvaluaCierre[2]);
            }// if resp

        } catch (SQLException e) {
            e.getMessage();
        }

    }

    public void CargaPagSort(int label_cont) throws SQLException {
        for (Component column : grid.getColumns().getChildren()) {

            column.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Column row = (Column) arg0.getTarget();

                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    if (rowSelected != null && rowSelected) {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", true);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", false);
                        row.setSclass("z-row-background-color-on-select-sort");

                    } else {
                        for (Component rownn : grid.getColumns().getChildren()) {
                            rownn.getClass();
                            Column nnn = (Column) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");
                        }
                        row.setAttribute("Selected", true);
                        row.setSclass("z-row-background-color-on-select-sort");

                    }

                }
            });
        }

        this.listSol = conexSol.getSolNuev_EnvSort(UsuarioCustodia, label_cont);
        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);

        grid.setModel(bandejacustodia);
        grid.renderAll();

        CargaEventos();

    }

    private void CargaEventos() throws SQLException {
        for (Component row : grid.getRows().getChildren()) {
            row.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

                public void onEvent(Event arg0) throws Exception {
                    Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) arg0.getTarget();
                    BandejaCustodia banc = (BandejaCustodia) row3.getValue();

                    String value = banc.getProducto();
                    Component c = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    //System.out.println("BANC NU SOL ----->> " + banc.getNumsol());
                    id_solicitud = banc.getNumsol();
                    nameProductCode = banc.getProducto();
                    fecSolicCode = banc.getFechaRegistro();
                    cantOpera = banc.getNumDctos();

                    String bard = banc.getCodeBar();

                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);

                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
                    _idCodeBarRear23.setContent(bufferedImage128);
                    _yyyyy.setValue(bard);

                    _idCodeBarRear23.setVisible(true);
                    _yyyyy.setVisible(true);

//                    String bard = banc.getCodeBar();
//                    
//                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);
//
//                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
//                    _idCodeBarRear2.setContent(bufferedImage128);
//                    _yyyy.setValue(bard);
//                    
//                    
//                    _idCodeBarRear2.setVisible(true);
//                    _yyyy.setVisible(true);
//                    String bard = banc.getCodeBar();
//                    
//                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);
//
//                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
//                    _idCodeBarRear2.setContent(bufferedImage128);
//                    _yyyy.setValue(bard);
//                    
//                    
//                    _idCodeBarRear2.setVisible(true);
//                    _yyyy.setVisible(true);
                    codeBarSoldiv.setVisible(true);

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    for (final BandejaDetalle nnns : DetSolModelList) {
                        System.out.println("LALALAL: " + nnns.getIdOperacion());

                        //generateCodeOper(nnns.getnOperacion());
                        // nnns.setBufferedImage128(generateCodeOper(nnns.getnOperacion()));
                        _conListDOc = new MvcConfig().documentosJDBC();
                        _listDOc = _conListDOc.listDocumento(nnns.getIdOperacion());
                        _listModelDOc = new ListModelList<Documento>(_listDOc);

                        for (final Documento dco : _listModelDOc) {
                            System.out.println("Documento GLOSA : " + dco.getTipoDocumentoGlosa());
                            //nnns.setDv_estadoOpe(dco.getTipoDocumentoGlosa());
                            nnns.setTipoDoc(dco.getTipoDocumentoGlosa());
                        }
                    }

                    grid_DetSol.setModel(DetSolModelList);

                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                    coder.setVisible(false);

                    devolv_div.setVisible(false);

                    //_yyyy.setValue("");

                    /*Metodo boton cerrar solicitud*/
                    cierreSolok(id_solicitud);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);

                        codeBarSoldiv.setVisible(false);

                        btnCerrarAll.setVisible(false);

                        onOpenFal.setOpen(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");

                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }
            });
        }
    }

    @Listen("onClick = #btnCerrarAll")
    public void cerrarAllSol() throws SQLException {

        Messagebox.show("Operacion: " + id_solicitud + " sera Cerrada y Enviada a Bandeja Terminadas.Desea Continuar?", "Cerrar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    System.out.println("Entra a cerrar solicitud");
                    /*Metodo cerrar solicitud*/
                    System.out.println("Cierra Solicitud n�: " + id_solicitud);
                    String SolCompletada = "";

                    /**/
                    List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
                    lbd = getListDet(id_solicitud);

                    solConPendientes = 0;
                    contSolCompletada = 0;
                    for (int i = 0; i < lbd.size(); i++) {
                        if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                            // SolCompletada = "No";
                        } else {
                            if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                                solConPendientes = solConPendientes + 1;
                            }
                            if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                                // SolCompletada = "Si";
                                contSolCompletada = contSolCompletada + 1;
                            }
                        }

                    }

                    if ((solConPendientes + contSolCompletada) == lbd.size()) {
                        SolCompletada = "Si";

                    } else {
                        SolCompletada = "No";
                    }

                    String[] respStr = new String[3];
                    respStr = new String[3];
                    respStr[0] = SolCompletada;
                    respStr[1] = String.valueOf(contSolCompletada);
                    respStr[2] = String.valueOf(solConPendientes);

//                    cierraSolicitud(respStr);
                    // updateDetalleGrid(id_solicitud);
                    solConPendientes = 0;
                    contSolCompletada = 0;
                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                    System.out.println("Cancela");
                }
            }
        });

    }

    public void cierreSolok(int id_sol) throws SQLException {

        int countPen = 0;
        List<Estado> codEstadoPencus;
        List<Estado> codEstadoCdscus;

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

        codEstadoPencus = estJDBC.getIdEstadoByCodigo("PENCUS");
        codEstadoCdscus = estJDBC.getIdEstadoByCodigo("CDSCUS");

        nnCierra = conexDetSol.listDetSolicitud(id_sol);
        int solConPendientess = 0;
        int contSolCompletadass = 0;

        for (final BandejaDetalle nnn : nnCierra) {
            int coda = nnn.getDi_fk_idestado();

//            if (coda == codEstadoPencus.get(0).getDi_IdEstado() || coda == codEstadoCdscus.get(0).getDi_IdEstado()) {
//               
//                countPen++;
//                
//                
//            }
            /**/
            if (coda == codEstadoPencus.get(0).getDi_IdEstado()) {
                solConPendientess = solConPendientess + 1;
            }
            if (coda == codEstadoCdscus.get(0).getDi_IdEstado()) {
                // SolCompletada = "Si";
                contSolCompletadass = contSolCompletadass + 1;
            }

        }

        System.out.println("cierreSolok contSolCompletadass : " + contSolCompletadass);
        System.out.println("cierreSolok solConPendientess : " + solConPendientess);
        System.out.println("cierreSolok cantOpera :" + cantOpera);

        if ((solConPendientess + contSolCompletadass) == cantOpera) {

            if (solConPendientess == cantOpera) {
                btnCerrarAll.setVisible(false);
            } else {
                btnCerrarAll.setVisible(false);//*
            }

        } else {
            btnCerrarAll.setVisible(false);
        }

//        if (countPen == cantOpera) {
//            btnCerrarAll.setVisible(true);
//        } else {
//            btnCerrarAll.setVisible(false);
//        }
    }

    @Listen("onClick = #btnDevoSol")
    public void btnDevoSol() throws SQLException, WriterException, ParseException {

        int solDevo = id_solicitud;
        //boolean insert=true;
        //cambioEstado = true;
        boolean resp = false;
        String motivoDev = "Prescrito";
//        motDevGen.setValue(motivoDev);
//        motDevGen.setText(motivoDev);
        String txtObsDev = txt_GobsSol.getText();

        if (txtObsDev == "") {
            System.out.println("txt_GobsSol vacio");
            Messagebox.show("Debe Ingresar Observacion para devolver", "", Messagebox.OK, null);
        } else {
            System.out.println("txt_GobsSol lleno");

            /**/
            //Cambio de Estado de Solicitud
            //Insert tabla tracking tb_Sys_Devolucion
            //cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(solDevo, "CerradoNuevaRep");
            //insert = devolucionJDBC.updateObsDevo(txtObsDev,solDevo); 
            /*CICLO FOR COMIENZA        */
            DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(solDevo));

            for (final BandejaDetalle nnns : DetSolModelList) {
                System.out.println("Numero de Operaciones: " + nnns.getIdOperacion());

                try {
                    System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
                    //Se deja como devuelta

                    resp = conexSol.cambioEstadoSolDet(idDetSolGlobal, "DEV", txtObservaciones.getValue());

                    boolean respInsertRep = false;
                    if (resp) {

                        List<MotivoDevolucionEnt> ltd = new ArrayList<MotivoDevolucionEnt>();
                        ltd = motivoDevolucionJDBC.getMotivoDevolucionPorNom(motivoDev);
                        int id_tipoDev = ltd.get(0).getDi_fk_TipoDev();
                        int idMotivoDev = ltd.get(0).getDi_IdMotivoDev();

                        DevolucionEnt dev = new DevolucionEnt();
                        dev.setDi_fk_IdOper(idDetSolGlobal);
                        dev.setDi_fk_IdTipoDev(id_tipoDev);
                        dev.setDi_fk_IdMotivoDev(idMotivoDev);
                        dev.setDi_fk_IdSolOrigen(id_solicitud);
                        dev.setDi_fk_IdSolActual(0);
                        dev.setDi_fk_IdEstadoDev(1);
                        //dev.setDdt_FechaActualiza();
                        dev.setDi_fk_IdUbiOrigen(8);
                        dev.setDi_fk_IdUbiDestino(2);
                        dev.setDv_ObsOperacion("");
                        dev.setDv_ObsSolicitud(txtObsDev);

                        respInsertRep = devolucionJDBC.insert(dev);

                        System.out.println("Reparo Insertado ? => " + respInsertRep);

                        cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "CerradoNuevaDev");
                    }

                    if (resp) {

                        Messagebox.show("Operacion Devuelta Correctamente ", "", Messagebox.OK, null,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                }
                            }
                        });

                        activePage = grid_DetSol.getPaginal().getActivePage();
                        System.out.println("-->*#ACTIVE PAGE " + activePage);
                        updateDetalleGrid(id_solicitud);
                        id_addDevPanel.setVisible(false);

                        txtObservaciones.setValue(null);

                    } else {
                        Messagebox.show("Error al devolver Solicitd N� " + solDevo, "", Messagebox.OK, null);
                    }

                } catch (SQLException e) {
                    e.getMessage();
                }

            }
            /*FIN CICLO FOR*/

 /**/
            txt_GobsSol.setText("");
            obsSolRep.setVisible(false);

        }

    }

    @Listen("onClick = #btnCerrarAllTerm")
    public void cerrarAllOperTerm() throws SQLException {

        System.out.println("btnCerrarAllTerm");

        obsSolRep.setVisible(true);

//        List<BandejaDetalle> lbdINi = new ArrayList<BandejaDetalle>();
//        lbdINi = getListDet(id_solicitud);
//        int cantTerminadasIni = 0;
//        for (BandejaDetalle dsolINi : lbdINi) {
//            if (dsolINi.getDv_codestado().equals("CDSCUS")) {
//                cantTerminadasIni = cantTerminadasIni + 1;
//            }
//        }
//
//        if (cantTerminadasIni > 0) {
//            Messagebox.show("Se cerraran las operaciones con estado terminadas para la solicitud:  " + id_solicitud + ",desea Cerrada y Enviada a Bandeja Terminadas?", "Cerrar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
//                    new org.zkoss.zk.ui.event.EventListener() {
//                public void onEvent(Event e) throws SQLException, WriterException, ParseException {
//                    if (Messagebox.ON_OK.equals(e.getName())) {
//                        System.out.println("Entra a cerrar operaciones terminadas - Solicitud n�: " + id_solicitud);
//                        List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
//                        lbd = getListDet(id_solicitud);
//                        int cantTerminadas = 0;
//                        for (BandejaDetalle dsol : lbd) {
//                            if (dsol.getDv_codestado().equals("CDSCUS")) {
//                                cantTerminadas = cantTerminadas + 1;
//                            }
//                        }
//
//                        boolean updCodeBarSol = false;
//                        boolean updCantidadDocs = false;
//                        //Cerrar Solcicitud(Cambiar estado a la solicitud)
//                        //  cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerradoConP");
//                        //Actualizar cantidad de documentos (Resta documentos Pendientes)
//                        boolean cambEstOp = false;
//                        //Cambia de estado ooperaciones en tabla tb_Sys_DetSolicitud(Completa)                                    
//                        // cambEstOp = conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);
//
//                        // Se crea hijo de esta para las operaciones con estado Pendiente y se obtiene hijo creado
//                        List<Estado> ListEstado = new ArrayList();
//                        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
//
//                        ListEstado = estJDBC.getIdEstadoByCodigo("CSCUS");
//
//                        //se obtiene id de Ubicacion                            
//                        lUbi = ubicacionJDBC.getUbicacion("dv_DetUbi", "UCC");
//
//                        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//                        Date dat = new Date();
//                        System.out.println("df.format(dat) => " + df.format(dat).toString());
//                        //Se obtiene la solicitud padre
//                        Solicitud sol = new Solicitud();
//                        sol = conexSol.getSolicitud(id_solicitud);
//
//                        newHijosol = new Solicitud();
//                        // newHijosol.setDi_IdSolicitud(id_solicitud);
//                        newHijosol.setDi_fk_IdUbi(lUbi.get(0).getDi_IdUbicacion());
//                        newHijosol.setDi_fk_IdHito(sol.getDi_fk_IdHito());
//                        newHijosol.setDi_fk_IdEstado(ListEstado.get(0).getDi_IdEstado());
//                        newHijosol.setDdt_FechaCreacion(df.format(dat));
//                        newHijosol.setDi_cantidadDocs(cantTerminadas);
//                        //newHijosol.setDi_SolPadre(sol.getDi_IdSolicitud());
//
//                        crearHijo = conexSol.InsertHijoSol(newHijosol);
//
//                        //  idSolHijo = crearHijo.getDi_IdSolicitud();
//                        //  idSolHijo = crearHijo.getDi_IdSolicitud();
//                        System.out.println("idSolHijo ======>>> " + crearHijo.getDi_IdSolicitud());
//
//                        System.out.println("idSolHijo ======>>> " + crearHijo.getDi_IdSolicitud());
//
//                        //Si el hijo se creo correctamente, se vicula el padre con el hijo
//                        if (crearHijo != null) {
//
//                            updCantidadDocs = conexSol.updateSolicitud(id_solicitud, "di_CantDctosSolicitados", (lbd.size() - cantTerminadas));
//
//                            // updPadre = conexSol.updateSolicitud(id_solicitud, "di_SolHijo", crearHijo.getDi_IdSolicitud());
//                            JerarquiaSolEnt jerEnt = new JerarquiaSolEnt();
//                            jerEnt.setDi_fk_IdSolicitudPadre(sol.getDi_IdSolicitud());
//                            jerEnt.setDi_fk_IdSolicitudHijo(crearHijo.getDi_IdSolicitud());
//
//                            updPadre = jerarquiaSolJDBC.insertJerarquiaSol(jerEnt);
//                            System.out.println("updPadre => " + updPadre);
//                        }
//
//                        //Una vez actualizado el padre se vinculan las operaciones a la solic. hijo creada.
//                        if (updPadre) {
//                            System.out.println("crearHijo.getDi_IdSolicitud() => " + crearHijo.getDi_IdSolicitud());
//                            updOperNvaSol = conexDetSol.updateIdSolOperacion(id_solicitud, "di_fk_IdSolicitud", crearHijo.getDi_IdSolicitud(), "COMPLETADAS");
//                        }
//
//                        if (updOperNvaSol) {
//
//                            String codbar = generaCodigoBarra(crearHijo.getDi_IdSolicitud());
//
//                            updCodeBarSol = conexSol.updateSolicitudCodeBar(crearHijo.getDi_IdSolicitud(), "dv_CodeBar", codbar);
//
//                            System.out.println("Solicitud Completada => Generar codigo de barra para solicitud: " + crearHijo.getDi_IdSolicitud());
//                        }
//
//                        actualizaBandejas();
//
//                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
//                        System.out.println("Cancela");
//                    }
//                }
//            });
//
//        } else {
//            Messagebox.show("No Hay Operaciones Completas Para La Solicitud: " + id_solicitud);
//        }
//
//        setDocumentos.setVisible(false);
//
//        _idCodeBarRear2.setVisible(false);
//        _yyyy.setValue("");
    }

    public void MatchDoc(int id_sol, final int Oper, int numDoc, int docEsp) throws SQLException {

        final List<BandejaDetalle> bandeja = getListDet(id_solicitud);

        boolean resp = false;
        List<BandejaDetalle> bandeja2 = bandeja;

        int numDocs = 0;
        int numDocsEsp = 0;
        String estProc = "Procesada";

        for (BandejaDetalle b : bandeja2) {
            if (Oper == b.getIdOperacion()) {
                numDocs = b.getNumero_docs();
                numDocsEsp = b.getNumero_docs_esperado();

                if (numDocs == numDocsEsp) {
                    id_addDocNew.setVisible(false);
                } else {
                    id_addDocNew.setVisible(true);
                }

            }

        }
        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(Oper);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

//        _conListDOc = new MvcConfig().documentosJDBC();
//        this._listDOc = _conListDOc.listDocumento(Oper);
//        _listModelDOc = new ListModelList<Documento>(_listDOc);
//
//        grid_Dctos.setModel(_listModelDOc);
//        _divFridDoc.setVisible(true);
        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

    }

    public void updateGridDoc(int id_operacion, int countNumDoc, int cantDocsEsperados) throws SQLException {

        _conListDOc = new MvcConfig().documentosJDBC();
        this._listDOc = _conListDOc.listDocumento(id_operacion);
        _listModelDOc = new ListModelList<Documento>(_listDOc);

        grid_Dctos.setModel(_listModelDOc);
        _divFridDoc.setVisible(true);

        id_addDocNew.setVisible(countNumDoc >= cantDocsEsperados ? false : true); // se debe corregir al nuevo proceso

    }

    public void updateDetalleGrid(int id_sol) throws SQLException {

        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);

        grid_DetSol.setModel(DetSolModelList);

        grid_DetSol.setVisible(false);
        grid_DetSol.setVisible(true);

    }

    public void updateDetalle(int id_sol) throws SQLException {

        int id_soli = id_sol;
        DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(id_soli));

        grid_DetSol.setModel(DetSolModelList);
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
        id_addDevPanel.setVisible(false);
        id_detalleSolicitud.setVisible(true);

        Clients.scrollIntoView(id_detalleSolicitud);

    }

    /**
     * Se utiliza para cargar el detalle y trabajarlo con el hijo de esta
     * ventana
     *
     * @param id_sol
     */
    public void CargaBandejaDetalle(int id_sol, int numDocEsperados) {

        id_addDevPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDocPanel.setVisible(true);
        txt_RutDcto.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto.setValue(detSeleccion.getnOperacion());
        txt_TioAux.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public void CargaBandejaDetallePend(int id_sol, int numDocEsperados) {

        _divFridDoc.setVisible(false);
        id_addDocPanel.setVisible(false);
        detSeleccion = new BandejaDetalle();
        id_addDevPanel.setVisible(false);

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == id_sol) {
                detSeleccion = banddettt;
            }
        }

        id_addDevPanel.setVisible(true);
        txt_RutDcto2.setValue(detSeleccion.getRutCompleto());
        txt_NombreDcto2.setValue(detSeleccion.getNombrecomp());
        txt_OperDcto2.setValue(detSeleccion.getnOperacion());
        txt_TioAux2.setValue(detSeleccion.getDv_IdTipOpe());
        txt_docsEsperados2.setValue(String.valueOf(numDocEsperados));

        List list3;
        ListModelList lm2 = new ListModelList();
        ListModelList lm3;
        // self.
        _listtipoDocto = _tipoDoc.getListTipoDocumento("");
        _tipoDoctoList = new ListModelList<TipoDocumento>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        list3 = new ArrayList();

        for (final Estado est : _estadoDocto) {
            list3.add(est.getDv_NomEstado());
        }

        List<ProdTipoDoc> dp = getListTipDoc();
        for (ProdTipoDoc lmx : dp) {
            lm2.add(lmx.getDv_NomTipDcto());
        }

        lm3 = new ListModelList(list3);

        cmb_EstDcto.setValue("");
        cmb_EstDcto.setPlaceholder("Seleccione Estado.");
        cmb_EstDcto.setModel(lm3);

        cmb_TipoDocumento.setValue("");
        cmb_TipoDocumento.setPlaceholder("Seleccione Tipo dcto.");
        cmb_TipoDocumento.setModel(lm2);

        txt_GlosaDcto2.setValue("");
        id_chechApoderado.setChecked(false);

    }

    public void setValuesDocsNew(int id_sol, int countNumDoc, int cantDocEsperados) throws SQLException {

        CargaBandejaDetalle(id_sol, cantDocEsperados);

//        id_addDocNew.setVisible(countNumDoc >= 2 ? false : true);
//        id_addDocumentos.setVisible(countNumDoc >= 2 ? false : true);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc, cantDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);

        oPer = ope.getDi_IdOperacion();
        nDoc = countNumDoc;
        docEs = cantDocEsperados;

        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setValuesDevolucionOpe(int id_sol, int countNumDoc, int cantDocEsperados) throws SQLException {

        CargaBandejaDetallePend(id_sol, cantDocEsperados);
        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());
            updateGridDoc(ope.getDi_IdOperacion(), countNumDoc, cantDocEsperados);
        }

        Clients.scrollIntoView(id_addDevPanel);
    }

    public void setValuesDocsComplete(int id_sol, int NumDocEsperados, int NumDocumentos) throws SQLException {

        CargaBandejaDetalle(id_sol, NumDocEsperados);

//        id_addDocumentos.setVisible(false);
        id_addDocNew.setVisible(false);

        if (detSeleccion != null && !detSeleccion.getnOperacion().isEmpty()) {
            ope = _op.getOperaciones(detSeleccion.getnOperacion());

            System.out.println("ope.getDi_IdOperacion => " + ope.getDi_IdOperacion());
            System.out.println("detSeleccion.getNumero_docs() => " + ope.getDi_IdOperacion());
            System.out.println("NumDocEsperados => " + NumDocEsperados);

            updateGridDoc(ope.getDi_IdOperacion(), detSeleccion.getNumero_docs(), NumDocEsperados);
        }

        Clients.scrollIntoView(id_addDocPanel);
        // Messagebox.show("BandejaDetalle.rutcompleto["+detSeleccion.getRutCompleto()+"]txt_RutDcto["+txt_RutDcto.getValue()+"]txt_NombreDcto["+txt_NombreDcto.getValue()+"]txt_OperDcto["+txt_OperDcto.getValue()+"]");
    }

    public void setButtonVisible() {

//        id_addDocumentos.setVisible(true);
        id_addDocNew.setVisible(true);

    }

    public void deleteDocs(final int id_operacion) throws SQLException {
        final List<BandejaDetalle> bandeja = getListDet(id_solicitud);
//        idOpe2=id_operacion;
        Messagebox.show("�Desea eliminar documento? ", "Eliminar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                new org.zkoss.zk.ui.event.EventListener() {
            public void onEvent(Event e) throws SQLException {
                if (Messagebox.ON_OK.equals(e.getName())) {
                    boolean eliminado = false;
                    List<BandejaDetalle> bandeja2 = bandeja;
                    eliminado = _conListDOc.eliminarDocumento(id_operacion);
                    if (eliminado) {
                        Messagebox.show("Documento eliminado correctamente", "", Messagebox.OK, Messagebox.INFORMATION);
                        refrescaGridDetalle();
                        btnCerrarAll.setVisible(false);
                    }

                    int numDocs = 0;
                    int numDocsEsp = 0;
                    int idDetalleOpe = 0;

                    for (BandejaDetalle b : bandeja2) {
                        if (id_operacion == b.getIdOperacion()) {
                            numDocs = b.getNumero_docs();
                            numDocsEsp = b.getNumero_docs_esperado();
                            idDetalleOpe = b.getIdDetSol();
                        }
                    }

                    if (eliminado) {
                        conexSol.cambioEstadoSolDet(idDetalleOpe, "R", "Incompleta");
                    }
//                    setValuesDocsNew(id_solicitud, numDocs, numDocsEsp);
                    grid_DetSol.setModel(DetSolModelList);
                    updateGridDoc(id_operacion, numDocs, numDocsEsp);
                    getListDet(id_solicitud);
                    coder.setVisible(false);
                    _idCodeBarRear2.setVisible(false);
                    _yyyy.setValue("");
                    id_codebar.setValue("");
                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {

                }
            }
        }
        );

    }

    public List<BandejaDetalle> getListDet(int id_sol) throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_sol);
        return listDet;
    }

    public void refrescaGridDetalle() throws SQLException {
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_solicitud);
        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
        grid_DetSol.setModel(DetSolModelList);
//               detSeleccion 
    }

    public void irBanPendientes(int id_sol, String numOpe) throws SQLException {
        Map<String, Object> arguments = new HashMap<String, Object>();

        System.out.println("BandCustEntController.irBanPendientes id_solicitud => " + id_solicitud);
        System.out.println("BandCustEntController.irBanPendientes numOpe => " + numOpe);

//        detSeleccion.setDi_idSolicitud(id_solicitud);
//        detSeleccion.setnOperacion(numOpe);
        arguments.put("idSolicitud", id_solicitud);
//        arguments.put("id_solicitud", id_solicitud);
        String template = "custodia/BandejaPendientes.zul?idSolicitud=" + String.valueOf(id_solicitud);
        //String template = "custodia/BandejaPendientes.zul";
        //  System.out.println("Prueba fellow{" + capturawin.getParent().getParent().getFellows().toString() + "}");

        Include inc = ((Include) capturawin.getParent().getParent().getFellow("pageref"));

        try {
            inc.setSrc(template);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick = #id_procesarLotesss")
    public void readCode() {

        System.out.println("PROCESA POR LOTES");
        Map<String, Object> arguments = new HashMap<String, Object>();

        ListModelList<BandejaDetalle> DetSolModelList2;
        DetSolModelList2 = new ListModelList<BandejaDetalle>(listDet);

        System.out.println("ID SOL A MANDAR " + id_solicitud);
        // arguments.put("orderItems", DetSolModelList2);
        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("txt_RutDcto", txt_RutDcto.getValue());
        arguments.put("txt_NombreDcto", txt_NombreDcto.getValue());
        arguments.put("txt_OperDcto", txt_OperDcto.getValue());
        arguments.put("rut_Cli", 15.014544);
        arguments.put("id_solicitud", id_solicitud);

        String template = "custodia/BarCodeLotes.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonGuardarDoc");
        //final Textbox nn = (Textbox) window.getFellow("txt_hiddenIdOper");
        //final Textbox CodOper = (Textbox) window.getFellow("txt_OperDcto");
        //final Textbox AddNewValue = (Textbox) window.getFellow("txt_hiddenIdExito");

//        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
//            @Override
//            public void onEvent(Event event) throws SQLException {
//
//                // String idoper=
//                window.detach();
//                int NumeroDocs = 0;
//                updateDetalleGrid(id_solicitud);
//                int numdocsagregados = conexDetSol.NumeroDocumentosAgregadosSolicitud(id_solicitud);
//                // ojo aca el valor de getNumeroDocumentosSolicitud se agrego de forma estatica,se debe arreglar
//                int numerototaldedocs = conexSol.getNumeroDocumentosSolicitud(id_solicitud);
//
//                if (numdocsagregados == numerototaldedocs) {
//
//                    if (conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "cerrado")) {
//                        Messagebox.show("Se Cerro correctamente la Solicitud ", "Terminar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
//                                new org.zkoss.zk.ui.event.EventListener() {
//                            public void onEvent(Event e) {
//                                if (Messagebox.ON_OK.equals(e.getName())) {
//
//                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
//
//                                }
//                            }
//                        }
//                        );
//                    } else {
//                        Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
//                                new org.zkoss.zk.ui.event.EventListener() {
//                            public void onEvent(Event e) {
//                                if (Messagebox.ON_OK.equals(e.getName())) {
//                                    //OK is clicked
//                                    // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
//                                    Executions.sendRedirect("/sisboj/index");
//                                } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
//                                    //Cancel is clicked
//                                    // Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
//                                }
//                            }
//                        });
//
//                    }
//
//                }
//
//                //  window.print(buildParameters(window),planner);
//            }
//        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick = #id_procesarLotes")
    public void codereader() throws SQLException {

        int getIdEst = 275;
        //cantOpera
        int countPen = 0;

        if (coder.setVisible(true)) {

            coder.setVisible(false);
            _idCodeBarRear2.setVisible(false);
            _yyyy.setValue("");
            id_codebar.setValue("");
        } else if (coder.setVisible(false)) {
            coder.setVisible(true);
            coder.setFocus(true);

            /*CODIGO PROCESAR POR LOTES TODAS LAS OPERACIONES EN PENDIENTE*/
            List<Estado> codEstado;
            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPREC");
            nnn = conexDetSol.listDetSolicitud(id_solicitud);

            for (final BandejaDetalle nnn : nnn) {
                int coda = nnn.getDi_fk_idestado();

                if (coda == codEstado.get(0).getDi_IdEstado()) {
                    countPen++;
                }
            }
            System.out.println("CountPent: " + countPen);
            System.out.println("CantOpera: " + cantOpera);

            if (countPen == cantOpera) {

                Messagebox.show("Operaciones en Reparo, Desea Reprocesar Lote?", "ReProcesar lote", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            System.out.println("Entra a Reprocesar");
                            /*List<BandejaDetalle> nne;*/
                            for (final BandejaDetalle bdj : nnn) {
                                System.out.println("Cambiando Estado Operacion: " + bdj.getnOperacion() + " --> Estado actual: " + bdj.getDi_fk_idestado());
                                //conexDetSol.updateOpeSol(id_solicitud,"di_fk_IdEstado",141);
                                conexSol.cambioEstadoSolDet(bdj.getIdDetSol(), "R", "");
                            }
                            //conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);
                            updateDetalleGrid(id_solicitud);

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            System.out.println("Cancela");
                        }
                    }
                });

            }

        }/*Else If*/

    }

    public void codereaderFoundSol(final String Barcode) throws SQLException, WriterException {
        try {

            System.out.println("Entra a codereaderFoundSol: " + Barcode.trim().toUpperCase());
            //Barcode = Barcode.trim().toUpperCase();

            glosaNom2.setValue(Barcode.trim().toUpperCase());

            String string = Barcode.trim().toUpperCase();
            String[] parts = string.split("E");
            String part1 = parts[0];
            String part2 = parts[1];

            String[] parts2 = part1.split("SOL");
            String part3 = parts2[0];
            String idSolicitudDecode = parts2[1];

            System.out.println("idSolicitudDecode: " + idSolicitudDecode);
            idSolicitudDecodeFinal = Barcode.trim().toUpperCase();
            int idSolicitudDecodeint = Integer.parseInt(idSolicitudDecode);

            //SOL4162E201907251
            //SOL4157E201907257
            for (Component row : grid.getRows().getChildren()) {

                Row row2 = (Row) row;
                BandejaCustodia banc = (BandejaCustodia) row2.getValue();
                int idss = banc.getNumsol();

                if (idss == idSolicitudDecodeint) {

                    Boolean rowSelected = (Boolean) row2.getAttribute("Selected");

                    id_solicitud = banc.getNumsol();
                    nameProductCode = banc.getProducto();
                    fecSolicCode = banc.getFechaRegistro();
                    cantOpera = banc.getNumDctos();
                    String bard = banc.getCodeBar();
                    encodeCode128 = new Code128Writer().encode(bard, BarcodeFormat.CODE_128, 150, 80, null);
                    bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);
                    _idCodeBarRear23.setContent(bufferedImage128);
                    _yyyyy.setValue(bard);

                    _idCodeBarRear23.setVisible(true);
                    _yyyyy.setVisible(true);

                    codeBarSoldiv.setVisible(true);
                    btnCerrarAllTerm.setVisible(true);
                    id_printCodSol.setVisible(true);

                    DetSolModelList = new ListModelList<BandejaDetalle>(getListDet(banc.getNumsol()));

                    for (final BandejaDetalle nnns : DetSolModelList) {
                        System.out.println("LALALAL: " + nnns.getIdOperacion());

                        _conListDOc = new MvcConfig().documentosJDBC();
                        _listDOc = _conListDOc.listDocumento(nnns.getIdOperacion());
                        _listModelDOc = new ListModelList<Documento>(_listDOc);

                        for (final Documento dco : _listModelDOc) {
                            System.out.println("Documento GLOSA : " + dco.getTipoDocumentoGlosa());
                            nnns.setTipoDoc(dco.getTipoDocumentoGlosa());
                        }
                    }

                    grid_DetSol.setModel(DetSolModelList);

                    id_addDocPanel.setVisible(false);
                    _divFridDoc.setVisible(false);
                    id_detalleSolicitud.setVisible(true);
                    coder.setVisible(false);
                    devolv_div.setVisible(false);

                    if (codeBarSoldiv.isVisible()) {
                       // codeBarSoldiv.setVisible(false);
                    }

                    /*Metodo boton cerrar solicitud*/
                    cierreSolok(id_solicitud);

                    Clients.scrollIntoView(id_detalleSolicitud);

                    if (rowSelected != null && rowSelected) {
                        row.setAttribute("Selected", false);
                        // row.setStyle("");
                        row2.setSclass("");
                        id_detalleSolicitud.setVisible(false);
                        id_addDocPanel.setVisible(false);

                        btnCerrarAll.setVisible(false);
                        codeBarSoldiv.setVisible(false);

                        onOpenFal.setOpen(false);
                        //row.setc
                    } else {
                        for (Component rownn : grid.getRows().getChildren()) {
                            rownn.getClass();
                            Row nnn = (Row) rownn;
                            nnn.setAttribute("Selected", false);
                            nnn.setSclass("");

                        }
                        row.setAttribute("Selected", true);
                        // row.setStyle("background-color: #CCCCCC !important");   // inline style
                        row2.setSclass("z-row-background-color-on-select");         // sclass
                    }

                }

            }
            glosaNom2.setText("");
            glosaNom2.setValue("");
            glosaNom2.setFocus(true);

        } catch (Exception ex) {
            System.out.println("Error -> Get Pistoleo Solicitud: " + ex);
            Messagebox.show("Error al Pistolear Solicitud", "", Messagebox.OK, null);
        }

    }

    public void codereaderChange(final String Barcode) throws WriterException, SQLException, FormatException, ChecksumException, NotFoundException, IOException, ParseException {

        //Barcode = Barcode.toUpperCase();
        System.out.println("Barcode: " + Barcode.trim().toUpperCase());

        String devOrRep = "";

        try {
            String radioBut = sv2.getSelectedItem().toString();
            System.out.println("RADIOBUTTO " + sv2.getSelectedItem().getValue());
            System.out.println();

            if (rb_recep.isChecked() || rb_devol.isChecked()) {

                final int result = conexDetSol.getIdSolDet(Barcode.trim().toUpperCase());

                if (sv2.getSelectedItem().getValue().equals("1")) {

                    //irRecepcion(result,Barcode);
                    System.out.println("IR DETALLE --> " + id_solicitud + " " + result + " " + Barcode.trim().toUpperCase());
                    setCambioEstadoBojDet(result, Barcode.trim().toUpperCase(), id_solicitud);

                } else if (sv2.getSelectedItem().getValue().equals("2")) {
                    System.out.println("entra a devolucion");

                    devOrRep = sv3.getSelectedItem().getValue();

                    String cmb_motivo3Str = cmb_motivo_dev3.getValue();

                    System.out.println("DEVO --> " + devOrRep + " " + cmb_motivo3Str);
                    idDetSolGlobal = result;
                    setCambioEstadoOPDevRepMasivo();
                }

            } else {
                Messagebox.show("Debe seleccionar accion ", "", Messagebox.OK, null);
            }

        } catch (Exception ex) {
            System.out.println("Problemas al leer codigo de barra Exception--> " + ex);
        }

    }

    public void setCambioEstadoOPDevRepMasivo() throws SQLException, WriterException, ParseException {

        System.out.println("rb_dev.getValue() => " + rb_dev3.isChecked());
        System.out.println("rb_rep.getValue() => " + rb_rep3.isChecked());
        System.out.println("cmb_motivo_dev.getValue() => " + cmb_motivo_dev3.getValue());
        String cmb_motivoStr = cmb_motivo_dev3.getValue();

        responseVal2 = true;
        if (!rb_dev3.isChecked() && !rb_rep3.isChecked()) {
            responseVal2 = false;
            Messagebox.show("Debe seleccionar si es Devolucion o Reparo) ", "", Messagebox.OK, null,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
//                        rb_dev.setSelected(true);

                    }
                }
            });
        }

        if ("".equals(cmb_motivoStr) || cmb_motivoStr == "") {

            if (rb_dev3.isChecked()) {
                responseVal2 = false;
                Messagebox.show("Debe seleccionar Motivo de Devolucion ", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
//                            cmb_motivo_dev.setFocus(true);

                        }
                    }
                });
            }

            if (rb_rep3.isChecked()) {
                responseVal2 = false;
                Messagebox.show("Debe seleccionar Motivo para el Reparo", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
//                            cmb_motivo_dev.setFocus(true);

                        }
                    }
                });
            }
        }

        boolean resp = false;
        detSeleccion = new BandejaDetalle();
//        if (!"".equals(txtObservaciones.getValue())) {
        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getnOperacion() == txt_OperDcto2.getValue()) {
                detSeleccion = banddettt;
            }
        }

        String MDev = "";
        if (responseVal2) {
            try {
                System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
                //Se deja como devuelta
                if ("devol".equals(devrepchange)) {
                    resp = conexSol.cambioEstadoSolDet(idDetSolGlobal, "DEV", txtObservaciones.getValue());
                    MDev = "DEV";
                }
                if ("repar".equals(devrepchange)) {
                    resp = conexSol.cambioEstadoSolDet(idDetSolGlobal, "REP", txtObservaciones.getValue());
                    MDev = "REP";
                }

                boolean respInsertRep = false;
                if (resp) {

                    List<MotivoDevolucionEnt> ltd = new ArrayList<MotivoDevolucionEnt>();
                    ltd = motivoDevolucionJDBC.getMotivoDevolucionPorNom(cmb_motivoStr);
                    int id_tipoDev = ltd.get(0).getDi_fk_TipoDev();
                    int idMotivoDev = ltd.get(0).getDi_IdMotivoDev();

                    DevolucionEnt dev = new DevolucionEnt();
                    dev.setDi_fk_IdOper(idDetSolGlobal);
                    dev.setDi_fk_IdTipoDev(id_tipoDev);
                    dev.setDi_fk_IdMotivoDev(idMotivoDev);
                    dev.setDi_fk_IdSolOrigen(id_solicitud);
                    dev.setDi_fk_IdSolActual(0);
                    dev.setDi_fk_IdEstadoDev(1);
//                  dev.setDdt_FechaActualiza();
                    dev.setDi_fk_IdUbiOrigen(8);
                    dev.setDi_fk_IdUbiDestino(2);
                    dev.setDv_ObsOperacion("");

                    respInsertRep = devolucionJDBC.insert(dev);

                    System.out.println("Reparo Insertado ? => " + respInsertRep);

                }

                if (resp) {
                    if ("devol".equals(devrepchange)) {
                        Messagebox.show("Operacion Devuelta Correctamente ", "", Messagebox.OK, null,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                }
                            }
                        });
                    }
                    if ("repar".equals(devrepchange)) {
                        Messagebox.show("Reparo Realizado Correctamente ", "", Messagebox.OK, null,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                }
                            }
                        });
                    }

                    activePage = grid_DetSol.getPaginal().getActivePage();
                    System.out.println("-->*#ACTIVE PAGE " + activePage);
                    updateDetalleGrid(id_solicitud);
                    id_addDevPanel.setVisible(false);

                    txtObservaciones.setValue(null);

                    //Envia a cierrre de Solicitud
                    String[] EvaluaCierre = validaCantOperaciones(id_solicitud);

                    if ("Si".equals(EvaluaCierre[0])) {
                        cierraSolicitudBoj(EvaluaCierre);
                    }
                    solConPendientes = 0;
                    contSolCompletada = 0;
                    cantOpeComplet2 = 0;
                    cantOpePend2 = 0;
                    SolCompletada2 = "";
                    SolCompletada2 = EvaluaCierre[0];
                    cantOpeComplet2 = Integer.parseInt(EvaluaCierre[1]);
                    cantOpDev = Integer.parseInt(EvaluaCierre[2]);
                    cantOpRep = Integer.parseInt(EvaluaCierre[3]);

                }
//                else {
//                    Messagebox.show("Problemas al Mover el archivo el Documento. ", null, Messagebox.OK, Messagebox.ERROR,
//                            new org.zkoss.zk.ui.event.EventListener() {
//                        public void onEvent(Event e) {
//                            if (Messagebox.ON_OK.equals(e.getName())) {
//                                //OK is clicked
//                                Executions.sendRedirect("/sisboj/index");
//                            }
//                        }
//                    }
//                    );
//
//                }

            } catch (SQLException e) {
                e.getMessage();
            }
        }
//        } else {
//            Messagebox.show("Debe Agregar Observacion. ", null, Messagebox.OK, Messagebox.ERROR,
//                    new org.zkoss.zk.ui.event.EventListener() {
//                public void onEvent(Event e) {
//                    if (Messagebox.ON_OK.equals(e.getName())) {
//                        //OK is clicked
//                        txtObservaciones.focus();
//                    }
//                }
//            }
//            );
//        }

    }

    public String generaCodigoBarra() throws ParseException, WriterException {
        /*GENERACION DE CODIGO BARRA PARA SOLICITUD*/
        int idProdv = 0;
        String idProdS = "";

        int cantOperI = 0;
        String cantOperS = "";

        String fecCode = "";

        String CodeSol = "";
        CodeSol += id_solicitud;

        System.out.println("nameProductCode " + nameProductCode);

        //_conListDOc = new MvcConfig().documentosJDBC();
        lprod = conexProdCod.selectProductoByName(nameProductCode);

        for (Productos lmx : lprod) {
            idProdv = lmx.getDi_IdProducto();
        }
        idProdS = String.valueOf(idProdv);
        //CodeSol += idProdS;

        cantOperI = conexSol.getNumeroDocumentosSolicitud(id_solicitud);
        cantOperS = String.valueOf(cantOperI);

        //CodeSol += cantOperS;
        String dateStr = fecSolicCode;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = sdf.parse(dateStr);
        sdf = new SimpleDateFormat("yyyyMMdd");
        dateStr = sdf.format(date);

        CodeSol = "SOL" + id_solicitud + dateStr + cantOperS;
        //CodeSol += dateStr;

        /*encodeCode128SolP = new Code128Writer().encode(CodeSol, BarcodeFormat.CODE_128, 150, 80, null);
        bufferedImage128SolP = MatrixToImageWriter.toBufferedImage(encodeCode128SolP);
        _idCodeBarRear23.setContent(bufferedImage128SolP);
        _yyyyy.setValue(CodeSol);*/

 /*GENERACION DE CODIGO BARRA PARA SOLICITUD*/
        return CodeSol;
    }

    /*Metodo Decode para BufferedImage*/
    public void readQR(BufferedImage img) throws FormatException, ChecksumException, NotFoundException, IOException {

        LuminanceSource source = new BufferedImageLuminanceSource(img);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Reader reader = new MultiFormatReader();
        Result stringBarCode = reader.decode(bitmap);

        System.out.println("Encode128 ----> " + stringBarCode.getText());
    }

    public void printCodeBarOper(String code) throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        System.out.println("PRINT: " + code);
        ArrayList<String> codes = new ArrayList<String>();
        codes.add("ZE"+code);
        
        selecImpresoraInstalada(id_solicitud, codes);

        //imprimeCodBarra2(code);
    }

    /*Metodo en desuso reemplazado por metodo selecImpresoraInstalada() y popup*/
    public void imprimeCodBarra2(String codeBar) throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService psZebra = null;
        String bac = "\\";
        boolean verificaService = false;
        ResourceBundle myResources = ResourceBundle.getBundle("zpldiccionario");
        String ipZebra = myResources.getString("ipZebraConnection");

        for (PrintService printService : ps) {
            System.out.println("printService.getName() " + printService.getName());
            //System.out.println("RED :" + bac + bac + "161.131.230.132" + bac + "ZDesigner GT800 (EPL)"); ZDesigner GT800 (ZPL)
            PrintServiceAttributeSet printServiceAttributes = printService.getAttributes();
            PrinterState prnState = (PrinterState) printService.getAttribute(PrinterState.class);

            if (printService.getName().toString().contains("ZDesigner")) {

                System.out.println("ZDesigner PrintServices : --> " + printService.getName());

                if (printService.getName().equals(bac + bac + ipZebra + bac + "ZDesigner GT800 (EPL)")) {
                    psZebra = printService;

                    Attribute[] a = printServiceAttributes.toArray();
                    for (Attribute unAtribute : a) {
                        System.out.println("atributo: " + unAtribute.getName());
                    }

                    System.out.println("--- viendo valores especificos de los atributos ");

                    // valor especifico de un determinado atributo de la impresora
                    System.out.println("PrinterLocation: " + printServiceAttributes.get(PrinterLocation.class));
                    System.out.println("PrinterInfo: " + printServiceAttributes.get(PrinterInfo.class));
                    System.out.println("PrinterState: " + printServiceAttributes.get(PrinterState.class));
                    System.out.println("Destination: " + printServiceAttributes.get(Destination.class));
                    System.out.println("PrinterMakeAndModel: " + printServiceAttributes.get(PrinterMakeAndModel.class));
                    System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributes.get(PrinterIsAcceptingJobs.class));

                    /*VERIFICA*/
                    if (prnState == PrinterState.STOPPED) {
                        PrinterStateReasons prnStateReasons = (PrinterStateReasons) printService.getAttribute(PrinterStateReasons.class);
                        if ((prnStateReasons != null) && (prnStateReasons.containsKey(PrinterStateReason.SHUTDOWN))) {

                            Messagebox.show("PrintService is no longer available.", "", Messagebox.OK, null);
                            //throw new PrintException("PrintService is no longer available.");

                        }
                    } else {

                        if ((PrinterIsAcceptingJobs) (printService.getAttribute(PrinterIsAcceptingJobs.class)) == PrinterIsAcceptingJobs.NOT_ACCEPTING_JOBS) {

                            Messagebox.show("Printer is not accepting job.", "", Messagebox.OK, null);
                        } else {
                            System.out.println("ZDesigner Network: --> " + printService.getName());
                            verificaService = true;
                            break;
                        }

                    }

                }
            }

        }

        DiccionarioCodeZpl dicc = new DiccionarioCodeZpl();
        String codeFinal = dicc.diccionarioCodeZpl(codeBar);

        String zplCommand = "^XA^F090,75^BY3^BCN,100,Y,N,N^FD>9" + codeFinal + "\n"
                + "^XZ";

        // convertimos el comando a bytes  
        byte[] by = zplCommand.getBytes();
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(by, flavor, null);

        if (verificaService) {

            // creamos el printjob  
            DocPrintJob job = psZebra.createPrintJob();
            // imprimimos  
            //job.print(doc, null);

        } else {

            Messagebox.show("No se Encontro Impresora Zebra Conectada ", "", Messagebox.OK, null);
        }

    }
    
    //Este Metodo levanta popup impresora
    public void selecImpresoraInstalada(int idsol, ArrayList<String> codeBar) {

        String iddsol = String.valueOf(idsol);

        ArrayList<String> Idsol = new ArrayList<String>();
        Idsol.add(iddsol);

        Map<String, ArrayList<String>> arguments = new HashMap<String, ArrayList<String>>();

        //detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("IdSolicitud", Idsol);
        arguments.put("Codes", codeBar);

        String template = "custodia/detallesPrint.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    

    @Listen("onClick = #id_printCod")
    public void codePrints() throws SQLException, IOException, WriterException, PrintException, PrintException, COSVisitorException {
        ArrayList<String> codes = new ArrayList<String>();
        nnn = conexDetSol.listDetSolicitud(id_solicitud);

        for (final BandejaDetalle nnn : nnn) {

            codes.add(nnn.getnOperacion());

        }
        selecImpresoraInstalada(id_solicitud, codes);
        //imprimeCodBarra(codes);/*Este ya no se usa por que lo hacemos desde el popup*/
        //iframe.setVisible(true);

    }

    @Listen("onClick = #id_printCodSol")
    public void codePrintsSol() throws SQLException, IOException, WriterException, PrintException, PrintException, COSVisitorException {
        ArrayList<String> codes = new ArrayList<String>();
//        nnn = conexDetSol.listDetSolicitud(id_solicitud);
//
//        for (final BandejaDetalle nnn : nnn) {
//
//            codes.add(nnn.getnOperacion());
//
//        }

        System.out.println("idSolicitudDecodeFinal--> "+idSolicitudDecodeFinal);
        codes.add(idSolicitudDecodeFinal);
        selecImpresoraInstalada(id_solicitud, codes);
        
        //imprimeCodBarra(codes);/*Este ya no se usa por que lo hacemos desde el popup*/
        //iframe.setVisible(true);

    }

    /*Metodo en desuso reemplazado por metodo selecImpresoraInstalada() y popup*/
    public void imprimeCodBarra(ArrayList<String> codeBar) throws IOException, WriterException, PrintException, COSVisitorException, SQLException {
        // resultWin.detach();
        BitMatrix bitMatrixXXXX;
        PDDocument document = new PDDocument();
        PDPage emptyPage = null;
        int line = 0;
        PDFont font = PDType1Font.HELVETICA;
        PDFont fontMono = PDType1Font.COURIER;

        // emptyPage = new PDPage(PDPage.);
        emptyPage = new PDPage();
        PDPage blankPage = new PDPage();
        PDRectangle rect = emptyPage.getMediaBox();
        // emptyPage.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));
        document.addPage(emptyPage);
        //document.addPage( blankPage );
        //document.save("EmptyPage.pdf");

        //PDFRenderer renderer = new PDFRenderer(document);
        //BufferedImage img = renderer.renderImage(12 - 1, 4f);
        //ImageIOUtil.writeImage(img, new File(RESULT_FOLDER, "ENG-US_NMATSCJ-1.103-0330-page12cropped.jpg").getAbsolutePath(), 300);
        PDPageContentStream content = new PDPageContentStream(document, emptyPage);

        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLACK);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
        content.drawString("Codigos Operaciones Solicitud N� " + id_solicitud);
        content.endText();
        int cont = 0;

        System.out.println("CODEBARSIZE: " + codeBar.size());

        for (int x = 0; x < codeBar.size(); x++) {

            rimpagString = rect.getHeight() - (49 + 1) * (++line);
            rimpagImage = rect.getHeight() - 50 * (++line);

            content.beginText();
            content.setFont(fontMono, 15);
            content.setNonStrokingColor(Color.BLACK);
            content.moveTextPositionByAmount(175, rimpagString);
            content.drawString(codeBar.get(x));
            content.endText();

            bitMatrixXXXX = new Code128Writer().encode(codeBar.get(x), BarcodeFormat.CODE_128, 150, 80, null);
            BufferedImage buffImgt = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
            PDXObjectImage ximaget = new PDPixelMap(document, buffImgt);
            content.drawXObject(ximaget, 150, rimpagImage, 150, 50);
            cont++;

            //if number of results exceeds what can fit on the first page
            if (cont % 6 == 0) {

                content.close();

                line = 0;

                emptyPage = new PDPage(PDPage.PAGE_SIZE_LETTER);
                document.addPage(emptyPage);
                content = new PDPageContentStream(document, emptyPage);

                //generate data for second page
            }

        }

        //// agregando codigos de los Documentos del Lote
        conexDetSol = new MvcConfig().detSolicitudJDBC();

        String CompiladoNumeros = "";

        content.close();
        //contente.close();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        pss = new PDStream(document);
        // document.close();

        // PDStream contents = emptyPage.getContents();
        String path = "C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf";
        File f = new File(path);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            //iframe.setContent(mymedia);
        }

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            //Samsung M536x Series
            if (printService.getName().equals("Samsung M536x Series")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            //Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());
            Messagebox.show("Codigos Impresos Correctamente.");

        }

        InputStream is = pss.createInputStream();

        //Messagebox.show("is" + is.toString());
        final byte[] byteStream = out.toByteArray();
        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        // printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());
    }

    public String[] GuardarDocumentoCode(String _codigoOperOrigen) throws SQLException {
        boolean insert = false;
        String[] respStr = new String[3];
        String Glosa = "Documento Escaneado Por C�digo de Barra";

//        try {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        int id_tipDcto = 0;
        int id_EstDcto = 0;

        System.out.println("cmb_TipoDocumento.getValue() => " + cmb_TipoDocumento.getValue());
        System.out.println("cmb_EstDcto.getValue() => " + cmb_EstDcto.getValue());
        System.out.println("txt_docsEsperados.getValue() => " + txt_docsEsperados.getValue());

        String lm2 = "";
        int tipoDocInt = 0;
        List<ProdTipoDoc> dp = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(_codigoOperOrigen));

        for (ProdTipoDoc lmx : dp) {
            lm2 = lmx.getDv_NomTipDcto();
            tipoDocInt = lmx.getDi_IdTipDcto();
        }

        System.out.println("String NOM DOC " + lm2 + " " + tipoDocInt);

        String tipodoctodesc = lm2;//cmb_TipoDocumento.getValue();
        //String estado = cmb_EstDcto.getValue();
        //docsEsp = Integer.parseInt(txt_docsEsperados.getValue());
        this._nuevodocto = new Documento();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();

        ope = _op.getOperaciones(_codigoOperOrigen);

        //se retorna a la pag principal
        Id_Operacion = ope.getDi_IdOperacion();
        //boolean checkapbanco = id_chechApoderado.isChecked();

        boolean checkapbanco = false;

        this._nuevodocto.setDdt_FechaSolicitud(dateFormat.format(date));
        this._nuevodocto.setDb_ApoderadoBanco(checkapbanco);
        this._nuevodocto.setDb_checkOk(true);
        this._nuevodocto.setDi_fk_IdUbi(2);
        this._nuevodocto.setDv_Glosa(Glosa);
        this._nuevodocto.setDdt_FechaSalidaBov(dateFormat.format(date));
        this._nuevodocto.setDi_fk_IdEstado(152);
        this._nuevodocto.setDi_fk_IdOper(ope.getDi_IdOperacion());
        this._nuevodocto.setDi_fk_TipoDcto(tipoDocInt);

        insert = _docto.insert(_nuevodocto);

        boolean respCambEst = false;
        if (insert) {

            //Se cambia el estado a la operacion (Completa)
            respCambEst = conexSol.cambioEstadoSolDet(Id_Operacion, "C", "");

            bNuevo = true;
            Messagebox.show("El Documento fue agregado Correctamente", "Guardar Documento", Messagebox.OK, Messagebox.INFORMATION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                    }
                }
            }
            );

            updateDetalleGrid(id_solicitud);

            if (bNuevo) {
                numDctsOpAct += 1;
            } else {
                numDctsOpAct = numDctsOpAct;
            }

        } else {
            Messagebox.show("Problemas al Agregar el Documento. \n", "Guardar Documento", Messagebox.OK, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        // Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        //Executions.sendRedirect("/sisboj/index");
                    }
                }
            }
            );

        }

        /*Limpia datos info documento*/
        cmb_TipoDocumento.setValue(
                null);
        cmb_EstDcto.setValue(
                null);
        //  txt_docsEsperados.setValue(null);
        id_chechApoderado.setChecked(
                false);
        txt_GlosaDcto2.setValue(
                "");

        // Messagebox.show("DatosCamputados:::");
        if (insert && respCambEst) {
            //se verifica si estan agregados todos los documentos en las operaciones de la solicitud                
            List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
            lbd = getListDet(id_solicitud);
            String SolCompletada = "";
            solConPendientes = 0;
            contSolCompletada = 0;
            for (int i = 0; i < lbd.size(); i++) {
                if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "No";
                } else {
                    if ("PENCUS".equals(lbd.get(i).getDv_codestado())) {
                        solConPendientes = solConPendientes + 1;
                    }
                    if ("CDSCUS".equals(lbd.get(i).getDv_codestado())) {
                        // SolCompletada = "Si";
                        contSolCompletada = contSolCompletada + 1;
                    }
                }

            }

            if ((solConPendientes + contSolCompletada) == lbd.size()) {
                SolCompletada = "Si";

            } else {
                SolCompletada = "No";
            }

            System.out.println("GuardarDocumento contSolCompletada2 => " + contSolCompletada);
            System.out.println("GuardarDocumento solConPendientes2 => " + solConPendientes);

            respStr = new String[3];
            respStr[0] = SolCompletada;
            respStr[1] = String.valueOf(contSolCompletada);
            respStr[2] = String.valueOf(solConPendientes);

        }

        return respStr;

    }

    @Listen("onClick = #ButtonGuardarDev")
    public void setCambioEstadoOPDevRep() throws SQLException, WriterException, ParseException {

        System.out.println("rb_dev.getValue() => " + rb_dev.isChecked());
        System.out.println("rb_rep.getValue() => " + rb_rep.isChecked());
        System.out.println("cmb_motivo_dev.getValue() => " + cmb_motivo_dev.getValue());
        String cmb_motivoStr = cmb_motivo_dev.getValue().trim();
        String txtObs = txtObservaciones.getValue().trim();

        responseVal2 = true;
        if (!rb_dev.isChecked() && !rb_rep.isChecked()) {
            responseVal2 = false;
            Messagebox.show("Debe seleccionar si es Devolucion o Reparo) ", "", Messagebox.OK, null,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
//                        rb_dev.setSelected(true);

                    }
                }
            });
        }

        if ("".equals(cmb_motivoStr) || cmb_motivoStr == "") {

            if (rb_dev.isChecked()) {
                responseVal2 = false;
                Messagebox.show("Debe seleccionar Motivo de Devolucion ", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
//                            cmb_motivo_dev.setFocus(true);

                        }
                    }
                });
            }

            if (rb_rep.isChecked()) {
                responseVal2 = false;
                Messagebox.show("Debe seleccionar Motivo para el Reparo", "", Messagebox.OK, null,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
//                            cmb_motivo_dev.setFocus(true);

                        }
                    }
                });
            }
        }

        boolean resp = false;
        detSeleccion = new BandejaDetalle();
//        if (!"".equals(txtObservaciones.getValue())) {
        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getnOperacion() == txt_OperDcto2.getValue()) {
                detSeleccion = banddettt;
            }
        }

        String MDev = "";
        if (responseVal2) {
            try {
                System.out.println("detSeleccion.getIdDetSol() => " + detSeleccion.getIdDetSol());
                //Se deja como devuelta
                if ("devol".equals(devrepchange)) {
                    resp = conexSol.cambioEstadoSolDet(detSeleccion.getIdDetSol(), "DEV", txtObservaciones.getValue());
                    MDev = "DEV";
                }
                if ("repar".equals(devrepchange)) {
                    resp = conexSol.cambioEstadoSolDet(detSeleccion.getIdDetSol(), "REP", txtObservaciones.getValue());
                    MDev = "REP";
                }

                boolean respInsertRep = false;
                if (resp) {

                    List<MotivoDevolucionEnt> ltd = new ArrayList<MotivoDevolucionEnt>();
                    ltd = motivoDevolucionJDBC.getMotivoDevolucionPorNom(cmb_motivoStr);
                    int id_tipoDev = ltd.get(0).getDi_fk_TipoDev();
                    int idMotivoDev = ltd.get(0).getDi_IdMotivoDev();

                    DevolucionEnt dev = new DevolucionEnt();
                    dev.setDi_fk_IdOper(detSeleccion.getIdOperacion());
                    dev.setDi_fk_IdTipoDev(id_tipoDev);
                    dev.setDi_fk_IdMotivoDev(idMotivoDev);
                    dev.setDi_fk_IdSolOrigen(id_solicitud);
                    dev.setDi_fk_IdSolActual(0);
                    dev.setDi_fk_IdEstadoDev(1);
//                    dev.setDdt_FechaActualiza();
                    dev.setDi_fk_IdUbiOrigen(8);
                    dev.setDi_fk_IdUbiDestino(2);
                    if (!"".equals(txtObs)) {
                        dev.setDv_ObsOperacion(txtObs);
                    } else {
                        dev.setDv_ObsOperacion("Sin Informaci�n Ingresada");
                    }
                    respInsertRep = devolucionJDBC.insert(dev);

                    System.out.println("Reparo Insertado ? => " + respInsertRep);

                }

                if (resp) {
                    if ("devol".equals(devrepchange)) {
                        Messagebox.show("Operacion Devuelta Correctamente ", "", Messagebox.OK, null,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                }
                            }
                        });
                    }
                    if ("repar".equals(devrepchange)) {
                        Messagebox.show("Reparo Realizado Correctamente ", "", Messagebox.OK, null,
                                new org.zkoss.zk.ui.event.EventListener() {
                            public void onEvent(Event e) {
                                if (Messagebox.ON_OK.equals(e.getName())) {
                                }
                            }
                        });
                    }

                    activePage = grid_DetSol.getPaginal().getActivePage();
                    System.out.println("-->*#ACTIVE PAGE " + activePage);
                    updateDetalleGrid(id_solicitud);
                    id_addDevPanel.setVisible(false);

                    txtObservaciones.setValue(null);

                    //Envia a cierrre de Solicitud
                    String[] EvaluaCierre = validaCantOperaciones(id_solicitud);

                    if ("Si".equals(EvaluaCierre[0])) {
                        cierraSolicitudBoj(EvaluaCierre);
                    }
                    solConPendientes = 0;
                    contSolCompletada = 0;
                    cantOpeComplet2 = 0;
                    cantOpePend2 = 0;
                    SolCompletada2 = "";
                    SolCompletada2 = EvaluaCierre[0];
                    cantOpeComplet2 = Integer.parseInt(EvaluaCierre[1]);
                    cantOpDev = Integer.parseInt(EvaluaCierre[2]);
                    cantOpRep = Integer.parseInt(EvaluaCierre[3]);

                }
//                else {
//                    Messagebox.show("Problemas al Mover el archivo el Documento. ", null, Messagebox.OK, Messagebox.ERROR,
//                            new org.zkoss.zk.ui.event.EventListener() {
//                        public void onEvent(Event e) {
//                            if (Messagebox.ON_OK.equals(e.getName())) {
//                                //OK is clicked
//                                Executions.sendRedirect("/sisboj/index");
//                            }
//                        }
//                    }
//                    );
//
//                }

            } catch (SQLException e) {
                e.getMessage();
            }
        }
//        } else {
//            Messagebox.show("Debe Agregar Observacion. ", null, Messagebox.OK, Messagebox.ERROR,
//                    new org.zkoss.zk.ui.event.EventListener() {
//                public void onEvent(Event e) {
//                    if (Messagebox.ON_OK.equals(e.getName())) {
//                        //OK is clicked
//                        txtObservaciones.focus();
//                    }
//                }
//            }
//            );
//        }
    }

    public String[] validaCantOperaciones(int idSol) throws SQLException {

        String[] respStr = new String[3];
        //se verifica si estan agregados todos los documentos en las operaciones de la solicitud                
        List<BandejaDetalle> lbd = new ArrayList<BandejaDetalle>();
        lbd = getListDet(idSol);
        String SolCompletada = "";
        solConPendientes = 0;
        contSolCompletada = 0;
        contSolRecepcionada = 0;
        contSolReparo = 0;
        cantOpDev = 0;
        cantOpRep = 0;
        cantTotalOp = 0;

        for (int i = 0; i < lbd.size(); i++) {
            if ("Incompleta".equals(lbd.get(i).getDv_estadoOpe()) && !"PENCUS".equals(lbd.get(i).getDv_codestado())) {
                // SolCompletada = "No";
            } else {
                if ("BOJOPREC".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "Si";
                    contSolRecepcionada = contSolRecepcionada + 1;
                }
                if ("BOJOPDEV".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "Si";
                    cantOpDev = cantOpDev + 1;
                }
                if ("BOJOPCREP".equals(lbd.get(i).getDv_codestado())) {
                    // SolCompletada = "Si";
                    cantOpRep = cantOpRep + 1;
                }
            }

        }

        if ((contSolRecepcionada + cantOpDev + cantOpRep) == lbd.size() || (contSolRecepcionada + cantOpDev) == lbd.size() || (contSolRecepcionada + cantOpRep) == lbd.size()
                || (cantOpDev + cantOpRep) == lbd.size()) {

            if (cantOpDev == lbd.size()) {
                SolCompletada = "Si";
            } else if (cantOpRep == lbd.size()) {
                SolCompletada = "Si";
            } else {
                SolCompletada = "Si";
            }

        } else {
            SolCompletada = "No";
        }

        System.out.println("GuardarDocumento contSolCompletada2 => " + contSolRecepcionada);
        System.out.println("GuardarDocumento cantOpDev => " + cantOpDev);
        System.out.println("GuardarDocumento cantOpRep => " + cantOpRep);

        respStr = new String[4];
        respStr[0] = SolCompletada;
        respStr[1] = String.valueOf(contSolRecepcionada);
        respStr[2] = String.valueOf(cantOpDev);
        respStr[3] = String.valueOf(cantOpRep);

        updateDetalleGrid(idSol);
        listCountProdTipoDoc();

        return respStr;

    }

    public boolean cierraSolicitudBoj(String[] Str) throws SQLException, WriterException, ParseException {

        System.out.println(" cierraSolicitud SolCompletada => " + Str[0]);
        System.out.println(" cierraSolicitud cantOpeComplet => " + Str[1]);
        System.out.println(" cierraSolicitud cantOpePend => " + Str[2]);

//        SolCompletada2 = Str[0];
//        cantOpeComplet2 = Integer.parseInt(Str[1]);
//        cantOpePend2 = Integer.parseInt(Str[2]);
        SolCompletadaBoj = Str[0];
        contSolRecepcionada = Integer.parseInt(Str[1]);
        cantOpDev = Integer.parseInt(Str[2]);
        cantOpRep = Integer.parseInt(Str[3]);

        //Si estan completas todas las operaciones se cambia de estado la solicitud  
//        try {
        //int cantTotalOp = contSolRecepcionada + cantOpDev + cantOpRep;
        cantTotalOp = Integer.parseInt(Str[1]) + Integer.parseInt(Str[2]) + Integer.parseInt(Str[3]);

        if ("Si".equals(SolCompletadaBoj) && cantOpDev == 0 && cantOpRep == 0) {

            //Cerra Solcicitud para ser enviada a bandeja recepcion de BOJ(Cambiar estado a la solicitud)
            cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "recepcionado");

            //cambioEstado=true; LRECBOJ
            if (cambioEstado) {

                boolean updCodeBarSol;
                String codbar = generaCodigoBarra();

                //updCodeBarSol = conexSol.updateSolicitudCodeBar(id_solicitud, "dv_CodeBar", codbar);
                System.out.println("Solicitud Recepcionada Correctamente => Generar codigo de barra");

            }
            if (cambioEstado) {
                responseCierreSolBoj = true;
                Messagebox.show("Solicitud Recepcionada Correctamente", "Fue Movida a Solicitudes Recepcionadas.", Messagebox.OK, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            System.out.println("Solicitud Recepcionada Correctamente");

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                            responseCierreSolBoj = false;
                        }

                        gridPrincipal();
                        cantOpRep = 0;
                    }

                });
                // return responseCierreSolBoj;
            } else {
                responseCierreSolBoj = false;
                //return responseCierreSolBoj;
            }
        }

        if ("Si".equals(SolCompletadaBoj) && cantOpDev == cantTotalOp) {
            System.out.println("estoy aqui Recepcion");
            // responseCierreSolBoj = cierreConPendientes();
            Messagebox.show("Solicitud Con Operaciones para devolver.", "�Desea Cerrar esta solicitud para ser Devuelta a UCC?.", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                    if (Messagebox.ON_OK.equals(e.getName())) {

//                        //Cerrar Solcicitud(Cambiar estado a la solicitud)
                        cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "CerradoNuevaDev");

                        System.out.println("SI CON OP PARA DEVOLVER");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        responseCierreSolBoj = false;

                    }

                    gridPrincipal();

                }
            });

            SolCompletadaBoj = "";
            contSolRecepcionada2 = 0;
            contSolReparo2 = 0;
            cantOpDev = 0;
            cantOpDev = 0;
        }

        if ("Si".equals(SolCompletadaBoj) && cantOpRep == cantTotalOp) {
            System.out.println("estoy aqui Recepcion");
            Messagebox.show("Solicitud de Operaciones con Reparo.", "�Desea Cerrar esta solicitud para ser Devuelta a UCC?.", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                    if (Messagebox.ON_OK.equals(e.getName())) {

//                        //Cerrar Solcicitud(Cambiar estado a la solicitud)
                        cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "CerradoNuevaRep");

                        System.out.println("SI CON REPARO");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        responseCierreSolBoj = false;

                    }

                    gridPrincipal();

                }
            });

            SolCompletadaBoj = "";
            contSolRecepcionada2 = 0;
            contSolReparo2 = 0;
            cantOpDev = 0;
            cantOpRep = 0;
        }

        if ("Si".equals(SolCompletadaBoj) && (cantOpDev > 0 || cantOpRep > 0) && cantOpDev != cantTotalOp && cantOpRep != cantTotalOp && contSolRecepcionada2 != cantTotalOp) {
            System.out.println("estoy aqui Recepcion 4");
            // responseCierreSolBoj = cierreConPendientes();
            Messagebox.show("Solicitud Con Operaciones para devolver.", "�Desea Cerrar esta solicitud para ser Devuelta a UCC?.", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) throws SQLException, WriterException, ParseException {
                    if (Messagebox.ON_OK.equals(e.getName())) {

////                        //Cerrar Solcicitud(Cambiar estado a la solicitud)
//                        cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "CerradoNuevaRep");
//
                        boolean cambEstOp = false;
//                        //Cambia de estado ooperaciones en tabla tb_Sys_DetSolicitud(Completa)                                    
//                        
//                        
//                             cambEstOp = conexDetSol.updateOpeSol(id_solicitud, "di_fk_IdEstado", 0);
//
//                        if (cambioEstado && cambEstOp && updCantidadDocs) {
////                          if (cambioEstado && cambEstOp) {
//                            // Se crea hijo de esta para las operaciones con estado Pendiente y se obtiene hijo creado

                        //se obtiene id de Ubicacion
                        List<Ubicacion> lUbi = new ArrayList<Ubicacion>();
                        lUbi = ubicacionJDBC.getUbicacion("dv_DetUbi", "BACK OFFICE");
//
//
                        if (cantOpDev > 0) {
                            List<Estado> ListEstado = new ArrayList();
                            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
                            ListEstado = estJDBC.getIdEstadoByCodigo("NVADEV");
//
//                        
                            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Date dat = new Date();
                            System.out.println("df.format(dat) => " + df.format(dat).toString());
//                            //Se obtiene la solicitud padre
                            Solicitud sol = new Solicitud();
                            sol = conexSol.getSolicitud(id_solicitud);

                            newHijosol = new Solicitud();
                            // newHijosol.setDi_IdSolicitud(id_solicitud);
                            newHijosol.setDi_fk_IdUbi(lUbi.get(0).getDi_IdUbicacion());
                            newHijosol.setDi_fk_IdHito(sol.getDi_fk_IdHito());
                            newHijosol.setDi_fk_IdEstado(ListEstado.get(0).getDi_IdEstado());
                            newHijosol.setDdt_FechaCreacion(df.format(dat));
                            newHijosol.setDi_cantidadDocs(cantOpDev);
                            //newHijosol.setDi_SolPadre(sol.getDi_IdSolicitud());

                            crearHijo = conexSol.InsertHijoSol(newHijosol);

                            //Se Cambiar estado a la solicitud Original
//                            cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(crearHijo.getDi_IdSolicitud(), "CerradoNuevaRep");
//                            //Si el hijo se creo correctamente, se vicula el padre con el hijo
                            if (crearHijo != null) {

                                JerarquiaSolEnt jerEnt = new JerarquiaSolEnt();
                                jerEnt.setDi_fk_IdSolicitudPadre(sol.getDi_IdSolicitud());
                                jerEnt.setDi_fk_IdSolicitudHijo(crearHijo.getDi_IdSolicitud());
//
                                updPadre = jerarquiaSolJDBC.insertJerarquiaSol(jerEnt);
                                System.out.println("updPadre => " + updPadre);
                            }
//
//                            //Una vez actualizado el padre se vinculan las operaciones pendientes a la solic. hijo creada.
                            if (updPadre) {
                                System.out.println("crearHijo.getDi_IdSolicitud() => " + crearHijo.getDi_IdSolicitud());
                                updOperNvaSol = conexDetSol.updateIdSolOperacionBoj(id_solicitud, "di_fk_IdSolicitud", crearHijo.getDi_IdSolicitud(), "DEV");
                            }

                            if (updOperNvaSol) {

                                boolean updCodeBarSol;
                                String codbar = generaCodigoBarra();

                                updCodeBarSol = conexSol.updateSolicitudCodeBar(crearHijo.getDi_IdSolicitud(), "dv_CodeBar", codbar);

                                System.out.println("Solicitud Completada Con Operaciones Pendientes => Generar codigo de barra");
                            }

//                             SolCompletada2 = "";
//                             cantOpeComplet2 = 0;
//                             cantOpePend2 = 0;
                            responseCierreSolBoj = true;
                        }

                        if (cantOpRep > 0) {
                            List<Estado> ListEstado = new ArrayList();
                            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
                            ListEstado = estJDBC.getIdEstadoByCodigo("NVACREP");
//
//                        
                            DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            Date dat = new Date();
                            System.out.println("df.format(dat) => " + df.format(dat).toString());
//                            //Se obtiene la solicitud padre
                            Solicitud sol = new Solicitud();
                            sol = conexSol.getSolicitud(id_solicitud);

                            newHijosol = new Solicitud();
                            // newHijosol.setDi_IdSolicitud(id_solicitud);
                            newHijosol.setDi_fk_IdUbi(lUbi.get(0).getDi_IdUbicacion());
                            newHijosol.setDi_fk_IdHito(sol.getDi_fk_IdHito());
                            newHijosol.setDi_fk_IdEstado(ListEstado.get(0).getDi_IdEstado());
                            newHijosol.setDdt_FechaCreacion(df.format(dat));
                            newHijosol.setDi_cantidadDocs(cantOpDev);
                            //newHijosol.setDi_SolPadre(sol.getDi_IdSolicitud());

                            crearHijo = conexSol.InsertHijoSol(newHijosol);

                            //Se Cambiar estado a la solicitud Original
//                            cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(crearHijo.getDi_IdSolicitud(), "CerradoNuevaRep");
//                            //Si el hijo se creo correctamente, se vicula el padre con el hijo
                            if (crearHijo != null) {

                                JerarquiaSolEnt jerEnt = new JerarquiaSolEnt();
                                jerEnt.setDi_fk_IdSolicitudPadre(sol.getDi_IdSolicitud());
                                jerEnt.setDi_fk_IdSolicitudHijo(crearHijo.getDi_IdSolicitud());
//
                                updPadre = jerarquiaSolJDBC.insertJerarquiaSol(jerEnt);
                                System.out.println("updPadre => " + updPadre);
                            }
//
//                            //Una vez actualizado el padre se vinculan las operaciones pendientes a la solic. hijo creada.
                            if (updPadre) {
                                System.out.println("crearHijo.getDi_IdSolicitud() => " + crearHijo.getDi_IdSolicitud());
                                updOperNvaSol = conexDetSol.updateIdSolOperacionBoj(id_solicitud, "di_fk_IdSolicitud", crearHijo.getDi_IdSolicitud(), "REP");
                            }

                            if (updOperNvaSol) {

                                boolean updCodeBarSol;
                                String codbar = generaCodigoBarra();

                                updCodeBarSol = conexSol.updateSolicitudCodeBar(crearHijo.getDi_IdSolicitud(), "dv_CodeBar", codbar);

                                System.out.println("Solicitud Completada Con Operaciones Pendientes => Generar codigo de barra");
                            }

//                             SolCompletada2 = "";
//                             cantOpeComplet2 = 0;
//                             cantOpePend2 = 0;
                            responseCierreSolBoj = true;
                        }

                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        responseCierreSolBoj = false;

                    }

                    gridPrincipal();

                }
            });

            devolv_div.setVisible(false);
            rb_dev3.setValue("");
            rb_rep3.setValue("");
            cmb_motivo_dev3.setValue("");
            //Se Cambiar estado a la solicitud Original
            //Cerra Solcicitud para ser enviada a bandeja recepcion de BOJ(Cambiar estado a la solicitud)
            cambioEstado = conexSol.CerrarOCambiarEstadoSolicitud(id_solicitud, "recepcionado");

            boolean updCantidadDocs = false;
            int restaDocs = 0;
            restaDocs = cantTotalOp - (cantOpDev + cantOpRep);

            updCantidadDocs = conexSol.updateSolicitud(id_solicitud, "di_CantDctosSolicitados", restaDocs);

            SolCompletadaBoj = "";
            contSolRecepcionada2 = 0;
            contSolReparo2 = 0;
            cantOpDev = 0;
            cantOpDev = 0;

        }

        // responseCierreSolBoj = false;
        return responseCierreSolBoj;
    }

    public void gridPrincipal() throws SQLException {
        id_addDocPanel.setVisible(false);
        _divFridDoc.setVisible(false);
//        this.listSol = conexSol.getSolNuev_Pend(UsuarioCustodia);
//
//        bandejacustodia = new ListModelList<BandejaCustodia>(listSol);
//
//        grid.setModel(bandejacustodia);
        id_detalleSolicitud.setVisible(false);
        coder.setVisible(false);
//        _idCodeBarRear2.setVisible(false);
//        _yyyy.setValue("");
        btnCerrarAll.setVisible(false);
        CargaPag();
        CargaEventos();
    }

    @Listen("onClick = #BtnCancelarGuardarDoc")
    public void cancelaGuardarDocumento() throws SQLException {

        updateDetalleGrid(id_solicitud);
        //  updateGridDoc(Id_Operacion, numerodocsOperActual, docsEsp);

        id_addDocPanel.setVisible(false);

    }

    @Listen("onClick = #BtnCancelarDev")
    public void cancelaEnviaDev() throws SQLException {

        updateDetalleGrid(id_solicitud);

        id_addDocPanel.setVisible(false);
        id_addDevPanel.setVisible(false);
        txtObservaciones.setValue(null);

    }

    /**
     * Metodo que retorna cantidad de documentos por producto
     *
     * @return List<ProdTipoDoc>
     */
    public List<ProdTipoDoc> listCountProdTipoDoc() {
        return prodTipoDocConex.getListCountProdTipoDoc();
    }

    public List<ProdTipoDoc> getListTipDoc() {
        List<ProdTipoDoc> ptd = new ListModelList(prodTipoDocConex.getListDocsPorIdProducto(detSeleccion.getnOperacion()));
        return ptd;
    }

    //esta funcionalidad levanta popup
    public void agregarObservacion(int op) {

        detSeleccion = new BandejaDetalle();

        for (final BandejaDetalle banddettt : listDet) {
            if (banddettt.getIdDetSol() == op) {
                detSeleccion = banddettt;
            }
        }

        Map<String, Object> arguments = new HashMap<String, Object>();

        detSeleccion.setDi_idSolicitud(id_solicitud);
        arguments.put("Seleccion", detSeleccion);

        String template = "custodia/detallesPoput.zul";

        window = (Window) Executions.createComponents(template, null, arguments);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick = #rb_dev")
    public void llenaComboMotDevDev() {
        cmb_motivo_dev.setValue("");
        List<MotivoDevolucionEnt> mdl = new ArrayList<MotivoDevolucionEnt>();
        mdl = motivoDevolucionJDBC.getListMotivoDevolucion(1);
        ListModelList lmCombo = new ListModelList();
        for (MotivoDevolucionEnt listaMD : mdl) {
            lmCombo.add(listaMD.getDv_NomMotivoDev());
        }

        cmb_motivo_dev.setModel(lmCombo);
        devrepchange = "devol";
    }

    @Listen("onClick = #rb_dev3")
    public void llenaComboMotDevDev3() {
        cmb_motivo_dev.setValue("");
        List<MotivoDevolucionEnt> mdl = new ArrayList<MotivoDevolucionEnt>();
        mdl = motivoDevolucionJDBC.getListMotivoDevolucion(1);
        ListModelList lmCombo = new ListModelList();
        for (MotivoDevolucionEnt listaMD : mdl) {
            lmCombo.add(listaMD.getDv_NomMotivoDev());
        }

        cmb_motivo_dev3.setModel(lmCombo);
        devrepchange = "devol";
        id_codebar.setFocus(true);
    }

    @Listen("onClick = #rb_rep")
    public void llenaComboMotDevRep() {

        cmb_motivo_dev.setValue("");
        List<MotivoDevolucionEnt> mdl = new ArrayList<MotivoDevolucionEnt>();
        mdl = motivoDevolucionJDBC.getListMotivoDevolucion(2);
        ListModelList lmCombo = new ListModelList();
        for (MotivoDevolucionEnt listaMD : mdl) {
            lmCombo.add(listaMD.getDv_NomMotivoDev());
        }

        cmb_motivo_dev.setModel(lmCombo);
        devrepchange = "repar";
    }

    @Listen("onClick = #rb_rep3")
    public void llenaComboMotDevRep3() {

        cmb_motivo_dev.setValue("");
        List<MotivoDevolucionEnt> mdl = new ArrayList<MotivoDevolucionEnt>();
        mdl = motivoDevolucionJDBC.getListMotivoDevolucion(2);
        ListModelList lmCombo = new ListModelList();
        for (MotivoDevolucionEnt listaMD : mdl) {
            lmCombo.add(listaMD.getDv_NomMotivoDev());
        }

        cmb_motivo_dev3.setModel(lmCombo);
        devrepchange = "repar";
        id_codebar.setFocus(true);
    }

    @Listen("onClick = #rb_devol")
    public void rb_devol() {

        devolv_div.setVisible(true);
        System.out.println("devolssss");
        id_codebar.setFocus(true);
    }

    public void codeChangeFocus() {

        id_codebar.setFocus(true);
    }

    @Listen("onClick = #rb_recep")
    public void rb_recep() {

        devolv_div.setVisible(false);
        System.out.println("recepssss");
        id_codebar.setFocus(true);
    }

}
