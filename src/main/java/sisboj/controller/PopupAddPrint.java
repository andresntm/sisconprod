package sisboj.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import config.MvcConfig;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.PrinterInfo;
import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.attribute.standard.PrinterLocation;
import javax.print.attribute.standard.PrinterMakeAndModel;
import javax.print.attribute.standard.PrinterState;
import javax.print.attribute.standard.PrinterStateReason;
import javax.print.attribute.standard.PrinterStateReasons;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Operaciones;
import sisboj.entidades.TipDcto;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.utilidades.DiccionarioCodeZpl;

/**
 *
 * @author esilves
 */
@SuppressWarnings("serial")
public class PopupAddPrint extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

    @Wire
    Window win_AdjuntaDcto;

    @Wire
    Button ButtonGuardarDoc;

    @Wire
    Combobox cmb_TipDcto;

    @Wire
    Vlayout btn_idvlayoutinfodoc;
    @Wire
    Vlayout _idvlayoutinfodoc;

    private EventQueue eq;
    private TipDctoJDBC _tipoDoc;
    private EstadoJDBC _estado;
    private ListModelList<TipDcto> _tipoDoctoList;
    private List<TipDcto> _listtipoDocto = new ArrayList<TipDcto>();
    private List<Estado> _estadoDocto = new ArrayList<Estado>();
    private Documento _nuevodocto;
    private final DocumentoJDBC _docto;
    private final OperacionesJDBC _op;
    private BandejaDetalle bandDet;
    private int numDctsOpAct = 0;
    private boolean bNuevo = false;
    private Operaciones ope;
    private int Id_Operacion;
    private int id_solicitud;
    private String barCodeSol = "";

    private String stringCode = "";
    private String[] parts;
    private String part1, part2;

    float rimpagString;
    float rimpagImage;
    PDStream pss;

    private ArrayList<String> Idsol1;
    private ArrayList<String> Idsol2;

    public PopupAddPrint() throws SQLException {
        _tipoDoc = new MvcConfig().tipDctoJDBC();
        _estado = new MvcConfig().estadoJDBC();
        _docto = new MvcConfig().documentosJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        try {
            CreaPag();
        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            win_AdjuntaDcto.detach();
        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            win_AdjuntaDcto.detach();
        }
    }

    private void CreaPag() throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        final Execution exec = Executions.getCurrent();

        bandDet = new BandejaDetalle();

        if (exec.getArg().get("Seleccion") != (null)) {
            bandDet = (BandejaDetalle) exec.getArg().get("Seleccion");
        } else {

        }

        int cont = 0;
        Object Idsol = exec.getArg().get("IdSolicitud");
        System.out.println("ARGUMENTO IDSOL!: " + Idsol);
        Object Codes = exec.getArg().get("Codes");
        System.out.println("ARGUMENTO Codes!: " + Codes);

        List list2;
        ListModelList lm2;

        Idsol1 = (ArrayList<String>) Idsol;
        Idsol2 = (ArrayList<String>) Codes;

        for (String tipodocto : Idsol1) {

            System.out.println("IDSOL--> " + Idsol1.get(cont));
            id_solicitud = Integer.parseInt(Idsol1.get(cont));

        }

        for (String tipodocto : Idsol2) {

            System.out.println("CODES--> " + Idsol2.get(cont));
            barCodeSol = Idsol2.get(cont);
            cont++;
        }

        list2 = new ArrayList();

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            //Samsung M536x Series

            System.out.println("PrintServices --> " + printService.getName());

            if (barCodeSol.startsWith("ZE")) {
                if (printService.getName().toString().contains("ZDesigner")) {
                    list2.add(printService.getName());
                    System.out.println("<-- Impresion Zebra -->");
                }
            } else {
                if(printService.getName().toString().contains("ZDesigner")){
                    System.out.println("*-->");
                }else{
                    list2.add(printService.getName());
                }
            }

        }

        _listtipoDocto = _tipoDoc.listTipDcto();
        _tipoDoctoList = new ListModelList<TipDcto>(_listtipoDocto);
        _estadoDocto = _estado.listEstado("Operaci�n C/Documento en custodia");

        lm2 = new ListModelList(list2);

        cmb_TipDcto.setModel(lm2);
        cmb_TipDcto.setPlaceholder("Seleccione Impresora.");

    }

    private void logicaTioAux() {
        String dv_idTipOpe;

        try {

            dv_idTipOpe = bandDet.getDv_IdTipOpe();

        } catch (Exception Ex) {

        }

    }

    @Listen("onClick = #closeButton")
    public void close() {
        win_AdjuntaDcto.detach();
    }

    @Listen("onClick = #ButtonGuardarDoc")
    public void GuardarDocumento() throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        System.out.println("Valor --> " + cmb_TipDcto.getValue());
        String nomImp = cmb_TipDcto.getValue();

        if (nomImp.equals("") || nomImp.equals(null) || nomImp == "" || nomImp.equals("Seleccione Impresora.")) {

            System.out.println("No a seleccionado impresora");
            Messagebox.show("Debe Seleccionar Impresora", "Impresora no seleccionada !!", Messagebox.OK, Messagebox.ERROR);
            return;
        } else {

            if (barCodeSol.startsWith("ZE")) {
                stringCode = barCodeSol;
                parts = stringCode.split("ZE");
                part1 = parts[0]; // 123
                part2 = parts[1]; // 654321
                System.out.println("Impresion Zebra--> " + part2);
                imprimeCodBarra2(part2, nomImp);
            } else {
                imprimeCodBarra(Idsol2, nomImp);
            }
            win_AdjuntaDcto.detach();
        }

    }

    public void imprimeCodBarra(ArrayList<String> codeBar, String nombreImpresora) throws IOException, WriterException, PrintException, COSVisitorException, SQLException {

        BitMatrix bitMatrixXXXX;
        PDDocument document = new PDDocument();
        PDPage emptyPage = null;
        int line = 0;
        PDFont font = PDType1Font.HELVETICA;
        PDFont fontMono = PDType1Font.COURIER;

        //emptyPage = new PDPage(PDPage.);
        emptyPage = new PDPage();
        PDPage blankPage = new PDPage();
        PDRectangle rect = emptyPage.getMediaBox();
        //emptyPage.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));
        document.addPage(emptyPage);
        //document.addPage( blankPage );
        //document.save("EmptyPage.pdf");

        //PDFRenderer renderer = new PDFRenderer(document);
        //BufferedImage img = renderer.renderImage(12 - 1, 4f);
        //ImageIOUtil.writeImage(img, new File(RESULT_FOLDER, "ENG-US_NMATSCJ-1.103-0330-page12cropped.jpg").getAbsolutePath(), 300);
        PDPageContentStream content = new PDPageContentStream(document, emptyPage);

        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLACK);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));

        if (barCodeSol.startsWith("SOL")) {
            content.drawString("Codigo Solicitud N� " + id_solicitud);
        } else {
            content.drawString("Codigos Operaciones Solicitud N� " + id_solicitud);
        }

        content.endText();
        int cont = 0;

        //codeBar.add("D11810036567329");
        System.out.println("CODEBARSIZE: " + codeBar.size());

        for (int x = 0; x < codeBar.size(); x++) {

            rimpagString = rect.getHeight() - (49 + 1) * (++line);
            rimpagImage = rect.getHeight() - 50 * (++line);

            content.beginText();
            content.setFont(fontMono, 15);
            content.setNonStrokingColor(Color.BLACK);
            content.moveTextPositionByAmount(175, rimpagString);
            content.drawString(codeBar.get(x));
            content.endText();

            bitMatrixXXXX = new Code128Writer().encode(codeBar.get(x), BarcodeFormat.CODE_128, 150, 80, null);
            BufferedImage buffImgt = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
            PDXObjectImage ximaget = new PDPixelMap(document, buffImgt);
            content.drawXObject(ximaget, 150, rimpagImage, 150, 50);
            cont++;

            //if number of results exceeds what can fit on the first page
            if (cont % 6 == 0) {

                content.close();

                line = 0;

                emptyPage = new PDPage(PDPage.PAGE_SIZE_LETTER);
                document.addPage(emptyPage);
                content = new PDPageContentStream(document, emptyPage);

                //generate data for second page
            }

        }

        String CompiladoNumeros = "";

        content.close();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        pss = new PDStream(document);
        // document.close();

        // PDStream contents = emptyPage.getContents();
        String path = "C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf";
        File f = new File(path);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            // iframe.setContent(mymedia);
        }

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            //Samsung M536x Series
            if (printService.getName().equals(nombreImpresora)) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            Messagebox.show("Codigos Impresos Correctamente.");

        }

        InputStream is = pss.createInputStream();

        //Messagebox.show("is" + is.toString());
        final byte[] byteStream = out.toByteArray();
        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());
    }

    /*Metodo de impresion para zebra*/
    public void imprimeCodBarra2(String codeBar, String nombreImpresora) throws IOException, WriterException, PrintException, COSVisitorException, SQLException, NotFoundException, ChecksumException, FormatException {

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);
        PrintService psZebra = null;
        String bac = "\\";
        boolean verificaService = false;
        ResourceBundle myResources = ResourceBundle.getBundle("zpldiccionario");
        String ipZebra = myResources.getString("ipZebraConnection");

        for (PrintService printService : ps) {
            System.out.println("printService.getName() " + printService.getName());
            //System.out.println("RED :" + bac + bac + "161.131.230.132" + bac + "ZDesigner GT800 (EPL)"); ZDesigner GT800 (ZPL)
            PrintServiceAttributeSet printServiceAttributes = printService.getAttributes();
            PrinterState prnState = (PrinterState) printService.getAttribute(PrinterState.class);

            if (printService.getName().toString().contains("ZDesigner")) {

                System.out.println("ZDesigner PrintServices : --> " + printService.getName());

                if (printService.getName().equals(bac + bac + ipZebra + bac + nombreImpresora)) {
                    psZebra = printService;

                    Attribute[] a = printServiceAttributes.toArray();
                    for (Attribute unAtribute : a) {
                        System.out.println("atributo: " + unAtribute.getName());
                    }

                    System.out.println("--- viendo valores especificos de los atributos ");

                    // valor especifico de un determinado atributo de la impresora
                    System.out.println("PrinterLocation: " + printServiceAttributes.get(PrinterLocation.class));
                    System.out.println("PrinterInfo: " + printServiceAttributes.get(PrinterInfo.class));
                    System.out.println("PrinterState: " + printServiceAttributes.get(PrinterState.class));
                    System.out.println("Destination: " + printServiceAttributes.get(Destination.class));
                    System.out.println("PrinterMakeAndModel: " + printServiceAttributes.get(PrinterMakeAndModel.class));
                    System.out.println("PrinterIsAcceptingJobs: " + printServiceAttributes.get(PrinterIsAcceptingJobs.class));

                    /*VERIFICA*/
                    if (prnState == PrinterState.STOPPED) {
                        PrinterStateReasons prnStateReasons = (PrinterStateReasons) printService.getAttribute(PrinterStateReasons.class);
                        if ((prnStateReasons != null) && (prnStateReasons.containsKey(PrinterStateReason.SHUTDOWN))) {

                            Messagebox.show("PrintService is no longer available.", "", Messagebox.OK, null);
                            //throw new PrintException("PrintService is no longer available.");

                        }
                    } else {

                        if ((PrinterIsAcceptingJobs) (printService.getAttribute(PrinterIsAcceptingJobs.class)) == PrinterIsAcceptingJobs.NOT_ACCEPTING_JOBS) {

                            Messagebox.show("Printer is not accepting job.", "", Messagebox.OK, null);
                        } else {
                            System.out.println("ZDesigner Network: --> " + printService.getName());
                            verificaService = true;
                            break;
                        }

                    }

                }
            }

        }

        //D41599999433sdsd
        DiccionarioCodeZpl dicc = new DiccionarioCodeZpl();
        String codeFinal = dicc.diccionarioCodeZpl(codeBar);

        String zplCommand = "^XA^F090,75^BY3^BCN,100,Y,N,N^FD>9" + codeFinal + "\n"
                + "^XZ";

        // convertimos el comando a bytes  
        byte[] by = zplCommand.getBytes();
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(by, flavor, null);

        if (verificaService) {

            // creamos el printjob  
            DocPrintJob job = psZebra.createPrintJob();
            // imprimimos  
            //job.print(doc, null);

        } else {

            Messagebox.show("No se Encontro Impresora Zebra Conectada ", "", Messagebox.OK, null);
        }

    }

}
