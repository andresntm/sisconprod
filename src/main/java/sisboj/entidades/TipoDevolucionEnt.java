/*
 * To change this license header choose License Headers in Project Properties.
 * To change this template file choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 *
 * @author exvguba
 */
public class TipoDevolucionEnt {
    
    private int di_IdTipoDev;
    private String dv_NomTipoDev;
    private String dv_CodTipoDev;
    private String dv_Descripcion;
    private Date ddt_FechaCreacion;
    private Date ddt_FechaActualiza;
    private int bit_Activo;

    public int getDi_IdTipoDev() {
        return di_IdTipoDev;
    }

    public void setDi_IdTipoDev(int di_IdTipoDev) {
        this.di_IdTipoDev = di_IdTipoDev;
    }

    public String getDv_NomTipoDev() {
        return dv_NomTipoDev;
    }

    public void setDv_NomTipoDev(String dv_NomTipoDev) {
        this.dv_NomTipoDev = dv_NomTipoDev;
    }

    public String getDv_CodTipoDev() {
        return dv_CodTipoDev;
    }

    public void setDv_CodTipoDev(String dv_CodTipoDev) {
        this.dv_CodTipoDev = dv_CodTipoDev;
    }

    public String getDv_Descripcion() {
        return dv_Descripcion;
    }

    public void setDv_Descripcion(String dv_Descripcion) {
        this.dv_Descripcion = dv_Descripcion;
    }

    public Date getDdt_FechaCreacion() {
        return ddt_FechaCreacion;
    }

    public void setDdt_FechaCreacion(Date ddt_FechaCreacion) {
        this.ddt_FechaCreacion = ddt_FechaCreacion;
    }

    public Date getDdt_FechaActualiza() {
        return ddt_FechaActualiza;
    }

    public void setDdt_FechaActualiza(Date ddt_FechaActualiza) {
        this.ddt_FechaActualiza = ddt_FechaActualiza;
    }

    public int getBit_Activo() {
        return bit_Activo;
    }

    public void setBit_Activo(int bit_Activo) {
        this.bit_Activo = bit_Activo;
    }


}
