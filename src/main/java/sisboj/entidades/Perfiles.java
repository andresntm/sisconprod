/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.io.Serializable;

/**
 *
 * @author exaesan
 */
public class Perfiles implements Serializable  {
    
    private int id_perfil;
    private String descripcion;
    private int id_jefe;
    private String dv_CodPerfil;

//    public Perfiles(int id_perfil, String descripcion, int id_jefe, String dv_CodPerfil) {
//        this.id_perfil = id_perfil;
//        this.descripcion = descripcion;
//        this.id_jefe = id_jefe;
//        this.dv_CodPerfil = dv_CodPerfil;
//    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId_jefe() {
        
        return id_jefe;
    }

    public void setId_jefe(int id_jefe) {
        this.id_jefe = id_jefe;
    }

    public String getDv_CodPerfil() {
        return dv_CodPerfil;
    }

    public void setDv_CodPerfil(String dv_CodPerfil) {
        this.dv_CodPerfil = dv_CodPerfil;
    }
    
    
}
