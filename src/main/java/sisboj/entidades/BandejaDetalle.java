/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import config.MetodosGenerales;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import org.zkoss.zul.Image;

/**
 *
 * @author excosoc
 */
public class BandejaDetalle {

    private String nOperacion;
    private int rut;
    private char digVer;
    private String nombrecomp;
    private boolean contrato;
    private boolean pagare;
    private int idDetSol;
    private String rutCompleto;
    private int numero_docs;
    private int numero_docs_esperado;
    private String dv_IdTipOpe;
    private int id_fk_ubicacion;
    private int di_idSolicitud;
    private Date ddt_fechaIngreso;
    private int idOperacion;
    private int di_fk_idestado;
    private String dv_codestado;
    private String dv_estadoOpe;
    private String dv_EstadoActual;
    private String ope_inhibida;
    
    private String tipoDoc;
    
    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getDv_codestado() {
        return dv_codestado;
    }

    public void setDv_codestado(String dv_codestado) {
        this.dv_codestado = dv_codestado;
    }
    private MetodosGenerales metodo = new MetodosGenerales();

    public BandejaDetalle(String nOperacion, int rut,
            char digVer,
            String nombrecomp,
            int idDetSol,
            boolean contrato,
            boolean pagare,
            String rutCompleto) {

        this.nOperacion = nOperacion;
        this.rut = rut;
        this.digVer = digVer;
        this.nombrecomp = nombrecomp;
        this.contrato = contrato;
        this.pagare = pagare;
        this.idDetSol = idDetSol;
        this.rutCompleto = rutCompleto;
    }

    /**
     * @return the ddt_fechaIngreso
     */
    public Date getDdt_fechaIngreso() {
        return ddt_fechaIngreso;
    }

    /**
     * @param ddt_fechaIngreso the ddt_fechaIngreso to set
     */
    public void setDdt_fechaIngreso(Date ddt_fechaIngreso) {
        this.ddt_fechaIngreso = ddt_fechaIngreso;
    }

    /**
     * @return the dv_IdTipOpe
     */
    public String getDv_IdTipOpe() {
        return dv_IdTipOpe;
    }

    /**
     * @param dv_IdTipOpe the dv_IdTipOpe to set
     */
    public void setDv_IdTipOpe(String dv_IdTipOpe) {
        this.dv_IdTipOpe = dv_IdTipOpe;
    }

    /**
     * @return the idDetSol
     */
    public int getIdDetSol() {
        return idDetSol;
    }

    /**
     * @param idDetSol the idDetSol to set
     */
    public void setIdDetSol(int idDetSol) {
        this.idDetSol = idDetSol;
    }

    public int getNumero_docs_esperado() {
        return numero_docs_esperado;
    }

    public void setNumero_docs_esperado(int numero_docs_esperado) {
        this.numero_docs_esperado = numero_docs_esperado;
    }

    public int getNumero_docs() {
        return numero_docs;
    }

    public void setNumero_docs(int numero_docs) {
        this.numero_docs = numero_docs;
    }

    public BandejaDetalle() {
    }

    public String getnOperacion() {
        return nOperacion;
    }

    public void setnOperacion(String nOperacion) {
        this.nOperacion = nOperacion;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public char getDigVer() {
        return digVer;
    }

    public void setDigVer(char digVer) {
        this.digVer = digVer;
    }

    public String getNombrecomp() {
        return nombrecomp;
    }

    public void setNombrecomp(String nombrecomp) {
        this.nombrecomp = nombrecomp;
    }

    public boolean getContrato() {
        return contrato;
    }

    public void setContrato(boolean contrato) {
        this.contrato = contrato;
    }

    public boolean getPagare() {
        return pagare;
    }

    public void setPagare(boolean pagare) {
        this.pagare = pagare;
    }

    /**
     * @return the rutCompleto
     */
    public String getRutCompleto() {
        return rutCompleto;
    }

    /**
     * @param r the rutCompleto to set
     * @param d the rutCompleto to set
     */
    public void setRutCompleto(int r, char d) {
        this.rutCompleto = r + "-" + d;
    }

    /**
     * @param rut the rutCompleto to set
     */
    public void setRutCompleto(String rut) {

        char a = ' ';

        if (metodo.validarRut(rut)) {
            this.rutCompleto = rut;
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            rut = rut.substring(0, rut.length() - 1);
            a = metodo.CalculaDv(Integer.parseInt(rut));

        } else {

        }

    }

    /**
     * @return the id_fk_ubicacion
     */
    public int getId_fk_ubicacion() {
        return id_fk_ubicacion;
    }

    /**
     * @param id_fk_ubicacion the id_fk_ubicacion to set
     */
    public void setId_fk_ubicacion(int id_fk_ubicacion) {
        this.id_fk_ubicacion = id_fk_ubicacion;
    }

    /**
     * @return the di_idSolicitud
     */
    public int getDi_idSolicitud() {
        return di_idSolicitud;
    }

    /**
     * @param di_idSolicitud the di_idSolicitud to set
     */
    public void setDi_idSolicitud(int di_idSolicitud) {
        this.di_idSolicitud = di_idSolicitud;
    }

    public int getIdOperacion() {
        return idOperacion;
    }

    public void setIdOperacion(int idOperacion) {
        this.idOperacion = idOperacion;
    }

    public int getDi_fk_idestado() {
        return di_fk_idestado;
    }

    public void setDi_fk_idestado(int di_fk_idestado) {
        this.di_fk_idestado = di_fk_idestado;
    }

    public String getDv_estadoOpe() {
        return dv_estadoOpe;
    }

    public void setDv_estadoOpe(String dv_estadoOpe) {
        this.dv_estadoOpe = dv_estadoOpe;
    }

    public String getDv_EstadoActual() {
        return dv_EstadoActual;
    }

    public void setDv_EstadoActual(String dv_EstadoActual) {
        this.dv_EstadoActual = dv_EstadoActual;
    }

    public String getOpe_inhibida() {
        return ope_inhibida;
    }

    public void setOpe_inhibida(String ope_inhibida) {
        this.ope_inhibida = ope_inhibida;
    }
    
    
    

}
