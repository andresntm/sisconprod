/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class Estado {

    private int di_IdEstado;
    private String dv_NomEstado;
    private String dv_DetEstado;
    private String dv_CodEstado;
    private String dv_Lugar;

    public Estado() {
    }

    ;
    
    public Estado(int di_IdEstado,
            String dv_NomEstado,
            String dv_DetEstado,
            String dv_CodEstado,
            String dv_Lugar) {

        this.di_IdEstado = di_IdEstado;
        this.dv_NomEstado = dv_NomEstado;
        this.dv_DetEstado = dv_DetEstado;
        this.dv_CodEstado = dv_CodEstado;
        this.dv_Lugar = dv_Lugar;
    }

    /**
     * @return the di_IdEstado
     */
    public int getDi_IdEstado() {
        return di_IdEstado;
    }

    /**
     * @param di_IdEstado the di_IdEstado to set
     */
    public void setDi_IdEstado(int di_IdEstado) {
        this.di_IdEstado = di_IdEstado;
    }

    /**
     * @return the dv_NomEstado
     */
    public String getDv_NomEstado() {
        return dv_NomEstado;
    }

    /**
     * @param dv_NomEstado the dv_NomEstado to set
     */
    public void setDv_NomEstado(String dv_NomEstado) {
        this.dv_NomEstado = dv_NomEstado;
    }

    /**
     * @return the dv_DetEstado
     */
    public String getDv_DetEstado() {
        return dv_DetEstado;
    }

    /**
     * @param dv_DetEstado the dv_DetEstado to set
     */
    public void setDv_DetEstado(String dv_DetEstado) {
        this.dv_DetEstado = dv_DetEstado;
    }

    /**
     *
     * @return dv_CodEstado
     */
    public String getDv_CodEstado() {
        return dv_CodEstado;
    }

    /**
     *
     * @param dv_CodEstado
     */
    public void setDv_CodEstado(String dv_CodEstado) {
        this.dv_CodEstado = dv_CodEstado;
    }

    /**
     * @return dv_Lugar
     */
    public String getDv_Lugar() {
        return dv_Lugar;
    }

    /**
     * @param dv_Lugar
     */
    public void setDv_Lugar(String dv_Lugar) {
        this.dv_Lugar = dv_Lugar;
    }

}
