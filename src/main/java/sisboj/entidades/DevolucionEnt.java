/*
 * To change this license header choose License Headers in Project Properties.
 * To change this template file choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 *
 * @author excosoc
 */
public class DevolucionEnt {

    private int di_fk_IdOper;
    private int di_fk_IdTipoDev;
    private int di_fk_IdMotivoDev;
    private int di_fk_IdSolOrigen;
    private int di_fk_IdSolActual;
    private int di_fk_IdEstadoDev;
    private Date ddt_FechaCreacion;
    private Date ddt_FechaActualiza;
    private String dv_ObsOperacion;
    private String dv_ObsSolicitud;
    private int di_fk_IdUbiOrigen;
    private int di_fk_IdUbiDestino;

    
    public String getDv_ObsSolicitud() {
        return dv_ObsSolicitud;
    }

    public void setDv_ObsSolicitud(String dv_ObsSolicitud) {
        this.dv_ObsSolicitud = dv_ObsSolicitud;
    }
    
    public int getDi_fk_IdOper() {
        return di_fk_IdOper;
    }

    public void setDi_fk_IdOper(int di_fk_IdOper) {
        this.di_fk_IdOper = di_fk_IdOper;
    }

    public int getDi_fk_IdTipoDev() {
        return di_fk_IdTipoDev;
    }

    public void setDi_fk_IdTipoDev(int di_fk_IdTipoDev) {
        this.di_fk_IdTipoDev = di_fk_IdTipoDev;
    }

    public int getDi_fk_IdMotivoDev() {
        return di_fk_IdMotivoDev;
    }

    public void setDi_fk_IdMotivoDev(int di_fk_IdMotivoDev) {
        this.di_fk_IdMotivoDev = di_fk_IdMotivoDev;
    }

    public int getDi_fk_IdSolOrigen() {
        return di_fk_IdSolOrigen;
    }

    public void setDi_fk_IdSolOrigen(int di_fk_IdSolOrigen) {
        this.di_fk_IdSolOrigen = di_fk_IdSolOrigen;
    }

    public int getDi_fk_IdSolActual() {
        return di_fk_IdSolActual;
    }

    public void setDi_fk_IdSolActual(int di_fk_IdSolActual) {
        this.di_fk_IdSolActual = di_fk_IdSolActual;
    }

    public int getDi_fk_IdEstadoDev() {
        return di_fk_IdEstadoDev;
    }

    public void setDi_fk_IdEstadoDev(int di_fk_IdEstadoDev) {
        this.di_fk_IdEstadoDev = di_fk_IdEstadoDev;
    }

    public Date getDdt_FechaCreacion() {
        return ddt_FechaCreacion;
    }

    public void setDdt_FechaCreacion(Date ddt_FechaCreacion) {
        this.ddt_FechaCreacion = ddt_FechaCreacion;
    }

    public Date getDdt_FechaActualiza() {
        return ddt_FechaActualiza;
    }

    public void setDdt_FechaActualiza(Date ddt_FechaActualiza) {
        this.ddt_FechaActualiza = ddt_FechaActualiza;
    }

    public String getDv_ObsOperacion() {
        return dv_ObsOperacion;
    }

    public void setDv_ObsOperacion(String dv_ObsOperacion) {
        this.dv_ObsOperacion = dv_ObsOperacion;
    }

    public int getDi_fk_IdUbiOrigen() {
        return di_fk_IdUbiOrigen;
    }

    public void setDi_fk_IdUbiOrigen(int di_fk_IdUbiOrigen) {
        this.di_fk_IdUbiOrigen = di_fk_IdUbiOrigen;
    }

    public int getDi_fk_IdUbiDestino() {
        return di_fk_IdUbiDestino;
    }

    public void setDi_fk_IdUbiDestino(int di_fk_IdUbiDestino) {
        this.di_fk_IdUbiDestino = di_fk_IdUbiDestino;
    }
    
    

}
