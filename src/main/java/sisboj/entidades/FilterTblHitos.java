/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.Negocio.Entidades;

import java.util.Date;
import org.zkoss.zul.Datebox;

/**
 *
 * @author EXVGUBA
 */
public class FilterTblHitos {
    
    private String ubicacion="";
    private String oficina="";
//    private int n_of;
//    private String reg;
//    private String op_orig;
//    private String operacion2;
//    private String tipo_op;
//    private double monto;
//    private String banca;
//    private String rut_completo;
//    private Long rut;
//    private String dv;
//    private String nombre;
//    private Date fan;
//    private String fcurse;
//    private int dias_mora;
//    private String code;
//    private String glosa;
//    private String reneg_por_nza;
//    private String ejecutivo_neg;
//    private int prioridad;
//    private String nom_banca;
//    private String producto;
//    private String solicitud;
//    private String nombre_region;
//    private Date fecha_ejecucion;
//    private String prioridad_pyme;
//    private String usuario_genera;
//    private int hito_especial;
//    private int acelerado;

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion==null?"":ubicacion.trim();
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina==null?"":oficina.trim();
    }
    
//
//    public int getN_of() {
//        return n_of;
//    }
//
//    public void setN_of(int n_of) {
//        this.n_of = n_of==null?"":n_of.trim();
//    }
//
//    public String getReg() {
//        return reg;
//    }
//
//    public void setReg(String reg) {
//        this.reg = reg==null?"":reg.trim();
//    }
//
//    public String getOp_orig() {
//        return op_orig;
//    }
//
//    public void setOp_orig(String op_orig) {
//        this.op_orig = op_orig==null?"":op_orig.trim();
//    }
//
//    public String getOperacion2() {
//        return operacion2;
//    }
//
//    public void setOperacion2(String operacion2) {
//        this.operacion2 = operacion2==null?"":operacion2.trim();
//    }
//
//    public String getTipo_op() {
//        return tipo_op;
//    }
//
//    public void setTipo_op(String tipo_op) {
//        this.tipo_op = tipo_op==null?"":tipo_op.trim();
//    }
//
//    public double getMonto() {
//        return monto;
//    }
//
//    public void setMonto(double monto) {
//        this.monto = monto==null?"":monto.trim();
//    }
//
//    public String getBanca() {
//        return banca;
//    }
//
//    public void setBanca(String banca) {
//        this.banca = banca==null?"":banca.trim();
//    }
//
//    public String getRut_completo() {
//        return rut_completo;
//    }
//
//    public void setRut_completo(String rut_completo) {
//        this.rut_completo = rut_completo==null?"":rut_completo.trim();
//    }
//
//    public Long getRut() {
//        return rut;
//    }
//
//    public void setRut(Long rut) {
//        this.rut = rut==null?"":rut;
//    }
//
//    public String getDv() {
//        return dv;
//    }
//
//    public void setDv(String dv) {
//        this.dv = dv==null?"":dv.trim();
//    }
//
//    public String getNombre() {
//        return nombre;
//    }
//
//    public void setNombre(String nombre) {
//        this.nombre = nombre==null?"":nombre.trim();
//    }
//
//    public Date getFan() {
//        return fan;
//    }
//
//    public void setFan(Date fan) {
//        this.fan = fan==null?"":fan;
//    }
//
//    public String getFcurse() {
//        return fcurse;
//    }
//
//    public void setFcurse(String fcurse) {
//        this.fcurse = fcurse==null?"":fcurse.trim();
//    }
//
//    public int getDias_mora() {
//        return dias_mora;
//    }
//
//    public void setDias_mora(int dias_mora) {
//        this.dias_mora = dias_mora==null?"":dias_mora.trim();
//    }
//
//    public String getCod() {
//        return cod;
//    }
//
//    public void setCode(String code) {
//        this.code = code==null?"":code.trim();
//    }
//
//    public String getGlosa() {
//        return glosa;
//    }
//
//    public void setGlosa(String glosa) {
//        this.glosa = glosa==null?"":glosa.trim();
//    }
//
//    public String getReneg_por_nza() {
//        return reneg_por_nza;
//    }
//
//    public void setReneg_por_nza(String reneg_por_nza) {
//        this.reneg_por_nza = reneg_por_nza==null?"":reneg_por_nza.trim();
//    }
//
//    public String getEjecutivo_neg() {
//        return ejecutivo_neg;
//    }
//
//    public void setEjecutivo_neg(String ejecutivo_neg) {
//        this.ejecutivo_neg = ejecutivo_neg==null?"":ejecutivo_neg.trim();
//    }
//
//    public int getPrioridad() {
//        return prioridad;
//    }
//
//    public void setPrioridad(int prioridad) {
//        this.prioridad = prioridad==null?"":prioridad;
//    }
//
//    public String getNom_banca() {
//        return nom_banca;
//    }
//
//    public void setNom_banca(String nom_banca) {
//        this.nom_banca = nom_banca==null?"":nom_banca.trim();
//    }
//
//    public String getProducto() {
//        return producto;
//    }
//
//    public void setProducto(String producto) {
//        this.producto = producto==null?"":producto.trim();
//    }
//
//    public String getSolicitud() {
//        return solicitud;
//    }
//
//    public void setSolicitud(String solicitud) {
//        this.solicitud = solicitud==null?"":solicitud.trim();
//    }
//
//    public String getNombre_region() {
//        return nombre_region;
//    }
//
//    public void setNombre_region(String nombre_region) {
//        this.nombre_region = nombre_region==null?"":nombre_region.trim();
//    }
//
//    public Date getFecha_ejecucion() {
//        return fecha_ejecucion;
//    }
//
//    public void setFecha_ejecucion(Date fecha_ejecucion) {
//        this.fecha_ejecucion = fecha_ejecucion==null?"":fecha_ejecucion.toString();
//    }
//
//    public String getPrioridad_pyme() {
//        return prioridad_pyme;
//    }
//
//    public void setPrioridad_pyme(String prioridad_pyme) {
//        this.prioridad_pyme = prioridad_pyme==null?"":prioridad_pyme.trim();
//    }
//
//    public String getUsuario_genera() {
//        return usuario_genera;
//    }
//
//    public void setUsuario_genera(String usuario_genera) {
//        this.usuario_genera = usuario_genera==null?"":usuario_genera.trim();
//    }
//
//    public int getHito_especial() {
//        return hito_especial;
//    }
//
//    public void setHito_especial(int hito_especial) {
//        this.hito_especial = hito_especial==null?"":hito_especial.trim();
//    }
//
//    public int getAcelerado() {
//        return acelerado;
//    }
//
//    public void setAcelerado(int acelerado) {
//        this.acelerado = acelerado==null?0:acelerado;
//    }

   
    
    
}
