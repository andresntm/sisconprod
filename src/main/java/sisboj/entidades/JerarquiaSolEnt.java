/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 *
 * @author EXVGUBA
 */
public class JerarquiaSolEnt {
    
    private int di_IdJerarquia;
    private int di_fk_IdSolicitudPadre;
    private int di_fk_IdSolicitudHijo;

    public int getDi_IdJerarquia() {
        return di_IdJerarquia;
    }

    public void setDi_IdJerarquia(int di_IdJerarquia) {
        this.di_IdJerarquia = di_IdJerarquia;
    }

    public int getDi_fk_IdSolicitudPadre() {
        return di_fk_IdSolicitudPadre;
    }

    public void setDi_fk_IdSolicitudPadre(int di_fk_IdSolicitudPadre) {
        this.di_fk_IdSolicitudPadre = di_fk_IdSolicitudPadre;
    }

    public int getDi_fk_IdSolicitudHijo() {
        return di_fk_IdSolicitudHijo;
    }

    public void setDi_fk_IdSolicitudHijo(int di_fk_IdSolicitudHijo) {
        this.di_fk_IdSolicitudHijo = di_fk_IdSolicitudHijo;
    }

    @Override
    public String toString() {
        return "JerarquiaSolEnt{" + "di_IdJerarquia=" + di_IdJerarquia + ", di_fk_IdSolicitudPadre=" + di_fk_IdSolicitudPadre + ", di_fk_IdSolicitudHijo=" + di_fk_IdSolicitudHijo + '}';
    }
    
    

}
