package sisboj.entidades;

public class BandejaCustodia {

    private int numDctos;
    private int numsol;
    private String estado;
    private String producto;
    private String hito;
    private String ubacion;
    private String codeBar;
    private String FechaRegistro;
    private String fechaCierre;
    private String fechaEnvio;
    private int numOpeInh;

    public BandejaCustodia() {
    }

    ;
    
    public BandejaCustodia(String hito, int numsol, int numDctos, String estado, String producto, String ubacion, String codeBar, String fechaCierre, String fechaEnvio,int numOpeInh) {
        this.numsol = numsol;
        this.hito = hito;
        this.estado = estado;
        this.numDctos = numDctos;
        this.producto = producto;
        this.ubacion = ubacion;
        this.codeBar = codeBar;
        this.fechaCierre = fechaCierre;
        this.fechaEnvio = fechaEnvio;
        this.numOpeInh = numOpeInh;

    }

    public String getCodeBar() {
        return codeBar;
    }

    public void setCodeBar(String codeBar) {
        this.codeBar = codeBar;
    }

    public int getNumsol() {
        return numsol;
    }

    public void setNumsol(int numsol) {
        this.numsol = numsol;
    }

    public int getNumDctos() {
        return numDctos;
    }

    public void setNumDctos(int numDctos) {
        this.numDctos = numDctos;
    }

    public String getHito() {
        return hito;
    }

    public void setHito(String hito) {
        this.hito = hito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the ubacion
     */
    public String getUbacion() {
        return ubacion;
    }

    /**
     * @param ubacion the ubacion to set
     */
    public void setUbacion(String ubacion) {
        this.ubacion = ubacion;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(String FechaRegistro) {
        this.FechaRegistro = FechaRegistro;
    }

    public String getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public String getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public int getNumOpeInh() {
        return numOpeInh;
    }

    public void setNumOpeInh(int numOpeInh) {
        this.numOpeInh = numOpeInh;
    }
    
    
    
    
}
