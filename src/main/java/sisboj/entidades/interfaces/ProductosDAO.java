/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.Productos;

/**
 *
 * @author excosoc
 */
public interface ProductosDAO {
            
    public void setDataSource(DataSource ds);
   
    public List<Productos> getListProductos(String nom);
    
    public boolean updateProducto(Productos prod);
    
    public List<Productos> nomProductosExiste(String nom);
    
    public List<Productos> codProductosExiste(String cod);
    
    public List<Productos> selectProductoByID(int id);
    
    public List<Productos> selectProductoByName(String name);
}
