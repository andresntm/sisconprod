/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.MotivoDevolucionEnt;

/**
 *
 * @author excosoc
 */
public interface MotivoDevolucionDAO {
            
    public void setDataSource(DataSource ds);
   
    public List<MotivoDevolucionEnt> getListMotivoDevolucion(int idTipoDev);
    
    public List<MotivoDevolucionEnt> getMotivoDevolucionPorNom(String nomMotDev);
    
  
    
}
