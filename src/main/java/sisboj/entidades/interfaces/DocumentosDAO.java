/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.Documento;
import sisboj.entidades.TipoDocumento;

/**
 *
 * @author excosoc
 */
public interface DocumentosDAO {

    public void setDataSource(DataSource ds);

    public Documento getDocumento(int di_IdDcto);

    public List<Documento> listDocumento(int id_operacion);

    public boolean update(int di_IdDcto, String tb_Campo, Object val);

    public boolean insert(Documento dcto);

    public int getDocumentoPorOperacion(String di_Dioperacion);

    public List<Documento> listDocumentoLotePorBarcode(String Barcode);

    public boolean ActualizaEstadoDocumento(int id_documento, int id_estado);
    
    public boolean eliminarDocumento(int id_operacion);
    
}
