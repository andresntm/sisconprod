/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.Date;
import java.util.List;
import sisboj.entidades.BancaTblHitosEnt;
import sisboj.entidades.EstOpeHitoCount;
import sisboj.entidades.OficinaTblHitosEnt;
import sisboj.entidades.RegionTblHitosEnt;
import sisboj.entidades.TblHitosEnt;
import sisboj.entidades.UbicacionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public interface TblHitosDAO {
    
    public List<TblHitosEnt> getListTblHitos(Date fIni, Date fFin,List<UbicacionTblHitosEnt> ubicacion,String oficina, String region,String banca,String rut,int dmi, int dmf, String n_of,String n_op, String producto,String estado,String nSol);
    public List<UbicacionTblHitosEnt> getUbicacionListTblHitos();
    public List<OficinaTblHitosEnt> getOficinaListTblHitos();
    public List<RegionTblHitosEnt> getRegionListTblHitos();
    public List<BancaTblHitosEnt> getBancaListTblHitos();
    public EstOpeHitoCount getCountEstadoOp();
}
