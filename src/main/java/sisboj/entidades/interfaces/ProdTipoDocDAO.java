/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.ProdTipoDoc;

/**
 *
 * @author excosoc
 */
public interface ProdTipoDocDAO {
            
    public void setDataSource(DataSource ds);
   
    public List<ProdTipoDoc> getListProdTipoDoc(String nom);
    
    public boolean updateProdTipoDoc(ProdTipoDoc ptd);
    
    public List<ProdTipoDoc> prodTipoDocExiste(int prod,int doc);
    
    public List<ProdTipoDoc> prodTipoDocExiste2(String prod,String doc);
    
    public List<ProdTipoDoc> selectProdTipoDctoByID(int id);
    
    public List<ProdTipoDoc> getListCountProdTipoDoc();
    
    public boolean insertProdTipoDoc(final ProdTipoDoc prod);
    
    public List<ProdTipoDoc> listDocsObligatoriosPorIdOpe(int idOp);
    
}
