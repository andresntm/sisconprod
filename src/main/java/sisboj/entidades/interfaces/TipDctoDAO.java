/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.TipDcto;


/**
 *
 * @author excosoc
 */
public interface TipDctoDAO {

    public void setDataSource(DataSource ds);

    public TipDcto getTipDcto(int di_IdTipDcto);

    public List<TipDcto> listTipDcto();

    public boolean update(TipDcto tDoc);

    public int insert(final TipDcto tDoc);

    public boolean inactiva_activa(int IdTipDcto,boolean bit_Activo);
    
    public List<TipDcto> listTipoDocsPorCodOpe(String codOpe);
    
    public TipDcto getTipDctoPor(String campo, String valor);
}
