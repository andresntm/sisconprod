/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.DetLote;


/**
 *
 * @author excosoc
 */
public interface DetLoteDao {
    
    public void setDataSource(DataSource ds);
   
    public DetLote getDetLote(int di_IdDetLote);

    public List<DetLote> listDetLote(String SQL);

    public boolean update(int di_IdDetLote, String tb_Campo,Object val);
    public void setJdbc();
    public int insert(DetLote dLot);
    public int insertDetLoteDocument(int id_detlote,int id_documento);
    public boolean setDetLoteRecepcionado(int id_lote);
}
