/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import sisboj.entidades.DevolucionEnt;
import sisboj.entidades.TipoDevolucionEnt;

/**
 *
 * @author excosoc
 */
public interface DevolucionDAO {

   public List<DevolucionEnt> listDevoluciones(int id_operacion);
   
   public List<TipoDevolucionEnt> listTipoDevolucion();
    
}
