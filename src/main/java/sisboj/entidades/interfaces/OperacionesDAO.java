/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.Operaciones;

/**
 *
 * @author excosoc
 */
public interface OperacionesDAO {
    
    /** 
    * This is the method to be used to initialize
    * database resources ie. connection.
    */
   public void setDataSource(DataSource ds);
   /** 
    * This is the method to be used to return
    * database resources ie. connection.
    */
   //public JdbcTemplate getDataSource();
   /** 
    * This is the method to be used to list down
    * a record from the Operacion table corresponding
    * to a passed student id.
    */
         public Operaciones getOperaciones(String dv_CodOperacion);
   
   public List<Operaciones> getOperaciones(int di_fk_IdProduc);
   /** 
    * This is the method to be used to list down
    * all the records from the Operacion table.
    */
   public List<Operaciones> listOperaciones(String SQL);
   /** 
    * This is the method to be used to update
    * a record into the Operacion table.
    */
   public boolean update(String dv_CodOperacion, String tb_Campo,Object val);
    
}
