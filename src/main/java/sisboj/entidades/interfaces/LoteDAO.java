/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.Lote;


/**
 *
 * @author excosoc
 */
public interface LoteDAO {
            
    public void setDataSource(DataSource ds);
   
    public Lote getLote(int di_IdLote);

    public List<Lote> listLote(String SQL);

    public boolean update(int di_IdLote, String tb_Campo,Object val);
    public int getIdNewtLote();
    
    public void setJdbc();
    public int insertLoteCustodia(Lote lot);
    public boolean IsCompleto();
    public boolean IsExisteInLote(String CodBarDocumento);
    public boolean setPickingDocumento(String CodBarDocumento);
    public boolean IsPicking(String CodBarDocumento);
    
    public boolean setAbrirLote(String CodBarLote);
    
    
    public int getCountDocPicking();
     public int getCountDocFaltan();
     public boolean setLoteRecepcionado(int id_lote);
    public boolean IsUltimaOperacion();
    public int insertLoteCustodiaConIdSol(final Lote lot,int idSolicitud);
}
