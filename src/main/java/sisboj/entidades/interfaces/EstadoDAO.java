/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.Estado;

/**
 *
 * @author excosoc
 */
public interface EstadoDAO {
            
    public void setDataSource(DataSource ds);
   
    public Estado getEstado(String codEstado);
    
    public List<Estado> getIdEstadoByCodigo(String codEstado);

    public List<Estado> listEstado(String SQL);

    public boolean update(int di_IdEstado, String tb_Campo,Object val);
    public void setJdbc();
    public Estado getEstadoXNombre(String dv_NomEstado, String dv_DetEstado,String dv_lugar);
}
