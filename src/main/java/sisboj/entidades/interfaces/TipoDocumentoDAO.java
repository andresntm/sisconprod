/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.TipoDocumento;

/**
 *
 * @author exvguba
 */
public interface TipoDocumentoDAO {
            
    public void setDataSource(DataSource ds);
   
    /**
     *
     * @param nom
     * @return List
     */
    public List<TipoDocumento> getListTipoDocumento(String nom);
    
    public boolean updateTipoDocumento(TipoDocumento td);
    
    public List<TipoDocumento> nomTipoDocumentoExiste(String nom);
    
    public List<TipoDocumento> codTipoDocumentoExiste(int cod);
    
    public List<TipoDocumento> selectTipoDocumentoByID(int id);
    
     public List<TipoDocumento> getListTipoDocumentoPorProd(String prod);
     
     public List<TipoDocumento> selectDocumentoPorNombre(String name);
     
     public TipoDocumento selectDocPorNombre(String name);
}
