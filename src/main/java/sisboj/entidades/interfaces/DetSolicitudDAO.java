/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.DetSolicitud;

/**
 *
 * @author excosoc
 */
public interface DetSolicitudDAO {

    public void setDataSource(DataSource ds);

    public DetSolicitud getDetSolicitud(int di_IdDetSol);

    public List<DetSolicitud> listDetSolicitud(String SQL);

//    public boolean update(int di_IdDetSol, String tb_Campo, Object val);
    public boolean insert(DetSolicitud detsol);

    public List<DetSolicitud> getDetSolicitudXSolicitud(int di_fk_IdSolicitud);

    public boolean getDetSolicitud(int di_IdDetSol, String di_fk_IdOper);

    public boolean updateOpeSol(int di_IdSolicitud, String tb_Campo, int val) throws SQLException;

    public boolean updateOpeSolBoj(int di_IdSolicitud, String tb_Campo, int val, String codOpActual) throws SQLException;

    public boolean updateIdSolOperacion(int di_IdSolicitud, String tb_Campo, int val) throws SQLException;

    public boolean updateIdSolOperacionBoj(int di_IdSolicitud, String tb_Campo, int val, String EstdoTipoDev) throws SQLException;

    public boolean updateIdSolOperacion(int di_IdSolicitud, String tb_Campo, int val, String Estdo) throws SQLException;
    
    public boolean updateEstadoOperacion(String newEstado, int idOpe) throws SQLException;
}
