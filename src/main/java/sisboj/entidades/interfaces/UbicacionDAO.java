/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.util.List;
import sisboj.entidades.Ubicacion;

/**
 *
 * @author excosoc
 */
public interface UbicacionDAO {

    public List<Ubicacion> getUbicacion(String param, String val);

}
