/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.Solicitud;

/**
 *
 * @author excosoc
 */
public interface SolicitudDAO {

    public void setDataSource(DataSource ds);

    public Solicitud getSolicitud(int di_IdSolicitud);

    public List<Solicitud> listSolicitud(String SQL);

   public boolean updateSolicitud(int di_IdSolicitud, String tb_Campo, int val);

    public int getNumeroDocumentosSolicitud(int numdoc);

    public boolean CerrarOCambiarEstadoSolicitud(int id_sol, String accion);

    public List<Solicitud> getSolicitudXEstado(int di_fk_IdEstado);

    public int[] getNumeroDocSOLYPEN(int di_IdSolicitud) throws SQLException;

    public boolean cambioEstadoSolDet(int idDetSol, String accion,String glosa) throws SQLException;

    public List<BandejaCustodia> getSolNuev_Pend(String ubi_custodia) throws SQLException;

    public List<BandejaCustodia> getSolTerminadas(String ubi_custodia);

    public List<BandejaCustodia> getSolEnviadas(String ubi_custodia);

    public List<BandejaCustodia> getSolEnBoj(String ubi_custodia);

    public boolean EnviarBojSolicitud(int id_sol);
    
    public List<BandejaCustodia> getSolPendientes(String ubi_custodia) throws SQLException;
    
    public Solicitud InsertHijoSol(Solicitud newSol);
    
    public int getNumeroInhporSols(int di_IdSolicitud)throws SQLException;
    
    
    
}
