/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.interfaces;

import sisboj.entidades.RutasLdap;
import java.util.List;

/**
 *
 * @author EXVGUBA
 */
public interface LdapDAO {
    
   public List<RutasLdap> listRutasLdap();
}
