/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MetodosGenerales;
import config.MvcConfig;
import configuracion.SisBojConf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.BandejaLoteMapper;
import sisboj.RowMapper.LoteMapper;
import sisboj.RowMapper.principalMapper;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaLote;
import sisboj.entidades.Documento;
import sisboj.entidades.Lote;
import sisboj.entidades.Solicitud;
import sisboj.entidades.interfaces.LoteDAO;


/**
 *
 * @author excosoc
 */
public class LoteJDBC implements LoteDAO {

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private MetodosGenerales metodo;
    private SisBojConf _bojConfig;
    public Lote lot;
    public  List<Documento> docList;
    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        lot=new Lote();
    }
    public LoteJDBC(){
    _bojConfig=new SisBojConf();
    
    }
    
    @Override
    public int  getIdNewtLote() {
        int id_max_lote=0;
              String SQL = "SELECT CASE \n"
                + "           WHEN    (MAX(di_IdLote)>1) --ya tiene registros la base\n"
                + "               THEN \n"
                + "                   (MAX(di_IdLote))\n"
                + "               ELSE --Primer hito generado\n"
                + "                   0\n"
                + "               END IdSolicitud\n"
                + "FROM tb_Sys_Lote";

       id_max_lote = jdbcTemplateObject.queryForInt(SQL);
        return id_max_lote;
    }



    public Lote getLot() {
        return lot;
    }

    public void setLot(Lote lot) {
        this.lot = lot;
    }



    public void Insert(Solicitud newSol) { 
        metodo = new MetodosGenerales();
        String insertSol = "";
        Timestamp tS = metodo.ConvStringDate(newSol.getDdt_FechaCreacion());
        try {
            insertSol = "INSERT INTO [dbo].[tb_Sys_Solicitud]\n"
                    + "           ([di_IdSolicitud]\n"
                    + "           ,[di_fk_IdUbi]\n"
                    + "           ,[di_fk_IdHito]\n"
                    + "           ,[di_fk_IdEstado]\n"
                    + "           ,[ddt_FechaCreacion])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,(CONVERT(datetime,?,101)))";

            jdbcTemplateObject.update(insertSol, new Object[]{newSol.getDi_IdSolicitud(),
                 newSol.getDi_fk_IdUbi(),
                 newSol.getDi_fk_IdHito(),
                 newSol.getDi_fk_IdEstado(),
                 tS
            });

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
        }
    }



    public List<BandejaCustodia> listLote(int proceso) {
        String SQL = "";
        if (proceso == 1) {
            SQL = "SELECT di_IdSolicitud,ubi.dv_DetUbi,est.dv_NomEstado,count(dsol.di_iddetsol) cantidad,dhit.di_IdHito,pro.dv_NomProduc\n"
                    + "FROM tb_Sys_Solicitud sol \n"
                    + "left outer join tb_Sys_DetSolicitud dsol \n"
                    + "on dsol.di_fk_IdSolicitud = sol.di_IdSolicitud \n"
                    + "inner join tb_Sys_DetHito dhit \n"
                    + "on dhit.di_IdDetHito = dsol.di_fk_IdDetHito \n"
                    + "inner join tb_sys_producto pro \n"
                    + "on pro.di_IdProduc = dhit.di_fk_IdProduc \n"
                    + "inner join tb_Sys_Estado est \n"
                    + "on est.di_IdEstado = sol.di_fk_IdEstado \n"
                    + "inner join tb_Sys_Ubicacion ubi\n"
                    + "on ubi.di_IdUbicacion = sol.di_fk_IdUbi\n"
                    + "WHERE (sol.di_fk_IdEstado = 130 OR sol.di_fk_IdEstado = 131) \n"
                    + "AND (dsol.di_fk_IdDcto is null)\n";
            SQL += "GROUP BY di_IdSolicitud,est.dv_NomEstado,di_IdHito,pro.dv_NomProduc,ubi.dv_DetUbi";
        }

        /*else if (proceso == 2){
        
        
        }
        else if (proceso == 3){
        
        }*/
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }
    
    
    
    public List<BandejaLote> listLotes(int proceso, String ubi_custodia) {
        String SQL = "";


     if (proceso == 4) {
                    SQL = "select lot.dv_CodBarra,ubi.dv_DetUbi,est.dv_NomEstado,count(dlot.di_fk_IdLote)cantidad\n"
                    +"from tb_Sys_Lote lot \n"
                    +"left outer join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                    +"inner join tb_Sys_Estado est on est.di_IdEstado = lot.di_fk_IdEstado \n"
                    +"inner join tb_Sys_Ubicacion ubi on ubi.di_IdUbicacion = lot.di_IdUbi_Salida \n"
                    +"where lot.di_fk_IdEstado in (269,272)  \n"
                    +"GROUP BY lot.dv_CodBarra,est.dv_NomEstado,ubi.dv_DetUbi";
                            }
     if (proceso == 5) {
                    SQL = "select lot.dv_CodBarra,ubi.dv_DetUbi,est.dv_NomEstado,count(dlot.di_fk_IdLote)cantidad\n"
                    +"from tb_Sys_Lote lot \n"
                    +"left outer join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                    +"inner join tb_Sys_Estado est on est.di_IdEstado = lot.di_fk_IdEstado \n"
                    +"inner join tb_Sys_Ubicacion ubi on ubi.di_IdUbicacion = lot.di_IdUbi_Salida \n"
                    +"where lot.di_fk_IdEstado = 270  \n"
                    +"GROUP BY lot.dv_CodBarra,est.dv_NomEstado,ubi.dv_DetUbi";
                            }
       _bojConfig.print("ListLotes linea 226["+SQL+"]");
        List<BandejaLote> dsol;
        dsol = jdbcTemplateObject.query(SQL,new BandejaLoteMapper());

        return dsol;
    }
    public List<BandejaCustodia> listSolicitud2(int proceso, String ubi_custodia) {
        String SQL = "";
        if("ADMIN".equals(ubi_custodia)){
        ubi_custodia=" in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        }else {
        ubi_custodia=" ='"+ubi_custodia+"'";
        }
        if (proceso == 1) {
            SQL = "SELECT di_IdSolicitud,ubi.dv_DetUbi,est.dv_NomEstado,count(dsol.di_iddetsol) cantidad,dhit.di_IdHito,pro.dv_NomProduc\n"
                    + "FROM tb_Sys_Solicitud sol \n"
                    + "left outer join tb_Sys_DetSolicitud dsol \n"
                    + "on dsol.di_fk_IdSolicitud = sol.di_IdSolicitud \n"
                    + "inner join tb_Sys_DetHito dhit \n"
                    + "on dhit.di_IdDetHito = dsol.di_fk_IdDetHito \n"
                    + "inner join tb_sys_producto pro \n"
                    + "on pro.di_IdProduc = dhit.di_fk_IdProduc \n"
                    + "inner join tb_Sys_Estado est \n"
                    + "on est.di_IdEstado = sol.di_fk_IdEstado \n"
                    + "inner join tb_Sys_Ubicacion ubi\n"
                    + "on ubi.di_IdUbicacion = sol.di_fk_IdUbi\n"
                    + "WHERE (sol.di_fk_IdEstado = 130 OR sol.di_fk_IdEstado = 131) \n"
                    + "AND (dsol.di_fk_IdDcto is null)\n"
                    + "AND (ubi.dv_DetUbi " + ubi_custodia + " )\n";
            SQL += "GROUP BY di_IdSolicitud,est.dv_NomEstado,di_IdHito,pro.dv_NomProduc,ubi.dv_DetUbi";
        }

        if (proceso == 2) {
            SQL = "SELECT di_IdSolicitud,ubi.dv_DetUbi,est.dv_NomEstado,count(dsol.di_iddetsol) cantidad,dhit.di_IdHito,pro.dv_NomProduc\n"
                    + "FROM tb_Sys_Solicitud sol \n"
                    + "left outer join tb_Sys_DetSolicitud dsol \n"
                    + "on dsol.di_fk_IdSolicitud = sol.di_IdSolicitud \n"
                    + "inner join tb_Sys_DetHito dhit \n"
                    + "on dhit.di_IdDetHito = dsol.di_fk_IdDetHito \n"
                    + "inner join tb_sys_producto pro \n"
                    + "on pro.di_IdProduc = dhit.di_fk_IdProduc \n"
                    + "inner join tb_Sys_Estado est \n"
                    + "on est.di_IdEstado = sol.di_fk_IdEstado \n"
                    + "inner join tb_Sys_Ubicacion ubi\n"
                    + "on ubi.di_IdUbicacion = sol.di_fk_IdUbi\n"
                    + "WHERE (sol.di_fk_IdEstado = 132 OR sol.di_fk_IdEstado = 131) \n"
                    + "AND (dsol.di_fk_IdDcto is null)\n"
                    + "AND (ubi.dv_DetUbi " + ubi_custodia + " )\n";
            SQL += "GROUP BY di_IdSolicitud,est.dv_NomEstado,di_IdHito,pro.dv_NomProduc,ubi.dv_DetUbi";
        }

        /*else if (proceso == 2){
        
        
        }
        else if (proceso == 3){
        
        }*/
      //  Messagebox.show("Quey[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<Solicitud> CreaNuevasLote() {
        List<Solicitud> sol = new ArrayList<Solicitud>();
        String SQL = "";

        SQL = "SELECT DISTINCT(di_fk_IdProduc) di_fk_Produc,\n"
                + //Id producto
                "di_fk_IdUbi,\n"
                + //Ubicacion
                "di_IdHito di_fk_IdHito,\n"
                + //Id del encabezado Hito
                "130 di_fk_IdEstado \n"
                + "FROM		tb_Sys_DetHito dethit\n"
                + "WHERE		dethit.di_IdDetHito not in(SELECT di_fk_IdDetHito FROM tb_Sys_DetSolicitud)"
                + "ORDER BY           di_IdHito";

        try {
            sol = jdbcTemplateObject.query(SQL,
                    new RowMapper() {

                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Solicitud mapSol = new Solicitud();
                    mapSol.setDi_fk_Prod(rs.getInt("di_fk_Produc"));
                    mapSol.setDi_fk_IdUbi(rs.getInt("di_fk_IdUbi"));
                    mapSol.setDi_fk_IdHito(rs.getInt("di_fk_IdHito"));
                    mapSol.setDi_fk_IdEstado(rs.getInt("di_fk_IdEstado"));
                    return mapSol;
                }
            });

        } catch (Exception e) {
            Messagebox.show(e.toString());

        }

        return sol;
    }

    public Lote getLote(int di_IdLote) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Lote> listLote(String SQL) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean update(int di_IdLote, String tb_Campo, Object val) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
        @Override
    public void setJdbc() {
        try {
            this.dataSource = new MvcConfig().getDataSourceSisBoj();
        } catch (SQLException ex) {
            Logger.getLogger(LoteJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }
    
    
       @Override
    public int insertLoteCustodia(final Lote lot) {
        final String insertDetSol = "INSERT INTO [dbo].[tb_Sys_Lote]\n"
                + "           ([di_CantOper]	   \n"
                + "           ,[dv_CodBarra]	   \n"
                + "           ,[di_IdUbi_Salida]   \n"
                + "           ,[di_fk_IdEstado]    \n"
                + "           ,[ddt_FechaCreacion] \n"
                + "           ,[dv_GlosaLote]	   \n"
                + "           ,[di_CantDctos]	   \n"
                + "           ,[dv_CodLote]	   \n"
                + "           ,[di_IdUbi_Llegada])	   \n"
                + "     VALUES   \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,(CONVERT(datetime,?,101)) \n"
                + "           ,? \n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)\n";    
        
        
        
        String Simulate="INSERT INTO [dbo].[tb_Sys_Lote]\n"
                + "           ([di_CantOper]	   \n"
                + "           ,[dv_CodBarra]	   \n"
                + "           ,[di_IdUbi_Salida]   \n"
                + "           ,[di_fk_IdEstado]    \n"
                + "           ,[ddt_FechaCreacion] \n"
                + "           ,[dv_GlosaLote]	   \n"
                + "           ,[di_CantDctos])	   \n"
                + "           ,[dv_CodLote])	   \n"
                + "           ,[di_IdUbi_Llegada])	   \n"                
                + "     VALUES   \n"
                + "           ( "+lot.getDi_CantOper()+" \n"
                + "           ,"+lot.getDv_CodBarra()+" \n"
                + "           ,"+lot.getDi_IdUbi_Salida()+" \n"
                + "           ,"+lot.getDi_fk_IdEstado()+" \n"
                + "           ,(CONVERT(datetime,"+lot.getDdt_FechaCreacion()+",101)) \n"
                + "           ,"+lot.getDv_GlosaLote()+" \n"
                + "           ,"+lot.getDi_CantDctos()+"\n"
                + "           ,'CodLote') \n"
                + "           ,'1') \n";        
       // Messagebox.show("QuerySimulate:["+Simulate+"]");
          _bojConfig.print("ListLotes linea 226["+Simulate+"]");
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"di_IdLote"});
                    ps.setInt(1, lot.getDi_CantOper());
                    ps.setString(2,  lot.getDv_CodBarra());// lot.getDv_CodBarra());
                    ps.setInt(3, lot.getDi_IdUbi_Salida());
                    ps.setInt(4, lot.getDi_fk_IdEstado());
                    ps.setString(5, lot.getDdt_FechaCreacion());
                    ps.setString(6, lot.getDv_GlosaLote());// lot.getDv_GlosaLote());
                    ps.setInt(7, lot.getDi_CantDctos());
                    ps.setString(8, "CodLote");
                    ps.setInt(9,1);
                    //logger.debug(sql + " " + Arrays.asList(values));
                    return ps;
                }
            }, key);

            return key.getKey().intValue();
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return -1;
        }
    }

    
    
          @Override
    public int insertLoteCustodiaConIdSol(final Lote lot,final int idSolicitud) {
        final String insertDetSol = "INSERT INTO [dbo].[tb_Sys_Lote]\n"
                + "           ([di_CantOper]	   \n"
                + "           ,[dv_CodBarra]	   \n"
                + "           ,[di_IdUbi_Salida]   \n"
                + "           ,[di_fk_IdEstado]    \n"
                + "           ,[ddt_FechaCreacion] \n"
                + "           ,[dv_GlosaLote]	   \n"
                + "           ,[di_CantDctos]	   \n"
                + "           ,[dv_CodLote]	   \n"
                + "           ,[di_fk_IdSolicitud]	   \n"
                + "           ,[di_IdUbi_Llegada])	   \n"
                + "     VALUES   \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,(CONVERT(datetime,?,101)) \n"
                + "           ,? \n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"  
                + "           ,?)\n";   
     
        
        
        String Simulate="INSERT INTO [dbo].[tb_Sys_Lote]\n"
                + "           ([di_CantOper]	   \n"
                + "           ,[dv_CodBarra]	   \n"
                + "           ,[di_IdUbi_Salida]   \n"
                + "           ,[di_fk_IdEstado]    \n"
                + "           ,[ddt_FechaCreacion] \n"
                + "           ,[dv_GlosaLote]	   \n"
                + "           ,[di_CantDctos])	   \n"
                + "           ,[dv_CodLote])	   \n"
                + "           ,[di_IdUbi_Llegada])	   \n"                
                + "     VALUES   \n"
                + "           ( "+lot.getDi_CantOper()+" \n"
                + "           ,"+lot.getDv_CodBarra()+" \n"
                + "           ,"+lot.getDi_IdUbi_Salida()+" \n"
                + "           ,"+lot.getDi_fk_IdEstado()+" \n"
                + "           ,(CONVERT(datetime,"+lot.getDdt_FechaCreacion()+",101)) \n"
                + "           ,"+lot.getDv_GlosaLote()+" \n"
                + "           ,"+lot.getDi_CantDctos()+"\n"
                + "           ,'CodLote') \n"
                + "           ,'1') \n";        
       // Messagebox.show("QuerySimulate:["+Simulate+"]");
          _bojConfig.print("ListLotes linea 226["+Simulate+"]");
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"di_IdLote"});
                    ps.setInt(1, lot.getDi_CantOper());
                    ps.setString(2,  lot.getDv_CodBarra());// lot.getDv_CodBarra());
                    ps.setInt(3, lot.getDi_IdUbi_Salida());
                    ps.setInt(4, lot.getDi_fk_IdEstado());
                    ps.setString(5, lot.getDdt_FechaCreacion());
                    ps.setString(6, lot.getDv_GlosaLote());// lot.getDv_GlosaLote());
                    ps.setInt(7, lot.getDi_CantDctos());
                    ps.setString(8, "CodLote");
                     ps.setInt(9,idSolicitud);
                    ps.setInt(10,1);
                   
                    //logger.debug(sql + " " + Arrays.asList(values));
                    return ps;
                }
            }, key);

            return key.getKey().intValue();
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return -1;
        }
    }

    
     
           @Override
    public boolean IsCompleto(){
    int _numTotoalDocumentos=0;
    int _numTotoalDocumentosEnTransito=0;
    int _numActualDocumentosPickeados=0;
               String SQLtramsito = "select count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"' and Doc.di_fk_IdEstado=160 ) as resumen ";
 _bojConfig.print("DocJDBC linea 96["+SQLtramsito+"]");
       _numTotoalDocumentosEnTransito = jdbcTemplateObject.queryForInt(SQLtramsito);
       
       this.lot.setN_DocumentosEntransito(_numTotoalDocumentosEnTransito);
               String SQLpikeados = "select count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"' and Doc.di_fk_IdEstado=176 ) as resumen ";       
       
        _bojConfig.print("DocJDBC linea 96["+SQLpikeados+"]");
      _numActualDocumentosPickeados= jdbcTemplateObject.queryForInt(SQLpikeados);
      
      this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);

      
      if(_numTotoalDocumentosEnTransito>0)    return false;  
 else return true;
 
 
 
 
 //if(_numActualDocumentosPickeados>0)    return false;
 
 
 
   //     return false;
    }
             @Override
    public boolean IsExisteInLote(String CodBarDocumento){
    int _numDocumentos=0;

               String SQLtramsito = "select  count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "inner join tb_Sys_Operacion ope on ope.di_IdOperacion=doc.di_fk_IdOper \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"' and ope.dv_CodOperacion='"+CodBarDocumento+"' ) as resumen ";

       _numDocumentos = jdbcTemplateObject.queryForInt(SQLtramsito); 

      
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos>0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;
    }
              @Override
    public boolean IsPicking(String CodBarDocumento){
    int _numDocumentos=0;

               String SQLtramsito = "select  count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "inner join tb_Sys_Operacion ope on ope.di_IdOperacion=doc.di_fk_IdOper \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"' and ope.dv_CodOperacion='"+CodBarDocumento+"'  and Doc.di_fk_IdEstado=170  ) as resumen ";

       _numDocumentos = jdbcTemplateObject.queryForInt(SQLtramsito); 

      
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos>0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;
    }   
    
              @Override
    public boolean setPickingDocumento(String CodBarDocumento){
          int codope=0;
        String BuscaOper="Select ope.di_IdOperacion from tb_Sys_Operacion ope where ope.dv_CodOperacion='"+CodBarDocumento+"'";
        
        
        try {
        codope = jdbcTemplateObject.queryForInt(BuscaOper); 
           // return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }
        
        
        String SQL = "UPDATE tb_Sys_Documento "
                + "SET    di_fk_IdEstado           = 170 \n"

                + "WHERE  di_fk_IdOper         = "+codope;

              try {
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }

    }
    
    
     @Override
        public boolean setAbrirLote(String CodBarLote){

        
        
        String SQL = "UPDATE tb_Sys_Lote "
                + "SET    di_fk_IdEstado           = 269 \n"

                + "WHERE  dv_CodBarra         = '"+CodBarLote+"'";

        
        _bojConfig.print("SetLoteEstado269 linea 505["+SQL+"]");
        
        
              try {
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }

    }
  
        @Override
        public boolean setLoteRecepcionado(int id_lote){

        
        
        String SQL = "UPDATE tb_Sys_Lote "
                + "SET    di_fk_IdEstado           = 270 \n"

                + "WHERE  di_IdLote         = '"+id_lote+"'";

             
      _bojConfig.print("setLoteRecepcionado linea 575["+SQL+"]");
        try {
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }

    }     
        
              @Override
    public int getCountDocPicking(){
    int _numDocumentosPickeados=0;

               String SQLtramsito = "select  count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "inner join tb_Sys_Operacion ope on ope.di_IdOperacion=doc.di_fk_IdOper \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"'  and Doc.di_fk_IdEstado=170  ) as resumen ";

       _numDocumentosPickeados = jdbcTemplateObject.queryForInt(SQLtramsito); 

      
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentosPickeados;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;
    }       
                @Override
    public int getCountDocFaltan(){
    int _numDocumentosPickeados=0;

               String SQLtramsito = "select  count(*) from ( \n"
                       + "select tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                       + "from tb_Sys_Lote lot \n"
                       + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                       + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                       + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                       + "inner join tb_Sys_Operacion ope on ope.di_IdOperacion=doc.di_fk_IdOper \n"
                       + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                       + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                       + "where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"'  and Doc.di_fk_IdEstado=160  ) as resumen ";
 _bojConfig.print("DocJDBC linea 96["+SQLtramsito+"]");
       _numDocumentosPickeados = jdbcTemplateObject.queryForInt(SQLtramsito); 

      
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentosPickeados;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;
    } 
    
    
                 @Override
    public boolean IsUltimaOperacion(){
    int _numOperacions=0;

    
    	           String SQLtramsito =      "select  count(*) from ( \n"
                    +"select DISTINCT   ope.di_IdOperacion \n"
                    +"from tb_Sys_Lote lot \n"
                    +"inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                    +"Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                    +"inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                    +"inner join tb_Sys_Operacion ope on ope.di_IdOperacion=doc.di_fk_IdOper \n"
                    +"left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                    +"left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                    +"where lot.dv_CodBarra='"+this.lot.getDv_CodBarra()+"'  and Doc.di_fk_IdEstado=160  ) as resumen  ";
    

 _bojConfig.print("DocJDBC linea 96["+SQLtramsito+"]");
       _numOperacions = jdbcTemplateObject.queryForInt(SQLtramsito); 

      
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
       // return _numDocumentosPickeados;
        return _numOperacions==1;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;
    }    
    
    public boolean CargaLote(String CodBarra) {
        String SQL = "";
	
                            
	             SQL = "select lot.di_IdLote,lot.dv_CodBarra,lot.di_fk_IdEstado,lot.ddt_FechaCreacion\n"
                    +"from tb_Sys_Lote lot \n"
		    +"where lot.dv_CodBarra='"+CodBarra+"' ";
        
     //    Lote dsol=new Lote();
        
       _bojConfig.print("CargaLote linea 619["+SQL+"]");
       List<Lote> lott= jdbcTemplateObject.query(SQL,new LoteMapper());
       lot =lott.get(0);
          
        return true;
    }  
    
    
    
    
        public String getCodiGoLote(int idSolicitud) {
        String SQL = "";
	


                     
                     		SQL="select lot.di_IdLote,lot.dv_CodBarra,lot.di_fk_IdEstado,lot.ddt_FechaCreacion from tb_Sys_Lote lot \n" 
				+"inner join tb_Sys_Solicitud sol on sol.di_IdSolicitud=lot.di_fk_IdSolicitud \n"
				+" where sol.di_IdSolicitud="+idSolicitud;
                     
     //    Lote dsol=new Lote();
        
       _bojConfig.print("CargaLote linea 732["+SQL+"]");
       List<Lote> lott= jdbcTemplateObject.query(SQL,new LoteMapper());
       //lot =
          
        return lott.get(0).getDv_CodBarra();
    }   
    
}    
    
    

