/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.TipoDctoMapper;
import sisboj.entidades.TipDcto;
import sisboj.entidades.interfaces.TipDctoDAO;

/**
 *
 * @author excosoc
 */
public class TipDctoJDBC implements TipDctoDAO {

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    TipDcto tDoc;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public TipDctoJDBC() {
    }

    public TipDctoJDBC(DataSource dS) throws SQLException {
        setDataSource(dS);
        tDoc = new TipDcto();
    }

    @Override
    public TipDcto getTipDcto(int di_IdTipDcto) {

        try {
            String SQL = "SELECT TSTD.*\n"
                    + "FROM TB_SYS_TIPODCTO TSTD\n"
                    + "WHERE TSTD.DI_IDTIPDCTO = ?;";
            tDoc = jdbcTemplateObject.queryForObject(SQL, new Object[]{di_IdTipDcto}, new TipDcto());

        } catch (DataAccessException ex) {
            tDoc = null;
        } catch (SQLException ex) {
            Logger.getLogger(TipDctoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tDoc;
    }

    @Override
    public List<TipDcto> listTipDcto() {
        List<TipDcto> tDoc = new ArrayList<TipDcto>();
        try {
            String SQL = "SELECT *\n"
                    + "FROM TB_SYS_TIPODCTO TSTD;";
            tDoc = jdbcTemplateObject.query(SQL, new TipoDctoMapper());
        } catch (DataAccessException ex) {
            tDoc = null;
        }
        return tDoc;
    }

    @Override
    public boolean update(TipDcto tDoc) {
        String SQL = "UPDATE DBO.TB_SYS_TIPODCTO\n"
                + "  SET\n"
                + "      DV_NOMTIPDCTO = ?, -- VARCHAR\n"
                + "      DI_IDTIPDCTOPJ = ? -- INT\n"
                + "WHERE DI_IDTIPDCTO = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tDoc.getDv_NomTipDcto(),
                tDoc.getDi_IdTipDctoPJ(),
                tDoc.getDi_IdTipDcto()

            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    @Override
    public int insert(final TipDcto tDoc) {
        KeyHolder key = new GeneratedKeyHolder();
        final String SQL = "INSERT INTO DBO.TB_SYS_TIPODCTO\n"
                + "(\n"
                + "    --DI_IDTIPDCTO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "    DV_NOMTIPDCTO,\n"
                + "    DI_IDTIPDCTOPJ\n"
                + "    --DDT_FECINGRESO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "    --BIT_ACTIVO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "    -- DI_IDTIPDCTO - INT\n"
                + "    '', -- DV_NOMTIPDCTO - VARCHAR\n"
                + "    0, -- DI_IDTIPDCTOPJ - INT\n"
                + "    -- DDT_FECINGRESO - DATETIME\n"
                + "    -- BIT_ACTIVO - BIT\n"
                + ");";

        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"di_IdTipDcto"});

                    ps.setString(1, tDoc.getDv_NomTipDcto());
                    ps.setInt(2, tDoc.getDi_IdTipDctoPJ());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            Messagebox.show("SQL Exception: " + ex.toString());
            return 0;
        }
    }

    @Override
    public boolean inactiva_activa(int IdTipDcto, boolean bit_Activo) {
        final String SQL = "UPDATE DBO.TB_SYS_TIPODCTO\n"
                + "  SET BIT_ACTIVO = ? --BIT\n"
                + "WHERE DI_IDTIPDCTO = ?;";

        try {

            jdbcTemplateObject.update(SQL, new Object[]{bit_Activo, IdTipDcto});

            return true;
        } catch (DataAccessException ex) {
            return false;
        }
    }

    public List<TipDcto> listTipoDocsPorCodOpe(String codOpe) {

        String SQL = "SELECT td.*\n"
                + "from dbo.tb_Sys_TipoDcto td\n"
                + "where td.di_IdTipDcto not in(select doc.di_fk_TipoDcto \n"
                + "                               from tb_Sys_Documento doc \n"
                + "							  where doc.di_fk_IdOper in (select ope.di_IdOperacion \n"
                + "							                               from TB_SYS_OPERACION ope \n"
                + "														  where ope.dv_CodOperacion ='" + codOpe + "'\n"
                + "														 )\n"
                + "                             )";

        System.out.println(" TipDctoJDBC.listDocsPorCodOpe => [" + SQL + " ]");
        List<TipDcto> tipoDoc = jdbcTemplateObject.query(SQL, new TipoDctoMapper());
        return tipoDoc;
    }

    public TipDcto getTipDctoPor(String campo, String valor) {
        try {
            String SQL = "SELECT TSTD.*\n"
                    + "FROM TB_SYS_TIPODCTO TSTD\n"
                    + "WHERE " + campo + " = " + valor;

            System.out.println(" TipDctoJDBC.getTipDctoPor => [" + SQL + " ]");
            tDoc = jdbcTemplateObject.queryForObject(SQL, new TipoDctoMapper());

        } catch (DataAccessException ex) {
            tDoc = null;
        }

        return tDoc;
    }

}
