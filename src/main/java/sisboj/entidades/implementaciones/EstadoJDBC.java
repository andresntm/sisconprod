/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.EstadoMapper;
import sisboj.entidades.Estado;
import sisboj.entidades.interfaces.EstadoDAO;



/**
 *
 * @author excosoc
 */
public class EstadoJDBC implements EstadoDAO{

    
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    
    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
//        this.setJdbcTemplateObject(new JdbcTemplate(getDataSource()));
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public Estado getEstado(String codEstado) {
        String SQL = "select * from tb_Sys_Estado  where dv_CodEstado = ?";
        
         Estado est =jdbcTemplateObject.queryForObject(SQL,new Object[]{codEstado},new EstadoMapper());
        return est;
    }

    @Override
    public List<Estado> listEstado(String Estado) {
        String SQL = "select * from tb_Sys_Estado  where dv_DetEstado = ?";
        
        List <Estado> est = jdbcTemplateObject.query(SQL,new Object[]{Estado},new EstadoMapper());
        return est;
   
    }

    @Override
    public boolean update(int di_IdEstado, String tb_Campo, Object val) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * @return the dataSource
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * @return the jdbcTemplateObject
     */
    public JdbcTemplate getJdbcTemplateObject() {
        return jdbcTemplateObject;
    }

    /**
     * @param jdbcTemplateObject the jdbcTemplateObject to set
     */
    public void setJdbcTemplateObject(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject = jdbcTemplateObject;
    }
    
    
        @Override
    public void setJdbc() {
        try {
            this.dataSource = new MvcConfig().getDataSourceSisBoj();
            this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        } catch (SQLException ex) {
            Logger.getLogger(EstadoJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
        @Override
    public Estado getEstadoXNombre(String dv_NomEstado, String dv_DetEstado,String dv_lugar) {
        Estado est = new Estado();
        try {
            String SQL = "SELECT * FROM tb_Sys_Estado "
                    + "WHERE    dv_NomEstado    = ? "
                    + "AND      dv_DetEstado    = ? "
                    + "AND      dv_lugar        = ? ";
            est = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{
                        dv_NomEstado,
                        dv_DetEstado,
                        dv_lugar
                    }, new EstadoMapper());

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return est;
    }

    public List<Estado> getIdEstadoByCodigo(String codEstado) {
        List<Estado> LEstado = new ArrayList<Estado>();
        System.out.println("EstadoJDBC.getIdEstadoByCodigo codEstado => "+codEstado);
         String SQL = "select * from tb_Sys_Estado  where dv_CodEstado ='"+codEstado+"'";        
         LEstado = this.jdbcTemplateObject.query(SQL, new EstadoMapper());
        return LEstado;
    }
    
    
    
    
}
