/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.UsuariosLdapMapper;
import sisboj.entidades.UsuariosLdapEnt;
import sisboj.entidades.interfaces.UsuariosLdapDAO;



/**
 *
 * @author EXVGUBA
 */
public class UsuariosLdapJDBC implements UsuariosLdapDAO{
    
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

   

    public List<UsuariosLdapEnt> listUsuariosLdap() {
        List<UsuariosLdapEnt> lUsuLdap = new ArrayList<UsuariosLdapEnt>();
        try {
            String SQL = "SELECT * FROM tbl_usuarios_ldap";
             lUsuLdap = jdbcTemplateObject.query(SQL, new UsuariosLdapMapper());             
             System.out.println(" lUsuLdap Cant. => "+lUsuLdap.size());
             
        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return lUsuLdap;
    }
    
    
    
}
