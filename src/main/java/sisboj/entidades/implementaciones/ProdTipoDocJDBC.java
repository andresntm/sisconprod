/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.RowMapper.CountProdTipoDocMapper;
import sisboj.RowMapper.ProdTipoDocMapper;
import sisboj.RowMapper.ProductosMapper;
import sisboj.entidades.ProdTipoDoc;
import sisboj.entidades.Productos;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.interfaces.ProdTipoDocDAO;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class ProdTipoDocJDBC implements ProdTipoDocDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private ProductosJDBC productosJDBC;
    private TipoDocumentoJDBC documentoJDBC;
    int obligatorio = 0;
    int activo = 0;

    public ProdTipoDocJDBC() throws SQLException {
        productosJDBC = new MvcConfig().productosJDBC();
        documentoJDBC = new MvcConfig().tipoDocumentoJDBC();
    }

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
//        this.setJdbcTemplateObject(new JdbcTemplate(getDataSource()));
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public List<ProdTipoDoc> getListProdTipoDoc(String nom) {

        String donde = "";
        if (!"".equals(nom) && nom != null) {
            donde = " where LOWER(dv_NomProduc) like '%" + nom.toLowerCase() + "%'";
        }

        String SQL = "select ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo,ptd.obligatorio  from dbo.tb_sys_ProductoTipoDocumento ptd\n"
                + "	inner join dbo.tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto\n"
                + "	inner join dbo.tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + donde + "\n"
                + "ORDER BY pr.dv_NomProduc ASC";

        System.out.println(" getListProdTipoDoc => " + SQL);
        List<ProdTipoDoc> ProdTipoDoc = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());
        return ProdTipoDoc;

    }

    /**
     * devuelve > 0 si producto ya existe
     *
     * @param prod
     * @param doc
     * @return List<ProdTipoDoc>
     */
    public List<ProdTipoDoc> prodTipoDocExiste(int prod, int doc) {
        String SQL = "select ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo, ptd.obligatorio  from dbo.tb_sys_ProductoTipoDocumento ptd\n"
                + "	inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto\n"
                + "	inner join tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + "	where  pr.di_IdProduc=" + prod + ""
                + "     AND td.di_IdTipDcto=" + doc;

        List<ProdTipoDoc> productos = new ArrayList<ProdTipoDoc>();
        try {
            productos = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());

        } catch (DataAccessException e) {
            e.getMessage();
        }

        return productos;
    }

    /**
     * devuelve > 0 si producto ya existe
     *
     * @param prod
     * @param doc
     * @return List<ProdTipoDoc>
     */
    public List<ProdTipoDoc> prodTipoDocExiste2(String prod, String doc) {
        String SQL = "select ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo, ptd.obligatorio  from dbo.tb_sys_ProductoTipoDocumento ptd\n"
                + "	inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto\n"
                + "	inner join tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + "	where  pr.dv_NomProduc=" + prod + ""
                + "     AND td.dv_NomTipDcto=" + doc;

        List<ProdTipoDoc> productos = new ArrayList<ProdTipoDoc>();
        try {
            productos = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());

        } catch (DataAccessException e) {
            e.getMessage();
        }

        return productos;
    }

    //Insert Tipo de documento por producto
    public boolean insertProdTipoDoc(final ProdTipoDoc prod) {

        final String SQL = "INSERT INTO [dbo].[tb_sys_ProductoTipoDocumento]\n"
                + "           ([fk_idProducto]\n"
                + "           ,[fk_idTipoDocto]\n"
                + "           ,[bit_Activo]\n"
                + "           ,[obligatorio])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            List<Productos> producto = productosJDBC.selectProductoByName(prod.getDv_NomProduc());
            List<TipoDocumento> documento = documentoJDBC.selectDocumentoPorNombre(prod.getDv_NomTipDcto());

            final int idprod = producto.get(0).getDi_IdProducto();
            final int iddoc = documento.get(0).getDi_IdTipDcto();

            System.out.println("producto.get(0).getDi_IdProducto() => " + idprod);
            System.out.println("int iddoc = documento.get(0).getDi_fk_TipoDcto() => " + iddoc);

            if ("Activo".equals(prod.getBit_activo())) {
                activo = 1;
            }
            if ("Inactivo".equals(prod.getBit_activo())) {
                activo = 0;
            }

            if ("SI".equals(prod.getObligatorio())) {
                obligatorio = 1;
            }
            if ("NO".equals(prod.getObligatorio())) {
                obligatorio = 0;
            }

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_prodtipdoc"});

                    ps.setInt(1, idprod);
                    ps.setInt(2, iddoc);
                    ps.setInt(3, activo);
                    ps.setInt(4, obligatorio);
                    return ps;
                }
            },
                    keyHolder);
            return true;

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            return false;
        }
    }

    //Update Producto
    public boolean updateProdTipoDoc(ProdTipoDoc prod) {
        String SQL = "UPDATE [dbo].[tb_sys_ProductoTipoDocumento]\n"
                + "   SET [fk_idProducto] = ?\n"
                + "      ,[fk_idTipoDocto] = ?\n"
                + "      ,[bit_Activo] = ?\n"
                + "      ,[obligatorio] = ?\n"
                + " WHERE id_prodtipdoc = ?";
        System.out.println("tb_sys_ProductoTipoDocumento SQL => " + SQL);

        int est = 0;
        int oblig = 0;
        if ("Activo".equals(prod.getBit_activo())) {
            est = 1;
        }
        if ("Inactivo".equals(prod.getBit_activo())) {
            est = 0;
        }
        if ("SI".equals(prod.getObligatorio())) {
            oblig = 1;
        } else {
            oblig = 0;
        }

        try {
            this.jdbcTemplateObject.update(SQL, new Object[]{
                prod.getDi_IdProduc(),
                prod.getDi_IdTipDcto(),
                est,
                oblig,
                prod.getId_ProdTipDoc()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public List<ProdTipoDoc> selectProdTipoDctoByID(int id) {
        String SQL = "select * from tb_sys_ProductoTipoDocumento where id_prodtipdoc=" + id;
        List<ProdTipoDoc> prod = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());
        return prod;
    }

    public List<ProdTipoDoc> getListCountProdTipoDoc() {
        String SQL = "select ptd.fk_idProducto,pr.dv_NomProduc ,pr.dv_CodProducto,count(ptd.fk_idProducto) cant \n"
                + "	from tb_sys_ProductoTipoDocumento ptd\n"
                + "	inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto \n"
                + "	group by ptd.fk_idProducto,pr.dv_NomProduc ,pr.dv_CodProducto\n"
                + "	order by ptd.fk_idProducto,pr.dv_NomProduc ,pr.dv_CodProducto desc";

        List<ProdTipoDoc> ProdTipoDoc = jdbcTemplateObject.query(SQL, new CountProdTipoDocMapper());
        return ProdTipoDoc;
    }

    public List<ProdTipoDoc> getListDocsPorIdProducto(String Op) {
        String SQL = "SELECT ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo, ptd.obligatorio  from dbo.tb_sys_ProductoTipoDocumento ptd\n"
                + "inner join dbo.tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto\n"
                + "inner join dbo.tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + "join TB_SYS_OPERACION ope ON dv_CodOperacion ='" + Op + "'\n"
                + "WHERE pr.di_IdProduc = ope.di_fk_IdProduc\n"
                + "AND td.di_IdTipDcto not in (select di_fk_TipoDcto from tb_Sys_Documento where di_fk_IdOper = ope.di_IdOperacion)\n"
                + "ORDER BY td.dv_NomTipDcto asc";

        System.out.println(" getListDocsPorIdProducto => " + SQL);
        List<ProdTipoDoc> ProdTipoDoc = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());
        return ProdTipoDoc;
    }

    public List<ProdTipoDoc> getListDocsPorIdProductoDevo(String Op) {
        String SQL = "SELECT ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo, ptd.obligatorio  from dbo.tb_sys_ProductoTipoDocumento ptd\n"
                + "inner join dbo.tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto\n"
                + "inner join dbo.tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + "join TB_SYS_OPERACION ope ON dv_CodOperacion ='" + Op + "'\n"
                + "WHERE pr.di_IdProduc = ope.di_fk_IdProduc\n"
                //+ "AND td.di_IdTipDcto not in (select di_fk_TipoDcto from tb_Sys_Documento where di_fk_IdOper = ope.di_IdOperacion)\n"
                + "ORDER BY td.dv_NomTipDcto asc";

        System.out.println(" getListDocsPorIdProducto => " + SQL);
        List<ProdTipoDoc> ProdTipoDoc = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());
        return ProdTipoDoc;
    }

    public List<ProdTipoDoc> listDocsObligatoriosPorIdOpe(int idOp) {

        String SQL = "select ptd.id_prodtipdoc,pr.dv_NomProduc,ptd.fk_idProducto di_IdProduc,td.dv_NomTipDcto, ptd.fk_idTipoDocto di_IdTipDcto, ptd.bit_Activo, ptd.obligatorio\n"
                + "from tb_sys_ProductoTipoDocumento ptd\n"
                + "join tb_Sys_Operacion ope ON ope.di_IdOperacion ="+idOp+"\n"
                + "join tb_Sys_Producto pr ON pr.di_IdProduc = ope.di_fk_IdProduc\n"
                + "join tb_Sys_TipoDcto td ON td.di_IdTipDcto = ptd.fk_idTipoDocto\n"
                + "where fk_idProducto = ope.di_fk_IdProduc\n"
                + "and obligatorio = 1";

        System.out.println(" listDocsObligatoriosPorIdOpe => [" + SQL + " ]");
        List<ProdTipoDoc> ProdTipoDoc = jdbcTemplateObject.query(SQL, new ProdTipoDocMapper());
        return ProdTipoDoc;
    }

}
