/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MetodosGenerales;
import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.Estado;
import sisboj.entidades.Solicitud;

/**
 *
 * @author EXAESAN
 */
public class HiloInh extends Thread {

    EstadoJDBC estJDBC;
    int c = 0;
    int res = 0;
    int num_sol = 0;
    int cont = 0;

    int idsola = 0;
    int resul = 0;

    static List<BandejaCustodia> bandeja2;
    ArrayList<Integer> id_soles = new ArrayList();
    static ArrayList<Integer> numInh = new ArrayList();

    public HiloInh(ArrayList<Integer> listas) throws SQLException {
        //c = ids;
        id_soles = listas;

    }

    @Override
    public void run() {
        System.out.println("Hola, soy el hilo " + c);

        try {
            SolicitudJDBC soljdbc = new SolicitudJDBC();
            soljdbc.setDataSource(new MvcConfig().getDataSourceSisBoj());

            for (int i = 0; i < id_soles.size(); i++) {

                idsola = id_soles.get(i);
                resul = soljdbc.getNumeroInhporSols(idsola);
                numInh.add(resul);
                System.out.println("IDSOL " + idsola + " RESULSET-> " + resul);

            }

        } catch (Exception ex) {
            Logger.getLogger(HiloInh.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static ArrayList<Integer> getValue() throws SQLException {

        return numInh;
    }

}
