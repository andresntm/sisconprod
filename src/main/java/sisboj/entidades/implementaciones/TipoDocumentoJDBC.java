/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.RowMapper.TipoDocumentoMapper;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.interfaces.TipoDocumentoDAO;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class TipoDocumentoJDBC implements TipoDocumentoDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public List<TipoDocumento> getListTipoDocumento(String nom) {

        String donde = "";
        if (!"".equals(nom) && nom != null) {
            donde = " where LOWER(dv_NomTipDcto) like '%" + nom.toLowerCase() + "%'";
        }
        String SQL = " select * from tb_Sys_TipoDcto \n"
                + donde;

        System.out.println(" getListTipoDocumento => " + SQL);
        List<TipoDocumento> tipoDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
        return tipoDocumento;
    }

    /**
     * devuelve > 0 si tipoDocumento ya existe
     *
     * @param nom
     * @return
     */
    public List<TipoDocumento> nomTipoDocumentoExiste(String nom) {
//        int cant;
        String SQL = "select * from tb_Sys_TipoDcto where LOWER(dv_NomTipDcto)='" + nom.toLowerCase() + "'";
        List<TipoDocumento> tipoDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
//        cant = tipoDocumento.size();
        return tipoDocumento;
    }

    /**
     * devuelve > 0 si codigo de tipoDocumento ya existe
     *
     * @param nom
     * @return
     */
    public List<TipoDocumento> codTipoDocumentoExiste(int cod) {
//        int cant;
        String SQL = "select * from tb_Sys_TipoDcto where di_IdTipDctoPJ=" + cod;
        List<TipoDocumento> tipoDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
//        cant = tipoDocumento.size();
        return tipoDocumento;
    }

    //Insert TipoDocumento
    public boolean insertTipoDocumento(final TipoDocumento td) {

        final String SQL = "INSERT INTO [dbo].[tb_Sys_TipoDcto]\n"
                + "           ([dv_NomTipDcto]\n"
                + "           ,[di_IdTipDctoPJ]\n"
                + "           ,[bit_Activo])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"di_IdTipDcto"});

                    ps.setString(1, td.getDv_NomTipDcto());
                    ps.setInt(2, td.getDi_IdTipDctoPJ());
                    //  ps.setDate(3, (java.sql.Date) new Date());
                    ps.setInt(3, Integer.parseInt(td.getBit_Activo()));
                    return ps;
                }
            },
                    keyHolder);
            return true;

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            return false;
        }
    }

    //Update TipoDocumento
    public boolean updateTipoDocumento(TipoDocumento td) {
        //dv_NomTipDcto,di_IdTipDctoPJ,ddt_FecIngreso,bit_Activo
        String SQL = "UPDATE [dbo].[tb_Sys_TipoDcto]\n"
                + "   SET [dv_NomTipDcto] = ?\n"
                + "      ,[di_IdTipDctoPJ] = ?\n"
                + "      ,[bit_Activo] = ?\n"
                + " WHERE di_IdTipDcto = ?";
        System.out.println("updateProducto SQL => " + SQL);

        int est = 0;
        if ("Activo".equals(td.getBit_Activo())) {
            est = 1;
        }
        if ("Inactivo".equals(td.getBit_Activo())) {
            est = 0;
        }

        try {
            this.jdbcTemplateObject.update(SQL, new Object[]{
                td.getDv_NomTipDcto(),
                td.getDi_IdTipDctoPJ(),
                est,
                td.getDi_IdTipDcto()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public List<TipoDocumento> selectTipoDocumentoByID(int id) {
        String SQL = "select * from tb_Sys_TipoDcto where di_IdTipDcto=" + id;
        List<TipoDocumento> tipoDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
        return tipoDocumento;
    }

    public List<TipoDocumento> getListTipoDocumentoPorProd(String prod) {

        String donde = "";
        if (!"".equals(prod) && prod != null) {
            donde = "where di_IdTipDcto not in (select fk_idTipoDocto \n"
                    + "                             from tb_sys_ProductoTipoDocumento ptd\n"
                    + "							 join tb_sys_Producto prod on prod.di_IdProduc = ptd.fk_idProducto \n"
                    + "							 where prod.dv_NomProduc ='" + prod + "')";
        }
        String SQL = "select td.* from tb_Sys_TipoDcto td\n"
                + donde;

        System.out.println(" getListTipoDocumento => " + SQL);
        List<TipoDocumento> tipoDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
        return tipoDocumento;
    }

    public List<TipoDocumento> selectDocumentoPorNombre(String name) {
        String SQL = "select * from tb_Sys_TipoDcto where dv_NomTipDcto ='" + name + "'";
        List<TipoDocumento> TipDocumento = jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
        return TipDocumento;
    }

    public TipoDocumento selectDocPorNombre(String name) {
        
        TipoDocumento td = new TipoDocumento();
        String SQL = "select * from tb_Sys_TipoDcto where dv_NomTipDcto ='" + name + "'";
         List<TipoDocumento> tipDocumento  =  jdbcTemplateObject.query(SQL, new TipoDocumentoMapper());
         td = tipDocumento.get(0);
        return td;
    }

}
