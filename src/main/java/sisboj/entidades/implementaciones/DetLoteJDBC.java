/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.DetLoteMapper;
import sisboj.entidades.DetLote;
import sisboj.entidades.interfaces.DetLoteDao;


/**
 *
 * @author excosoc
 */
public class DetLoteJDBC implements DetLoteDao{
    
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    
    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public DetLote getDetLote(int di_IdDetLote) {
        String SQL = "select * from tb_Sys_DetLote where di_IdDetLote = ?";
        DetLote dlote = jdbcTemplateObject.queryForObject(SQL,new Object[]{di_IdDetLote}, new DetLoteMapper());
        return dlote;
    }

    @Override
    public List<DetLote> listDetLote(String SQL) {
        List <DetLote> dlote = jdbcTemplateObject.query(SQL,new DetLoteMapper());
        return dlote;
    }

    @Override
    public boolean update(int di_IdDetLote, String tb_Campo, Object val) {
        try{
            String SQL = "update tb_Sys_DetLote set where di_IdDetLote = ?";
            jdbcTemplateObject.update(SQL, val);
            System.out.println("Updated Record with ID = " + val );
            return true;
        }catch (Exception e) {
            //System.out.println("SQL Exception: " + e.toString());
            return false;
        }
    }
    
     @Override
    public void setJdbc() {
        try {
            this.dataSource = new MvcConfig().getDataSourceSisBoj();
            this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
        } catch (SQLException ex) {
            Logger.getLogger(DetLoteJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
      @Override
    public int insert(final DetLote dLot) {
        final String insertDetSol = "INSERT INTO [dbo].[tb_Sys_DetLote]\n"
                + "           ([di_fk_IdLote],\n"
                + "           [di_fk_IdOper],\n"
                + "           [di_fk_IdDetSol],"
                + "           [di_fk_IdEstado])\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,? )\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"di_IdDetLote"});
                    int value1=dLot.getDi_fk_IdLote();
                    ps.setInt(1,value1 );
                    int value2= dLot.getDi_fk_IdOper();
                    ps.setInt(2,value2);
                    int value3= dLot.getDi_fk_IdDetSol();
                    ps.setInt(3,value3 );
                    int value4= dLot.getDi_fk_IdEstado();
                    
                    ps.setInt(4,value4);
                    return ps;
                }
            }, key);

            /*insertDetSol,new Object[]{    detSol.getDi_fk_IdSolicitud()
                                                    ,detSol.getDi_fk_IdUbi()
                                                    ,detSol.getDi_fk_IdDetHito()
                                                });*/
           return key.getKey().intValue();
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());

            return -1;
        }
    }  
   
    
 
      @Override
    public int insertDetLoteDocument(final int id_detlote,final int id_documento) {
        final String insertDetSol = "INSERT INTO [dbo].[tb_Sys_DetLotDoc]\n"
                + "           ([di_fk_IdDcto],\n"
                + "           [di_fk_IdDetLote])\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? )\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"di_IdDetLotDoc"});
                    int value1=id_documento;
                    ps.setInt(1,value1 );
                    int value2=id_detlote ;
                    ps.setInt(2,value2);
                    return ps;
                }
            }, key);

            /*insertDetSol,new Object[]{    detSol.getDi_fk_IdSolicitud()
                                                    ,detSol.getDi_fk_IdUbi()
                                                    ,detSol.getDi_fk_IdDetHito()
                                                });*/
           return key.getKey().intValue();
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());

            return -1;
        }
    }
    
    
          @Override
        public boolean setDetLoteRecepcionado(int id_lote){

        
        
        String SQL = "UPDATE tb_Sys_DetLote "
                + "SET    di_fk_IdEstado           = 271 \n"

                + "WHERE  di_fk_IdLote         = '"+id_lote+"'";

              try {
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }

    }   
    
    
}
