/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import sisboj.entidades.interfaces.OperacionesDAO;
import sisboj.RowMapper.OperacionesMapper;
import sisboj.entidades.Operaciones;

/**
 *
 * @author excosoc
 */
public class OperacionesJDBC implements OperacionesDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    private Operaciones oper;

    public OperacionesJDBC() {
    }

    /*public OperacionesJDBCTemplate(DataSource ds){
        setDataSource(ds);
    }*/
    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    /*@Override
    public JdbcTemplate getDataSource() {
        return this.jdbcTemplateObject;
    }*/
    @Override
    public Operaciones getOperaciones(String dv_CodOperacion) {
        Operaciones oper = null;
        String SQL = "select TOP 1 * from tb_Sys_Operacion where dv_CodOperacion = ?";
        try {
            oper = jdbcTemplateObject.queryForObject(SQL, new Object[]{dv_CodOperacion}, new OperacionesMapper());
            System.out.print("####### ---- Imprimo Resultado Query///oper.getDi_IdOperacion()/// ERR:[" + oper.getDi_IdOperacion() + "]----#######");
        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///getoperacion/// ERR:[" + ex.getMessage() + "]----#######");
        }
        return oper;
    }

    @Override
    public List<Operaciones> listOperaciones(String SQL) {
        List<Operaciones> oper = jdbcTemplateObject.query(SQL, new OperacionesMapper());
        return oper;
    }

    @Override

    //En contrsucacion
    public boolean update(String dv_CodOperacion, String tb_Campo, Object val) {
        try {
            String SQL = "update tb_Sys_Operacion set where dv_CodOperacion = ?";
            jdbcTemplateObject.update(SQL, val);
            System.out.println("Updated Record with ID = " + val);
            return true;
        } catch (Exception e) {
            //System.out.println("SQL Exception: " + e.toString());
            return false;
        }
    }

    @Override
    public List<Operaciones> getOperaciones(int di_fk_IdProduc) {
        String SQL;
        oper = new Operaciones();
        SQL = oper.selectOperacion();

        if (di_fk_IdProduc == 3) {

            SQL = SQL + "WHERE (di_fk_IdProduc = " + di_fk_IdProduc + " OR di_fk_IdProduc =4) AND bit_Nuevo = 1";
        } else {
            SQL = SQL + "WHERE di_fk_IdProduc = " + di_fk_IdProduc + " AND bit_Nuevo = 1";
        }

        List<Operaciones> oper = jdbcTemplateObject.query(SQL, new OperacionesMapper());
        return oper;
    }

}
