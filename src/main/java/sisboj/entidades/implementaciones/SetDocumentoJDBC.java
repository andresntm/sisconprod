/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.RowMapper.DetSetDocumentosMapper;
import sisboj.RowMapper.SetDocumentosMapper;
import sisboj.entidades.SetDocumentosEnt;
import sisboj.entidades.interfaces.SetDocumentosDAO;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class SetDocumentoJDBC implements SetDocumentosDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public List<SetDocumentosEnt> listSetDocumentosE() {
        //String SQL = "select * from tb_Sys_SetDocs order by dv_NomSetDoc asc";

        String SQL = "select * from tb_Sys_SetDocs";

        List<SetDocumentosEnt> listsetdocs = new ArrayList<SetDocumentosEnt>();
        try {
            System.out.println(" listSetDocumentosE => [ " + SQL + " ]");
            listsetdocs = jdbcTemplateObject.query(SQL, new SetDocumentosMapper());
        } catch (Exception e) {
            System.out.println("SetDocumentoJDBC.listSetDocumentosE ==> Problemas al obtener listado de set de documentos: " + e.getMessage());

        }

        return listsetdocs;
    }

    /*
    public boolean prodDetDocExiste(final int idSetDoc, final int idSetTipoDoc) {

        boolean response = false;
        String SQL = "select * from tb_Sys_DetSetDocs\n"
                + "	where  di_fk_IdSetDoc=" + idSetDoc + ""
                + "     AND    di_fk_IdTipDcto=" + idSetTipoDoc;

        List<SetDocumentosEnt> productos = null;
        try {
            System.out.println(" prodDetDocExiste => [ " + SQL + " ]");
            productos = jdbcTemplateObject.query(SQL, new DetSetDocumentosMapper());
            if (productos.size() > 0) {
                
                response = true;
            }

        } catch (DataAccessException e) {
            e.getMessage();
        }
        System.out.println("RESPONSE: "+response);
        return response;
    }
     */
    public boolean prodDetDocExiste(List<SetDocumentosEnt> productos) {

        boolean response = false;
        List<SetDocumentosEnt> prodSel = new ArrayList<SetDocumentosEnt>();

        String SQL = "";

        for (int p = 0; p < productos.size(); p++) {

            SQL = "";

            SQL = "select * from tb_Sys_DetSetDocs\n"
                    + "	where  di_fk_IdSetDoc=" + productos.get(p).getDi_IdSetDoc() + ""
                    + "     AND    di_fk_IdTipDcto=" + productos.get(p).getDi_fk_IdTipDcto();

            try {
                System.out.println(" prodDetDocExiste => [ " + SQL + " ]");

                prodSel = jdbcTemplateObject.query(SQL, new DetSetDocumentosMapper());

                if (prodSel.size() > 0) {
                    System.out.println(" NO insertar ...");
                    //UPDATE 
                    //response = insertSetDocs(productos.get(p).getDi_IdSetDoc(), productos.get(p).getDi_fk_IdTipDcto());

                } else {
                    System.out.println("Insertando ...");
                    //eliminarSetDoc(productos.get(p).getDi_IdSetDoc());
                    response = insertSetDocs(productos.get(p).getDi_IdSetDoc(), productos.get(p).getDi_fk_IdTipDcto());
                }

            } catch (DataAccessException e) {
                e.getMessage();
            }
        }

        System.out.println("RESPONSE: " + response);
        return response;
    }

    public List<SetDocumentosEnt> listSetDocumentos() {
        String SQL = "select * from tb_Sys_SetDocs order by dv_NomSetDoc asc";

//        String SQL = "select sd.*,td.dv_NomTipDcto,td.di_IdTipDcto from tb_Sys_SetDocs sd\n"
//                + "join tb_Sys_DetSetDocs dsd on dsd.di_fk_IdSetDoc = sd.di_IdSetDoc\n"
//                + "join tb_Sys_TipoDcto td on td.di_IdTipDcto = dsd.di_fk_IdTipDcto\n"
//                + "order by dv_NomSetDoc asc";
        List<SetDocumentosEnt> listsetdocs = new ArrayList<SetDocumentosEnt>();
        try {
            System.out.println(" listSetDocumentos => [ " + SQL + " ]");
            listsetdocs = jdbcTemplateObject.query(SQL, new SetDocumentosMapper());
        } catch (Exception e) {
            System.out.println("SetDocumentoJDBC.listSetDocumentos ==> Problemas al obtener listado de set de documentos: " + e.getMessage());

        }

        return listsetdocs;
    }

    public List<SetDocumentosEnt> listDetSetDocumentos(int setId) {
        String SQL = "";
        List<SetDocumentosEnt> listsetdocs = new ArrayList<SetDocumentosEnt>();

        SQL = "select sd.*,td.dv_NomTipDcto,td.di_IdTipDcto from tb_Sys_SetDocs sd\n"
                + "join tb_Sys_DetSetDocs dsd on dsd.di_fk_IdSetDoc = sd.di_IdSetDoc\n"
                + "join tb_Sys_TipoDcto td on td.di_IdTipDcto = dsd.di_fk_IdTipDcto\n"
                + "and di_IdSetDoc =" + setId;

        try {

            System.out.println(" listDetSetDocumentos => [ " + SQL + " ]");

            listsetdocs = jdbcTemplateObject.query(SQL, new DetSetDocumentosMapper());

        } catch (Exception e) {
            System.out.println("SetDocumentoJDBC.listDetSetDocumentos ===> Problemas al obtener detalle de set de documentos: " + e.getMessage());

        }

        return listsetdocs;
    }

    public int getDetSetIdByNom(String nomDoc) {
        String SQL = "";
        int listsetdocs = 0;

        SQL = "select di_IdSetDoc from tb_Sys_SetDocs \n"
                + "where dv_NomSetDoc = '" + nomDoc + "'";

        try {

            System.out.println(" listDetSetDocumentos => [ " + SQL + " ]");

            listsetdocs = jdbcTemplateObject.queryForInt(SQL);

        } catch (Exception e) {
            System.out.println("SetDocumentoJDBC.listDetSetDocumentos ===> Problemas al obtener detalle de set de documentos: " + e.getMessage());

        }

        return listsetdocs;
    }

    //Insert TipoDocumento
    public boolean insertSetDocs(final int idSetDoc, final int idSetTipoDoc) {

        final String SQL = "INSERT INTO [dbo].[tb_Sys_DetSetDocs]\n"
                + "           ([di_fk_IdSetDoc]\n"
                + "           ,[di_fk_IdTipDcto])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"di_IdDetSetDoc"});

                    ps.setInt(1, idSetDoc);
                    //  ps.setDate(3, (java.sql.Date) new Date());
                    ps.setInt(2, idSetTipoDoc);
                    return ps;
                }
            },
                    keyHolder);
            return true;

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            return false;
        }
    }

//    public boolean updateSetDoc(int di_IdSolicitud, String tb_Campo, int val) {
//        try {
//            String SQL = "update tb_Sys_Solicitud set " + tb_Campo + "=" + val + " where di_IdSolicitud= " + di_IdSolicitud;
//            System.out.println("updateSolicitud SQL => " + SQL);
//            jdbcTemplateObject.update(SQL);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//
//    }
    public boolean eliminarSetDoc(int id_operacion) {
        try {
            String SQL = "DELETE tb_Sys_DetSetDocs where di_fk_IdSetDoc=" + id_operacion;

            System.out.println("eliminarSetDoc SQL => " + SQL);
            jdbcTemplateObject.execute(SQL);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public  List<SetDocumentosEnt> getSetIdByNomList(int idDocSet) {
        String SQL = "";
         List<SetDocumentosEnt> listsetdocs = new ArrayList<SetDocumentosEnt>();

        SQL = "select * from tb_Sys_SetDocs \n"
                + "where di_IdSetDoc = " + idDocSet ;

        try {

            System.out.println(" getSetIdByNomList => [ " + SQL + " ]");

          
            listsetdocs = jdbcTemplateObject.query(SQL, new SetDocumentosMapper());

        } catch (Exception e) {
            System.out.println("SetDocumentoJDBC.getSetIdByNomList ===> Problemas al obtener detalle de set de documentos: " + e.getMessage());

        }

        return listsetdocs;
    }
    
    
    
     public boolean updateSetDocumento(List<SetDocumentosEnt> td) {
        
        boolean response = false;

          for (int p = 0; p < td.size(); p++) {

              String SQL = "UPDATE [dbo].[tb_Sys_SetDocs]\n"
                + "   SET [dv_NomSetDoc] = ?\n"
                + "      ,[ddt_FechaMod] = ?\n"
                + "      ,[dv_Descripcion] = ?\n"
                + " WHERE di_IdSetDoc = ?";
                System.out.println("updateSetDocumento SQL => " + SQL);
                //productos.get(p).getDi_IdSetDoc() 
                    
            
            try {
            this.jdbcTemplateObject.update(SQL, new Object[]{
                td.get(p).getDv_NomSetDoc(),
                td.get(p).getDdt_FechaMod(),
                td.get(p).getDv_Descripcion(),
                td.get(p).getDi_IdSetDoc()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
                    
             }

        return true;
    }

}
