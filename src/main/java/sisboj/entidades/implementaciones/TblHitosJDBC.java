/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.entidades.interfaces.TblHitosDAO;
import sisboj.RowMapper.TblHitosMapper;
import sisboj.entidades.BancaTblHitosEnt;
import sisboj.entidades.EstOpeHitoCount;
import sisboj.entidades.OficinaTblHitosEnt;
import sisboj.entidades.ProductosTblHitosEnt;
import sisboj.entidades.RegionTblHitosEnt;
import sisboj.entidades.TblHitosEnt;
import sisboj.entidades.UbicacionTblHitosEnt;

/**
 *
 * @author EXVGUBA
 */
public class TblHitosJDBC implements TblHitosDAO, Serializable {

    SimpleDateFormat simpleDF = new SimpleDateFormat("yyyy-MM-dd");
    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private List<TblHitosEnt> tblHitosList = new ArrayList<TblHitosEnt>();
    List<UbicacionTblHitosEnt> tblHitosListUbicacion = new ArrayList<UbicacionTblHitosEnt>();
    List<OficinaTblHitosEnt> tblHitosListOficinas = new ArrayList<OficinaTblHitosEnt>();
    List<RegionTblHitosEnt> regionTblHitos = new ArrayList<RegionTblHitosEnt>();
    List<BancaTblHitosEnt> tblHitosListBanca = new ArrayList<BancaTblHitosEnt>();
    List<ProductosTblHitosEnt> listProductos = new ArrayList<ProductosTblHitosEnt>();
    private Date fechaInicio;
    private Date fechaFin;
    private String ubicacionTmp = "";
    private String oficina = "";
    private String region = "";
    private String banca = "";
    private String StrfechaInicio = "";
    private String StrfechaFin = "";
    private EstOpeHitoCount countEstOpe;

//    PreparedStatement pestament =null;
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        this.countEstOpe = new EstOpeHitoCount();

    }

    /**
     * Metodo que permite calcular fechas, sumar y restar a�os a una fecha. para
     * restar a�os el numero debe ser negativo (ej: -6 )
     *
     * @param fecha
     * @param anioRestar
     * @return objeto tipo calendar c1
     */
    public static Calendar calcularFechas(String periodoT, Date fecha, int anioRestar) {
        try {
            Calendar c1 = Calendar.getInstance();
            c1.setTime(fecha);
            System.out.println("--> " + c1.getTime());
            if ("anio".equals(periodoT)) {
                c1.add(Calendar.YEAR, anioRestar);
            }
            if ("mes".equals(periodoT)) {
                c1.add(Calendar.MONTH, anioRestar);
            }
            if ("dia".equals(periodoT)) {
                c1.add(Calendar.DAY_OF_MONTH, anioRestar);
            }
            System.out.println("--> + " + c1.getTime());
            return c1;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param fIni
     * @param fFin
     * @param ubicacion
     * @param oficina
     * @param region
     * @param banca
     * @param rut
     * @param dmi
     * @param dmf
     * @return
     */
    public List<TblHitosEnt> getListTblHitos(Date fIni, Date fFin, List<UbicacionTblHitosEnt> ubicacion, String oficina, String region, String banca, String rut, int dmi, int dmf, String n_of, String n_op, String producto, String estado, String nSol) {
        //se limpian vacian los filtros para llenarlos nuevamente
        countEstOpe = new EstOpeHitoCount();
        tblHitosListUbicacion.clear();
        tblHitosListOficinas.clear();
        regionTblHitos.clear();
        tblHitosListBanca.clear();
        listProductos.clear();
        //-----------------------------------//
        fechaInicio = fIni;
        fechaFin = fFin;
        ubicacionTmp = "";

        System.out.println("TblHitosJDBC.getListTblHitos fechaFin Inicio Query ");
        System.out.println("TblHitosJDBC.getListTblHitos fechaInicio => " + fIni);
        System.out.println("TblHitosJDBC.getListTblHitos fechaFin => " + fFin);
//        System.out.println("TblHitosJDBC.getListTblHitos ubicacion => " + ubicacion.toString());
        System.out.println("TblHitosJDBC.getListTblHitos fechaFin => " + oficina);
        System.out.println("TblHitosJDBC.getListTblHitos fechaInicio => " + region);
        System.out.println("TblHitosJDBC.getListTblHitos fechaFin => " + banca);
        System.out.println("TblHitosJDBC.getListTblHitos producto => " + producto);
        System.out.println("TblHitosJDBC.getListTblHitos String estado => " + estado);
        System.out.println("TblHitosJDBC.getListTblHitos String estado => " + nSol);
//        System.out.println("TblHitosJDBC.getListTblHitos Dias mora Inicio => " + banca);
//        System.out.println("TblHitosJDBC.getListTblHitos Dias Mora Fin => " + banca);

        List<TblHitosEnt> tblHitosList = new ArrayList<TblHitosEnt>();

        if (fIni == null && fFin == null) {

            StrfechaInicio = "";
            StrfechaFin = "";

            /*
            fechaFin = new Date();
            fechaInicio = calcularFechas("mes",fechaFin, -6).getTime();
            System.out.println("fecha asdadas=> "+fechaInicio);

            StrfechaInicio = simpleDF.format(fechaInicio);
            StrfechaFin = simpleDF.format(fechaFin);
             */
//            fechaInicio="CONVERT(varchar, getdate(), 23)";
//            fechaFin="CONVERT(varchar, DATEADD(YY, -1, getdate()), 23)";
        } else if (fIni != null && fFin == null) {
            StrfechaInicio = " AND CONVERT(varchar, fecha_ejecucion, 23) >= '" + simpleDF.format(fIni) + "' ";
            StrfechaFin = "";
        } else if (fIni == null && fFin != null) {
            StrfechaFin = "AND CONVERT(varchar, fecha_ejecucion, 23) <= '" + simpleDF.format(fFin) + "' ";
            StrfechaInicio = "";

        } else {
            StrfechaInicio = " AND CONVERT(varchar, fecha_ejecucion, 23) >= '" + simpleDF.format(fIni) + "' ";
            StrfechaFin = "AND CONVERT(varchar, fecha_ejecucion, 23) <= '" + simpleDF.format(fFin) + "' ";
        }

        // Filtro Ubicacion 
        String filtroUbicacion = "";

//        if (ubicacion != null && "".equals(ubicacion.toString().trim()) ) {
        if (ubicacion != null && ubicacion.size() > 0) {
            System.out.println("filtroUbicacion.ubicacion => " + ubicacion.toString());
            for (int i = 0; i < ubicacion.size(); i++) {

                ubicacionTmp = ubicacionTmp + ubicacion.get(i).getUbicacion();

                if (i < ubicacion.size() - 1) {
                    ubicacionTmp = ubicacionTmp + "','";
                }

            }
            System.out.println("ubicacionTmp => " + ubicacionTmp);
            filtroUbicacion = "AND ubicacion in('" + ubicacionTmp + "')";
            System.out.println("filtroUbicacion => " + filtroUbicacion);

        }

        // Filtro Oficina 
        String filtroOficina = "";
        if (oficina != null && !"".equals(oficina)) {
            filtroOficina = "AND oficina = '" + oficina.trim() + "' ";
        }
        // Filtro Region 
        String filtroRegion = "";
        if (region != null && !"".equals(region)) {
            filtroRegion = "AND reg = '" + region.trim() + "' ";
        }

        // Filtro Banca 
        String filtroBanca = "";
        if (banca != null && !"".equals(banca)) {
            filtroBanca = "AND banca ='" + banca.trim() + "' ";
        }

        // Filtro Rut 
        String filtroRut = "";
        if (rut != null && !"".equals(rut)) {
            filtroRut = "AND rut_completo LIKE '%" + rut.trim() + "%' ";
        }
        //Filtro Numero de Oficina
        String filtroNOf = "";
        if (n_of != null && !"".equals(n_of)) {
            filtroNOf = "AND n_of ='" + n_of.trim() + "' ";
        }

        // Filtro Numero de Operacion
        String filtroNOp = "";
        if (n_op != null && !"".equals(n_op)) {
            filtroNOp = "AND op_orig LIKE '%" + n_op.trim() + "%' ";
        }

        // Filtro Dias Mora Inicio
        String FiltroDiasMoraIni = "";
        if (dmi > 0) {
            FiltroDiasMoraIni = "AND dias_mora >=" + dmi + " ";
        }
        // Filtro Dias Mora Fin 
        String FiltroDiasMoraFin = "";
        if (dmf > 0) {
            FiltroDiasMoraFin = "AND dias_mora <=" + dmf + " ";
        }

        // Filtro Producto 
        String filtroProducto = "";
        if (producto != null && !"".equals(producto)) {
            filtroRut = "AND producto LIKE '%" + producto.trim() + "%' ";
        }

        // Filtro estado 
        String filtroEstado = "";
        if (estado != null && !"".equals(estado)) {
            filtroEstado = "AND estado in (" + estado.trim() + ")";
        }
        
         // Filtro Numero de Operacion
        String filtroNSol = "";
        if (nSol != null && !"".equals(nSol)) {
            filtroNSol = "AND SOLICITUD = '" + nSol.trim() + "' ";
        }

        System.out.println("filtroEstado => " + filtroEstado);
        System.out.println("Fecha Query StrfechaInicio =>" + StrfechaInicio);
        System.out.println("Fecha Query StrfechaInicio =>" + StrfechaInicio);

        try {

            String SQL = "select hit.UBICACION,hit.OFICINA,hit.N_OF,hit.REG,hit.OP_ORIG,hit.OPERACION_2,hit.TIPO_OP,hit.MONTO,hit.BANCA,hit.RUT_COMPLETO,hit.RUT,hit.DV,hit.NOMBRE\n"
                    + "         ,hit.FAN,hit.FCURSE,hit.DIAS_MORA,hit.CODE,hit.GLOSA,hit.RENEG_POR_NZA,hit.EJECUTIVO_NEG,hit.PRIORIDAD,hit.NOM_BANCA,hit.PRODUCTO,hit.SOLICITUD\n"
                    + "         ,hit.nombre_region,hit.Fecha_ejecucion,hit.PRIORIDAD_PYME,hit.USUARIO_GENERA,hit.hito_especial,hit.acelerado,tmp.estado\n"
                    + "    from in_cbza.dbo.tbl_hito_jud_bci_historico hit\n"
                    + "    join tb_Sys_tempCuntodiaEspadoOperaciones tmp on tmp.operaciones = hit.OP_ORIG\n"
                    + "    where CONVERT(date,hit.Fecha_ejecucion) in (select top 2 CONVERT(date,Fecha_ejecucion) from in_cbza.dbo.tbl_hito_jud_bci_historico  group by Fecha_ejecucion order by Fecha_ejecucion desc)\n"
                    + StrfechaFin
                    + " " + filtroUbicacion + ""
                    + " " + filtroOficina + ""
                    + " " + filtroRegion + ""
                    + " " + filtroBanca + ""
                    + " " + filtroRut + ""
                    + " " + FiltroDiasMoraIni + ""
                    + " " + FiltroDiasMoraFin + ""
                    + " " + filtroNOf + ""
                    + " " + filtroNOp + ""
                    + " " + filtroNSol + ""
                    + " " + filtroProducto + ""
                    + " " + filtroEstado + ""
                    + " union all\n"
                    + " select hit2.UBICACION,hit2.OFICINA,hit2.N_OF,hit2.REG,hit2.OP_ORIG,hit2.OPERACION_2,hit2.TIPO_OP,hit2.MONTO,hit2.BANCA,hit2.RUT_COMPLETO,hit2.RUT,hit2.DV,hit2.NOMBRE\n"
                    + "      ,hit2.FAN,hit2.FCURSE,hit2.DIAS_MORA,hit2.CODE,hit2.GLOSA,hit2.RENEG_POR_NZA,hit2.EJECUTIVO_NEG,hit2.PRIORIDAD,hit2.NOM_BANCA,hit2.PRODUCTO,hit2.SOLICITUD\n"
                    + "      ,hit2.nombre_region,hit2.Fecha_ejecucion,hit2.PRIORIDAD_PYME,hit2.USUARIO_GENERA,hit2.hito_especial,hit2.acelerado,tmp2.estado\n"
                    + "    from sisboj_tbl_hito_jud_bci_historico hit2\n"
                    + "    join tb_Sys_tempCuntodiaEspadoOperaciones tmp2 on tmp2.operaciones = hit2.OP_ORIG\n"
                    + "    where CONVERT(date,hit2.Fecha_ejecucion) in (select top 2 CONVERT(date,Fecha_ejecucion) from in_cbza.dbo.tbl_hito_jud_bci_historico  group by Fecha_ejecucion order by Fecha_ejecucion desc)\n"
                    + "    and hit2.OP_ORIG not in (select OP_ORIG from in_cbza.dbo.tbl_hito_jud_bci_historico\n"
                    + "        where CONVERT(date,hit2.Fecha_ejecucion) in (select top 2 CONVERT(date,max(fecha_ejecucion)) FROM in_cbza.dbo.tbl_hito_jud_bci_historico))AND fecha_ejecucion in(select max(fecha_ejecucion)  FROM in_cbza.dbo.tbl_hito_jud_bci_historico)\n"
                    + StrfechaFin
                    + " " + filtroUbicacion + ""
                    + " " + filtroOficina + ""
                    + " " + filtroRegion + ""
                    + " " + filtroBanca + ""
                    + " " + filtroRut + ""
                    + " " + FiltroDiasMoraIni + ""
                    + " " + FiltroDiasMoraFin + ""
                    + " " + filtroNOf + ""
                    + " " + filtroNOp + ""
                    + " " + filtroNSol + ""
                    + " " + filtroProducto + ""
                    + " " + filtroEstado + ""
                    + "ORDER BY rut_completo,fecha_ejecucion DESC;";

            System.out.println("Pase por aca => getListTblHitos SQL => " + SQL);
            tblHitosList = jdbcTemplateObject.query(SQL, new TblHitosMapper());
            System.out.println("Pase por aca => getListTblHitos 2 => ");

            // INICIO CUENTA ESTADOS OPERACIONES
            String estad = "";
            int nuevo = 0;
            int con_salida = 0;
            int pendiente = 0;

            for (TblHitosEnt h : tblHitosList) {
                estad = h.getEstado();
                if ("NUEVO".equals(estad)) {
                    nuevo = nuevo + 1;
                }
                if ("CON_SALIDA".equals(estad)) {
                    con_salida = con_salida + 1;
                }
                if ("PENDIENTE".equals(estad)) {
                    pendiente = pendiente + 1;
                }
            }
            //OPERACIONES NUEVAS
            countEstOpe.setEstadoOpeN("NUEVO");
            countEstOpe.setCantidadN(nuevo);

            //OPERACIONES CON_SALIDA
            countEstOpe.setEstadoOpeF("CON_SALIDA");
            countEstOpe.setCantidadF(con_salida);

            //OPERACIONES PENDIENTES
            countEstOpe.setEstadoOpeP("PENDIENTE");
            countEstOpe.setCantidadP(pendiente);


            /*FIN CUENTA ESTADO OPERACIONES*/

 /*Proceos que Limpia data a desplegar la data =>*/
 /*
            List<TblHitosEnt> hitosTmp = new ArrayList<TblHitosEnt>();
            TblHitosEnt hTmp = new TblHitosEnt();
            // tblHitosList
            for (int z = 0; z < tblHitosList.size(); z++) {
                String ruttmp = "";
                hTmp.setRut_completo(tblHitosList.get(z).getRut_completo());
                ruttmp = hTmp.getRut_completo();
                // System.out.println("ruttmp => "+ruttmp );
                boolean nextRut = true;
                // compruebo que el rut ya no este guardado en el array
                if (tblHitosList.size() > 0) {
                    for (int h = 0; h < tblHitosList.size(); h++) {
                        if (ruttmp.equals(tblHitosList.get(h).getRut_completo())) {
                            nextRut = false;
                        }
                    }
                }
                if (nextRut) {
                    // Se buscan rut iguales y se guardan en arreglo hitosTmp 
                    hitosTmp.clear();
                    int cont = 0;
                    for (int i = 0; i < tblHitosList.size(); i++) {
                        if (ruttmp.equals(tblHitosList.get(i).getRut_completo())) {
                            hitosTmp.add(tblHitosList.get(i));
                            if (cont == 0) {
                                cont = cont + 1;
                                tblHitosList.add(hitosTmp.get(0));
                            }
                        }

                    }
                }

            }
             */

 /*Fin Proceso Limpia data*/
            //************* Inicio Proceso Llena Filtros *************************//
            if (tblHitosList.size() > 0) {

                //************* Inicio llena Ubicacion *************************//
                for (int h = 0; h < tblHitosList.size(); h++) {
                    String ub = tblHitosList.get(h).getUbicacion();
                    if (!"".equals(ub) && ub != null) {
                        if (tblHitosListUbicacion.size() > 0) {
                            int contUbi = 0;
                            for (int t = 0; t < tblHitosListUbicacion.size(); t++) {
                                if (ub.equals(tblHitosListUbicacion.get(t).getUbicacion())) {
                                    contUbi = contUbi + 1;
                                }
                            }

                            if (contUbi == 0) {
                                UbicacionTblHitosEnt uH = new UbicacionTblHitosEnt();
                                uH.setUbicacion(ub);
                                tblHitosListUbicacion.add(uH);
                            }

                        } else {
                            UbicacionTblHitosEnt uHt = new UbicacionTblHitosEnt();
                            uHt.setUbicacion(ub);
                            tblHitosListUbicacion.add(uHt);
                        }

                    }

                }

                //************* Inicio llena Oficina *************************//
                for (int h = 0; h < tblHitosList.size(); h++) {
                    // uH.setUbicacion(tblHitosList.get(h).getUbicacion());
                    String ub = tblHitosList.get(h).getOficina();
                    if (!"".equals(ub) && ub != null) {
                        if (tblHitosListOficinas.size() > 0) {
                            int contUbi = 0;
                            for (int t = 0; t < tblHitosListOficinas.size(); t++) {
                                if (ub.equals(tblHitosListOficinas.get(t).getOficina())) {
                                    contUbi = contUbi + 1;
                                    //t=tblHitosListUbicacion.size();
                                }
                            }

                            if (contUbi == 0) {
                                OficinaTblHitosEnt uH = new OficinaTblHitosEnt();
                                uH.setOficina(ub);
                                tblHitosListOficinas.add(uH);
                            }

                        } else {
                            OficinaTblHitosEnt uHt = new OficinaTblHitosEnt();
                            uHt.setOficina(ub);
                            tblHitosListOficinas.add(uHt);
                        }

                    }

                }

                //************* Inicio llena Regiones *************************//
                for (int h = 0; h < tblHitosList.size(); h++) {
                    String ub = tblHitosList.get(h).getReg();
                    if (!"".equals(ub) && ub != null) {
                        if (regionTblHitos.size() > 0) {
                            int contUbi = 0;
                            for (int t = 0; t < regionTblHitos.size(); t++) {
                                if (ub.equals(regionTblHitos.get(t).getReg())) {
                                    contUbi = contUbi + 1;
                                    //t=tblHitosListUbicacion.size();
                                }
                            }

                            if (contUbi == 0) {
                                RegionTblHitosEnt uH = new RegionTblHitosEnt();
                                uH.setReg(ub);
                                regionTblHitos.add(uH);
                            }

                        } else {
                            RegionTblHitosEnt uHt = new RegionTblHitosEnt();
                            uHt.setReg(ub);
                            regionTblHitos.add(uHt);
                        }

                    }

                }

                //************* Inicio llena Banca *************************//
                for (int h = 0; h < tblHitosList.size(); h++) {
                    String ub = tblHitosList.get(h).getBanca();
                    if (!"".equals(ub) && ub != null) {
                        if (tblHitosListBanca.size() > 0) {
                            int contUbi = 0;
                            for (int t = 0; t < tblHitosListBanca.size(); t++) {
                                if (ub.equals(tblHitosListBanca.get(t).getBanca())) {
                                    contUbi = contUbi + 1;
                                }
                            }

                            if (contUbi == 0) {
                                BancaTblHitosEnt uH = new BancaTblHitosEnt();
                                uH.setBanca(ub);
                                tblHitosListBanca.add(uH);
                            }

                        } else {
                            BancaTblHitosEnt uHt = new BancaTblHitosEnt();
                            uHt.setBanca(ub);
                            tblHitosListBanca.add(uHt);
                        }

                    }

                }

                //************* Inicio llena Producto *************************//
                for (int h = 0; h < tblHitosList.size(); h++) {
                    String ub = tblHitosList.get(h).getProducto();
                    if (!"".equals(ub) && ub != null) {
                        if (listProductos.size() > 0) {
                            int contUbi = 0;
                            for (int t = 0; t < listProductos.size(); t++) {
                                if (ub.equals(listProductos.get(t).getProducto())) {
                                    contUbi = contUbi + 1;
                                }
                            }

                            if (contUbi == 0) {
                                ProductosTblHitosEnt uH = new ProductosTblHitosEnt();
                                uH.setProducto(ub);
                                listProductos.add(uH);
                            }

                        } else {
                            ProductosTblHitosEnt uHt = new ProductosTblHitosEnt();
                            uHt.setProducto(ub);
                            listProductos.add(uHt);
                        }

                    }

                }

            }

            //************* Fin Proceso Llena Filtros *************************//
        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
            System.out.println("TblHitosJDBC.getListTblHitos error al Obtener Lista de Hitos => " + ex.getMessage());

        }

        return tblHitosList;
    }

    public List<UbicacionTblHitosEnt> getUbicacionListTblHitos() {
        return tblHitosListUbicacion;
    }

    public List<OficinaTblHitosEnt> getOficinaListTblHitos() {
        return tblHitosListOficinas;
    }

    public List<RegionTblHitosEnt> getRegionListTblHitos() {

        return regionTblHitos;
    }

    public List<BancaTblHitosEnt> getBancaListTblHitos() {
        return tblHitosListBanca;
    }

    public List<ProductosTblHitosEnt> getProductoListTblHitos() {

        return listProductos;
    }

    public EstOpeHitoCount getCountEstadoOp() {

        return countEstOpe;
    }

}
