/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.DevolucionMapper;
import sisboj.RowMapper.TipoDevolucionMapper;
import sisboj.entidades.DevolucionEnt;
import sisboj.entidades.TipoDevolucionEnt;
import sisboj.entidades.interfaces.DevolucionDAO;

/**
 *
 * @author EXVGUBA
 */
public class DevolucionJDBC implements DevolucionDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public DevolucionJDBC() {

    }

    public List<DevolucionEnt> listDevoluciones(int id_operacion) {

        List<DevolucionEnt> dev = null;
        String SQL = "select * from tb_Sys_Devolucion where di_fk_IdOper =? ";
        try {

            dev = jdbcTemplateObject.query(SQL, new Object[]{id_operacion}, new DevolucionMapper());

            System.out.println("DevolucionEntJDBC.listDevoluciones SQL => " + SQL);

        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///listDevoluciones/// ERR:[" + ex.getMessage() + "]----#######");
        }

        return dev;
    }

    public boolean insert(DevolucionEnt dev) {
        String SQL = "INSERT INTO [dbo].[tb_Sys_Devolucion]\n"
                + "           ([di_fk_IdOper]\n"
                + "           ,[di_fk_IdTipoDev]\n"
                + "           ,[di_fk_IdMotivoDev]\n"
                + "           ,[di_fk_IdSolOrigen]\n"
                + "           ,[di_fk_IdSolActual]\n"
                + "           ,[di_fk_IdEstadoDev]\n"
                //                + "           ,[ddt_FechaCreacion]\n"
                + "           ,[ddt_FechaActualiza]\n"
                + "           ,[dv_ObsOperacion]\n"
                + "           ,[dv_ObsSolicitud]\n"
                + "           ,[di_fk_IdUbiOrigen]\n"
                + "           ,[di_fk_IdUbiDestino])\n"
                + "     VALUES\n"
                + "            (?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                //                + "            ,(CONVERT(datetime,?))\n"
                + "            ,(CONVERT(datetime,?))\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?);";

        try {

            System.out.println("DevolucionJDBC.insert SQL => " + SQL);
            jdbcTemplateObject.update(SQL, new Object[]{
                dev.getDi_fk_IdOper(),
                dev.getDi_fk_IdTipoDev(),
                dev.getDi_fk_IdMotivoDev(),
                dev.getDi_fk_IdSolOrigen(),
                dev.getDi_fk_IdSolActual(),
                dev.getDi_fk_IdEstadoDev(),
                //                dev.getDdt_FechaCreacion(),
                dev.getDdt_FechaActualiza(),
                dev.getDv_ObsOperacion(),
                dev.getDv_ObsSolicitud(),
                dev.getDi_fk_IdUbiOrigen(),
                dev.getDi_fk_IdUbiDestino()
            });
            return true;

        } catch (DataAccessException ex) {
            Messagebox.show("SQL Exception: " + ex.toString());
            return false;
        }
    }

    public List<TipoDevolucionEnt> listTipoDevolucion() {

        List<TipoDevolucionEnt> ltd = null;
        String SQL = "select * from tb_Sys_TipoDevolucion";
        try {

            ltd = jdbcTemplateObject.query(SQL, new TipoDevolucionMapper());

            System.out.println("DevolucionEntJDBC.listTipoDevolucion SQL =>[ " + SQL + " ] ");

        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///listTipoDevolucion/// ERR:[" + ex.getMessage() + "]----#######");
        }

        return ltd;
    }

    //
    public boolean updateObsDevo(String txtObsDevo, int id_sol) {

        try {

            String sql = "UPDATE tb_Sys_Devolucion SET dv_ObsSolicitud =" + txtObsDevo + " WHERE di_IdSolicitud=" + id_sol;
            System.out.println("DevolucionEntJDBC.updateObsDevo => " + sql);
            jdbcTemplateObject.update(sql);
            return true;

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        }

    }

}
