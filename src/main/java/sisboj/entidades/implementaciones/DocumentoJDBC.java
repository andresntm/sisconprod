/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import configuracion.SisBojConf;
import config.CodeQrController;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.DocumentoMapper;
import sisboj.entidades.Documento;
import sisboj.entidades.interfaces.DocumentosDAO;

/**
 *
 * @author excosoc
 */
public class DocumentoJDBC implements DocumentosDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;
    CodeQrController QR = null;
    SisBojConf _bojConfig;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        // log4j.logger.org.springframework.jdbc.core=DEBUG;
    }

    public DocumentoJDBC() {
        _bojConfig = new SisBojConf();

    }

    @Override
    public boolean update(int di_IdDcto, String tb_Campo, Object val) {
        try {
            String SQL = "update tb_Sys_Documento set where di_IdDcto = ?";
            jdbcTemplateObject.update(SQL, val);
            System.out.println("Updated Record with ID = " + val);
            return true;
        } catch (Exception e) {
            //System.out.println("SQL Exception: " + e.toString());
            return false;
        }
    }

    @Override
    public Documento getDocumento(int di_IdDcto) {
        String SQL = "select * from tb_Sys_Documento where di_IdDcto = ?";
        Documento doc = jdbcTemplateObject.queryForObject(SQL, new Object[]{di_IdDcto}, new DocumentoMapper());
        return doc;
    }

    @Override
    public List<Documento> listDocumento(int id_operacion) {

        List<Documento> doc = null;
        String SQL2 = "    select Ope.di_IdOperacion,Ope.dv_CodOperacion,doc.dv_CodBarra,tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                + " from  tb_Sys_Documento doc \n"
                + " left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                + " left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                + " left join [dbo].[tb_Sys_Operacion] as Ope on Ope.di_IdOperacion=doc.di_fk_IdOper \n"
                + " where di_fk_IdOper=? ";
        try {
            
            
            doc = jdbcTemplateObject.query(SQL2, new Object[]{id_operacion}, new DocumentoMapper());

            System.out.println("DocumentoJDBC.listDocumento SQLlistDocumento => " + SQL2);
            
        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///listDocumento/// ERR:[" + ex.getMessage() + "]----#######");
        }

        return doc;
    }

    @Override
    public List<Documento> listDocumentoLotePorBarcode(String Barcode) {

        // String SQL = "select * from tb_Sys_Documento where di_fk_IdOper = ?";
        List<Documento> doc = null;
        String SQL2 = "select Ope.dv_CodOperacion,doc.dv_CodBarra,tp.dv_NomTipDcto,est.dv_NomEstado,doc.* \n"
                + "from tb_Sys_Lote lot \n"
                + "inner join tb_Sys_DetLote dlot on dlot.di_fk_IdLote=lot.di_IdLote \n"
                + "Inner join tb_Sys_DetLotDoc lotdoc on lotdoc.di_fk_IdDetLote=dlot.di_IdDetLote \n"
                + "inner join tb_Sys_Documento doc on doc.di_IdDcto=lotdoc.di_fk_IdDcto \n"
                + "left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=doc.di_fk_TipoDcto \n"
                + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=doc.di_fk_IdEstado \n"
                + "left join [dbo].[tb_Sys_Operacion] as Ope on Ope.di_IdOperacion=doc.di_fk_IdOper \n"
                + "where lot.dv_CodBarra='" + Barcode + "' ";

        _bojConfig.print("DocJDBC linea 96[" + SQL2 + "]");
        try {
            doc = jdbcTemplateObject.query(SQL2, new DocumentoMapper());

        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///listDocumento/// ERR:[" + ex.getMessage() + "]----#######");
        }

        return doc;
    }

   
    @Override
    public boolean insert(Documento dcto) {
        String SQL = "INSERT INTO [dbo].[tb_Sys_Documento]\n"
                //                + "            ([di_IdDcto],\n"
                + "            ([di_fk_IdUbi]\n"
                + "            ,[di_fk_TipoDcto]\n"
                + "            ,[ddt_FechaSalidaBov]\n"
                + "            ,[di_fk_IdEstado]\n"
                + "            ,[dv_CodBarra]\n"
                + "            ,[di_fk_IdOper]\n"
                + "            ,[db_checkOk]\n"
                + "            ,[dv_Glosa]\n"
                + "            ,[db_ApoderadoBanco]\n"
                + "            ,[ddt_FechaSolicitud])\n"
                + "     VALUES\n"
                //                + "            ((select 1 + MAX(di_IdDcto) from tb_Sys_Documento),\n"
                + "            (?\n"
                + "            ,?\n"
                + "            ,(CONVERT(datetime,?))\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,(CONVERT(datetime,?)));";

        try {
            System.out.println("Documento dto getDi_fk_IdUbi() =>" + dcto.getDi_fk_IdUbi());
            System.out.println("Documento dto Di_fk_TipoDcto() =>" + dcto.getDi_fk_TipoDcto());
            System.out.println("Documento dto Ddt_FechaSalidaBov() =>" + dcto.getDdt_FechaSalidaBov());
            System.out.println("Documento dto Di_fk_IdEstado() =>" + dcto.getDi_fk_IdEstado());
            System.out.println("Documento dto Dv_CodBarra() =>" + dcto.getDv_CodBarra());
            System.out.println("Documento dto Di_fk_IdOper() =>" + dcto.getDi_fk_IdOper());
            System.out.println("Documento dto Db_checkOk() =>" + dcto.getDb_checkOk());
            System.out.println("Documento dto Dv_Glosa() =>" + dcto.getDv_Glosa());
            System.out.println("Documento dto Db_ApoderadoBanco() =>" + dcto.getDb_ApoderadoBanco());
            System.out.println("Documento dto Db_ApoderadoBanco() =>" + dcto.getDdt_FechaSolicitud());

            System.out.println("DocumentoJDBC.insert SQL => " + SQL);
            jdbcTemplateObject.update(SQL, new Object[]{
                dcto.getDi_fk_IdUbi(),
                dcto.getDi_fk_TipoDcto(),
                dcto.getDdt_FechaSalidaBov(),
                dcto.getDi_fk_IdEstado(),
                dcto.getDv_CodBarra(),
                dcto.getDi_fk_IdOper(),
                dcto.getDb_checkOk(),
                dcto.getDv_Glosa(),
                dcto.getDb_ApoderadoBanco(),
                dcto.getDdt_FechaSolicitud()
            });
            return true;
        } catch (DataAccessException ex) {
            Messagebox.show("SQL Exception: " + ex.toString());
            return false;
        }
    }

    @Override
    public int getDocumentoPorOperacion(String di_Dioperacion) {
        String SQL = "select count(*) from tb_Sys_Documento as Doc inner join [dbo].[tb_Sys_Operacion] as Ope on (ope.di_IdOperacion=Doc.di_fk_IdOper) where ope.dv_CodOperacion='" + di_Dioperacion + "'";
        int count = 0;
        count = jdbcTemplateObject.queryForInt(SQL);
        
        System.out.println("DetSolicitudJDBC.getDocumentoPorOperacion DocXOperacion => " + SQL);
        return count;
    }

    @Override
    public boolean ActualizaEstadoDocumento(int id_doc, int id_estado) {
        try {
            String SQL = "UPDATE tb_Sys_Documento SET di_fk_IdEstado=" + id_estado + " where di_IdDcto=" + id_doc;
            jdbcTemplateObject.update(SQL);
            System.out.println("ActualizaEstadoDocumento  SQL => " + SQL);
            return true;
        } catch (Exception e) {
            //System.out.println("SQL Exception: " + e.toString());
            return false;
        }
    }

    /**
     * @param id_operacion
     * @return boolean
     */
    public boolean eliminarDocumento(int id_operacion) {
        try {
            String SQL = "DELETE tb_Sys_Documento where di_fk_IdOper=" + id_operacion;

            System.out.println("EliminaDocumento SQL => " + SQL);
            jdbcTemplateObject.execute(SQL);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }
    
    public boolean eliminarDocumentoDevo(int id_operacion,String glosa) {
        try {
            String SQL = "DELETE tb_Sys_Documento where di_fk_IdOper=" + id_operacion + " and dv_Glosa='" + glosa + "'";

            System.out.println("EliminaDocumento SQL => " + SQL);
            jdbcTemplateObject.execute(SQL);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }
    
    
    public boolean eliminarDocumentoByTipGlosa(int id_operacion,int glosa) {
        try {
            String SQL = "DELETE tb_Sys_Documento where di_fk_IdOper=" + id_operacion + " and di_IdDcto='" + glosa + "'";

            System.out.println("EliminaDocumento SQL => " + SQL);
            jdbcTemplateObject.execute(SQL);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

}
