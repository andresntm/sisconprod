/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import sisboj.RowMapper.UbicacionMapper;
import sisboj.entidades.Ubicacion;
import sisboj.entidades.interfaces.UbicacionDAO;

/**
 *
 * @author excosoc
 */
public class UbicacionJDBC implements UbicacionDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }


    public List<Ubicacion> getUbicacion(String param, String val) {
    // ubicacionJDBC.getUbicacion("dv_DetUbi", "UCC");{
        List<Ubicacion> ubicacion = new ArrayList<Ubicacion>();
        String where = "";        
        if (!"".equals(param)) {

            if ("di_idUbicacion".equals(param)) {
                where = "where di_idUbicacion = " + Integer.parseInt(val);
            }
            if ("dv_DetUbi".equals(param)) {
                where = "where dv_DetUbi = '"+val+"'";
            }
        }
        
        String SQL = "select * from tb_Sys_Ubicacion " + where;
        System.out.println("UbicacionJDBC.getUbicacion Ubicacion => " + SQL);
        ubicacion = this.jdbcTemplateObject.query(SQL, new UbicacionMapper());
        return ubicacion;

    }

}
