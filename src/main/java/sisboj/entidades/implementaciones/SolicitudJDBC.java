/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MetodosGenerales;
import configuracion.SisBojConf;
import java.sql.Timestamp;
import config.MvcConfig;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.Messagebox;
import sisboj.entidades.interfaces.SolicitudDAO;
import sisboj.RowMapper.SolicitudMapper;
import sisboj.RowMapper.principalMapper;
import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.Estado;
import sisboj.entidades.Solicitud;

/**
 *
 * @author excosoc
 */
public class SolicitudJDBC implements SolicitudDAO {

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private MetodosGenerales metodo;
    private SisBojConf _bojConfig;
    List<Estado> codEstado;
    EstadoJDBC estJDBC;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    @Override
    public Solicitud getSolicitud(int di_IdSolicitud) {
        String SQL = "SELECT * FROM tb_Sys_Solicitud WHERE di_IdSolicitud = ?";
        Solicitud dsol = null;
        try {
            dsol = jdbcTemplateObject.queryForObject(SQL, new Object[]{di_IdSolicitud}, new SolicitudMapper());
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
        }
        System.out.println("getSolicitud SQL => " + SQL);

        return dsol;
    }

    public SolicitudJDBC() {
        _bojConfig = new SisBojConf();
        estJDBC = new EstadoJDBC();
        codEstado = new ArrayList();
    }

    @Override
    public int getNumeroDocumentosSolicitud(int di_IdSolicitud) {
        int numdocs = 0;
        try {
            String SQL = "SELECT di_CantDctosSolicitados FROM tb_Sys_Solicitud WHERE di_IdSolicitud = " + di_IdSolicitud;
            System.out.println("SolicitudJDBC.getNumeroDocumentosSolicitud SQL => " + SQL);

            numdocs = jdbcTemplateObject.queryForInt(SQL);

        } catch (DataAccessException ex) {
            ex.getMessage();
        }
        return numdocs;
    }

    @Override
    public List<Solicitud> listSolicitud(String SQL) {
        List<Solicitud> sol = jdbcTemplateObject.query(SQL, new SolicitudMapper());
        return sol;
    }

    public boolean updateSolicitud(int di_IdSolicitud, String tb_Campo, int val) {
        try {
            String SQL = "update tb_Sys_Solicitud set " + tb_Campo + "=" + val + " where di_IdSolicitud= " + di_IdSolicitud;
            System.out.println("updateSolicitud SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean updateSolicitudCodeBar(int di_IdSolicitud, String tb_Campo, String val) {
        try {
            String SQL = "update tb_Sys_Solicitud set " + tb_Campo + "='" + val + "' where di_IdSolicitud= " + di_IdSolicitud;
            System.out.println("updateSolicitud SQL => " + SQL);
            jdbcTemplateObject.update(SQL);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public void Insert(Solicitud newSol) {
        metodo = new MetodosGenerales();
        String insertSol = "";
        Timestamp tS = metodo.ConvStringDate(newSol.getDdt_FechaCreacion());
        try {
            insertSol = "INSERT INTO tb_Sys_Solicitud\n"
                    + "           ([di_IdSolicitud]\n"
                    + "           ,[di_fk_IdUbi]\n"
                    + "           ,[di_fk_IdHito]\n"
                    + "           ,[di_fk_IdEstado]\n"
                    + "           ,[ddt_FechaCreacion])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,(CONVERT(datetime,?)))";

            jdbcTemplateObject.update(insertSol, new Object[]{newSol.getDi_IdSolicitud(),
                newSol.getDi_fk_IdUbi(),
                newSol.getDi_fk_IdHito(),
                newSol.getDi_fk_IdEstado(),
                tS,
                0,
                0
            });

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
        }
    }

    public void setJdbc() throws SQLException {
        this.dataSource = new MvcConfig().getDataSourceSisBoj();
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }

    public List<BandejaCustodia> listSolicitud(int proceso) {
        String SQL = "";
        if (proceso == 1) {
            SQL = "SELECT di_IdSolicitud,ubi.dv_DetUbi,est.dv_NomEstado,count(dsol.di_iddetsol) cantidad,dhit.di_IdHito,pro.dv_NomProduc\n"
                    + "FROM tb_Sys_Solicitud sol \n"
                    + "left outer join tb_Sys_DetSolicitud dsol \n"
                    + "on dsol.di_fk_IdSolicitud = sol.di_IdSolicitud \n"
                    + "inner join tb_Sys_DetHito dhit \n"
                    + "on dhit.di_IdDetHito = dsol.di_fk_IdDetHito \n"
                    + "inner join tb_sys_producto pro \n"
                    + "on pro.di_IdProduc = dhit.di_fk_IdProduc \n"
                    + "inner join tb_Sys_Estado est \n"
                    + "on est.di_IdEstado = sol.di_fk_IdEstado \n"
                    + "inner join tb_Sys_Ubicacion ubi\n"
                    + "on ubi.di_IdUbicacion = sol.di_fk_IdUbi\n"
                    + "WHERE (sol.di_fk_IdEstado = 130 OR sol.di_fk_IdEstado = 131) \n"
                    + "AND (dsol.di_fk_IdDcto is null)\n";

            SQL += "GROUP BY di_IdSolicitud,est.dv_NomEstado,di_IdHito,pro.dv_NomProduc,ubi.dv_DetUbi";
        }

        /*else if (proceso == 2){
        
        
        }
        else if (proceso == 3){
        
        }*/
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    /*boj*/
    public List<BandejaCustodia> getSolRecBoj(String ubi_custodia) throws SQLException {
        String SQL;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('ENVCUS','REPRESCUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolRecBoj => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    /* fin boj*/
    public List<BandejaCustodia> getSolDevueltas(String ubi_custodia) throws SQLException {
        String SQL;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        ubi_custodia = "in ('UCC','BACK OFFICE')";
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("NVADEV");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('NVADEV','NVACREP')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolDevueltas => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolDevueltas_Sort(String ubi_custodia, int id_label) throws SQLException {
        String SQL;
        String order = "";
        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        switch (id_label) {
            case 1:
                order = "DI_IDSOLICITUD";
                System.out.println("Order -->> " + order);
                break;
            case 2:
                order = "DV_NOMESTADO";
                System.out.println("Order -->> " + order);
                break;
            case 3:
                order = "DV_DETUBI";
                System.out.println("Order -->> " + order);
                break;
            case 4:
                order = "DV_NOMPRODUC";
                System.out.println("Order -->> " + order);
                break;
            case 5:
                order = "ddt_fechaIngreso";
                System.out.println("Order -->> " + order);
                break;
            case 6:
                order = "DI_IDHITO";
                System.out.println("Order -->> " + order);
                break;
            case 8:
                order = "COUNT(DI_IDDETSOL)";
                System.out.println("Order -->> " + order);
                break;
            default:
                break;
        }
        ubi_custodia = "in ('UCC','BACK OFFICE')";
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("NVADEV");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in('NVADEV','NVACREP')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        if (!"".equals(order)) {
            SQL += " ORDER BY " + order + ";";
        }

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolDevueltas_Sort => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;

    }

    public List<BandejaCustodia> getSolNuev_Pend(String ubi_custodia) throws SQLException {

        String SQL;
        int res = 0;
        int num_sol = 0;
        int cont = 0;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('NSCUS','ASCUS','NUPECUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolNuev_Pend => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());
        List<BandejaCustodia> bandeja2 = dsol;

        /*Codigo para determinar operaciones inhibidas por Numero de solicitud*/
//        for (BandejaCustodia b : bandeja2) {
//
//            num_sol = b.getNumsol();
//            
//            res = getNumeroInhporSol(num_sol);
//           
//            b.setNumOpeInh(res);
//            bandeja2.set(cont, b);
//            cont++;
//        }
        /**/
//        dsol = bandeja2;
        return bandeja2;
    }

    @Override
    public int getNumeroInhporSols(int di_IdSolicitud) throws SQLException {

        int arrayNum = 0;
        //estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        //codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {
            String SQL1 = "SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                    + "FROM TB_SYS_SOLICITUD SOL\n"
                    + "LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                    + "INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                    + "INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                    + "WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23) AND SOL.di_IdSolicitud = " + di_IdSolicitud + "";

            //System.out.println("SolicitudJDBC.getNumeroInhporSolsHilo SQL => " + SQL1);
            arrayNum = jdbcTemplateObject.queryForInt(SQL1);

        } catch (DataAccessException ex) {
            ex.getMessage();
        }
        return arrayNum;
    }

    public List<BandejaCustodia> getSolNuev_Pad(String ubi_custodia, int num_sol) throws SQLException {
        String SQL;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        System.out.println("----->GETSOLNUEVPAD : " + num_sol);

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "     INNER JOIN tb_Sys_JerarquiaSol JSO ON SOL.di_IdSolicitud = JSO.di_fk_IdSolicitudPadre\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('NSCUS','ASCUS','NUPECUS','CSPCUS','ENVCUS','LRECBOJ')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n"
                + "      AND (JSO.di_fk_IdSolicitudHijo = " + num_sol + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolNuev_Pad* SQL => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolNuev_PendSort(String ubi_custodia, int id_label) throws SQLException, InterruptedException {

        String SQL;
        int res = 0;
        int num_sol = 0;
        int cont = 0;
        String order = "";

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        switch (id_label) {
            case 1:
                order = "DI_IDSOLICITUD";
                System.out.println("Order -->> " + order);
                break;
            case 2:
                order = "DV_NOMESTADO";
                System.out.println("Order -->> " + order);
                break;
            case 3:
                order = "DV_DETUBI";
                System.out.println("Order -->> " + order);
                break;
            case 4:
                order = "DV_NOMPRODUC";
                System.out.println("Order -->> " + order);
                break;
            case 5:
                order = "ddt_fechaIngreso";
                System.out.println("Order -->> " + order);
                break;
            case 6:
                order = "DI_IDHITO";
                System.out.println("Order -->> " + order);
                break;
            case 8:
                order = "COUNT(DI_IDDETSOL)";
                System.out.println("Order -->> " + order);
                break;
            default:
                break;
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO  in (1,0)\n"
                + "      AND EST.dv_CodEstado in('NSCUS','ASCUS','NUPECUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        if (!"".equals(order)) {
            SQL += " ORDER BY " + order + ";";
        }

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolNuev_PendSort => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        List<BandejaCustodia> bandeja2 = dsol;
        List<BandejaCustodia> value = null;

        ArrayList<Integer> id_soles = new ArrayList();
        ArrayList<Integer> numInh = new ArrayList();

        /*Codigo para determinar operaciones inhibidas por Numero de solicitud*/
        for (BandejaCustodia b : dsol) {

            num_sol = b.getNumsol();

            id_soles.add(num_sol);
//            HiloInh hilo = new HiloInh(num_sol);
//            hilo.start();
//            hilo.join();
            //value = HiloInh.getValue();

            //System.out.println("DENTRO FOR "+cont);
            res = getNumeroInhporSols(num_sol);
            b.setNumOpeInh(res);
            bandeja2.set(cont, b);
            cont++;
        }

        HiloInh hilo = new HiloInh(id_soles);
        //hilo.start();
        //hilo.join();

        if (hilo.isAlive()) {
            System.out.println("HILO ACTIVO");
            
        } else {
            System.out.println("HILO INACTIVO");
            numInh = HiloInh.getValue();
            System.out.println("GET VALUE HILO -> " + numInh.toString());
            
            for(int i=0;i<numInh.size();i++)
            {
                System.out.println("JOIN "+numInh.get(i));
                 for (BandejaCustodia b : bandeja2) {
                     b.setNumOpeInh(numInh.get(i));
                     
                    
                 }
            }

        }

        /**/
        dsol = bandeja2;
        return bandeja2;
        // return dsol;
    }

    public List<BandejaCustodia> getSolTerminadas(String ubi_custodia) {
        String SQL;
        System.out.println("LLEGA getSolTerminadas");

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('CSPCUS','CSCUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolTerminadas => " + SQL);
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolNuev_TermSort(String ubi_custodia, int id_label) throws SQLException {
        String SQL;
        String order = "";
        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        switch (id_label) {
            case 1:
                order = "DI_IDSOLICITUD";
                System.out.println("Order -->> " + order);
                break;
            case 2:
                order = "DV_NOMESTADO";
                System.out.println("Order -->> " + order);
                break;
            case 3:
                order = "DV_DETUBI";
                System.out.println("Order -->> " + order);
                break;
            case 4:
                order = "DV_NOMPRODUC";
                System.out.println("Order -->> " + order);
                break;
            case 5:
                order = "ddt_fechaIngreso";
                System.out.println("Order -->> " + order);
                break;
            case 6:
                order = "DI_IDHITO";
                System.out.println("Order -->> " + order);
                break;
            case 8:
                order = "COUNT(DI_IDDETSOL)";
                System.out.println("Order -->> " + order);
                break;
            case 9:
                order = "COUNT(ddt_FechaCierre)";
                System.out.println("Order -->> " + order);
                break;
            default:
                break;
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('CSPCUS','CSCUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        if (!"".equals(order)) {
            SQL += " ORDER BY " + order + ";";
        }

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolNuev_TermSort => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;

    }

    public List<BandejaCustodia> getSolEnviadas(String ubi_custodia) {
        String SQL;
        System.out.println("LLEGA getSolEnviadas");

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }
        ubi_custodia = "in ('UCC','BACK OFFICE')";
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('ENVCUS','LRECBOJ')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolEnviadas => " + SQL);
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolEnviadasBojs(String ubi_custodia, String recp) {
        String SQL;
        System.out.println("LLEGA getSolEnviadas");

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        ubi_custodia = "in ('UCC','BACK OFFICE')";
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('ENVCUS','REPRESCUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolEnviadas => " + SQL);
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolRecepcionadas(String ubi_custodia) {
        String SQL;
        System.out.println("LLEGA getSolEnviadas");

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }
        ubi_custodia = "in ('UCC','BACK OFFICE')";
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('LRECBOJ')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolEnviadas => " + SQL);
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public List<BandejaCustodia> getSolNuev_EnvSort(String ubi_custodia, int id_label) throws SQLException {
        String SQL;
        String order = "";
        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        switch (id_label) {
            case 1:
                order = "DI_IDSOLICITUD";
                System.out.println("Order -->> " + order);
                break;
            case 2:
                order = "DV_NOMESTADO";
                System.out.println("Order -->> " + order);
                break;
            case 3:
                order = "DV_DETUBI";
                System.out.println("Order -->> " + order);
                break;
            case 4:
                order = "DV_NOMPRODUC";
                System.out.println("Order -->> " + order);
                break;
            case 5:
                order = "ddt_fechaIngreso";
                System.out.println("Order -->> " + order);
                break;
            case 6:
                order = "DI_IDHITO";
                System.out.println("Order -->> " + order);
                break;
            case 8:
                order = "COUNT(DI_IDDETSOL)";
                System.out.println("Order -->> " + order);
                break;
            case 9:
                order = "COUNT(ddt_FechaEnvio)";
                System.out.println("Order -->> " + order);
                break;
            default:
                break;
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO = 265\n"
                //                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in ('ENVCUS','REPRESCUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        if (!"".equals(order)) {
            SQL += " ORDER BY " + order + ";";
        }

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        System.out.println("SolicitudJDBC.getSolNuev_TermSort => " + SQL);
        //  Messagebox.show("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;

    }

    public List<BandejaCustodia> getSolEnBoj(String ubi_custodia) {
        String SQL;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        SQL = "SELECT di_IdSolicitud,ubi.dv_DetUbi,est.dv_NomEstado,count(dsol.di_iddetsol) cantidad,dhit.di_IdHito,pro.dv_NomProduc\n"
                + "FROM tb_Sys_Solicitud sol \n"
                + "left outer join tb_Sys_DetSolicitud dsol \n"
                + "on dsol.di_fk_IdSolicitud = sol.di_IdSolicitud \n"
                + "inner join tb_Sys_DetHito dhit \n"
                + "on dhit.di_IdDetHito = dsol.di_fk_IdDetHito \n"
                + "inner join tb_sys_producto pro \n"
                + "on pro.di_IdProduc = dhit.di_fk_IdProduc \n"
                + "inner join tb_Sys_Estado est \n"
                + "on est.di_IdEstado = sol.di_fk_IdEstado \n"
                + "inner join tb_Sys_Ubicacion ubi\n"
                + "on ubi.di_IdUbicacion = sol.di_fk_IdUbi\n"
                + "WHERE (sol.di_fk_IdEstado = 133 OR sol.di_fk_IdEstado = 131) \n"
                + "AND (dsol.di_fk_IdDcto is null)\n"
                + "AND (ubi.dv_DetUbi " + ubi_custodia + " )\n";

        SQL += "GROUP BY di_IdSolicitud,est.dv_NomEstado,di_IdHito,pro.dv_NomProduc,ubi.dv_DetUbi";

        _bojConfig.print("DocJDBC linea 238[" + SQL + "]");
        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    //Cierra una Solicitud o la cambia de estado
    public boolean CerrarOCambiarEstadoSolicitud(int id_sol, String accion) {
        // se actualiza la solicitud al valor CSCUS que corresponde al valor cerrado

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date dat = new Date();

        try {
            String est = "";
            String setDate = "";
            boolean respoCrearHijo = false;
            if ("cerrado".equals(accion)) {
                est = "CSCUS";
                setDate = " ,ddt_FechaCierre = '" + df.format(dat) + "'";
            }
            if ("abierto".equals(accion)) {
                est = "ASCUS";
            }
            if ("nuevo".equals(accion)) {
                est = "NSCUS";
            }
            if ("enviado".equals(accion)) {
                est = "ENVCUS";
                setDate = " ,ddt_FechaEnvio = '" + df.format(dat) + "'";
            }
            if ("reparo".equals(accion)) {
                est = "REPCUS";
            }
            if ("recepcionado".equals(accion)) {
                est = "LRECBOJ";
            }
            if ("cerradoConP".equals(accion)) {
                est = "CSPCUS";
                setDate = " ,ddt_FechaCierre = '" + df.format(dat) + "'";
            }
            if ("CerradoNuevaDev".equals(accion)) {
                est = "NVADEV";
                setDate = " ,ddt_FechaCierre = '" + df.format(dat) + "'";
            }
            if ("CerradoNuevaRep".equals(accion)) {
                est = "NVACREP";
                setDate = " ,ddt_FechaCierre = '" + df.format(dat) + "'";
            }
            if ("reparoResuelto".equals(accion)) {
                est = "REPRESCUS";
                setDate = " ,ddt_FechaEnvio = '" + df.format(dat) + "'";
            }

            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo(est);

            String sql = "UPDATE tb_Sys_Solicitud SET di_fk_IdEstado=" + codEstado.get(0).getDi_IdEstado() + setDate + " WHERE di_IdSolicitud=" + id_sol;
            System.out.println("SolicitudJDBC.CerrarOCambioEstadoSolicitud => " + sql);
            jdbcTemplateObject.update(sql);

            return true;

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        } catch (SQLException ex) {
            Messagebox.show(ex.getMessage());
            return false;
        }

        //return true;
    }

    public boolean EnviarBojSolicitud(int id_sol) {
        // se actualiza la solicitud al valor ENVCUS que corresponde al valor cerrado       
        try {

            estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
            codEstado = estJDBC.getIdEstadoByCodigo("ENVCUS");

            String sql = "UPDATE tb_Sys_Solicitud SET di_fk_IdEstado=" + codEstado.get(0).getDi_IdEstado() + " WHERE di_IdSolicitud=" + id_sol;
            System.out.println("SolicitudJDBC.EnviarBojSolicitud => " + sql);
            jdbcTemplateObject.update(sql);
            return true;

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        } catch (SQLException ex) {
            Messagebox.show(ex.toString());
            return false;
        }

        //return true;
    }

    @Override
    public List<Solicitud> getSolicitudXEstado(int di_fk_IdEstado) {
        String SQL = "SELECT * FROM tb_Sys_Solicitud WHERE di_fk_IdEstado = ?";
        List<Solicitud> sol = jdbcTemplateObject.query(SQL, new Object[]{di_fk_IdEstado}, new SolicitudMapper());
        return sol;
    }

    public boolean cambioEstadoSolDet(int idDetSol, String accion, String glosa) throws SQLException {
        // se actualiza la solicitud al valor PENCUS (pendiente cierre)

        //Operacion Completa
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

        if ("C".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("CDSCUS");
        }
        //Operacion a reprocesar
        if ("R".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("INDSCUS");
        }
        //Operacion pendiente
        if ("P".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        }
        //Operacion encontrado
        if ("N".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("NEOCUS");
        }

        if ("RECP".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPREC");
        }

        if ("DEV".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPDEV");
        }

        if ("REP".equals(accion)) {
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPCREP");
        }

        String gl = "";
        if (!"".equals(glosa)) {
            gl = ",dv_EstadoActual='" + glosa + "'";
        }

        String Repo = "En Reproceso";

        System.out.println("cambioEstadoSolDet idDetSol => " + idDetSol);
        String sql = "";
        if ("C".equals(accion)) {
            sql = "UPDATE TB_SYS_DETSOLICITUD SET di_fk_IdEstado=" + codEstado.get(0).getDi_IdEstado() + gl + " WHERE di_fk_IdOper =" + idDetSol;
        } else {
            sql = "UPDATE TB_SYS_DETSOLICITUD SET di_fk_IdEstado=" + codEstado.get(0).getDi_IdEstado() + gl + " WHERE di_IdDetSol=" + idDetSol;
        }
        try {
            System.out.println("sisboj.entidades.implementaciones.SolicitudJDBC.cambioEstadoSolDet => [" + sql + "]");
            jdbcTemplateObject.update(sql);
            return true;

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        }

        //return true;
    }

    /**
     * Solicitudes pendientes con estado PENCUS (pendiente de cierre custodia)
     *
     * @param ubi_custodia
     * @return
     */
    public List<BandejaCustodia> getSolPendientes(String ubi_custodia) throws SQLException {
        String SQL;

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        System.out.println("codEstado ==>=>=> "+codEstado.get(0).getDi_IdEstado());

        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO =" + codEstado.get(0).getDi_IdEstado() + " \n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in('NSCUS','ASCUS','NUPECUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        _bojConfig.print("DocJDBC linea 298[" + SQL + "]");

        System.out.println("SolicitudJDBC.getSolPendientes => " + SQL);

        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    /**
     * Solicitudes pendientes con estado PENCUS (pendiente de cierre custodia)
     *
     * @param ubi_custodia
     * @return
     */
    public List<BandejaCustodia> getSolPendientesSort(String ubi_custodia, int id_label) throws SQLException {
        String SQL;
        String order = "";

        if ("ADMIN".equals(ubi_custodia)) {
            ubi_custodia = " in ('SIN UBICACION','UCC','UAV','TBANC','UCG','SUCURSAL RE','CUSTODIA','BACK OFFICE','ABOGADO','NOVA')";
        } else {
            ubi_custodia = " ='" + ubi_custodia + "'";
        }

        switch (id_label) {
            case 1:
                order = "DI_IDSOLICITUD";
                System.out.println("Order -->> " + order);
                break;
            case 2:
                order = "DV_NOMESTADO";
                System.out.println("Order -->> " + order);
                break;
            case 3:
                order = "DV_DETUBI";
                System.out.println("Order -->> " + order);
                break;
            case 4:
                order = "DV_NOMPRODUC";
                System.out.println("Order -->> " + order);
                break;
            case 5:
                order = "ddt_fechaIngreso";
                System.out.println("Order -->> " + order);
                break;
            case 6:
                order = "DI_IDHITO";
                System.out.println("Order -->> " + order);
                break;
            case 8:
                order = "COUNT(DI_IDDETSOL)";
                System.out.println("Order -->> " + order);
                break;
            default:
                break;
        }

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        SQL = "SELECT SL1.*,SL2.* \n"
                + "FROM("
                + "SELECT DI_IDSOLICITUD,\n"
                + "       UBI.DV_DETUBI,\n"
                + "       EST.DV_NOMESTADO,\n"
                + "       COUNT(DSOL.DI_IDDETSOL) CANTIDAD,\n"
                + "       DHIT.DI_IDHITO,\n"
                + "       PRO.DV_NOMPRODUC,\n"
                + "       DSOL.ddt_fechaIngreso,\n"
                + "       SOL.ddt_FechaCierre,\n"
                + "       SOL.ddt_FechaEnvio,\n"
                + "       SOL.dv_CodeBar\n"
                + "FROM TB_SYS_SOLICITUD SOL\n"
                + "     LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD AND DSOL.DI_FK_IDESTADO =" + codEstado.get(0).getDi_IdEstado() + " \n"
                + "     INNER JOIN TB_SYS_DETHITO DHIT ON DHIT.DI_IDDETHITO = DSOL.DI_FK_IDDETHITO\n"
                + "     INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DI_IDPRODUC = DHIT.DI_FK_IDPRODUC\n"
                + "     INNER JOIN TB_SYS_ESTADO EST ON EST.DI_IDESTADO = SOL.DI_FK_IDESTADO\n"
                + "     INNER JOIN TB_SYS_UBICACION UBI ON UBI.DI_IDUBICACION = SOL.DI_FK_IDUBI\n"
                + "     INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "WHERE TSO.BIT_NUEVO in (1,0)\n"
                + "      AND EST.dv_CodEstado in('NSCUS','ASCUS','NUPECUS')\n"
                + "      AND (DSOL.DI_FK_IDDCTO IS NULL)\n"
                + "      AND (UBI.DV_DETUBI " + ubi_custodia + " )\n";
        //+ "      AND TSO.di_fk_IdProduc =7\n";

        SQL += "GROUP BY DI_IDSOLICITUD,\n"
                + "         EST.DV_NOMESTADO,\n"
                + "         DI_IDHITO,\n"
                + "         PRO.DV_NOMPRODUC,\n"
                + "         UBI.DV_DETUBI,DSOL.ddt_fechaIngreso,\n"
                + "         SOL.ddt_FechaCierre,\n"
                + "         SOL.ddt_FechaEnvio,\n"
                + "         SOL.dv_CodeBar) AS SL1,\n"
                + "	(SELECT COUNT(INH.OPERACION) CANTIDADINH\n"
                + "		FROM TB_SYS_SOLICITUD SOL\n"
                + "				LEFT OUTER JOIN TB_SYS_DETSOLICITUD DSOL ON DSOL.DI_FK_IDSOLICITUD = SOL.DI_IDSOLICITUD\n"
                + "				INNER JOIN TB_SYS_OPERACION TSO ON DSOL.DI_FK_IDOPER = TSO.DI_IDOPERACION\n"
                + "				INNER JOIN IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL INH ON INH.OPERACION = TSO.dv_CodOperacion\n"
                + "		WHERE INH.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)) AS SL2";

        if (!"".equals(order)) {
            SQL += " ORDER BY " + order + ";";
        }

        _bojConfig.print("DocJDBC linea 298[" + SQL + "]");

        System.out.println("SolicitudJDBC.getSolPendientesSort => " + SQL);

        List<BandejaCustodia> dsol = jdbcTemplateObject.query(SQL, new principalMapper());

        return dsol;
    }

    public int[] getNumeroDocSOLYPEN(int di_IdSolicitud) throws SQLException {
        int arrayNum[];
        arrayNum = new int[2];
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {
            String SQL1 = "SELECT isnull(sum(TIPOPE.DI_CANTDCTOSESP),'0') AS CANTDOCSOL "
                    + "FROM DBO.TB_SYS_DETSOLICITUD AS DSOL\n"
                    + "INNER JOIN DBO.TB_SYS_DETHITO AS DHIT ON DSOL.DI_FK_IDDETHITO = DHIT.DI_IDDETHITO\n"
                    + "INNER JOIN DBO.TB_SYS_OPERACION AS OPE ON DHIT.DI_FK_IDOPE = OPE.DI_IDOPERACION\n"
                    + "INNER JOIN DBO.TB_SYS_TIPOOPERACION AS TIPOPE ON(TIPOPE.DV_IDTIPOPE = OPE.DV_FK_IDTIPOPE)\n"
                    + "INNER JOIN DBO.TB_SYS_CLIENTE AS CLI ON OPE.DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE\n"
                    + "INNER JOIN DBO.TB_SYS_PERSONA AS PER ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA\n"
                    + "WHERE DSOL.DI_FK_IDSOLICITUD =" + di_IdSolicitud + " AND DSOL.DI_FK_IDESTADO !=" + codEstado.get(0).getDi_IdEstado();

            System.out.println("SolicitudJDBC.getNumeroDocumentosSolicitud SQL => " + SQL1);

            String SQL2 = "SELECT isnull(sum(TIPOPE.DI_CANTDCTOSESP),'0') AS CANTDOCSOL\n"
                    + "FROM DBO.TB_SYS_DETSOLICITUD AS DSOL\n"
                    + "INNER JOIN DBO.TB_SYS_DETHITO AS DHIT ON DSOL.DI_FK_IDDETHITO = DHIT.DI_IDDETHITO\n"
                    + "INNER JOIN DBO.TB_SYS_OPERACION AS OPE ON DHIT.DI_FK_IDOPE = OPE.DI_IDOPERACION\n"
                    + "INNER JOIN DBO.TB_SYS_TIPOOPERACION AS TIPOPE ON(TIPOPE.DV_IDTIPOPE = OPE.DV_FK_IDTIPOPE)\n"
                    + "INNER JOIN DBO.TB_SYS_CLIENTE AS CLI ON OPE.DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE\n"
                    + "INNER JOIN DBO.TB_SYS_PERSONA AS PER ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA\n"
                    + "WHERE DSOL.DI_FK_IDSOLICITUD =" + di_IdSolicitud + " AND DSOL.DI_FK_IDESTADO =" + codEstado.get(0).getDi_IdEstado();

            System.out.println("SolicitudJDBC.getNumeroDocumentosSolicitud SQL => " + SQL2);

            arrayNum[0] = jdbcTemplateObject.queryForInt(SQL1);
            arrayNum[1] = jdbcTemplateObject.queryForInt(SQL2);

        } catch (DataAccessException ex) {
            ex.getMessage();
        }
        return arrayNum;
    }

    public Solicitud InsertHijoSol(final Solicitud newSol) {
        Solicitud respSol = new Solicitud();
        metodo = new MetodosGenerales();
        KeyHolder key = new GeneratedKeyHolder();

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {

                    String insertSol = "INSERT INTO tb_Sys_Solicitud\n"
                            //                    + "           ([di_IdSolicitud]\n"
                            + "           ([di_fk_IdUbi]\n"
                            + "           ,[di_fk_IdHito]\n"
                            + "           ,[di_fk_IdEstado]\n"
                            + "           ,[ddt_FechaCreacion]\n"
                            + "           ,[di_CantDctosSolicitados])\n"
                            + "     VALUES\n"
                            + "           (?\n"
                            + "           ,?\n"
                            + "           ,?\n"
                            + "           ,(CONVERT(datetime,?))\n"
                            + "           ,?)";

                    PreparedStatement ps = conex.prepareStatement(insertSol, new String[]{"di_IdSolicitud"});
                    int value1 = newSol.getDi_fk_IdUbi();
                    ps.setInt(1, value1);
                    int value2 = newSol.getDi_fk_IdHito();
                    ps.setInt(2, value2);
                    int value3 = newSol.getDi_fk_IdEstado();
                    ps.setInt(3, value3);
                    String value4 = newSol.getDdt_FechaCreacion();
                    ps.setString(4, value4);
                    int value5 = newSol.getDi_cantidadDocs();
                    ps.setInt(5, value5);
                    return ps;
                }
            }, key);

            Number nuKey = key.getKey();
            System.out.println("NUMBER KEY => " + nuKey);

            String SQL2 = "SELECT * FROM tb_Sys_Solicitud WHERE di_IdSolicitud = ?";
            respSol = jdbcTemplateObject.queryForObject(SQL2, new Object[]{nuKey}, new SolicitudMapper());
            System.out.println("InsertHijoSol respSol => " + respSol.getDi_IdSolicitud());

        } catch (Exception e) {
            e.printStackTrace();

        }
        return respSol;

    }

}
