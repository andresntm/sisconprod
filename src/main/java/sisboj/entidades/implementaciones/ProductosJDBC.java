/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.RowMapper.ProductosMapper;
import sisboj.RowMapper.TipoDocumentoMapper;
import sisboj.entidades.Productos;
import sisboj.entidades.TipoDocumento;
import sisboj.entidades.interfaces.ProductosDAO;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class ProductosJDBC implements ProductosDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
//        this.setJdbcTemplateObject(new JdbcTemplate(getDataSource()));
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public List<Productos> getListProductos(String nom) {
       

         String donde = "";
        if (!"".equals(nom) && nom != null) {
            donde = " where LOWER(dv_NomProduc) like '%" + nom.toLowerCase() + "%'";
        }
        String SQL = " select * from tb_Sys_Producto \n"
                + donde;

        System.out.println(" getListProductos => " + SQL);
        List<Productos> Productos = jdbcTemplateObject.query(SQL, new ProductosMapper());
        return Productos;
        
    }

    /**
     * devuelve > 0 si producto ya existe
     *
     * @param nom
     * @return
     */
    public List<Productos> nomProductosExiste(String nom) {
//        int cant;
        String SQL = "select * from tb_Sys_Producto where LOWER(dv_NomProduc)='" + nom.toLowerCase() + "'";
        List<Productos> productos = jdbcTemplateObject.query(SQL, new ProductosMapper());
//        cant = productos.size();
        return productos;
    }

    /**
     * devuelve > 0 si codigo de producto ya existe
     *
     * @param nom
     * @return
     */
    public List<Productos> codProductosExiste(String cod) {
//        int cant;
        String SQL = "select * from tb_Sys_Producto where LOWER(dv_CodProducto)='" + cod.toLowerCase() + "'";
        List<Productos> productos = jdbcTemplateObject.query(SQL, new ProductosMapper());
//        cant = productos.size();
        return productos;
    }

    //Insert Producto
    public boolean insertProducto(final Productos prod) {

        final String SQL = "INSERT INTO [dbo].[tb_Sys_Producto]\n"
                + "           ([dv_NomProduc]\n"
                + "           ,[dv_CodProducto]\n"
                + "           ,[dv_Activo])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"di_IdProduc"});

                    ps.setString(1, prod.getDv_NomProducto());
                    ps.setString(2, prod.getDv_CodProducto());
                    ps.setInt(3, Integer.parseInt(prod.getDv_Activo()));
                    return ps;
                }
            },
                    keyHolder);
            return true;

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            return false;
        }
    }

    //Update Producto
    public boolean updateProducto(Productos prod) {
        String SQL = "UPDATE [dbo].[tb_Sys_Producto]\n"
                + "   SET [dv_NomProduc] = ?\n"
                + "      ,[dv_CodProducto] = ?\n"
                + "      ,[dv_Activo] = ?\n"
                + " WHERE di_IdProduc = ?";
        System.out.println("updateProducto SQL => " + SQL);

        int est = 0;
        if ("Activo".equals(prod.getDv_Activo())) {
            est = 1;
        }
        if ("Inactivo".equals(prod.getDv_Activo())) {
            est = 0;
        }

        try {
            this.jdbcTemplateObject.update(SQL, new Object[]{
                prod.getDv_NomProducto(),
                prod.getDv_CodProducto(),
                est,
                prod.getDi_IdProducto()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public List<Productos> selectProductoByID(int id) {
        String SQL = "select * from tb_Sys_Producto where di_IdProduc=" + id;
        List<Productos> prod = jdbcTemplateObject.query(SQL, new ProductosMapper());
        return prod;
    }

    public List<Productos> selectProductoByName(String name) {
        String SQL = "select * from tb_sys_Producto where dv_NomProduc ='" + name + "'";
        List<Productos> prod = jdbcTemplateObject.query(SQL, new ProductosMapper());
        
         System.out.println(" selectProductoByName => " + SQL);
        return prod;
    }

}
