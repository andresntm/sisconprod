/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MetodosGenerales;
import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;


/**
 *
 * @author excosoc
 */
public class GenericJDBC {
    
    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private MetodosGenerales metodo;
    //private BojTimer timerBoj;
    
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    
    public void setJdbc() throws SQLException{
        this.dataSource = new MvcConfig().getDataSourceSisBoj();
        this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
    }
    
    /*public BojTimer getWeekDay() {
        timerBoj = new BojTimer();

        String SQL = "SELECT DATENAME(dw,GETDATE()) nombreDia,DATEPART(dw,GETDATE()) numDia";
        try {

            timerBoj = (BojTimer) jdbcTemplateObject.query(SQL, new RowMapper() {
                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BojTimer mapProSol = new BojTimer();
                    mapProSol.setnDSemana(rs.getInt("numDia"));
                    mapProSol.setdSemana(rs.getString("nombreDia"));
                    return mapProSol;
                }
            });

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
        }

        return timerBoj;
    }*/
    
    /*public BojTimer getHourNow() {
        timerBoj = new BojTimer();

        String SQL = "select cast(DATEPART(hour, GETDATE()) as int)hora,cast(DATEPART(MINUTE, GETDATE()) as int)minuto";
        try {

            timerBoj = (BojTimer) jdbcTemplateObject.query(SQL, new RowMapper() {
                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    BojTimer mapProSol = new BojTimer();
                    mapProSol.setHora(rs.getInt("hora"));
                    mapProSol.setMinuto(rs.getInt("minuto"));
                    return mapProSol;
                }
            });

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
        }

        return timerBoj;
    }*/
    
    public String getDateNow() {
        String fecha;
        String SQL = "select convert(datetime,getdate(),120) fecha";
        try {
            fecha =(String) jdbcTemplateObject.queryForObject(SQL,new RowMapper() {
                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("fecha");
                }
            });
            return fecha;

        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return "no";
        }

    }
    
}