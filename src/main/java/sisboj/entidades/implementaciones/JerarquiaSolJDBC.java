/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sisboj.entidades.JerarquiaSolEnt;
import sisboj.entidades.interfaces.JerarquiaSolDAO;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class JerarquiaSolJDBC implements JerarquiaSolDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    //Insert Jerarquia
    public boolean insertJerarquiaSol(final JerarquiaSolEnt JerSolEnt) {

        boolean response = false;
        final String insertJer = "INSERT INTO [dbo].[tb_Sys_JerarquiaSol]\n"
                + "           ([di_fk_IdSolicitudPadre]\n"
                + "           ,[di_fk_IdSolicitudHijo])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        KeyHolder key = new GeneratedKeyHolder();

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {

                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {

                    PreparedStatement ps = conex.prepareStatement(insertJer, new String[]{"di_IdJerarquia"});
                    int value1 = JerSolEnt.getDi_fk_IdSolicitudPadre();
                    ps.setInt(1, value1);
                    int value2 = JerSolEnt.getDi_fk_IdSolicitudHijo();
                    ps.setInt(2, value2);
                    return ps;
                }
            }, key);

            response = true;

            /*
        try {
            
            jdbcTemplateObject.update(SQL, new Object[]{
                JerSolEnt.getDi_fk_IdSolicitudPadre(),
                JerSolEnt.getDi_fk_IdSolicitudHijo()
            });
            return true;

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            return false;
        }
    }
             */
        } catch (DataAccessException ex) {
            System.out.println("ERR: [" + ex.getMessage() + "]");
            response = false;
        }
        return response;
    }
}
