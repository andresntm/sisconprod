/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import sisboj.RowMapper.RutasLdapMapper;
import sisboj.entidades.RutasLdap;
import sisboj.entidades.interfaces.LdapDAO;

/**
 *
 * @author EXVGUBA
 */
public class DirLdapJDBC implements LdapDAO{

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    List<RutasLdap> listaLdap = new ArrayList<RutasLdap>();



   public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    public List<RutasLdap> listRutasLdap() {
        String SQL = "select * from SysCon.dbo.tbl_directorio_ldap;";

        System.out.println("Pase por ac� => UsuarioLdapJDBC SQL => " + SQL);
        listaLdap = jdbcTemplateObject.query(SQL, new RutasLdapMapper());

        return listaLdap;
    }
    
}
