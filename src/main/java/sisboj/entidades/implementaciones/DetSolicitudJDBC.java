/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import config.MvcConfig;
import configuracion.SisBojConf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jca.cci.InvalidResultSetAccessException; 
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import sisboj.RowMapper.DetSolicitudMapper;
import sisboj.RowMapper.bandDetMapper;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Estado;
import sisboj.entidades.interfaces.DetSolicitudDAO;

/**
 *
 * @author excosoc
 */
public class DetSolicitudJDBC implements DetSolicitudDAO {

    private JdbcTemplate DetSolJdbc;
    private DataSource dataSource;
    private SisBojConf _bojConfig;
    List<Estado> codEstado;
    EstadoJDBC estJDBC;

    @Override
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.DetSolJdbc = new JdbcTemplate(dataSource);
    }

    public DetSolicitudJDBC() throws SQLException {
        _bojConfig = new SisBojConf();
        codEstado = new ArrayList<Estado>();
        estJDBC = new EstadoJDBC();
    }

    @Override
    public DetSolicitud getDetSolicitud(int di_IdDetSol) {
        String SQL = "select * from dbo.tb_Sys_DetSolicitud where di_IdDetSol = ?";
        DetSolicitud dsol = DetSolJdbc.queryForObject(SQL, new Object[]{di_IdDetSol}, new DetSolicitudMapper());
        return dsol;
    }

    @Override
    public boolean getDetSolicitud(int di_IdDetSol, String dv_CodOperacion) {
        boolean valido = false;
        String SQL = "";
        try {
            SQL = "SELECT COUNT(DS.di_fk_IdOper)  FROM dbo.tb_Sys_DetSolicitud DS \n"
                    + "INNER JOIN dbo.tb_Sys_Operacion OP\n"
                    + "ON DS.di_fk_IdOper = OP.di_IdOperacion \n "
                    + "WHERE OP.dv_CodOperacion = '" + dv_CodOperacion + "'\n"
                    + "AND DS.di_fk_IdSolicitud = " + di_IdDetSol;
            // int v = (int) DetSolJdbc.queryForInt(SQL, new Object[]{di_IdDetSol,dv_CodOperacion}, new RowMapper() {
            int v = (int) DetSolJdbc.queryForInt(SQL);

            //Messagebox.show(SQL);
            if (v == 1) {
                valido = true;
            } else {
                valido = false;
            }
        } catch (DataAccessException ex) {
            Messagebox.show("Error:" + ex.toString());
            valido = false;
        }

        System.out.println("DetSolicitudJDBC.getDetSolicitud Det => " + SQL);
        return valido;

    }

    @Override
    public List<DetSolicitud> listDetSolicitud(String SQL) {
        List<DetSolicitud> dsol = DetSolJdbc.query(SQL, new DetSolicitudMapper());
        return dsol;
    }

    public void setJdbc() throws SQLException {
        this.dataSource = new MvcConfig().getDataSourceSisBoj();
        this.DetSolJdbc = new JdbcTemplate(this.dataSource);
    }

    public List<BandejaDetalle> listDetSolicitud(int solicitud) throws SQLException {
        List<BandejaDetalle> dsol = new ArrayList<BandejaDetalle>();
        String SQL2 = "";

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {
            SQL2 = "SELECT PRI.*,\n"
                    + "       NUMERODOCTOS = ISNULL(SEG.NUMERODOCS, '0'),\n"
                    + "       OPE_INHIBIDA = ISNULL(INH.OPERACION, 'NO')\n"
                    //                    + "       ,CASE WHEN SEG.NUMERODOCS = DI_CANTDCTOSESP THEN 'Completa' ELSE 'Incompleta' END as DV_ESTADOOPE\n"
                    + "FROM\n"
                    + "(\n"
                    + "    SELECT PER.DI_RUT,\n"
                    + "           PER.DC_DIGVER,\n"
                    + "           NOMBRE = ISNULL(PER.DV_NOMBRE+' '+PER.DV_APEPATER+' '+PER.DV_APEMATER, 'NO EXISTE NOMBRE EN DB'),\n"
                    + "           OPE.DV_CODOPERACION,\n"
                    + "           DSOL.DI_IDDETSOL,\n"
                    + "           OPE.DI_IDOPERACION,\n"
                    + "           DP.CANT DI_CANTDCTOSESP,\n"
                    + "           TIPOPE.DV_IDTIPOPE,\n"
                    + "           DSOL.DDT_FECHAINGRESO,\n"
                    + "           DSOL.DI_FK_IDESTADO\n,"
                    + "           EST.DV_CODESTADO,\n"
                    + "           DSOL.DV_ESTADOACTUAL,\n"
                    + "           EST.dv_NomEstado DV_ESTADOOPE\n"
                    + "    FROM dbo.TB_SYS_DETSOLICITUD AS DSOL\n"
                    + "         INNER JOIN dbo.TB_SYS_DETHITO AS DHIT ON DSOL.DI_FK_IDDETHITO = DHIT.DI_IDDETHITO\n"
                    + "         INNER JOIN dbo.TB_SYS_OPERACION AS OPE ON DHIT.DI_FK_IDOPE = OPE.DI_IDOPERACION\n"
                    + "         INNER JOIN dbo.TB_SYS_TIPOOPERACION AS TIPOPE ON(TIPOPE.DV_IDTIPOPE = OPE.DV_FK_IDTIPOPE)\n"
                    + "         INNER JOIN dbo.TB_SYS_CLIENTE AS CLI ON OPE.DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE\n"
                    + "         INNER JOIN dbo.TB_SYS_PERSONA AS PER ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA\n"
                    + "         INNER JOIN dbo.TB_SYS_ESTADO AS EST ON DSOL.di_fk_IdEstado = EST.di_IdEstado\n"
                    + "         JOIN (select ptd.fk_idProducto,pr.dv_CodProducto,count(ptd.fk_idProducto) cant \n"
                    + "					from tb_sys_ProductoTipoDocumento ptd\n"
                    + "					inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto \n"
                    + "					and ptd.bit_Activo=1\n"
                    + "					group by ptd.fk_idProducto,pr.dv_CodProducto\n"
                    + "				) AS DP ON(DP.fk_idProducto = OPE.DI_FK_IDPRODUC)\n"
                    + "    WHERE DSOL.DI_FK_IDSOLICITUD =" + Integer.toString(solicitud) + "\n"
                    //  + "    AND DSOL.DI_FK_IDESTADO !=" + codEstado + "\n"
                    + ") AS PRI\n"
                    + "LEFT JOIN\n"
                    + "(\n"
                    + "    SELECT DOC.DI_FK_IDOPER,\n"
                    + "           COUNT(DOC.DI_FK_IDOPER) AS NUMERODOCS\n"
                    + "    FROM dbo.TB_SYS_DOCUMENTO AS DOC\n"
                    + "    GROUP BY DOC.DI_FK_IDOPER\n"
                    + ") AS SEG ON(SEG.DI_FK_IDOPER = PRI.DI_IDOPERACION)"
                    + "LEFT JOIN (SELECT I.OPERACION\n"
                    + "           FROM IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL I WHERE I.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)\n" 
                    + "		   ) AS INH ON (INH.OPERACION = PRI.DV_CODOPERACION) \n" 
                    + "ORDER BY DI_IDDETSOL";

            _bojConfig.print("DocJDBC linea 96[" + SQL2 + "]");
            System.out.println("DetSolicitudJDBC.listDetSolicitud detalle => " + SQL2);

            dsol = DetSolJdbc.query(SQL2, new bandDetMapper());

        } catch (DataAccessException e) {
            switch (solicitud) {
                case 1:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes nuevas.");
                    break;
                case 2:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes termiandas.");
                    break;
                case 3:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes listas para despacho.");
                    break;
                default:
                    break;
            }
        }

        return dsol;
    }

    public List<BandejaDetalle> listDetBuscaOperSolicitud(String dvoperacion, int solicitud) throws SQLException {
        List<BandejaDetalle> dsol = new ArrayList<BandejaDetalle>();
        String SQL2 = "";

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {
            SQL2 = "SELECT PRI.*,\n"
                    + "       NUMERODOCTOS = ISNULL(SEG.NUMERODOCS, '0'),\n"
                    + "       CASE WHEN SEG.NUMERODOCS = DI_CANTDCTOSESP THEN 'Completa' ELSE 'Incompleta' END as DV_ESTADOOPE,\n"
                    + "       OPE_INHIBIDA = ISNULL(INH.OPERACION, 'NO')\n"
                    + "FROM\n"
                    + "(\n"
                    + "    SELECT PER.DI_RUT,\n"
                    + "           PER.DC_DIGVER,\n"
                    + "           NOMBRE = ISNULL(PER.DV_NOMBRE+' '+PER.DV_APEPATER+' '+PER.DV_APEMATER, 'NO EXISTE NOMBRE EN DB'),\n"
                    + "           OPE.DV_CODOPERACION,\n"
                    + "           DSOL.DI_IDDETSOL,\n"
                    + "           OPE.DI_IDOPERACION,\n"
                    + "           DP.CANT DI_CANTDCTOSESP,\n"
                    + "           TIPOPE.DV_IDTIPOPE,\n"
                    + "           DSOL.DDT_FECHAINGRESO,\n"
                    + "           DSOL.DI_FK_IDESTADO\n,"
                    + "           EST.DV_CODESTADO,\n"
                    + "           DSOL.DV_ESTADOACTUAL\n"
                    + "    FROM dbo.TB_SYS_DETSOLICITUD AS DSOL\n"
                    + "         INNER JOIN dbo.TB_SYS_DETHITO AS DHIT ON DSOL.DI_FK_IDDETHITO = DHIT.DI_IDDETHITO\n"
                    + "         INNER JOIN dbo.TB_SYS_OPERACION AS OPE ON DHIT.DI_FK_IDOPE = OPE.DI_IDOPERACION\n"
                    + "         INNER JOIN dbo.TB_SYS_TIPOOPERACION AS TIPOPE ON(TIPOPE.DV_IDTIPOPE = OPE.DV_FK_IDTIPOPE)\n"
                    + "         INNER JOIN dbo.TB_SYS_CLIENTE AS CLI ON OPE.DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE\n"
                    + "         INNER JOIN dbo.TB_SYS_PERSONA AS PER ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA\n"
                    + "         INNER JOIN dbo.TB_SYS_ESTADO AS EST ON DSOL.di_fk_IdEstado = EST.di_IdEstado\n"
                    + "         JOIN (select ptd.fk_idProducto,pr.dv_CodProducto,count(ptd.fk_idProducto) cant \n"
                    + "					from tb_sys_ProductoTipoDocumento ptd\n"
                    + "					inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto \n"
                    + "					group by ptd.fk_idProducto,pr.dv_CodProducto\n"
                    + "				) AS DP ON(DP.fk_idProducto = OPE.DI_FK_IDPRODUC)\n"
                    + "    WHERE DSOL.DI_FK_IDSOLICITUD =" + Integer.toString(solicitud) + " \n"
                    + "    AND OPE.DV_CODOPERACION ='" + dvoperacion + "'\n"
                    + ") AS PRI\n"
                    + "LEFT JOIN\n"
                    + "(\n"
                    + "    SELECT DOC.DI_FK_IDOPER,\n"
                    + "           COUNT(DOC.DI_FK_IDOPER) AS NUMERODOCS\n"
                    + "    FROM dbo.TB_SYS_DOCUMENTO AS DOC\n"
                    + "    GROUP BY DOC.DI_FK_IDOPER\n"
                    + ") AS SEG ON(SEG.DI_FK_IDOPER = PRI.DI_IDOPERACION)\n"
                    + "LEFT JOIN (SELECT I.OPERACION\n"
                    + "           FROM IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL I WHERE I.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)\n" 
                    + "		   ) AS INH ON (INH.OPERACION = PRI.DV_CODOPERACION) \n" 
                    + "ORDER BY DI_IDDETSOL";

            _bojConfig.print("DocJDBC linea 96[" + SQL2 + "]");
            System.out.println("DetSolicitudJDBC.listDetBuscaOperSolicitud detalle => " + SQL2);

            dsol = DetSolJdbc.query(SQL2, new bandDetMapper());

        } catch (DataAccessException e) {
            switch (solicitud) {
                case 1:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes nuevas.");
                    break;
                case 2:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes termiandas.");
                    break;
                case 3:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes listas para despacho.");
                    break;
                default:
                    break;
            }
        }

        return dsol;
    }

    public int NumeroDocumentosAgregadosSolicitud(int solicitud) throws SQLException {
        int numDocsEnSolic = 0;
        String SQL = "", SQL2 = "";

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {

            SQL2 = "select sum(NumeroDoctos) from (\n"
                    + "select pri.*,NumeroDoctos =	isnull(seg.NumeroDocs,'0')\n"
                    + "from (\n"
                    + "SELECT per.di_Rut, per.dc_DigVer,Nombre =	isnull(per.dv_Nombre + ' ' + per.dv_ApePater + ' ' + per.dv_ApeMater , 'No Existe Nombre en DB') ,ope.dv_CodOperacion,dsol.di_IdDetSol,ope.di_IdOperacion\n"
                    + "FROM dbo.tb_Sys_DetSolicitud AS dsol \n"
                    + "INNER JOIN dbo.tb_Sys_DetHito AS dhit ON dsol.di_fk_IdDetHito = dhit.di_IdDetHito\n"
                    + "INNER JOIN dbo.tb_Sys_Operacion AS ope ON dhit.di_fk_IdOpe = ope.di_IdOperacion \n"
                    + "INNER JOIN dbo.tb_Sys_Cliente AS cli ON ope.di_fk_IdCliente = cli.di_IdCliente \n"
                    + "INNER JOIN dbo.tb_Sys_Persona AS per ON cli.di_fk_IdPersona = per.di_IdPersona \n"
                    + "WHERE \n"
                    + "dsol.di_fk_IdSolicitud = " + Integer.toString(solicitud) + "\n"
                    + "AND DSOL.DI_FK_IDESTADO !=" + codEstado.get(0).getDi_IdEstado() + "\n"
                    + ") as pri\n"
                    + " left join (\n"
                    + "select doc.di_fk_IdOper,count(doc.di_fk_IdOper) AS NumeroDocs from dbo.tb_Sys_Documento AS doc \n"
                    + "group by doc.di_fk_IdOper ) as seg  on (seg.di_fk_IdOper=pri.di_IdOperacion)\n"
                    + ") as HH";
            // "where pri.di_IdDetSol = " + Integer.toString(solicitud) ;

            //  Messagebox.show(SQL2);
            //  id_condonacion = jdbcTemplate.queryForInt(sql);
            System.out.println("DetSolicitudJDBC.NumeroDocumentosAgregadosSolicitud => \n" + SQL2);
            numDocsEnSolic = DetSolJdbc.queryForInt(SQL2);

        } catch (DataAccessException e) {
            if (solicitud == 1) {
                Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes nuevas.");
            } else if (solicitud == 2) {
                Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes termiandas.");
            } else if (solicitud == 3) {
                Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes listas para despacho.");
            }
        }

        return numDocsEnSolic;
    }

    public List<DetSolicitud> SelectDetHitoForDetSol(int di_fk_IdProc, int di_fk_IdUbi) {
        List<DetSolicitud> dsol = null;

        String SQL = "SELECT		dethit.di_IdDetHito di_fk_IdDetHito\n"
                + "FROM		tb_Sys_DetHito dethit\n"
                + "WHERE		dethit.di_fk_IdProduc = " + Integer.toString(di_fk_IdProc) + "\n"
                + "AND            dethit.di_fk_IdUbi = " + Integer.toString(di_fk_IdUbi) + "\n"
                + "AND		dethit.di_IdDetHito not in(SELECT di_fk_IdDetHito FROM tb_Sys_DetSolicitud)";
        try {

            dsol = DetSolJdbc.query(SQL, new RowMapper() {
                @Override
                public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                    DetSolicitud mapDetSol = new DetSolicitud();
                    mapDetSol.setDi_fk_IdDetHito(rs.getInt("di_fk_IdDetHito"));
                    return mapDetSol;
                }
            });

        } catch (Exception e) {
            Messagebox.show(e.toString());
        }

        return dsol;
    }

//    @Override
//    public boolean update(int di_IdDetSol, String tb_Campo, Object val) {
//        try {
//            String SQL = "update tb_Sys_DetSolicitud set where di_IdDetSol = ?";
//            DetSolJdbc.update(SQL, val);
//            return true;
//        } catch (Exception e) {
//            //System.out.println("SQL Exception: " + e.toString());
//            return false;
//        }
//    }
    @Override
    public boolean insert(final DetSolicitud detSol) {
        final String insertDetSol = "INSERT INTO [dbo].[tb_Sys_DetSolicitud]\n"
                + "           ([di_fk_IdSolicitud]\n"
                + "           ,[di_fk_IdUbi]\n"
                + "           ,[di_fk_IdDetHito]\n"
                + "           ,[di_fk_IdOper])\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,?)\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            DetSolJdbc.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(insertDetSol, new String[]{"di_IdDetSol"});
                    ps.setInt(1, detSol.getDi_fk_IdSolicitud());
                    ps.setInt(2, detSol.getDi_fk_IdUbi());
                    ps.setInt(3, detSol.getDi_fk_IdDetHito());
                    ps.setInt(4, detSol.getDi_fk_IdOper());
                    return ps;
                }
            }, key);

            /*insertDetSol,new Object[]{    detSol.getDi_fk_IdSolicitud()
                                                    ,detSol.getDi_fk_IdUbi()
                                                    ,detSol.getDi_fk_IdDetHito()
                                                });*/
            return true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            return false;
        }

    }

    public int getCantOperDetSol(int di_fk_IdSolicitud) {
        int dsol = 0;
        String SQL = "select count(distinct(di_fk_idOper)) from tb_Sys_DetSolicitud where di_fk_IdSolicitud = " + di_fk_IdSolicitud;
        try {
            dsol = DetSolJdbc.queryForInt(SQL);

        } catch (DataAccessException e) {
            Messagebox.show("Error Db[" + e.toString() + "]");
            //return false;
        }

        return dsol;
    }

    public int getIdSolDet(String barcode) {
        int dsol = 0;
        int dsol2 = 0;

        String SQL = "select di_IdOperacion from TB_SYS_OPERACION where dv_CodOperacion = '" + barcode + "'";
        try {
            dsol = DetSolJdbc.queryForInt(SQL);

            String SQL2 = "select di_IdDetSol from TB_SYS_DETSOLICITUD where di_fk_IdOper = '" + dsol + "'";
            //select di_IdDetSol from TB_SYS_DETSOLICITUD where di_fk_IdOper = 1671
            dsol2 = DetSolJdbc.queryForInt(SQL2);
        } catch (DataAccessException e) {
            Messagebox.show("Error Db[" + e.toString() + "]");
            //return false;
        }

        return dsol2;
    }

    @Override
    public List<DetSolicitud> getDetSolicitudXSolicitud(int di_fk_IdSolicitud) {
        String SQL = "SELECT * FROM tb_Sys_DetSolicitud  WHERE di_fk_IdSolicitud= " + di_fk_IdSolicitud;
        List<DetSolicitud> dsol = null;

        try {
            dsol = DetSolJdbc.query(SQL, new DetSolicitudMapper());

        } catch (InvalidResultSetAccessException e) {
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }

        /*} catch (DataAccessException e) {
            Messagebox.show("Error:["+e.toString()+"]Query["+SQL+"");
         
        }*/
        return dsol;
    }

    public List<BandejaDetalle> listDetSolPendientes(int solicitud) throws SQLException {
        List<BandejaDetalle> dsol = new ArrayList<BandejaDetalle>();
        String SQL2 = "";

        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {
            SQL2 = "SELECT PRI.*,\n"
                    + "       NUMERODOCTOS = ISNULL(SEG.NUMERODOCS, '0'),\n"
                    + "       CASE WHEN SEG.NUMERODOCS = DI_CANTDCTOSESP THEN 'Completa' ELSE 'Incompleta' END as DV_ESTADOOPE,\n"
                    + "       OPE_INHIBIDA = ISNULL(INH.OPERACION, 'NO')\n"
                    + "FROM\n"
                    + "(\n"
                    + "    SELECT PER.DI_RUT,\n"
                    + "           PER.DC_DIGVER,\n"
                    + "           NOMBRE = ISNULL(PER.DV_NOMBRE+' '+PER.DV_APEPATER+' '+PER.DV_APEMATER, 'NO EXISTE NOMBRE EN DB'),\n"
                    + "           OPE.DV_CODOPERACION,\n"
                    + "           DSOL.DI_IDDETSOL,\n"
                    + "           OPE.DI_IDOPERACION,\n"
                    + "           DP.CANT DI_CANTDCTOSESP,\n"
                    + "           TIPOPE.DV_IDTIPOPE,\n"
                    + "           DSOL.DDT_FECHAINGRESO,\n"
                    + "           DSOL.DI_FK_IDESTADO,\n"
                    + "           EST.DV_CODESTADO,\n"
                    + "           DSOL.DV_ESTADOACTUAL\n"
                    + "    FROM dbo.TB_SYS_DETSOLICITUD AS DSOL\n"
                    + "         INNER JOIN dbo.TB_SYS_DETHITO AS DHIT ON DSOL.DI_FK_IDDETHITO = DHIT.DI_IDDETHITO\n"
                    + "         INNER JOIN dbo.TB_SYS_OPERACION AS OPE ON DHIT.DI_FK_IDOPE = OPE.DI_IDOPERACION\n"
                    + "         INNER JOIN dbo.TB_SYS_TIPOOPERACION AS TIPOPE ON(TIPOPE.DV_IDTIPOPE = OPE.DV_FK_IDTIPOPE)\n"
                    + "         INNER JOIN dbo.TB_SYS_CLIENTE AS CLI ON OPE.DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE\n"
                    + "         INNER JOIN dbo.TB_SYS_PERSONA AS PER ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA\n"
                    + "         INNER JOIN dbo.TB_SYS_ESTADO AS EST ON DSOL.di_fk_IdEstado = EST.di_IdEstado\n"
                    + "         JOIN (select ptd.fk_idProducto,pr.dv_CodProducto,count(ptd.fk_idProducto) cant \n"
                    + "					from tb_sys_ProductoTipoDocumento ptd\n"
                    + "					inner join tb_Sys_Producto pr ON pr.di_IdProduc = ptd.fk_idProducto \n"
                    + "					group by ptd.fk_idProducto,pr.dv_CodProducto\n"
                    + "				) AS DP ON(DP.fk_idProducto = OPE.DI_FK_IDPRODUC)\n"
                    + "    WHERE DSOL.DI_FK_IDSOLICITUD =" + Integer.toString(solicitud) + "\n"
                    + "    AND DSOL.DI_FK_IDESTADO =" + codEstado.get(0).getDi_IdEstado() + "\n"
                    + ") AS PRI\n"
                    + "LEFT JOIN\n"
                    + "(\n"
                    + "    SELECT DOC.DI_FK_IDOPER,\n"
                    + "           COUNT(DOC.DI_FK_IDOPER) AS NUMERODOCS\n"
                    + "    FROM dbo.TB_SYS_DOCUMENTO AS DOC\n"
                    + "    GROUP BY DOC.DI_FK_IDOPER\n"
                    + ") AS SEG ON(SEG.DI_FK_IDOPER = PRI.DI_IDOPERACION)\n"
                    + "LEFT JOIN (SELECT I.OPERACION\n"
                    + "           FROM IN_CBZA.DBO.TBL_INHIBICION_PREJUDICIAL I WHERE I.FECHA_VENC_INHIBICION >= CONVERT(varchar,getDate(),23)\n" 
                    + "		   ) AS INH ON (INH.OPERACION = PRI.DV_CODOPERACION) \n" 
                    + "ORDER BY DI_IDDETSOL";

            _bojConfig.print("DetSolicitudJDBC.listDetSolPendientes SQL2 [" + SQL2 + "]");
            System.out.println("DetSolicitudJDBC.listDetSolPendientes detalle => " + SQL2);

            dsol = DetSolJdbc.query(SQL2, new bandDetMapper());

        } catch (DataAccessException e) {
            switch (solicitud) {
                case 1:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes nuevas.");
                    break;
                case 2:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes termiandas.");
                    break;
                case 3:
                    Messagebox.show("Sr(a). Usuario(a), no hay registros de solicitudes listas para despacho.");
                    break;
                default:
                    break;
            }
        }

        return dsol;

    }

    public boolean updateOpeSol(int di_IdSolicitud, String tb_Campo, int val) throws SQLException {
        System.out.println("updateOpeSol di_IdSolicitud => " + di_IdSolicitud);
        System.out.println("updateOpeSol tb_Campo => " + tb_Campo);
        System.out.println("updateOpeSol val => " + val);

        String SQL = "";
        List<Estado> estado2 = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

        if ("di_fk_IdSolicitud".equals(tb_Campo)) {
            codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");
        }
        if ("di_fk_IdEstado".equals(tb_Campo)) {
            codEstado = estJDBC.getIdEstadoByCodigo("CDSCUS");
            estado2 = estJDBC.getIdEstadoByCodigo("PENCUS");
        }

        try {
            if ("di_fk_IdSolicitud".equals(tb_Campo)) {
                SQL = "update tb_Sys_DetSolicitud set " + tb_Campo + "=" + val + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado = " + codEstado.get(0).getDi_IdEstado();
            }
            if ("di_fk_IdEstado".equals(tb_Campo)) {
                SQL = "update tb_Sys_DetSolicitud set di_fk_IdEstado=" + codEstado.get(0).getDi_IdEstado() + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado != " + estado2.get(0).getDi_IdEstado() + " ;";
            }

            System.out.println("DetSolicitudJDBC.updateOpeSol SQL [ " + SQL + " ]");
            DetSolJdbc.update(SQL);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean updateOpeSolBoj(int di_IdSolicitud, String tb_Campo, int val, String codOpActual) throws SQLException {
        System.out.println("updateOpeSol di_IdSolicitud => " + di_IdSolicitud);
        System.out.println("updateOpeSol tb_Campo => " + tb_Campo);
        System.out.println("updateOpeSol val => " + val);

        String SQL = "";
        List<Estado> estado2 = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());

        codEstado = estJDBC.getIdEstadoByCodigo("PENCUS");

        try {

            SQL = "update tb_Sys_DetSolicitud set " + tb_Campo + "=" + val + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado = " + codEstado.get(0).getDi_IdEstado();

            System.out.println("DetSolicitudJDBC.updateOpeSol SQL [ " + SQL + " ]");
            DetSolJdbc.update(SQL);
            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean updateIdSolOperacion(int di_IdSolicitud, String tb_Campo, int val) throws SQLException {

        String SQL = "";
        List<Estado> estado2 = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        codEstado = estJDBC.getIdEstadoByCodigo("CDSCUS");

        try {
            SQL = "update tb_Sys_DetSolicitud set " + tb_Campo + "=" + val + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado !=" + codEstado.get(0).getDi_IdEstado();
            System.out.println("DetSolicitudJDBC.updateOpeSol SQL [ " + SQL + " ]");
            DetSolJdbc.update(SQL);

            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean updateIdSolOperacionBoj(int di_IdSolicitud, String tb_Campo, int val, String EstdoTipoDev) throws SQLException {

        String SQL = "";
        List<Estado> estado2 = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        if ("DEV".equals(EstdoTipoDev)) {
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPDEV");
        }
        if ("REP".equals(EstdoTipoDev)) {
            codEstado = estJDBC.getIdEstadoByCodigo("BOJOPCREP");
        }

        try {
            SQL = "update tb_Sys_DetSolicitud set " + tb_Campo + "=" + val + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado =" + codEstado.get(0).getDi_IdEstado();
            System.out.println("DetSolicitudJDBC.updateIdSolOperacionBoj SQL [ " + SQL + " ]");
            DetSolJdbc.update(SQL);

            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean updateIdSolOperacion(int di_IdSolicitud, String tb_Campo, int val, String Estdo) throws SQLException {

        String SQL = "";
        List<Estado> codEst = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());
        if ("COMPLETADAS".equals(Estdo)) {
            codEst = estJDBC.getIdEstadoByCodigo("CDSCUS");
            System.out.println("codEstado = estJDBC.getIdEstadoByCodigo('CDSCUS'); => " + codEst.size());
        }
        if ("PENDIENTES".equals(Estdo)) {
            codEst = estJDBC.getIdEstadoByCodigo("PENCUS");

            System.out.println("codEstado = estJDBC.getIdEstadoByCodigo('PENCUS'); => " + codEst.size());
        }

        try {

            System.out.println("DetSolicitudJDBC.updateIdSolOperacion SQL [ " + SQL + " ]");
            SQL = "update tb_Sys_DetSolicitud set " + tb_Campo + "=" + val + " where di_fk_IdSolicitud= " + di_IdSolicitud + " and di_fk_IdEstado =" + codEst.get(0).getDi_IdEstado();
            DetSolJdbc.update(SQL);

            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean updateEstadoOperacion(String newEstado, int idOpe) throws SQLException {

        String SQL = "";
        List<Estado> estado = new ArrayList<Estado>();
        estJDBC.setDataSource(new MvcConfig().getDataSourceSisBoj());        
        estado = estJDBC.getIdEstadoByCodigo(newEstado);

        try {
            SQL = "update tb_Sys_DetSolicitud set di_fk_IdEstado =" + estado.get(0).getDi_IdEstado() + " where di_fk_IdOper= " + idOpe + " ;";
            System.out.println("DetSolicitudJDBC.updateEstadoOperacion SQL [ " + SQL + " ]");
            DetSolJdbc.update(SQL);

            return true;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

    }

}
