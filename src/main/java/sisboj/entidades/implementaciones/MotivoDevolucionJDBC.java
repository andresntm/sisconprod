/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades.implementaciones;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import sisboj.RowMapper.MotivoDevolucionMapper;
import sisboj.entidades.MotivoDevolucionEnt;
import sisboj.entidades.interfaces.MotivoDevolucionDAO;

/**
 *
 * @author EXVGUBA
 */
public class MotivoDevolucionJDBC implements MotivoDevolucionDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    public void setDataSource(DataSource ds) {

        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public List<MotivoDevolucionEnt> getListMotivoDevolucion(int idTipoDev) {
        String SQL = "select * from tb_Sys_MotivoDevolucion where di_fk_TipoDev= " + idTipoDev + " order by dv_NomMotivoDev;";
        List<MotivoDevolucionEnt> listMotivoDevol = new ArrayList<MotivoDevolucionEnt>();

        System.out.println(" getListMotivoReparo => " + SQL);
        try {
            listMotivoDevol = jdbcTemplateObject.query(SQL, new MotivoDevolucionMapper());
        } catch (Exception e) {
            System.out.println(" Error al obtener listado de motivos de devolucion : " + e.getMessage());
        }

        return listMotivoDevol;

    }

    public List<MotivoDevolucionEnt> getMotivoDevolucionPorNom(String nomMotDev) {
        String SQL = "select * from tb_Sys_MotivoDevolucion where dv_NomMotivoDev= '" + nomMotDev + "' ;";
        List<MotivoDevolucionEnt> listMotivoDevol = new ArrayList<MotivoDevolucionEnt>();

        System.out.println(" MotivoDevolucionJDBC.getMotivoDevolucionPorNom => [ " + SQL + " ]");
        try {
            listMotivoDevol = jdbcTemplateObject.query(SQL, new MotivoDevolucionMapper());
        } catch (Exception e) {
            System.out.println(" Error al obtener listado de motivos de devolucion : " + e.getMessage());
        }

        System.out.println("listMotivoDevol --> "+listMotivoDevol.toString());
        
        return listMotivoDevol;

    }

}
