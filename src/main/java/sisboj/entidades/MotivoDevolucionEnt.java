/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 * ProdTipoDoc
 *
 * @author EXVGUBA
 */
public class MotivoDevolucionEnt {

    private int di_IdMotivoDev;
    private String dv_NomMotivoDev;
    private String dv_CodMotivoDev;
    private int di_fk_TipoDev;
    private String dv_Descripcion;
   // private Date ddt_FechaCreacion; /*Por defecto en BD*/
    private Date ddt_FechaActualiza;
    private int bit_Activo;

    public int getDi_IdMotivoDev() {
        return di_IdMotivoDev;
    }

    public void setDi_IdMotivoDev(int di_IdMotivoDev) {
        this.di_IdMotivoDev = di_IdMotivoDev;
    }

    public String getDv_NomMotivoDev() {
        return dv_NomMotivoDev;
    }

    public void setDv_NomMotivoDev(String dv_NomMotivoDev) {
        this.dv_NomMotivoDev = dv_NomMotivoDev;
    }

    public String getDv_CodMotivoDev() {
        return dv_CodMotivoDev;
    }

    public void setDv_CodMotivoDev(String dv_CodMotivoDev) {
        this.dv_CodMotivoDev = dv_CodMotivoDev;
    }

    public int getDi_fk_TipoDev() {
        return di_fk_TipoDev;
    }

    public void setDi_fk_TipoDev(int di_fk_TipoDev) {
        this.di_fk_TipoDev = di_fk_TipoDev;
    }

    public String getDv_Descripcion() {
        return dv_Descripcion;
    }

    public void setDv_Descripcion(String dv_Descripcion) {
        this.dv_Descripcion = dv_Descripcion;
    }

    public Date getDdt_FechaActualiza() {
        return ddt_FechaActualiza;
    }

    public void setDdt_FechaActualiza(Date ddt_FechaActualiza) {
        this.ddt_FechaActualiza = ddt_FechaActualiza;
    }

    public int getBit_Activo() {
        return bit_Activo;
    }

    public void setBit_Activo(int bit_Activo) {
        this.bit_Activo = bit_Activo;
    }

    @Override
    public String toString() {
        return "MotivoDevolucionEnt{" + "di_IdMotivoDev=" + di_IdMotivoDev + ", dv_NomMotivoDev=" + dv_NomMotivoDev + ", dv_CodMotivoDev=" + dv_CodMotivoDev + ", di_fk_TipoDev=" + di_fk_TipoDev + ", dv_Descripcion=" + dv_Descripcion + ", ddt_FechaActualiza=" + ddt_FechaActualiza + ", bit_Activo=" + bit_Activo + '}';
    }
    
    
}
