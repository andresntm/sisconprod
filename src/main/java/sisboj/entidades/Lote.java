/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import config.MetodosGenerales;
import org.zkoss.zul.Messagebox;


/**
 *
 * @author excosoc
 */
public class Lote {
    
    
    //Atributos DB
    private int di_IdLote;
    private String dv_CodBarra;
    private int di_IdUbi;
    private int di_fk_IdEstado;
    private String ddt_FechaCreacion;
    private int di_CantOper;
    private int di_IdUbi_Salida;
    private String dv_GlosaLote;
    private int di_CantDctos;
    private MetodosGenerales metodo;
    private String dv_CodLote; 
     
    //Atributos Vistuales 
    
    private int N_DocumentosPikeacos;
    private int N_DocumentosEntransito;
    private String Estadolote;
    
    
    
    public int getN_DocumentosPikeacos() {
        return N_DocumentosPikeacos;
    }

    public void setN_DocumentosPikeacos(int N_DocumentosPikeacos) {
        this.N_DocumentosPikeacos = N_DocumentosPikeacos;
    }

    public int getN_DocumentosEntransito() {
        return N_DocumentosEntransito;
    }

    public void setN_DocumentosEntransito(int N_DocumentosEntransito) {
        this.N_DocumentosEntransito = N_DocumentosEntransito;
    }

    public String getEstadolote() {
        return Estadolote;
    }

    public void setEstadolote(String Estadolote) {
        this.Estadolote = Estadolote;
    }
    

    public Lote(){};
    
    public Lote(int di_IdLote,
                String dv_CodBarra,
                int di_IdUbi,
                int di_fk_IdEstado,
                String ddt_FechaCreacion){
        
        this.di_IdLote =di_IdLote;
        this.dv_CodBarra =dv_CodBarra;
        this.di_IdUbi =di_IdUbi;
        this.di_fk_IdEstado =di_fk_IdEstado;
        this.ddt_FechaCreacion =ddt_FechaCreacion;
        
    }
    
    /**
     * @return the di_IdLote
     */
    public int getDi_IdLote() {
        return di_IdLote;
    }

    /**
     * @param di_IdLote the di_IdLote to set
     */
    public void setDi_IdLote(int di_IdLote) {
        this.di_IdLote = di_IdLote;
    }

    /**
     * @return the dv_CodBarra
     */
    public String getDv_CodBarra() {
        return dv_CodBarra;
    }

    /**
     * @param dv_CodBarra the dv_CodBarra to set
     */
    public void setDv_CodBarra(String dv_CodBarra) {
        this.dv_CodBarra = dv_CodBarra;
    }

    /**
     * @return the di_IdUbi
     */
    public int getDi_IdUbi() {
        return di_IdUbi;
    }

    /**
     * @param di_IdUbi the di_IdUbi to set
     */
    public void setDi_IdUbi(int di_IdUbi) {
        this.di_IdUbi = di_IdUbi;
    }

    /**
     * @return the di_fk_IdEstado
     */
    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }

    /**
     * @param di_fk_IdEstado the di_fk_IdEstado to set
     */
    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }

    /**
     * @return the ddt_FechaCreacion
     */
    public String getDdt_FechaCreacion() {
        return ddt_FechaCreacion;
    }

    /**
     * @param ddt_FechaCreacion the ddt_FechaCreacion to set
     */
    public void setDdt_FechaCreacion(String ddt_FechaCreacion) {
        this.ddt_FechaCreacion = ddt_FechaCreacion;
    }
    
    
        /**
     * @param di_CantOper the di_CantOper to set
     */
    public void setDi_CantOper(int di_CantOper) {
        this.di_CantOper = di_CantOper;
    }
    
    
    
        /**
     * @param di_IdUbi_Salida the di_IdUbi_Salida to set
     */
    public void setDi_IdUbi_Salida(int di_IdUbi_Salida) {
        this.di_IdUbi_Salida = di_IdUbi_Salida;
    }
    
        /**
     * @param dv_GlosaLote the dv_GlosaLote to set
     */
    public void setDv_GlosaLote(String dv_GlosaLote) {
        this.dv_GlosaLote = dv_GlosaLote;
    }
    
    
        /**
     * @param di_CantDctos the di_CantDctos to set
     */
    public void setDi_CantDctos(int di_CantDctos) {
        this.di_CantDctos = di_CantDctos;
    }
    
        /**
     * @return the di_CantOper
     */
    public int getDi_CantOper() {
        return di_CantOper;
    }
    
    
        /**
     * @return the di_IdUbi_Salida
     */
    public int getDi_IdUbi_Salida() {
        return di_IdUbi_Salida;
    }
    
    
        /**
     * @return the dv_GlosaLote
     */
    public String getDv_GlosaLote() {
        return dv_GlosaLote;
    }
       /**
     * @return the di_CantDctos
     */
    public int getDi_CantDctos() {
        return di_CantDctos;
    }
    
        /**
     * @param dv_CodLote the dv_CodLote to set
     */
    public void setDv_CodLote(String dv_CodLote) {
        String codLote;
        codLote = "";
        metodo = new MetodosGenerales();
        
        try{
            if(dv_CodLote.equals(null) || dv_CodLote.isEmpty() || dv_CodLote.equals("")){
                codLote = metodo.ConvertId_Into_CodString("L", getDi_IdLote());
                this.dv_CodLote = codLote;
            }else{
                this.dv_CodLote = dv_CodLote;
            }
        }catch(Exception ex){
            Messagebox.show("");
        }
        
    }
    
}
