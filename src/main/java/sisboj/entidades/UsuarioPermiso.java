/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import siscon.entidades.Perfil;

/**
 *
 * @author exesilr
 */
public class UsuarioPermiso implements Serializable {

    private static final long serialVersionUID = 1L;

    String cuenta;
    String nombre;

    public AreaTrabajo getArea() {
        return area;
    }

    public void setArea(AreaTrabajo area) {
        this.area = area;
    }
    AreaTrabajo area;

    int di_rut;
    //Perfil per=new Perfil();
    Set<String> perfiles = new HashSet<String>();

    Set<Perfil> perfiless = new HashSet<Perfil>();

    public UsuarioPermiso(String cuenta, String nombre) {
        this.cuenta = cuenta;
        this.nombre = nombre;
    }

    public UsuarioPermiso(String cuenta, String nombre, int di_rut) {
        this.cuenta = cuenta;
        this.nombre = nombre;
        this.di_rut = di_rut;
    }

    public UsuarioPermiso() {
        this.cuenta = "invitado";
        this.nombre = "invitado";
        perfiles.add("invitado");
    }

    public int getDi_rut() {
        return di_rut;
    }

    public void setDi_rut(int di_rut) {
        this.di_rut = di_rut;
    }

    public boolean isInvitado() {
        return hasRole("invitado") || "invitado".equals(cuenta);
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean hasRole(String role) {
        return perfiles.contains(role);
    }

    public void addRole(String role) {
        perfiles.add(role);
    }

    public boolean TienePerfil(Perfil perfil) {
        return perfiless.contains(perfil);
    }

    public void agregarPerfil(Perfil perfil) {
        perfiless.add(perfil);
    }
}
