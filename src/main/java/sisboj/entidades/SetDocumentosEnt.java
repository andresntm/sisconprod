/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author exvguba
 */
public class SetDocumentosEnt {
    
      private int  di_IdSetDoc;
      private String dv_NomSetDoc;
      private Date ddt_FechaCrea;
      private Date ddt_FechaMod;
      private String dv_Descripcion;
      private int di_fk_IdUbi;
      private String dv_NomTipDcto;
      private int di_fk_IdTipDcto;
      

    public int getDi_IdSetDoc() {
        return di_IdSetDoc;
    }

    public void setDi_IdSetDoc(int di_IdSetDoc) {
        this.di_IdSetDoc = di_IdSetDoc;
    }

    public String getDv_NomSetDoc() {
        return dv_NomSetDoc;
    }

    public void setDv_NomSetDoc(String dv_NomSetDoc) {
        this.dv_NomSetDoc = dv_NomSetDoc;
    }

    public Date getDdt_FechaCrea() {
        return ddt_FechaCrea;
    }

    public void setDdt_FechaCrea(Date ddt_FechaCrea) {
        this.ddt_FechaCrea = ddt_FechaCrea;
    }

    public Date getDdt_FechaMod() {
        return ddt_FechaMod;
    }

    public void setDdt_FechaMod(Date ddt_FechaMod) {
        this.ddt_FechaMod = ddt_FechaMod;
    }

    public String getDv_Descripcion() {
        return dv_Descripcion;
    }

    public void setDv_Descripcion(String dv_Descripcion) {
        this.dv_Descripcion = dv_Descripcion;
    }

    public int getDi_fk_IdUbi() {
        return di_fk_IdUbi;
    }

    public void setDi_fk_IdUbi(int di_fk_IdUbi) {
        this.di_fk_IdUbi = di_fk_IdUbi;
    }

    public String getDv_NomTipDcto() {
        return dv_NomTipDcto;
    }

    public void setDv_NomTipDcto(String dv_NomTipDcto) {
        this.dv_NomTipDcto = dv_NomTipDcto;
    }

    public int getDi_fk_IdTipDcto() {
        return di_fk_IdTipDcto;
    }

    public void setDi_fk_IdTipDcto(int di_fk_IdTipDcto) {
        this.di_fk_IdTipDcto = di_fk_IdTipDcto;
    }

  
      
}
