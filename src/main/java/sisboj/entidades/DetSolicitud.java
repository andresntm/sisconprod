/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class DetSolicitud {

    private int di_IdDetSol;
    private int di_fk_IdUbi;
    private int di_fk_IdSolicitud;
    private int di_fk_IdDcto;
    private int di_fk_IdDetHito;
    private int di_fk_IdOper;
    private int di_fk_IdEstado;
    private String dv_EstadoActual;

    private int contador_documentos;

    public DetSolicitud() {
    }

    ;
    
    public DetSolicitud(int di_IdDetSol, int di_fk_IdSolicitud, int di_fk_IdDcto, int di_fk_IdUbi, int di_fk_IdDetHito, int di_fk_IdOper) {
        this.di_IdDetSol = di_IdDetSol;
        this.di_fk_IdSolicitud = di_fk_IdSolicitud;
        this.di_fk_IdDcto = di_fk_IdDcto;
        this.di_fk_IdUbi = di_fk_IdUbi;
        this.di_fk_IdDetHito = di_fk_IdDetHito;
        this.di_fk_IdOper = di_fk_IdOper;
    }

    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }

    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }

    public int getContador_documentos() {
        return contador_documentos;
    }

    public void setContador_documentos(int contador_documentos) {
        this.contador_documentos = contador_documentos;
    }

    /**
     * @return the di_IdDetSol
     */
    public int getDi_IdDetSol() {
        return di_IdDetSol;
    }

    /**
     * @param di_IdDetSol the di_IdDetSol to set
     */
    public void setDi_IdDetSol(int di_IdDetSol) {
        this.di_IdDetSol = di_IdDetSol;
    }

    /**
     * @return the di_fk_IdSolicitud
     */
    public int getDi_fk_IdSolicitud() {
        return di_fk_IdSolicitud;
    }

    /**
     * @param di_fk_IdSolicitud the di_fk_IdSolicitud to set
     */
    public void setDi_fk_IdSolicitud(int di_fk_IdSolicitud) {
        this.di_fk_IdSolicitud = di_fk_IdSolicitud;
    }

    /**
     * @return the di_fk_IdDcto
     */
    public int getDi_fk_IdDcto() {
        return di_fk_IdDcto;
    }

    /**
     * @param di_fk_IdDcto the di_fk_IdDcto to set
     */
    public void setDi_fk_IdDcto(int di_fk_IdDcto) {
        this.di_fk_IdDcto = di_fk_IdDcto;
    }

    /**
     * @return the di_fk_IdUbi
     */
    public int getDi_fk_IdUbi() {
        return di_fk_IdUbi;
    }

    /**
     * @param di_fk_IdUbi the di_fk_IdUbi to set
     */
    public void setDi_fk_IdUbi(int di_fk_IdUbi) {
        this.di_fk_IdUbi = di_fk_IdUbi;
    }

    /**
     * @return the di_fk_IdDetHito
     */
    public int getDi_fk_IdDetHito() {
        return di_fk_IdDetHito;
    }

    /**
     * @param di_fk_IdDetHito the di_fk_IdDetHito to set
     */
    public void setDi_fk_IdDetHito(int di_fk_IdDetHito) {
        this.di_fk_IdDetHito = di_fk_IdDetHito;
    }

    /**
     * @return the di_fk_IdOper
     */
    public int getDi_fk_IdOper() {
        return di_fk_IdOper;
    }

    /**
     * @param di_fk_IdOper the di_fk_IdOper to set
     */
    public void setDi_fk_IdOper(int di_fk_IdOper) {
        this.di_fk_IdOper = di_fk_IdOper;
    }

    public String getDv_EstadoActual() {
        return dv_EstadoActual;
    }

    public void setDv_EstadoActual(String dv_EstadoActual) {
        this.dv_EstadoActual = dv_EstadoActual;
    }



}
