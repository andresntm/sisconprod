/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 *
 * @author EXVGUBA
 */
public class TipoDocumento {
    
    private int di_IdTipDcto;
    private String dv_NomTipDcto;
    private int di_IdTipDctoPJ;
    private Date ddt_FecIngreso;
    private String bit_Activo;

    public int getDi_IdTipDcto() {
        return di_IdTipDcto;
    }

    public void setDi_IdTipDcto(int di_IdTipDcto) {
        this.di_IdTipDcto = di_IdTipDcto;
    }

    public String getDv_NomTipDcto() {
        return dv_NomTipDcto;
    }

    public void setDv_NomTipDcto(String dv_NomTipDcto) {
        this.dv_NomTipDcto = dv_NomTipDcto;
    }

    public int getDi_IdTipDctoPJ() {
        return di_IdTipDctoPJ;
    }

    public void setDi_IdTipDctoPJ(int di_IdTipDctoPJ) {
        this.di_IdTipDctoPJ = di_IdTipDctoPJ;
    }

    public Date getDdt_FecIngreso() {
        return ddt_FecIngreso;
    }

    public void setDdt_FecIngreso(Date ddt_FecIngreso) {
        this.ddt_FecIngreso = ddt_FecIngreso;
    }

    public String getBit_Activo() {
        return bit_Activo;
    }

    public void setBit_Activo(String bit_Activo) {
        this.bit_Activo = bit_Activo;
    }

    @Override
    public String toString() {
        return "TipoDocumento{" + "di_IdTipDcto=" + di_IdTipDcto + ", dv_NomTipDcto=" + dv_NomTipDcto + ", di_IdTipDctoPJ=" + di_IdTipDctoPJ + ", ddt_FecIngreso=" + ddt_FecIngreso + ", bit_Activo=" + bit_Activo + '}';
    }

    
}
