/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author exesilr
 */
public class AreaTrabajo {

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDesccripcion() {
        return desccripcion;
    }

    public void setDesccripcion(String desccripcion) {
        this.desccripcion = desccripcion;
    }

    public String getFilian() {
        return filian;
    }

    public void setFilian(String filian) {
        this.filian = filian;
    }

    public AreaTrabajo(String codigo, String desccripcion, String filian) {
        this.codigo = codigo;
        this.desccripcion = desccripcion;
        this.filian = filian;
    }

    public AreaTrabajo() {
    }
    
    String codigo;
    String desccripcion;
    String filian;
    
    
}
