/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class Ubicacion {

    private int di_IdUbicacion;
    private String dv_DetUbicacion;

    public Ubicacion() {
    }

    
    public Ubicacion(int di_IdUbicacion, String dv_DetUbicacion) {

        this.di_IdUbicacion = di_IdUbicacion;
        this.dv_DetUbicacion = dv_DetUbicacion;
    }

    public int getDi_IdUbicacion() {
        return di_IdUbicacion;
    }

    public void setDi_IdUbicacion(int di_IdUbicacion) {
        this.di_IdUbicacion = di_IdUbicacion;
    }

    public String getDv_DetUbicacion() {
        return dv_DetUbicacion;
    }

    public void setDv_DetUbicacion(String dv_DetUbicacion) {
        this.dv_DetUbicacion = dv_DetUbicacion;
    }

    

}
