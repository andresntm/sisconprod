/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.entidades.interfaces.TipDctoDAO;

/**
 *
 * @author excosoc
 */
public class TipDcto implements RowMapper<TipDcto> {

    private int di_IdTipDcto;
    private String dv_NomTipDcto;
    private int di_IdTipDctoPJ;
    private Date ddt_FecIngreso;
    private boolean bit_Activo;
    private List<DetTipoDcto> dTD = new ArrayList<DetTipoDcto>();
    private MvcConfig conex;
    private TipDctoDAO tDDAO;

    public TipDcto() throws SQLException {
        InitVariables();
    }

    /**
     * @param di_IdTipDcto int
     * @param dv_NomTipDcto String
     * @param di_IdTipDctoPJ int
     * @param ddt_FecIngreso Date
     */
    public TipDcto(int di_IdTipDcto, String dv_NomTipDcto, int di_IdTipDctoPJ, Date ddt_FecIngreso) throws SQLException {
        InitVariables();

        this.di_IdTipDcto = di_IdTipDcto;
        this.dv_NomTipDcto = dv_NomTipDcto;
        this.di_IdTipDctoPJ = di_IdTipDctoPJ;
        this.ddt_FecIngreso = ddt_FecIngreso;

    }

    /**
     * @param di_IdTipDcto int
     * @param dv_NomTipDcto String
     * @param di_IdTipDctoPJ int
     */
    public TipDcto(int di_IdTipDcto, String dv_NomTipDcto, int di_IdTipDctoPJ) throws SQLException {
        InitVariables();

        this.di_IdTipDcto = di_IdTipDcto;
        this.dv_NomTipDcto = dv_NomTipDcto;
        this.di_IdTipDctoPJ = di_IdTipDctoPJ;

    }

    /**
     * @param di_IdTipDcto int
     * @param dv_NomTipDcto String
     * @param di_IdTipDctoPJ int
     * @param ddt_FecIngreso Date
     * @param dTD List of DetTipoDcto
     */
    public TipDcto(int di_IdTipDcto, String dv_NomTipDcto, int di_IdTipDctoPJ, Date ddt_FecIngreso, List<DetTipoDcto> dTD) throws SQLException {
        InitVariables();

        this.di_IdTipDcto = di_IdTipDcto;
        this.dv_NomTipDcto = dv_NomTipDcto;
        this.di_IdTipDctoPJ = di_IdTipDctoPJ;
        this.ddt_FecIngreso = ddt_FecIngreso;
        this.dTD = dTD;

    }

    private void InitVariables() throws SQLException {
       // conex = new MvcConfig();
       // tDDAO = (TipDctoDAO) new TipDctoJDBC(conex.getDataSourceSisBoj());

    }

    /**
     * @return the di_IdTipDcto
     */
    public int getDi_IdTipDcto() {
        return di_IdTipDcto;
    }

    /**
     * @param di_IdTipDcto the di_IdTipDcto to set
     */
    public void setDi_IdTipDcto(int di_IdTipDcto) {
        this.di_IdTipDcto = di_IdTipDcto;
    }

    /**
     * @return the dv_NomTipDcto
     */
    public String getDv_NomTipDcto() {
        return dv_NomTipDcto;
    }

    /**
     * @param dv_NomTipDcto the dv_NomTipDcto to set
     */
    public void setDv_NomTipDcto(String dv_NomTipDcto) {
        this.dv_NomTipDcto = dv_NomTipDcto;
    }

    /**
     * @return the di_IdTipDctoPJ
     */
    public int getDi_IdTipDctoPJ() {
        return di_IdTipDctoPJ;
    }

    /**
     * @param di_IdTipDctoPJ the di_IdTipDctoPJ to set
     */
    public void setDi_IdTipDctoPJ(int di_IdTipDctoPJ) {
        this.di_IdTipDctoPJ = di_IdTipDctoPJ;
    }

    /**
     * @return the ddt_FecIngreso
     */
    public Date getDdt_FecIngreso() {
        return ddt_FecIngreso;
    }

    /**
     * @param ddt_FecIngreso the ddt_FecIngreso to set
     */
    public void setDdt_FecIngreso(Date ddt_FecIngreso) {
        this.ddt_FecIngreso = ddt_FecIngreso;
    }

    /**
     * @return the bit_Activo
     */
    public boolean isBit_Activo() {
        return bit_Activo;
    }

    /**
     * @param bit_Activo the bit_Activo to set
     */
    public void setBit_Activo(boolean bit_Activo) {

        if (bit_Activo != this.bit_Activo) {
            tDDAO.inactiva_activa(this.di_IdTipDcto, bit_Activo);
        }

        this.bit_Activo = bit_Activo;
    }

    /**
     * @return the dTD
     */
    public List<DetTipoDcto> getdTD() {
        return dTD;
    }

    /**
     * @param dTD the dTD to set
     */
    public void setdTD(List<DetTipoDcto> dTD) {
        this.dTD = dTD;
    }

    public int insert(TipDcto tD) {
        int id_tipDcto = 0;

        id_tipDcto = tDDAO.insert(tD);

        return id_tipDcto;
    }

    public boolean update(TipDcto tD) {
        boolean ok = false;

        ok = tDDAO.update(tD);

        return ok;
    }

    @Override
    public TipDcto mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipDcto dDoc = new TipDcto();
        dDoc.setDi_IdTipDcto(rs.getInt("di_IdTipDcto"));
        dDoc.setDv_NomTipDcto(rs.getString("dv_NomTipDcto"));
        dDoc.setDi_IdTipDctoPJ(rs.getInt("di_IdTipDctoPJ"));
        dDoc.setDdt_FecIngreso(rs.getDate("ddt_FecIngreso"));
        dDoc.setBit_Activo(rs.getBoolean("bit_Activo"));
        return dDoc;
    }

}
