/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author exesilr
 */
public class Hito implements Serializable,Cloneable {
    private static final long serialVersionUID = 1L;
    int id_hito; 
    String rut;
    String Nombre;
    String operacion_original;
    String fecha_hito;
    float monto_hito;
    int dias_mora;
    
    
    	public Hito(int id_hito,String rut,String Nombre, String operacion_original,String fecha_hito, int monto_hito,int dias_mora) {
	this.id_hito = id_hito;	
            this.rut = rut;
             this.Nombre = Nombre;
             this.operacion_original = operacion_original;
             this.fecha_hito=fecha_hito;
	     this.monto_hito = monto_hito;
	     this.dias_mora = dias_mora;
	}
      
}
