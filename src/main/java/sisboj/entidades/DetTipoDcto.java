/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.util.Date;

/**
 *
 * @author excosoc
 */
public class DetTipoDcto {

    /**
     * @return the di_IdDetTipDcto
     */
    public int getDi_IdDetTipDcto() {
        return di_IdDetTipDcto;
    }

    /**
     * @param di_IdDetTipDcto the di_IdDetTipDcto to set
     */
    public void setDi_IdDetTipDcto(int di_IdDetTipDcto) {
        this.di_IdDetTipDcto = di_IdDetTipDcto;
    }

    /**
     * @return the di_fk_IdTipDcto
     */
    public int getDi_fk_IdTipDcto() {
        return di_fk_IdTipDcto;
    }

    /**
     * @param di_fk_IdTipDcto the di_fk_IdTipDcto to set
     */
    public void setDi_fk_IdTipDcto(int di_fk_IdTipDcto) {
        this.di_fk_IdTipDcto = di_fk_IdTipDcto;
    }

    /**
     * @return the dv_DetTipDcto
     */
    public String getDv_DetTipDcto() {
        return dv_DetTipDcto;
    }

    /**
     * @param dv_DetTipDcto the dv_DetTipDcto to set
     */
    public void setDv_DetTipDcto(String dv_DetTipDcto) {
        this.dv_DetTipDcto = dv_DetTipDcto;
    }

    /**
     * @return the ddt_FecIngreso
     */
    public Date getDdt_FecIngreso() {
        return ddt_FecIngreso;
    }

    /**
     * @param ddt_FecIngreso the ddt_FecIngreso to set
     */
    public void setDdt_FecIngreso(Date ddt_FecIngreso) {
        this.ddt_FecIngreso = ddt_FecIngreso;
    }

    private int di_IdDetTipDcto;
    private int di_fk_IdTipDcto;
    private String dv_DetTipDcto;
    private Date ddt_FecIngreso;

    public DetTipoDcto() {
    }
    
    /**
     * 
     * @param di_IdDetTipDcto int
     * @param di_fk_IdTipDcto int
     * @param dv_DetTipDcto  String
     */
    public DetTipoDcto(int di_IdDetTipDcto,
            int di_fk_IdTipDcto,
            String dv_DetTipDcto) {

        this.di_IdDetTipDcto = di_IdDetTipDcto;
        this.di_fk_IdTipDcto = di_fk_IdTipDcto;
        this.dv_DetTipDcto = dv_DetTipDcto;

    }
    
    /**
     * 
     * @param di_IdDetTipDcto int
     * @param di_fk_IdTipDcto int
     * @param dv_DetTipDcto String
     * @param ddt_FecIngreso Date
     */
    public DetTipoDcto(int di_IdDetTipDcto,
            int di_fk_IdTipDcto,
            String dv_DetTipDcto,
            Date ddt_FecIngreso) {

        this.di_IdDetTipDcto = di_IdDetTipDcto;
        this.di_fk_IdTipDcto = di_fk_IdTipDcto;
        this.dv_DetTipDcto = dv_DetTipDcto;
        this.ddt_FecIngreso = ddt_FecIngreso;

    }

}
