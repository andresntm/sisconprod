/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 * ProdTipoDoc
 *
 * @author EXVGUBA
 */
public class ProdTipoDoc {
// ptd.id_prodtipdoc,pr.dv_NomProduc,pr.di_IdProduc, td.dv_NomTipDcto,td.di_IdTipDcto, ptd.bit_Activo

    private int id_ProdTipDoc;
    private String dv_NomProduc;
    private int di_IdProduc;
    private String cod_Producto;
    private String dv_NomTipDcto;
    private int di_IdTipDcto;
    private String bit_activo;
    private String obligatorio;
    // private Date datetime; // auto en BD
    private int cant;

    public int getId_ProdTipDoc() {
        return id_ProdTipDoc;
    }

    public void setId_ProdTipDoc(int id_ProdTipDoc) {
        this.id_ProdTipDoc = id_ProdTipDoc;
    }

    public String getDv_NomProduc() {
        return dv_NomProduc;
    }

    public void setDv_NomProduc(String dv_NomProduc) {
        this.dv_NomProduc = dv_NomProduc;
    }

    public int getDi_IdProduc() {
        return di_IdProduc;
    }

    public void setDi_IdProduc(int di_IdProduc) {
        this.di_IdProduc = di_IdProduc;
    }

    public String getCod_Producto() {
        return cod_Producto;
    }

    public void setCod_Producto(String cod_Producto) {
        this.cod_Producto = cod_Producto;
    }

    public String getDv_NomTipDcto() {
        return dv_NomTipDcto;
    }

    public void setDv_NomTipDcto(String dv_NomTipDcto) {
        this.dv_NomTipDcto = dv_NomTipDcto;
    }

    public int getDi_IdTipDcto() {
        return di_IdTipDcto;
    }

    public void setDi_IdTipDcto(int di_IdTipDcto) {
        this.di_IdTipDcto = di_IdTipDcto;
    }

    public String getBit_activo() {
        return bit_activo;
    }

    public void setBit_activo(String bit_activo) {
        this.bit_activo = bit_activo;
    }

    public String getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(String obligatorio) {
        this.obligatorio = obligatorio;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    @Override
    public String toString() {
        return "ProdTipoDoc{" + "id_ProdTipDoc=" + id_ProdTipDoc + ", dv_NomProduc=" + dv_NomProduc + ", di_IdProduc=" + di_IdProduc + ", cod_Producto=" + cod_Producto + ", dv_NomTipDcto=" + dv_NomTipDcto + ", di_IdTipDcto=" + di_IdTipDcto + ", bit_activo=" + bit_activo + ", obligatorio=" + obligatorio + ", cant=" + cant + '}';
    }    
    

}
