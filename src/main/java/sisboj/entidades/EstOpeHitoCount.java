/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author EXVGUBA
 */
public class EstOpeHitoCount {

    String EstadoOpeN;
    int CantidadN;
    String EstadoOpeP;
    int CantidadP;
    String EstadoOpeF;
    int CantidadF;

    public String getEstadoOpeN() {
        return EstadoOpeN;
    }

    public void setEstadoOpeN(String EstadoOpeN) {
        this.EstadoOpeN = EstadoOpeN;
    }

    public int getCantidadN() {
        return CantidadN;
    }

    public void setCantidadN(int CantidadN) {
        this.CantidadN = CantidadN;
    }

    public String getEstadoOpeP() {
        return EstadoOpeP;
    }

    public void setEstadoOpeP(String EstadoOpeP) {
        this.EstadoOpeP = EstadoOpeP;
    }

    public int getCantidadP() {
        return CantidadP;
    }

    public void setCantidadP(int CantidadP) {
        this.CantidadP = CantidadP;
    }

    public String getEstadoOpeF() {
        return EstadoOpeF;
    }

    public void setEstadoOpeF(String EstadoOpeF) {
        this.EstadoOpeF = EstadoOpeF;
    }

    public int getCantidadF() {
        return CantidadF;
    }

    public void setCantidadF(int CantidadF) {
        this.CantidadF = CantidadF;
    }

    @Override
    public String toString() {
        return "EstOpeHitoCount{" + "EstadoOpeN=" + EstadoOpeN + ", CantidadN=" + CantidadN + ", EstadoOpeP=" + EstadoOpeP + ", CantidadP=" + CantidadP + ", EstadoOpeF=" + EstadoOpeF + ", CantidadF=" + CantidadF + '}';
    }
    
    

}
