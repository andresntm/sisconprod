/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.io.Serializable;

/**
 *
 * @author EXVGUBA
 */
public class RutasLdap  implements Serializable {
    private static final long serialVersionUID = 1L;
    
    
    Integer id;
    String dir;
    String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "RutasLdap{" + "id=" + id + ", dir=" + dir + ", descripcion=" + descripcion + '}';
    }
    
    
    
}
