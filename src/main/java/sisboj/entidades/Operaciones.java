/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author excosoc
 */
public class Operaciones {

    //Datos tabla [tb_Sys_Operacion]
    private int di_IdOperacion;

    public Operaciones(String dv_CodOperacion,
            String dv_CodOperRelacionada,
            int di_fk_IdCliente,
            int di_fk_IdProduc,
            int di_IdEstado,
            int di_DiasMora,
            BigDecimal dm_SaldoInsoluto,
            float df_SaldoInsoUF,
            String dv_TipoMoneda,
            String dv_fk_IdTipOpe,
            int di_fk_IdOficina,
            int di_fk_IdCedente,
            String dv_fk_TipRegion,
            String ddt_FechVencAct,
            String ddt_FechCurse,
            String ddt_FechUltimoPago,
            String dv_fk_IdBanca,
            String dv_ApodoBanca,
            String dv_Garantia,
            String dv_Origen,
            float df_DtdD00,
            String dv_ReneNza,
            String dv_EjecutivoReneNza,
            int di_SucursalNova,
            int di_CanalVentaNova,
            String dv_CodTipCreditoNova,
            String dv_4012Nova,
            int di_FaseNova,
            String dv_CodeBci,
            String dv_CodeGlosaBci,
            String ddt_FechaIngreso) {
        this.dv_CodOperacion = dv_CodOperacion;
        this.dv_CodOperRelacionada = dv_CodOperRelacionada;
        this.di_fk_IdCliente = di_fk_IdCliente;
        this.di_fk_IdProduc = di_fk_IdProduc;
        this.di_IdEstado = di_IdEstado;
        this.di_DiasMora = di_DiasMora;
        this.dm_SaldoInsoluto = dm_SaldoInsoluto;
        this.df_SaldoInsoUF = df_SaldoInsoUF;
        this.dv_TipoMoneda = dv_TipoMoneda;
        this.dv_fk_IdTipOpe = dv_fk_IdTipOpe;
        this.di_fk_IdOficina = di_fk_IdOficina;
        this.di_fk_IdCedente = di_fk_IdCedente;
        this.dv_fk_TipRegion = dv_fk_TipRegion;
        this.ddt_FechVencAct = ddt_FechVencAct;
        this.ddt_FechCurse = ddt_FechCurse;
        this.ddt_FechUltimoPago = ddt_FechUltimoPago;
        this.dv_fk_IdBanca = dv_fk_IdBanca;
        this.dv_ApodoBanca = dv_ApodoBanca;
        this.dv_Garantia = dv_Garantia;
        this.dv_Origen = dv_Origen;
        this.df_DtdD00 = df_DtdD00;
        this.dv_ReneNza = dv_ReneNza;
        this.dv_EjecutivoReneNza = dv_EjecutivoReneNza;
        this.di_SucursalNova = di_SucursalNova;
        this.di_CanalVentaNova = di_CanalVentaNova;
        this.dv_CodTipCreditoNova = dv_CodTipCreditoNova;
        this.dv_4012Nova = dv_4012Nova;
        this.di_FaseNova = di_FaseNova;
        this.dv_CodeBci = dv_CodeBci;
        this.dv_CodeGlosaBci = dv_CodeGlosaBci;
        this.ddt_FechaIngreso = ddt_FechaIngreso;
    }
    
    private String dv_CodOperacion;
    private String dv_CodOperRelacionada;
    private int di_fk_IdCliente;
    private int di_fk_IdProduc;
    private int di_IdEstado;
    private int di_DiasMora;
    private BigDecimal dm_SaldoInsoluto;
    private float df_SaldoInsoUF;
    private String dv_TipoMoneda;
    private String dv_fk_IdTipOpe;
    private int di_fk_IdOficina;
    private int di_fk_IdCedente;
    private String dv_fk_TipRegion;
    private String ddt_FechVencAct;
    private String ddt_FechCurse;
    private String ddt_FechUltimoPago;
    private String dv_fk_IdBanca;
    private String dv_ApodoBanca;
    private String dv_Garantia;
    private String dv_Origen;
    private float df_DtdD00;
    private String dv_ReneNza;
    private String dv_EjecutivoReneNza;
    private int di_SucursalNova;
    private int di_CanalVentaNova;
    private String dv_CodTipCreditoNova;
    private String dv_4012Nova;
    private int di_FaseNova;
    private String dv_CodeBci;
    private String dv_CodeGlosaBci;
    private String ddt_FechaIngreso;

    public Operaciones() {
    }

    private Operaciones(int di_IdOperacion,
            String dv_CodOperacion,
            String dv_CodOperRelacionada,
            int di_fk_IdCliente,
            int di_fk_IdProduc,
            int di_DiasMora,
            BigDecimal dm_SaldoInsoluto,
            float df_SaldoInsoUF,
            String dv_TipoMoneda,
            String dv_fk_IdTipOpe,
            int di_fk_IdOficina,
            int di_fk_IdCedente,
            String dv_fk_TipRegion,
            String ddt_FechVencAct,
            String ddt_FechCurse,
            String ddt_FechUltimoPago,
            String dv_fk_IdBanca,
            String dv_ApodoBanca,
            String dv_Garantia,
            String dv_Origen,
            float df_DtdD00,
            String dv_ReneNza,
            String dv_EjecutivoReneNza,
            String dV_Regional,
            int di_SucursalNova,
            int di_CanalVentaNova,
            String dv_CodTipCreditoNova,
            String dv_4012Nova,
            int di_FaseNova,
            String dv_CodeBci,
            String dv_CodeGlosaBci) {

        this.di_IdOperacion = di_IdOperacion;
        this.dv_CodOperacion = dv_CodOperacion;
        this.dv_CodOperRelacionada = dv_CodOperRelacionada;
        this.di_fk_IdCliente = di_fk_IdCliente;
        this.di_fk_IdProduc = di_fk_IdProduc;
        this.di_DiasMora = di_DiasMora;
        this.dm_SaldoInsoluto = dm_SaldoInsoluto;
        this.df_SaldoInsoUF = df_SaldoInsoUF;
        this.dv_TipoMoneda = dv_TipoMoneda;
        this.dv_fk_IdTipOpe = dv_fk_IdTipOpe;
        this.di_fk_IdOficina = di_fk_IdOficina;
        this.di_fk_IdCedente = di_fk_IdCedente;
        this.dv_fk_TipRegion = dv_fk_TipRegion;
        this.ddt_FechVencAct = ddt_FechVencAct;
        this.ddt_FechCurse = ddt_FechCurse;
        this.ddt_FechUltimoPago = ddt_FechUltimoPago;
        this.dv_fk_IdBanca = dv_fk_IdBanca;
        this.dv_ApodoBanca = dv_ApodoBanca;
        this.dv_Garantia = dv_Garantia;
        this.dv_Origen = dv_Origen;
        this.df_DtdD00 = df_DtdD00;
        this.dv_ReneNza = dv_ReneNza;
        this.dv_EjecutivoReneNza = dv_EjecutivoReneNza;
        this.di_SucursalNova = di_SucursalNova;
        this.di_CanalVentaNova = di_CanalVentaNova;
        this.dv_CodTipCreditoNova = dv_CodTipCreditoNova;
        this.dv_4012Nova = dv_4012Nova;
        this.di_FaseNova = di_FaseNova;
        this.dv_CodeBci = dv_CodeBci;
        this.dv_CodeGlosaBci = dv_CodeGlosaBci;

    }

    public Operaciones(String wD40100720214, String vD40100720214, int i, int i0, int i1, int i2, int i3, String aNULL, String clp, String coN007, int i4, int i5, String rm, String _000000000, String _0000000000, String aNULL0, String pre, String retail, String aNULL1, String aNULL2, String aNULL3, String si, String __________, String aNULL4, String aNULL5, String aNULL6, String aNULL7, String aNULL8, String docpag, String pagare, String _110959810) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Operaciones(String wD40100720214, String vD40100720214, int i, int i0, int i1, int i2, int i3, int i4, String clp, String coN007, int i5, int i6, String rm, String _000000000, String _0000000000, String aNULL, String pre, String retail, String aNULL0, String aNULL1, int i7, String si, String string, int i8, int i9, String aNULL2, String aNULL3, int i10, String docpag, String pagare, String _110959810) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the dv_CodOperacion
     */
    public String getDv_CodOperacion() {
        return dv_CodOperacion;
    }

    /**
     * @param dv_CodOperacion the dv_CodOperacion to set
     */
    public void setDv_CodOperacion(String dv_CodOperacion) {
        this.dv_CodOperacion = dv_CodOperacion;
    }

    /**
     * @return the dv_CodOperRelacionada
     */
    public String getDv_CodOperRelacionada() {
        return dv_CodOperRelacionada;
    }

    /**
     * @param dv_CodOperRelacionada the dv_CodOperRelacionada to set
     */
    public void setDv_CodOperRelacionada(String dv_CodOperRelacionada) {
        this.dv_CodOperRelacionada = dv_CodOperRelacionada;
    }

    /**
     * @return the di_fk_IdCliente
     */
    public int getDi_fk_IdCliente() {
        return di_fk_IdCliente;
    }

    /**
     * @param di_fk_IdCliente the di_fk_IdCliente to set
     */
    public void setDi_fk_IdCliente(int di_fk_IdCliente) {
        this.di_fk_IdCliente = di_fk_IdCliente;
    }

    /**
     * @return the di_fk_IdProduc
     */
    public int getDi_fk_IdProduc() {
        return di_fk_IdProduc;
    }

    /**
     * @param di_fk_IdProduc the di_fk_IdProduc to set
     */
    public void setDi_fk_IdProduc(int di_fk_IdProduc) {
        this.di_fk_IdProduc = di_fk_IdProduc;
    }

    /**
     * @return the di_DiasMora
     */
    public int getDi_DiasMora() {
        return di_DiasMora;
    }

    /**
     * @param di_DiasMora the di_DiasMora to set
     */
    public void setDi_DiasMora(int di_DiasMora) {
        this.di_DiasMora = di_DiasMora;
    }

    /**
     * @return the dm_SaldoInsoluto
     */
    public BigDecimal getDm_SaldoInsoluto() {
        return dm_SaldoInsoluto;
    }

    /**
     * @param dm_SaldoInsoluto the dm_SaldoInsoluto to set
     */
    public void setDm_SaldoInsoluto(BigDecimal dm_SaldoInsoluto) {
        this.dm_SaldoInsoluto = dm_SaldoInsoluto;
    }

    /**
     * @return the df_SaldoInsoUF
     */
    public float getDf_SaldoInsoUF() {
        return df_SaldoInsoUF;
    }

    /**
     * @param df_SaldoInsoUF the df_SaldoInsoUF to set
     */
    public void setDf_SaldoInsoUF(float df_SaldoInsoUF) {
        this.df_SaldoInsoUF = df_SaldoInsoUF;
    }

    /**
     * @return the dv_TipoMoneda
     */
    public String getDv_TipoMoneda() {
        return dv_TipoMoneda;
    }

    /**
     * @param dv_TipoMoneda the dv_TipoMoneda to set
     */
    public void setDv_TipoMoneda(String dv_TipoMoneda) {
        this.dv_TipoMoneda = dv_TipoMoneda;
    }

    /**
     * @return the dv_fk_IdTipOpe
     */
    public String getDv_fk_IdTipOpe() {
        return dv_fk_IdTipOpe;
    }

    /**
     * @param dv_fk_IdTipOpe the dv_fk_IdTipOpe to set
     */
    public void setDv_fk_IdTipOpe(String dv_fk_IdTipOpe) {
        this.dv_fk_IdTipOpe = dv_fk_IdTipOpe;
    }

    /**
     * @return the di_fk_IdOficina
     */
    public int getDi_fk_IdOficina() {
        return di_fk_IdOficina;
    }

    /**
     * @param di_fk_IdOficina the di_fk_IdOficina to set
     */
    public void setDi_fk_IdOficina(int di_fk_IdOficina) {
        this.di_fk_IdOficina = di_fk_IdOficina;
    }

    /**
     * @return the di_fk_IdCedente
     */
    public int getDi_fk_IdCedente() {
        return di_fk_IdCedente;
    }

    /**
     * @param di_fk_IdCedente the di_fk_IdCedente to set
     */
    public void setDi_fk_IdCedente(int di_fk_IdCedente) {
        this.di_fk_IdCedente = di_fk_IdCedente;
    }

    /**
     * @return the dv_fk_TipRegion
     */
    public String getDv_fk_TipRegion() {
        return dv_fk_TipRegion;
    }

    /**
     * @param dv_fk_TipRegion the dv_fk_TipRegion to set
     */
    public void setDv_fk_TipRegion(String dv_fk_TipRegion) {
        this.dv_fk_TipRegion = dv_fk_TipRegion;
    }

    /**
     * @return the ddt_FechVencAct
     */
    public String getDdt_FechVencAct() {
        return ddt_FechVencAct;
    }

    /**
     * @param ddt_FechVencAct the ddt_FechVencAct to set
     */
    public void setDdt_FechVencAct(String ddt_FechVencAct) {
        this.ddt_FechVencAct = ddt_FechVencAct;
    }

    /**
     * @return the ddt_FechCurse
     */
    public String getDdt_FechCurse() {
        return ddt_FechCurse;
    }

    /**
     * @param ddt_FechCurse the ddt_FechCurse to set
     */
    public void setDdt_FechCurse(String ddt_FechCurse) {
        this.ddt_FechCurse = ddt_FechCurse;
    }

    /**
     * @return the ddt_FechUltimoPago
     */
    public String getDdt_FechUltimoPago() {
        return ddt_FechUltimoPago;
    }

    /**
     * @param ddt_FechUltimoPago the ddt_FechUltimoPago to set
     */
    public void setDdt_FechUltimoPago(String ddt_FechUltimoPago) {
        this.ddt_FechUltimoPago = ddt_FechUltimoPago;
    }

    /**
     * @return the dv_fk_IdBanca
     */
    public String getDv_fk_IdBanca() {
        return dv_fk_IdBanca;
    }

    /**
     * @param dv_fk_IdBanca the dv_fk_IdBanca to set
     */
    public void setDv_fk_IdBanca(String dv_fk_IdBanca) {
        this.dv_fk_IdBanca = dv_fk_IdBanca;
    }

    /**
     * @return the dv_ApodoBanca
     */
    public String getDv_ApodoBanca() {
        return dv_ApodoBanca;
    }

    /**
     * @param dv_ApodoBanca the dv_ApodoBanca to set
     */
    public void setDv_ApodoBanca(String dv_ApodoBanca) {
        this.dv_ApodoBanca = dv_ApodoBanca;
    }

    /**
     * @return the dv_Garantia
     */
    public String getDv_Garantia() {
        return dv_Garantia;
    }

    /**
     * @param dv_Garantia the dv_Garantia to set
     */
    public void setDv_Garantia(String dv_Garantia) {
        this.dv_Garantia = dv_Garantia;
    }

    /**
     * @return the dv_Origen
     */
    public String getDv_Origen() {
        return dv_Origen;
    }

    /**
     * @param dv_Origen the dv_Origen to set
     */
    public void setDv_Origen(String dv_Origen) {
        this.dv_Origen = dv_Origen;
    }

    /**
     * @return the df_DtdD00
     */
    public float getDf_DtdD00() {
        return df_DtdD00;
    }

    /**
     * @param df_DtdD00 the df_DtdD00 to set
     */
    public void setDf_DtdD00(float df_DtdD00) {
        this.df_DtdD00 = df_DtdD00;
    }

    /**
     * @return the dv_ReneNza
     */
    public String getDv_ReneNza() {
        return dv_ReneNza;
    }

    /**
     * @param dv_ReneNza the dv_ReneNza to set
     */
    public void setDv_ReneNza(String dv_ReneNza) {
        this.dv_ReneNza = dv_ReneNza;
    }

    /**
     * @return the dv_EjecutivoReneNza
     */
    public String getDv_EjecutivoReneNza() {
        return dv_EjecutivoReneNza;
    }

    /**
     * @param dv_EjecutivoReneNza the dv_EjecutivoReneNza to set
     */
    public void setDv_EjecutivoReneNza(String dv_EjecutivoReneNza) {
        this.dv_EjecutivoReneNza = dv_EjecutivoReneNza;
    }

    /**
     * @return the di_SucursalNova
     */
    public int getDi_SucursalNova() {
        return di_SucursalNova;
    }

    /**
     * @param di_SucursalNova the di_SucursalNova to set
     */
    public void setDi_SucursalNova(int di_SucursalNova) {
        this.di_SucursalNova = di_SucursalNova;
    }

    /**
     * @return the di_CanalVentaNova
     */
    public int getDi_CanalVentaNova() {
        return di_CanalVentaNova;
    }

    /**
     * @param di_CanalVentaNova the di_CanalVentaNova to set
     */
    public void setDi_CanalVentaNova(int di_CanalVentaNova) {
        this.di_CanalVentaNova = di_CanalVentaNova;
    }

    /**
     * @return the dv_CodTipCreditoNova
     */
    public String getDv_CodTipCreditoNova() {
        return dv_CodTipCreditoNova;
    }

    /**
     * @param dv_CodTipCreditoNova the dv_CodTipCreditoNova to set
     */
    public void setDv_CodTipCreditoNova(String dv_CodTipCreditoNova) {
        this.dv_CodTipCreditoNova = dv_CodTipCreditoNova;
    }

    /**
     * @return the dv_4012Nova
     */
    public String getDv_4012Nova() {
        return dv_4012Nova;
    }

    /**
     * @param dv_4012Nova the dv_4012Nova to set
     */
    public void setDv_4012Nova(String dv_4012Nova) {
        this.dv_4012Nova = dv_4012Nova;
    }

    /**
     * @return the di_FaseNova
     */
    public int getDi_FaseNova() {
        return di_FaseNova;
    }

    /**
     * @param di_FaseNova the di_FaseNova to set
     */
    public void setDi_FaseNova(int di_FaseNova) {
        this.di_FaseNova = di_FaseNova;
    }

    /**
     * @return the dv_CodeBci
     */
    public String getDv_CodeBci() {
        return dv_CodeBci;
    }

    /**
     * @param dv_CodeBci the dv_CodeBci to set
     */
    public void setDv_CodeBci(String dv_CodeBci) {
        this.dv_CodeBci = dv_CodeBci;
    }

    /**
     * @return the dv_CodeGlosaBci
     */
    public String getDv_CodeGlosaBci() {
        return dv_CodeGlosaBci;
    }

    /**
     * @param dv_CodeGlosaBci the dv_CodeGlosaBci to set
     */
    public void setDv_CodeGlosaBci(String dv_CodeGlosaBci) {
        this.dv_CodeGlosaBci = dv_CodeGlosaBci;
    }

    /**
     * @return the di_IdEstado
     */
    public int getDi_IdEstado() {
        return di_IdEstado;
    }

    /**
     * @param di_IdEstado the di_IdEstado to set
     */
    public void setDi_IdEstado(int di_IdEstado) {
        this.di_IdEstado = di_IdEstado;
    }

    /**
     * @return the ddt_FechaIngreso
     */
    public String getDdt_FechaIngreso() {
        return ddt_FechaIngreso;
    }

    /**
     * @param ddt_FechaIngreso the ddt_FechaIngreso to set
     */
    public void setDdt_FechaIngreso(String ddt_FechaIngreso) {
        this.ddt_FechaIngreso = ddt_FechaIngreso;
    }

    public String selectOperacion() {
        String SQL;

        SQL = "SELECT [di_IdOperacion]"
                + ",[dv_CodOperacion]"
                + ",[dv_CodOperRelacionada]"
                + ",[di_fk_IdCliente]"
                + ",[di_fk_IdProduc]"
                + ",[di_IdEstado]"
                + ",[di_DiasMora]"
                + ",[dm_SaldoInsoluto]"
                + ",[df_SaldoInsoUF]"
                + ",[dv_TipoMoneda]"
                + ",[dv_fk_IdTipOpe]"
                + ",[di_fk_IdOficina]"
                + ",[di_fk_IdCedente]"
                + ",[dv_fk_TipRegion]"
                + ",[ddt_FechVencAct]"
                + ",[ddt_FechCurse]"
                + ",[ddt_FechUltimoPago]"
                + ",[dv_fk_IdBanca]"
                + ",[dv_ApodoBanca]"
                + ",[dv_Garantia]"
                + ",[dv_Origen]"
                + ",[df_DtdD00]"
                + ",[dv_ReneNza]"
                + ",[dv_EjecutivoReneNza]"
                + ",[di_SucursalNova]"
                + ",[di_CanalVentaNova]"
                + ",[dv_CodTipCreditoNova]"
                + ",[dv_4012Nova]"
                + ",[di_FaseNova]"
                + ",[dv_CodeBci]"
                + ",[dv_CodeGlosaBci]"
                + ",[ddt_FechaIngreso]"
                + "FROM tb_Sys_Operacion ";

        return SQL;
    }

    /**
     * @return the di_IdOperacion
     */
    public int getDi_IdOperacion() {
        return di_IdOperacion;
    }

    /**
     * @param di_IdOperacion the di_IdOperacion to set
     */
    public void setDi_IdOperacion(int di_IdOperacion) {
        this.di_IdOperacion = di_IdOperacion;
    }

}
