/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author exesilr
 */
public class BandejaLote {

    public int getNumDctos() {
        return numDctos;
    }

    public void setNumDctos(int numDctos) {
        this.numDctos = numDctos;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getHito() {
        return hito;
    }

    public void setHito(String hito) {
        this.hito = hito;
    }

    public String getUbacion() {
        return ubacion;
    }

    public void setUbacion(String ubacion) {
        this.ubacion = ubacion;
    }
    private int numDctos;
    private String numLote;
    private String estado;
    private String producto;
    private String hito;
    private String ubacion;

    public BandejaLote() {
    }

    public BandejaLote(int numDctos, String numLote, String estado, String producto, String hito, String ubacion) {
        this.numDctos = numDctos;
        this.numLote = numLote;
        this.estado = estado;
        this.producto = producto;
        this.hito = hito;
        this.ubacion = ubacion;
    }
}
