/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class Productos {
    
    private int di_IdProducto;
    private String dv_NomProducto;
    private String dv_CodProducto;
    private String dv_Activo;

    public int getDi_IdProducto() {
        return di_IdProducto;
    }

    public void setDi_IdProducto(int di_IdProducto) {
        this.di_IdProducto = di_IdProducto;
    }

    public String getDv_NomProducto() {
        return dv_NomProducto;
    }

    public void setDv_NomProducto(String dv_NomProducto) {
        this.dv_NomProducto = dv_NomProducto;
    }

    public String getDv_CodProducto() {
        return dv_CodProducto;
    }

    public void setDv_CodProducto(String dv_CodProducto) {
        this.dv_CodProducto = dv_CodProducto;
    }

    public String getDv_Activo() {
        return dv_Activo;
    }

    public void setDv_Activo(String dv_Activo) {
        this.dv_Activo = dv_Activo;
    }

    @Override
    public String toString() {
        return "Productos{" + "di_IdProducto=" + di_IdProducto + ", dv_NomProducto=" + dv_NomProducto + ", dv_CodProducto=" + dv_CodProducto + ", dv_Activo=" + dv_Activo + '}';
    }


    
}
