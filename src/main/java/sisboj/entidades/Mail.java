package sisboj.entidades;

public class Mail {
    public int index;
    public String subject;
    public String date;
    public int size;

    public Mail(int index, String subject, String date, int size) {
        this.index = index;
        this.subject = subject;
        this.date = date;
        this.size = size;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getsize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    
}
