/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class Solicitud {

    /**
     * @return the di_CantDctosSolicitados
     */
    public int getDi_CantDctosSolicitados() {
        int di_CantDctosSolicitados = 0;
        return di_CantDctosSolicitados;
    }
    private int di_IdSolicitud;
    private int di_fk_IdUbi;
    private int di_fk_IdEstado;
    private int di_fk_IdHito;
    private String ddt_FechaCreacion;
    private int di_fk_Prod;
    private int di_cantidadDocs;
    private int di_cantOpeInh;

    public Solicitud() {
    }

    public Solicitud(int di_IdSolicitud, int di_fk_IdEstado, int di_fk_IdHito, String ddt_FechaCreacion, int di_fk_IdUbi, int di_fk_Prod, int di_cantidadDocs,int di_cantOpeInh) {
        this.di_IdSolicitud = di_IdSolicitud;
        this.di_fk_IdEstado = di_fk_IdEstado;
        this.di_fk_IdHito = di_fk_IdHito;
        this.ddt_FechaCreacion = ddt_FechaCreacion;
        this.di_fk_IdUbi = di_fk_IdUbi;
        this.di_fk_Prod = di_fk_Prod;
        this.di_cantidadDocs = di_cantidadDocs;
        this.di_cantOpeInh = di_cantOpeInh;
    }

    /**
     * @return the di_IdSolicitud
     */
    public int getDi_IdSolicitud() {
        return di_IdSolicitud;
    }

    /**
     * @param di_IdSolicitud the di_IdSolicitud to set
     */
    public void setDi_IdSolicitud(int di_IdSolicitud) {
        this.di_IdSolicitud = di_IdSolicitud;
    }

    /**
     * @return the di_fk_IdEstado
     */
    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }

    /**
     * @param di_fk_IdEstado the di_fk_IdEstado to set
     */
    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }

    /**
     * @return the di_fk_IdHito
     */
    public int getDi_fk_IdHito() {
        return di_fk_IdHito;
    }

    /**
     * @param di_fk_IdHito the di_fk_IdHito to set
     */
    public void setDi_fk_IdHito(int di_fk_IdHito) {
        this.di_fk_IdHito = di_fk_IdHito;
    }

    /**
     * @return the ddt_FechaCreacion
     */
    public String getDdt_FechaCreacion() {
        return ddt_FechaCreacion;
    }

    /**
     * @param ddt_FechaCreacion the ddt_FechaCreacion to set
     */
    public void setDdt_FechaCreacion(String ddt_FechaCreacion) {
        this.ddt_FechaCreacion = ddt_FechaCreacion;
    }

    /**
     * @return the di_fk_IdUbi
     */
    public int getDi_fk_IdUbi() {
        return di_fk_IdUbi;
    }

    /**
     * @param di_fk_IdUbi the di_fk_IdUbi to set
     */
    public void setDi_fk_IdUbi(int di_fk_IdUbi) {
        this.di_fk_IdUbi = di_fk_IdUbi;
    }

    /**
     * @return the di_fk_Prod
     */
    public int getDi_fk_Prod() {
        return di_fk_Prod;
    }

    /**
     * @param di_fk_Prod the di_fk_Prod to set
     */
    public void setDi_fk_Prod(int di_fk_Prod) {
        this.di_fk_Prod = di_fk_Prod;
    }

    public int getDi_cantidadDocs() {
        return di_cantidadDocs;
    }

    public void setDi_cantidadDocs(int di_cantidadDocs) {
        this.di_cantidadDocs = di_cantidadDocs;
    }

    public int getDi_cantOpeInh() {
        return di_cantOpeInh;
    }

    public void setDi_cantOpeInh(int di_cantOpeInh) {
        this.di_cantOpeInh = di_cantOpeInh;
    }
    
    

 

}
