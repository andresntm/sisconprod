/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class DetLote {

    private int di_IdDetLote;
    private int di_fk_IdLote;
    private int di_fk_IdDcto;
    private String dv_GlosaLote;
    private int di_fk_IdOper;
    private int di_fk_IdDetSol;
    private int di_fk_IdEstado;
    
    public DetLote(){}
    
    public DetLote( int di_IdDetLote,
                    int di_fk_IdLote,
                    int di_fk_IdDcto,
                    String dv_GlosaLote){
        
        this.di_IdDetLote = di_IdDetLote;
        this.di_fk_IdLote = di_fk_IdLote;
        this.di_fk_IdDcto = di_fk_IdDcto;
        this.dv_GlosaLote = dv_GlosaLote;
    }
    
    /**
     * @return the di_IdDetLote
     */
    public int getDi_IdDetLote() {
        return di_IdDetLote;
    }

    /**
     * @param di_IdDetLote the di_IdDetLote to set
     */
    public void setDi_IdDetLote(int di_IdDetLote) {
        this.di_IdDetLote = di_IdDetLote;
    }

    /**
     * @return the di_fk_IdLote
     */
    public int getDi_fk_IdLote() {
        return di_fk_IdLote;
    }

    /**
     * @param di_fk_IdLote the di_fk_IdLote to set
     */
    public void setDi_fk_IdLote(int di_fk_IdLote) {
        this.di_fk_IdLote = di_fk_IdLote;
    }

    /**
     * @return the di_fk_IdDcto
     */
    public int getDi_fk_IdDcto() {
        return di_fk_IdDcto;
    }

    /**
     * @param di_fk_IdDcto the di_fk_IdDcto to set
     */
    public void setDi_fk_IdDcto(int di_fk_IdDcto) {
        this.di_fk_IdDcto = di_fk_IdDcto;
    }

    /**
     * @return the dv_GlosaLote
     */
    public String getDv_GlosaLote() {
        return dv_GlosaLote;
    }

    /**
     * @param dv_GlosaLote the dv_GlosaLote to set
     */
    public void setDv_GlosaLote(String dv_GlosaLote) {
        this.dv_GlosaLote = dv_GlosaLote;
    }
    
    
    
        /**
     * @param di_fk_IdOper the di_fk_IdOper to set
     */
    public void setDi_fk_IdOper(int di_fk_IdOper) {
        this.di_fk_IdOper = di_fk_IdOper;
    }
    
        /**
     * @param di_fk_IdDetSol the di_fk_IdDetSol to set
     */
    public void setDi_fk_IdDetSol(int di_fk_IdDetSol) {
        this.di_fk_IdDetSol = di_fk_IdDetSol;
    }
    
    
        /**
     * @param di_fk_IdEstado the di_fk_IdEstado to set
     */
    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }
        /**
     * @return the di_fk_IdOper
     */
    public int getDi_fk_IdOper() {
        return di_fk_IdOper;
    }
        /**
     * @return the di_fk_IdDetSol
     */
    public int getDi_fk_IdDetSol() {
        return di_fk_IdDetSol;
    }
    
        /**
     * @return the di_fk_IdEstado
     */
    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }
    
}
