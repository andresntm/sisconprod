/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author excosoc
 */
public class Documento {

    private int di_IdDcto;
    private int di_fk_IdUbi;
    private int di_fk_TipoDcto;
    private String ddt_FechaSalidaBov;
    private int di_fk_IdEstado;
    private String dv_CodBarra;
    private int di_fk_IdOper;
    private boolean db_checkOk;
    private String dv_Glosa;
    private boolean db_ApoderadoBanco;
    private String _tipoDocumentoGlosa;
    private String _estadoGlosa;
    private String CodigoOperacion;
    private String ddt_FechaSolicitud;

    public Documento() {
    }

    public Documento(int di_IdDcto,
            int di_fk_IdUbi,
            int di_fk_TipoDcto,
            String ddt_FechaSalidaBov,
            int di_fk_IdEstado,
            String dv_CodBarra,
            int di_fk_IdOper,
            boolean db_checkOk,
            String dv_Glosa,
            boolean db_ApoderadoBanco,
            String ddt_FechaSolicitud) {

        this.di_IdDcto = di_IdDcto;
        this.di_fk_IdUbi = di_fk_IdUbi;
        this.di_fk_TipoDcto = di_fk_TipoDcto;
        this.ddt_FechaSalidaBov = ddt_FechaSalidaBov;
        this.di_fk_IdEstado = di_fk_IdEstado;
        this.dv_CodBarra = dv_CodBarra;
        this.di_fk_IdOper = di_fk_IdOper;
        this.db_checkOk = db_checkOk;
        this.dv_Glosa = dv_Glosa;
        this.db_ApoderadoBanco = db_ApoderadoBanco;
        this.ddt_FechaSolicitud = ddt_FechaSolicitud;

    }

    public int getDi_IdDcto() {
        return di_IdDcto;
    }

    public void setDi_IdDcto(int di_IdDcto) {
        this.di_IdDcto = di_IdDcto;
    }

    public int getDi_fk_IdUbi() {
        return di_fk_IdUbi;
    }

    public void setDi_fk_IdUbi(int di_fk_IdUbi) {
        this.di_fk_IdUbi = di_fk_IdUbi;
    }

    public int getDi_fk_TipoDcto() {
        return di_fk_TipoDcto;
    }

    public void setDi_fk_TipoDcto(int di_fk_TipoDcto) {
        this.di_fk_TipoDcto = di_fk_TipoDcto;
    }

    public String getDdt_FechaSalidaBov() {
        return ddt_FechaSalidaBov;
    }

    public void setDdt_FechaSalidaBov(String ddt_FechaSalidaBov) {
        this.ddt_FechaSalidaBov = ddt_FechaSalidaBov;
    }

    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }

    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }

    public String getDv_CodBarra() {
        return dv_CodBarra;
    }

    public void setDv_CodBarra(String dv_CodBarra) {
        this.dv_CodBarra = dv_CodBarra;
    }

    public int getDi_fk_IdOper() {
        return di_fk_IdOper;
    }

    public void setDi_fk_IdOper(int di_fk_IdOper) {
        this.di_fk_IdOper = di_fk_IdOper;
    }

    public int getDb_checkOk() {
        return di_fk_IdOper;
    }

    public boolean isDb_checkOk() {
        return db_checkOk;
    }

    public void setDb_checkOk(boolean db_checkOk) {
        this.db_checkOk = db_checkOk;
    }

    public String getDv_Glosa() {
        return dv_Glosa;
    }

    public void setDv_Glosa(String dv_Glosa) {
        this.dv_Glosa = dv_Glosa;
    }

    public boolean getDb_ApoderadoBanco() {
        return db_ApoderadoBanco;
    }

    public boolean isDb_ApoderadoBanco() {
        return db_ApoderadoBanco;
    }

    public void setDb_ApoderadoBanco(boolean db_ApoderadoBanco) {
        this.db_ApoderadoBanco = db_ApoderadoBanco;
    }

    public String getTipoDocumentoGlosa() {
        return _tipoDocumentoGlosa;
    }

    public void setTipoDocumentoGlosa(String _tipoDocumentoGlosa) {
        this._tipoDocumentoGlosa = _tipoDocumentoGlosa;
    }

    public String getEstadoGlosa() {
        return _estadoGlosa;
    }

    public void setEstadoGlosa(String _estadoGlosa) {
        this._estadoGlosa = _estadoGlosa;
    }

    public String getCodigoOperacion() {
        return CodigoOperacion;
    }

    public void setCodigoOperacion(String CodigoOperacion) {
        this.CodigoOperacion = CodigoOperacion;
    }

    public String getDdt_FechaSolicitud() {
        return ddt_FechaSolicitud;
    }

    public void setDdt_FechaSolicitud(String ddt_FechaSolicitud) {
        this.ddt_FechaSolicitud = ddt_FechaSolicitud;
    }

}
