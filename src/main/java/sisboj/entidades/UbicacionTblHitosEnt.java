/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisboj.entidades;

/**
 *
 * @author EXVGUBA
 */
public class UbicacionTblHitosEnt {

    public UbicacionTblHitosEnt(String ubicacion) {
        this.ubicacion = ubicacion;
    }  
    
     public UbicacionTblHitosEnt() {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
    
    private String ubicacion;  
   
    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    public boolean equals(Object o){    
        return (o instanceof UbicacionTblHitosEnt)&&((UbicacionTblHitosEnt)o).toString().equals(toString());
    }
    
    @Override
    public String toString() {
        return "UbicacionTblHitosEnt{" + "ubicacion=" + ubicacion + '}';
    }

     
}
