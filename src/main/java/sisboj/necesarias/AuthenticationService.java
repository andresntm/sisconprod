/* 
	Description:
		ZK Essentials
	History:
		Created by dennis

Copyright (C) 2012 Potix Corporation. All Rights Reserved.
*/
package sisboj.necesarias ;

import java.util.List;



public interface AuthenticationService {

	/**login with account and password**/
	public boolean login(String account, String password);
	
	/**logout current user**/
	public void logout();
	 public  String UserAcees(String nm, String pd);
	/**get current user credential**/
	public UserCredential getUserCredential();
	
}
