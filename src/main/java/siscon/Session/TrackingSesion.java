/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.Session;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;

/**
 *
 * @author excosoc
 */
public final class TrackingSesion {

    /*
     *
     *  1	SISCONEDIT	editar Informacion SisCon;
     *  2	SISCONINIT	Inicio De Session-login;
     *  3	SISCONEND	Fin De Session-login;
     *  4	SISBOJINIT	Inicio De Session-login SisBoj;
     *  5	SISBOJEND	Fin De Session-login SisBoj;
     *  6	SISCONINSERT	Inserta registro en la base de datos;
     *  7	SISCONUPDATE	Actualiza registro en la base de datos;
     *  8	SISCONDELETE	Elimina registro en la base de datos;
     *  9	SISCONSELECT	selecciona registros de la base de datos;
     *
     */
    /**
     * @param intIdSessionSiscon the intIdSessionSiscon to set
     */
    public void setIntIdSessionSiscon(int intIdSessionSiscon) {
        this.intIdSessionSiscon = intIdSessionSiscon;
    }

    /**
     * @param userPermisos the userPermisos to set
     */
    public void setUserPermisos(UsuarioPermiso userPermisos) {
        this.userPermisos = userPermisos;
    }

    HttpSession hSes;
    String strHttpSession;
    private int intIdSessionSiscon;
    private HttpSessionJDBC httpSesionImp;
    MvcConfig mConfig;
    Session sess;
    private UsuarioPermiso userPermisos;
    String strUsuario;
    String strNombreUser;

    public TrackingSesion() {
        try {
            mConfig = new MvcConfig();
            sess = Sessions.getCurrent();
            hSes = (HttpSession) sess.getNativeSession();

            userPermisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
            httpSesionImp = new HttpSessionJDBC(mConfig.getDataSource());
            strHttpSession = hSes.getId();
            intIdSessionSiscon = httpSesionImp.GetSisConIdSession(strHttpSession);

        } catch (SQLException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

    }

    public void registraAccion(String strControl, String strAccion, int intTipAccion) {
        boolean boolRegistro = false;
        String strSmgError;

        strSmgError = "Error al registrar la acci�n realizada\n"
                + "control:" + strControl + "\n"
                + "acci�n:" + strAccion + "\n"
                + "Tipo acci�n:" + intTipAccion;
        try {

            boolRegistro = httpSesionImp.setSessionAccion(strAccion, intTipAccion, intTipAccion);

            if (boolRegistro == false) {
                Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, strSmgError);
                System.out.println(strSmgError);
            }

        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, strSmgError, ex);
            System.out.println(strSmgError);
            boolRegistro = false;
        } finally {
        }

    }

    /**
     * @param httpSesionImp the httpSesionImp to set
     */
    public void setHttpSesionImp(HttpSessionJDBC httpSesionImp) {
        try {
            if (httpSesionImp != null) {
                this.httpSesionImp = httpSesionImp;
            } else {
                mConfig = new MvcConfig();
                this.httpSesionImp = new HttpSessionJDBC(mConfig.getDataSource());
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
    }

    public void registraSesion() {

    }

    public void buscarEnTrackingSessionAccion() {

    }

    public void buscarEnTrackingSession() {

    }

}
