package siscon.services;

import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.MouseEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelList;

import sisboj.entidades.Mail;

public class DataUserController extends SelectorComposer<Component> {
        
    @Wire
    Grid BanEntr;

    ListModelList<Mail> myListModel;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        //get data from service and wrap it to list-model for the view
        List<Mail> mList = new ArrayList<Mail>();
        if (!mList.isEmpty())
			mList.clear();
        mList.add(new Mail(1, "zk Spreadsheet RC Released Check this out", "2010/10/17 20:37:12", 24));
        mList.add(new Mail(2, "[zk 5 - Help] RE: SelectedItemConverter Question 3", "2010/10/17 18:31:12", 24));
        mList.add(new Mail(3, "[zk 5 - Help] RE: SelectedItemConverter Question 2", "2010/10/17 17:30:12", 24));
        mList.add(new Mail(4, "Times_Series Chart help", "2010/10/17 15:26:37", 24));
        mList.add(new Mail(5, "RE: Times_Series Chart help", "2010/10/17 14:22:37", 24));
        mList.add(new Mail(6, "RE: Times_Series Chart help(Updated)", "2010/10/17 13:26:37", 24));
        mList.add(new Mail(7, "[zk 5 - General] Grid Rendering problem", "2010/10/17 10:41:33", 24));
        mList.add(new Mail(8, "[zk 5 - Help] RE: SelectedItemConverter Question", "2010/10/17 10:14:27", 24));
        mList.add(new Mail(9, "[Personal] RE: requirement of new project", "2010/10/16 13:34:37", 24));		
        mList.add(new Mail(10, "[zk 3 - Feature] Client programming Question", "2010/10/15 04:31:12", 24));		
        mList.add(new Mail(11, "[zk 5 - Feature] Hlayout/Vlayout Usage", "2010/10/15 04:31:12", 24));
        mList.add(new Mail(12, "RE: Times_Series Chart help(Updated)", "2010/10/15 03:26:37", 24));
        mList.add(new Mail(13, "[zk 3 - Feature] JQuery support", "2010/10/14 04:31:12", 24));
        mList.add(new Mail(14, "[zk 5 - Help] RE: Times_Series Chart help", "2010/10/14 02:43:34", 24));
        mList.add(new Mail(15, "[Personal] requirement of new project", "2010/10/14 02:44:35", 24));
        mList.add(new Mail(16, "[zk 5 - Help] RE: SelectedItemConverter Question", "2010/10/13 02:14:27", 24));
        myListModel = new ListModelList<Mail>(mList);
        BanEntr.setModel(myListModel);
    }

    @Listen("onClick = #BanEntr > rows > row")
    public void click(MouseEvent event) {
        Clients.showNotification("Row selected!");
    }
}
