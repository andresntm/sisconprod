/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.TipoProvision;
import siscon.entidades.UsrProvision;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.ProvisionJDBC;
import siscon.entidades.implementaciones.ReparoJDBC;
import siscon.entidades.implementaciones.SisConWebLogJDBC;
import siscon.entidades.implementaciones.TipoProvisionJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscore.comunes.LogController;

/**
 *
 * @author exesilr
 */
public class EjeProvisionReparosGJPoputController extends SelectorComposer<Window> {

    @Wire
    Grid BanProvicion;
    final CondonacionInterfaz cond;
    ListModelList<UsrProvision> listProvision;
    List<TipoProvision> _listTipoProvision = new ArrayList<TipoProvision>();
    private List<UsrProvision> listCondonaciones;
    ListModelList<TipoProvision> _tipoDoctoList;
    TipoProvisionJDBC _tipoDoc;
    ProvisionJDBC _prov;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    @Wire
    Combobox cmb_TipProc;
    @Wire
    Textbox id_provision;

    @Wire
    Label idope;
    @Wire
    Label id_cond;
    Session sess;
    UsuarioPermiso permisos;
    String AreaTrabajo;
    String cuenta;
    String Nombre;
    @Wire
    private Window id_ProvWin;
    Window capturawin;
    NumberFormat nf;
    ReparoJDBC _reparo;
    SisConWebLogJDBC _log;
    LogController _systemLog;
    
    public EjeProvisionReparosGJPoputController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());
        _tipoDoc = new TipoProvisionJDBC(mmmm.getDataSource());
        _prov = new ProvisionJDBC(mmmm.getDataSource());
        this._reparo = new ReparoJDBC(mmmm.getDataSource());
        this._log = new SisConWebLogJDBC(mmmm.getDataSource());
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = comp;
        
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

        List list2;

        ListModelList lm2;

        // self.
        _listTipoProvision = _tipoDoc.listTipProvision();
        _tipoDoctoList = new ListModelList<TipoProvision>(_listTipoProvision);
        // _estadoDocto = _estado.listEstado("Operación C/Documento en custodia");

        //   try{
        list2 = new ArrayList();

        for (final TipoProvision tipodocto : _listTipoProvision) {
            list2.add(tipodocto.getDv_desc());
        }
        lm2 = new ListModelList(list2);

        lm2.addSelection(lm2.get(0));

        cmb_TipProc.setModel(lm2);
        Locale.setDefault(new Locale("es", "CL"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
        listCondonaciones = this._prov.listProvisionXrut(idope.getValue());
        listProvision = new ListModelList<UsrProvision>(listCondonaciones);

        BanProvicion.setModel(listProvision);
        BanProvicion.renderAll();
    }

    @Listen("onClick = #ButtonGuardarProv")
    public void GuardarProvision() {
        int id_TipoProv = 0;
        final String tipodoctodesc = cmb_TipProc.getValue();

        for (final TipoProvision tipoprov : _listTipoProvision) {
            if (tipoprov.getDv_desc().equals(tipodoctodesc)) {
                id_TipoProv = tipoprov.getDi_id();
            }
        }
         _systemLog = new LogController();
 final int id_condonacion = Integer.parseInt(id_cond.getValue());
    final String rutcliente = id_cond.getValue();
    
    
    final int tiii=id_TipoProv;
        int insert = cond.insertUsrProvision(((AreaTrabajo) permisos.getArea()).getId_colaborador(), Integer.parseInt(id_cond.getValue()), Float.parseFloat(id_provision.getValue()), tipodoctodesc, idope.getValue(), id_TipoProv);

        if (insert > 0) {
            //txt_hiddenIdExito.setValue("true");

            Messagebox.show("Se Agregó Correctamente la Provisión", "Guardar Provisión", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        
                        //id_ProvWin.detach();
                        
                        
                String _motivoReparo = "Edición de Provisión de Condonación:";
                String _glosaReparo = "Reparo Automático Por edición de Provisión,   Provisión Nueva [" + Float.parseFloat(id_provision.getValue())+"] Tipo de Provisión ["+tipodoctodesc+"]";
                String _glosacambio = "Edición de Provision, ID_TipoProvision: [ "+tiii+ "]   Tipo de Provisión[" +tipodoctodesc + "]Provisión Nueva  : ["+ Float.parseFloat(id_provision.getValue())+"]";
                // log de aplicacion siscon para automata 

                if (_reparo.guardarReparo(id_condonacion, _motivoReparo, _glosaReparo, cuenta)) {

                    _log._logEdita(id_condonacion, cuenta, rutcliente, "Ejecutivo.Reparos.MostrarSacabop.EdicionProvision", "EjeSacabopReparos.zul", "webapp\\WEB-INF\\condonaciones\\Ejecutivo\\Poput\\EjeSacabopReparos.zul", _systemLog.getClassName(), "Editar Provision de la Condonacion", _glosacambio);

                } else {
                    Messagebox.show("Reparando... con Error]");
                }
                        
                        
                        
                        
                        
                        
                        capturawin.detach();
//                        capturawin.getParent().detach();
                        //***/
                        capturawin = null;

                        String template = "Ejecutivo/Poput/ProvisionReparosPoput.zul";
                        Map<String, Object> arguments = new HashMap<String, Object>();
      arguments.put("Rut", idope.getValue());
        arguments.put("condonacion", id_cond.getValue());
                        capturawin = (Window) Executions.createComponents(template,null, arguments);

                        try {
                            capturawin.doModal();
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
       
                        
                        
                   //     Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
                    //    inc.setSrc(null);
                    //    Sessions.getCurrent().setAttribute("rutcliente", idope.getValue());
                   //     inc.setSrc("EjecutivoPyme/PmSacabop.zul");
                   //     id_ProvWin.detach();

                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {

                        //txt_hiddenIdExito.setValue("false");
                        capturawin.detach();

                    }
                }
            }
            );

        } else {
            Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        Executions.sendRedirect("/sisboj/index");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                    }
                }
            }
            );

        }
    }

    @Listen("onClick = #ButtonGuardarProv2")
    public void Cerrar() {

        //  Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
        //  inc.setSrc(null);
        //  Sessions.getCurrent().setAttribute("rutcliente", idope.getValue());
        //  inc.setSrc("EjecutivoPyme/PmSacabop.zul");
        //txt_hiddenIdExito.setValue("false");
        // id_ProvWin.detach();
        capturawin.detach();
        capturawin.detach();
        //***/
        capturawin = null;

        String template = "Ejecutivo/Poput/EjeSacabopReparos.zul";
        Map<String, Object> arguments = new HashMap<String, Object>();

        capturawin = (Window) Executions.createComponents(template, null, arguments);

        try {
            capturawin.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void UpdateGridProv() {

        listCondonaciones = this._prov.listProvisionXrut(idope.getValue());
        listProvision = new ListModelList<UsrProvision>(listCondonaciones);
        BanProvicion.setModel(listProvision);
        BanProvicion.renderAll();
    }

    public void Elimina(int id_prov) {

        //Messagebox.show("sdkjlaskjdlkasjd IdCon :["+idcondonacion+"]");
        if (this._prov.EliminaProvision(id_prov)) {
            UpdateGridProv();
            //    Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
            //   inc.setSrc(null);
            //    Sessions.getCurrent().setAttribute("rutcliente", idope.getValue());
            //    inc.setSrc("Ejecutivo/sacabop.zul");
            Messagebox.show("Se ha Eliminado la prov Nro  :[" + id_prov + "]");

        } else {

            Messagebox.show("No se ha Eliminado  la prov  :[" + id_prov + "]");
        }

    }
    
    
    
    
    
        public void Editar(int id_prov) {

        Messagebox.show("sdkjlaskjdlkasjd IdProv :["+id_prov+"]");
        /*
        if (this._prov.EliminaProvision(id_prov)) {
            UpdateGridProv();
            //    Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
            //   inc.setSrc(null);
            //    Sessions.getCurrent().setAttribute("rutcliente", idope.getValue());
            //    inc.setSrc("Ejecutivo/sacabop.zul");
            Messagebox.show("Se ha Eliminado la prov Nro  :[" + id_prov + "]");

        } else {

            Messagebox.show("No se ha Eliminado  la prov  :[" + id_prov + "]");
        }*/

    }


}
