/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.ReparoJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class ZoReparoPoputController extends SelectorComposer<Component>{
        @Wire
    Grid BanEntrAnalista;
        final CondonacionInterfaz cond;
        ListModelList<CondonacionTabla> bandeCondTerminada;
        
    private List<CondonacionTabla> listCondonaciones;    
    @Wire 
    Textbox motivoReparo;
    @Wire
    Textbox id_condonacion;
   @WireVariable
   ListModelList<BandejaAnalista> myListModel; 
     MvcConfig mmmm = new MvcConfig();
    ReparoJDBC _reparo;     

    @Wire
    Textbox usruaios_oldpass2;
    public ZoReparoPoputController() throws SQLException {
    
    this.cond = new CondonacionImpl(mmmm.getDataSource());
    this._reparo= new ReparoJDBC(mmmm.getDataSource());
    
    
    }         
            @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        
        listCondonaciones=this.cond.GetCondonacionesAprobadas();
        bandeCondTerminada=new ListModelList<CondonacionTabla>(listCondonaciones);
        
//                BanEntrAnalista.setModel(bandeCondTerminada);
  //              BanEntrAnalista.renderAll();

        
    }
    
    
    
    
    
    
    
    
    
    	@Listen("onClick=#_idReparoDoc")
	public void Reparar(){
                //id_winSacabobAnalista.detach();
                Messagebox.show("Reparando...Condonación Numero :["+id_condonacion.getValue()+"]");
                
                
           if(this._reparo.guardarReparo(Integer.parseInt( id_condonacion.getValue()), motivoReparo.getValue(), usruaios_oldpass2.getValue(),"AquiVaLaCuenta" ))
           {
               this.cond.SetCondonacionesReparadaAnalista(Integer.parseInt(id_condonacion.getValue()));
               
                     
               Messagebox.show("Reparando...Condonación Numero:["+id_condonacion.getValue()+"]");
               
           }       
           
           else{
               Messagebox.show("Reparando... con Error]");
           }
                
                
	}    
    
}
