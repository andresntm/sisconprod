/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.util.Clients;

/**
 *
 * @author excosoc
 */
public class ReproductorController extends SelectorComposer<Component> {

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
    }

    public void onClick$playBtn(Event e) throws InterruptedException {
        Clients.evalJavaScript("play()");
        
    }

    public void onClick$pauseBtn(Event e) throws InterruptedException {
        Clients.evalJavaScript("pause()");
    }

    public void onClick$stopBtn(Event e) throws InterruptedException {
        Clients.evalJavaScript("stop()");
    }

}
