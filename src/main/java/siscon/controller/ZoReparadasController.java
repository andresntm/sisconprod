/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class ZoReparadasController extends SelectorComposer<Component>{
        @Wire
    Grid BanEntrAnalista;
        final CondonacionInterfaz cond;
        ListModelList<CondonacionTabla2> bandeCondTerminada;
        
    private List<CondonacionTabla2> listCondonaciones;    
        
   @WireVariable
   ListModelList<BandejaAnalista> myListModel; 
     MvcConfig mmmm = new MvcConfig();
            
    public ZoReparadasController() throws SQLException {
    
    this.cond = new CondonacionImpl(mmmm.getDataSource());
    
    
    
    }         
            @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        
        listCondonaciones=this.cond.GetCondonacionesReparads();
        bandeCondTerminada=new ListModelList<CondonacionTabla2>(listCondonaciones);
        
                BanEntrAnalista.setModel(bandeCondTerminada);
                BanEntrAnalista.renderAll();

        
    }
}
