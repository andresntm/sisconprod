/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import config.SisConGenerales;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.DetalleCliente;
import siscon.entidades.HornorariosJudicialesCliente;
import siscon.entidades.PorcentajesXYZSimulador;
import siscon.entidades.SacaBop;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class PmModificaReglaCapitalInterezPoputController extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla> bandeCondTerminada;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox valueneg2222;
    @Wire
    Textbox valueneg2;
    String ValorOriginal;
    String ValorOriginalInterez;
    String ValorOriginalHonorario;
    @Wire
    Label idPorcentageCalculadoInterzqueda;
    @Wire
    Label idPorcentageCalculadoInterzqueda23;
    @Wire
    Textbox id_nuevoporcentaje;
    @Wire
    Textbox id_nuevoporcentajeCapital;
    @Wire
    Textbox id_nuevoAbonoCapital;
    @Wire
    Textbox id_nuevoAbonoCapital3;
    @Wire
    Textbox idInterezQueda;
    @Wire
    Textbox idInterezQueda2222;
    @Wire
    Textbox id_nuevoporcentajeHonorarios;
    @Wire
    Textbox id_autorelleno;
    @Wire
    Label idPorcentageCalculadoCap;
    @Wire
    Label valueneg3;
    @Wire
    Label idPorcentageCalculadohonor;
    @Wire
    Label idhonortot;
    @Wire
    Label idmuestra;
    @Wire
    Label idInterezQueda2;
    Window mywin;
    private double unitPrice;
    private String unitPriceS;
    @Wire
    Textbox unitPriceDoublebox;
    
    
   @Wire
   Textbox idHonorRecibe;   
        @Wire
    Textbox idHonorQueda;
        
        
            @Wire
    Textbox    idHonorarioQueda;
        
        
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
    @Wire
    Label idPorcentageCalculadoCapQueda;
    @Wire
    Label idPorcentageCalculadoCapQueda2222;
    int interesPersonalizado;
    private List<CondonacionTabla> listCondonaciones;
    ListModelList<SacaBop> sacaboj;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    
    ListModelList<HornorariosJudicialesCliente> _result;
    SisConGenerales _control;
    int RutClienteFormateado;
    public String getUnitPriceS() {
        return unitPriceS;
    }

    public void setUnitPriceS(String unitPriceS) {
        this.unitPriceS = unitPriceS;
    }

    public PmModificaReglaCapitalInterezPoputController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        mywin = comp;
        interesPersonalizado = this.cond.getSumaInterezPersonalizado(interesPersonalizado, interesPersonalizado);
        ValorOriginal = valueneg.getValue().trim().length() > 0 ? valueneg.getValue() : "";
        ValorOriginalInterez = idInterezQueda.getValue().trim().length() > 0 ? idInterezQueda.getValue() : "";
        ValorOriginalHonorario = idhonortot.getValue().trim().length() > 0 ? idhonortot.getValue() : "";
        id_nuevoporcentajeHonorarios.setValue("0");
        idPorcentageCalculadoCap.setVisible(false);
        idPorcentageCalculadoCapQueda.setVisible(false);
        idPorcentageCalculadoInterzqueda.setVisible(false);
        String Total_Interes = (String) Executions.getCurrent().getArg().get("Total_Interes");
         _control= new SisConGenerales();
        id_autorelleno.setValue("0");
         RutClienteFormateado =Integer.parseInt(Executions.getCurrent().getArg().get("rut_cliente").toString());
        
        String momento = null;

        sacaboj = (ListModelList<SacaBop>) Executions.getCurrent().getArg().get("grid");
       ListDetOperModel = (ListModelList<DetalleCliente>) Executions.getCurrent().getArg().get("ListOpers");
        // id_nuevoAbonoCapital3.setConstraint(ValorOriginal);
        //id_nuevoAbonoCapital3.
        //    DecimalFormat df2 = new DecimalFormat(" ##,#00.0 %");
        // df2.setMultiplier(1);
        //unitPriceDoublebox.setFormat(" ##,#00.0 %");
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Listen("onChanging = textbox#id_nuevoAbonoCapital")
    public void change(InputEvent event) {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (event.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }
        //String jj=valueneg.getValue();

        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();
        float valor1 = Float.parseFloat(nn2);
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float valor11 = Float.parseFloat(nn21);

        float toalMenosVdes = valor2 - valor11;
        idmuestra.setValue(Float.toString(toalMenosVdes));

//        idInterezQueda2.
        //   if(toalMenosVdes-)
        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCap.setValue(porcentageStr);
        idPorcentageCalculadoCap.setVisible(true);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

    }

    @Listen("onChanging = textbox#id_nuevoporcentaje")
    public void changeinterez(InputEvent event) {

        if (event.getValue().trim().length() == 0) {
            idPorcentageCalculadoInterzqueda.setVisible(false);

            return;
        }
        
         String valor = event.getValue();
         valor = valor.replace(",", ".");
         
         
        if(Float.parseFloat(id_autorelleno.getValue())>1) changeAutorelleno2(Integer.parseInt(valor));
        
        
        //aceptamos Float como datos de ingreso
       
        

        if (Float.parseFloat(valor) < Float.parseFloat("0") || Float.parseFloat(valor) >= Float.parseFloat("101")) {
            id_nuevoporcentaje.setValue("0");
            Messagebox.show("Rango Valido 0 a 100 %");

            return;
        }

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = valor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        //    idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        //idPorcentageCalculadoCap.setValue(porcentageStr);
        //idPorcentageCalculadoCap.setVisible(true);
        //idPorcentageCalculadoCapQueda.setVisible(true);
        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);

        float jjjjjj = valor1 - resta;

        idInterezQueda2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(valor);

        idPorcentageCalculadoInterzqueda23.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoInterzqueda23.setVisible(true);

    }

    @Listen("onChanging = textbox#id_nuevoporcentajeCapital")
    public void changePorCapital(InputEvent event) {

        if (event.getValue().trim().length() == 0) {

            return;
        }

        String valor = event.getValue();
        valor = valor.replace(",", ".");

        if (Float.parseFloat(valor) <= Float.parseFloat("0") || Float.parseFloat(valor) >= Float.parseFloat("101")) {
            id_nuevoporcentaje.setValue("0");
            Messagebox.show("Rango Valido 0 a 100 %");

            return;
        }
        String jj = ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = valor;

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {

            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
        }

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

        float jjjjjj = valor1 - resta;

        valueneg2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(valor);

        idPorcentageCalculadoCapQueda2222.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoCapQueda2222.setVisible(true);

    }

    @Listen("onChanging = textbox#id_nuevoAbonoCapital3")
    public void changeCapitalDecimal(InputEvent event) {
        if (event.getValue().trim().length() == 0) {
            // idPorcentageCalculadoInterzqueda.setVisible(false);

            return;
        }

        //  event.
        Textbox kk = (Textbox) event.getTarget();
        //Textbox row = (Textbox) event.getTarget();

        //aceptamos Float como datos de ingreso
        if (Float.parseFloat(event.getValue()) <= Float.parseFloat("0") || Float.parseFloat(event.getValue()) >= Float.parseFloat("101")) {
            //   id_nuevoAbonoCapital3.setValue("0");
            //  kk.setValue("0");
            kk.setValue("0");
            id_nuevoAbonoCapital3.setValue("0");
            // id_nuevoAbonoCapital3.setVisible(false);
            //  id_nuevoAbonoCapital3.setVisible(true);
            Messagebox.show("Rango Valido 0 a 100 % value Entero:[" + event.getValue() + "] decimal : " + event.getValue() + "");
            //id_nuevoAbonoCapital3.detach();
            // clearbox.getChildren().clear();
            kk.setValue("0");
            id_nuevoAbonoCapital3.setValue("0");

            return;
        }

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(event.getValue());
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        //    idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        //idPorcentageCalculadoCap.setValue(porcentageStr);
        //idPorcentageCalculadoCap.setVisible(true);
        //idPorcentageCalculadoCapQueda.setVisible(true);
        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);

    }

    @Listen("onChange = #unitPriceDoublebox")
    public void changeUnitPrice() {

        String valueInput = unitPriceDoublebox.getValue();

        valueInput = valueInput.replace(",", ".");
        double unitPrice2 = new Double(valueInput);
        DecimalFormat df2 = null;
        //= Double.parseDouble((String) );

        // float unitprice=unitPriceDoublebox.getValue();
        if (Float.parseFloat(valueInput) <= Float.parseFloat("0") || Float.parseFloat(valueInput) >= Float.parseFloat("100.1")) {
            Messagebox.show("Rango Valido 0 a 100 % value Entero:[" + valueInput + "]");
            unitPriceDoublebox.setValue(("0"));

            return;
        }

        double d = unitPrice2;
        if (Float.parseFloat(valueInput) <= Float.parseFloat("10") || Float.parseFloat(valueInput) >= Float.parseFloat("100.1")) {
            df2 = new DecimalFormat(" #,#0.0 %");
        } else {
            df2 = new DecimalFormat(" ##,#00.0 %");

        }

        df2.setMultiplier(1);
        //System.out.println(df2.format(d));
        //        this.setUnitPrice(Double.parseDouble(df2.format(d)));
        this.setUnitPriceS(df2.format(d));
        Messagebox.show("Valor del Precio: [" + this.getUnitPriceS() + "]");
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        //  NumberFormat formatter = new DecimalFormat(unitPriceDoublebox.getFormat());

        showNotify("AAAA:[" + df2.format(d) + "]Changed to: " + df2.format(d), unitPriceDoublebox);
    }

    private void showNotify(String msg, Component ref) {
        Clients.showNotification(msg, "info", ref, "end_center", 2000);
    }

    @Listen("onChanging = textbox#id_autorelleno")
    public void changeAutorelleno(InputEvent event) {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (event.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }
        //String jj=valueneg.getValue();

        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();

        float valor1 = Float.parseFloat(nn2);

        String interes = this.ValorOriginalInterez;
        String interes2 = interes.replace("$", "");
        String interes3 = interes2.replace(".", "");
        //String interes4 = event.getValue();

        float _interes = Float.parseFloat(interes3);

        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        
        
        
        
                //id_nuevoporcentajeHonorarios
        
                // Condonacion Honorarios % 
        String Honor = id_nuevoporcentajeHonorarios.getValue();
        String honor2 = Honor.replace("$", "");
        String honor3 = honor2.replace(".", "");
        //String interes4 = event.getValue();

   
        // Condonacion Capital %
                String Cap = id_nuevoporcentajeCapital.getValue();
        String Cap2 = Cap.replace("$", "");
        String Cap3 = Cap2.replace(".", "");
        
        
   
         float _PorcentajeCap = Float.parseFloat(Cap3);
         float _PorcentajeHonor = Float.parseFloat(honor3);
        
        
        float suma_honorariosRecibe=0;
        float suma_honorariosCondona=0;
        _result= _control.calculaHonorariosJudiciales(ListDetOperModel,(float)_PorcentajeCap/(float)100,(float)_PorcentajeHonor/(float)100, this.RutClienteFormateado);
        
        for (HornorariosJudicialesCliente _res:_result){
        
        suma_honorariosRecibe=suma_honorariosRecibe + _res.getHonorario_recibe();
        suma_honorariosCondona=suma_honorariosCondona+_res.getHonorario_condona();
        }
        
        
        
        
        
        
        
        
        
        
        
        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        List<PorcentajesXYZSimulador> _list = new ArrayList<PorcentajesXYZSimulador>();

        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float VDES = Float.parseFloat(nn21);

        float MontoTraeCliente = Float.parseFloat(valuellll);
        float menorMenor = MontoTraeCliente;
        int X = 0, Y = 0, Z = 0;

        // Messagebox.show("CA[" + valor1 + "IN[" + _interes + "]HO[" + 1000000 + "]");
        for (int z = 0; z <= 100; z++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int x = 0; x <= 100; x++) {
                for (int y = 0; y <= 100; y++) {

                    float ecuacion;
                    float porcentaje_x = ((float) x / (float) 100);
                    float porcentaje_y = ((float) y / (float) 100);
                    float porcentaje_z = ((float) z / (float) 100);

                    //(float) ((float) pppp3 / (float) 100);
                    ecuacion = porcentaje_x * (valor1) + porcentaje_y * _interes + porcentaje_z * suma_honorariosRecibe + VDES;
                    float diferencia = ecuacion - MontoTraeCliente;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor) {

                        menorMenor = direfenciaabsoluta;
                        X = x;
                        Y = y;
                        Z = z;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        PorcentajesXYZSimulador _puntoExito = new PorcentajesXYZSimulador(0, 0, 0);

                        _puntoExito.setX(x);
                        _puntoExito.setY(y);
                        _puntoExito.setY(z);
                        _list.add(_puntoExito);
                        menorMenor = MontoTraeCliente;
                    }

                }

            }

        }

        String Imprime = "";
        if (_list.size() > 0) {

            for (PorcentajesXYZSimulador puntos : _list) {
                Imprime = Imprime + "[x(" + puntos.getX() + "),y(" + puntos.getY() + "),z(" + puntos.getZ() + ")]";
            }

            //Messagebox.show("La ecuacion tiene los siguientes CEROS::[" + Imprime);
        }

        float valor11 = Float.parseFloat(nn21);

        float MontoTraeCliente2 = (float) menorMenor;
        float menorMenor2 = MontoTraeCliente2;
        int XX = 0, YY = 0, ZZ = 0;

        for (int zz = 0; zz <= 100; zz++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int xx = 0; xx <= 100; xx++) {
                for (int yy = 0; yy <= 100; yy++) {

                    float ecuacion;
                    float porcentaje_xx = ((float) xx / (float) 100);
                    float porcentaje_yy = ((float) yy / (float) 100);
                    float porcentaje_zz = ((float) zz / (float) 100);

                    //(float) ((float) pppp3 / (float) 100);
                    ecuacion = porcentaje_xx * (valor1) + porcentaje_yy * _interes + porcentaje_zz * 1000000;
                    float diferencia = ecuacion - MontoTraeCliente2;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor2) {

                        menorMenor2 = direfenciaabsoluta;
                        XX = xx;
                        YY = yy;
                        ZZ = zz;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        menorMenor2 = MontoTraeCliente2;
                    }

                }

            }

        }

        float ooo = ((float) X / (float) 100) * (valor1) + ((float) Y / (float) 100) * _interes + ((float) Z / (float) 100) * 1000000;
        String montoEcuacion = nf.format(ooo).replaceFirst("Ch", "");
        //  Messagebox.show("menorMenor=[" + menorMenor + "] variables X[" + X + "] Y[" + Y + "] Z[" + Z + "]apriximado : " + montoEcuacion + "");   
        //  Messagebox.show("menorMenor=[" + menorMenor2 + "] variables X[" + XX + "] Y[" + YY + "] Z[" + ZZ + "]"); 
        //  id_nuevoporcentaje.setValue(Integer.toString(100-Y));
        //Events.sendEvent(new Event("onChanging", ((Textbox) ((Window) mywin ).getFellow("id_nuevoporcentajeCapital")).setValue(Integer.toString(100-X)));
        // id_nuevoporcentajeCapital.setValue(Integer.toString(100-X));
        modificaceldacapital(Integer.toString(100 - X));
        modificaceldainteres(Integer.toString(100 - Y));
        modificaceldaHonorarios(100 - X, Integer.toString(100 - Z));

        float toalMenosVdes = valor2 - valor11;
        
        
        
        
        
                float HonorCondonadoRecibe =(float) Z/ (float)100 * suma_honorariosRecibe;
        
        float HonorCondonadoCondona =(float) (100-Z) / (float)100 * suma_honorariosRecibe;
        
        
        String montoHonorRecibePeso = nf.format(HonorCondonadoRecibe ).replaceFirst("Ch", "");
         String montoHonorCondonaPeso = nf.format(HonorCondonadoCondona).replaceFirst("Ch", "");
        
        
//        idHonorQueda.setValue(montoHonorRecibePeso);
        idHonorarioQueda.setValue(montoHonorCondonaPeso);
        idHonorRecibe.setValue(montoHonorRecibePeso);
        
        
        
        
        // idmuestra.setValue(Float.toString(toalMenosVdes));

//        idInterezQueda2.
        //   if(toalMenosVdes-)
        // idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        // idPorcentageCalculadoCap.setValue(porcentageStr);
        //  idPorcentageCalculadoCap.setVisible(true);
        //   idPorcentageCalculadoCapQueda.setVisible(true);
        //  valueneg.setValue(peso_format);
    }

    public void modificaceldacapital(String nuevovalor) {

        String jj = ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        int PorcentageQuedaNum = 100 - Integer.parseInt(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
//        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {
//
//            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
//        }
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setVisible(false);
        valueneg.setValue(peso_format);
        valueneg.setVisible(true);
        id_nuevoporcentajeCapital.setValue(nuevovalor);
        float jjjjjj = valor1 - resta;

        valueneg2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(nuevovalor);

        idPorcentageCalculadoCapQueda2222.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoCapQueda2222.setVisible(true);

    }

    public void modificaceldainteres(String nuevovalor) {

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);
        id_nuevoporcentaje.setValue(nuevovalor);

        float jjjjjj = valor1 - resta;

        idInterezQueda2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(nuevovalor);

        idPorcentageCalculadoInterzqueda23.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoInterzqueda23.setVisible(true);

    }

    public void modificaceldaHonorarios(int Capital, String nuevovalor) {
        float temp = 0;

        for (SacaBop _sac : sacaboj) {

            //  temp = (float)Float.parseFloat(_sac.getHonorarioJudicial()) * (float)Capital / (float) 100 ;
            float honor = _sac.getHonorarioJuducial();

            temp = temp + (float) honor * (float) Capital / (float) 100;

            //momento=momento+",["+_sac.getHonorarioJuducial()+"]";
        }

        //Messagebox.show("LEctura de contyrolador a acontrolador ["+momento+"]");
        String jj = idhonortot.getValue();
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadohonor.setValue(PorcentageQuedaStr);
        idPorcentageCalculadohonor.setVisible(true);
        idhonortot.setValue(peso_format);
        //id_nuevoporcentaje.setValue(nuevovalor);

        id_nuevoporcentajeHonorarios.setValue(nuevovalor);

    }

    @Listen("onChanging = textbox#id_nuevoporcentajeHonorarios")
    public void changeHonorarios(InputEvent event) {

        if (event.getValue().trim().length() == 0) {
            idPorcentageCalculadohonor.setVisible(false);

            return;
        }
        String valor = event.getValue();
        valor = valor.replace(",", ".");
        try {

            //aceptamos Float como datos de ingreso
            if (Float.parseFloat(valor) <= Float.parseFloat("0") || Float.parseFloat(valor) >= Float.parseFloat("101")) {
                id_nuevoporcentajeHonorarios.setValue("0");
                Messagebox.show("Rango Valido 0 a 100 %");

                return;
            }
        } catch (NumberFormatException ex) {

            Messagebox.show("NumberFormatException::::El valor ingresado es invalido :[" + valor + "] Menssage : [" + ex.getMessage() + "]");

        } catch (WrongValueException ex) {
            Messagebox.show("WrongValueException::::El valor ingresado es invalido :[" + valor + "] Menssage : [" + ex.getMessage() + "]");
        }
        String jj = ValorOriginalHonorario;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = valor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        idPorcentageCalculadohonor.setValue(PorcentageQuedaStr);
        idPorcentageCalculadohonor.setVisible(true);
        idhonortot.setValue(peso_format);

    }

    public void changeAutorelleno2(int Procentaje_estatico) {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
       /* if (event.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }   */
        //String jj=valueneg.getValue();
        int Interes_forzado=0;
        Interes_forzado=Procentaje_estatico;
        //ValorOriginal  capital original
        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = id_autorelleno.getValue();  //event.getValue();

        float valor1 = Float.parseFloat(nn2);

        
       
        
       // return false;
        
        
        // interes
        String interes = this.ValorOriginalInterez;
        String interes2 = interes.replace("$", "");
        String interes3 = interes2.replace(".", "");
        //String interes4 = event.getValue();

        float _interes = Float.parseFloat(interes3);

        
        //id_nuevoporcentajeHonorarios
        
                // Condonacion Honorarios % 
        String Honor = id_nuevoporcentajeHonorarios.getValue();
        String honor2 = Honor.replace("$", "");
        String honor3 = honor2.replace(".", "");
        //String interes4 = event.getValue();

   
        // Condonacion Capital %
                String Cap = id_nuevoporcentajeCapital.getValue();
        String Cap2 = Cap.replace("$", "");
        String Cap3 = Cap2.replace(".", "");
        
        
   
         float _PorcentajeCap = Float.parseFloat(Cap3);
         float _PorcentajeHonor = Float.parseFloat(honor3);
        
        
        float suma_honorariosRecibe=0;
        float suma_honorariosCondona=0;
        _result= _control.calculaHonorariosJudiciales(ListDetOperModel,(float)_PorcentajeCap/(float)100,(float)_PorcentajeHonor/(float)100, this.RutClienteFormateado);
        
        for (HornorariosJudicialesCliente _res:_result){
        
        suma_honorariosRecibe=suma_honorariosRecibe + _res.getHonorario_recibe();
        suma_honorariosCondona=suma_honorariosCondona+_res.getHonorario_condona();
        }
        
        
        
        //Honorarios Judiciales
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        List<PorcentajesXYZSimulador> _list = new ArrayList<PorcentajesXYZSimulador>();
          float ecuacionResultado=0;
        /// Inicio valor de VDES
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float VDES = Float.parseFloat(nn21);
        /// Fin valor de VDES

        float MontoTraeCliente = Float.parseFloat(valuellll);
        float menorMenor = MontoTraeCliente;
        int X = 0, Y = 0, Z = 0;
        
        Y= Interes_forzado > 0 ? Interes_forzado :0;

        // Messagebox.show("CA[" + valor1 + "IN[" + _interes + "]HO[" + 1000000 + "]");
        for (int z = 0; z <= 100; z++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int x = 0; x <= 100; x++) {
                for (int y = 0; y <= 100; y++) {

                    float ecuacion;
                    float porcentaje_x = ((float) x / (float) 100);
                    float porcentaje_y =   Interes_forzado > 0 ? ((float)(100-Interes_forzado)/(float)100):((float) y / (float) 100);
                    float porcentaje_z = ((float) z / (float) 100);

                    //(float) ((float) pppp3 / (float) 100);
                    ecuacion = porcentaje_x * (valor1) + porcentaje_y * _interes + porcentaje_z * suma_honorariosRecibe + VDES;
                    float diferencia = ecuacion - MontoTraeCliente;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor) {

                        menorMenor = direfenciaabsoluta;
                        X = x;
                        Y = y;
                        Z = z;
                        ecuacionResultado=ecuacion;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        PorcentajesXYZSimulador _puntoExito = new PorcentajesXYZSimulador(0, 0, 0);

                        _puntoExito.setX(x);
                        _puntoExito.setY(y);
                        _puntoExito.setY(z);
                        _list.add(_puntoExito);
                        menorMenor = MontoTraeCliente;
                    }

                }

            }

        }

        String Imprime = "";
        if (_list.size() > 0) {

            for (PorcentajesXYZSimulador puntos : _list) {
                Imprime = Imprime + "[x(" + puntos.getX() + "),y(" + puntos.getY() + "),z(" + puntos.getZ() + ")]";
            }

            Messagebox.show("La ecuacion tiene los siguientes CEROS::[" + Imprime);
        }

        float valor11 = Float.parseFloat(nn21);

    /*    float MontoTraeCliente2 = (float) menorMenor;
        float menorMenor2 = MontoTraeCliente2;
        int XX = 0, YY = 0, ZZ = 0;

        for (int zz = 0; zz <= 100; zz++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int xx = 0; xx <= 100; xx++) {
                for (int yy = 0; yy <= 100; yy++) {

                    float ecuacion;
                    float porcentaje_xx = ((float) xx / (float) 100);
                    float porcentaje_yy = ((float) yy / (float) 100);
                    float porcentaje_zz = ((float) zz / (float) 100);

                    //(float) ((float) pppp3 / (float) 100);
                    ecuacion = porcentaje_xx * (valor1) + porcentaje_yy * _interes + porcentaje_zz * 1000000;
                    float diferencia = ecuacion - MontoTraeCliente2;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor2) {

                        menorMenor2 = direfenciaabsoluta;
                        XX = xx;
                        YY = yy;
                        ZZ = zz;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        menorMenor2 = MontoTraeCliente2;
                    }

                }

            }

        }*/

        float ooo = ((float) X / (float) 100) * (valor1) + ((float) Y / (float) 100) * _interes + ((float) Z / (float) 100) * 1000000;
        String montoEcuacion = nf.format(ooo).replaceFirst("Ch", "");
        
     //   Messagebox.show("Valor Porcentaje Interez Ingresado: ["+Procentaje_estatico+"]  Monto Ingresado : ["+valuellll+"] X: ["+X+"] Y : ["+Y+"] Z : ["+Z+"]  Valor Ecuacion : ["+ecuacionResultado+"]");
        
        
        modificaceldacapital(Integer.toString(100 - X));
        modificaceldainteres(Interes_forzado > 0 ? Integer.toString(Interes_forzado) :Integer.toString(100 - Y));
        modificaceldaHonorarios(100 - X, Integer.toString(100 - Z));

        
        
        
        
        
        float toalMenosVdes = valor2 - valor11;
        
        
        float HonorCondonadoRecibe =(float) Z/ (float)100 * suma_honorariosRecibe;
        
        float HonorCondonadoCondona =(float) (100-Z) / (float)100 * suma_honorariosRecibe;
        
        
        String montoHonorRecibePeso = nf.format(HonorCondonadoRecibe ).replaceFirst("Ch", "");
         String montoHonorCondonaPeso = nf.format(HonorCondonadoCondona).replaceFirst("Ch", "");
        
        
//        idHonorQueda.setValue(montoHonorRecibePeso);
        idHonorarioQueda.setValue(montoHonorCondonaPeso);
        idHonorRecibe.setValue(montoHonorRecibePeso);
      //  suma_honorariosRecibe
        
        
        

    }

}
