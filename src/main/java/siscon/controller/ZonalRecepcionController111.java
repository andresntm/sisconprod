/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Include;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

/**
 *
 * @author exesilr
 */
public class ZonalRecepcionController111 extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla2> bandeCondTerminada;
    Window window;
    Window windows;
    MetodosGenerales MT;
    private List<CondonacionTabla2> listCondonaciones;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private MetodosGenerales metodo;
    NumberFormat nf;
    Session session;
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;
    String modulo;
    ListModelList<Reparos> ListReparosCondonacionModel;
    @Wire
    Button id_refresch;
    int idCondonacion;
    @Wire
    Window capturawin;

    public ZonalRecepcionController111() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.modulo = "AnalistRecepcion";
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        session = Sessions.getCurrent();

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

        windows = comp;
        capturawin = comp;
        Locale.setDefault(new Locale("es", "CL"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
        listCondonaciones = this.cond.GetCondonacionesPendientes2CountReparoPm();
        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

        BanEntrAnalista.setModel(bandeCondTerminada);
//        BanEntrAnalista.renderAll();

    }

    //Cosorio
    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }

    public void AprobarCondonacion(int idcondonacion) {
        boolean resultado;
        CondonacionTabla2 condApro = new CondonacionTabla2();
        ActualizaTrackinEstado(idcondonacion);

        for (CondonacionTabla2 valida : listCondonaciones) {
            if (valida.getId_condonacion() == idcondonacion) {
                condApro = valida;
            }
        }

        if (condApro.getNum_reparos() > 0) {
            resultado = this.cond.SetCambiaEstadoCondonaciones("ZonalPyme.Recepcion", "ZonalPyme.Aprobadas", permisos, idcondonacion, "Estado.Reparadas.Recepcion.ZonalPyme", "Estado.Reparadas.AprobadoZonal");
        } else {
            resultado = this.cond.SetCambiaEstadoCondonaciones("ZonalPyme.Recepcion", "ZonalPyme.Aprobadas", permisos, idcondonacion, "Estado.Recepcionado", "Estado.AprobadoZonal");
        }

        if (resultado) {

            Messagebox.show("Se ha Aprobado la Condonacion Nro IdCon :[" + idcondonacion + "]");
            Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
            inc.setSrc(null);
            inc.setSrc("Analista/AnalistaRecepcion.zul");
            this.capturawin.detach();

        } else {
            Messagebox.show("No se ha podido guardar la Informacion IdCon :[" + idcondonacion + "]");
        }

    }

    public void RepararCondonacion(int id_condonacion) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        ActualizaTrackinEstado(id_condonacion);

        arguments.put("id_condonacion", id_condonacion);
        /// Messagebox.show("Me EJEcuto######## ttttttttttttttttttttttttttttt####");
        arguments.put("rut", 15.014544);
        String template = "ZonalPyme/ZonalReparoPoput.zul";
        window = (Window) Executions.createComponents(template, this.capturawin, arguments);

        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });

        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });

        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            //strings[i]=((String)aaa[i]).split("=");
            coupleArray[i] = ss.split("=");
            // Messagebox.show("KSKSKS["+Arrays.toString(coupleArray[i])+"]");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);
//        MT.CalculaDv(rutcliente);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, "jliguen");
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);
            } else {
                listModelCondo.add(detCliHono);
            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }

            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {

                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }
            }
        }
                Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);

        //   Messagebox.show(" Operaciones Seleccionadas ##"+Operaciones+" ####");
        // arguments.put("rut", 15.014544);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        String template = "ZonalPyme/Poput/ZonalSacabopMostrar.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void SacabogCondonacion(final int id_condonacion) {

        //Messagebox.show("Prueba");
        String ruuut = "14212287-1";
        /*  String jjjjj = Executions.getCurrent().getDesktop().getPages().toString();
        String[] hhhh = jjjjj.split(" ");
        String xxx = hhhh[1].replace("_]]", "");
        String iddiv = xxx.concat("le");
        Div nnnn = null;
        nnnn.setId(iddiv.toString());*/

        // Include headerPage = new Include();
        // headerPage.setSrc("sacabop.zul?rutcliente=" + ruuut);
        //  headerPage.setParent(nnnn);
        Messagebox.show("Prueba fellow{" + windows.getParent().getParent().getFellows().toString() + "}");
        Include inc = (Include) windows.getParent().getParent().getFellow("pageref");
        inc.setSrc(null);
        inc.setSrc("ZonalPyme/ZonalSacabop.zul?rutcliente=" + ruuut);
        // headerPage.setParent(nnnn);
        // Include headerPage = new Include();
        // headerPage.setSrc("sacabop.zul?rutcliente=" + ruuut);
        //  headerPage.setParent(nnnn);

    }

    public void ReglaCondonacion() {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        // arguments.put("id_condonacion", id_condonacion);
        //  arguments.put("rut", "14212287-1");
        //  Messagebox.show("Me EJEcuto######## ttttttttttttttttttttttttttttt####");
        arguments.put("rut", 15.014544);
        String template = "ZonalPyme/ZonalRegla.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_GenrarCondonacion");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        Messagebox.show("Imprimo TokenPJ:[" + _token.Token5().toString() + "]");

        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    //@Listen("onClick = #id_MostrarGarantias")
    public void MostrarReparos(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }
        int idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rutEjecutivo", rutejecutivo);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);
        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion_ZonalPyme(idCondonacion));

        arguments.put("orderItems", ListReparosCondonacionModel);
        String template = "ZonalPyme/Poput/ReparosPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, this.capturawin, arguments);

        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                windowx.detach();
            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick=#id_refresch")
    public void actualizaGridsReparoPendientes() {
        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

    }

}
