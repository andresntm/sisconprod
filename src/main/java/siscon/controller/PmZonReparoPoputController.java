/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.ColaboradorJefe;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ColaboradorJDBC;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.ReparoJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class PmZonReparoPoputController extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla> bandeCondTerminada;

    private List<CondonacionTabla> listCondonaciones;
    @Wire
    Textbox motivoReparo;
    @Wire
    Textbox id_condonacion;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    ReparoJDBC _reparo;

    @Wire
    Textbox usruaios_oldpass2;
    Session sess;
    UsuarioPermiso permisos;
    String cuenta;
    String Nombre;
    @Wire
    Window capturawin;

    ColaboradorJDBC _colaboradorQuery;
    int id_colaboradorDestino;
    String cuentaDestino;

    public PmZonReparoPoputController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this._reparo = new ReparoJDBC(mmmm.getDataSource());

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = comp;

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        //	        ColaboradorJefe _colJefe = this._colaboradorQuery.GetIdJefe(cuenta);
        //  id_colaboradorDestino = _colJefe.getMyId_jefe();
        //  cuentaDestino = _colJefe.getUsuario().getAlias();

        listCondonaciones = this.cond.GetCondonacionesAprobadas();
        bandeCondTerminada = new ListModelList<CondonacionTabla>(listCondonaciones);

//                BanEntrAnalista.setModel(bandeCondTerminada);
        //              BanEntrAnalista.renderAll();
    }

    @Listen("onClick=#_idReparoDoc")
    public void Reparar() {
        //id_winSacabobAnalista.detach();
        boolean resultado;
        //Messagebox.show("Reparando...Condonación Numero :[" + id_condonacion.getValue() + "]");

        if (this._reparo.guardarReparo(Integer.parseInt(id_condonacion.getValue()), motivoReparo.getValue(), usruaios_oldpass2.getValue(), cuenta)) {
            this.cond.SetCondonacionesReparadaAnalista(Integer.parseInt(id_condonacion.getValue()));

            resultado = this.cond.SetCambiaEstadoCondonaciones("ZonalPyme.Reparadas.Recepcion", "EjecutivoPyme.Reparadas", permisos, Integer.parseInt(id_condonacion.getValue()), "Estado.Reparadas.Recepcion", "Estado.Reparada.EjecutivoPyme");

            Messagebox.show("Reparando...Condonación Numero: [" + id_condonacion.getValue() + "] ");

            if (capturawin.getParent().getId().equals("id_poput_reparo")) {

                Events.sendEvent(new Event("onClick", (Button) capturawin.getParent().getParent().getFellow("btn_refresh")));
                capturawin.getParent().detach();
//                Events.sendEvent(new Event("onClick", (Button) capturawin.getParent().getFellow("btn_closeButton2")));
            } else if (capturawin.getParent().getId().equals("pag_Infor")) {
                Events.sendEvent(new Event("onClick", (Button) capturawin.getParent().getFellow("btn_refresh")));
            }
            this.capturawin.detach();

        } else {
            Messagebox.show("Reparando... con Error]");
        }

    }

}
