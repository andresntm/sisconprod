/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import org.zkoss.lang.Threads;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;

import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;

import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Textbox;
import siscon.entidades.UsrMontoAtribucion;
import siscon.entidades.implementaciones.ColaboradorJDBC;
import sissmall.entidades.SmallUsuarios;
import sissmall.implementaciones.UsuariosImpl;

/**
 *
 * @author exesilr
 */
public class MantenedorAtribucionesController extends SelectorComposer<Component> {

    Session session;
    @Wire
    Grid grid;

    @Wire
    Bandbox buscarinputRutAtribucion;

    @Wire
    Button btn_condona;
    @Wire
    Button btn_sacabop;
    //MvcConfig mmmm = new MvcConfig();

    //busqueda usuarios SmallCore
    ColaboradorJDBC _colaborador;
    List<UsrMontoAtribucion> _UsrMtAtr;
    MvcConfig mmmm = new MvcConfig();
    @Wire
    Textbox smallusruaios_alias;

    @Wire
    Textbox smallusruaios_perfil;
    @Wire
    Textbox smallusruaios_tipousuario;
    @Wire
    Textbox smallusruaios_nombrecompleto;
    @Wire
    Textbox smallusruaios_rut;
    @Wire
    Textbox smallusruaios_regional;

    @Wire
    Textbox smallusruaios_cedente;
    @Wire
    Textbox smallusruaios_cargo;
    @Wire
    Textbox smallusruaios_estado;
    @Wire
    Textbox smallusruaios_sucnova;

    @Wire
    Button smallusruaios_btn_respassword;
    @Wire
    Tabbox cola_tabbox;
    @Wire
    Grid smallusruaios_grid;

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        session = Sessions.getCurrent();

    }

    public MantenedorAtribucionesController() throws SQLException {
        this._colaborador = new ColaboradorJDBC(mmmm.getDataSource());

    }

    @Listen("onClick=#btn-find-rutAtribucion; onOK=#loginWin")
    public void doLogin() throws SQLException {
        String nm = buscarinputRutAtribucion.getValue();
        if (nm.trim().length() < 0) {
            Messagebox.show("STRINGGGG[VACIO]");
            return;
        }
        if (nm.trim().length() > 0) {

            
            cola_tabbox.setVisible(true);
            String mm = nm.replace(".", "");
            String[] ParteEntera = mm.split("-");

            _UsrMtAtr = _colaborador.getMontoAtribucionCondonacionRut(Integer.parseInt(ParteEntera[0]));
            
            if (this._UsrMtAtr.size() > 0) {

               // Messagebox.show("habilito opciones]");
               
                grid.setVisible(true);
                
                
            } else {

                Messagebox.show("No se Encontraron resultados.");

            }

        } else {
            Messagebox.show("KSDJAKLDJKLSJDKL----USER[VACIO]");
        }

    }

}
