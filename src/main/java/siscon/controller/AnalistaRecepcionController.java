/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Column;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.usuario;
import siscon.entidades.TrackingRowTomadaUsr;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;
/**
 *
 * @author exesilr
 */
public class AnalistaRecepcionController extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

    @Wire
    Grid grd_GridInfor;
    final CondonacionInterfaz cond;
    final UsuariosSMInterfaz cond2;
    @Wire
    Window pag_Infor;
    ListModelList<CondonacionTabla2> bandeCondTerminada;
    MetodosGenerales MT;
    // final CondonacionInterfaz cond;
    private List<CondonacionTabla2> listCondonaciones;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    Window window;
    MvcConfig mmmm = new MvcConfig();
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private MetodosGenerales metodo;
    NumberFormat nf;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<CondonacionTabla2> lCondFilter;
    
    private List<TrackingRowTomadaUsr> listTrackRow;
    String rutaTracking="siscon.controller";
    String bandejaTracking="AnalistaRecepcion.zul";
    int timeRowDat;
     private List<usuario> strListName;
    private String strUserTake;
    int idUsuarioRow;
    int minGlobal;

//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<CondonacionTabla2> lCondFinal;
    ////////////////////////////////////////////////////////
    Session session;
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;
    int id_colaborador;
    String modulo;
    ListModelList<Reparos> ListReparosCondonacionModel;
    @Wire
    Button id_refresch;
    int idCondonacion;
    Window prueba;
int puedeEvaluar = 0;
    public AnalistaRecepcionController() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.cond2 = new UsuariosSMImpl(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.modulo = "AnalistRecepcion";
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        
        
        
         prueba = (Window) comp;
        session = Sessions.getCurrent();

       int getTime = cond2.getTimeRow();
       timeRowDat = getTime;
       System.out.println("RETORNO DE TIMEROWDAT--> "+timeRowDat);
      if(timeRowDat == 0){
           timeRowDat=5;
       }
       System.out.println("RETORNO DE TIMEROWDAT 2--> "+timeRowDat);

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        id_colaborador=permisos.getId_colaborador();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

        Locale.setDefault(new Locale("es", "CL"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

        cargaPag();
    }

    private void cargaPag() {

        try {
            sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
            sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

            eventos();

            refreschGrid();
            muestraCol();
        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones cerradas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<CondonacionTabla2> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

        grd_GridInfor.setModel(bandeCondTerminada);
//        grd_GridInfor.setModel(new GroupsModel<Object, Object, Object>);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_GridInfor.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

    }

    private List<CondonacionTabla2> filterGrid() {
        List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lCondFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lCondFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (CondonacionTabla2 cT2 : lCondFinal) {
                        String cadena = cT2.getId_condonacion() + ";"
                                + cT2.getTimestap() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getUsuariocondona() + ";"
                                + cT2.getRegla() + ";"
                                + cT2.getTipocondonacion() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getDi_num_opers() + ";"
                                + cT2.getComentario_resna() + ";"
                                + cT2.getMonto_total_capitalS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_condonadoS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_recibitS().replace(".", "").replace(",", "") + ";"
                                + cT2.getRutcCliente();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }


        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;

        return lFilterGrid;
    }

    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }

    public void AprobarCondonacion(int idcondonacion) {
        boolean resultado;

        CondonacionTabla2 condApro = new CondonacionTabla2();
        ActualizaTrackinEstado(idcondonacion);

        for (CondonacionTabla2 valida : listCondonaciones) {
            if (valida.getId_condonacion() == idcondonacion) {
                condApro = valida;
            }
        }

        if (condApro.getNum_reparos() > 0) {
            resultado = this.cond.SetCambiaEstadoCondonaciones("Analista.Recepcion", "Analista.Aprobadas", permisos, idcondonacion, "Estado.Reparadas.Recepcion.Analista", "Estado.Reparadas.AprobadoAnalista");
        } else {
            resultado = this.cond.SetCambiaEstadoCondonaciones("Analista.Recepcion", "Analista.Aprobadas", permisos, idcondonacion, "Estado.Recepcionado", "Estado.AprobadoAnalista");
        }

        if (resultado) {

            Messagebox.show("Se ha Aprobado la Condonacion Nro IdCon :[" + idcondonacion + "]");
            refreschGrid();
        } else {
            Messagebox.show("No se ha podido guardar la Informacion IdCon :[" + idcondonacion + "]");
        }

    }

    public void RepararCondonacion(int id_condonacion) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", id_condonacion);
        arguments.put("rut", 15.014544);
        String template = "Analista/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, pag_Infor, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void RepararCondonacionV2(int id_condonacion) throws ParseException {
        
        idUsuarioRow = id_condonacion;
        String ctl = compruebaTomaLinea(idUsuarioRow);
        System.out.println("*****************************************PRUEBARepararCondonacionV2 "+ctl);
        
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        if(ctl == "no"){
        
        arguments.put("id_condonacion", id_condonacion);
        arguments.put("rut", 15.014544);
        String template = "Analista/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, pag_Infor, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                /*UPDATE TRACKINFFLAG*/
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(idUsuarioRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("se updatea estado tracking a 0");
                }else{
                    System.out.println("NNNNO se updatea estado");
                }
                /**/
                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                /*UPDATE TRACKINFFLAG*/
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(idUsuarioRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("se updatea estado tracking a 0");
                }else{
                    System.out.println("NNNNO se updatea estado");
                }
                /**/
                window.detach();

            }
        });
        
        
        window.addEventListener("onClose", new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println("se cierra popup y se updatea estado tracking");
                /**/
//                listCondonaciones = cond.getUsuariosSM();
//                bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
//                grd_InformeCond.setModel(bandeCondTerminada);
                
                
                /*UPDATE TRACKINFFLAG*/
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(idUsuarioRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("se updatea estado tracking a 0");
                }else{
                    System.out.println("NNNNO se updatea estado");
                }
                /**/
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        }else if(ctl == "si"){
             //Messagebox.show("Registro Tomado por Usuario ("+strUserTake+")" );
             String message = "Registro Tomado por Usuario("+strUserTake+") ";
             String msg = "" + message + "\n " +minGlobal+" Minutos Restantes";
             Messagebox.show(msg, "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        //ENDIF

    }

    public void UpdateGridDoc() {

    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;
   
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        String rutclienteS= (coupleArray[2][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        puedeEvaluar = cond.isCliente(rutclienteS);
        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta,(puedeEvaluar == 2) ? 1 : 0);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {

                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);

        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }

        session.setAttribute("detClientCond", detClientCond);

        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        String template = "Analista/Poput/AnSacabopMostrar.zul";

        window = (Window) Executions.createComponents(template, pag_Infor, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });

        try {
            if (window.inOverlapped()) {
                window.detach();
                window.doModal();
            } else {
                window.doModal();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void MostrarCondonacionV2(Object[] aaa) throws ParseException {
        System.out.println("MostrarCondonacionV2 llega ");
        
        window = null;
     //int idUsuarioRow = this.id_colaborador;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        String rutclienteS= (coupleArray[2][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        System.out.println("********idCondonacion--> "+idCondonacion);
        
        //IF 
        idUsuarioRow = Integer.parseInt(coupleArray[0][1]);
        String ctl = compruebaTomaLinea(idUsuarioRow);
        System.out.println("*****************************************PRUEBA "+ctl);
        
        if(ctl == "no"){
        
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        puedeEvaluar = cond.isCliente(rutclienteS);
        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta,(puedeEvaluar == 2) ? 1 : 0);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {

                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);

        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }

        session.setAttribute("detClientCond", detClientCond);

        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        String template = "Analista/Poput/AnSacabopMostrar.zul";

        window = (Window) Executions.createComponents(template, pag_Infor, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });
        
        
         Button printButton = (Button) window.getFellow("btn_Rechaza");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                /*UPDATE TRACKINFFLAG*/
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(idUsuarioRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("se updatea estado tracking a 0");
                }else{
                    System.out.println("NNNNO se updatea estado");
                }
                /**/
                window.detach();

            }
        });
       // Button printButton = (Button) window.getFellow("btn_close");

        window.addEventListener("onClose", new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println("se cierra popup y se updatea estado tracking");
                /**/
//                listCondonaciones = cond.getUsuariosSM();
//                bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
//                grd_InformeCond.setModel(bandeCondTerminada);
                
                
                /*UPDATE TRACKINFFLAG*/
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(idUsuarioRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("se updatea estado tracking a 0");
                }else{
                    System.out.println("NNNNO se updatea estado");
                }
                /**/
                window.detach();

            }
        });
        

        try {
            if (window.inOverlapped()) {
                window.detach();
                window.doModal();
            } else {
                window.doModal();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        }else if(ctl == "si"){
             //Messagebox.show("Registro Tomado por Usuario ("+strUserTake+")" );
             String message = "Registro Tomado por Usuario("+strUserTake+") ";
             String msg = "" + message + "\n " +minGlobal+" Minutos Restantes";
             Messagebox.show(msg, "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        //ENDIF

    }

    public void ReglaCondonacion() {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("rut", 15.014544);
        String template = "Analista/AnRegla.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_GenrarCondonacion");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        Messagebox.show("Imprimo TokenPJ:[" + _token.Token5().toString() + "]");

        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    public void MostrarReparos(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }
        int idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rutEjecutivo", rutejecutivo);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

        arguments.put("orderItems", ListReparosCondonacionModel);
        String template = "Analista/Poput/ReparosPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template,(Window) prueba, arguments);
//final Window windowx = (Window) Executions.createComponents(template, pag_Infor, arguments);
        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {         windowx.detach();        }      });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick=#id_refresch")
    public void actualizaGridsReparoPendientes() {

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

    }

    @Listen("onClick=#btn_refresh")
    public void refreschGrid() {

        lCondFinal = new ArrayList<CondonacionTabla2>();
        lCondFilter = new ArrayList<CondonacionTabla2>();
        lCondFinal = this.cond.GetCondonacionesPendientes2CountReparo();
        lCondFilter = lCondFinal;

        cargaGrid(lCondFilter);

    }
    
//    public String compruebaTomaLinea(int usuarioTomaRow) throws ParseException{
//        boolean instFlag;
//        String retFlaf="";
//        int colab = permisos.getId_colaborador();
//        System.out.println("222222222-> "+permisos.getNombre());
//        Date dateToma = new Date();
//        java.util.Date utilStartDate = dateToma;
//        java.sql.Date sqlStartDate = new java.sql.Date(utilStartDate.getTime());
//        
//        int usuarioTRow = usuarioTomaRow;
//        //getlistTrackRow
//        System.out.println("+++usuarioTomaRow---> "+usuarioTRow);
//        listTrackRow = cond2.getTrackingRow(usuarioTRow);
//        System.out.println("Lista aqui ->");
//        System.out.println(listTrackRow);
//        
//        if(listTrackRow.isEmpty()){
//            System.out.println("ETSA VACIO!");
//            //inserta flag para este usuario y row
//            TrackingRowTomadaUsr trflg = new TrackingRowTomadaUsr();
//            trflg.setIdRow(usuarioTRow);
//            trflg.setId_colaborador(colab);
//            trflg.setRuta(rutaTracking);
//            trflg.setBandeja(bandejaTracking);
//            trflg.setFecha_evento(null);
//            trflg.setFecha_evento_final(null);
//            trflg.setEstado(1);
//            
//          instFlag = cond2.insertFlagRow(trflg,timeRowDat);
//          retFlaf = "no";
//        }else{
//            System.out.println("ESTA LLENO!");
//            //muestra alert de row tomada x estado
//            int idColab_Row=0;
//            int estadoFlag = 2;
//            Timestamp fecIni = null;
//            Timestamp fecFin = null;
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//
//            for (TrackingRowTomadaUsr xcolprov : listTrackRow) {
//               estadoFlag = xcolprov.getEstado();
//               idColab_Row = xcolprov.getId_colaborador();
//               fecIni = xcolprov.getFecha_evento();
//                System.out.println("fecIni 1 --> "+fecIni);
//               fecFin = xcolprov.getFecha_evento_final();
//                System.out.println("fecFin 1 --> "+fecFin);
//            }
//            
//            
//            
//            strListName = cond2.getNomColabById(idColab_Row);
//            for (usuario xcolprovus : strListName) {
//               System.out.println("NAMEEEE!!!! --> "+xcolprovus.getAlias());
//               strUserTake = xcolprovus.getAlias();
//            }
//            
//            if(estadoFlag == 1){
//                /*LOGICA PARA VER TIEMPO TOMADO*/
//                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                Date date1 = sdf.parse(fecIni.toString());
//                Date date2 = sdf.parse(fecFin.toString());
//                Date date3 = sdf.parse(timestamp.toString());
//                
//                
//                System.out.println("FECHA ACTUAL ----> "+date3);
//                System.out.println("FECHA INICIAL ----->> "+date1);
//                System.out.println("FECHA FINAL ----->> "+date2);
//                
//                if (date3.after(date2)) {
//                System.out.println("SE LIBERA REGISTRO SETEO ESTADO 0");
//                int estadoflag=0;
//                boolean updFlag = cond2.updateFlagRow(usuarioTRow,estadoflag,timeRowDat);
//                if(updFlag){
//                    System.out.println("liberado 0 updatea estado 1");
//                    int estadoflagUpd=1;
//                    cond2.updateFlagRow(usuarioTRow,estadoflagUpd,timeRowDat);
//                }else{
//                    System.out.println(" NNNNO liberado se updatea estado");
//                }
//                instFlag = false;
//                retFlaf = "no";
//                }else{
//                    instFlag = false;
//                    retFlaf = "si";
//                }
//                
////                DateTime date = new DateTime(new Date());
////                date.isBeforeNow();
////                or
////                date.isAfterNow();
//                /**/
//                
//            }else if(estadoFlag == 0){
//                //update a estado 1
//                int estadoflag=1;
//                boolean updFlag = cond2.updateFlagRow(usuarioTRow,estadoflag,timeRowDat);
//                if(updFlag){
//                    System.out.println("compruebaTomaLinea se updatea estado");
//                }else{
//                    System.out.println("compruebaTomaLinea NNNNO se updatea estado");
//                }
//                
//                retFlaf = "no";
//            }
//         
//        }
//        
//        
//        //System.out.println("instFlag--->>>"+instFlag);
//        //idTrack
//        //id_colab *
//        //fecha_toma *
//        //idUsuarioRow * 
//        //estado
//        
//    
//        //return "******_________________________________Entra en CompruebaTomaLinea "+colab+" ->UsuarioTOMAROW :"+usuarioTRow +" ->Fecha: "+dateToma;
//        return retFlaf;
//    }
    
    public String compruebaTomaLinea(int usuarioTomaRow) throws ParseException{
//        boolean instFlagw;
//        instFlagw = cond2.delRow();
//        
//        if(instFlagw){
//        System.out.println("alosi");
//        }else{System.out.println("alono");}
//        String retFlaf="no";
        boolean instFlag;
        int idCOL1 = cond2.getIdColabByNom(permisos.getNombre());
        
        
        
        String retFlaf="";
        int colab = idCOL1;
        System.out.println("2222222221-> "+colab);
        System.out.println("222222222-> "+permisos.getNombre());
        Date dateToma = new Date();
        java.util.Date utilStartDate = dateToma;
        java.sql.Date sqlStartDate = new java.sql.Date(utilStartDate.getTime());
        
        int usuarioTRow = usuarioTomaRow;
        //getlistTrackRow
        System.out.println("+++usuarioTomaRow---> "+usuarioTRow);
        listTrackRow = cond2.getTrackingRow(usuarioTRow);
        System.out.println("Lista aqui ->");
        System.out.println(listTrackRow);
        
        if(listTrackRow.isEmpty()){
            System.out.println("ETSA VACIO!");
            //inserta flag para este usuario y row
            TrackingRowTomadaUsr trflg = new TrackingRowTomadaUsr();
            trflg.setIdRow(usuarioTRow);
            trflg.setId_colaborador(colab);
            trflg.setRuta(rutaTracking);
            trflg.setBandeja(bandejaTracking);
            trflg.setFecha_evento(null);
            trflg.setFecha_evento_final(null);
            trflg.setEstado(1);
            
          instFlag = cond2.insertFlagRow(trflg,timeRowDat);
          retFlaf = "no";
        }else{
            System.out.println("ESTA LLENO!");
            //muestra alert de row tomada x estado
            int idColab_Row=0;
            int estadoFlag = 2;
            Timestamp fecIni = null;
            Timestamp fecFin = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            for (TrackingRowTomadaUsr xcolprov : listTrackRow) {
               estadoFlag = xcolprov.getEstado();
               idColab_Row = xcolprov.getId_colaborador();
               System.out.println("idColab_Row --> "+idColab_Row);
               fecIni = xcolprov.getFecha_evento();
                System.out.println("fecIni 1 --> "+fecIni);
               fecFin = xcolprov.getFecha_evento_final();
                System.out.println("fecFin 1 --> "+fecFin);
            }
            
            
            
            strListName = cond2.getNomColabById(idColab_Row);
            for (usuario xcolprovus : strListName) {
               System.out.println("NAMEEEE!!!! --> "+xcolprovus.getAlias());
               strUserTake = xcolprovus.getAlias();
            }
            
            if(estadoFlag == 1){
                /*LOGICA PARA VER TIEMPO TOMADO*/
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                Date date1 = sdf.parse(fecIni.toString());
                Date date2 = sdf.parse(fecFin.toString());
                Date date3 = sdf.parse(timestamp.toString());
                
                
                System.out.println("FECHA ACTUAL ----> "+date3);
                System.out.println("FECHA INICIAL ----->> "+date1);
                System.out.println("FECHA FINAL ----->> "+date2);
                
                long differenceInMillis = date2.getTime() - date3.getTime();
                long minutes = (differenceInMillis / 1000) / 60;
                minGlobal = (int) minutes;
                System.out.println("CALULO MILISEGUNDOS --> "+differenceInMillis);
                System.out.println("CALULO MINUTOS QUEDAN --> "+minutes);
                
                if (date3.after(date2)) {
                System.out.println("SE LIBERA REGISTRO SETEO ESTADO 0");
                int estadoflag=0;
                boolean updFlag = cond2.updateFlagRow(usuarioTRow,estadoflag,timeRowDat);
                if(updFlag){
                    System.out.println("liberado 0 updatea estado 1");
                    int estadoflagUpd=1;
                    cond2.updateFlagRow(usuarioTRow,estadoflagUpd,timeRowDat);
                }else{
                    System.out.println(" NNNNO liberado se updatea estado");
                }
                instFlag = false;
                retFlaf = "no";
                }else{
                    instFlag = false;
                    retFlaf = "si";
                }
                
//                DateTime date = new DateTime(new Date());
//                date.isBeforeNow();
//                or
//                date.isAfterNow();
                /**/
                
            }else if(estadoFlag == 0){
                //update a estado 1
                int estadoflag=1;
                boolean updFlag = cond2.updateFlagRow2(usuarioTRow,estadoflag,timeRowDat,colab);
                if(updFlag){
                    System.out.println("compruebaTomaLinea se updatea estado");
                }else{
                    System.out.println("compruebaTomaLinea NNNNO se updatea estado");
                }
                
                retFlaf = "no";
            }
         
        }
        
        
        //System.out.println("instFlag--->>>"+instFlag);
        //idTrack
        //id_colab *
        //fecha_toma *
        //idUsuarioRow * 
        //estado
        
    
        //return "******_________________________________Entra en CompruebaTomaLinea "+colab+" ->UsuarioTOMAROW :"+usuarioTRow +" ->Fecha: "+dateToma;
        return retFlaf;
    }

}
