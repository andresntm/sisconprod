/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.Cond_Prorrogadas;
import siscon.entidades.Tipo_Prorroga;
import siscon.entidades.Tipo_Prorroga;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
@SuppressWarnings("serial")
public class ApplyProrrogasPopUp extends SelectorComposer<Component> {

    @Wire
    private Window win_prorrogas;
    @Wire
    private Grid grid_prorroga;
    @Wire
    private Label lbl_idCond;
    @Wire
    private Textbox txt_detalle;
    @Wire
    private Button btn_prorroga;
    @Wire
    private Button btn_close;
    @Wire
    private Combobox cmb_tipoProrroga;
    ////////////////////////////////////////////////////////////////////////////
    UsuarioPermiso permisos;
    Session sess;
    final CondonacionInterfaz cond;
    MvcConfig mvcC = new MvcConfig();
    int id_condonacionProrrogada = 0;
    private EventQueue eq;//cosoriosound
    Window thisWindow;
    private List<Tipo_Prorroga> lTP;
    private Tipo_Prorroga tProrroga;
    private ListModelList<Tipo_Prorroga> lML_TP;
    private List<Comboitem> lItemFull;
    private Cond_Prorrogadas cProrroga;
    private boolean validForm;

    public ApplyProrrogasPopUp() throws SQLException {
        this.cond = new CondonacionImpl(mvcC.getDataSource());

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        thisWindow = (Window) comp;
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cargaPag();
    }

    private void cargaPag() {
        final Execution exec = Executions.getCurrent();
        cProrroga = new Cond_Prorrogadas();
        tProrroga = new Tipo_Prorroga();
        try {
            eventos();

            if (exec.getArg().get("id_condonacion") != null) {
                id_condonacionProrrogada = (Integer) exec.getArg().get("id_condonacion");
            }

            if (id_condonacionProrrogada != 0) { //percarga rechazo
                cProrroga.setFk_idCondonacion(id_condonacionProrrogada);
                cProrroga.setFk_idColabProrrogador(permisos.getId_colaborador());
            }

            cargaListTipProrrogas();

        } catch (Exception ex) {
            SisCorelog("Error al cargar popUp rechazos. Error:" + ex.getMessage());
        }
    }

    public void prorroga() {
        boolean resultado = false;
        int idcondonacion;
        idcondonacion = 0;

        cProrroga.setDetalle(txt_detalle.getText());
        idcondonacion = id_condonacionProrrogada;
        ActualizaTrackinEstado(idcondonacion);
        resultado = this.cond.SetCambiaEstadoCondonaciones("Aplicacion.Recepcion", "Ejecutivo.Prorrogadas", permisos, idcondonacion, "Estado.Aprobada", "Estado.ProrrogadaAplicador");

        if (resultado = true) {
            cProrroga.setDetalle(txt_detalle.getText());
            cProrroga.insert();
        } else {
            Messagebox.show("Error al rechazar  la Condonaci�n Nro.: [" + idcondonacion + "]");
        }

    }

    private void cargaListTipProrrogas() {
        lTP = new ArrayList<Tipo_Prorroga>();
        lTP = tProrroga.listAll();
        lML_TP = new ListModelList<Tipo_Prorroga>(lTP);
//        cmb_tipoProrroga.setInplace(true);
        cmb_tipoProrroga.setReadonly(true);
        cmb_tipoProrroga.setAutodrop(true);
        cmb_tipoProrroga.setWidth("100%");
        cmb_tipoProrroga.setClass("input-group-block");
        cmb_tipoProrroga.setPlaceholder("Seleccione un tipo de prorroga.");
        cmb_tipoProrroga.setStyle("color : black !important; font-weight : bold");

        cmb_tipoProrroga.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String prorroga;
                Tipo_Prorroga tR = (Tipo_Prorroga) data;

                prorroga = tR.getDetalle();
                item.setLabel(prorroga);
                item.setValue(data);
            }

        });

        cmb_tipoProrroga.setModel(lML_TP);

        lItemFull = cmb_tipoProrroga.getItems();

    }

    //Cosorio
    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }

    private void eventos() {
        cmb_tipoProrroga.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Combobox cB = (Combobox) event.getTarget();
//                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Tipo_Prorroga xTR = new Tipo_Prorroga();
                Comboitem lITR = new Comboitem();
                tProrroga = new Tipo_Prorroga();

                lITR = cB.getSelectedItem();
                xTR = (Tipo_Prorroga) lITR.getValue();

                tProrroga = xTR;
                cProrroga.settR(xTR);

            }
        });

        win_prorrogas.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("Prorroga", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", win_prorrogas, map));

            }

        });

        btn_close.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("Prorroga", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_close, map));
                win_prorrogas.detach();
            }
        });

        btn_prorroga.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                validaForm();

            }

        });
    }

    private void valido(boolean estado) {
        if (estado) {
            validForm = estado;
            validaForm();
        }
    }

    private void validaForm() {
        int valido = 0;//0 = false; 1 = true; 2  = valida contra variable validForm
        final Event ev;
        try {
            if (validForm) {
                valido = 1;
            } else {
                if (cmb_tipoProrroga.getSelectedItem() == null) {
                    Messagebox.show("Sr(a) usuario(a), para continuar debe seleccionar un tipo de prorroga.", "Siscon-rechazos", Messagebox.OK, Messagebox.EXCLAMATION);
                    cmb_tipoProrroga.setFocus(true);
                    valido = 0;
                } else {
                    if (txt_detalle.getText().equals("")) {
                        Messagebox.show("Sr(a) usuario(a), la condonaci�n se prorrogo y se envi� al ejecutivo correctamente para hacer gesti�n sobre el caso sin un detalle.\n�Desea ingresar un detalle?", "Siscon-rechazos", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_detalle.setFocus(true);
                                    txt_detalle.select();
                                } else {
                                    valido(true);
                                }
                            }
                        });
                        valido = 2;
                    } else {
                        valido = 1;
                    }
                }
            }

        } catch (Exception ex) {
            valido = 0;
        } finally {
            if (valido == 1) {
                final HashMap<String, Object> map = new HashMap<String, Object>();
                prorroga();
                map.put("prorrogaCond", cProrroga);
                eq = EventQueues.lookup("Prorroga", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_prorroga, map));
                win_prorrogas.detach();
            }
        }

    }

}
