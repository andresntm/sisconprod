/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.GlosaVista;
import siscon.entidades.GraficoTPcondonaDataGrid;
import siscon.entidades.JudicialCliente;
import siscon.entidades.datosGraficoInformeCon;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.GlosaVistaImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class GridInformeController extends SelectorComposer<Component> {

    @WireVariable
    ListModelList<GraficoTPcondonaDataGrid> myListModel;

    
     @WireVariable
    ListModelList<CondonacionTabla2> myListModel2;   
    
    @Wire
    Grid GridGrafico;
    @Wire
    Grid GridGrafico2;
    MvcConfig mmmm = new MvcConfig();
        @WireVariable
    ListModelList<GlosaVista> ListGlosaVistaModel;
        final CondonacionInterfaz cond;
        private static List <CondonacionTabla2> DataGridList= new ArrayList<CondonacionTabla2>(); 
    final    GlosaVistaImpl glosa;
        
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        
        DataGridList=this.cond.GetCondonacionesReparads();
        
        myListModel2 = new ListModelList<CondonacionTabla2>(DataGridList);
        myListModel = new ListModelList<GraficoTPcondonaDataGrid>(datosGraficoInformeCon.getGridDataList());
        
        GridGrafico.setModel(myListModel);
         GridGrafico2.setModel(myListModel2);
        
        
        ListGlosaVistaModel = new ListModelList<GlosaVista>(glosa.getListGlosaVista());
    }
    
    
    
    
       @Listen("onClick = #btn_verglosa")
    public void submit() {
        // TOS should be checked before accepting order
        
        
        
       // if(tosCheckbox.isChecked()) {
           // ListModelList<OrderItem>)orderItemsModel;
            // show result
            Map<String, Object> arguments = new HashMap<String, Object>();
            arguments.put("orderItems", ListGlosaVistaModel);
            arguments.put("totalPrice", 15.014544);
            String template = "detallesPoput.zul";
            Window window = (Window)Executions.createComponents(template, null, arguments);
            window.doModal();
      //  } else {
       //     Messagebox.show("Please read the terms of service and accept it before you submit the order.");
      //  }
        
        
    }
    
    
    
   public GridInformeController()throws SQLException {
    this.glosa = new GlosaVistaImpl(mmmm.getDataSource());
    this.cond = new CondonacionImpl(mmmm.getDataSource());
    
    }
    
}
