package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.jtds.jdbc.DateTime;
import org.zkoss.lang.Objects;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Cliente;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.DetalleCliente;
import siscon.entidades.GarantiasCliente;
import siscon.entidades.GlosaDET;
import siscon.entidades.GlosaENC;
import siscon.entidades.JudicialCliente;
import siscon.entidades.Regla;
import siscon.entidades.SacaBop;
import siscon.entidades.SbifCliente;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsrProvision;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.ClienteSisconJDBC;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.CondonadorJDBC;
import siscon.entidades.implementaciones.GarantiasClienteJDBC;
import siscon.entidades.implementaciones.GeneralAppJDBC;
import siscon.entidades.implementaciones.OperacionJDBC;
import siscon.entidades.implementaciones.ProvisionJDBC;
import siscon.entidades.implementaciones.ReglasImpl;
import siscon.entidades.implementaciones.SbifClienteJDBC;
import siscon.entidades.implementaciones.TrackinJDBC;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.GarantiasClienteInterfaz;
import siscon.entidades.interfaces.ReglasInterfaz;
import siscon.entidades.interfaces.SbifClienteInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class VisuSacabopController extends SelectorComposer<Window> {

    @Wire
    Label negoc;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox nro_condonacion;
    @Wire
    Label viewrutcliente;
    @Wire
    Grid Grid_Sacabop;
    @Wire
    Grid Grid_SacabopXX;

    @Wire
    Textbox id_fechahoy;
    @Wire
    Textbox id_nomcliente;

    @Wire
    Textbox id_oficinaorigen;
    @Wire
    Textbox id_TotalSumaCondonaCapital;

    @Wire
    Textbox id_TotalSumaCapital;

    @Wire
    Textbox id_TotalSumaRecibeCapital;
    @Wire
    Textbox id_saldototalmoroso;
    float id_saldototalmorosoFloat;
    @Wire
    Label idTotalRecibe;

    @Wire
    Label id_TotalTotal;
    @Wire
    Label id_TotalCondona;
    @Wire
    Label idValorUF;
    @Wire
    Label id_TotlaCapital;

    @Wire
    Label idtotalhonor;

    @Wire
    Textbox id_TotalSumaInteres;
    @Wire
    Textbox id_TotalSumaCondonaInteres;
    @Wire
    Textbox id_TotalSumaRecibeInteres;

    // identificadores de textbod judiciales en la grilla
    @Wire
    Textbox id_TotalSumaHonorJud;
    @Wire
    Textbox id_TotalSumaHonorJudCond;
    @Wire
    Textbox id_TotalSumaHonorJudRec;

    // Totoales Parciales Recibe Color GREEN
    @Wire
    Textbox id_TotoalParcialCapital;
    @Wire
    Textbox id_TotoalParcialInteres;
    @Wire
    Textbox id_TotoalParcialhonor;

    //Totoales Parciales Condona Color Red
    @Wire
    Textbox id_TotoalParcialCondonaCapital;
    @Wire
    Textbox id_TotoalParcialCondonaInteres;
    @Wire
    Textbox id_TotoalParcialCondonaHono;

    /// Totoales parciales Capital YELLOW
    @Wire
    Textbox id_TotoalParcialCapita;
    @Wire
    Textbox id_TotoalParcialCapitaInteres;
    @Wire
    Textbox id_TotoalParcialCapitaHonor;

    /// variables del llenado de info de condonacion
    @Wire
    Textbox id_PrimerMesDeCondonacion;
    @Wire
    Textbox id_MesMasAntiguoCastigo;

    @Wire
    Textbox id_AtribucionEjecutiva;
    @Wire
    Textbox id_porcentajeCondoEjecutiva;

    @Wire
    Textbox id_PuedeCondonarOnline;

    @Wire
    Textbox id_rangoFechaCondonacion;
    @Wire
    Panel panelgridddd;

    @Wire
    Label id_msgeCona;

    @Wire
    Button btn_enviaSacaBopg;
    @Wire
    Button id_modificaRegla;
    @Wire
    Button id_modificaRegla2;
    @Wire
    Button btn_GenrarCondonacion;

    @Wire
    Vlayout vlayoutmensajje;

    @Wire
    Label id_TotalVDEs;
    @Wire
    Label id_SumaProvision;
    @Wire
    Window id_windowsMessajje;
     @Wire
    Window GG;   
    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    @WireVariable
    ListModelList<SacaBop> sacaboj;
    final DetalleOperacionesClientes detoper;
    final JudicialClienteInterz JudCliente;
    final CondonacionInterfaz cond;
    final ReglasInterfaz _reglas;
    final GarantiasClienteInterfaz _garantias_cliente;
    final SbifClienteInterfaz _deuda_sbif;
    String FechaPrimerCastigo;
    int mesescastigomasantiguo;
    Condonacion CurrentCondonacion;
    List<DetalleCliente> ListDetOper = null;
    //variables sumatorias totales de cada columna por orden 1->A  etc...
    int RutClienteFormateado;
    float totalA;
    int MaximoMesCastigo;
    int MaximoMesCastigoOnPrevio;
    float totalB;
    float totalC;
    float totalD;
    float totalE;
    float totalF;
    float totalG;
    float totalH;
    float totalI;
    float sumaMoraTotal;
    final ClienteInterfazImpl clienteinfo;
    @WireVariable
    Cliente InfoCliente;
    ClienteSisconJDBC _cliente;
    GeneralAppJDBC _ggJDBC;
    TrackinJDBC _track;
    CondonadorJDBC _condonadorJDBC;
    OperacionJDBC _operJDBC;
    OperacionJDBC _operJDBCsiscon;
    Session sess;
    String AreaTrabajo;
    String cuenta;
    String Nombre;
    @Wire
    Window capturawin;
    String rutcliente;
    float ReglaInteresPorcentajeCondonacion;
    float reglaCapitalPorcentajeCndonacion;
    float ReglaHonorarioPorcentajeCondonacion;
    List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();
    List<DetalleCliente> detClientSacabop = new ArrayList<DetalleCliente>();
    UsuarioPermiso permisos;
    usuario user;
    Trackin_Estado tE;
    @Wire
    Window id_wSacabop;
    Window window;
    float pppp2;
    float SumaTotalVDEs = 0;
    float SumaTotalProvision = 0;
    float InteresBanco = 0;
    int id_valor_regla = 0;
    int id_valor_regla_capital = 0;
    int id_valor_regla_Honorario = 0;
    float UfDia;
    float ufAplica;
    int OpCont;
    float ppppppp;
    float pppp3;
    int condonacion = 0;
    NewJerseyClient _token;
    private MetodosGenerales metodo;
    @WireVariable
    ListModelList<GarantiasCliente> ListGarantiasClienteModel;
    ListModelList<SbifCliente> ListSbifClienteClienteModel;
    private EventQueue eq;//cosoriosound
    private GlosaENC glosa; //cosoriosound
    private String modulo = "EjeSacabop";//cosoriosound
    private AdjuntarENC adjunta;
    //SbifCliente
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
    private List<UsrProvision> listProvisiones;
    ProvisionJDBC _prov;
    private float sizeFiles = 0;
    private GlosaDET glosaDet = new GlosaDET();
    int isRechazo;
    private CondonacionesEspeciales cEIngresada = null; //cosorio
    private UsrProvision uProv;
    int rut_enterocliente;
    String rutcliente_ofe;
     UsuarioDAO _usu;
      usuario usu;
    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        //limpia popup al cerrar el sacabop
        capturawin = (Window) comp;
       
                    // msgcaption.setLabel("Gestionar prorroga de");
                  // Lbalemsg.setValue("FJLKFJLKFJLKJLKHFKJADHFKYADK;JHASLKHDLKSAHDLKASHDKLAHSKldhk");
               // this.dangerwin.setVisible(true);
               // msgcaption.set
                 rutcliente = Sessions.getCurrent().getAttribute("rutcliente").toString();
               rutcliente_ofe = Sessions.getCurrent().getAttribute("rutclienteint").toString();
              // int kk=45;
               String oferta=clienteinfo.getClienteOferta(Integer.parseInt(rutcliente_ofe));
             if(this.clienteinfo.GetIsClienteCampana(Integer.parseInt(rutcliente_ofe))==0){
             //  Messagebox.show("GGFOLLOW[[["+GG.getFellows());
                Caption ctionc = (Caption) GG.getFellow("cptn_tituloo");
                GG.setVisible(true);
              ////  Messagebox.show("GGFOLLOW[[["+ctionc.getLabel().toString());
                ctionc.setLabel(""+oferta+".");
                 //      ction.setLabel("Gestionar prorroga de condonaci�n N�: " + kk+ ".");
                
                }
                else {
                 this.GG.setVisible(false);
                
                }
        capturawin.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
                if (EventQueues.exists("Glosa", EventQueues.DESKTOP)) {
                    EventQueues.remove("Glosa", EventQueues.DESKTOP);
                }
                if (EventQueues.exists("CondEspecial", EventQueues.DESKTOP)) {
                    EventQueues.remove("CondEspecial", EventQueues.DESKTOP);
                }
                id_wSacabop.setAction("onClose");
            }

        });
        
        
        
        
        String AliasEjecutivo = "ejesiscon";
        
       
         usu = this._usu.buscarUsuarioCuenta(AliasEjecutivo);
         //usu.get
          int rutejecutivo = usu.getDi_rut();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta ="ejesiscon"; //permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        detClientCond = (List<DetalleCliente>) sess.getAttribute("detClientCond");
        cEIngresada = null;
        //Executions.getCurrent().getParameter("rutcliente");

        if (Sessions.getCurrent().getAttribute("MarcaCondEspecial") instanceof CondonacionesEspeciales) {//cosorio
            if (Sessions.getCurrent().getAttribute("MarcaCondEspecial") != null) {
                cEIngresada = new CondonacionesEspeciales();
                cEIngresada = (CondonacionesEspeciales) Sessions.getCurrent().getAttribute("MarcaCondEspecial");
//                Sessions.getCurrent().removeAttribute("MarcaCondEspecial");
            }
        }

        isRechazo = 2;//(Integer) Sessions.getCurrent().getAttribute("rechazo");//Executions.getCurrent().getParameter("rutcliente");

        //Condonasound code
        glosa = new GlosaENC();//cosorio incia variable limpia
        adjunta = new AdjuntarENC();
        //Herencia entre Sacabop e hijos
        {//Bloque anonimo creado por cosorio

            //Herencia ente Glosa y Sacabop
            eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    glosaDet = (GlosaDET) map.get("GlosaSS");

                    if (glosaDet != null) {
                        glosa.setCant_glosas(1);

                    } else {
                        glosa.setCant_glosas(0);
                    }
                }
            });

            //Herencia ente Adjuntar y Sacabop
            eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    List<AdjuntarDET> det = new ArrayList<AdjuntarDET>();
                    det = (List<AdjuntarDET>) map.get("ListAdjuntoSS");
                    sizeFiles = (Float) map.get("tama�o");

                    if (det != null) {
                        if (!det.isEmpty()) {
                            adjunta.setNumero_archivos(det.size());
                            adjunta.setTama�o_total(String.format("%.4f", sizeFiles) + "Mb");
                        }

                    }
                    adjunta.setaDet(det);
                }
            });

            //Herencia ente MarcaCondEspecial y Sacabop
            eq = EventQueues.lookup("CondEspecial", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();

                    if ((Object) map.get("condEspecialMarcada") instanceof CondonacionesEspeciales) {
                        if (map.get("condEspecialMarcada") != null) {
                            cEIngresada = new CondonacionesEspeciales();
                            cEIngresada = (CondonacionesEspeciales) map.get("condEspecialMarcada");

                            Sessions.getCurrent().setAttribute("MarcaCondEspecial", cEIngresada);
//                            if (cEIngresada.getId_Provision() != 0) {
//                                recargaPagina();
//                            }
                        } else {
                            cEIngresada = new CondonacionesEspeciales();
                        }
                    } else {
                        cEIngresada = new CondonacionesEspeciales();
                    }

                }
            });
        };

        String mm = rutcliente.replace(".", "");
        String[] ParteEntera = mm.split("-");

        ListDetOper = detoper.Cliente(Integer.parseInt(ParteEntera[0]),cuenta, isRechazo);

        if (ListDetOper == null) {
            Messagebox.show("Error No Existe Detalle Operaciones.");
            return;

        }

        this.listProvisiones = this._prov.listProvisionXrut(rutcliente);
        //// calculo de suma vdes
        for (DetalleCliente detCliHono : ListDetOper) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {

                SumaTotalVDEs = SumaTotalVDEs + (float) detCliHono.getSaldoinsoluto();

            }
        }
        //// calculo de suma Provision
        for (UsrProvision prov : listProvisiones) {
            SumaTotalProvision = SumaTotalProvision + (float) prov.getMonto();

        }

        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;

        if (detClientCond.size() > 0) {

            for (DetalleCliente seleccion : detClientCond) {

                detClientSacabop.add(seleccion);

            }
        }

        ListDetOper.clear();
        ListDetOper = detClientSacabop;

        RutClienteFormateado = Integer.parseInt(ParteEntera[0]);
        List<JudicialCliente> ListJudCliente = JudCliente.JudClie(Integer.parseInt(ParteEntera[0]), "jliguen");
        InfoCliente = clienteinfo.infocliente(Integer.parseInt(ParteEntera[0]));

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
        ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

        sacaboj = new ListModelList<SacaBop>();
        String oper = "NULL";
        String MeseCatigo = "NULL";
        String FechaCastigo = "14-01-2012 09:29:58";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = new Date();
        String[] parse;
        DateTime NN;
        // creamos instancia de condonacion      

        CurrentCondonacion = new Condonacion(rutcliente, cuenta, sess.getWebApp().toString());

        /// Busqueda de Fecha Castigo mas Antigua
        //////##### AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            if (ListDetOperModel.get(i) != null) {
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigoOnPrevio) {
                    this.MaximoMesCastigoOnPrevio = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigoOnPrevio);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
            }
        }

        //////##### FIN-AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        //////######   AQUI SE BUSCA Y GUARDA LA INFORMACION DEL EJECUTIVO####///////
        //  CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonador(permisos.getDi_rut(), permisos.getNombre(), this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
        CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsultingV5(usu.getDi_rut(), usu.getAlias(), this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));

        this.CurrentCondonacion.setMontoMaximoAtribucionEjecutiva(CurrentCondonacion._condonador.getAtribucionMaxima());
        this.CurrentCondonacion.setMontoAtribucionMaxima(CurrentCondonacion._condonador.getAtribucionMaxima());
        ppppppp = 0;
        pppp2 = 0;
        pppp3 = 0;
        for (final Regla _regla : this.CurrentCondonacion._condonador._reglaList) {
            if (_regla.getDesTipoValor().equals("Monto Capital")) {
                ppppppp = _regla.getPorcentajeCondonacion100();
                id_valor_regla_capital = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Interes")) {
                pppp2 = _regla.getPorcentajeCondonacion100();
                id_valor_regla = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Honorario Judicial")) {
                pppp3 = _regla.getPorcentajeCondonacion100();
                id_valor_regla_Honorario = _regla.getIdRegla();
            }

        }

        /// validamos modificacion de Regla de Condonacion por el Ejecutivo (id_condonacion,id_usuario,id_regla_valor)
        if (this.CurrentCondonacion._condonador.getPorcentajeActual() >= 0) {
            pppp2 = this.CurrentCondonacion._condonador.getF_PorcentajeActualInt();

        }
        if (this.CurrentCondonacion._condonador.getPorcentajeActualCapital() >= 0) {
            ppppppp = this.CurrentCondonacion._condonador.getF_PorcentajeActualCap();

        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActualHonorario() >= 0) {
            pppp3 = this.CurrentCondonacion._condonador.getF_PorcentajeActualHon();

        }

        reglaCapitalPorcentajeCndonacion = (float) ((float) ppppppp / (float) 100);
        ReglaInteresPorcentajeCondonacion = (float) ((float) pppp2 / (float) 100);
        ReglaHonorarioPorcentajeCondonacion = (float) ((float) pppp3 / (float) 100);
        String rangoFechaInicio = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRanfoFInicio());
        String rangoFechaFin = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRangoFFin());

        this.CurrentCondonacion.setPorcentajeCondonaCapital(reglaCapitalPorcentajeCndonacion);
        this.CurrentCondonacion.setPorcentajeCondonaHonorario(ReglaHonorarioPorcentajeCondonacion);
        this.CurrentCondonacion.setPorcentajeCondonaInteres(ReglaInteresPorcentajeCondonacion);
        this.CurrentCondonacion.setRangoFechaInicio(rangoFechaInicio);
        this.CurrentCondonacion.setRangoFehaFin(rangoFechaFin);
        this.CurrentCondonacion.setPuedeCondonarEnLinea(false);
        this.CurrentCondonacion.setNumeroDeOperaciones(ListDetOper.size());

        /// Si esta en  juicio la operacion o no le cobramos el porcentaje correspondiente
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            SacaBop temp = new SacaBop();
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setDetalleCredito(ListDetOperModel.get(i).getDetalleCredito());
                temp.setCedente(ListDetOperModel.get(i).getCedente());
                temp.setFechaCastigo(ListDetOperModel.get(i).getFechaCastigo());
                temp.setFechaFencimiento(ListDetOperModel.get(i).getFechaFencimiento());
                temp.setDiasMora(ListDetOperModel.get(i).getDiasMora());
                temp.setMarcaRenegociado(ListDetOperModel.get(i).getMarcaRenegociado());

                //  Calculo del Juicio Activo Para el Conbro de Honorarios Judiciales
                for (int j = 0; j < ListJudClienteModel.getSize(); j++) {
                    String OperacionJud = ListJudClienteModel.get(j).getOperacion() != null ? ListJudClienteModel.get(j).getOperacion() : "0";
                    String OperacionDet = ListDetOperModel.get(i).getOperacionOriginal() != null ? ListDetOperModel.get(i).getOperacionOriginal() : "0";
                    int nnnn = OperacionJud.compareTo(OperacionDet);

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);
                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneJuicio(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneJuicio(false);
                    }

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);

                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneRol(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneRol(false);
                    }

                }

                ////* Fin Juicio Activo
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                System.out.println("#------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                temp.setOperacion(oper);
                temp.setMesesCastigo(MeseCatigo);

                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigo) {
                    this.MaximoMesCastigo = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigo);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
                this.totalA = (float) this.totalA + (float) ListDetOperModel.get(i).getSaldoinsoluto();

                temp.setCapital(ListDetOperModel.get(i).getSaldoEnPesosChileno());

                // verificar si el usuario ha ingresado otro interes personalizado
                float interes = 0;
                float montoInteresAjustado = this._operJDBCsiscon.getProcentajeInteresActual(this.RutClienteFormateado, this.cuenta, oper, this.condonacion);

                if (montoInteresAjustado >= 0) {
                    interes = montoInteresAjustado;

                } else {
                    interes = (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                }

                InteresBanco = InteresBanco + (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                sumaMoraTotal = sumaMoraTotal + (float) ListDetOperModel.get(i).getMora();
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                float porcentaje_condonacion = (float) this.CurrentCondonacion.getPorcentajeCondonaCapital();
                float porcentaje_condonacion_Interes = (float) this.CurrentCondonacion.getPorcentajeCondonaInteres();
                float capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion;
                this.totalB = (float) (this.totalB + capital_condonado);
                float capitalarecibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;
                this.totalC = (float) (capitalarecibir + this.totalC);

                /// agregagos el monto a recibir a la clase condonacion
                this.CurrentCondonacion.setMontoARecibir(capitalarecibir);

                float interes_condonado = interes * porcentaje_condonacion_Interes;
                float interesarecibir = interes - interes_condonado;

                this.totalD = (float) (interes + this.totalD);
                this.totalE = (float) (interes_condonado + this.totalE);
                this.totalF = (float) (interesarecibir + this.totalF);

                // se agrega el monto a condonar a la clase sacabop
                temp.setMontoCondonar((long) capital_condonado);
                temp.setMontoCondonarPesos(nf.format(capital_condonado).replaceFirst("Ch", ""));
                temp.setCapitalARecibirPesos(nf.format(capitalarecibir).replaceFirst("Ch", ""));

                temp.setMontoARecibir((long) capitalarecibir);
                double porcentaje_honorjud = 0;
                double honorariojud = 0;
                double honorariojud2 = 0;
                double honorariojud_sobrecondonado = 0;
                double Honor2 = 0;
                double capital_a_recibir = (double) ListDetOperModel.get(i).getSaldoinsoluto() - (double) capital_condonado;

                //  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (double) 0.15;

                    double uf = Math.round((double) this.ufAplica);
                    double capital = Math.round((double) capital_a_recibir);
                    double uf_X_10 = Math.round(((double) 10 * uf));

                    if (capital <= uf_X_10) {

                        honorariojud_sobrecondonado = Math.round(capital * 0.09);
                    } else if ((uf_X_10) < capital && capital <= ((double) 50 * uf)) {

                        double uf10_X_09 = Math.round((uf_X_10 * 0.09));
                        double cap_menos_uf10 = Math.round((capital - uf_X_10));
                        double ww = Math.round(cap_menos_uf10);
                        double ww3 = Math.round(ww * (double) 0.06);

                        honorariojud_sobrecondonado = Math.round(uf10_X_09 + ww3);

                    } else if (capital > ((double) 50 * uf)) {

                        double uno = (double) (((double) 10 * uf) * ((double) 0.09));
                        double dos = (double) (40 * uf * (double) 0.06);
                        double tes_part1 = capital;
                        double tres_part2 = ((double) (50 * uf));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;

                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (double) 0.50;

                    double capitalOrig = (double) capital_a_recibir;
                    double uf_x_500 = ((double) 500 * (double) this.ufAplica);

                    if (capitalOrig <= uf_x_500) {

                        honorariojud_sobrecondonado = (double) capital_a_recibir * (double) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir
                            && capital_a_recibir <= (3000 * this.ufAplica)) {

//                        double uf = (double) this.ufAplica;
                        double capital = (double) capital_a_recibir;
//                        double unff = ((double) 500 * uf);

//                        double hh = Math.round((unff * 0.15));
//                        double amenosb = Math.round((capital - unff));
                        double hh = Math.round((uf_x_500 * 0.15));
                        double amenosb = Math.round((capital - uf_x_500));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.05);

                        honorariojud_sobrecondonado = Math.round(hh + ww3);

                    } else if ((double) capital_a_recibir > ((double) 3000 * (double) this.ufAplica)) {
                        double uno = (double) (500 * (double) this.ufAplica) * (double) 0.15;
                        double dos = (double) (2500 * (double) this.ufAplica * (double) 0.06);
                        double tes_part1 = (double) capital_a_recibir;
                        double tres_part2 = ((double) (3000 * this.ufAplica));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (double) 0.15;

                    double c = Math.round((double) this.ufAplica);
                    double a = Math.round((double) ListDetOperModel.get(i).getSaldoinsoluto());
                    double b = Math.round(((double) 10 * c));

                    if (a <= b) {

                        honorariojud = Math.round(a * 0.09);
                    } else if ((b) < a && a <= ((double) 50 * c)) {

                        double hh = Math.round((b * 0.09));
                        double amenosb = Math.round((a - b));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.06);

                        honorariojud = Math.round(hh + ww3);

                    } else if (a > ((double) 50 * c)) {
                        double uno = (double) ((double) 10 * c) * (double) 0.09;
                        double dos = (double) (40 * c * (double) 0.06);
                        double tes_part1 = a;
                        double tres_part2 = ((double) (50 * c));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (double) 0.50;

                    double capital2 = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                    double rango0_500 = ((double) 500 * (double) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud = (double) ListDetOperModel.get(i).getSaldoinsoluto() * (double) 0.15;

                    } else if ((500 * this.ufAplica) < ListDetOperModel.get(i).getSaldoinsoluto() && ListDetOperModel.get(i).getSaldoinsoluto() <= (3000 * this.ufAplica)) {

                        double uf = (double) this.ufAplica;
                        double capital = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                        double ptje2 = (double) 0.05;
                        double unff = ((double) 500 * uf);

                        double hh = Math.round((unff * 0.15));
                        double amenosb = Math.round((capital - unff));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.05);

                        honorariojud = Math.round(hh + ww3);
                    } else if ((double) ListDetOperModel.get(i).getSaldoinsoluto() > ((double) 3000 * (double) this.ufAplica)) {
                        double uno = (double) (500 * (double) this.ufAplica) * (double) 0.15;
                        double dos = (double) (2500 * (double) this.ufAplica * (double) 0.06);
                        double tes_part1 = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                        double tres_part2 = ((double) (3000 * this.ufAplica));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                }

                //  el calculo de los honorarios judiciales esta compuesto del CalculaMontoJudicial(monto a recibir)
                //PorcentajeCondonaHonorario
                double honorarioJudCondonado = (double) honorariojud * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudReibido = (double) honorariojud - (double) honorarioJudCondonado;

                double honorarioJudCondonado2 = (double) honorariojud_sobrecondonado * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudReibido2 = (double) honorariojud_sobrecondonado - (double) honorarioJudCondonado2;
                // se muestra sobre el total capital condonado
                this.totalG = (float) (honorariojud_sobrecondonado + this.totalG);
                this.totalH = (float) (honorarioJudCondonado2 + this.totalH);
                this.totalI = (float) (honorarioJudReibido2 + this.totalI);

                // Grid-Column Judicial 
                temp.setHonorarioJuducial((float) honorariojud);

                temp.setHonorarioJudicial2((double) honorariojud_sobrecondonado);

                //Montos en pesos
                String valorPesos = nf.format(honorariojud).replaceFirst("Ch", "");
                temp.setHonorarioJudicialPesos(valorPesos);
                temp.setHonorarioJudicialCondonadoPesos(nf.format(honorarioJudCondonado).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialRecibidoPesos(nf.format(honorarioJudReibido).replaceFirst("Ch", ""));

                temp.setInteresCondonadoPesos(nf.format(interes_condonado).replaceFirst("Ch", ""));
                temp.setInteresARecibirPesos(nf.format(interesarecibir).replaceFirst("Ch", ""));

                temp.setInteres(nf.format(interes).replaceFirst("Ch", ""));

                sacaboj.add(temp);
            } else {
                oper = "SOYNULL";
            }

            System.out.println("#------------@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        }

        this.CurrentCondonacion.setHonorarioJudicial2((double) this.totalG);
        Grid_SacabopXX.setModel(sacaboj);
        Grid_SacabopXX.setVisible(false);
        Grid_SacabopXX.setVisible(true);
        idValorUF.setValue(Float.toString(this.UfDia));

        System.out.println("#------------@@@@@@@@@@@@@@@@@ ListDetOperModel.getSize()[" + ListDetOperModel.getSize() + "]Grid_SacabopXX [" + Grid_SacabopXX.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());

            /// Agregagamos valores totales a la clase condonacion
            CurrentCondonacion.setCapitalClass(this.totalA + this.totalD + this.totalG);
            CurrentCondonacion.setTotalCondonado(this.totalB + this.totalE + this.totalH);

            //totales de la Grilla
            id_TotalSumaCapital.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotalSumaCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotalSumaRecibeCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));

            id_TotalSumaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotalSumaCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            id_TotalSumaRecibeInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            id_TotalSumaHonorJud.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            idtotalhonor.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // totoales de Sumatorias

            /// calculo con honorario
            // calculo sin honorario
            idTotalRecibe.setValue(nff.format(this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            //Guarda en condonacion
            this.CurrentCondonacion.setTotoalRecibe(this.totalC + this.totalF + this.totalI);

            id_TotalCondona.setValue(nff.format(this.totalB + this.totalE + this.totalH).replaceFirst("Ch", ""));
            id_TotlaCapital.setValue(CurrentCondonacion.TotoalCapital.getValorPesos());

            // totales Parciales en La Suma de Resultados Por Color GREEN
            id_TotoalParcialCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));
            id_TotoalParcialInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            // Suma de VDEs para green
            id_TotalVDEs.setValue(nff.format(SumaTotalVDEs).replaceFirst("Ch", ""));
            this.id_SumaProvision.setValue(nff.format(SumaTotalProvision).replaceFirst("Ch", ""));

            id_TotalTotal.setValue(nff.format(this.SumaTotalProvision + SumaTotalVDEs + this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color RED
            id_TotoalParcialCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotoalParcialCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color YELLOW
            id_TotoalParcialCapita.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaHonor.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // totales Parciales en La Suma de Resultados Por Color RED

            // fin totales Grilla
            id_fechahoy.setValue(dateFormat.format(date));
            nro_condonacion.setValue(Integer.toString(this.condonacion));
            valueneg.setValue(Nombre);
            this.CurrentCondonacion.setNombreEjecutiva(Nombre);
            viewrutcliente.setValue(rutcliente);
            this.CurrentCondonacion.setRutCliente(rutcliente);
            id_nomcliente.setValue(InfoCliente.getNombreCOmpleto());
            this.CurrentCondonacion.setNombreCliente(InfoCliente.getNombreCOmpleto());
            id_oficinaorigen.setValue(InfoCliente.getOficina());
            id_saldototalmoroso.setValue(InfoCliente.getSaldoTotalMoraPesos());
            id_saldototalmorosoFloat = (float) Float.parseFloat(InfoCliente.getSaldototal());
            // llenado de informacuion de valores Utilizados en condonacion       
            id_PrimerMesDeCondonacion.setValue(this.CurrentCondonacion.getFechaPrimerCatigo());
            id_MesMasAntiguoCastigo.setValue(Integer.toString(this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
            id_AtribucionEjecutiva.setValue(this.CurrentCondonacion.getMontoAtribucionMaxima().getValorPesos());
            DecimalFormat df = new DecimalFormat("####0.00");

            // id_porcentajeCondoEjecutiva.setValue("Ca:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaCapital()) + "  In:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaInteres()) + "  Ho:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaHonorario()));
            id_porcentajeCondoEjecutiva.setValue("Ca:" + String.format("%.2f", reglaCapitalPorcentajeCndonacion * 100) + "%  In:" + String.format("%.2f", ReglaInteresPorcentajeCondonacion * 100) + "%  Ho:" + String.format("%.2f", ReglaHonorarioPorcentajeCondonacion * 100) + "%");
            id_PuedeCondonarOnline.setValue(Boolean.toString(this.CurrentCondonacion.isPuedeCondonarEnLinea()));
            id_rangoFechaCondonacion.setValue("FI:" + this.CurrentCondonacion.getRangoFechaInicio() + " FF:" + this.CurrentCondonacion.getRangoFehaFin());
            float TotalCapital = (float) this.CurrentCondonacion.TotoalCapital.getValor();

            float MontoStribucionMaxima = (float) this.CurrentCondonacion.MontoAtribucionMaxima.getValor();
            if (TotalCapital < MontoStribucionMaxima) {
                btn_GenrarCondonacion.setVisible(false);
                id_modificaRegla.setVisible(true);
                id_modificaRegla2.setVisible(false);
                id_windowsMessajje.setClass("alert alert-success");
                Label temp;
                temp = (Label) id_windowsMessajje.getFellow("id_msgeCona");
                temp.setValue("Ejecutivo " + Nombre + " Cumple Los Requisitos para Condonar Online");
            } else {
                btn_enviaSacaBopg.setVisible(false);
                id_windowsMessajje.setClass("alert alert-danger");
                id_modificaRegla.setVisible(false);
                id_modificaRegla2.setVisible(true);

            }

            String jjj = Executions.getCurrent().getAttributes().toString();
            System.out.println("#------------@@@@@@@@@@@@@@@@@Tratando de accesar other panels" + jjj + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        } catch (WrongValueException e) {
            System.out.println("EXCEPTION: " + e);

        }
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        System.out.println("doBeforeCompose executed");

        return super.doBeforeCompose(page, parent, compInfo);

    }

//    private void recargaPagina() {
//        Include inc = (Include) capturawin.getParent().getFellow("pageref");
//        inc.setSrc(null);
//
//        Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
//        inc.setSrc("Ejecutivo/Poput/RetailSacabop.zul");
//    }
    public VisuSacabopController() throws SQLException {
        this.MaximoMesCastigo = 0;
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceProduccion());
        _cliente = new ClienteSisconJDBC(mmmm.getDataSource());
        _track = new TrackinJDBC(mmmm.getDataSource());
        _condonadorJDBC = new CondonadorJDBC(mmmm.getDataSource());
        _reglas = new ReglasImpl(mmmm.getDataSource());
        _garantias_cliente = new GarantiasClienteJDBC(mmmm.getDataSource());
        _deuda_sbif = new SbifClienteJDBC(mmmm.getDataSourceProduccion());
        _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
        this._operJDBCsiscon = new OperacionJDBC(mmmm.getDataSource(), "siscon");
        _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());
        this.sumaMoraTotal = 0;
        this.metodo = new MetodosGenerales();
        _prov = new ProvisionJDBC(mmmm.getDataSource());
        tE = new Trackin_Estado();
this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this.UfDia = (float) 0.0;
        OpCont = 0;
    }

    //Se utiliza cuando el ejecutivo no cumple con las atribuciones necesarias para aprobar la condonaci�n
    private void generaTrackin_Estado(String estadoOrig, String estadoDest, String moduloOrig) {
        //Cosorio
        tE = new Trackin_Estado();
        tE.setDi_fk_ColaboradorOrigen(permisos.getId_colaborador());
        tE.setDv_CunetaOrigen(permisos.getCuenta());
        tE.setDv_estadoInicio(estadoOrig);
        tE.setDv_estadoFin(estadoDest);
        tE.setDv_ModuloOrigen(moduloOrig);

        tE.insertTrackin_Estado();
    }

    //Se utiliza cuando el ejecutivo tiene atribuciones para poder aprobar directamente la condonaci�n
    private void generaTrackin_Estado(String estadoOrig, String estadoDest, String moduloOrig, String moduloDest) {
        //Cosorio
        tE = new Trackin_Estado();
        tE.setDi_fk_ColaboradorOrigen(permisos.getId_colaborador());
        tE.setDv_CunetaOrigen(permisos.getCuenta());
        tE.setDv_estadoInicio(estadoOrig);
        tE.setDv_estadoFin(estadoDest);
        tE.setDv_ModuloOrigen(moduloOrig);
        tE.setDv_ModuloDestino(moduloDest);
        tE.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        tE.setDv_CuentaDestino(permisos.getCuenta());
        tE.insertTrackin_Estado();
    }

    @Listen("onClick=#btn_GenrarCondonacion")
    public boolean SaveMetodoCondonacionEnLinea() {

        String mssje;
        int respSaveClientinfo = -1;
        final String msgKey = "actions.save.ok";
        List<GlosaDET> lGlosDet = new ArrayList<GlosaDET>();

        //Adjunta Glosa a la condonaci�n.
        lGlosDet.add(glosaDet);
        glosa.setDetalle(lGlosDet);
        this.CurrentCondonacion.setGlosa(glosa);
        /////////////////////////////////////////////
        this.CurrentCondonacion.setAdjuntar(adjunta);
        this.cond.setCurrentCondonacion(this.CurrentCondonacion);
        this.cond.setDettaleOPeraciones(ListDetOper);
        boolean detalle_condonacion = false;

        condonacion = this.cond.GuardarCondonacion(6, 1, "Condonacion En Linea", isRechazo);
        //Cosorio
        this.cond.SetCambiaEstadoCondonaciones("Ejecutivo.Nueva", "Ejecutivo.Aprobadas", permisos, condonacion, "Estado.Nuevo", "Condonacion En Linea");

        if (condonacion <= 0) {
            mssje = "Error Guardando la informaci�n del Detalle Condonaci�n.";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);

            return false;
        }

        boolean respSave = this.cond.Guardar();

        if (!respSave) {

            mssje = "Error Guardando la informaci�n del Detalle Condonaci�n.";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
            return false;

        }

        ListDetOper = detoper.Cliente(this.RutClienteFormateado, "ejesiscon");
        // generar tacking Info Morahoy Cliente
        respSaveClientinfo = this.clienteinfo.insertDetSpClienteInfoCondonado(condonacion);

        //si guardamos exitosamente al cliente entonces  Guardar Tacking Cliente y operacion
        if (respSaveClientinfo > 0) {
            this.detoper.insertDetSpOperacionesClientCondonado(condonacion, respSaveClientinfo);

        } else {
            this.detoper.insertDetSpOperacionesClientCondonado(condonacion, 0);

        }

        int idColaborador = ((AreaTrabajo) permisos.getArea()).getId_colaborador();
        this.cond.SetCondonacionIntoRegla(idColaborador, condonacion, id_valor_regla);
        this.cond.SetCondonacionIntoRegla(idColaborador, condonacion, id_valor_regla_capital);
        this.cond.SetCondonacionIntoRegla(idColaborador, condonacion, id_valor_regla_Honorario);
        //Actualiza regla

        if (respSave) {

            Messagebox.show("Sr(a) Usuario(a), se ha generado correctamente la condonaci�n.",
                    "aprobaci�n en linea", Messagebox.OK,
                    Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        Executions.sendRedirect("/siscon/index");
                    }
                }
            });

        } else {

            mssje = "ERROR Guardando Informacion  Monto Capital :[" + this.CurrentCondonacion.TotoalCapital.getValorPesos() + "]";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
            return false;
        }

        return true;
    }

    @Listen("onClick=#btn_enviaSacaBopg")
    public void btn_enviaSacaBopgeee() {
              
        
        
        validaForm(btn_GenrarCondonacion);

    }

    
    
    
    
    private void validaForm(final Button fin) {
     boolean vF = false;
    
       // boolean adjuntoExcel = false;
        fin.setDisabled(true);
        fin.setVisible(false);
      //  fin.setVisible(true);
        try {
        if (cEIngresada == null) {  
            
            adjunta = new AdjuntarENC();
            cEIngresada = new CondonacionesEspeciales();   
            int pruebaDeValor=adjunta.getTama�o_total().compareTo("0,0Mb");
        }

     int pruebaDeValor2=adjunta.getTama�o_total().compareTo("0,0Mb");
            try {
                SimpleDateFormat sdfFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date finMinuta = sdfFormat.parse("22/11/2018");
                Date hoy = new Date();

                int diasFaltantes = 0;
                long fltQuedan = (finMinuta.getTime() - hoy.getTime());

                
                // si se acabo el tiempo o no es condonacion especial  pedimos sacabop
                
                if (fltQuedan < 0) {//cosorio modificaci�n cierre minuta en sistema
                    if (cEIngresada.getId_TipoEspecial() != 0 &&  adjunta.getTama�o_total().compareTo("0,0Mb") == 0 ) {// si condoancion especial y el tama�o es ceo(no archivo)
                        Messagebox.show("Sr(a) Usuario(a), debe adjuntar el sacabop para poder continuar.", "Sacabop", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                if (event.getName().equals(Messagebox.ON_OK)) {
                                  btn_enviaSacaBopg.setDisabled(false);
                                  
                                Adjuntar();
                                fin.setVisible(false);
                               // btn_enviaSacaBopg.setDisabled(true);
                                }
                            }
                        });
                        return;
                    } else {
                        if (cEIngresada.getId_TipoEspecial() != 0 &&  adjunta.getTama�o_total().compareTo("0,0Mb")  != 0) { //si es condonacion especial y tiene archivo

                                        if (SaveMetodoCondonacionPendiente()) {
                                            fin.setDisabled(false);
                                        } else {
                                            fin.setDisabled(true);
                                        }

                                    
                                
                           
                            return;
                        } else {
                            if (adjunta.getaDet() == null) {  ///aca se mete
                                Messagebox.show("Sr(a) Usuario(a), no hay archivos adjuntos. \n�Desea adjuntar?", "Sacabop", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION, new EventListener<Event>() {
                                    public void onEvent(Event event) throws Exception {
                                        if (event.getName().equals(Messagebox.ON_YES)) {
                                            Clients.clearBusy();
     
                                            Adjuntar();
                                           
                                      //  if (SaveMetodoCondonacionPendiente()) {
                                      //          fin.setDisabled(false);
                                      //      } else {
                                    //            fin.setDisabled(true);
                                     //       }
                                            
                                  // fin.setDisabled(false);
                                           btn_enviaSacaBopg.setDisabled(false);
                                           //  fin.setDisabled(false);

                                        } else if (event.getName().equals(Messagebox.ON_NO)) {
                                            if (SaveMetodoCondonacionPendiente()) {
                                                fin.setDisabled(false);
                                            } else {
                                                fin.setDisabled(true);
                                            }

                                        }
                                    }
                                });
                                return;
                            }

                            if (_cliente.ExisteCliente(Executions.getCurrent().getParameter("rutcliente"))) {

                                Messagebox.show("Enviando Formulario de Condonaci�n a Analista Validador");

                            } else {
                         //       if (fin.getId().equals("btn_enviaSacaBopg")) {
                                    if (SaveMetodoCondonacionPendiente()) {
                                        fin.setDisabled(false);
                                    } else {
                                        fin.setDisabled(true);
                                    }
                               // }
//                                } else if (fin.getId().equals("btn_GenrarCondonacion")) {
//                                    if (SaveMetodoCondonacionEnLinea()) {
//                                        fin.setDisabled(false);
//                                    } else {
//                                        fin.setDisabled(true);
//                                    }
//                                }
                            }
                        }
                    }
                }else if (cEIngresada.getId_TipoEspecial() != 0 && adjunta.getTama�o_total().compareTo("0,0Mb") == 0 ) { // si es condonacion especial y no hay archivo = pide archivo
                    Messagebox.show("Sr(a) Usuario(a), debe adjuntar el sacabop para poder continuar.", "Sacabop", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            if (event.getName().equals(Messagebox.ON_OK)) {
                                btn_enviaSacaBopg.setDisabled(false);
                                Adjuntar();
                                btn_enviaSacaBopg.setDisabled(true);
                            }
                        }
                    });
                    return;
                } else if (cEIngresada.getId_TipoEspecial() == 0 &&  adjunta.getTama�o_total().compareTo("0,0Mb")  != 0 )   {   /// si no es especial y tiene archivo  == pasa
                    
                    if (SaveMetodoCondonacionPendiente()) {
                        fin.setDisabled(false);
                    } else {
                        fin.setDisabled(true);
                    }
                    
                }else if (cEIngresada.getId_TipoEspecial() != 0 &&  adjunta.getTama�o_total().compareTo("0,0Mb")  != 0 )   {   /// si no es especial y tiene archivo  == pasa
                    
                    if (SaveMetodoCondonacionPendiente()) {
                        fin.setDisabled(false);
                    } else {
                        fin.setDisabled(true);
                    }
                    
                } else  { // si es especial y  tiene archivo pasa
                                   Messagebox.show("Sr(a) Usuario(a), debe adjuntar el sacabop para poder continuar.", "Sacabop", Messagebox.OK, Messagebox.EXCLAMATION, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            if (event.getName().equals(Messagebox.ON_OK)) {
                                btn_enviaSacaBopg.setDisabled(false);
                                Adjuntar();
                            }
                        }
                    });
                    return;
                }
            } catch (ParseException ex) {
                Logger.getLogger(VisuSacabopController.class.getName()).log(Level.SEVERE, null, ex);
            }
            vF = true;

        } catch (Exception ex) {

            vF = false;
        } finally {
        }

    
    
    
    }
    
    
    
    
    
    public boolean SaveMetodoCondonacionPendiente() {

        String mssje;
        int respSaveClientinfo = -1;
        final String msgKey = "actions.save.ok";
        List<GlosaDET> lGlosDet = new ArrayList<GlosaDET>();

        //Adjunta Glosa a la condonaci�n.
        lGlosDet.add(glosaDet);
        glosa.setDetalle(lGlosDet);
        this.CurrentCondonacion.setGlosa(glosa);
        /////////////////////////////////////////////
        this.CurrentCondonacion.setAdjuntar(adjunta);
        this.cond.setCurrentCondonacion(this.CurrentCondonacion);
        this.cond.setDettaleOPeraciones(ListDetOper);

        boolean detalle_condonacion = false;

        condonacion = this.cond.GuardarCondonacion(9, 2, "Con Aprobaci�n Analista", isRechazo);

        this.cond.SetCambiaEstadoCondonaciones("Ejecutivo.Nueva", "Analista.Recepcion", permisos, condonacion, "Estado.Nuevo", "Estado.Recepcion.Analista");

        if (condonacion <= 0) {
            mssje = "Error Guardando la informaci�n del Detalle Condonaci�n.";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);

            return false; 
        }

        boolean respSave = this.cond.Guardar();

        if (!respSave) {

            mssje = "Error Guardando la informaci�n del Detalle Condonaci�n.";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
            return false;

        }

        ListDetOper = detoper.Cliente(this.RutClienteFormateado, "ejesiscon");
        // generar tacking Info Morahoy Cliente
        respSaveClientinfo = this.clienteinfo.insertDetSpClienteInfoCondonado(condonacion);

        // Guardar Tacking Cliente y operacion
        if (respSaveClientinfo > 0) {
            this.detoper.insertDetSpOperacionesClientCondonado(condonacion, respSaveClientinfo);

        } else {
            this.detoper.insertDetSpOperacionesClientCondonado(condonacion, 0);

        }

        int idColaborador = ((AreaTrabajo) permisos.getArea()).getId_colaborador();

        this.cond.SetCondonacionIntoReglaV2Decimal(idColaborador, condonacion, id_valor_regla);
        this.cond.SetCondonacionIntoReglaV2Decimal(idColaborador, condonacion, id_valor_regla_capital);
        this.cond.SetCondonacionIntoReglaV2Decimal(idColaborador, condonacion, id_valor_regla_Honorario);
        //Actualiza regla

        //guarda tipo condonacion especiales:
        if (cEIngresada != null) {
            if (cEIngresada.getId_Cond_Esp() == 0 && cEIngresada.getId_TipoEspecial() != 0) {
                cEIngresada.setId_Condonacion(condonacion);
                if (cEIngresada.insertCondEspecial(cEIngresada)) {

                    System.out.println("Estimado(a) colaborador, la condonaci�n N�" + condonacion + " fue marcada correctamente como especial.");
                } else {

                    System.out.println("Estimado(a) colaborador, la condonaci�n N�" + condonacion + " no fue marcada como especial.");
                    if (cEIngresada.getId_Provision() > 0) {
                        uProv = new UsrProvision();
                        uProv = uProv.getProvision_X_Id(cEIngresada.getId_Provision());
                        eliminaProv(uProv.getId());
                    }
                }
            }
        }

        if (respSave) {

            Messagebox.show("Se ha Enviado Correctamente la Condonaci�n Al Analista Validador.",
                    "Envi� Sacabog", Messagebox.OK,
                    Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        Executions.sendRedirect("/siscon/index");
                    }
                }
            }
            );

        } else {

            mssje = "ERROR Guardando Informacion  Monto Capital :[" + this.CurrentCondonacion.TotoalCapital.getValorPesos() + "]";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
            return false;
        }

        return true;
    }

    public void ModificaReglaCondonacion() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion);

        arguments.put("rut", 15.014544);
        String template = "Ejecutivo/ModificaReglaPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {
                // _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, pppp2, Integer.parseInt(nn.getValue()));

                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("Ejecutivo/Poput/RetailSacabop.zul");

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void ModificaReglaCondonacion2() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        int InteresPersonalizado = this.cond.getSumaInterezPersonalizado(((AreaTrabajo) permisos.getArea()).getId_colaborador(), this.RutClienteFormateado);
        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion * 100);
        arguments.put("procentajeCondCapital", reglaCapitalPorcentajeCndonacion * 100);
        arguments.put("procentajeCondHono", ReglaHonorarioPorcentajeCondonacion * 100);
        arguments.put("Total_Capital", nf.format(this.totalA).replaceFirst("Ch", ""));
        float dif = sumaMoraTotal - id_saldototalmorosoFloat;
        String peso_formatt = nf.format(InteresBanco).replaceFirst("Ch", "");

        arguments.put("Total_Interes", peso_formatt);
        arguments.put("Total_honorario", id_TotalSumaHonorJud.getValue());
        arguments.put("ListOpers", ListDetOperModel);
        arguments.put("rut_cliente", this.RutClienteFormateado);
        arguments.put("rut_colaborador", permisos.getDi_rut());
        String InteresPersonalizado2 = nf.format(InteresPersonalizado).replaceFirst("Ch", "");
        String vdess = nf.format(SumaTotalVDEs).replaceFirst("Ch", "");
        arguments.put("InteresPersonalizado", InteresPersonalizado2);
        arguments.put("vdes", vdess);
        //String template = "Ejecutivo/ModificaReglaCapitalInterezPoput.zul";
        arguments.put("grid", sacaboj);
        String template = "visualizacion/Poput/VisuDecimalModificaReglaCapitalInterezPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");
        final Textbox nn2 = (Textbox) window.getFellow("id_nuevoporcentajeCapital");
        final Textbox nn3 = (Textbox) window.getFellow("id_nuevoAbonoCapital");
        final Textbox nn4 = (Textbox) window.getFellow("id_nuevoporcentajeHonorarios");

        if (nn.getValue().trim().length() == 0) {
            nn.setValue("0");
        }
        if (nn2.getValue().trim().length() == 0) {
            nn2.setValue("0");
        }
        if (nn3.getValue().trim().length() == 0) {
            nn3.setValue("0");
        }

        if (nn4.getValue().trim().length() == 0) {
            nn4.setValue("0");
        }

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {

//                _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, pppp2, Integer.parseInt(nn.getValue()));
//                _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_capital, ppppppp, Integer.parseInt(nn2.getValue()));
//                _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_Honorario, pppp3, Integer.parseInt(nn4.getValue()));
//                
                // _reglas.insertEditUsrModifReglaDecimales(Integer.parseInt(nro_condonacion.getValue()), ((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, Float.toString(pppp2),Float.toString(Float.parseFloat(nn.getValue())) );
                // _reglas.insertEditUsrModifReglaDecimales(Integer.parseInt(nro_condonacion.getValue()), ((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_capital, Float.toString(ppppppp),Float.toString(Float.parseFloat( nn2.getValue())) );
                // _reglas.insertEditUsrModifReglaDecimales(Integer.parseInt(nro_condonacion.getValue()), ((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_Honorario, Float.toString(pppp3),Float.toString(Float.parseFloat( nn4.getValue())) );
                _reglas.insertUsrModifReglaDecimales(usu.getId_colaborador(), id_valor_regla, Float.toString(pppp2), Float.toString(Float.parseFloat(nn.getValue())));
                _reglas.insertUsrModifReglaDecimales(usu.getId_colaborador(), id_valor_regla_capital, Float.toString(ppppppp), Float.toString(Float.parseFloat(nn2.getValue())));
                _reglas.insertUsrModifReglaDecimales(usu.getId_colaborador(), id_valor_regla_Honorario, Float.toString(pppp3), Float.toString(Float.parseFloat(nn4.getValue())));

                _reglas.insertUsrAbonoCapital(usu.getId_colaborador(), RutClienteFormateado, Float.parseFloat(nn3.getValue().trim().length() > 0 ? nn3.getValue() : "0"), sumaMoraTotal, id_saldototalmorosoFloat);

                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("visualizacion/Poput/VisuSacabop.zul");

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateSacabopV2() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion);
        arguments.put("rut", 15.014544);
        String template = "Ejecutivo/ModificaReglaPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {

                ReglaInteresPorcentajeCondonacion = Float.parseFloat(nn.getValue()) / 100;

                window.detach();

            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
            window.setFocus(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void MostrarGarantias() {
        // TOS should be checked before accepting order

        ListGarantiasClienteModel = new ListModelList<GarantiasCliente>(_garantias_cliente.GarantiasCliente(RutClienteFormateado, cuenta));
        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListGarantiasClienteModel);
        arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/GarantiasPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    public void MostrarSbif() {
        // TOS should be checked before accepting order

        ListSbifClienteClienteModel = new ListModelList<SbifCliente>(_deuda_sbif.SbifCliente(RutClienteFormateado, cuenta));
        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListSbifClienteClienteModel);
        arguments.put("totalSumaGarantias", this._deuda_sbif.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/SbifPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                windowx.detach();

            }
        });

        try {
            windowx.doModal();
            windowx.setFocus(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void IngresaInteresPersonalizado(final String Operacion) {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("Operacion", Operacion);

        DetalleCliente temp = new DetalleCliente();

        for (final DetalleCliente _ope : ListDetOperModel) {
            if (_ope.getOperacion().equals(Operacion)) {
                temp = _ope;
            }
        }

        final float interesss = (float) temp.getMora() - (float) temp.getSaldoinsoluto();

        arguments.put("Interes", interesss);
        String template = "Ejecutivo/ModificaInteresPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {

                _reglas.insertUsrModifInteres(((AreaTrabajo) permisos.getArea()).getId_colaborador(), Operacion, interesss, Float.parseFloat(nn.getValue()), RutClienteFormateado);

                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("Ejecutivo/Poput/RetailSacabop.zul");

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Adjuntar() {                                                
        CondonacionesEspeciales cEspecialLocal = new CondonacionesEspeciales();
        final HashMap<String, Object> map = new HashMap<String, Object>();

        if (adjunta == null) {
            adjunta = new AdjuntarENC();
        }

        if (cEIngresada != null) {
            cEspecialLocal = cEIngresada;
        }

        map.put("AdjuntaEspecial", cEspecialLocal);
        map.put("ListAdjuntoSS", adjunta.getaDet());
        map.put("modulo", modulo);

        try {
            Window fileWindow = (Window) Executions.createComponents("Ejecutivo/AdjuntarPoput.zul", null, map);
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void Glosa() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("GlosaSS", glosaDet);
        map.put("modulo", modulo);

        try {
            Window glosaWin = (Window) Executions.createComponents(
                    "EjecutivoPyme/include/Glosa.zul", null, map
            );
            glosaWin.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar la glosa. \nError: " + e);
        }
    }

    public void Provision() {

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("Rut", this.rutcliente);
        arguments.put("condonacion", this.condonacion);
        String template = "Ejecutivo/ProvisionPoput.zul";
        Window provWin = (Window) Executions.createComponents(template, capturawin, arguments);

        try {
            provWin.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CondEspecial() {

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("rutCliente", this.rutcliente);
        arguments.put("condonacion", this.condonacion);

        if (cEIngresada != null) {
            arguments.put("condEspecial", this.cEIngresada);
        } else {
            arguments.put("condEspecial", null);
        }

        String template = "Ejecutivo/Poput/EjeCondEspecial.zul";
        Window condEspWin = (Window) Executions.createComponents(template, capturawin, arguments);

        try {
            condEspWin.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eliminaProv(int id_prov) {
        try {
            MvcConfig mC = new MvcConfig();
            _prov = new ProvisionJDBC(mC.getDataSource());
        } catch (SQLException ex) {
            Logger.getLogger(EjeCondEspecialController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (this._prov.EliminaProvision(id_prov)) {
            Messagebox.show("Estimado(a) colaborador(a), se ha eliminado la provisi�n N�:[" + id_prov + "] correctamente.");

        } else {
            Messagebox.show("Estimado(a) colaborador(a), no se ha podido eliminado la provisi�n N�:[" + id_prov + "]");
        }

    }
}
