/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Column;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class APPAnRecepcionPYMEController extends SelectorComposer<Component> {

    @Wire
    Grid grd_GridInfor;
    final CondonacionInterfaz cond;
    final CondonacionInterfaz cond_prod;
    ListModelList<CondonacionTabla2> bandeCondTerminada;
    Window window;
    Window windows;
    MetodosGenerales MT;
    // final CondonacionInterfaz cond;
    private List<CondonacionTabla2> listCondonaciones;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private MetodosGenerales metodo;
    NumberFormat nf;
    Session session;
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;
    String modulo;
    ListModelList<Reparos> ListReparosCondonacionModel;
    @Wire
    Button id_refresch;
    int idCondonacion;
    private EventQueue eq;//cosoriosound
    @Wire
    Window win_recepApply;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<CondonacionTabla2> lCondFilter;

//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<CondonacionTabla2> lCondFinal;
    ////////////////////////////////////////////////////////
    int puedeEvaluar = 0;

    //  EstadosJDBC _estados;
    public APPAnRecepcionPYMEController() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.cond_prod = new CondonacionImpl(mmmm.getDataSourceProduccion());
        this.MT = new MetodosGenerales();
        this.modulo = "AnalistRecepcion";
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        session = Sessions.getCurrent();

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

//        windows = null;
//        Locale.setDefault(new Locale("es", "CL"));
//        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
//        listCondonaciones = this.cond.GetCondonacionesPendientesDeAplicacion(cuenta);
//        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);
//
//        grd_GridInfor.setModel(bandeCondTerminada);
        //BanEntrAnalista.renderAll();
        cargaPag();

        //Herencia ente Adjuntar y Sacabop
        eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {
                listCondonaciones = cond.GetCondonacionesPendientesDeAplicacion(cuenta);
                bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

                grd_GridInfor.setModel(bandeCondTerminada);
                txt_filtra.setText("");
            }
        });

    }

    private void cargaPag() {

        try {
            sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
            sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

            eventos();

            lCondFinal = new ArrayList<CondonacionTabla2>();
            lCondFilter = new ArrayList<CondonacionTabla2>();
            lCondFinal = this.cond.GetCondonacionesPendientesDeAplicacion(cuenta,"PYME");
            lCondFilter = lCondFinal;

            cargaGrid(lCondFilter);
            muestraCol();
        } catch (Exception e) {
            System.out.print(e);
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones cerradas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<CondonacionTabla2> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

        grd_GridInfor.setModel(bandeCondTerminada);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_GridInfor.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

        //        dbx_desde.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dDesdeLocal = dbx_desde.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
        //        dbx_hasta.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dHastaLocal = dbx_hasta.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
    }

    public void RepararCondonacion(int id_condonacion) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", id_condonacion);
        arguments.put("rut", 15.014544);
        String template = "Analista/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<CondonacionTabla2> filterGrid() {
        List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lCondFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lCondFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (CondonacionTabla2 cT2 : lCondFinal) {
                        String cadena = cT2.getCedente() + ";"
                                + cT2.getId_condonacion() + ";"
                                + cT2.getTimestap() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getUsuariocondona() + ";"
                                + cT2.getRegla() + ";"
                                + cT2.getTipocondonacion() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getDi_num_opers() + ";"
                                + cT2.getComentario_resna() + ";"
                                + cT2.getMonto_total_capitalS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_condonadoS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_recibitS().replace(".", "").replace(",", "") + ";"
                                + cT2.getRutcCliente();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

//        if (dDesdeLocal != dDesdeLocalResp) {
//            if (dDesdeLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.after(dDesdeLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
//
//        if (dHastaLocal != dHastaLocalResp) {
//            if (dHastaLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.before(dHastaLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    public void UpdateGridDoc() {
    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;
        String ced = null;
        String template;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        String rutclienteS= (coupleArray[2][1]);
        
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
         puedeEvaluar = cond.isCliente(rutclienteS);
        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta,(puedeEvaluar == 2) ? 1 : 0);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        for (CondonacionTabla2 tC2 : listCondonaciones) {
            if (tC2.getId_condonacion() == idCondonacion) {
                ced = tC2.getCedente();
                break;
            }
        }

        if (ced.equals("PYME")) {
            template = "AnAplicacion/Poput/ZonalSacabopMostrar.zul";
        } else {
            template = "AnAplicacion/Poput/AnSacabopMostrar.zul";
        }

        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void SacabogCondonacion(final int id_condonacion) {

        String ruuut = "14212287-1";
        Messagebox.show("Prueba fellow{" + windows.getParent().getParent().getFellows().toString() + "}");
        Include inc = (Include) windows.getParent().getParent().getFellow("pageref");
        inc.setSrc(null);
        inc.setSrc("Analista/AnSacabop.zul?rutcliente=" + ruuut);

    }

    public void ReglaCondonacion() {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("rut", 15.014544);
        String template = "Analista/AnRegla.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_GenrarCondonacion");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        Messagebox.show("Imprimo TokenPJ:[" + _token.Token5().toString() + "]");

        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    public void MostrarReparos(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }
        int idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        //arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);
        // Messagebox.show("FFFFFF{"+this.AreaTrabajo+"}");
//        MT.CalculaDv(rutcliente);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        // Messagebox.show("Idcondonacion["+idCondonacion+"] rutEjecutivo ["+rutejecutivo+"]");

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

        //Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListReparosCondonacionModel);
        //  arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        //  arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Analista/Poput/ReparosPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick=#id_refresch")
    public void actualizaGridsReparoPendientes() {

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

    }

}
