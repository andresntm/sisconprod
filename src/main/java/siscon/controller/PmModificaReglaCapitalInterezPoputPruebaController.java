/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import config.SisConGenerales;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.DetalleCliente;
import siscon.entidades.HornorariosJudicialesCliente;
import siscon.entidades.PorcentajesXYZSimulador;
import siscon.entidades.SacaBop;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class PmModificaReglaCapitalInterezPoputPruebaController extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla> bandeCondTerminada;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox valueneg2222;
    @Wire
    Textbox valueneg2;
    String ValorOriginal;
    String ValorOriginalInterez;
    String ValorOriginalHonorario;
    @Wire
    Label idPorcentageCalculadoInterzqueda;
    @Wire
    Label idPorcentageCalculadoInterzqueda23;
    @Wire
    Textbox id_nuevoporcentaje;
    @Wire
    Textbox id_nuevoporcentajeCapital;
    @Wire
    Textbox id_nuevoAbonoCapital;
    @Wire
    Textbox id_nuevoAbonoCapital3;
    @Wire
    Textbox idInterezQueda;
    @Wire
    Textbox idInterezQueda2222;
    @Wire
    Textbox id_nuevoporcentajeHonorarios;
    
    
    
    
      @Wire
    Textbox  valueneg2222sdsds;
    @Wire
    Textbox id_autorelleno;
    @Wire
    Label idPorcentageCalculadoCap;
    @Wire
    Label valueneg3;
    @Wire
    Label idPorcentageCalculadohonor;
    @Wire
    Label idhonortot;
    @Wire
    Label idmuestra;
    @Wire
    Label idInterezQueda2;
    Window mywin;
    private double unitPrice;
    private String unitPriceS;
    @Wire
    Textbox unitPriceDoublebox;

    @Wire
    Textbox idHonorRecibe;
    @Wire
    Textbox idHonorQueda;

    @Wire
    Textbox idHonorarioQueda;
    
       @Wire
    Textbox valueneg2ffff;

    @Wire
    Label idPorcentageCalculadoHonorqueda;
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
    @Wire
    Label idPorcentageCalculadoCapQueda;
    @Wire
    Label idPorcentageCalculadoCapQueda2222;
    int interesPersonalizado;
    private List<CondonacionTabla> listCondonaciones;
    ListModelList<SacaBop> sacaboj;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;

    ListModelList<HornorariosJudicialesCliente> _result;
    SisConGenerales _control;
    int RutClienteFormateado;

    public String getUnitPriceS() {
        return unitPriceS;
    }

    public void setUnitPriceS(String unitPriceS) {
        this.unitPriceS = unitPriceS;
    }

    public PmModificaReglaCapitalInterezPoputPruebaController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        mywin = comp;
        interesPersonalizado = this.cond.getSumaInterezPersonalizado(interesPersonalizado, interesPersonalizado);
        ValorOriginal = valueneg.getValue().trim().length() > 0 ? valueneg.getValue() : "";
        ValorOriginalInterez = idInterezQueda.getValue().trim().length() > 0 ? idInterezQueda.getValue() : "";
        ValorOriginalHonorario = idhonortot.getValue().trim().length() > 0 ? idhonortot.getValue() : "";
        id_nuevoporcentajeHonorarios.setValue("0");
        idPorcentageCalculadoCap.setVisible(false);
        idPorcentageCalculadoCapQueda.setVisible(false);
        idPorcentageCalculadoInterzqueda.setVisible(false);
        String Total_Interes = (String) Executions.getCurrent().getArg().get("Total_Interes");
        _control = new SisConGenerales();
        id_autorelleno.setValue("0");
                idHonorarioQueda.setValue("$0");
        valueneg2222.setValue("$0");
        idInterezQueda2222.setValue("$0");
        idHonorRecibe.setValue(idhonortot.getValue());
        RutClienteFormateado = Integer.parseInt(Executions.getCurrent().getArg().get("rut_cliente").toString());

        String momento = null;

        sacaboj = (ListModelList<SacaBop>) Executions.getCurrent().getArg().get("grid");
        ListDetOperModel = (ListModelList<DetalleCliente>) Executions.getCurrent().getArg().get("ListOpers");
        // id_nuevoAbonoCapital3.setConstraint(ValorOriginal);
        //id_nuevoAbonoCapital3.
        //    DecimalFormat df2 = new DecimalFormat(" ##,#00.0 %");
        // df2.setMultiplier(1);
        //unitPriceDoublebox.setFormat(" ##,#00.0 %");
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Listen("onChanging = textbox#id_nuevoAbonoCapital")
    public void change(InputEvent event) {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (event.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }
        //String jj=valueneg.getValue();

        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();
        float valor1 = Float.parseFloat(nn2);
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor1 > 0 ? (valor2 * 100 / valor1) : 0;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float valor11 = Float.parseFloat(nn21);

        float toalMenosVdes = valor2 - valor11;
        idmuestra.setValue(Float.toString(toalMenosVdes));

//        idInterezQueda2.
        //   if(toalMenosVdes-)
        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCap.setValue(porcentageStr);
        idPorcentageCalculadoCap.setVisible(true);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

    }

    @Listen("onChanging = textbox#id_nuevoporcentaje")
    public void changeinterez(InputEvent event) {

        if (event.getValue().trim().length() == 0) {
            idPorcentageCalculadoInterzqueda.setVisible(false);

            return;
        }

        String valor = event.getValue();
        valor = valor.replace(",", ".");

        //String NuevoValor=id_autorelleno.getValue()
        float montoIngresado = this.parsePesoToFloat(id_autorelleno.getValue());

        //id_autorelleno.setValue(nf.format().replaceFirst("Ch", ""));
        //aceptamos Float como datos de ingreso
        float porcentajeInt_ingreso = Float.parseFloat(valor);

        if (porcentajeInt_ingreso <= Float.parseFloat("0") || porcentajeInt_ingreso >= Float.parseFloat("101")) {
            id_nuevoporcentaje.setValue("0");
            Messagebox.show("Rango Valido 0 a 100 %");

            return;
        } else if (porcentajeInt_ingreso == 100) {
            if (montoIngresado > 1) {
                changeAutorelleno2(montoIngresado, porcentajeInt_ingreso);
                idInterezQueda.setValue("$0");
            }
            //Messagebox.show("aca deberiamos eliminar el porcentaje de ibteres");
        } else {

            String jj = ValorOriginalInterez;
            String nn1 = jj.replace("$", "");
            String nn2 = nn1.replace(".", "");
            String valuellll = valor;

            // valor ineteres $$
            float valor1 = Float.parseFloat(nn2);
            //String iiiiii=id_nuevoAbonoCapital.getValue();
            float valor2 = Float.parseFloat(valuellll);

            float porcentageToNum = valor2 * valor1 / 100;
            float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
            String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
            // String porcentageStr = "%"+porcentageNum;
            float resta = valor1 - porcentageToNum;
            String peso_format = nf.format(resta).replaceFirst("Ch", "");
            //    idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
            //idPorcentageCalculadoCap.setValue(porcentageStr);
            //idPorcentageCalculadoCap.setVisible(true);
            //idPorcentageCalculadoCapQueda.setVisible(true);
            idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
            idPorcentageCalculadoInterzqueda.setVisible(true);
            idInterezQueda.setValue(peso_format);

            float jjjjjj = valor1 - resta;

            idInterezQueda2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

            String PorcentageQuedaStr22 = "%" + Float.parseFloat(valor);

            idPorcentageCalculadoInterzqueda23.setValue(PorcentageQuedaStr22);
            idPorcentageCalculadoInterzqueda23.setVisible(true);
             UpdateTotalRecibe();

        }

    }

    @Listen("onChanging = textbox#id_nuevoporcentajeCapital")
    public void changePorCapital(InputEvent event) {

        if (event.getValue().trim().length() == 0) {

            return;
        }

        String valor = event.getValue();
        valor = valor.replace(",", ".");

        if (Float.parseFloat(valor) <= Float.parseFloat("0") || Float.parseFloat(valor) >= Float.parseFloat("101")) {
            id_nuevoporcentaje.setValue("0");
            Messagebox.show("Rango Valido 0 a 100 %");

            return;
        }
        String jj = ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = valor;

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {

            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
        }

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

        float jjjjjj = valor1 - resta;

        valueneg2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(valor);

        idPorcentageCalculadoCapQueda2222.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoCapQueda2222.setVisible(true);

    }

    @Listen("onChanging = textbox#id_nuevoAbonoCapital3")
    public void changeCapitalDecimal(InputEvent event) {
        if (event.getValue().trim().length() == 0) {
            // idPorcentageCalculadoInterzqueda.setVisible(false);

            return;
        }

        //  event.
        Textbox kk = (Textbox) event.getTarget();
        //Textbox row = (Textbox) event.getTarget();

        //aceptamos Float como datos de ingreso
        if (Float.parseFloat(event.getValue()) <= Float.parseFloat("0") || Float.parseFloat(event.getValue()) >= Float.parseFloat("101")) {
            //   id_nuevoAbonoCapital3.setValue("0");
            //  kk.setValue("0");
            kk.setValue("0");
            id_nuevoAbonoCapital3.setValue("0");
            // id_nuevoAbonoCapital3.setVisible(false);
            //  id_nuevoAbonoCapital3.setVisible(true);
            Messagebox.show("Rango Valido 0 a 100 % value Entero:[" + event.getValue() + "] decimal : " + event.getValue() + "");
            //id_nuevoAbonoCapital3.detach();
            // clearbox.getChildren().clear();
            kk.setValue("0");
            id_nuevoAbonoCapital3.setValue("0");

            return;
        }

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(event.getValue());
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        //    idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        //idPorcentageCalculadoCap.setValue(porcentageStr);
        //idPorcentageCalculadoCap.setVisible(true);
        //idPorcentageCalculadoCapQueda.setVisible(true);
        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);

    }

    @Listen("onChange = #unitPriceDoublebox")
    public void changeUnitPrice() {

        String valueInput = unitPriceDoublebox.getValue();

        valueInput = valueInput.replace(",", ".");
        double unitPrice2 = new Double(valueInput);
        DecimalFormat df2 = null;
        //= Double.parseDouble((String) );

        // float unitprice=unitPriceDoublebox.getValue();
        if (Float.parseFloat(valueInput) <= Float.parseFloat("0") || Float.parseFloat(valueInput) >= Float.parseFloat("100.1")) {
            Messagebox.show("Rango Valido 0 a 100 % value Entero:[" + valueInput + "]");
            unitPriceDoublebox.setValue(("0"));

            return;
        }

        double d = unitPrice2;
        if (Float.parseFloat(valueInput) <= Float.parseFloat("10") || Float.parseFloat(valueInput) >= Float.parseFloat("100.1")) {
            df2 = new DecimalFormat(" #,#0.0 %");
        } else {
            df2 = new DecimalFormat(" ##,#00.0 %");

        }

        df2.setMultiplier(1);
        //System.out.println(df2.format(d));
        //        this.setUnitPrice(Double.parseDouble(df2.format(d)));
        this.setUnitPriceS(df2.format(d));
        Messagebox.show("Valor del Precio: [" + this.getUnitPriceS() + "]");
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        //  NumberFormat formatter = new DecimalFormat(unitPriceDoublebox.getFormat());

        showNotify("AAAA:[" + df2.format(d) + "]Changed to: " + df2.format(d), unitPriceDoublebox);
    }

    private void showNotify(String msg, Component ref) {
        Clients.showNotification(msg, "info", ref, "end_center", 2000);
    }

    public void modificaceldacapital(String nuevovalor) {

        String jj = ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
//        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {
//
//            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
//        }
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setVisible(false);
        valueneg.setValue(peso_format);
        valueneg.setVisible(true);
        id_nuevoporcentajeCapital.setValue(nuevovalor);
        float jjjjjj = valor1 - resta;

        valueneg2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(nuevovalor);

        idPorcentageCalculadoCapQueda2222.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoCapQueda2222.setVisible(true);

    }

    
        public void modificaceldacapitalV2(String nuevovalor) {

        String jj = valueneg2.getValue();
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
//        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {
//
//            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
//        }
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setVisible(false);
        valueneg.setValue(peso_format);
        valueneg.setVisible(true);
        id_nuevoporcentajeCapital.setValue(nuevovalor);
        float jjjjjj = valor1 - resta;

        valueneg2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(nuevovalor);

        idPorcentageCalculadoCapQueda2222.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoCapQueda2222.setVisible(true);

    }

    
    
    public void modificaceldainteres(String nuevovalor) {

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);
        id_nuevoporcentaje.setValue(nuevovalor);

        float jjjjjj = valor1 - resta;

        idInterezQueda2222.setValue(nf.format(jjjjjj).replaceFirst("Ch", ""));

        String PorcentageQuedaStr22 = "%" + Float.parseFloat(nuevovalor);

        idPorcentageCalculadoInterzqueda23.setValue(PorcentageQuedaStr22);
        idPorcentageCalculadoInterzqueda23.setVisible(true);

    }

    public void modificaceldaHonorarios(float Capital, String nuevovalor) {
        float temp = 0;
        float temp2 = 0;
        for (SacaBop _sac : sacaboj) {

            //  temp = (float)Float.parseFloat(_sac.getHonorarioJudicial()) * (float)Capital / (float) 100 ;
            float honor = _sac.getHonorarioJuducial();

            //  temp = temp + (float) honor * (float) Capital / (float) 100;
            temp = temp + (float) honor * ((100 - (Float.parseFloat(nuevovalor))) / 100);
            temp2 = temp2 + (float) honor;
            //momento=momento+",["+_sac.getHonorarioJuducial()+"]";
        }

        //Messagebox.show("LEctura de contyrolador a acontrolador ["+momento+"]");
        String jj = idhonortot.getValue();
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        /// float resta = valor1 - porcentageToNum;
        float resta = temp2;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        String hono_recibe_peso = nf.format(temp).replaceFirst("Ch", "");
        String hono_condona_pesos = nf.format(temp2 - temp).replaceFirst("Ch", "");

        idPorcentageCalculadohonor.setValue(PorcentageQuedaStr);
        idPorcentageCalculadohonor.setVisible(true);
        idhonortot.setValue(peso_format);
        //id_nuevoporcentaje.setValue(nuevovalor);

        idHonorRecibe.setValue(hono_recibe_peso);
        idHonorarioQueda.setValue(hono_condona_pesos);
        id_nuevoporcentajeHonorarios.setValue(nuevovalor);
        idPorcentageCalculadoHonorqueda.setValue(PorcentageQuedaStr);

    }

    public void modificaceldaHonorariosV2(float Capital, String nuevovalor, float sumahonorariios) {
        float temp = 0;
        float temp2 = 0;
        for (SacaBop _sac : sacaboj) {

            //  temp = (float)Float.parseFloat(_sac.getHonorarioJudicial()) * (float)Capital / (float) 100 ;
            float honor = _sac.getHonorarioJuducial();

            //  temp = temp + (float) honor * (float) Capital / (float) 100;
            temp = temp + (float) honor * ((100 - (Float.parseFloat(nuevovalor))) / 100);
            temp2 = temp2 + (float) honor;
            //momento=momento+",["+_sac.getHonorarioJuducial()+"]";
        }

        //Messagebox.show("LEctura de contyrolador a acontrolador ["+momento+"]");
        String jj = idhonortot.getValue();
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = nuevovalor;

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(nuevovalor);
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        /// float resta = valor1 - porcentageToNum;
        float resta = temp2;
        String peso_format = nf.format(sumahonorariios).replaceFirst("Ch", "");
        String hono_recibe_peso = nf.format(sumahonorariios).replaceFirst("Ch", "");
        String hono_condona_pesos = nf.format(0).replaceFirst("Ch", "");

        idPorcentageCalculadohonor.setValue(PorcentageQuedaStr);
        idPorcentageCalculadohonor.setVisible(true);
        idhonortot.setValue(peso_format);
        //id_nuevoporcentaje.setValue(nuevovalor);

        idHonorRecibe.setValue(hono_recibe_peso);
        idHonorarioQueda.setValue(hono_condona_pesos);
        id_nuevoporcentajeHonorarios.setValue(nuevovalor);
        idPorcentageCalculadoHonorqueda.setValue(PorcentageQuedaStr);

    }

    @Listen("onChanging = textbox#id_nuevoporcentajeHonorarios")
    public void changeHonorarios(InputEvent event) {

        if (event.getValue().trim().length() == 0) {
            idPorcentageCalculadohonor.setVisible(false);

            return;
        }
        String valor = event.getValue();
        valor = valor.replace(",", ".");
        try {

            //aceptamos Float como datos de ingreso
            if (Float.parseFloat(valor) < Float.parseFloat("0") || Float.parseFloat(valor) >= Float.parseFloat("101")) {
                id_nuevoporcentajeHonorarios.setValue("0");
                Messagebox.show("Rango Valido 0 a 100 %");

                return;
            }
        } catch (NumberFormatException ex) {

            Messagebox.show("NumberFormatException::::El valor ingresado es invalido :[" + valor + "] Menssage : [" + ex.getMessage() + "]");

        } catch (WrongValueException ex) {
            Messagebox.show("WrongValueException::::El valor ingresado es invalido :[" + valor + "] Menssage : [" + ex.getMessage() + "]");
        }

        String valorin = id_nuevoporcentaje.getValue();
        valorin = valorin.replace(",", ".");
        float porcentajeInt_honor = Float.parseFloat(valor);
        float porcentajeInt_ingreso = Float.parseFloat(valorin);
        float montoIngresado = this.parsePesoToFloat(id_autorelleno.getValue());

        if (porcentajeInt_honor == 0 && porcentajeInt_ingreso == 100) {

            changeAutorellenoHonor(montoIngresado);
            idInterezQueda.setValue("$0");
            //Messagebox.show("aca deberiamos eliminar el porcentaje de ibteres");
        } else {

            String jj = ValorOriginalHonorario;
            String nn1 = jj.replace("$", "");
            String nn2 = nn1.replace(".", "");
            String valuellll = valor;

            // valor ineteres $$
            float valor1 = Float.parseFloat(nn2);
            //String iiiiii=id_nuevoAbonoCapital.getValue();
            float valor2 = Float.parseFloat(valuellll);

            float porcentageToNum = valor2 * valor1 / 100;
            float PorcentageQuedaNum = 100 - Float.parseFloat(valor);
            String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
            // String porcentageStr = "%"+porcentageNum;
            float resta = valor1;//- porcentageToNum;
            String peso_format = nf.format(resta).replaceFirst("Ch", "");
            idPorcentageCalculadohonor.setValue(PorcentageQuedaStr);
            idPorcentageCalculadohonor.setVisible(true);
            idhonortot.setValue(peso_format);

        }

    }

    // calculo automatico de interes
    public void changeAutorelleno2(float montoingresado, float Procentaje_estatico) {

        /// Inicio valor de VDES
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float VDES = Float.parseFloat(nn21);
        /// Fin valor de VDES

        float MontoTraeCliente = this.parsePesoToFloat(id_autorelleno.getValue());
        float menorMenor = MontoTraeCliente;
        float X = 0, Y = 0, Z = 0;

        float Interes_forzado = 0;
        Interes_forzado = Procentaje_estatico;
        //ValorOriginal  capital original
        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = id_autorelleno.getValue();  //event.getValue();

        float valor1 = Float.parseFloat(nn2);

        // interes
        String interes = this.ValorOriginalInterez;
        String interes2 = interes.replace("$", "");
        String interes3 = interes2.replace(".", "");
        //String interes4 = event.getValue();

        float _interes = Float.parseFloat(interes3);

        //id_nuevoporcentajeHonorarios
        // Condonacion Honorarios % 
        String Honor = id_nuevoporcentajeHonorarios.getValue();
        String honor2 = Honor.replace("$", "");
        //float hooo = Float.parseFloat(honor2);
        String honor3 = honor2.replace("", "");
        //String interes4 = event.getValue();

        // Condonacion Capital %
        String Cap = id_nuevoporcentajeCapital.getValue();
        String Cap2 = Cap.replace("$", "");
        String Cap3 = Cap2.replace("", "");

        float _PorcentajeCap = Float.parseFloat(Cap2);
        float _PorcentajeHonor = Float.parseFloat(honor2);

        float suma_honorariosRecibe = 0;
        float suma_honorariosCondona = 0;

        float d_p_cap = (float) _PorcentajeCap / (float) 100;
        float d_p_hon = (float) _PorcentajeHonor / (float) 100;

        //// formula para calcular el porcentaje capital basando en interes 100 y Honorarios 0
        //   MT  = p1 * ca + p2 INT + p3 * F(p1*ca)+ VDES
        //  MT = p1*ca +0 + 1* F(p1*ca) +vde
        //MT = p1 * ca + F(p1 * ca) + VDEs
        // _result= _control.calculaHonorariosJudiciales(ListDetOperModel,d_p_cap,d_p_hon, this.RutClienteFormateado);
        
         float suma_honorariosRecibe2 = 0;
        
        
        _result = _control.calculaHonorariosJudicialesV2(ListDetOperModel, d_p_cap, d_p_hon, this.RutClienteFormateado, MontoTraeCliente, VDES);
        for (HornorariosJudicialesCliente _res : _result) {

            suma_honorariosRecibe = suma_honorariosRecibe + _res.getHonorario_recibe();
            suma_honorariosCondona = suma_honorariosCondona + _res.getHonorario_condona();
            suma_honorariosRecibe2=suma_honorariosRecibe2 + (_res.getHONOR_F1()/_result.size());
        }
      suma_honorariosRecibe=suma_honorariosRecibe2;
      
        //Honorarios Judiciales
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = this.parsePesoToFloat(id_autorelleno.getValue());
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor1 > 0 ? (valor2 * 100 / valor1) : 0;// valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        List<PorcentajesXYZSimulador> _list = new ArrayList<PorcentajesXYZSimulador>();
        float ecuacionResultado = 0;

        Y = Interes_forzado > 0 ? Interes_forzado : 0;

        // Messagebox.show("CA[" + valor1 + "IN[" + _interes + "]HO[" + 1000000 + "]");
        for (int z = 0; z <= 100; z++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int x = 0; x <= 100; x++) {

                float ecuacion;
                float porcentaje_x = ((float) x / (float) 100);

                float porcentaje_z = ((float) z / (float) 100);

                //(float) ((float) pppp3 / (float) 100);
                ecuacion = porcentaje_x * (valor1) + porcentaje_z * suma_honorariosRecibe + VDES;
                float diferencia = ecuacion - MontoTraeCliente;
                float direfenciaabsoluta = Math.abs(diferencia);
                if (direfenciaabsoluta < menorMenor) {

                    menorMenor = direfenciaabsoluta;
                    X = x;
                    // Y = y;
                    Z = z;
                    ecuacionResultado = ecuacion;

                }

                if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                    PorcentajesXYZSimulador _puntoExito = new PorcentajesXYZSimulador(0, 0, 0);

                    _puntoExito.setX(x);
                    _puntoExito.setY(0);
                    _puntoExito.setY(z);
                    _list.add(_puntoExito);
                    menorMenor = MontoTraeCliente;
                }

            }

        }

        String Imprime = "";
        if (_list.size() > 0) {

            for (PorcentajesXYZSimulador puntos : _list) {
                Imprime = Imprime + "[x(" + puntos.getX() + "),y(" + puntos.getY() + "),z(" + puntos.getZ() + ")]";
            }

           // Messagebox.show("La ecuacion tiene los siguientes CEROS::[" + Imprime);
        }

        float valor11 = Float.parseFloat(nn21);

        float p1 = (((float) X / (float) 100));
        float p2 = (((float) Y / (float) 100));
        float p3 = (((float) Z / (float) 100));

        float pp1 = p1 * (valor1);
        float pp2 = p2 * _interes;
        float pp3 = p3 * suma_honorariosRecibe;

        float ooo = pp1 + pp2 + pp3 + VDES;

        String montoEcuacion = nf.format(ooo).replaceFirst("Ch", "");

       //    Messagebox.show("Valor Porcentaje Interez Ingresado: ["+Procentaje_estatico+"]  Monto Ingresado : ["+valuellll+"] X: ["+X+"] Y : ["+Y+"] Z : ["+Z+"]  Valor Ecuacion : ["+nf.format(ecuacionResultado).replaceFirst("Ch", "")+"]");
        float diferenciaddd = MontoTraeCliente - ooo;

        //formula para recalcular el porcentaje sobrante
        //||||0.12*3282754||||  = 10000000 -( 0.25*21.885.030    + 0.08*46849264 + 387000)
        //zz * suma_honorariosRecibe =  MontoTraeCliente - ( pp1 +   pp2 + VDES)  
        // zz= MontoTraeCliente - ( pp1 +   pp2 + VDES)   / suma_honorariosRecibe
        if (diferenciaddd < 0) {
      //   Messagebox.show("Diferencia Negativa");
        } else {
 //Messagebox.show("Diferencia Positiva");
        }

//            float pp1 =p1 * (valor1);                   // CAPITAL
//            float pp2 =p2 * _interes;                   // INTERES
//            float pp3 =p3 * suma_honorariosRecibe;      // HONORARIOS
        // float nuevo_p3 *suma_honorariosRecibe  =(MontoTraeCliente - ( pp1 +   pp2 + VDES) ) ;
        //p1 * (valor1) =MontoTraeCliente-(p3 * suma_honorariosRecibe  + p2 * _interes + VDES)
        //p1  =MontoTraeCliente -(pp3 + 0 + VDES)/(valor1)
        // float nuevo_p1=MontoTraeCliente -(pp3 + 0 + VDES)/(valor1);
        //valor1>0 ?  (valor2 * 100 / valor1):0;
        float nuevo_p1 = 0;
        float nuevo_pp1 = 0;

        if (_PorcentajeHonor > 0) {
            nuevo_p1 = valor1 > 0 ? ((MontoTraeCliente - (pp3 + VDES)) / (valor1)) : 0;

            nuevo_pp1 = nuevo_p1 * valor1;
        } else if (_PorcentajeHonor == 0) {

            nuevo_p1 = valor1 > 0 ? (((MontoTraeCliente - suma_honorariosRecibe - VDES)) / valor1) : 0;

            nuevo_pp1 = nuevo_p1 * valor1;

        }

        /*   if ((nuevo_pp1*100) >=100)
         {
         
          Messagebox.show("Porcentaje > 100  condonaremos CAPPPPP");
           
          
          //   nuevo_ph*suma_honorariosRecibe=mt -(pp1 + pp2 + vde )
          
          //    0 = mt-(pp1+0+vde)
          
          //   pp1 = mt-vde
          //   pc*cap = mt-vde
           // pc = (mt-vde)/cap
           
          float nuevo_p1 = (MontoTraeCliente-VDES)/valor1;
                  
         Messagebox.show("Porcentaje > 100  condonaremos CAPPPPP %con_cap["+nuevo_p1+"] nuevo valor pp1 :"+nf.format((nuevo_p1*valor1+VDES)).replaceFirst("Ch", "") +"");         
          nuevo_pp1=100;
          Y=100;
         }
         else
         { 
         
             
              Messagebox.show("(nuevo_p3*100):"+(nuevo_p3*100)+"]"); 
             
         }*/
        // Z=nuevo_p3;
        float ooo3 = nuevo_pp1 + pp3 + (VDES);
        float ooo2 = pp1 + pp2 + pp3 + (VDES);

        String montoEcuacionX = nf.format(ooo).replaceFirst("Ch", "");
        String montoDif = nf.format(diferenciaddd).replaceFirst("Ch", "");
        String montoDif2 = nf.format(ooo2).replaceFirst("Ch", "");
        //   Messagebox.show("Capital :["+nf.format(valor1).replaceFirst("Ch", "")+"] Interes :["+nf.format(_interes).replaceFirst("Ch", "")+"]  HONORARIOS :["+nf.format(suma_honorariosRecibe).replaceFirst("Ch", "")+"]  Vde ["+nf.format(VDES).replaceFirst("Ch", "")+"]   pp1 =["+nf.format(pp1).replaceFirst("Ch", "")+"] pp2=["+nf.format(pp2).replaceFirst("Ch", "")+"] pp3=["+nf.format(pp3).replaceFirst("Ch", "")+"]   menorMenor=[" + menorMenor + "] variables X[" + X + "] Y[" + Y + "] Z[" + Z + "]variables2 p1[" + p1 + "] p2[" + p2 + "] p3[" + p3 + "]  Sobrante2 :[DDD]  apriximado : " + montoEcuacionX + " Monto Trae Cliente- Monto calculado ::["+(montoDif)+"]  El Resultado de OOO2 es :["+montoDif2+"]");   

        // Messagebox.show("Nuevo % CAP  :["+nuevo_p1+"]  monto PP1 :["+nf.format(nuevo_pp1).replaceFirst("Ch", "")+"]   nuevoMonto ooo3:["+nf.format(ooo3).replaceFirst("Ch", "")+"]");
        // Messagebox.show("nuevo_p1 =(MontoTraeCliente - ( pp3 +   0 + VDES) )/ (valor1)   =  nuevo_p1 =["+nuevo_p1+"]  MontoTraeCliente :["+nf.format(MontoTraeCliente).replaceFirst("Ch", "")+"]   pp3:["+nf.format(pp3).replaceFirst("Ch", "")+"]valor1:["+nf.format(valor1).replaceFirst("Ch", "")+"] p3 * suma_honorariosRecibe  == p3 ["+p3+"] suma_honorariosRecibe :["+nf.format(suma_honorariosRecibe).replaceFirst("Ch", "")+"]");
        String nuevo_p_cap = Float.toString(100 - (nuevo_p1 * 100));
        // String cappp= _PorcentajeHonor >0 ?  Integer.toString(100 - X): Float.toString(100-nuevp_p1);

        modificaceldacapital(nuevo_p_cap);
        modificaceldainteres(Float.toString(100 - 0));
        modificaceldaHonorarios((float) 100 - (nuevo_p1 * 100), _PorcentajeHonor > 0 ? Float.toString(100 - p3) : Float.toString(100 - 100));

        float toalMenosVdes = valor2 - valor11;

        float HonorCondonadoRecibe = suma_honorariosRecibe > 0 ? ((float) Z / (float) 100 * suma_honorariosRecibe) : 0;

        float HonorCondonadoCondona = suma_honorariosRecibe > 0 ? ((float) (100 - Z) / (float) 100 * suma_honorariosRecibe) : 0;

        String montoHonorRecibePeso = nf.format(HonorCondonadoRecibe).replaceFirst("Ch", "");
        String montoHonorCondonaPeso = nf.format(HonorCondonadoCondona).replaceFirst("Ch", "");

        //idHonorQueda.setValue(montoHonorRecibePeso);
        // idHonorarioQueda.setValue(montoHonorCondonaPeso);
        // idHonorRecibe.setValue(montoHonorRecibePeso);
        //  suma_honorariosRecibe
        
        UpdateTotalRecibe();
        
        
        
    }

    //// funcionalidad del BOTON APLICAR
    @Listen("onClick = #ButtonSimula")
    public void SimulaMonto() {

        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (id_autorelleno.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }
        //String jj=valueneg.getValue();

        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = id_autorelleno.getValue();
        
        valuellll=valuellll.replace("$", "");
        valuellll=valuellll.replace(".", "");
        
        
        
        
        float MontoCLie = this.parsePesoToFloat(id_autorelleno.getValue());
        float valor1 = Float.parseFloat(nn2);

        String interes = this.ValorOriginalInterez;
        String interes2 = interes.replace("$", "");
        String interes3 = interes2.replace(".", "");
        //String interes4 = event.getValue();

        float _interes = Float.parseFloat(interes3);

        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = MontoCLie;
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor1 > 0 ? (valor2 * 100 / valor1) : 0; //valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        //id_nuevoporcentajeHonorarios
        // Condonacion Honorarios % 
        String Honor = id_nuevoporcentajeHonorarios.getValue();
        String honor2 = Honor.replace("$", "");
        String honor3 = honor2.replace(".", "");
        //String interes4 = event.getValue();

        // Condonacion Capital %
        String Cap = id_nuevoporcentajeCapital.getValue();
        String Cap2 = Cap.replace("$", "");
        String Cap3 = Cap2.replace(".", "");

        float _PorcentajeCap = Float.parseFloat(Cap3);
        float _PorcentajeHonor = Float.parseFloat(honor3);

        float suma_honorariosRecibe = 0;
        float suma_honorariosRecibe2 = 0;
        float suma_honorariosCondona = 0;

        _result = _control.calculaHonorariosJudiciales(ListDetOperModel, (float) _PorcentajeCap / (float) 100, (float) _PorcentajeHonor / (float) 100, this.RutClienteFormateado);

        for (HornorariosJudicialesCliente _res : _result) {

            suma_honorariosRecibe = suma_honorariosRecibe + _res.getHonorario_recibe();
            suma_honorariosRecibe2 = suma_honorariosRecibe + _res.getHONOR_F1();
            suma_honorariosCondona = suma_honorariosCondona + _res.getHonorario_condona();
        }
        suma_honorariosRecibe=suma_honorariosRecibe2;
        

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        List<PorcentajesXYZSimulador> _list = new ArrayList<PorcentajesXYZSimulador>();

        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float VDES = Float.parseFloat(nn21);

        float MontoTraeCliente = MontoCLie;
        float menorMenor = MontoTraeCliente;
        int X = 0, Y = 0, Z = 0;

        // Messagebox.show("CA[" + valor1 + "IN[" + _interes + "]HO[" + 1000000 + "]");
        for (int z = 0; z <= 100; z++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int x = 0; x <= 100; x++) {
                for (int y = 0; y <= 100; y++) {

                    float ecuacion;
                    float porcentaje_x = ((float) x / (float) 100);
                    float porcentaje_y = ((float) y / (float) 100);
                    float porcentaje_z = ((float) z / (float) 100);

                    //(float) ((float) pppp3 / (float) 100);
                    ecuacion = porcentaje_x * (valor1) + porcentaje_y * (_interes) + porcentaje_z * (suma_honorariosRecibe) + (VDES);
                    float diferencia = ecuacion - MontoTraeCliente;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor) {

                        menorMenor = direfenciaabsoluta;
                        X = x;
                        Y = y;
                        Z = z;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        PorcentajesXYZSimulador _puntoExito = new PorcentajesXYZSimulador(0, 0, 0);

                        _puntoExito.setX(x);
                        _puntoExito.setY(y);
                        _puntoExito.setY(z);
                        _list.add(_puntoExito);
                        menorMenor = MontoTraeCliente;
                    }

                }

            }

        }

        String Imprime = "";
        if (_list.size() > 0) {

            for (PorcentajesXYZSimulador puntos : _list) {
                Imprime = Imprime + "[x(" + puntos.getX() + "),y(" + puntos.getY() + "),z(" + puntos.getZ() + ")]";
            }

          //  Messagebox.show("La ecuacion tiene los siguientes CEROS::[" + Imprime);
        }

        float valor11 = Float.parseFloat(nn21);

        float ooo = ((float) X / (float) 100) * (valor1) + ((float) Y / (float) 100) * _interes + ((float) Z / (float) 100) * suma_honorariosRecibe + (VDES);

        float diferenciaddd = MontoTraeCliente - ooo;

        float menorMenor3 = MontoTraeCliente - ooo;

        float MontoTraeCliente2 = (float) menorMenor3;
        float menorMenor2 = MontoTraeCliente2;
        float XX = 0, YY = 0, ZZ = 0;

        for (int zz = 0; zz <= 100; zz++) {
            //neuronK[k]=new Neuron1(entCapak);
            for (int xx = 0; xx <= 100; xx++) {
                for (int yy = 0; yy <= 100; yy++) {

                    float ecuacion;
                    float porcentaje_xx = ((float) xx);
                    float porcentaje_yy = ((float) yy);
                    float porcentaje_zz = ((float) zz);

                    //(float) ((float) pppp3 / (float) 100);
                    // ecuacion = porcentaje_xx * X + porcentaje_yy * Y + porcentaje_zz * Z;
                    float A = Float.parseFloat("0." + xx);
                    float B = Float.parseFloat("0." + yy);
                    float C = Float.parseFloat("0." + zz);
                    ecuacion = A * (valor1) + B * (_interes) + C * (suma_honorariosRecibe) + (VDES);
                    float diferencia = ecuacion - MontoTraeCliente2;
                    float direfenciaabsoluta = Math.abs(diferencia);
                    if (direfenciaabsoluta < menorMenor3) {

                        menorMenor3 = direfenciaabsoluta;
                     //   Messagebox.show("Ecuacion : ["+ecuacion+"] A : ["+A+"] valor1: ["+valor1+"]  B : ["+B+"] interes : ["+interes+"] C : ["+C+"]  honorario :["+VDES+"]");
                        XX = A;
                        YY = B;
                        ZZ = C;

                    }

                    if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                        menorMenor3 = MontoTraeCliente2;
                    }

                }

            }

        }

        float p1 = (((float) X / (float) 100) + (XX));
        float p2 = (((float) Y / (float) 100) + (YY));
        float p3 = (((float) Z / (float) 100) + (ZZ));

        float pp1 = p1 * (valor1);
        float pp2 = p2 * _interes;
        float pp3 = p3 * suma_honorariosRecibe;

        //formula para recalcular el porcentaje sobrante
        //||||0.12*3282754||||  = 10000000 -( 0.25*21.885.030    + 0.08*46849264 + 387000)
        //zz * suma_honorariosRecibe =  MontoTraeCliente - ( pp1 +   pp2 + VDES)  
        // zz= MontoTraeCliente - ( pp1 +   pp2 + VDES)   / suma_honorariosRecibe
        float nuevo_p3 = 0;
        float nuevo_pp3 = 0;
        // float nuevp_p1=0;
        //float nuevo_pp1=0;

        //  if(_PorcentajeHonor > 0){
        nuevo_p3 = suma_honorariosRecibe > 0 ? ((MontoTraeCliente - (pp1 + pp2 + VDES)) / suma_honorariosRecibe) : 0;

        nuevo_pp3 = nuevo_p3 * suma_honorariosRecibe;
        // }
//         else if(_PorcentajeHonor==0){
//         
//                  nuevp_p1 =valor1 >0?  (((MontoTraeCliente - suma_honorariosRecibe - VDES) )  / valor1):0;
//         
//         nuevo_pp1=nuevp_p1*valor1;
//             
//             
//         
//         }

        // Z=nuevo_p3;
        //float diferenciaddd=MontoTraeCliente-ooo;
        float ooo3 = pp1 + pp2 + nuevo_pp3 + (VDES);
        float ooo2 = pp1 + pp2 + pp3 + (VDES);

        String montoEcuacion = nf.format(ooo).replaceFirst("Ch", "");
        String montoDif = nf.format(diferenciaddd).replaceFirst("Ch", "");
        String montoDif2 = nf.format(ooo2).replaceFirst("Ch", "");
        //Messagebox.show("Capital :["+nf.format(valor1).replaceFirst("Ch", "")+"] Interes :["+nf.format(_interes).replaceFirst("Ch", "")+"]  HONORARIOS :["+nf.format(suma_honorariosRecibe).replaceFirst("Ch", "")+"]  Vde ["+nf.format(VDES).replaceFirst("Ch", "")+"]   pp1 =["+nf.format(pp1).replaceFirst("Ch", "")+"] pp2=["+nf.format(pp2).replaceFirst("Ch", "")+"] pp3=["+nf.format(pp3).replaceFirst("Ch", "")+"]   menorMenor=[" + menorMenor + "] variables X[" + X + "] Y[" + Y + "] Z[" + Z + "]variables2 p1[" + p1 + "] p2[" + p2 + "] p3[" + p3 + "]  Sobrante2 :["+menorMenor3+"]  apriximado : " + montoEcuacion + " Monto Trae Cliente- Monto calculado ::["+(montoDif)+"] XX:["+XX+"] YY : ["+YY+"] ZZ:["+ZZ+"] El Resultado de OOO2 es :["+montoDif2+"]");   
        //  Messagebox.show("Nuevo % honor  :["+nuevo_p3+"]  monto PP3 :["+nf.format(nuevo_pp3).replaceFirst("Ch", "")+"]   nuevoMonto ooo3:["+nf.format(ooo3).replaceFirst("Ch", "")+"]");
        //  Messagebox.show("menorMenor=[" + menorMenor2 + "] variables X[" + XX + "] Y[" + YY + "] Z[" + ZZ + "]"); 
        //  id_nuevoporcentaje.setValue(Integer.toString(100-Y));
        //  Events.sendEvent(new Event("onChanging", ((Textbox) ((Window) mywin ).getFellow("id_nuevoporcentajeCapital")).setValue(Integer.toString(100-X)));
        //  id_nuevoporcentajeCapital.setValue(Integer.toString(100-X));
       // String cappp = _PorcentajeHonor > 0 ? Integer.toString(100 - X) : Float.toString(100 - p1);
        String cappp =  Float.toString((100-(p1*100)));
        modificaceldacapital(cappp);
        modificaceldainteres(Integer.toString(100 - Y));
        modificaceldaHonorarios(100 - (float) X, Float.toString(100 - (nuevo_p3 * 100)));

        float toalMenosVdes = valor2 - valor11;

        float HonorCondonadoRecibe = suma_honorariosRecibe > 0 ? ((float) Z / (float) 100 * suma_honorariosRecibe) : 0;

        float HonorCondonadoCondona = suma_honorariosRecibe > 0 ? ((float) (100 - Z) / (float) 100 * suma_honorariosRecibe) : 0;

        String montoHonorRecibePeso = nf.format(HonorCondonadoRecibe).replaceFirst("Ch", "");
        String montoHonorCondonaPeso = nf.format(HonorCondonadoCondona).replaceFirst("Ch", "");

//        idHonorQueda.setValue(montoHonorRecibePeso);
        idHonorarioQueda.setValue(montoHonorCondonaPeso);
        idHonorRecibe.setValue(montoHonorRecibePeso);
        
        
        
        
        CuadraValores();
        
        
        
        UpdateTotalRecibe();
        
        
        

    }
    
     public void CuadraValores(){
     
     String montoIngreso=id_autorelleno.getValue();
     String MontoRecibe=valueneg2ffff.getValue();
     
     
     montoIngreso=montoIngreso.replace("$", "");
     montoIngreso=montoIngreso.replace(".", "");
     
     MontoRecibe=MontoRecibe.replace("$", "");
     MontoRecibe=MontoRecibe.replace(".", "");
  
     
     float ingreso = Float.parseFloat(montoIngreso);
     float recibe = Float.parseFloat(MontoRecibe);
     
     // AA si monto ingresado es menor que el resultante
     
     
     if (ingreso < recibe){
     
         
     //  Messagebox.show("Ingreso es menor a lo que recibe");
     // quinamos al interes para cuadrar
         
         
     
     }
     
     
     
     }
    
    public void UpdateTotalRecibe(){
    
    
        float total=0;
        float total2=0;
        //// recibe
        String captital = valueneg.getValue();
        String honorario =idHonorRecibe.getValue();
        String Interez  = idInterezQueda.getValue();
           String vdes=valueneg3.getValue();
           
                   captital=captital.replace("$", "");
        captital=captital.replace(".", "");
        
        
        honorario=honorario.replace("$", "");
        honorario=honorario.replace(".", "");
        
        Interez=Interez.replace("$", "");
        Interez=Interez.replace(".", "");
           
           
           
        //condona
        
          String captitalC = valueneg2222.getValue();
        String honorarioC =idHonorarioQueda.getValue();
        String InterezC  = idInterezQueda2222.getValue();
        
        
               captitalC=captitalC.replace("$", "");
        captitalC=captitalC.replace(".", "");
        
        
        honorarioC=honorarioC.replace("$", "");
        honorarioC=honorarioC.replace(".", "");
        
        InterezC=InterezC.replace("$", "");
        InterezC=InterezC.replace(".", ""); 
     

        
        vdes=vdes.replace("$", "");     
        vdes=vdes.replace(".", "");
        
        
        total = Float.parseFloat(captital)+Float.parseFloat(honorario) + Float.parseFloat(Interez) + Float.parseFloat(vdes);
        total2 = Float.parseFloat(captitalC)+Float.parseFloat(honorarioC) + Float.parseFloat(InterezC) ;
        
        
        
        valueneg2ffff.setValue(nf.format(total).replaceFirst("Ch", ""));
        valueneg2222sdsds.setValue(nf.format(total2).replaceFirst("Ch", ""));
    }

    
    
        public void UpdateTotalCondona(){
    
    
    
    }
    
    @Listen("onChanging = textbox#id_autorelleno")
    public void changeAutorelleno(InputEvent event) {

        id_autorelleno.setValue(nf.format(this.parsePesoToFloat(event.getValue())).replaceFirst("Ch", ""));

    }

    private float parsePesoToFloat(String input) {
        float valueoutput = 0;

        String nuevoMonto1 = input.replace("$", "");
        String nuevoMonto12 = nuevoMonto1.replace(".", "");
        if ((!nuevoMonto12.equals(null)) && (!nuevoMonto12.equals(""))) {
            valueoutput = Float.parseFloat(nuevoMonto12);
        }

        return valueoutput;
    }

    // calculo automatico de interes
    public void changeAutorellenoHonor(float montoingresado) {

        /// Inicio valor de VDES
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float VDES = Float.parseFloat(nn21);
        /// Fin valor de VDES

        float MontoTraeCliente = this.parsePesoToFloat(id_autorelleno.getValue());
        float menorMenor = MontoTraeCliente;
        float X = 0, Y = 0, Z = 0;

        float Interes_forzado = 0;
        Interes_forzado = 100;
        //ValorOriginal  capital original
        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = id_autorelleno.getValue();  //event.getValue();

        float valor1 = Float.parseFloat(nn2);

        // interes
        String interes = this.ValorOriginalInterez;
        String interes2 = interes.replace("$", "");
        String interes3 = interes2.replace(".", "");
        //String interes4 = event.getValue();

        float _interes = Float.parseFloat(interes3);

        //id_nuevoporcentajeHonorarios
        // Condonacion Honorarios % 
        String Honor = id_nuevoporcentajeHonorarios.getValue();
        String honor2 = Honor.replace("$", "");
        //float hooo = Float.parseFloat(honor2);
        String honor3 = honor2.replace("", "");
        //String interes4 = event.getValue();

        // Condonacion Capital %
        String Cap = id_nuevoporcentajeCapital.getValue();
        String Cap2 = Cap.replace("$", "");
        String Cap3 = Cap2.replace("", "");

        float _PorcentajeCap = Float.parseFloat(Cap2);
        float _PorcentajeHonor = Float.parseFloat(honor2);

        float suma_honorariosRecibe = 0;
        float suma_honorariosCondona = 0;

        float d_p_cap = (float) _PorcentajeCap / (float) 100;
        float d_p_hon = 0;

        // _result= _control.calculaHonorariosJudiciales(ListDetOperModel,d_p_cap,d_p_hon, this.RutClienteFormateado);
        _result = _control.calculaHonorariosJudicialesV2(ListDetOperModel, d_p_cap, d_p_hon, this.RutClienteFormateado, MontoTraeCliente, VDES);
        for (HornorariosJudicialesCliente _res : _result) {

            suma_honorariosRecibe = suma_honorariosRecibe + _res.getHonorario_recibe();
            suma_honorariosCondona = suma_honorariosCondona + _res.getHonorario_condona();
        }

        //Honorarios Judiciales
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = this.parsePesoToFloat(id_autorelleno.getValue());
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor1 > 0 ? (valor2 * 100 / valor1) : 0;// valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        List<PorcentajesXYZSimulador> _list = new ArrayList<PorcentajesXYZSimulador>();
        float ecuacionResultado = 0;

        Y = Interes_forzado > 0 ? Interes_forzado : 0;

        // Messagebox.show("CA[" + valor1 + "IN[" + _interes + "]HO[" + 1000000 + "]");
        //neuronK[k]=new Neuron1(entCapak);
        for (int x = 0; x <= 100; x++) {

            float ecuacion;
            float porcentaje_x = ((float) x / (float) 100);

            //(float) ((float) pppp3 / (float) 100);
            ecuacion = porcentaje_x * (valor1) + 1 * suma_honorariosRecibe + VDES;
            float diferencia = ecuacion - MontoTraeCliente;
            float direfenciaabsoluta = Math.abs(diferencia);
            if (direfenciaabsoluta < menorMenor) {

                menorMenor = direfenciaabsoluta;
                X = x;
                // Y = y;
                ecuacionResultado = ecuacion;

            }

            if (direfenciaabsoluta == 0) { /// si alguno me sa exacto reseteo y sigo buscando

                PorcentajesXYZSimulador _puntoExito = new PorcentajesXYZSimulador(0, 0, 0);

                _puntoExito.setX(x);
                _puntoExito.setY(0);
                _puntoExito.setY(0);
                _list.add(_puntoExito);
                menorMenor = MontoTraeCliente;
            }

        }

        String Imprime = "";
        if (_list.size() > 0) {

            for (PorcentajesXYZSimulador puntos : _list) {
                Imprime = Imprime + "[x(" + puntos.getX() + "),y(" + puntos.getY() + "),z(" + puntos.getZ() + ")]";
            }

            Messagebox.show("La ecuacion tiene los siguientes CEROS::[" + Imprime);
        }

        float valor11 = Float.parseFloat(nn21);

        float p1 = (((float) X / (float) 100));
        // float p2 = (((float) Y / (float) 100)) ;
        // float p3 = (((float) Z / (float) 100));

        float pp1 = p1 * (valor1);
        //float pp2 =p2 * _interes;
        float pp3 = 1 * suma_honorariosRecibe;

        float ooo = pp1 + 0 + pp3 + VDES;

        String montoEcuacion = nf.format(ooo).replaceFirst("Ch", "");

        //   Messagebox.show("Valor Porcentaje Interez Ingresado: ["+Procentaje_estatico+"]  Monto Ingresado : ["+valuellll+"] X: ["+X+"] Y : ["+Y+"] Z : ["+Z+"]  Valor Ecuacion : ["+ecuacionResultado+"]");
        float diferenciaddd = MontoTraeCliente - ooo;

        //formula para recalcular el porcentaje sobrante
        //||||0.12*3282754||||  = 10000000 -( 0.25*21.885.030    + 0.08*46849264 + 387000)
        //zz * suma_honorariosRecibe =  MontoTraeCliente - ( pp1 +   pp2 + VDES)  
        // zz= MontoTraeCliente - ( pp1 +   pp2 + VDES)   / suma_honorariosRecibe
        if (diferenciaddd < 0) {

        } else {

        }

//            float pp1 =p1 * (valor1);                   // CAPITAL
//            float pp2 =p2 * _interes;                   // INTERES
//            float pp3 =p3 * suma_honorariosRecibe;      // HONORARIOS
        // float nuevo_p3 *suma_honorariosRecibe  =(MontoTraeCliente - ( pp1 +   pp2 + VDES) ) ;
        //p1 * (valor1) =MontoTraeCliente-(p3 * suma_honorariosRecibe  + p2 * _interes + VDES)
        //p1  =MontoTraeCliente -(pp3 + 0 + VDES)/(valor1)
        // float nuevo_p1=MontoTraeCliente -(pp3 + 0 + VDES)/(valor1);
        //valor1>0 ?  (valor2 * 100 / valor1):0;
        float nuevo_p1 = 0;
        float nuevo_pp1 = 0;

        nuevo_p1 = valor1 > 0 ? (((MontoTraeCliente - suma_honorariosRecibe - VDES)) / valor1) : 0;

        nuevo_pp1 = nuevo_p1 * valor1;

        float ooo3 = nuevo_pp1 + pp3 + (VDES);
        float ooo2 = pp1 + 0 + pp3 + (VDES);

        String montoEcuacionX = nf.format(ooo).replaceFirst("Ch", "");
        String montoDif = nf.format(diferenciaddd).replaceFirst("Ch", "");
        String montoDif2 = nf.format(ooo2).replaceFirst("Ch", "");
        //   Messagebox.show("Capital :["+nf.format(valor1).replaceFirst("Ch", "")+"] Interes :["+nf.format(_interes).replaceFirst("Ch", "")+"]  HONORARIOS :["+nf.format(suma_honorariosRecibe).replaceFirst("Ch", "")+"]  Vde ["+nf.format(VDES).replaceFirst("Ch", "")+"]   pp1 =["+nf.format(pp1).replaceFirst("Ch", "")+"] pp2=["+nf.format(pp2).replaceFirst("Ch", "")+"] pp3=["+nf.format(pp3).replaceFirst("Ch", "")+"]   menorMenor=[" + menorMenor + "] variables X[" + X + "] Y[" + Y + "] Z[" + Z + "]variables2 p1[" + p1 + "] p2[" + p2 + "] p3[" + p3 + "]  Sobrante2 :[DDD]  apriximado : " + montoEcuacionX + " Monto Trae Cliente- Monto calculado ::["+(montoDif)+"]  El Resultado de OOO2 es :["+montoDif2+"]");   

        // Messagebox.show("Nuevo % CAP  :["+nuevo_p1+"]  monto PP1 :["+nf.format(nuevo_pp1).replaceFirst("Ch", "")+"]   nuevoMonto ooo3:["+nf.format(ooo3).replaceFirst("Ch", "")+"]");
        // Messagebox.show("nuevo_p1 =(MontoTraeCliente - ( pp3 +   0 + VDES) )/ (valor1)   =  nuevo_p1 =["+nuevo_p1+"]  MontoTraeCliente :["+nf.format(MontoTraeCliente).replaceFirst("Ch", "")+"]   pp3:["+nf.format(pp3).replaceFirst("Ch", "")+"]valor1:["+nf.format(valor1).replaceFirst("Ch", "")+"] p3 * suma_honorariosRecibe  == p3 ["+p3+"] suma_honorariosRecibe :["+nf.format(suma_honorariosRecibe).replaceFirst("Ch", "")+"]");
        String nuevo_p_cap = Float.toString(100 - (nuevo_p1 * 100));
        // String cappp= _PorcentajeHonor >0 ?  Integer.toString(100 - X): Float.toString(100-nuevp_p1);

        modificaceldacapital(nuevo_p_cap);
        modificaceldainteres(Float.toString(100 - 0));
        modificaceldaHonorariosV2((float) 100 - (nuevo_p1 * 100), Float.toString(0), suma_honorariosRecibe);

        float toalMenosVdes = valor2 - valor11;

        float HonorCondonadoRecibe = suma_honorariosRecibe > 0 ? ((float) Z / (float) 100 * suma_honorariosRecibe) : 0;

        float HonorCondonadoCondona = suma_honorariosRecibe > 0 ? ((float) (100 - Z) / (float) 100 * suma_honorariosRecibe) : 0;

        String montoHonorRecibePeso = nf.format(HonorCondonadoRecibe).replaceFirst("Ch", "");
        String montoHonorCondonaPeso = nf.format(HonorCondonadoCondona).replaceFirst("Ch", "");

        //idHonorQueda.setValue(montoHonorRecibePeso);
        // idHonorarioQueda.setValue(montoHonorCondonaPeso);
        // idHonorRecibe.setValue(montoHonorRecibePeso);
        //  suma_honorariosRecibe
    }

}
