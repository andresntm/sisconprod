/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.codec.digest.DigestUtils;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zss.ui.Spreadsheet;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.ColaboradorJDBC;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.ClienteInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;

/**
 *
 * @author excosoc
 */
@SuppressWarnings("serial")
public class UploadController extends SelectorComposer<Window> {

    /*Ejemplo integraci�n Adjuntar en Padre:
    
    -- Variable
    
      private EventQueue eq;
      private List<Media> fileAdjuntos;
    
    -- Adjuntar al doAfterCompose o donde declaren los eventos del controller
    
      //Herencia entre Adjuntar y Padre
            eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    fileAdjuntos = new ArrayList<Media>();
                    fileAdjuntos = (List<Media>) map.get("adjuntos");
                }
            });
    
    --Evento que levanta el PopUp
    
    public void Adjuntar() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        try {
            Window window = (Window) Executions.createComponents(
                    "Adjuntador/Adjuntar.zul", null, map
            );
            window.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }
     */
    ////////////////////////////////////////////////////////////////////////////
    @Wire
    Button btn_Upload;
    @Wire
    Listbox Lbox_Archivos;
    @Wire
    Spreadsheet inc_include;
    @Wire
    Button btn_subir;
    Fileupload btn_up;
    @Wire
    Window wnd_AdjuntarDctos;
    @Wire
    Listheader lh_eliminar;
    @Wire
    Listheader lh_user;
    ////////////////////////////////////////////////////////////////////////////
    private List<Media> FilesCargados;
    private int NumArch = 2;
    private EventQueue eq;
    private String Label_btn = "Adjuntar";
    private String Class_btn = "btn btn-sm btn-primary";
    private String Upload_btn = "true,maxsize=5120,native,accept=.odt|.doc|.docx|.rtf|.xls|.xlsx|.ods|.pdf|.csv|.jpg|.png";
    private Session sess;
    private String user = "Admin";
    private List<Media> listArchivos = new ArrayList<Media>();
    private ListModelList<AdjuntarDET> listModelArchivos = new ListModelList<AdjuntarDET>();
    private int MaxMBSizeFile = 5;//tama�o maximo archivos a subir en MB por defecto 5MB maximo
    private List<Media> listGuardados;
    private boolean graba;
    private List<AdjuntarDET> listDet;
    private AdjuntarENC aEnc;
    private int id_colaborador = 0;
    private UsuarioDAO uDAO;
    private String modulo = "";
    private MvcConfig mC;
    private float tama�o;
    private ColaboradorJDBC colaborador;
    private ClienteInterfaz rutCli;
    private String rutCliente;
    private CondonacionesEspeciales validaCE = null;

    public UploadController() {
        rutCli = new ClienteInterfazImpl();
    }

    private void cargaListBox() {

        listModelArchivos = new ListModelList<AdjuntarDET>(listDet);
        Lbox_Archivos.setModel(listModelArchivos);

//        HashMap<String, Media> map = new HashMap<String, Media>();
//        Set<Entry<String, Media>> set;
        try {
//            if (listArchivos.isEmpty()) {
//                listArchivos = listDet.;
//            } else {
//                listArchivos = ListUtils.union(listArchivos, newData);
//
//                for (Media recorre : listArchivos) {
//                    map.put(recorre.getName(), recorre);
//                    set = map.entrySet();
//                }
//                listArchivos.clear();
//
//                for (Entry<String, Media> entry : set) {
//                    listArchivos.add(entry.getValue());
//                }
//
//            }

//            listModelArchivos = new ListModelList<Media>(listArchivos);
            Lbox_Archivos.setModel(listModelArchivos);

            Lbox_Archivos.setItemRenderer(new ListitemRenderer<AdjuntarDET>() {

                Listcell cellName;
                Listcell cellEliminar;
                Listcell cellUser;

                public void render(Listitem item, final AdjuntarDET data, int index) throws Exception {

                    final Label lbl_NameArch = new Label();
                    String colab = "";

                    item.setId(Integer.toString(index));
                    lbl_NameArch.setValue(data.getFileName());

                    cellName = new Listcell();
                    cellName.setStyle("text-align:left;");
                    lbl_NameArch.setParent(cellName);

                    cellEliminar = new Listcell();

                    if (data.isNewFile()) {
                        {
                            final Button btn_EliminarArch = new Button();

                            btn_EliminarArch.setLabel("Eliminar");
                            btn_EliminarArch.setClass("btn btn-danger");

                            btn_EliminarArch.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                                public void onEvent(Event event) throws Exception {
                                    Button btn = (Button) event.getTarget();

                                    Listitem itemId = (Listitem) btn.getParent().getParent();

                                    int idEliminar = Integer.parseInt(itemId.getId());
                                    Label nameEliminar = (Label) itemId.getChildren().get(0).getChildren().get(0);
                                    final String nameFile = nameEliminar.getValue();
                                    final List<AdjuntarDET> listaFiltro = new ArrayList<AdjuntarDET>();

                                    listDet.forEach(new Consumer<AdjuntarDET>() {
                                        public void accept(AdjuntarDET t) {

                                            if (t.isNewFile()) {
                                                if (t.getFileUp().getName().equals(nameFile)) {
                                                } else {
                                                    listaFiltro.add(t);
                                                }
                                            } else {
                                                if (t.getFileName().equals(nameFile)) {
                                                } else {
                                                    listaFiltro.add(t);
                                                }
                                            }
                                        }

                                    });

                                    listDet = listaFiltro;
                                    RefreshList();

                                }

                            });

                            btn_EliminarArch.setParent(cellEliminar);
                        }
                        {
                            colab = user;
                        }
                    } else {
                        {
                            final Label lbl_Eli = new Label();

                            lbl_Eli.setValue("Bloqueado");
                            lbl_Eli.setParent(cellEliminar);
                        }
                        {
                            colab = colaborador.getColaborador(data.getFk_idColaborador());
                        }
                    }

                    cellUser = new Listcell();

                    final Label lbl_Eje = new Label();

                    lbl_Eje.setValue(colab);
                    lbl_Eje.setParent(cellUser);

                    cellName.setParent(item);
                    cellEliminar.setParent(item);
                    cellUser.setParent(item);

                    if (!data.isNewFile()) {
                        item.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                Filedownload.save(data.getFileDown(), null);

//                                if (data.getExtension().equals("xls") || data.getExtension().equals("xlsx")) {
//
//                                    final HashMap<String, Object> map = new HashMap<String, Object>();
//
////                                    map.put("SpreadsheetAdjunto", data.getFileDown());
//                                    sess.setAttribute("SpreadsheetAdjunto", data.getFileDown());
//
//                                    try {
////                                        Window glosaWin = (Window) Executions.createComponents(
////                                                "Adjuntador/SpreadSheet.zul", null, map
////                                        );
////                                        
////                                        glosaWin.;
//                                        Executions.getCurrent().sendRedirect("/siscon/Adjuntador/SpreadSheet", "_blank");
//
//                                    } catch (Exception e) {
//                                        Messagebox.show("Sr(a). Usuario(a), se encontro un error al abrir la vista de excel. \nError: " + e);
//                                    }
//
//                                }
                            }

                        });
                    }
                }

            });

        } catch (Exception ex) {
            Messagebox.show("No se pudo cargar la Lista.\n" + ex.toString());
        }
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        try {

            cargaPag();

        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Siscon encontro un error al cargar la pagina .\nError: " + ex);
            wnd_AdjuntarDctos.detach();
        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), Siscon encontro un error al cargar la pagina .\nError: " + ex);
            wnd_AdjuntarDctos.detach();
        }

    }

    private void cargaPag() {
        final Execution exec = Executions.getCurrent();

        try {
            listGuardados = new ArrayList<Media>();
            listDet = new ArrayList<AdjuntarDET>();
            aEnc = new AdjuntarENC();
            sess = Sessions.getCurrent();
            mC = new MvcConfig();
            uDAO = new UsuarioJDBC(mC.getDataSource());
            colaborador = new ColaboradorJDBC();
            validaCE = new CondonacionesEspeciales();

            eventos();

            UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
            user = permisos.getCuenta();
            id_colaborador = uDAO.buscarColaborador(user);

            if (exec.getArg().get("ListAdjuntoSS") != null) {
                listDet = (List<AdjuntarDET>) exec.getArg().get("ListAdjuntoSS");
            }
            if (exec.getArg().get("modulo") != null) {
                modulo = (String) exec.getArg().get("modulo");
            }
            if (exec.getArg().get("rutClienteAdjunto") != null) {
                rutCliente = (String) exec.getArg().get("rutClienteAdjunto");
            }
            if (exec.getArg().get("AdjuntaEspecial") != null) {
                validaCE = (CondonacionesEspeciales) exec.getArg().get("AdjuntaEspecial");
            }

            validaModulo();

            listDet.forEach(new Consumer<AdjuntarDET>() {
                public void accept(AdjuntarDET t) {

                    if (t.isNewFile()) {

                    } else {
                        File cargar = new File(t.getPatch_archivo());
                        if (cargar.exists()) {
                            t.setFileDown(cargar);
                        }

                        tama�o += t.getTama�oNum();
                    }
                }

            });

            Lbox_Archivos.setEmptyMessage("No hay archivos cargados.");
            cargaListBox();

        } catch (SQLException sqlE) {
            Messagebox.show("Sr(a). Usuario(a), error al adjuntar.\nError: " + sqlE.toString());
            wnd_AdjuntarDctos.detach();

        } catch (Exception E) {
            Messagebox.show("Sr(a). Usuario(a), error al adjuntar.\nError: " + E.toString());
            wnd_AdjuntarDctos.detach();

        }
    }

    private void eventos() {
        try {
            SimpleDateFormat sdfFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date finMinuta = sdfFormat.parse("22/11/2018");
            String formatosResividos = "true,maxsize=5120,native,accept=.odt|.doc|.docx|.rtf|.ods|.pdf|.jpg|.png";
            Date hoy = new Date();

            int diasFaltantes = 0;
            long fltQuedan = (finMinuta.getTime() - hoy.getTime());

            if (fltQuedan > 0) {
                formatosResividos = this.Upload_btn;
            }

            if (validaCE.getId_TipoEspecial() > 0) {
                formatosResividos = this.Upload_btn;
            }

            btn_Upload.setUpload(formatosResividos);

            btn_Upload.setLabel(this.Label_btn);
            btn_Upload.setClass(this.Class_btn);

            btn_Upload.addEventListener(Events.ON_UPLOAD, new EventListener<UploadEvent>() {
                public void onEvent(UploadEvent event) throws Exception {
                    preUploadFile(event);
                }

            });

            btn_subir.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {

                    try {

                        uploadFile();//dejar archivos en el servidor

                        final HashMap<String, Object> map = new HashMap<String, Object>();

                        if (graba) {
                            map.put("ListAdjuntoSS", listDet);
                            map.put("tama�o", tama�o);
                            eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, false);
                            eq.publish(new Event("onButtonClick", btn_subir, map));
                            wnd_AdjuntarDctos.detach();
                        }

                    } catch (WrongValueException e) {
                        Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                    } catch (Exception e) {
                        Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                    } finally {
//                    wnd_AdjuntarDctos.setAction("onClose");
                    }

                }

            });

            wnd_AdjuntarDctos.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

                @Override
                public void onEvent(Event arg0) throws InterruptedException {
                    try {
                        final HashMap<String, Object> map = new HashMap<String, Object>();
                        map.put("ListAdjuntoSS", listDet);
                        map.put("tama�o", tama�o);
                        eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, false);
                        eq.publish(new Event("onClose", wnd_AdjuntarDctos, map));

                    } catch (WrongValueException e) {
                        Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error en el proceso. \n Error: " + e.toString());
                    } catch (Exception e) {
                        Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error en el proceso. \n Error: " + e.toString());
                    } finally {
                        wnd_AdjuntarDctos.setAction("onClose");
                    }
                }
            });
        } catch (ParseException ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void preUploadFile(UploadEvent Ue) {

        this.FilesCargados = Arrays.asList(Ue.getMedias());

        try {
            if (FilesCargados.isEmpty()) {
                Messagebox.show("Sr(a) usuario(a), debe seleccionar al menos un archivo para continuar.", "Mensaje del Administrador.", Messagebox.OK, Messagebox.INFORMATION);
                return;
            } else {
                for (Media valida : listArchivos) {
                    if (valida.getName().equals(FilesCargados.get(0).getName())) {
                        Messagebox.show("Sr(a) usuario(a), el archivo seleccionado ya se encuentra cargado.", "Mensaje del Administrador.", Messagebox.OK, Messagebox.INFORMATION);
                    }
                }
//                listArchivos.forEach(new Consumer<Media>);
            }

            tama�o = 0;

            FilesCargados.forEach(new Consumer<Media>() {
                public void accept(Media t) {
                    AdjuntarDET aDet = new AdjuntarDET();

                    aDet.setFileUp(t);//ARCHIVO COMPLETO
                    aDet.setNewFile(true); //ARCHIVO NUEVO? SI
                    aDet.setFk_idColaborador(id_colaborador);//ID DEL COLABORADOR QUE REALIZA LA CONDONASAUN XD
                    aDet.setFileName(t.getName());//NOMBRE ACTUAL DEL ARCHIVO, ESTE VARIA AL FINAL DE LA ITERACI�N 
                    aDet.setExtension(t.getFormat());//EXTENCI�N ARCHIVO
                    aDet.setTama�o(String.format("%.4f", (((((float) t.getByteData().length) / 1024) / 1024))) + "Mb"); //PESO DEL ARCHIVO EN MEGAS STRING
                    aDet.setTama�oNum((((((float) t.getByteData().length) / 1024) / 1024))); //PESO DEL ARHCIVO EN MEGAS
                    tama�o += (((((float) t.getByteData().length) / 1024) / 1024)); //SUMA TODOS LOS PESOS

                    listDet.add(aDet);
                }

            });

            cargaListBox();

        } catch (Exception Ex) {
            Messagebox.show("Error: " + Ex.toString());
        }

    }

    private void uploadFile() {

        graba = false;

        try {
            if (!listDet.isEmpty()) {
                if (listDet != null) {
                    listDet.forEach(new Consumer<AdjuntarDET>() {
                        public void accept(AdjuntarDET t) {
                            if (t.isNewFile()) {
                                String NameFile = "";
                                final StreamSource fileFinal;
                                String filePath = "";
                                File termino;
                                NameFile = user;
                                NameFile += "_" + LocalDateTime.now();
                                NameFile = NameFile.replace(":", "-");
                                String md5 = DigestUtils.md5Hex(t.getFileUp().getName() + LocalDateTime.now());

                                if (rutCliente != null) {
                                    if (!rutCliente.equals("")) {

                                        NameFile += "_" + md5 + "." + t.getFileUp().getFormat();
                                    } else {
                                        NameFile += "_" + rutCliente + "_" + md5 + "." + t.getFileUp().getFormat();
                                    }
                                } else {
                                    NameFile += "_" + md5 + "." + t.getFileUp().getFormat();
                                }
//                               
//                        filePath = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
//                        filePath += LocalDate.now(ZoneId.of("Europe/Paris"));
                                filePath = "/opt/archivos/siscon/" + LocalDate.now(ZoneId.of("Europe/Paris"));

                                File baseDir = new File(filePath);

                                if (!baseDir.exists()) {
                                    baseDir.mkdirs();
                                }

                                filePath += "/" + NameFile;

                                t.setFileName(NameFile);
                                t.setPatch_archivo(filePath);

                                //Validamos el tipo de contenido del archivo para poder ajustar segun corresponda.
                                if (t.getFileUp().inMemory()) {
                                    if (t.getFileUp().isBinary()) {
                                        fileFinal = new StreamSource(new ByteArrayInputStream(t.getFileUp().getByteData()));
                                    } else {
                                        fileFinal = new StreamSource(t.getFileUp().getReaderData());
                                    }
                                } else {
                                    if (t.getFileUp().isBinary()) {
                                        fileFinal = new StreamSource(t.getFileUp().getStreamData());
                                    } else {
                                        fileFinal = new StreamSource(t.getFileUp().getReaderData());
                                    }
                                }

                                try {

                                    Files.copy(new File(filePath), fileFinal.getInputStream());

                                } catch (IOException ex) {
                                    Logger.getLogger(UploadController.class
                                            .getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }

                    });
//                    Messagebox.show("Sr(a). Usuario(a), Los archivos se cargaron correctamente.", "Adjuntar", Messagebox.OK, Messagebox.INFORMATION);
                    graba = true;
                } else {
                    Messagebox.show("Sr(a). Usuario(a), no hay archivos para cargar.", "Adjuntar", Messagebox.OK, Messagebox.INFORMATION);
                    graba = false;
                }
            } else {
                Messagebox.show("Sr(a). Usuario(a), debe seleccionar al menos un archivo para subir.");
                graba = false;
            }
//            inc_include.setSrc(filePath);
//            inc_include.disableUserAction(AuxAction.PROTECT_SHEET, true);
//            inc_include.applyProperties();
//            Sheet sheet = inc_include.getSelectedSheet();
//
//            if (sheet.isProtected()) {
//                Ranges.range(sheet).unprotectSheet("password");
//            } else {
//                Ranges.range(sheet).protectSheet("password",
//                        true, true, false, false, false, false, false,
//                        false, false, false, false, false, false, false, false);
//            }
//            inc_include.setVisible(false);

            listGuardados = listArchivos;//respaldamos archivos subidos para dejar registro en la DB
            listArchivos = new ArrayList<Media>();

//            listModelArchivos = new ListModelList<Media>(listArchivos);
            Lbox_Archivos.setModel(listModelArchivos);
            Lbox_Archivos.setVisible(false);
            Lbox_Archivos.setVisible(true);

        } catch (Exception Ex) {
            Messagebox.show("Error: " + Ex.toString());
            System.out.println(Ex.getMessage());
            graba = false;
        }
    }

    private void RefreshList() {
        listModelArchivos.clear();
        listModelArchivos = new ListModelList<AdjuntarDET>(listDet);
        Lbox_Archivos.setModel(listModelArchivos);
        Lbox_Archivos.setVisible(false);
        Lbox_Archivos.setVisible(true);
    }

    private void validaModulo() {
        if (modulo.equals("AnSacabopView")) {
            btn_subir.setVisible(false);
            btn_Upload.setVisible(false);
        }

    }
}
