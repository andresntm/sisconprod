/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.A;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Div;
import org.zkoss.zul.Popup;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.HttpSessionJDBC;

/**
 *
 * @author esilves
 */
public class NavbarController extends SelectorComposer<Component> {

    @Wire
    A /*atask,*/ anoti, amsg;
    @Wire
    A a_Aplicadas;
    @Wire
    A a_Aprobadas;
    @Wire
    A a_Reparadas;
    @Wire
    A a_Rechazadas;
    @Wire
    A a_Total;
    @Wire
    Div div_nav;
    @Wire
    Popup pop_Alert;
    @Wire
    Menu _menu_ususario;
    HttpSessionJDBC _httpsession;
    Session sess;
    HttpSession hses;
    String httpsession;
    int idsessionsiscon;
     MvcConfig mmmm = new MvcConfig();
    public NavbarController() throws SQLException {
        
        
        _httpsession= new  HttpSessionJDBC(mmmm.getDataSource());
    }
    
    
    
    
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        div_nav = (Div) comp;
        Session sess = Sessions.getCurrent();
        
        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        String Account = permisos.getCuenta();//user.getAccount();
        String Nombre = permisos.getNombre();
        //Limpiamos 
        _menu_ususario.setTooltiptext("");
        _menu_ususario.setLabel("");
        // seteamos
        _menu_ususario.setTooltiptext(Account);
        _menu_ususario.setLabel("Bienvenido, " + Nombre);
        hses=(HttpSession) sess.getNativeSession();
        httpsession =hses.getId();
        idsessionsiscon=_httpsession.GetSisConIdSession(httpsession);
        
        

    }

//    @Listen("onOpen = #taskpp")
//    public void toggleTaskPopup(OpenEvent event) {
//        toggleOpenClass(event.isOpen(), atask);
//    }
    @Listen("onOpen = #notipp")
    public void toggleNotiPopup(OpenEvent event) {
        _httpsession.setSessionAccion("onOpen = #notipp",idsessionsiscon,2);
        
        toggleOpenClass(event.isOpen(), anoti);
    }

    @Listen("onOpen = #msgpp")
    public void toggleMsgPopup(OpenEvent event) {
         _httpsession.setSessionAccion("onOpen = #msgpp",idsessionsiscon,2);
        toggleOpenClass(event.isOpen(), amsg);
    }

    // Toggle open class to the component
    public void toggleOpenClass(Boolean open, Component component) {
        _httpsession.setSessionAccion(" Toggle open class to the component",idsessionsiscon,2);
        HtmlBasedComponent comp = (HtmlBasedComponent) component;
        String scls = comp.getSclass();
        if (open) {
            comp.setSclass(scls + " open");
        } else {
            comp.setSclass(scls.replace(" open", ""));
        }
    }
}
