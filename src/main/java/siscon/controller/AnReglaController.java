/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import net.sourceforge.jtds.jdbc.DateTime;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Cliente;
import siscon.entidades.Condonacion;
import siscon.entidades.DetalleCliente;
import siscon.entidades.JudicialCliente;
import siscon.entidades.SacaBop;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.ClienteSisconJDBC;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.CondonadorJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class AnReglaController extends SelectorComposer<Window> {

    @Wire
    Label negoc;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox nro_condonacion;
    @Wire
    Label viewrutcliente;
    @Wire
    Grid Grid_Sacabop;
    @Wire
    Grid Grid_SacabopXX;

    @Wire
    Textbox id_fechahoy;
    @Wire
    Textbox id_nomcliente;

    @Wire
    Textbox id_oficinaorigen;
    @Wire
    Textbox id_TotalSumaCondonaCapital;

    @Wire
    Textbox id_TotalSumaCapital;

    @Wire
    Textbox id_TotalSumaRecibeCapital;
    @Wire
    Textbox id_saldototalmoroso;

    @Wire
    Label idTotalRecibe;
    @Wire
    Label id_TotalCondona;

    @Wire
    Label id_TotlaCapital;
    @Wire
    Textbox id_TotalSumaInteres;
    @Wire
    Textbox id_TotalSumaCondonaInteres;
    @Wire
    Textbox id_TotalSumaRecibeInteres;

    // identificadores de textbod judiciales en la grilla
    @Wire
    Textbox id_TotalSumaHonorJud;
    @Wire
    Textbox id_TotalSumaHonorJudCond;
    @Wire
    Textbox id_TotalSumaHonorJudRec;

    // Totoales Parciales Recibe Color GREEN
    @Wire
    Textbox id_TotoalParcialCapital;
    @Wire
    Textbox id_TotoalParcialInteres;
    @Wire
    Textbox id_TotoalParcialhonor;

    //Totoales Parciales Condona Color Red
    @Wire
    Textbox id_TotoalParcialCondonaCapital;
    @Wire
    Textbox id_TotoalParcialCondonaInteres;
    @Wire
    Textbox id_TotoalParcialCondonaHono;

    /// Totoales parciales Capital YELLOW
    @Wire
    Textbox id_TotoalParcialCapita;
    @Wire
    Textbox id_TotoalParcialCapitaInteres;
    @Wire
    Textbox id_TotoalParcialCapitaHonor;

    /// variables del llenado de info de condonacion
    @Wire
    Textbox id_PrimerMesDeCondonacion;
    @Wire
    Textbox id_MesMasAntiguoCastigo;

    @Wire
    Textbox id_AtribucionEjecutiva;
    @Wire
    Textbox id_porcentajeCondoEjecutiva;

    @Wire
    Textbox id_PuedeCondonarOnline;

    @Wire
    Textbox id_rangoFechaCondonacion;
    @Wire
    Panel panelgridddd;

    @Wire
    Label id_msgeCona;

    @Wire
    Button btn_enviaSacaBopg;

    @Wire
    Button btn_GenrarCondonacion;

    @Wire
    Vlayout vlayoutmensajje;

    @Wire
    Window id_windowsMessajje;
    @Wire
    Window id_winSacabobAnalista;

    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    @WireVariable
    ListModelList<SacaBop> sacaboj;
    final DetalleOperacionesClientes detoper;
    final JudicialClienteInterz JudCliente;
    final CondonacionInterfaz cond;
    String FechaPrimerCastigo;
    int mesescastigomasantiguo;
    Condonacion CurrentCondonacion;
    List<DetalleCliente> ListDetOper = null;
    //variables sumatorias totales de cada columna por orden 1->A  etc...
    int RutClienteFormateado;
    long totalA;
    int MaximoMesCastigo;
    long totalB;
    long totalC;
    long totalD;
    long totalE;
    long totalF;
    long totalG;
    long totalH;
    long totalI;
    final ClienteInterfazImpl clienteinfo;
    @WireVariable
    Cliente InfoCliente;
    ClienteSisconJDBC _cliente;
//final CondonacionImpl CondonacionImplDAO;
    @Wire
    Label IsCondonacion;
    Session sess;
    String AreaTrabajo;
    String cuenta;
    String Nombre;
    String Cliente;
    String jj;
    String xx;
CondonadorJDBC _condonadorJDBC;
    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        sess = Sessions.getCurrent();
        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

    //    Cliente = _cliente.GetRutCliente(Integer.parseInt(IsCondonacion.getValue()));
     //   jj = Cliente.replace("[", "");
     //   xx = jj.replace("]", "");
        //Messagebox.show("RUUT::"+viewrutcliente.getValue()+"Condonacion:["+IsCondonacion.getValue()+"]");
     //   String rutcliente = xx;
      //  String mm = rutcliente.replace(".", "");
      //  String[] ParteEntera = mm.split("-");




        sacaboj = new ListModelList<SacaBop>();
        String oper = "NULL";
        String MeseCatigo = "NULL";
        String FechaCastigo = "14-01-2012 09:29:58";
        //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = new Date();
        String[] parse;
        //d2=format.parse(d2.toString());
        DateTime NN;
        // creamos instancia de condonacion      

        CurrentCondonacion = new Condonacion("14212287-1", cuenta, sess.getWebApp().toString());
        
        
                this.CurrentCondonacion.setTieneJuicio(true);
        
                 FechaCastigo = "14-01-2012 09:29:58";
                        d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
             //   temp.setOperacion(oper);
             //   temp.setMesesCastigo(MeseCatigo);

                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigo) {
                    this.MaximoMesCastigo = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigo);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
        
         CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonador(permisos.getDi_rut(),permisos.getNombre(),this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
        //float atrMa=CurrentCondonacion._condonador.getAtribucionMaxima();
        this.CurrentCondonacion.setMontoMaximoAtribucionEjecutiva(CurrentCondonacion._condonador.getAtribucionMaxima());
        this.CurrentCondonacion.setMontoAtribucionMaxima(CurrentCondonacion._condonador.getAtribucionMaxima());

        this.CurrentCondonacion.setPorcentajeCondonaCapital(0.7);
        this.CurrentCondonacion.setPorcentajeCondonaHonorario(0.7);
        this.CurrentCondonacion.setPorcentajeCondonaInteres(0.7);
        this.CurrentCondonacion.setRangoFechaInicio("36");
        this.CurrentCondonacion.setRangoFehaFin("infinito");
        this.CurrentCondonacion.setPuedeCondonarEnLinea(false);

        
        
        

        
        
        /// Si esta en  juicio la operacion o no le cobramos el porcentaje correspondiente
        for (int i = 0; i < 12; i++) {
            SacaBop temp = new SacaBop();
           long insoluto=100000*i;
           long interes_mas_capital=insoluto+(20000*i);
                oper = "OPERRRR";

    

                this.totalA = this.totalA + insoluto;

                temp.setCapital("$"+insoluto);
                long interes = interes_mas_capital - insoluto;
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                double porcentaje_condonacion = 0.7;
                double porcentaje_condonacion_Interes = 0.7;
                double capital_condonado = insoluto * porcentaje_condonacion;
                this.totalB = (long) (this.totalB + capital_condonado);
                double capitalarecibir = insoluto - capital_condonado;
                this.totalC = (long) (capitalarecibir + this.totalC);

                /// agregagos el monto a recibir a la clase condonacion
                this.CurrentCondonacion.setMontoARecibir(capitalarecibir);

                double interes_condonado = interes * porcentaje_condonacion_Interes;
                double interesarecibir = interes - interes_condonado;

                this.totalD = (long) (interes + this.totalD);
                this.totalE = (long) (interes_condonado + this.totalE);
                this.totalF = (long) (interesarecibir + this.totalF);

                // se agrega el monto a condonar a la clase sacabop
                temp.setMontoCondonar((long) capital_condonado);
                temp.setMontoCondonarPesos(nf.format(capital_condonado).replaceFirst("Ch", ""));
                temp.setCapitalARecibirPesos(nf.format(capitalarecibir).replaceFirst("Ch", ""));

                temp.setMontoARecibir((long) capitalarecibir);
                double porcentaje_honorjud = 0;

                if (this.CurrentCondonacion.isTieneJuicio()) {
                    porcentaje_honorjud = 0.15;

                } else {

                    porcentaje_honorjud = 0.50;

                }

                //  el calculo de los honorarios judiciales esta compuesto del CalculaMontoJudicial(monto a recibir)
                double honorariojud = this.CurrentCondonacion.getMontoARecibir() * porcentaje_honorjud;
                double honorarioJudCondonado = honorariojud * 0.70;
                double honorarioJudReibido = honorariojud - honorarioJudCondonado;

                //Calculo de Totales para la Grilla
                this.totalG = (long) (honorariojud + this.totalG);
                this.totalH = (long) (honorarioJudCondonado + this.totalH);
                this.totalI = (long) (honorarioJudReibido + this.totalI);

                // Grid-Column Judicial 
                temp.setHonorarioJuducial((long) honorariojud);
                //Montos en pesos
                temp.setHonorarioJudicialPesos(nf.format(honorariojud).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialCondonadoPesos(nf.format(honorarioJudCondonado).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialRecibidoPesos(nf.format(honorarioJudReibido).replaceFirst("Ch", ""));

                // fn-Saldo en pesos
                temp.setHonorarioJuducialCondonado(300000);
                temp.setHonorarioJudicial("$200.000");
                temp.setHonorarioJuduciaRecibir(1254155);
                //Fn- Grid-Column Judicial 

                temp.setInteresCondonado(100);
                temp.setInteresCondonadoPesos(nf.format(interes_condonado).replaceFirst("Ch", ""));
                temp.setInteresARecibirPesos(nf.format(interesarecibir).replaceFirst("Ch", ""));

                temp.setInteresARecibir(12);
                temp.setInteres(nf.format(interes).replaceFirst("Ch", ""));

                sacaboj.add(temp);


//            System.out.println("#------------@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        }

        Grid_SacabopXX.setModel(sacaboj);
        //  Grid_Sacabop.setModel(sacaboj);
      //  System.out.println("#------------@@@@@@@@@@@@@@@@@ ListDetOperModel.getSize()[" + ListDetOperModel.getSize() + "]Grid_SacabopXX [" + Grid_SacabopXX.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        // sacaboj
        //String[] parameter = (String[]) param.get("test");
        try {
            //String fechahoy;
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            //fechahoy = ((Date)format.parse(d2.toString())).toString();
            NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());

            /// Agregagamos valores totales a la clase condonacion
            CurrentCondonacion.setCapitalClass(this.totalA + this.totalD + this.totalG);
            CurrentCondonacion.setTotalCondonado(this.totalB + this.totalE + this.totalH);

            //totales de la Grilla
            id_TotalSumaCapital.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotalSumaCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotalSumaRecibeCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));

            id_TotalSumaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotalSumaCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            id_TotalSumaRecibeInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            id_TotalSumaHonorJud.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            id_TotalSumaHonorJudCond.setValue(nff.format(this.totalH).replaceFirst("Ch", ""));
            id_TotalSumaHonorJudRec.setValue(nff.format(this.totalI).replaceFirst("Ch", ""));

            // totoales de Sumatorias
            idTotalRecibe.setValue(nff.format(this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));
            id_TotalCondona.setValue(nff.format(this.totalB + this.totalE + this.totalH).replaceFirst("Ch", ""));
            //id_TotlaCapital.setValue(nff.format(this.totalA + this.totalD+this.totalG).replaceFirst("Ch", ""));
            id_TotlaCapital.setValue(CurrentCondonacion.TotoalCapital.getValorPesos());

            // totales Parciales en La Suma de Resultados Por Color GREEN
            id_TotoalParcialCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));
            id_TotoalParcialInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));
            id_TotoalParcialhonor.setValue(nff.format(this.totalI).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color RED
            id_TotoalParcialCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotoalParcialCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            id_TotoalParcialCondonaHono.setValue(nff.format(this.totalH).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color YELLOW
            id_TotoalParcialCapita.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaHonor.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // totales Parciales en La Suma de Resultados Por Color RED

// fin totales Grilla
            id_fechahoy.setValue(dateFormat.format(date));
            nro_condonacion.setValue(Integer.toString(cond.consulta_id()));
            valueneg.setValue("14212287-1");
            this.CurrentCondonacion.setNombreEjecutiva(Nombre);
            viewrutcliente.setValue("14212287-1");
            this.CurrentCondonacion.setRutCliente("14212287-1");
            id_nomcliente.setValue("dfsdfdsf");
            this.CurrentCondonacion.setNombreCliente("dfsdfdsf");
            id_oficinaorigen.setValue("dfsdfdsf");
            id_saldototalmoroso.setValue("dfsdfdsf");

            // llenado de informacuion de valores Utilizados en condonacion       
            id_PrimerMesDeCondonacion.setValue(this.CurrentCondonacion.getFechaPrimerCatigo());
            id_MesMasAntiguoCastigo.setValue(Integer.toString(this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
            id_AtribucionEjecutiva.setValue(this.CurrentCondonacion.MontoAtribucionMaxima.getValorPesos());
            id_porcentajeCondoEjecutiva.setValue("C:" + Double.toString(this.CurrentCondonacion.getPorcentajeCondonaCapital()) + "I:" + Double.toString(this.CurrentCondonacion.getPorcentajeCondonaInteres()) + "H:" + Double.toString(this.CurrentCondonacion.getPorcentajeCondonaHonorario()));
            id_PuedeCondonarOnline.setValue(Boolean.toString(this.CurrentCondonacion.isPuedeCondonarEnLinea()));
            id_rangoFechaCondonacion.setValue("FI:" + this.CurrentCondonacion.getRangoFechaInicio() + " FF:" + this.CurrentCondonacion.getRangoFehaFin());
            // btn_enviaSacaBopg.setDisabled(true);
            //*btn_enviaSacaBopg.
            if (this.CurrentCondonacion.TotoalCondonado.getValor() < this.CurrentCondonacion.MontoAtribucionMaxima.getValor()) {
                //btn_GenrarCondonacion.setVisible(true);
                id_windowsMessajje.setClass("alert alert-success");
                // id_msgeCona.setValue("Ejecutivo " +user.getFullName()+ " Cumple Los Requisitos para Condonar Online");
                // id_msgeCona.setValue("asdasdas");
                Label temp;
                temp = (Label) id_windowsMessajje.getFellow("id_msgeCona");
                temp.setValue("Ejecutivo " + Nombre + " Cumple Los Requisitos para Condonar Online");
            } else {
                // btn_enviaSacaBopg.setVisible(true);
                id_windowsMessajje.setClass("alert alert-danger");
                // id_msgeCona.setValue("asdasdas2");
                //  id_msgeCona.setValue("Ejecutivo " +user.getFullName()+ " NO Cumple Los Requisitos para Condonar Online");

            }

            String jjj = Executions.getCurrent().getAttributes().toString();
            //panelgridddd=Executions.getCurrent().getAttributes()
            System.out.println("#------------@@@@@@@@@@@@@@@@@Tratando de accesar other panels" + jjj + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        } catch (WrongValueException e) {
            System.out.println("EXCEPTION: " + e);

        }

        // negoc.setValue("sadklasldas");
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        System.out.println("doBeforeCompose executed");
        // negoc.setValue("sadklasldas");

        return super.doBeforeCompose(page, parent, compInfo);

    }

    public AnReglaController() throws SQLException {
        this.MaximoMesCastigo = 0;
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceLucy());
        _cliente = new ClienteSisconJDBC(mmmm.getDataSource());
        _condonadorJDBC=new CondonadorJDBC(mmmm.getDataSource());
        //  this.contactDAO = mmmm.getContactoDAO();
        //this.informe=new InformesImpl(mmmm.getDataSourceLucy());
    }

  
 
    @Listen("onClick=#btn_cerrar")
    public void Salir() {
        id_winSacabobAnalista.detach();
    }

}
