/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.ConDetOper;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author exesilr
 */
public class AnCerradasController extends SelectorComposer<Component> {

    @Wire
    Grid grd_GridInfor;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<CondonacionTabla2> lCondFilter;

//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<CondonacionTabla2> lCondFinal;
    ////////////////////////////////////////////////////////
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla2> bandeCondTerminada;
    Condonacion CurrentCondonacion;
    private List<CondonacionTabla2> listCondonaciones;
    Window window;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    int idCondonacion;
    Session sess;
    UsuarioPermiso permisos;
    String cuenta;
    String Nombre;
    MetodosGenerales MT;
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private static List<CondonacionTabla2> DataGridList = new ArrayList<CondonacionTabla2>();
    Session session;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();

    public AnCerradasController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());
        listCondonaciones = null;
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        session = Sessions.getCurrent();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();

        cargaPag();

    }

    private void cargaPag() {

        try {
            sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
            sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

            eventos();

            lCondFinal = new ArrayList<CondonacionTabla2>();
            lCondFilter = new ArrayList<CondonacionTabla2>();
            lCondFinal = this.cond.GetCondonacionesCerradasAproAnalista(cuenta);
            lCondFilter = lCondFinal;

            cargaGrid(lCondFilter);
            muestraCol();
        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones cerradas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<CondonacionTabla2> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

        grd_GridInfor.setModel(bandeCondTerminada);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_GridInfor.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

        //        dbx_desde.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dDesdeLocal = dbx_desde.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
        //        dbx_hasta.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dHastaLocal = dbx_hasta.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
    }

    private List<CondonacionTabla2> filterGrid() {
        List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lCondFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lCondFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (CondonacionTabla2 cT2 : lCondFinal) {
                        String cadena = cT2.getId_condonacion() + ";"
                                + cT2.getTimestap() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getUsuariocondona() + ";"
                                + cT2.getRegla() + ";"
                                + cT2.getTipocondonacion() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getDi_num_opers() + ";"
                                + cT2.getComentario_resna() + ";"
                                + cT2.getMonto_total_capitalS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_condonadoS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_recibitS().replace(".", "").replace(",", "") + ";"
                                + cT2.getRutcCliente();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

//        if (dDesdeLocal != dDesdeLocalResp) {
//            if (dDesdeLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.after(dDesdeLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
//
//        if (dHastaLocal != dHastaLocalResp) {
//            if (dHastaLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.before(dHastaLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }

        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }

            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        String template = "Analista/Poput/AnSacabopMostrar.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
