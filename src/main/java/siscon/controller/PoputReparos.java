/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Grid;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.GlosaENC;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.AdjuntarAPI;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.AdjuntarInterfaz;
import siscon.entidades.interfaces.ClienteInterfaz;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class PoputReparos extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;
    @Wire
    Grid id_gridReparos;
    Window capturawin;
    Window window;
    @Wire
    Window id_poput_reparo;
    Div pag_Inforx;
    @Wire
    private Window idWinGarantiasPoput;
    @Wire
    Label lbl_idCondonacion;
    @Wire
    Button tn_ejeReparadaReparar;
    @Wire
    Button btn_closeButton2;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    private AdjuntarENC adjunta;
    private String modulo = "EjePopUpReparos";//cosorio
    private String modulo2 = "AnSacabopView";
    private GlosaENC glosa; //cosorio
    private EventQueue eq;//cosorio
    private float sizeFiles = 0;
    private int id_cond = 0;
    private int rutCli = 0;
    private String rutCliEntero = null;
    private int id_Cli = 0;
    private ClienteInterfaz buscaIdCliInter;
    private final AdjuntarInterfaz ai;
    final CondonacionInterfaz cond;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    @Wire
    public String TTTT = "EJESICON";
    MvcConfig mmmm = new MvcConfig();
    ListModelList<Reparos> ListReparosCondonacionModel;

    public String getAreaTrabajo() {
        return AreaTrabajo;
    }

    public void setAreaTrabajo(String AreaTrabajo) {
        this.AreaTrabajo = AreaTrabajo;
    }
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        this.capturawin = (Window) comp;
        //this.window = this.capturawin;
      //  pag_Inforx = (Div) capturawin.getParent();
        try {
            buscaIdCliInter = new ClienteInterfazImpl();
            //Condonasound code
            //glosa = new GlosaENC();//cosorio incia variable limpia
            adjunta = new AdjuntarENC();
            //Herencia entre Sacabop e hijos
            {//Bloque anonimo creado por cosorio

                //Herencia ente Glosa y Reparos
//                eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
//                eq.subscribe(new EventListener() {
//
//                    @Override
//                    @SuppressWarnings("unused")
//                    public void onEvent(Event event) throws Exception {
//                        final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
//                        List<GlosaDET> glosaDet = new ArrayList<GlosaDET>();
//                        glosaDet = (List<GlosaDET>) map.get("GlosaSS");
//
//                        if (glosaDet != null) {
//                            if (!glosaDet.isEmpty()) {
//                                glosa.setCant_glosas(glosaDet.size());
//
//                            }
//                            glosa.setDetalle(glosaDet);
//                        }
//                    }
//                });
                //Herencia ente Adjuntar y Reparos
                eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, true);
                eq.subscribe(new EventListener() {

                    @Override
                    @SuppressWarnings("unused")
                    public void onEvent(Event event) throws Exception {
                        final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                        List<AdjuntarDET> det = new ArrayList<AdjuntarDET>();
                        det = (List<AdjuntarDET>) map.get("ListAdjuntoSS");
                        sizeFiles = (Float) map.get("tama�o");

                        if (det != null) {
                            if (!det.isEmpty()) {
                                adjunta.setNumero_archivos(det.size());

                                adjunta.setTama�o_total(String.format("%.4f", sizeFiles) + "Mb");
                            }

                        }
                        adjunta.setaDet(det);

                        if (adjunta.getaDet() != null) {
                            if (!adjunta.getaDet().isEmpty()) {
                                //el retorno cuando la accion es 1 tiene que ser null
                                if (ai.api(adjunta, 1) == null) {
                                    String mssje = "Error Guardando la informaci�n (Archivos) del Detalle Condonaci�n.";
                                    Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                                };
                            };
                        };

                    }
                });
            };

            cargaPag();

        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            id_poput_reparo.detach();
        }

    }

    public PoputReparos() throws SQLException {
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.AreaTrabajo = "ANSISCON";
        ai = new AdjuntarAPI();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
    }

    @Listen("onClick=#btn_ejeReparadaReparar")
    public void RepararCondonacion() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", lbl_idCondonacion.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);
        String template = "Analista/Poput/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, capturawin, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                // UpdateGridDoc();
                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto _idReparoDoc");
                // UpdateGridDoc();
                window.detach();

            }
        });
        // printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void eventos() {
        id_poput_reparo.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("ReparoPopUp", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", id_poput_reparo, map));

                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
            }

        });

        btn_closeButton2.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("ReparoPopUp", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_closeButton2, map));
                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
                id_poput_reparo.detach();
            }
        });
    }

    public void cargaPag() {
        final Execution exec = Executions.getCurrent();

        if (exec.getArg().get("ListAdjuntoSS") != null) {
            adjunta.setaDet((List<AdjuntarDET>) exec.getArg().get("ListAdjuntoSS"));
        }

        if (exec.getArg().get("id_condonacion") != null) {
            id_cond = (Integer) exec.getArg().get("id_condonacion");
        }
        if (exec.getArg().get("rutclienteInt") != null) {
            rutCli = (Integer) exec.getArg().get("rutclienteInt");
        }
        if (exec.getArg().get("rutcliente") != null) {
            rutCliEntero = (String) exec.getArg().get("rutcliente");
        }

        //buscar id cliente en tabla de condonaciones por la id de condonaci�n.
        id_Cli = buscaIdCliInter.GetClienteCondonado(id_cond);

        adjunta.setFk_idCliente(id_Cli);
        adjunta.setFk_idCondonacion(id_cond);
        ///////////////////////////////////////////////////////////////////////

        eventos();

    }

    public void Adjuntar(Object[] aaa) {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        int id_condResp = 0;
        int id_CliResp = 0;
        String[][] coupleArray = new String[aaa.length][];
        AdjuntarENC adjLocal = new AdjuntarENC();

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        int id_reparo = Integer.parseInt(coupleArray[0][1]);
        id_condResp = adjunta.getFk_idCondonacion();
        id_CliResp = adjunta.getFk_idCliente();

        if (adjunta.getDi_id_reparo() != id_reparo) {
            if (adjunta.getDi_id_reparo() != 0) {
                adjunta = new AdjuntarENC();
            }
        }

        adjunta.setFk_idCliente(id_CliResp);
        adjunta.setFk_idCondonacion(id_condResp);
        adjunta.setDi_id_reparo(id_reparo);

        adjLocal = ai.api(adjunta, 2);

        if (adjLocal != null) {
            adjunta = adjLocal;
        }

        map.put("rutClienteAdjunto", rutCliEntero);
        map.put("ListAdjuntoSS", adjunta.getaDet());
        map.put("modulo", modulo);

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/AdjuntarPoput.zul", null, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void Adjuntar2(Object[] aaa) {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        int id_condResp = 0;
        int id_CliResp = 0;
        String[][] coupleArray = new String[aaa.length][];
        AdjuntarENC adjLocal = new AdjuntarENC();

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        int id_reparo = Integer.parseInt(coupleArray[0][1]);
        id_condResp = adjunta.getFk_idCondonacion();
        id_CliResp = adjunta.getFk_idCliente();

        if (adjunta.getDi_id_reparo() != id_reparo) {
            if (adjunta.getDi_id_reparo() != 0) {
                adjunta = new AdjuntarENC();
            }
        }

        adjunta.setFk_idCliente(id_CliResp);
        adjunta.setFk_idCondonacion(id_condResp);
        adjunta.setDi_id_reparo(id_reparo);

        adjLocal = ai.api(adjunta, 2);

        if (adjLocal != null) {
            adjunta = adjLocal;
        }

        map.put("rutClienteAdjunto", rutCliEntero);
        map.put("ListAdjuntoSS", adjunta.getaDet());
        map.put("modulo", modulo2);

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/AdjuntarPoput.zul", this.capturawin, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void UpdateGridReparos() {

        // _conListDOc = new MvcConfig().documentosJDBC();
        //this._listDOc = _conListDOc.listDocumentoLotePorBarcode(this._lot.lot.getDv_CodBarra()); //id_operacion
        //    _listModelDOc = new ListModelList<Documento>(_listDOc);
        //   grid_Dctos.setModel(_listModelDOc);
        //  Messagebox.show("Updated");
        //   grid_Dctos.setVisible(false);
        //    grid_Dctos.setVisible(true);
        // id_scanCode.setVisible(false);
        //   _divFridDoc.setVisible(false);
        //    _divFridDoc.setVisible(true);
    }

    @Listen("onClick=#btn_refresh")
    public void refreschGrid() {

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(Integer.parseInt(lbl_idCondonacion.getValue())));
        id_gridReparos.setModel(ListReparosCondonacionModel);
        id_gridReparos.setVisible(false);
        id_gridReparos.setVisible(true);

    }
}
