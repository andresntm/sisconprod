/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.function.Consumer;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Spinner;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.TipoCondEspecial;
import siscon.entidades.TipoProvision;
import siscon.entidades.Tipo_Prorroga;
import siscon.entidades.UsrProvision;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.ProvisionJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author excosoc
 */
public class EjeCondEspecialController extends SelectorComposer<Window> {

    @Wire
    Checkbox chk_CondEspecial;
    @Wire
    Combobox cmb_TipoCondEspecial;
    @Wire
    Intbox txt_SaldoCond;
    @Wire
    Spinner txt_NumCuotas;
    @Wire
    Div div_NumCuotas, div_Saldo;
    @Wire
    Button btn_IngTipoCondEspecial, btn_close;
    @Wire
    Window win_GesPror;

    private List<TipoCondEspecial> lTCE;
    private TipoCondEspecial tCE;
    private TipoCondEspecial tCESeleccionado = null;
    private ListModelList<TipoCondEspecial> lMLTCE;
    private TipoProvision tProvision = null;
    private UsrProvision uProv;
    private List<Comboitem> lItemFull;
    private TipoCondEspecial cItemSelected = new TipoCondEspecial();
    private CondonacionesEspeciales cE = null;
    private CondonacionesEspeciales cESeleccionada = null;
    private CondonacionesEspeciales cEYaIngresada = null;
    private UsuarioPermiso permisos;
    private Session sess;
    private ProvisionJDBC _prov;
    private CondonacionInterfaz cond;
    private MvcConfig beans;
    private String rutCliente;
    private Object padre;
    private EventQueue eq;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

//        if (comp instanceof Window) {
        win_GesPror = (Window) comp;
//        }

        cargaPag();
    }

    private void cargaPag() {
        try {
            beans = new MvcConfig();
            cond = new CondonacionImpl(beans.getDataSource());
        } catch (SQLException ex) {
            Logger.getLogger(EjeCondEspecialController.class.getName()).log(Level.SEVERE, null, ex);
        }

        final Execution exec = Executions.getCurrent();
        tCE = new TipoCondEspecial();
        cE = new CondonacionesEspeciales();
        sess = Sessions.getCurrent();

        chk_CondEspecial.setChecked(false);
        cmb_TipoCondEspecial.setDisabled(false);
        div_Saldo.setVisible(false);
        div_NumCuotas.setVisible(false);

        if (exec.getArg().get("rutCliente") != null) {
            rutCliente = (String) exec.getArg().get("rutCliente");
        }
        if (exec.getArg().get("condEspecial") != null) {
            cEYaIngresada = (CondonacionesEspeciales) exec.getArg().get("condEspecial");
        }
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cargaListTipCondEspeciales();

        if (cEYaIngresada != null) {
            cEYaIngresada = (CondonacionesEspeciales) exec.getArg().get("condEspecial");

            if (cEYaIngresada.getId_TipoEspecial() != 0) {

                chk_CondEspecial.setChecked(true);

                if (tCESeleccionado.getCodigo().equals("CONDPORCUOTAS")) {
                    div_NumCuotas.setVisible(true);
                    txt_NumCuotas.setValue(cEYaIngresada.getNumCuotas());
                } else if (tCESeleccionado.getCodigo().equals("CONDPORSALDO")) {
                    div_Saldo.setVisible(true);
                    uProv = new UsrProvision();
                    uProv = uProv.getProvision_X_Id(cEYaIngresada.getId_Provision());
                    txt_SaldoCond.setText(Math.round(uProv.getMonto()) + "");
                    tProvision = new TipoProvision();
                    tProvision = tProvision.getTipoProv_X_Codigo("SALDO");
                }

//                cmb_TipoCondEspecial.setSelectedItem(cItemSelected);
            }

        }

        cargaEventos();
    }

    private void cargaEventos() {

        chk_CondEspecial.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                if (chk_CondEspecial.isChecked()) {
                    cmb_TipoCondEspecial.setDisabled(false);

                } else {
                    cmb_TipoCondEspecial.setDisabled(true);
                    tCESeleccionado = null;
                    cEYaIngresada = null;
                    cESeleccionada = null;

                }
            }

        });

        cmb_TipoCondEspecial.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                TipoCondEspecial tCESelect = (TipoCondEspecial) cmb_TipoCondEspecial.getSelectedItem().getValue();

                div_NumCuotas.setVisible(false);
                div_Saldo.setVisible(false);

                if (tCESelect.getCodigo().equals("CONDPORCUOTAS")) {
                    div_NumCuotas.setVisible(true);
                } else if (tCESelect.getCodigo().equals("CONDPORSALDO")) {
                    div_Saldo.setVisible(true);
                    tProvision = new TipoProvision();
                    tProvision = tProvision.getTipoProv_X_Codigo("SALDO");
                } else {
                    div_NumCuotas.setVisible(false);
                    div_Saldo.setVisible(false);
                }

                tCESeleccionado = tCESelect;

                if (tCESeleccionado != null) {
                    if ((!tCESelect.getCodigo().equals("CONDPORSALDO"))) {
                        if (cEYaIngresada != null) {
                            if (cEYaIngresada.getId_Provision() > 0) {
                                eliminaProv(cEYaIngresada.getId_Provision());
                                cEYaIngresada.deleteLogicoCondEsp(cEYaIngresada.getId_Cond_Esp());
                            }
                        }
                    }
                }
            }

        });

        btn_close.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                CondonacionesEspeciales cERetiro = null;

                if (cEYaIngresada != null) {
                    cERetiro = cEYaIngresada;
                }
//                }else if (cESeleccionada  != null){
//                    cERetiro = cESeleccionada;
//                }

                map.put("condEspecialMarcada", cERetiro);
                eq = EventQueues.lookup("CondEspecial", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_close, map));
                win_GesPror.detach();
            }

        });

        btn_IngTipoCondEspecial.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                MvcConfig mC = new MvcConfig();
                cond = new CondonacionImpl(mC.getDataSource());

                if (validaForm()) {
                    cESeleccionada = null;
                    if (tCESeleccionado != null) {
                        if (tCESeleccionado.getId_CondEspecial() > 0) {
                            cESeleccionada = new CondonacionesEspeciales();
                            cESeleccionada.setId_TipoEspecial(tCESeleccionado.getId_CondEspecial());

                            if (tCESeleccionado.getCodigo().equals("CONDPORCUOTAS")) {
                                cESeleccionada.setNumCuotas(txt_NumCuotas.getValue());
                            } else if (tCESeleccionado.getCodigo().equals("CONDPORSALDO")) {
                                uProv = new UsrProvision();

                                if (cEYaIngresada != null) {
                                    if (cEYaIngresada.getId_Provision() == 0) {
                                        uProv.setId(cond.insertUsrProvision(((AreaTrabajo) permisos.getArea()).getId_colaborador(),
                                                0, Integer.valueOf(txt_SaldoCond.getText().replace(".", "")), "SALDO", rutCliente, tProvision.getDi_id()));
                                        if (uProv.getId() > 0) {
                                            cESeleccionada.setId_Provision(uProv.getId());
                                        }
                                    } else {
                                        cESeleccionada = cEYaIngresada;
                                    }
                                } else {
                                    uProv.setId(cond.insertUsrProvision(((AreaTrabajo) permisos.getArea()).getId_colaborador(),
                                            0, Integer.valueOf(txt_SaldoCond.getText().replace(".", "")), "SALDO", rutCliente, tProvision.getDi_id()));
                                    if (uProv.getId() > 0) {
                                        cESeleccionada.setId_Provision(uProv.getId());
                                    }
                                }
                                cESeleccionada.setActivo(true);
                                Include inc = (Include) win_GesPror.getParent().getParent().getFellow("pageref");
                                inc.setSrc(null);
                                Sessions.getCurrent().setAttribute("MarcaCondEspecial", cESeleccionada);
                                Sessions.getCurrent().setAttribute("rutcliente", rutCliente);
                                inc.setSrc("Ejecutivo/Poput/RetailSacabop.zul");
                                win_GesPror.detach();
                                return;
                            }
                        }
                    }
                    final HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("condEspecialMarcada", cESeleccionada);
                    eq = EventQueues.lookup("CondEspecial", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onButtonClick", btn_IngTipoCondEspecial, map));
                    win_GesPror.detach();

                }

            }

        });

    }

    private boolean validaForm() {
        boolean ok = false;

        try {
            if (chk_CondEspecial.isChecked()) {

                if (tCESeleccionado != null) {
                    if (tCESeleccionado.getId_CondEspecial() != 0) {

                        if (tCESeleccionado.getCodigo().equals("CONDPORCUOTAS")) {
                            if (txt_NumCuotas.getValue() == 0) {
                                Messagebox.show("Estimado(a) colaborador(a), para grabar marcar como condonación por cuotas debe ingresar el numero de cuotas.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                                ok = false;
                                txt_NumCuotas.setFocus(true);
                            } else if (txt_NumCuotas.getValue() > 12) {
                                Messagebox.show("Estimado(a) colaborador(a), para grabar marcar como condonación por cuotas el N° cuotas no pueden ser mayores a 12 cuotas.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                                ok = false;
                                txt_NumCuotas.setFocus(true);
                            } else if (txt_NumCuotas.getValue() < 1) {
                                Messagebox.show("Estimado(a) colaborador(a), para grabar marcar como condonación por cuotas el N° cuotas no pueden ser menores a 1 cuotas.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                                ok = false;
                                txt_NumCuotas.setFocus(true);
                            } else {
                                ok = true;
                            }

                        } else if (tCESeleccionado.getCodigo().equals("CONDPORSALDO")) {
                            if (txt_SaldoCond.getText() == null) {
                                Messagebox.show("Estimado(a) colaborador(a), para grabar marcar como condonación por cuotas debe ingresar el numero de cuotas.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                                ok = false;
                                txt_SaldoCond.setFocus(true);
                            } else if (txt_SaldoCond.getText().equals("")) {
                                Messagebox.show("Estimado(a) colaborador(a), para grabar marcar como condonación por cuotas debe ingresar el numero de cuotas.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                                ok = false;
                                txt_SaldoCond.setFocus(true);
                            } else {
                                ok = true;
                            }
                        } else {
                            ok = true;
                        }

                    } else {
                        Messagebox.show("Estimado(a) colaborador(a), para grabar una condonación especial debe seleccionar un tipo de condonación especial.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                        ok = false;
                        cmb_TipoCondEspecial.setFocus(true);
                    }
                } else {
                    Messagebox.show("Estimado(a) colaborador(a), para grabar una condonación especial debe seleccionar un tipo de condonación especial.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                    ok = false;
                    cmb_TipoCondEspecial.setFocus(true);
                }

            } else {
//                Messagebox.show("Estimado(a) colaborador(a), para grabar una condonación especial debe marcar esta misma como condonación especial.", "SisCon-Admin", Messagebox.OK, Messagebox.INFORMATION);
                ok = true;
                chk_CondEspecial.setFocus(true);
            }
        } catch (Exception ex) {

            ok = false;
        } finally {
            return ok;
        }
    }

    private void cargaListTipCondEspeciales() {
        lTCE = new ArrayList<TipoCondEspecial>();
        lTCE = tCE.listTipoCondEspecial();
        lMLTCE = new ListModelList<TipoCondEspecial>(lTCE);
        lItemFull = new ArrayList<Comboitem>();
        int recorreTP = 0;

        if (cEYaIngresada != null) {
            for (TipoCondEspecial tPLML : lMLTCE) {
                if (tPLML.getId_CondEspecial() == cEYaIngresada.getId_TipoEspecial()) {
                    lMLTCE.addSelection(lMLTCE.get(recorreTP));
                    tCESeleccionado = lMLTCE.get(recorreTP);
                    break;
                }

                recorreTP += 1;
            }
        }

        cmb_TipoCondEspecial.setReadonly(true);
        cmb_TipoCondEspecial.setAutodrop(true);
        cmb_TipoCondEspecial.setWidth("100%");
        cmb_TipoCondEspecial.setClass("input-group-block");
        cmb_TipoCondEspecial.setPlaceholder("Seleccione un tipo de condonación.");
        cmb_TipoCondEspecial.setStyle("color : black !important; font-weight : bold");

        cmb_TipoCondEspecial.setModel(lMLTCE);

        cmb_TipoCondEspecial.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String descripcion;
                String detalle;
                String detalleSalida = "";
                int recorreDetalle = 30;
                TipoCondEspecial tCERender = (TipoCondEspecial) data;

                descripcion = tCERender.getDescripcion();
                detalle = tCERender.getDetalle();
                detalleSalida = detalle;

                if ((detalle.length() / 30) > 1) {
                    detalleSalida = "";
                    while (recorreDetalle <= detalle.length()) {

                        if ((recorreDetalle + 30) > detalle.length()) {
                            detalleSalida += detalle.substring(recorreDetalle, detalle.length()) + "";

                        } else {
                            detalleSalida += detalle.substring(recorreDetalle - 30, recorreDetalle) + "";
                        }
                        recorreDetalle += 30;
                    }
                }

                item.setLabel(descripcion);
                item.setDescription(detalleSalida);
                item.setValue(data);
                item.setWidth(cmb_TipoCondEspecial.getWidth() + 40);
                lItemFull.add(item);

            }

        });

    }

    public void eliminaProv(int id_prov) {
        try {
            MvcConfig mC = new MvcConfig();
            _prov = new ProvisionJDBC(mC.getDataSource());
        } catch (SQLException ex) {
            Logger.getLogger(EjeCondEspecialController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (this._prov.EliminaProvision(id_prov)) {
            Messagebox.show("Estimado(a) colaborador(a), se ha eliminado la provisión N°:[" + id_prov + "] correctamente.");

        } else {
            Messagebox.show("Estimado(a) colaborador(a), no se ha podido eliminado la provisión N°:[" + id_prov + "]");
        }

    }

}
