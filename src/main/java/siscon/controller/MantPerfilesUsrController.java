/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.GlosaENC;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.ClienteInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;

/**
 *
 * @author exesilr
 */
public class MantPerfilesUsrController extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;

    Window capturawin;
    Window window;
    @Wire
    Window id_poput_reparo;
    @Wire
    private Window idWinGarantiasPoput;
    @Wire
    Label lbl_idCodPerfil;
    @Wire
    Button tn_ejeReparadaReparar;
    @Wire
    Button btn_closeButton2;
        @Wire
    Grid BanEntrAnalista;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    private AdjuntarENC adjunta;
    private String modulo = "EjePopUpReparos";//cosorio
    private String modulo2 = "AnSacabopView";
    private GlosaENC glosa; //cosorio
    private EventQueue eq;//cosorio
    private float sizeFiles = 0;
    private int id_cond = 0;
    private int rutCli = 0;
    private String rutCliEntero = null;
    private int id_Cli = 0;
    private ClienteInterfaz buscaIdCliInter;
    private List<usuario> _usr;
    List<usuario> _usuarios = new ArrayList<usuario>();
    UsuarioDAO _usu;
     ListModelList<usuario> bandeusuarios;
    //////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////
    @Wire
    public String TTTT = "EJESICON";
    MvcConfig mmmm = new MvcConfig();
    ListModelList<Reparos> ListReparosCondonacionModel;

    @Wire
    Grid id_gridReparos;

    public String getAreaTrabajo() {
        return AreaTrabajo;
    }

    public void setAreaTrabajo(String AreaTrabajo) {
        this.AreaTrabajo = AreaTrabajo;
    }
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        this.capturawin = (Window) comp;
        //this.window = this.capturawin;
      //  _usuarios = this._usu.UsuariosPorPerfil(lbl_idCodPerfil.getValue());
    //    bandeusuarios = new ListModelList<usuario>(_usuarios);

//        BanEntrAnalista.setModel(bandeusuarios);
  //      BanEntrAnalista.renderAll();


    }

    public MantPerfilesUsrController() throws SQLException {

        this.AreaTrabajo = "ADMINSISCON";

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        
        
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
    }

    @Listen("onClick=#btn_ejeReparadaReparar") 
    public void RepararCondonacion() {
        // window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("codPefil", lbl_idCodPerfil.getValue());
        //Messagebox.show("ReparoCondonacionEjecutivo["+lbl_idCondonacion.getValue()+"*");

        arguments.put("rut", 15.014544);
        String template = "Mantenedores/Poput/UsrAddPerfilPoput.zul";
        window = (Window) Executions.createComponents(template, this.capturawin, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto Close Button");
                // UpdateGridDoc();
                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto _idReparoDoc");
                // UpdateGridDoc();
                window.detach();

            }
        });
        // printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

 


    public void UpdateGridReparos() {

        // _conListDOc = new MvcConfig().documentosJDBC();
        //this._listDOc = _conListDOc.listDocumentoLotePorBarcode(this._lot.lot.getDv_CodBarra()); //id_operacion
        //    _listModelDOc = new ListModelList<Documento>(_listDOc);
        //   grid_Dctos.setModel(_listModelDOc);
        //  Messagebox.show("Updated");
        //   grid_Dctos.setVisible(false);
        //    grid_Dctos.setVisible(true);
        // id_scanCode.setVisible(false);
        //   _divFridDoc.setVisible(false);
        //    _divFridDoc.setVisible(true);
    }

    @Listen("onClick=#btn_refresh")
    public void refreschReparo() {

        // Messagebox.show("PRUEBA-REFRECH");
   //     ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(Integer.parseInt(lbl_idCodPerfil.getValue())));
     //   id_gridReparos.setModel(ListReparosCondonacionModel);
       // id_gridReparos.setVisible(false);
       // id_gridReparos.setVisible(true);

    }

    
    
     //   @Listen("onClick=#_idReparoDoc")
    public void Eliminar(Object[] aaa) {
        //id_winSacabobAnalista.detach();
        
 
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            //strings[i]=((String)aaa[i]).split("=");
            coupleArray[i] = ss.split("=");
            // Messagebox.show("KSKSKS["+Arrays.toString(coupleArray[i])+"]");

        }
         
        String aliass = (coupleArray[0][1]);
        String rut = (coupleArray[1][1]);
        
        boolean resultado;
        //Messagebox.show("Reparando...Condonación Numero :[" + id_condonacion.getValue() + "]");

        if (this._usu.BorrarUsuarioEnPerfilds(aliass,rut,lbl_idCodPerfil.getValue())) {
           // this.cond.SetCondonacionesReparadaAnalista(Integer.parseInt(id_condonacion.getValue()));
            //resultado = this.cond.SetCambiaEstadoCondonaciones("Analista.Recepcion", "Ejecutivo.Reparadas", cuenta, "EjecutivoDestino", Integer.parseInt(id_condonacion.getValue()), "Estado.Recepcion", "Estado.Reparada.Analista");

            Messagebox.show("Se Elimino la relacion:["+aliass+"-->"+lbl_idCodPerfil.getValue()+"]");

            //Messagebox.show("Prueba fellow2222{" + capturawin.getParent().getFellows().toString() + "}");
            //Include inc = (Include) capturawin.getParent().getParent().getParent().getFellow("pageref");
            // inc.setSrc(null);
            //Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
            //inc.setSrc("Ejecutivo/EjeReparadas.zul");
            //Events.sendEvent(new Event("onClick", (Button) ((Window) capturawin.getParent()).getFellow("btn_refresh")));
            this.capturawin.detach();
            // ligarWin.detach();

        } else {
            Messagebox.show("Error Eliminando Relacion... con Error]");
        }

    }

    
    
    
    
    
    
    
}
