/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.util.List;
import java.util.ArrayList;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Window;
import siscon.entidades.InformeCond_Ap_Re_Pe;

/**
 *
 * @author excosoc
 */
public class InformeCond_Ap_Re_PeController extends SelectorComposer<Window> {

    @Wire
    private Grid grd_InformeCond;
    @Wire
    private Radiogroup rdg_tip1;
    @Wire
    private Radio rdb_retail;
    @Wire
    private Radio rdb_pyme;

    @Wire
    private Column clm_0;
    @Wire
    private Column clm_1;
    @Wire
    private Column clm_2;
    @Wire
    private Column clm_3;
    @Wire
    private Column clm_4;
    @Wire
    private Column clm_5;
    @Wire
    private Column clm_6;
    @Wire
    private Column clm_7;
    @Wire
    private Column clm_8;
    @Wire
    private Column clm_9;

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private List<InformeCond_Ap_Re_Pe> lInforme;
    private ListModelList<InformeCond_Ap_Re_Pe> lMLInforme;
    private InformeCond_Ap_Re_Pe informe;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        cargaPag();
    }

    public void cargaPag() {

        rdg_tip1.setSelectedItem(rdb_pyme);

        lInforme = new ArrayList<InformeCond_Ap_Re_Pe>();
        informe = new InformeCond_Ap_Re_Pe();
        lInforme = informe.list_Informe_Pyme();

        refresh_grid(lInforme);
        eventos();
    }

    public void refresh_grid(List<InformeCond_Ap_Re_Pe> lICARP) {

        lMLInforme = new ListModelList<InformeCond_Ap_Re_Pe>(lICARP);
        grd_InformeCond.setModel(lMLInforme);
    }

    public void eventos() {
        rdg_tip1.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Radio rG = (Radio) event.getTarget();
                String id_radio = rG.getId();

                if (id_radio.equals("rdb_pyme")) {
                    clm_3.setVisible(true);
                    clm_4.setVisible(true);
                    lInforme = informe.list_Informe_Pyme();
                    refresh_grid(lInforme);
                } else if (id_radio.equals("rdb_retail")) {
                    clm_3.setVisible(false);
                    clm_4.setVisible(false);
                    lInforme = informe.list_Informe_Retail();
                    refresh_grid(lInforme);
                }

            }

        });

    }

}
