/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.SistemasImpl;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.SistemaInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.entidades.Sistema;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author exesilr
 */
public class MantSistemasController extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    final SistemaInterfaz _sis;
    ListModelList<Sistema> bandeCondTerminada;
    Window window;
    Window windows;
    MetodosGenerales MT;
    // final CondonacionInterfaz cond;
    private List<Sistema> listSistemas;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private MetodosGenerales metodo;
    NumberFormat nf;
    Session session;
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;
    String modulo;
    ListModelList<Reparos> ListReparosCondonacionModel;
     ListModelList<usuario> _UsrModelList;
    @Wire
    Button id_refresch;
    int idCondonacion;
    @Wire
    Window capturawin;
   UsuarioDAO _usu;
    
   List<usuario> _usuarios = new ArrayList<usuario>();
    public MantSistemasController() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this._sis = new SistemasImpl(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.modulo = "AnalistRecepcion";
         this._usu = new UsuarioJDBC(mmmm.getDataSource());
    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        session = Sessions.getCurrent();
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

        windows = comp;
        capturawin= comp;
        
        Locale.setDefault(new Locale("es", "CL"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
        
        
        
        listSistemas = this._sis.getSistmas();
        bandeCondTerminada = new ListModelList<Sistema>(listSistemas);

        BanEntrAnalista.setModel(bandeCondTerminada);
        BanEntrAnalista.renderAll();

    }



    public void UpdateGridDoc() {

        // _conListDOc = new MvcConfig().documentosJDBC();
        //this._listDOc = _conListDOc.listDocumentoLotePorBarcode(this._lot.lot.getDv_CodBarra()); //id_operacion
        //    _listModelDOc = new ListModelList<Documento>(_listDOc);
        //   grid_Dctos.setModel(_listModelDOc);
        //  Messagebox.show("Updated");
        //   grid_Dctos.setVisible(false);
        //    grid_Dctos.setVisible(true);
        // id_scanCode.setVisible(false);
        //   _divFridDoc.setVisible(false);
        //    _divFridDoc.setVisible(true);
    }

    public void MostrarUsuarios(Object[] aaa) {
        window = null;
        
        String template = "Mantenedores/Poput/UsuariosPerfilPoput.zul";
        int id_operacion_documento = 0;
        
        Map<String, Object> arguments = new HashMap<String, Object>();
        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        
        String CodPerfil = (coupleArray[0][1]);
        String DescPerfil = (coupleArray[1][1]);

        
        arguments.put("CodPerfil", CodPerfil);
        arguments.put("DescPerfil", DescPerfil);


        _UsrModelList =new ListModelList<usuario>( this._usu.UsuariosPorPerfil(coupleArray[0][1]));
       // String Operaciones = "";

       arguments.put("orderItems", _UsrModelList);
        //ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        //session.setAttribute("detClientCond", detClientCond);
       // session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        //session.setAttribute("rutEjecutivo", rutejecutivo);
        //session.setAttribute("idcondonacion", idCondonacion);
        
        window = (Window) Executions.createComponents(template, null, arguments);
        Button printButton = (Button) window.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    
    
    
        
       // @Listen("onClick=#btn_AddReg;")
    public void ProcesaResetearUsuarioPsw() {

        window = null;
        
        String template = "Mantenedores/Poput/AgregarSistema.zul";
        int id_operacion_documento = 0;
        
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("CodPerfil", "dd");
        arguments.put("DescPerfil", "gg");


        
        window = (Window) Executions.createComponents(template, null, arguments);
        Button printButton = (Button) window.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    
    
    
    @Listen("onClick = #btn_AddReg")
    public void onClick$btn_AddReg(Event event) {
      window = null;
        
        String template = "Mantenedores/Poput/AgregarSistemas.zul";
        int id_operacion_documento = 0;
        
        Map<String, Object> arguments = new HashMap<String, Object>();
   
        
       // arguments.put("CodPerfil", cmb_Perfil.getValue());
       // arguments.put("DescPerfil", "gg");

        
        window = (Window) Executions.createComponents(template, null, arguments);
        Button printButton = (Button) window.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    
    
}
