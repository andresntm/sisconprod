/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.io.ByteArrayOutputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.CondonacionesEjecutivos;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author excosoc
 */
public class InformeEjecutivosCondonaciones extends SelectorComposer<Window> {

    @Wire
    private Grid grd_InformeCond;
    @Wire
    private Grid grd_InformeCond2;
    @Wire
    private Radiogroup rdg_tip1;
    @Wire
    private Radio rdb_retail;
    @Wire
    private Radio rdb_pyme;
    ListModelList<CondonacionesEjecutivos> bandeCondTerminada;
    private List<CondonacionesEjecutivos> listCondonaciones;
    private List<CondonacionTabla2> listCondonaciones2;
    @Wire
    private Column clm_0;
    @Wire
    private Column clm_1;
    @Wire
    private Column clm_2;
    @Wire
    private Column clm_3;
    @Wire
    private Column clm_4;
    @Wire
    private Column clm_5;
    @Wire
    private Column clm_6;
    @Wire
    private Column clm_7;
    @Wire
    private Column clm_8;
    @Wire
    private Column clm_9;
    String modulo;
    @Wire
    Span btn_search;

    private List<CondonacionesEjecutivos> lCondFinal;
    private String sTextLocal;
    @Wire
    private String sTextLocalResp;
    @Wire
    Textbox txt_filtra;
    public String AreaTrabajo;
    MetodosGenerales MT;
    ListModelList<Reparos> ListReparosCondonacionModel;
    ListModelList<CondonacionesEjecutivos> ListOperacionesInformeModel;
    Session session;
    String cuenta;
    //Listbox prueba;
    UsuarioPermiso permisos;
    ListModelList<DetalleCliente> ListDetOperModel;
    Window window;
    int idCondonacion;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    final CondonacionInterfaz cond;
    DetalleOperacionesClientes detoper;
    Session sess;
    private List<CondonacionesEjecutivos> lCondFilter;

    UsuarioDAO _usu;
    List<CondonacionesEjecutivos> lRep = new ArrayList<CondonacionesEjecutivos>();
//lRep= lRep = new ArrayList<CondonacionesEjecutivos>();

    MvcConfig mmmm = new MvcConfig();
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private List<CondonacionesEjecutivos> lInforme;
    private ListModelList<CondonacionesEjecutivos> lMLInforme;
    private CondonacionesEjecutivos informe;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        cargaPag();

        txt_filtra.setText("");
        listCondonaciones = cond.getInfEjecutivoCondonacion();
        bandeCondTerminada = new ListModelList<CondonacionesEjecutivos>(listCondonaciones);

        lCondFinal = new ArrayList<CondonacionesEjecutivos>();
        lCondFinal = listCondonaciones;
        lCondFilter = lCondFinal;
        grd_InformeCond.setModel(bandeCondTerminada);
        grd_InformeCond2.setModel(bandeCondTerminada);
       // prueba.setModel(bandeCondTerminada);

        session = Sessions.getCurrent();
        sess = Sessions.getCurrent();
        //     lRep = cond.getInfClienV2();
        //muestra = new ListModelList<CondonacionesEjecutivos>(lRep);
        //  grillainformes.setModel(muestra);
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount(); 
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        eventos();

    }

    public void cargaPag() {

        rdg_tip1.setSelectedItem(rdb_pyme);

        lInforme = new ArrayList<CondonacionesEjecutivos>();
        informe = new CondonacionesEjecutivos();
        //  lInforme = informe.list_Informe_Pyme();

        refresh_grid(lInforme);
        // eventos();
    }

    public void refresh_grid(List<CondonacionesEjecutivos> lICARP) {

        lMLInforme = new ListModelList<CondonacionesEjecutivos>(lICARP);
        grd_InformeCond.setModel(lMLInforme);
    }

    /* public void eventos() {
        rdg_tip1.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Radio rG = (Radio) event.getTarget();
                String id_radio = rG.getId();

                if (id_radio.equals("rdb_pyme")) {
                    clm_3.setVisible(true);
                    clm_4.setVisible(true);
                    lInforme = informe.list_Informe_Pyme();
                    refresh_grid(lInforme);
                } else if (id_radio.equals("rdb_retail")) {
                    clm_3.setVisible(false);
                    clm_4.setVisible(false);
                    lInforme = informe.list_Informe_Retail();
                    refresh_grid(lInforme);
                }

            }

        });

    }*/
    private void eventos() {

        /* lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });*/
        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

        //        dbx_desde.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dDesdeLocal = dbx_desde.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
        //        dbx_hasta.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dHastaLocal = dbx_hasta.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
    }

    public InformeEjecutivosCondonaciones() throws SQLException {

        // this.cond = new CondonacionImpl(mmmm.getDataSourceProduccionMYSQL());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.modulo = "EfectividadComercial";

    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;
        String ced = null;
        String template;
        int rutcliente2;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        String AliasEjecutivo = coupleArray[1][1];

        //buscar cliente
        rutcliente2 = Integer.parseInt(coupleArray[2][1]);//Executions.getCurrent().getParameter("rutcliente");
        // String mm = rutcliente2.replace(".", "");
        //   String[] ParteEntera = mm.split("-");
        ListDetOper = detoper.Cliente(rutcliente2, cuenta);
        if (ListDetOper == null) {
            Messagebox.show("Error No Existe Detalle Operaciones.");
            return;

        }

        //buscar colaborador origen de la condoancion
        usuario usu;
        usu = this._usu.buscarUsuarioCuenta(AliasEjecutivo);

        int rutejecutivo = usu.getDi_rut();

        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

    }

    public void RepararCondonacion(int id_condonacion) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", id_condonacion);
        arguments.put("rut", 15.014544);
        String template = "Analista/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void MostrarReparos(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;
        int id_Condonacion;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        id_Condonacion = Integer.parseInt(coupleArray[0][1]);
        String AliasEjecutivo = coupleArray[1][1];
        //buscar colaborador origen de la condoancion
        usuario usu;
        usu = this._usu.buscarUsuarioCuenta(AliasEjecutivo);

        int rutejecutivo = usu.getDi_rut();

        //int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        // Messagebox.show("IDC:["+id_Condonacion+"]");
        int rutcliente = this.cond.getClienteEnCondonacion(id_Condonacion);
        arguments.put("id_condonacion", id_Condonacion);
        //arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);
        // Messagebox.show("FFFFFF{"+this.AreaTrabajo+"}");
//        MT.CalculaDv(rutcliente);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        Messagebox.show("Idcondonacion[" + id_Condonacion + "] rutEjecutivo [" + rutejecutivo + "] otroidcondonacion");

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(id_Condonacion, this.AreaTrabajo));

        //Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListReparosCondonacionModel);
        //  arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        //  arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Analista/Poput/ReparosPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void MostrarOperaciones(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;
        int id_Condonacion;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }

        id_Condonacion = Integer.parseInt(coupleArray[0][1]);
        int cliente = Integer.parseInt(coupleArray[1][1]);
        //buscar colaborador origen de la condoancion
        //   usuario usu;
        //     usu = this._usu.buscarUsuarioCuenta(AliasEjecutivo);
        //     
        //      int rutejecutivo = usu.getDi_rut();

        //int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        // Messagebox.show("IDC:["+id_Condonacion+"]");
        int rutcliente = this.cond.getClienteEnCondonacion(id_Condonacion);
        arguments.put("id_condonacion", id_Condonacion);
        //arguments.put("rut", "14212287-1");
        arguments.put("rutCliente", cliente);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);
        // Messagebox.show("FFFFFF{"+this.AreaTrabajo+"}");
//        MT.CalculaDv(rutcliente);

        char uuuu = MT.CalculaDv(cliente);

        arguments.put("rutcliente", cliente + "-" + uuuu);
        // Messagebox.show("Idcondonacion["+idCondonacion+"] rutEjecutivo ["+rutejecutivo+"]");

        // ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));
        ListOperacionesInformeModel = new ListModelList<CondonacionesEjecutivos>(this.cond.getInfEjecutivoCondonacion());

        // cond.getInfClienV2();
        //Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListOperacionesInformeModel);
        //  arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        //  arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Informes/Poput/OperacionesXrut.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<CondonacionesEjecutivos> filterGrid() {
        List<CondonacionesEjecutivos> lFilterGrid = new ArrayList<CondonacionesEjecutivos>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = listCondonaciones;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = listCondonaciones;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (CondonacionesEjecutivos cT2 : listCondonaciones) {
                        String cadena = cT2.getEjecutivo_condona() + ";"
                                + cT2.getUltimaCondonacionAplicada() + ";"
                                + cT2.getUltimaCondonacionaprovada() + ";"
                                + cT2.getNumero_condonaciones() + ";"
                                + cT2.getTotal_reparos();

                        if (MT.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

//        if (dDesdeLocal != dDesdeLocalResp) {
//            if (dDesdeLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.after(dDesdeLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
//
//        if (dHastaLocal != dHastaLocalResp) {
//            if (dHastaLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.before(dHastaLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    private void cargaGrid(List<CondonacionesEjecutivos> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<CondonacionesEjecutivos>(listCondonaciones);

        grd_InformeCond.setModel(bandeCondTerminada);

    }

    public void exportListboxToExcel() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024 * 10);
//FileInputStream fis = new FileInputStream(myFile);
        grd_InformeCond2.renderAll();
        ExcelExporter exporter = new ExcelExporter();
        exporter.export(grd_InformeCond2, out);

        AMedia amedia = new AMedia("InofrmeEjecutivos.xlsx", "xls", "application/file", out.toByteArray());
        Filedownload.save(amedia);
        out.close();
    }

    /*	public void exportGridToExcel() throws Exception {
		ExcelExporter exporter = new ExcelExporter();
		                 listCondonaciones = cond.getInfClienV2();
                bandeCondTerminada = new ListModelList<siscon_inf_clienteV2>(listCondonaciones);

                lCondFinal=new ArrayList<siscon_inf_clienteV2>();
                lCondFinal=listCondonaciones;
                lCondFilter=lCondFinal;
                grd_InformeCond.setModel(bandeCondTerminada);
                //grd_InformeCond.setro
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		exporter.export(grd_InformeCond, out);
		//exporter.export(grd_InformeCond, out);
		AMedia amedia = new AMedia("FirstReport.xlsx", "xls", "application/file", out.toByteArray());
		Filedownload.save(amedia);
		
		out.close();
	}*/
}
