/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sourceforge.jtds.jdbc.DateTime;
import org.zkoss.lang.Objects;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Cliente;
import siscon.entidades.Colaborador;
import siscon.entidades.Cond_Rechazadas;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.DetalleCliente;
import siscon.entidades.GarantiasCliente;
import siscon.entidades.GlosaDET;
import siscon.entidades.GlosaENC;
import siscon.entidades.JudicialCliente;
import siscon.entidades.Regla;
import siscon.entidades.SacaBop;
import siscon.entidades.SbifCliente;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsrProvision;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.ClienteSisconJDBC;
import siscon.entidades.implementaciones.ColaboradorJDBC;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.CondonadorJDBC;
import siscon.entidades.implementaciones.GarantiasClienteJDBC;
import siscon.entidades.implementaciones.GeneralAppJDBC;
import siscon.entidades.implementaciones.OperacionJDBC;
import siscon.entidades.implementaciones.ProvisionJDBC;
import siscon.entidades.implementaciones.ReglasImpl;
import siscon.entidades.implementaciones.SbifClienteJDBC;
import siscon.entidades.implementaciones.TrackinJDBC;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.GarantiasClienteInterfaz;
import siscon.entidades.interfaces.ReglasInterfaz;
import siscon.entidades.interfaces.SbifClienteInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class AnSacabopRecepcionController extends SelectorComposer<Window> {

    @Wire
    Label lbl_TioAux2;
    @Wire
    Label lbl_OpOrg2;
    @Wire
    Label lbl_CuMor2;
    @Wire
    Label lbl_SalIns2;
    @Wire
    Label lbl_CuPag2;
    @Wire
    Label lbl_CuPact2;
    @Wire
    Label lbl_DiaMor2;
    @Wire
    Label lbl_TioAux;
    @Wire
    Label lbl_OpOrg;
    @Wire
    Label lbl_CuMor;
    @Wire
    Label lbl_SalIns;
    @Wire
    Label lbl_CuPag;
    @Wire
    Label lbl_CuPact;
    @Wire
    Label lbl_DiaMor;
    @Wire
    Label lbl_Condona;
    @Wire
    Label negoc;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox nro_condonacion;
    @Wire
    Label viewrutcliente;
    @Wire
    Grid Grid_Sacabop;
    @Wire
    Grid Grid_SacabopXX;
    @Wire
    Textbox id_fechahoy;
    @Wire
    Textbox id_nomcliente;
    @Wire
    Textbox id_oficinaorigen;
    @Wire
    Textbox id_TotalSumaCondonaCapital;
    @Wire
    Textbox id_TotalSumaCapital;
    @Wire
    Textbox id_TotalSumaRecibeCapital;
    @Wire
    Textbox id_saldototalmoroso;
    float id_saldototalmorosoFloat;
    @Wire
    Label idTotalRecibe;
    @Wire
    Label id_TotalTotal;
    @Wire
    Label id_TotalCondona;
    @Wire
    Label idValorUF;
    @Wire
    Label id_TotlaCapital;
    @Wire
    Label idtotalhonor;
    @Wire
    Textbox id_TotalSumaInteres;
    @Wire
    Textbox id_TotalSumaCondonaInteres;
    @Wire
    Textbox id_TotalSumaRecibeInteres;

    // identificadores de textbod judiciales en la grilla
    @Wire
    Textbox id_TotalSumaHonorJud;
    @Wire
    Textbox id_TotalSumaHonorJudCond;
    @Wire
    Textbox id_TotalSumaHonorJudRec;

    // Totoales Parciales Recibe Color GREEN
    @Wire
    Textbox id_TotoalParcialCapital;
    @Wire
    Textbox id_TotoalParcialInteres;
    @Wire
    Textbox id_TotoalParcialhonor;

    //Totoales Parciales Condona Color Red
    @Wire
    Textbox id_TotoalParcialCondonaCapital;
    @Wire
    Textbox id_TotoalParcialCondonaInteres;
    @Wire
    Textbox id_TotoalParcialCondonaHono;

    /// Totoales parciales Capital YELLOW
    @Wire
    Textbox txt_TipoEspecial;
    @Wire
    Checkbox chk_especial;
    @Wire
    Textbox id_TotoalParcialCapita;
    @Wire
    Textbox id_TotoalParcialCapitaInteres;
    @Wire
    Textbox id_TotoalParcialCapitaHonor;

    /// variables del llenado de info de condonacion
    @Wire
    Textbox id_PrimerMesDeCondonacion;
    @Wire
    Textbox id_MesMasAntiguoCastigo;

    @Wire
    Textbox id_AtribucionEjecutiva;
    @Wire
    Textbox id_porcentajeCondoEjecutiva;

    @Wire
    Textbox id_PuedeCondonarOnline;

    @Wire
    Textbox id_rangoFechaCondonacion;
    @Wire
    Panel panelgridddd;

    @Wire
    Label id_msgeCona;

    @Wire
    Button btn_enviaSacaBopg;
    @Wire
    Button id_modificaRegla;
    @Wire
    Button id_modificaRegla2;
    @Wire
    Button btn_GenrarCondonacion;

    @Wire
    Vlayout vlayoutmensajje;
    @Wire
    Label id_TotalVDEs;

    @Wire
    Window id_windowsMessajje;

    @Wire
    Grid grd_GastosHono;
    @Wire
    Label id_SumaProvision;
    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    @WireVariable
    ListModelList<SacaBop> sacaboj;
    final DetalleOperacionesClientes detoper;
    final JudicialClienteInterz JudCliente;
    final CondonacionInterfaz cond;
    final ReglasInterfaz _reglas;
    final GarantiasClienteInterfaz _garantias_cliente;
    final SbifClienteInterfaz _deuda_sbif;
    String FechaPrimerCastigo;
    int mesescastigomasantiguo;
    Condonacion CurrentCondonacion;
    List<DetalleCliente> ListDetOper = null;
    //variables sumatorias totales de cada columna por orden 1->A  etc...
    int RutClienteFormateado;
    float totalA;
    int MaximoMesCastigo;
    int MaximoMesCastigoOnPrevio;
    float totalB;
    float totalC;
    float totalD;
    float totalE;
    float totalF;
    float totalG;
    float totalH;
    float totalI;
    float sumaMoraTotal;
    final ClienteInterfazImpl clienteinfo;
    @WireVariable
    Cliente InfoCliente;
    ClienteSisconJDBC _cliente;
    GeneralAppJDBC _ggJDBC;
    TrackinJDBC _track;
    CondonadorJDBC _condonadorJDBC;
    OperacionJDBC _operJDBC;
    OperacionJDBC _operJDBCsiscon;
    Session sess;
    String AreaTrabajo;
    String cuenta;
    String Nombre;
    @Wire
    Window capturawin;
    @Wire
    Window win_rechazos2;
    String rutcliente;
    float ReglaInteresPorcentajeCondonacion;
    float reglaCapitalPorcentajeCndonacion;
    float ReglaHonorarioPorcentajeCondonacion;
    List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();
    List<DetalleCliente> detClientSacabop = new ArrayList<DetalleCliente>();
    UsuarioPermiso permisos;
    @Wire
    Window id_wSacabop;
    Window window;
    float pppp2;
    float SumaTotalVDEs = 0;
    float InteresBanco = 0;
    int id_valor_regla = 0;
    int id_valor_regla_capital = 0;
    int id_valor_regla_Honorario = 0;
    float UfDia;
    float ufAplica;
    int OpCont;
    float ppppppp;
    float pppp3;
    int condonacion = 0;
    NewJerseyClient _token;
    UsuarioDAO _usu;
    ColaboradorJDBC _colabo;
    CondonacionInterfaz condInt;
    private final MetodosGenerales metodo;
    @WireVariable
    ListModelList<GarantiasCliente> ListGarantiasClienteModel;
    ListModelList<SbifCliente> ListSbifClienteClienteModel;
    //SbifCliente
    NumberFormat nf;//= NumberFormat.getCurrencyInstance(Locale.getDefault());
    float SumaTotalProvision = 0;
    ProvisionJDBC _prov;
    private List<UsrProvision> listProvisiones;

    private EventQueue eq;//cosorio
    private GlosaENC glosa; //cosorio
    private String modulo = "AnSacabopView";//cosorio
    private AdjuntarENC adjunta;
    private Cliente client;
    private List<Media> fileAdjuntos;//cosorio
    private float sizeFiles = 0;
    ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();
    private GlosaDET glosaDet;
    private CondonacionesEspeciales cE;
    int id_usrmod_int, id_usrmod_cap, id_usrmod_hon;
    Window glosaWin;
     int puedeEvaluar = 0;
    private Cond_Rechazadas cRechazo;

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        capturawin = (Window) comp;
        Locale.setDefault(new Locale("es", "CL"));

        eventos();

        lbl_TioAux2.setValue("C�digo <br>Operaci�n".replace("<br>", "\r\n"));
        lbl_OpOrg2.setValue("Operaci�n <br>original".replace("<br>", "\r\n"));
        lbl_SalIns2.setValue("Saldo <br>Insoluto".replace("<br>", "\r\n"));
        lbl_DiaMor2.setValue("D�as <br>Mora".replace("<br>", "\r\n"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        usuario usu;
        Colaborador colabo;
        /// traemos data del ejecutivo negociados
        String rutS=Sessions.getCurrent().getAttribute("rutEjecutivo").toString();
        puedeEvaluar = condInt.isCliente(rutS);
        int rut_ejecutivo = Integer.parseInt(Sessions.getCurrent().getAttribute("rutEjecutivo").toString());
        if (rut_ejecutivo > 0) {
            usu = this._usu.buscarUsuarioRutDi(rut_ejecutivo);

            this.condonacion = Integer.parseInt(Sessions.getCurrent().getAttribute("idcondonacion").toString());
            colabo = this._colabo.buscarColaboradorRutDi(rut_ejecutivo);
        } else {

            Messagebox.show("AnSacabopController1 Error no hay rut ejecutivo");
            return;

        }

        cuenta = usu.getAccount();//user.getAccount();
        Nombre = usu.getFullName();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        detClientCond = (List<DetalleCliente>) sess.getAttribute("detClientCond");

        rutcliente = Sessions.getCurrent().getAttribute("rutcliente").toString();//Executions.getCurrent().getParameter("rutcliente");
        String mm = rutcliente.replace(".", "");
        String[] ParteEntera = mm.split("-");

        ListDetOper = detoper.Cliente(Integer.parseInt(ParteEntera[0]), cuenta,(puedeEvaluar == 2) ? 1 : 0);
        if (ListDetOper == null) {
            Messagebox.show("Error No Existe Detalle Operaciones.");
            return;

        }
        //// calculo de suma vdes

        for (DetalleCliente detCliHono : ListDetOper) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {

                listModelHono.add(detCliHono);
                SumaTotalVDEs = SumaTotalVDEs + (float) detCliHono.getSaldoinsoluto();

            }
        }
        this.listProvisiones = this._prov.listProvisionXrut(rutcliente);
        for (UsrProvision prov : listProvisiones) {

            //  listModelHono.add(detCliHono);
            SumaTotalProvision = SumaTotalProvision + (float) prov.getMonto();

        }

        grd_GastosHono.setModel(listModelHono);

        grd_GastosHono.setVisible(false);
        grd_GastosHono.setVisible(true);

        glosa = new GlosaENC();//cosorio incia variable limpia
        adjunta = new AdjuntarENC();

        //Herencia entre Sacabop e hijos
        {//Bloque anonimo creado por cosorio

            //Herencia ente Glosa y Sacabop
            eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    glosaDet = (GlosaDET) map.get("GlosaSS");

                    if (glosaDet != null) {
                        glosa.setCant_glosas(1);

                    } else {
                        glosa.setCant_glosas(0);
                    }
                }
            });

            //Herencia ente Adjuntar y Sacabop
            eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    List<AdjuntarDET> det = new ArrayList<AdjuntarDET>();
                    det = (List<AdjuntarDET>) map.get("ListAdjuntoSS");
                    sizeFiles = (Float) map.get("tama�o");

                    if (det != null) {
                        if (!det.isEmpty()) {
                            adjunta.setNumero_archivos(det.size());
                            adjunta.setTama�o_total(String.format("%.4f", sizeFiles) + "Mb");
                            adjunta.setaDet(det);
                        }
                    }

                }

            });
        };

        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        int ii = 0;

        List<String> Operaciones = new ArrayList<String>();
        // Messagebox.show("FloatUF [+"+this.UfDia+"+]");
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;
        if (detClientCond.size() > 0) {

            for (DetalleCliente seleccion : detClientCond) {

                if (!detClientSacabop.contains(seleccion)) {
                    Operaciones.add(seleccion.getOperacion());
                    detClientSacabop.add(seleccion);

                }

            }
        }

        ListDetOper.clear();
        ListDetOper = detClientSacabop;

        RutClienteFormateado = Integer.parseInt(ParteEntera[0]);
        List<JudicialCliente> ListJudCliente = JudCliente.JudClie(Integer.parseInt(ParteEntera[0]),cuenta);
        InfoCliente = clienteinfo.infocliente(Integer.parseInt(ParteEntera[0]),cuenta,(puedeEvaluar == 2) ? 1 : 0);

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
        ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

        sacaboj = new ListModelList<SacaBop>();
        String oper = "NULL";
        String MeseCatigo = "NULL";
        String FechaCastigo = "14-01-2012 09:29:58";
        //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = new Date();
        String[] parse;
        //d2=format.parse(d2.toString());
        DateTime NN;
        // creamos instancia de condonacion      

        CurrentCondonacion = new Condonacion(rutcliente, cuenta, sess.getWebApp().toString());

        /// Busqueda de Fecha Castigo mas Antigua
        //////##### AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            if (ListDetOperModel.get(i) != null) {
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigoOnPrevio) {
                    this.MaximoMesCastigoOnPrevio = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigoOnPrevio);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
            }
        }

        CurrentCondonacion.setAdjuntar(cond.GetAdjuntoCond(condonacion));
        adjunta = CurrentCondonacion.getAdjuntar();
        CurrentCondonacion.setGlosa(cond.GetGlosaCond(condonacion));
        glosa = CurrentCondonacion.getGlosa();

        //////##### FIN-AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        //////######   AQUI SE BUSCA Y GUARDA LA INFORMACION DEL EJECUTIVO####///////
        // CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsulting(rut_ejecutivo, this.cuenta, this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));
        //  CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsultingV2(permisos.getDi_rut(), permisos.getNombre(), this.CurrentCondonacion.getNumeroMesesPrimerCastigo(),this.condonacion)); 
        CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsultingV4(rut_ejecutivo, usu.getAlias(), this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));

        //float atrMa=CurrentCondonacion._condonador.getAtribucionMaxima();
        this.CurrentCondonacion.setMontoMaximoAtribucionEjecutiva(CurrentCondonacion._condonador.getAtribucionMaxima());
        this.CurrentCondonacion.setMontoAtribucionMaxima(CurrentCondonacion._condonador.getAtribucionMaxima());
        ppppppp = 0;
        pppp2 = 0;
        pppp3 = 0;
        for (final Regla _regla : this.CurrentCondonacion._condonador._reglaList) {
            //list2.add(tipodocto.getDv_NomTipDcto());
            if (_regla.getDesTipoValor().equals("Monto Capital")) {
                ppppppp = _regla.getPorcentajeCondonacion100();
                id_valor_regla_capital = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Interes")) {
                pppp2 = _regla.getPorcentajeCondonacion100();
                id_valor_regla = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Honorario Judicial")) {
                pppp3 = _regla.getPorcentajeCondonacion100();
                id_valor_regla_Honorario = _regla.getIdRegla();
            }

        }

        /// validamos modificacion de Regla de Condonacion por el Ejecutivo (id_condonacion,id_usuario,id_regla_valor)
        if (this.CurrentCondonacion._condonador.getPorcentajeActual() >= 0) {
            pppp2 = this.CurrentCondonacion._condonador.getF_PorcentajeActualInt();
            id_usrmod_int = this.CurrentCondonacion._condonador.getId_usr_modif_int();

        }
        if (this.CurrentCondonacion._condonador.getPorcentajeActualCapital() >= 0) {
            ppppppp = this.CurrentCondonacion._condonador.getF_PorcentajeActualCap();
            id_usrmod_cap = this.CurrentCondonacion._condonador.getId_usr_modif_cap();
        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActualHonorario() >= 0) {
            pppp3 = this.CurrentCondonacion._condonador.getF_PorcentajeActualHon();
            id_usrmod_hon = this.CurrentCondonacion._condonador.getId_usr_modif_hon();
        }

        reglaCapitalPorcentajeCndonacion = (float) ((float) ppppppp / (float) 100);
        ReglaInteresPorcentajeCondonacion = (float) ((float) pppp2 / (float) 100);
        ReglaHonorarioPorcentajeCondonacion = (float) ((float) pppp3 / (float) 100);
        String rangoFechaInicio = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRanfoFInicio());
        String rangoFechaFin = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRangoFFin());

        this.CurrentCondonacion.setPorcentajeCondonaCapital(reglaCapitalPorcentajeCndonacion);
        this.CurrentCondonacion.setPorcentajeCondonaHonorario(ReglaHonorarioPorcentajeCondonacion);
        this.CurrentCondonacion.setPorcentajeCondonaInteres(ReglaInteresPorcentajeCondonacion);
        this.CurrentCondonacion.setRangoFechaInicio(rangoFechaInicio);
        this.CurrentCondonacion.setRangoFehaFin(rangoFechaFin);
        this.CurrentCondonacion.setPuedeCondonarEnLinea(false);
        this.CurrentCondonacion.setNumeroDeOperaciones(ListDetOper.size());

        /// Si esta en  juicio la operacion o no le cobramos el porcentaje correspondiente
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            SacaBop temp = new SacaBop();
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setDetalleCredito(ListDetOperModel.get(i).getDetalleCredito());
                temp.setCedente(ListDetOperModel.get(i).getCedente());
                temp.setFechaCastigo(ListDetOperModel.get(i).getFechaCastigo());
                temp.setFechaFencimiento(ListDetOperModel.get(i).getFechaFencimiento());
                temp.setDiasMora(ListDetOperModel.get(i).getDiasMora());
                temp.setMarcaRenegociado(ListDetOperModel.get(i).getMarcaRenegociado());

                //  Calculo del Juicio Activo Para el Conbro de Honorarios Judiciales
                for (int j = 0; j < ListJudClienteModel.getSize(); j++) {
                    String OperacionJud = ListJudClienteModel.get(j).getOperacion() != null ? ListJudClienteModel.get(j).getOperacion() : "0";
                    String OperacionDet = ListDetOperModel.get(i).getOperacionOriginal() != null ? ListDetOperModel.get(i).getOperacionOriginal() : "0";
                    int nnnn = OperacionJud.compareTo(OperacionDet);

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);
                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneJuicio(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneJuicio(false);
                    }

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);

                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneRol(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneRol(false);
                    }

                }

                ////* Fin Juicio Activo
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                System.out.println("#------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                temp.setOperacion(oper);
                temp.setMesesCastigo(MeseCatigo);

                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigo) {
                    this.MaximoMesCastigo = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigo);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
                this.totalA = (float) this.totalA + (float) ListDetOperModel.get(i).getSaldoinsoluto();

                temp.setCapital(ListDetOperModel.get(i).getSaldoEnPesosChileno());

                // verificar si el usuario ha ingresado otro interes personalizado
                float interes = 0;
                float montoInteresAjustado = this._operJDBCsiscon.getProcentajeInteresActual(this.RutClienteFormateado, this.cuenta, oper, this.condonacion);

                if (montoInteresAjustado >= 0) {
                    interes = montoInteresAjustado;

                } else {
                    interes = (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                }

                InteresBanco = InteresBanco + (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                sumaMoraTotal = sumaMoraTotal + (float) ListDetOperModel.get(i).getMora();
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                float porcentaje_condonacion = (float) this.CurrentCondonacion.getPorcentajeCondonaCapital();
                float porcentaje_condonacion_Interes = (float) this.CurrentCondonacion.getPorcentajeCondonaInteres();
                float capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion;
                this.totalB = (float) (this.totalB + capital_condonado);
                float capitalarecibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;
                this.totalC = (float) (capitalarecibir + this.totalC);

                /// agregagos el monto a recibir a la clase condonacion
                this.CurrentCondonacion.setMontoARecibir(capitalarecibir);

                float interes_condonado = interes * porcentaje_condonacion_Interes;
                float interesarecibir = interes - interes_condonado;

                this.totalD = (float) (interes + this.totalD);
                this.totalE = (float) (interes_condonado + this.totalE);
                this.totalF = (float) (interesarecibir + this.totalF);

                // se agrega el monto a condonar a la clase sacabop
                temp.setMontoCondonar((long) capital_condonado);
                temp.setMontoCondonarPesos(nf.format(capital_condonado).replaceFirst("Ch", ""));
                temp.setCapitalARecibirPesos(nf.format(capitalarecibir).replaceFirst("Ch", ""));

                temp.setMontoARecibir((long) capitalarecibir);
                double porcentaje_honorjud = 0;
                double honorariojud = 0;
                double honorariojud2 = 0;
                double honorariojud_sobrecondonado = 0;
                double Honor2 = 0;
                double capital_a_recibir = (double) ListDetOperModel.get(i).getSaldoinsoluto() - (double) capital_condonado;

                // 
                //  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (double) 0.15;

                    double c = Math.round((double) this.ufAplica);
                    double a = Math.round((double) capital_a_recibir);
                    double b = Math.round(((double) 10 * c));

                    if (a <= b) {

                        honorariojud_sobrecondonado = Math.round(a * 0.09);
                    } else if ((b) < a && a <= ((double) 50 * c)) {

                        double hh = Math.round((b * 0.09));
                        double amenosb = Math.round((a - b));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.06);

                        honorariojud_sobrecondonado = Math.round(hh + ww3);

                    } else if (a > ((double) 50 * c)) {
                        double uno = (double) ((double) 10 * c) * (double) 0.09;
                        double dos = (double) (40 * c * (double) 0.06);
                        double tes_part1 = a;
                        double tres_part2 = ((double) (50 * c));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (double) 0.50;

                    double capital2 = (double) capital_a_recibir;
                    double rango0_500 = ((double) 500 * (double) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud_sobrecondonado = (double) capital_a_recibir * (double) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir && capital_a_recibir <= (3000 * this.ufAplica)) {

                        double uf = (double) this.ufAplica;
                        double capital = (double) capital_a_recibir;
                        double ptje2 = (double) 0.05;
                        double unff = ((double) 500 * uf);

                        double hh = Math.round((unff * 0.15));
                        double amenosb = Math.round((capital - unff));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.05);

                        honorariojud_sobrecondonado = Math.round(hh + ww3);

                    } else if ((double) capital_a_recibir > ((double) 3000 * (double) this.ufAplica)) {
                        double uno = (double) (500 * (double) this.ufAplica) * (double) 0.15;
                        double dos = (double) (2500 * (double) this.ufAplica * (double) 0.06);
                        double tes_part1 = (double) capital_a_recibir;
                        double tres_part2 = ((double) (3000 * this.ufAplica));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (double) 0.15;

                    double c = Math.round((double) this.ufAplica);
                    double a = Math.round((double) ListDetOperModel.get(i).getSaldoinsoluto());
                    double b = Math.round(((double) 10 * c));

                    if (a <= b) {

                        honorariojud = Math.round(a * 0.09);

                    } else if ((b) < a && a <= ((double) 50 * c)) {

                        double hh = Math.round((b * 0.09));
                        double amenosb = Math.round((a - b));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.06);

                        honorariojud = Math.round(hh + ww3);

                    } else if (a > ((double) 50 * c)) {
                        double uno = (double) ((double) 10 * c) * (double) 0.09;
                        double dos = (double) (40 * c * (double) 0.06);
                        double tes_part1 = a;
                        double tres_part2 = ((double) (50 * c));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (double) 0.50;

                    double capital2 = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                    double rango0_500 = ((double) 500 * (double) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud = (double) ListDetOperModel.get(i).getSaldoinsoluto() * (double) 0.15;

                    } else if ((500 * this.ufAplica) < ListDetOperModel.get(i).getSaldoinsoluto() && ListDetOperModel.get(i).getSaldoinsoluto() <= (3000 * this.ufAplica)) {

                        double uf = (double) this.ufAplica;
                        double capital = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                        double ptje2 = (double) 0.05;
                        double unff = ((double) 500 * uf);

                        double hh = Math.round((unff * 0.15));
                        double amenosb = Math.round((capital - unff));
                        double ww = Math.round(amenosb);
                        double ww3 = Math.round(ww * (double) 0.05);

                        honorariojud = Math.round(hh + ww3);
                    } else if ((double) ListDetOperModel.get(i).getSaldoinsoluto() > ((double) 3000 * (double) this.ufAplica)) {
                        double uno = (double) (500 * (double) this.ufAplica) * (double) 0.15;
                        double dos = (double) (2500 * (double) this.ufAplica * (double) 0.06);
                        double tes_part1 = (double) ListDetOperModel.get(i).getSaldoinsoluto();
                        double tres_part2 = ((double) (3000 * this.ufAplica));
                        double tres = ((double) tes_part1 - (double) tres_part2) * (double) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                }

                //  el calculo de los honorarios judiciales esta compuesto del CalculaMontoJudicial(monto a recibir)
                double honorarioJudCondonado = (double) honorariojud * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudReibido = (double) honorariojud - (double) honorarioJudCondonado;

                double honorarioJudCondonado2 = (double) honorariojud_sobrecondonado * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudReibido2 = (double) honorariojud_sobrecondonado - (double) honorarioJudCondonado2;

                //Calculo de Totales para la Grilla
                // se muestra sobre el total capital condonado
                this.totalG = (float) (honorariojud_sobrecondonado + this.totalG);
                this.totalH = (float) (honorarioJudCondonado2 + this.totalH);
                this.totalI = (float) (honorarioJudReibido2 + this.totalI);

                // Grid-Column Judicial 
                temp.setHonorarioJuducial((float) honorariojud);

                temp.setHonorarioJudicial2((double) honorariojud_sobrecondonado);

                //Montos en pesos
                String valorPesos = nf.format(honorariojud).replaceFirst("Ch", "");
                temp.setHonorarioJudicialPesos(valorPesos);
                temp.setHonorarioJudicialCondonadoPesos(nf.format(honorarioJudCondonado).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialRecibidoPesos(nf.format(honorarioJudReibido).replaceFirst("Ch", ""));

                temp.setInteresCondonadoPesos(nf.format(interes_condonado).replaceFirst("Ch", ""));
                temp.setInteresARecibirPesos(nf.format(interesarecibir).replaceFirst("Ch", ""));

                temp.setInteres(nf.format(interes).replaceFirst("Ch", ""));

                sacaboj.add(temp);
            } else {
                oper = "SOYNULL";
            }
            // sacaboj.add(oper);

            System.out.println("#------------@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        }

        this.CurrentCondonacion.setHonorarioJudicial2((double) this.totalG);
        Grid_SacabopXX.setModel(sacaboj);
        Grid_SacabopXX.setVisible(false);
        Grid_SacabopXX.setVisible(true);
        idValorUF.setValue(Float.toString(this.UfDia));

        //  Grid_Sacabop.setModel(sacaboj);
        System.out.println("#------------@@@@@@@@@@@@@@@@@ ListDetOperModel.getSize()[" + ListDetOperModel.getSize() + "]Grid_SacabopXX [" + Grid_SacabopXX.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        // sacaboj
        //String[] parameter = (String[]) param.get("test");
        try {
            //String fechahoy;
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            //fechahoy = ((Date)format.parse(d2.toString())).toString();
            NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());

            /// Agregagamos valores totales a la clase condonacion
            CurrentCondonacion.setCapitalClass(this.totalA + this.totalD + this.totalG);
            CurrentCondonacion.setTotalCondonado(this.totalB + this.totalE + this.totalH);

            //totales de la Grilla
            id_TotalSumaCapital.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotalSumaCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotalSumaRecibeCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));

            id_TotalSumaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotalSumaCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            id_TotalSumaRecibeInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            id_TotalSumaHonorJud.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // id_TotalSumaHonorJudCond.setValue(nff.format(this.totalH).replaceFirst("Ch", ""));
            // id_TotalSumaHonorJudRec.setValue(nff.format(this.totalI).replaceFirst("Ch", ""));

            // totoales de Sumatorias
            idTotalRecibe.setValue(nff.format(this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            //Guarda en condonacion
            this.CurrentCondonacion.setTotoalRecibe(this.totalC + this.totalF + this.totalI);

            id_TotalCondona.setValue(nff.format(this.totalB + this.totalE + this.totalH).replaceFirst("Ch", ""));
            //id_TotlaCapital.setValue(nff.format(this.totalA + this.totalD+this.totalG).replaceFirst("Ch", ""));
            id_TotlaCapital.setValue(CurrentCondonacion.TotoalCapital.getValorPesos());

            // totales Parciales en La Suma de Resultados Por Color GREEN
            id_TotoalParcialCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));
            id_TotoalParcialInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));
            //id_TotoalParcialhonor.setValue(nff.format(this.totalI).replaceFirst("Ch", ""));

            // Suma de VDEs para green
            id_TotalVDEs.setValue(nff.format(SumaTotalVDEs).replaceFirst("Ch", ""));

            this.id_SumaProvision.setValue(nff.format(SumaTotalProvision).replaceFirst("Ch", ""));
            id_TotalTotal.setValue(nff.format(this.SumaTotalProvision + SumaTotalVDEs + this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color RED
            id_TotoalParcialCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotoalParcialCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            //  id_TotoalParcialCondonaHono.setValue(nff.format(this.totalH).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color YELLOW
            id_TotoalParcialCapita.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaHonor.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // totales Parciales en La Suma de Resultados Por Color RED

            // fin totales Grilla
            id_fechahoy.setValue(dateFormat.format(date));
            nro_condonacion.setValue(Integer.toString(condonacion));

            //Cosorio tipos especiales
            cE = new CondonacionesEspeciales();
            cE = cE.getCondonacionesEspeciales_X_IdCond(condonacion);
            chk_especial.setDisabled(true);
            txt_TipoEspecial.setDisabled(true);

            if (cE != null) {
                if (cE.getId_TipoEspecial() > 0) {
//                    cE.settCE(new TipoCondEspecial());
//                    cE.gettCE().getTipoCondEspecial_X_Id(cE.getId_TipoEspecial());
                    chk_especial.setChecked(true);
                    if (cE.gettCE().getCodigo().equals("CONDPORCUOTAS")) {
                        txt_TipoEspecial.setText(cE.gettCE().getDescripcion() + " (" + cE.getNumCuotas() + "");
                    } else {
                        txt_TipoEspecial.setText(cE.gettCE().getDescripcion());
                    }
                } else {
                    chk_especial.setChecked(false);
                    txt_TipoEspecial.setText("Sin marca especial");
                }
            } else {
                chk_especial.setChecked(false);
                txt_TipoEspecial.setText("Sin marca especial");
            }
            /////////////////////////////////////////
            valueneg.setValue(Nombre);
            this.CurrentCondonacion.setNombreEjecutiva(Nombre);
            viewrutcliente.setValue(rutcliente);
            this.CurrentCondonacion.setRutCliente(rutcliente);
            id_nomcliente.setValue(InfoCliente.getNombreCOmpleto());
            this.CurrentCondonacion.setNombreCliente(InfoCliente.getNombreCOmpleto());
            id_oficinaorigen.setValue(InfoCliente.getOficina());
            id_saldototalmoroso.setValue(InfoCliente.getSaldoTotalMoraPesos());
            id_saldototalmorosoFloat = (float) Float.parseFloat(InfoCliente.getSaldototal());
            // llenado de informacuion de valores Utilizados en condonacion       
            id_PrimerMesDeCondonacion.setValue(this.CurrentCondonacion.getFechaPrimerCatigo());
            id_MesMasAntiguoCastigo.setValue(Integer.toString(this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
            //id_AtribucionEjecutiva.setValue("MAT");
            id_AtribucionEjecutiva.setValue(this.CurrentCondonacion.getMontoAtribucionMaxima().getValorPesos());
            DecimalFormat df = new DecimalFormat("####0.00");

            //id_porcentajeCondoEjecutiva.setValue("Ca:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaCapital()) + "  In:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaInteres()) + "  Ho:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaHonorario()));
            id_porcentajeCondoEjecutiva.setValue("Ca:" + String.format("%.2f", reglaCapitalPorcentajeCndonacion * 100) + "%  In:" + String.format("%.2f", ReglaInteresPorcentajeCondonacion * 100) + "%  Ho:" + String.format("%.2f", ReglaHonorarioPorcentajeCondonacion * 100) + "%");

            id_PuedeCondonarOnline.setValue(Boolean.toString(this.CurrentCondonacion.isPuedeCondonarEnLinea()));
            id_rangoFechaCondonacion.setValue("FI:" + this.CurrentCondonacion.getRangoFechaInicio() + " FF:" + this.CurrentCondonacion.getRangoFehaFin());
            // btn_enviaSacaBopg.setDisabled(true);
            //*btn_enviaSacaBopg.
            // float TotalCondonado = (float) this.CurrentCondonacion.TotoalCondonado.getValor();
            float TotalCapital = (float) this.CurrentCondonacion.TotoalCapital.getValor();
            float MontoStribucionMaxima = (float) this.CurrentCondonacion.MontoAtribucionMaxima.getValor();
            if (TotalCapital < MontoStribucionMaxima) {
                btn_GenrarCondonacion.setVisible(true);
                id_modificaRegla.setVisible(true);
                id_modificaRegla2.setVisible(false);
                id_windowsMessajje.setClass("alert alert-success");
                // id_msgeCona.setValue("Ejecutivo " +user.getFullName()+ " Cumple Los Requisitos para Condonar Online");
                // id_msgeCona.setValue("asdasdas");
                Label temp;
                temp = (Label) id_windowsMessajje.getFellow("id_msgeCona");
                temp.setValue("Ejecutivo " + Nombre + " Cumple Los Requisitos para Condonar Online");
            } else {
//                btn_enviaSacaBopg.setVisible(true);
//                id_windowsMessajje.setClass("alert alert-danger");
                //  id_modificaRegla.setVisible(false);
                //  id_modificaRegla2.setVisible(true);
                // id_msgeCona.setValue("asdasdas2");
                //  id_msgeCona.setValue("Ejecutivo " +user.getFullName()+ " NO Cumple Los Requisitos para Condonar Online");

            }

            String jjj = Executions.getCurrent().getAttributes().toString();
            //panelgridddd=Executions.getCurrent().getAttributes()
            System.out.println("#------------@@@@@@@@@@@@@@@@@Tratando de accesar other panels" + jjj + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        } catch (WrongValueException e) {
            System.out.println("EXCEPTION: " + e);

        }

        // negoc.setValue("sadklasldas");
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent, ComponentInfo compInfo) {
        System.out.println("doBeforeCompose executed");
        // negoc.setValue("sadklasldas");

        return super.doBeforeCompose(page, parent, compInfo);

    }

    public AnSacabopRecepcionController() throws SQLException {
        this.MaximoMesCastigo = 0;
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceLucy());
        _cliente = new ClienteSisconJDBC(mmmm.getDataSource());
        _track = new TrackinJDBC(mmmm.getDataSource());
        _condonadorJDBC = new CondonadorJDBC(mmmm.getDataSource());
        _reglas = new ReglasImpl(mmmm.getDataSource());
        _garantias_cliente = new GarantiasClienteJDBC(mmmm.getDataSourceProduccion());
        _deuda_sbif = new SbifClienteJDBC(mmmm.getDataSourceProduccion());
        _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
        _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this._colabo = new ColaboradorJDBC(mmmm.getDataSource());
        this._operJDBCsiscon = new OperacionJDBC(mmmm.getDataSource(), "siscon");
        _prov = new ProvisionJDBC(mmmm.getDataSource());
        this.condInt = new CondonacionImpl(mmmm.getDataSource());

        this.sumaMoraTotal = 0;
        this.metodo = new MetodosGenerales();
        glosaDet = new GlosaDET();
        //this.informe=new InformesImpl(mmmm.getDataSourceLucy());
        /*  try {
        _token = new NewJerseyClient();
        }catch(ExceptionInInitializerError ex ){
        
            System.err.println("No se Pudo Inicializar Jersey Clase");
        }*/
        this.UfDia = (float) 0.0;
        OpCont = 0;
    }

    public boolean SaveMetodoCondonacionEnLinea() {

        String mssje;
        final String msgKey = "actions.save.ok";

        // this.CurrentCondonacion
        this.cond.setCurrentCondonacion(this.CurrentCondonacion);
        this.cond.setDettaleOPeraciones(ListDetOper);
        boolean respSave = this.cond.Guardar();

        if (respSave) {
            this.cond.GuardarCondonacion(6, 1, "Condonacion En Linea");
        }
        if (respSave) {
            _track.guardarTrackinEstado(5, "Nueva", "Condonado Online");

            //window.setVisible(false);
            Messagebox.show("La Informaci�n Correspondiente a la Condonaci�n OnLine ha sido Guardada Correctamente",
                    "Guardar Condonaci�n", Messagebox.OK | Messagebox.CANCEL,
                    Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        Messagebox.show("OOOK", "OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                        Executions.sendRedirect("/siscon/index");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        //Cancel is clicked
                        Messagebox.show("NO-OOOK", "NO-OOOOK", Messagebox.OK, Messagebox.INFORMATION);
                    }
                }
            }
            );

            /*    Messagebox.show("Something is changed. Are you sure?", 
    "Question", Messagebox.OK | Messagebox.CANCEL,
    Messagebox.QUESTION,
        new org.zkoss.zk.ui.event.EventListener<ClickEvent>(){
            public void onEvent(ClickEvent e){
                switch (e.getButton()) {
                case Messagebox.Button.OK: //OK is clicked
                case Messagebox.Button.CANCEL: //Cancel is clicked
                default: //if the Close button is clicked, e.getButton() returns null
                }
            }
        }
    );*/
            //  Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
            //        mssje="ERROR Guardando Informacion  Monto Capital :["+this.CurrentCondonacion.TotoalCapital.getValorPesos()+"]";
            //  Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
        } else {
            mssje = "ERROR Guardando Informacion  Monto Capital :[" + this.CurrentCondonacion.TotoalCapital.getValorPesos() + "]";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);

        }

        return true;
    }

    @Listen("onClick=#btn_enviaSacaBopg")
    public void btn_enviaSacaBopgeee() {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (_cliente.ExisteCliente(Executions.getCurrent().getParameter("rutcliente"))) {
            Messagebox.show("Enviando Formulario de Condonaci�n a Analista Validador");

        } else {

            Messagebox.show(" No existe User Enviando Formulario de Condonaci�n a Analista Validador");

            SaveMetodoCondonacionPendiente();

        }

    }

    public boolean SaveMetodoCondonacionPendiente() {

        String mssje;
        final String msgKey = "actions.save.ok";

        // this.CurrentCondonacion
        this.cond.setCurrentCondonacion(this.CurrentCondonacion);
        this.cond.setDettaleOPeraciones(ListDetOper);
        boolean respSave = this.cond.Guardar();
        if (respSave) {
            this.cond.GuardarCondonacion(9, 2, "Con Aprobaci�n Analista");
        }
        if (respSave) {

            //window.setVisible(false);
            Messagebox.show("Se ha Enviado Correctamente la Condonaci�n Al Analista Validador.",
                    "Envi� Sacabog", Messagebox.OK | Messagebox.CANCEL,
                    Messagebox.QUESTION,
                    new org.zkoss.zk.ui.event.EventListener() {
                public void onEvent(Event e) {
                    if (Messagebox.ON_OK.equals(e.getName())) {
                        //OK is clicked
                        Messagebox.show("Ok", "Ok", Messagebox.OK, Messagebox.INFORMATION);
                        Executions.sendRedirect("/siscon/index");
                    } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        //Cancel is clicked
                        Messagebox.show("Error", "Error", Messagebox.OK, Messagebox.INFORMATION);
                    }
                }
            }
            );

        } else {
            mssje = "ERROR Guardando Informacion  Monto Capital :[" + this.CurrentCondonacion.TotoalCapital.getValorPesos() + "]";
            Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);

        }

        return true;
    }

    public void ModificaReglaCondonacion() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion);

        arguments.put("rut", 15.014544);
        String template = "Ejecutivo/ModificaReglaPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");
        //  Messagebox.show("Prueba fellow11111{" + capturawin.getParent().getFellows().toString() + "}");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {
                //Messagebox.show("Capturo %:[" + nn.getValue() + "]");

                // Agregar la Tabla de Regla y Ademas 
                //((AreaTrabajo) permisos.getArea()).getId_colaborador();
//                _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, pppp2, Integer.parseInt(nn.getValue()));
                //ReglaInteresPorcentajeCondonacion=Float.parseFloat(nn.getValue())/100;
                //Ahora Debemos recargar la grilla
                //Messagebox.show("Prueba fellow2222{"+capturawin.getParent().getFellows().toString()+"}");
                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("Ejecutivo/sacabop.zul");
                //updateSacabop();

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  } else {
        //     Messagebox.show("Please read the terms of service and accept it before you submit the order.");
        //  }
        // Messagebox.show("PRUEBA");
    }

    public void ModificaReglaCondonacion2() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        int InteresPersonalizado = this.cond.getSumaInterezPersonalizado(((AreaTrabajo) permisos.getArea()).getId_colaborador(), this.RutClienteFormateado);
        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion);
        arguments.put("procentajeCondCapital", reglaCapitalPorcentajeCndonacion);
        arguments.put("Total_Capital", id_saldototalmoroso.getValue());
        float dif = sumaMoraTotal - id_saldototalmorosoFloat;
        String peso_formatt = nf.format(dif).replaceFirst("Ch", "");
        arguments.put("Total_Interes", peso_formatt);
        arguments.put("rut_cliente", 15.014544);
        arguments.put("rut_colaborador", 15.014544);
        String InteresPersonalizado2 = nf.format(InteresPersonalizado).replaceFirst("Ch", "");
        arguments.put("InteresPersonalizado", InteresPersonalizado2);
        String template = "Ejecutivo/ModificaReglaCapitalInterezPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");
        final Textbox nn2 = (Textbox) window.getFellow("id_nuevoporcentajeCapital");
        final Textbox nn3 = (Textbox) window.getFellow("id_nuevoAbonoCapital");
        final Textbox nn4 = (Textbox) window.getFellow("id_nuevoporcentajeHonorarios");
        /*  
         final Textbox nn5 = (Textbox) window.getFellow("id_nuevoporcentajeCapital");
              if(Integer.parseInt(nn5.getValue())<=0 || Integer.parseInt(nn5.getValue())>=90)
         {
           //  id_nuevoporcentajeCapital.setValue("0");
             
         Messagebox.show("Rango % capital Invalido Valido 0 a 90 %");
         
         return;
         }
         final Textbox nn6 = (Textbox) window.getFellow("id_nuevoporcentaje");
         
                  if(Integer.parseInt(nn6.getValue())<=0 || Integer.parseInt(nn6.getValue())>=101)
         {
             // id_nuevoporcentaje.setValue("0");
         Messagebox.show("Rango % Inter�s Valido 0 a 100 %");
         
        
         return;
         }
                  
         */
        //Messagebox.show("Prueba fellow11111{" + capturawin.getParent().getFellows().toString() + "}");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {

                _reglas.insertUsrModifReglaDecimales(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, Float.toString(pppp2), Float.toString(Float.parseFloat(nn.getValue())));
                _reglas.insertUsrModifReglaDecimales(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_capital, Float.toString(ppppppp), Float.toString(Float.parseFloat(nn2.getValue())));
                _reglas.insertUsrModifReglaDecimales(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla_Honorario, Float.toString(pppp3), Float.toString(Float.parseFloat(nn4.getValue())));

                _reglas.insertUsrAbonoCapital(((AreaTrabajo) permisos.getArea()).getId_colaborador(), RutClienteFormateado, Float.parseFloat(nn3.getValue().trim().length() > 0 ? nn3.getValue() : "0"), sumaMoraTotal, id_saldototalmorosoFloat);

                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("Ejecutivo/sacabop.zul");
                //updateSacabop();

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //  } else {
        //     Messagebox.show("Please read the terms of service and accept it before you submit the order.");
        //  }
        // Messagebox.show("PRUEBA");
    }

    public void updateSacabopV2() {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_ubicaciondoc", 54545);
        arguments.put("procentajeCondInterez", ReglaInteresPorcentajeCondonacion);
        arguments.put("rut", 15.014544);
        String template = "Ejecutivo/ModificaReglaPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {
                //  Messagebox.show("Capturo %:[" + nn.getValue() + "]");

                ReglaInteresPorcentajeCondonacion = Float.parseFloat(nn.getValue()) / 100;

                // updateSacabop();
                window.detach();

            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
            window.setFocus(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //@Listen("onClick = #id_MostrarGarantias")
    public void MostrarGarantias() {
        // TOS should be checked before accepting order

        ListGarantiasClienteModel = new ListModelList<GarantiasCliente>(_garantias_cliente.GarantiasCliente(RutClienteFormateado, cuenta));
        // if(tosCheckbox.isChecked()) {
        // ListModelList<OrderItem>)orderItemsModel;
        // show result
        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListGarantiasClienteModel);
        arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/GarantiasPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        //Messagebox.show("Imprimo TokenPJ:["+_token.Token5().toString()+"]");
        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    //@Listen("onClick = #id_MostrarGarantias")
    public void MostrarSbif() {
        // TOS should be checked before accepting order

        ListSbifClienteClienteModel = new ListModelList<SbifCliente>(_deuda_sbif.SbifCliente(RutClienteFormateado, cuenta));
        // if(tosCheckbox.isChecked()) {
        // ListModelList<OrderItem>)orderItemsModel;
        // show result
        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListSbifClienteClienteModel);
        arguments.put("totalSumaGarantias", this._deuda_sbif.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/SbifPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
            windowx.setFocus(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void IngresaInteresPersonalizado(final String Operacion) {

        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        //arguments.put("id_ubicaciondoc", 54545);
        arguments.put("Operacion", Operacion);

        DetalleCliente temp = new DetalleCliente();

        //   for (DetalleCliente:_temp : ){}  
        for (final DetalleCliente _ope : ListDetOperModel) {
            if (_ope.getOperacion().equals(Operacion)) {
                temp = _ope;
            }
        }

        final float interesss = (float) temp.getMora() - (float) temp.getSaldoinsoluto();

        arguments.put("Interes", interesss);
        String template = "Ejecutivo/ModificaInteresPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) window.getFellow("ButtonEnvioBoj");
        final Textbox nn = (Textbox) window.getFellow("id_nuevoporcentaje");
        ///  Messagebox.show("Prueba fellow11111{" + capturawin.getParent().getFellows().toString() + "}");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) throws ParseException {
                //Messagebox.show("Capturo %:[" + nn.getValue() + "]");

                // Agregar la Tabla de Regla y Ademas 
                //((AreaTrabajo) permisos.getArea()).getId_colaborador();
                // _reglas.insertUsrModifRegla(((AreaTrabajo) permisos.getArea()).getId_colaborador(), id_valor_regla, pppp2, Integer.parseInt(nn.getValue()));
                _reglas.insertUsrModifInteres(((AreaTrabajo) permisos.getArea()).getId_colaborador(), Operacion, interesss, Float.parseFloat(nn.getValue()), RutClienteFormateado);

                //ReglaInteresPorcentajeCondonacion=Float.parseFloat(nn.getValue())/100;
                //Ahora Debemos recargar la grilla
                //Messagebox.show("Prueba fellow2222{"+capturawin.getParent().getFellows().toString()+"}");
                Include inc = (Include) capturawin.getParent().getFellow("pageref");
                inc.setSrc(null);
                Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
                inc.setSrc("Ejecutivo/sacabop.zul");
                //updateSacabop();

                window.detach();
            }
        });
        printButton.setParent(window);

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Adjuntar() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        if (adjunta == null) {
            adjunta = new AdjuntarENC();
            map.put("ListAdjuntoSS", adjunta.getaDet());
        } else {
            map.put("ListAdjuntoSS", adjunta.getaDet());
        }

        map.put("modulo", modulo);

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/AdjuntarPoput.zul", null, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }
    }

    public void Glosa() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        if (glosa == null) {
            Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
            return;
        } else {
            if (glosa.getDetalle() == null) {
                Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
                return;
            } else {
                if (glosa.getDetalle().isEmpty()) {
                    Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
                    return;
                } else {
                    glosaDet = glosa.getDetalle().get(0);

                    map.put("GlosaSS", glosaDet);
                    map.put("modulo", modulo);

                    try {
                        Window glosaWin = (Window) Executions.createComponents(
                                "EjecutivoPyme/include/Glosa.zul", null, map
                        );
                        glosaWin.doModal();

                    } catch (Exception e) {
                        Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar la glosa. \nError: " + e);
                    }
                }
            }
        }
    }

    private void eventos() { //cosorio
        id_wSacabop.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                    EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                }
                if (EventQueues.exists("Glosa", EventQueues.DESKTOP)) {
                    EventQueues.remove("Glosa", EventQueues.DESKTOP);
                }
            }

        });
    }

    public void RechazarCondonacion() {

        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;
        Messagebox.show("Sr(a) Usuario(a), esta seguro(a) que desea rechazar la condonaci�n N�:" + idcondonacion,
                "Rechazo en linea", Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION,
                new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    procesoRechazo();
                } else if (Messagebox.ON_NO.equals(e.getName())) {
                }
            }
        });

    }

    private void procesoRechazo() {

        final Map<String, Object> map = new HashMap<String, Object>();
               
        map.put("id_condonacion", condonacion);
        map.put("id_int", id_usrmod_int);
        map.put("id_cap", id_usrmod_cap);
        map.put("id_hon", id_usrmod_hon);

        //   try {
        glosaWin = (Window) Executions.createComponents("Analista/Poput/rechazosPopUp.zul", win_rechazos2, map);
        //glosaWin = (Window) Executions.createComponents("Analista/Poput/rechazosPopUp.zul", this.capturawin, map);
        /*    Button printButton = (Button) glosaWin.getFellow("btn_rechazo2");
         Textbox nn=(Textbox) glosaWin.getFellow("txt_detalle");
         final String TextoInput=nn.getText();
         printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
////  aca van las funcionalidades del boton rechazar
                    //id_wSacabop.doModal();
                   // id_wSacabop.detach();
              //  glosaWin.doModal();
              
                      boolean resultado = false;
        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;
        ActualizaTrackinEstado(condonacion);
        resultado = cond.SetCambiaEstadoCondonaciones("Analista.Recepcion", "Ejecutivo.Rechazadas", permisos, idcondonacion, "Estado.PendienteAnalista", "Estado.RechazadaAnalista");

        if (resultado = true) {
              cRechazo = new Cond_Rechazadas();
             cRechazo.setDetalle(TextoInput);
            cRechazo.setDetalle(TextoInput);
            cRechazo.insert();
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),id_usrmod_int);
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),id_usrmod_cap);
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),id_usrmod_hon);
           // Messagebox.show("Guardo los Id de Condonacion.: int  [" + this.getId_int() + "] cap ["+this.getId_cap()+"] hon ["+this.getId_hon()+"]");
        } else {
            Messagebox.show("Error al rechazar  la Condonaci�n Nro.: [" + idcondonacion + "]");
        }
              
              
              
            }
        });
        printButton.setParent(glosaWin);*/
        try {
            // this.window.doModal();
            // capturawin.doModal();
            //id_wSacabop.detach();
            glosaWin.doModal();

            //   id_wSacabop.detach();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //    } catch (Exception e) {
        //        Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar los rechazos. \nError: " + e);
        //    }
    }

    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }
}
