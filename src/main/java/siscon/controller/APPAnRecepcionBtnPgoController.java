/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import config.MvcConfig;
import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Column;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Reparos;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

import org.zkoss.exporter.excel.ExcelExporter;
import org.zkoss.util.media.AMedia;
import org.zkoss.zul.Filedownload;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import static javax.ws.rs.client.Entity.json;
import net.sf.json.JSONException;
//import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;
import net.sourceforge.jtds.jdbc.DateTime;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.LoggerFactory;
import org.zkoss.lang.Objects;
import org.zkoss.zul.Combobox;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.Cliente;
import siscon.entidades.Condonacion;
import siscon.entidades.GlosaENC;
import siscon.entidades.JudicialCliente;
import siscon.entidades.Periodo;
import siscon.entidades.Regla;
import siscon.entidades.ResumenCondonacionesInforme;
import siscon.entidades.SacaBop;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.CondonadorJDBC;
import siscon.entidades.implementaciones.GeneralAppJDBC;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.OperacionJDBC;
import siscon.entidades.implementaciones.PeriodoJDBC;
import siscon.entidades.interfaces.JudicialClienteInterz;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.json.JSONObject;
import org.zkoss.json.JSONArray;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import siscon.entidades.DetalleClienteExcel;
import siscon.entidades.UsrProvision;
import siscon.entidades.implementaciones.ProvisionJDBC;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class APPAnRecepcionBtnPgoController extends SelectorComposer<Component> {

    @Wire
    Grid grd_GridInfor;
    final CondonacionInterfaz cond;
    final CondonacionInterfaz cond_prod;
    ListModelList<CondonacionTabla2> bandeCondTerminada;
    Window window;
    Window windows;
    MetodosGenerales MT;
    // final CondonacionInterfaz cond;
    private List<CondonacionTabla2> listCondonaciones;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private MetodosGenerales metodo;
    NumberFormat nf;
    Session session;
    Session sess;
    public String AreaTrabajo;
    String cuenta;
    String Nombre;
    UsuarioPermiso permisos;
    String modulo;
    ListModelList<Reparos> ListReparosCondonacionModel;
    @Wire
    Button id_refresch;
    int idCondonacion;
    private EventQueue eq;//cosoriosound
    @Wire
    Window win_recepApply;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<CondonacionTabla2> lCondFilter;
    private List<UsrProvision> listProvisiones;
    ProvisionJDBC _prov;
 int puedeEvaluar = 0;
//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<CondonacionTabla2> lCondFinal;
    ////////////////////////////////////////////////////////
    
    @Wire
    Combobox cmb_Periodo;
    
    @Wire
    Datebox db0;
    private ListModelList lCmb_Periodo;
    private List<Periodo> lPeriodo = new ArrayList<Periodo>();
    @Wire
    private Grid grd_InformeCond2;
    @Wire
    private Grid grd_GastosHono;
    @Wire
    private Grid Grid_SacabopXX;
     PeriodoJDBC _periodo;
     ListModelList<SacaBop> sacaboj2=new ListModelList<SacaBop>();
     ListModelList<DetalleCliente> listModelHono2 = new ListModelList<DetalleCliente>();
    
     List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();
     List<DetalleCliente> detClientSacabop = new ArrayList<DetalleCliente>();
     @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
     final JudicialClienteInterz JudCliente;
     float ReglaInteresPorcentajeCondonacion;
    float reglaCapitalPorcentajeCndonacion;
    float ReglaHonorarioPorcentajeCondonacion;
    
    Condonacion CurrentCondonacion;
    int RutClienteFormateado;
    Cliente InfoCliente;
    final ClienteInterfazImpl clienteinfo;
    int condonacion = 0;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(APPAnRecepcionBtnPgoController.class);
    float UfDia;
    float ufAplica;
    GeneralAppJDBC _ggJDBC;
    int MaximoMesCastigo;
    
    private AdjuntarENC adjunta;
    private GlosaENC glosa;
    CondonadorJDBC _condonadorJDBC;
    int id_valor_regla_capital = 0;
    int id_valor_regla_Honorario = 0;
    int id_valor_regla = 0;
    int id_usrmod_int, id_usrmod_cap, id_usrmod_hon;
     
    OperacionJDBC _operJDBCsiscon;
    float InteresBanco = 0;
    float sumaMoraTotal;
    OperacionJDBC _operJDBC;
    String rutPruebaStringGlobal;
    List<SacaBop> ListSa ;
    List<DetalleCliente> ListHonos;
    List<CondonacionTabla2> ListRutsInter;

    //  EstadosJDBC _estados;
    public APPAnRecepcionBtnPgoController() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.cond_prod = new CondonacionImpl(mmmm.getDataSourceProduccion());
        this.MT = new MetodosGenerales();
        this.modulo = "AnalistRecepcion";
        this._periodo = new PeriodoJDBC(mmmm.getDataSource());
        this.metodo = new MetodosGenerales();
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceLucy());
         _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());
         _condonadorJDBC = new CondonadorJDBC(mmmm.getDataSource());
         this._operJDBCsiscon = new OperacionJDBC(mmmm.getDataSource(), "siscon");
         _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
         _prov = new ProvisionJDBC(mmmm.getDataSource());
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        session = Sessions.getCurrent();

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        detClientCond = (List<DetalleCliente>) sess.getAttribute("detClientCond");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();

//        windows = null;
        Locale.setDefault(new Locale("es", "CL"));
        //nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
//        listCondonaciones = this.cond.GetCondonacionesPendientesDeAplicacion(cuenta);
//        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);
//
//        grd_GridInfor.setModel(bandeCondTerminada);
        //BanEntrAnalista.renderAll();
        cargaPag();
        
        cargaPeriodo();

        //Herencia ente Adjuntar y Sacabop
        eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {
                listCondonaciones = cond.GetCondonacionesPendientesDeAplicacionBtnPgn(cuenta);
                bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

                grd_GridInfor.setModel(bandeCondTerminada);
                txt_filtra.setText("");
            }
        });

    }

    private void cargaPag() {

        try {
            sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
            sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

            eventos();
            
//            DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
//            String _per =dft.format(db0.getValue());
//            this.cargaBandeja(_per);
            
            lCondFinal = new ArrayList<CondonacionTabla2>();
            lCondFilter = new ArrayList<CondonacionTabla2>();
            lCondFinal = this.cond.GetCondonacionesPendientesDeAplicacionBtnPgn(cuenta);
            lCondFilter = lCondFinal;

            cargaGrid(lCondFilter);
            muestraCol();
        } catch (Exception e) {
            System.out.print(e);
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones cerradas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<CondonacionTabla2> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<CondonacionTabla2>(listCondonaciones);

        grd_InformeCond2.setModel(bandeCondTerminada);
        grd_GridInfor.setModel(bandeCondTerminada);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_GridInfor.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

        //        dbx_desde.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dDesdeLocal = dbx_desde.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
        //        dbx_hasta.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dHastaLocal = dbx_hasta.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
    }

    public void RepararCondonacion(int id_condonacion) {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("id_condonacion", id_condonacion);
        arguments.put("rut", 15.014544);
        String template = "Analista/AnReparoPoput.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAddeds", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");
                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("closeButton");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });
        Button ReparaDoc = (Button) window.getFellow("_idReparoDoc");

        ReparaDoc.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<CondonacionTabla2> filterGrid() {
        List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lCondFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lCondFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (CondonacionTabla2 cT2 : lCondFinal) {
                        String cadena = cT2.getCedente() + ";"
                                + cT2.getId_condonacion() + ";"
                                + cT2.getTimestap() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getUsuariocondona() + ";"
                                + cT2.getRegla() + ";"
                                + cT2.getTipocondonacion() + ";"
                                + cT2.getEstado() + ";"
                                + cT2.getDi_num_opers() + ";"
                                + cT2.getComentario_resna() + ";"
                                + cT2.getMonto_total_capitalS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_condonadoS().replace(".", "").replace(",", "") + ";"
                                + cT2.getMonto_total_recibitS().replace(".", "").replace(",", "") + ";"
                                + cT2.getRutcCliente();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

//        if (dDesdeLocal != dDesdeLocalResp) {
//            if (dDesdeLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.after(dDesdeLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
//
//        if (dHastaLocal != dHastaLocalResp) {
//            if (dHastaLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.before(dHastaLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    public void UpdateGridDoc() {
    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;
        String ced = null;
        String template;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }
        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        String rutclienteS= (coupleArray[2][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
              puedeEvaluar = cond.isCliente(rutclienteS);
        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        System.out.println("EXTRACTO DATOS--> "+rutcliente +" --- "+cuenta +" --- "+puedeEvaluar);
        ListDetOper = detoper.Cliente(rutcliente, cuenta,(puedeEvaluar == 2) ? 1 : 0);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        for (CondonacionTabla2 tC2 : listCondonaciones) {
            if (tC2.getId_condonacion() == idCondonacion) {
                ced = tC2.getCedente();
                break;
            }
        }

        if (ced.equals("PYME")) {
            template = "AnAplicacion/Poput/ZonalSacabopMostrar.zul";
        } else {
            template = "AnAplicacion/Poput/AnSacabopBotonPagoMostrar.zul";
        }

        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                // UpdateGridDoc();
                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void SacabogCondonacion(final int id_condonacion) {

        String ruuut = "14212287-1";
        Messagebox.show("Prueba fellow{" + windows.getParent().getParent().getFellows().toString() + "}");
        Include inc = (Include) windows.getParent().getParent().getFellow("pageref");
        inc.setSrc(null);
        inc.setSrc("Analista/AnSacabop.zul?rutcliente=" + ruuut);

    }

    public void ReglaCondonacion() {
        window = null;
        int id_operacion_documento = 0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        arguments.put("rut", 15.014544);
        String template = "Analista/AnRegla.zul";
        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });
        Button printButton = (Button) window.getFellow("btn_GenrarCondonacion");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void WsPjToken() throws Exception {

        NewJerseyClient _token = new NewJerseyClient();

        Messagebox.show("Imprimo TokenPJ:[" + _token.Token5().toString() + "]");

        Messagebox.show("Imprimo Valor UF de SBIF[" + _token.TokenUF() + "]");

    }

    public void MostrarReparos(Object[] aaa) {
        // TOS should be checked before accepting order

        window = null;

        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");
        }
        int idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        //arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);
        arguments.put("modulo", this.modulo);
        arguments.put("area_trabajo", this.AreaTrabajo);
        // Messagebox.show("FFFFFF{"+this.AreaTrabajo+"}");
//        MT.CalculaDv(rutcliente);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);
        // Messagebox.show("Idcondonacion["+idCondonacion+"] rutEjecutivo ["+rutejecutivo+"]");

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

        //Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListReparosCondonacionModel);
        //  arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        //  arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Analista/Poput/ReparosPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Listen("onClick=#id_refresch")
    public void actualizaGridsReparoPendientes() {

        ListReparosCondonacionModel = new ListModelList<Reparos>(this.cond.GetReparosXcondonacion(idCondonacion, this.AreaTrabajo));

    }
    
    
    public void cargaPeriodo() {

        try {
            lPeriodo.clear();
            lPeriodo = _periodo.getALLPeriodo();
            lCmb_Periodo = new ListModelList<Periodo>(lPeriodo);
//      

            cmb_Periodo.setModel(lCmb_Periodo);
            cmb_Periodo.applyProperties();
            cmb_Periodo.setVisible(false);
            cmb_Periodo.setVisible(true);
            cmb_Periodo.setPlaceholder("Seleccione Periodo.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los Bacnas. \nError:" + e.toString() + "\nContactar a área tecnica.");
        }

    }
    
    
    /*cambios exportar excel*/
    private void cargaBandeja(String Periodo) {

        try {
            // sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
            //sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

            //eventos();
//            lCondFinal = new ArrayList<ResumenCondonacionesInforme>();
//            lCondFilter = new ArrayList<ResumenCondonacionesInforme>();
//            lCondFinal = this.cond.GetCondonacionesAplicadasAndInformeV3(cuenta, Periodo);
//            lCondFilter = lCondFinal;
            
            lCondFinal = new ArrayList<CondonacionTabla2>();
            lCondFilter = new ArrayList<CondonacionTabla2>();
            lCondFinal = this.cond.GetCondonacionesPendientesDeAplicacionBtnPgnV3(cuenta,Periodo);
            lCondFilter = lCondFinal;
            
            System.out.println("***Periodo---> *** "+Periodo);
            
            if (lCondFinal.isEmpty()) {
                this.grd_GridInfor.setEmptyMessage("No existen Resultados para el Periodo : [" + Periodo + "]");
            }
            cargaGrid(lCondFilter);
            
            //muestraCol();
        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones aplicadas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }
    
    
    @Listen("onSelect = #cmb_Periodo")
    public void onSelect$cmb_Periodo(Event event) {
        cmb_Periodo.getSelectedIndex();
        String _per = cmb_Periodo.getValue();

        //   Messagebox.show("lperiodo : ["+lPeriodo.size()+"]   _per : ["+_per+"]");
        for (final Periodo lrf : lPeriodo) {
            if (lrf.getPeriodo().equals(_per)) {
                // txt_DesdeFecha.setValue(Integer.toString(lrf.getRango_ini()));
                // txt_HastaFecha.setValue(Integer.toString(lrf.getRango_fin()));
                // Messagebox.show("Cargamos Periodo :["+_per+"]");
                this.cargaBandeja(_per);

            }
        }

    }
    
     
    public void onSelectdb0() {
        System.out.println("db0.getText();+--> "+ db0.getValue());
       
        DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
	System.out.println("db0.getText();+-->2 "+dft.format(db0.getValue()));
         String _per =dft.format(db0.getValue());
         this.cargaBandeja(_per);
        
//        for (final Periodo lrf : lPeriodo) {
//            if (lrf.getPeriodo().equals(_per)) {
//                
//                this.cargaBandeja(_per);
//
//            }
//        }

    }
    
    
    public void exportListboxToExcel() throws Exception {
        System.out.println("LLEGA BIEN");
        ListModelList<SacaBop> sacaboj = new ListModelList<SacaBop>();
        List<SacaBop> ListSa = new ArrayList<SacaBop>();
        List<DetalleCliente> ListHonos = new ArrayList<DetalleCliente>();
        List<Map<String,List<SacaBop>>> listSaca1 = new ArrayList<Map<String,List<SacaBop>>>();
        List<Map<String,List<DetalleCliente>>> listHonos1 = new ArrayList<Map<String,List<DetalleCliente>>>();
        Map<String, List<SacaBop>> map1 = new HashMap<String, List<SacaBop>>();
        Map<String, List<DetalleCliente>> map2 = new HashMap<String, List<DetalleCliente>>();
        //sacaboj.clear();
        ListRutsInter = new ArrayList<CondonacionTabla2>();
        
        JSONObject mainObj = new JSONObject();
        JSONObject mainObjj = new JSONObject();
        
        //SE RECORRE GRID
        
        for (Component row : grd_GridInfor.getRows().getChildren()) {
                    int MaximoMesCastigoOnPrevio=0;
                    float totalA = 0;
                    float totalB = 0;
                    float totalC = 0;
                    float totalD = 0;
                    float totalE = 0;
                    float totalF = 0;
                    float totalG = 0;
                    float totalH = 0;
                    float totalI = 0;
                    JSONArray ji = new JSONArray();
                    JSONArray ja = new JSONArray();
                    Double SumaTotalVDEs = 0.0 ;
                    Double SumaTotalProvision=0.;
                    Double TotalTotal=0.0;
//                  DetalleClienteExcel detclex = new DetalleClienteExcel();
                    //Row row = (Row) arg0.getTarget();
                    Row row3 = (Row) row;
                    CondonacionTabla2 banc = (CondonacionTabla2) row3.getValue();
                    //System.out.println("FOR ROW "+banc.getOperacion());
                    String valueRutCliente = banc.getRutcCliente();
                    String valueEstadoCliente = banc.getEstado();
                    String valueCedenteCliente = banc.getCedente();
                    int valueIdCliente = banc.getId_condonacion();
                    int valueIdCond = banc.getId_condonacion();
                    String valueFechaCliente = banc.getTimestap();
                    Component cC = row.getChildren().get(1);
                    Boolean rowSelected = (Boolean) row.getAttribute("Selected");

                    //Capturo todos los atributos de llegada
                    String nnnnn = row.getAttributes().toString();
                    System.out.println("****VvalueRutCliente--> "+valueIdCond+" "+valueRutCliente);
                    ListRutsInter.add(banc);
//                    ListDetOper.clear();
//                    ListDetOperModel.clear();
                    detClientSacabop.clear();
                   // ListJudClienteModel.clear();
                    //INICIA 
                    //16325416 --- Hroa --- 1
                    //15835752 --- Hroa --- 1
                    
                    String string = valueRutCliente;
                    String[] parts = string.split("-");
                    String part1 = parts[0]; // 123
                    String part2 = parts[1]; // 654321
                    
                    
                    this.listProvisiones = this._prov.listProvisionXrut(valueRutCliente);
                    for (UsrProvision prov : listProvisiones) {

                    //  listModelHono.add(detCliHono);
                    SumaTotalProvision = SumaTotalProvision + (float) prov.getMonto();

                    }
                    
                    
        int rutPrueba = Integer.parseInt(part1);//15835752;
        int rutPruebaEjecutivo=14212287;
        char uuuu = MT.CalculaDv(rutPrueba);
        String rutPruebaString=rutPrueba+"-"+uuuu;
        rutPruebaStringGlobal=rutPruebaString;
        
        int idCOnPrueba=valueIdCond;//63976;
        condonacion=idCOnPrueba;
        
        
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
       List<DetalleCliente> ListDetOper = detoper.Cliente(rutPrueba, cuenta,1);//cambiar por rut cliente
       ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        
         _condetoper = this.cond.getListDetOperCondonacion(idCOnPrueba);
         String Operaciones2 = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones2 = Operaciones2 + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
      ListModelList<DetalleCliente>  ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);


        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);
                ListHonos.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }
            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
//        for (DetalleCliente detCli : ListDetOperModel) {
//            String Tcedente = detCli.getCedente();
//            String codOpe = detCli.getOperacion();
//            String co1 = detCli.g
//            System.out.println(Tcedente +" -- "+codOpe);
//            
//        }
        
        //sacaboj.add(temp);
        
        
        /*
        detalle honorarios
        */
        
        
        //// calculo de suma vdes

        for (DetalleCliente detCliHono : ListDetOper) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {

                listModelHono.add(detCliHono);
                ListHonos.add(detCliHono);
                JSONObject ju = new JSONObject();
                SumaTotalVDEs = SumaTotalVDEs +  detCliHono.getSaldoinsoluto();
              
                System.out.println("Cedente-> "+detCliHono.getCedente());
                System.out.println("*****");
                ju.put("Cedente", detCliHono.getCedente());
                ju.put("CodigoOperacion", detCliHono.getTipoCedente());
                ju.put("OperacionOriginal", detCliHono.getOperacion());
                ju.put("Mora", detCliHono.getMoraEnPesosChileno());
                ju.put("SaldoInsoluto", detCliHono.getSaldoEnPesosChileno());
                ju.put("DiasMora", detCliHono.getDiasMora());
                ju.put("Producto", detCliHono.getProductos());
                ju.put("FecVenci", detCliHono.getFechaFencimiento());
                ju.put("FecCastigo", detCliHono.getFechaCastigo());
                
                ji.add(ju);
                mainObjj.put("Hono_" + valueRutCliente, ji);
            }
        }
       // grd_GastosHono.setModel(listModelHono);
        
       
        
        
        
        
        //Grid_SacabopXX
        glosa = new GlosaENC();
        adjunta = new AdjuntarENC();
        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        int ii = 0;

        List<String> Operaciones = new ArrayList<String>();
        // Messagebox.show("FloatUF [+"+this.UfDia+"+]");
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;
        if (detClientCond.size() > 0) {

            for (DetalleCliente seleccion : detClientCond) {

                if (!detClientSacabop.contains(seleccion)) {
                    Operaciones.add(seleccion.getOperacion());
                    detClientSacabop.add(seleccion);

                }

            }
        }

        ListDetOper.clear();
        ListDetOper = detClientSacabop;

        RutClienteFormateado = rutPrueba;
        List<JudicialCliente> ListJudCliente = JudCliente.JudClie(rutPrueba, cuenta);
        InfoCliente = clienteinfo.infocliente(Integer.parseInt(String.valueOf(rutPrueba)));

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
        ListModelList<JudicialCliente> ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

        //sacaboj = new ListModelList<SacaBop>();
        
        
        String oper = "NULL";
        String MeseCatigo = "NULL";
        String FechaCastigo = "14-01-2012 09:29:58";
        //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = this.cond.FechaCondonacion(this.condonacion); //new Date();
         logger.info("#########d2 602  - {}   sql_peridocond{}",d2.toString());
        String[] parse;
        //d2=format.parse(d2.toString());
        DateTime NN;
        // creamos instancia de condonacion      
String OO="";
System.out.println("RUT DEL CLIENTE --> "+rutPruebaString);
        CurrentCondonacion = new Condonacion(rutPruebaString, cuenta, sess.getWebApp().toString());

        /// Busqueda de Fecha Castigo mas Antigua
        //////##### AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            if (ListDetOperModel.get(i) != null) {
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                OO=OO+"------------"+ddddd;
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                System.out.println("--------@@@MaximoMesCastigoOnPrevio1 "+d1);
                long diff = d2.getTime() - d1.getTime();
                System.out.println("--------@@@MaximoMesCastigoOnPrevio2 "+diff);
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                System.out.println("--------@@@MaximoMesCastigoOnPrevio3 "+diffMonths);
                MeseCatigo = Long.toString(diffMonths);
                System.out.println("--------@@@MaximoMesCastigoOnPrevio4 "+MeseCatigo);
                // encontramos el mes de castigo mas antiguo
                System.out.println("--------@@@MaximoMesCastigoOnPrevio5 "+MaximoMesCastigoOnPrevio);
                if ((int) diffMonths > MaximoMesCastigoOnPrevio) {
                    MaximoMesCastigoOnPrevio = (int) diffMonths;
                    System.out.println("--------@@@MaximoMesCastigoOnPrevio "+MaximoMesCastigoOnPrevio);
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(MaximoMesCastigoOnPrevio);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
            }
        }
     //  Messagebox.show("MaximoMesCastigoOnPrevio-:{"+MaximoMesCastigoOnPrevio+"} MeseCatigo:{"+MeseCatigo+"}d2 :{"+d2+"}d1:["+d1+"] FechaCastigo: {"+FechaCastigo+"+    OO :{"+OO+"}");
        CurrentCondonacion.setAdjuntar(cond.GetAdjuntoCond(condonacion));
        adjunta = CurrentCondonacion.getAdjuntar();
        CurrentCondonacion.setGlosa(cond.GetGlosaCond(condonacion));
        glosa = CurrentCondonacion.getGlosa();

        //////##### FIN-AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        //////######   AQUI SE BUSCA Y GUARDA LA INFORMACION DEL EJECUTIVO####///////
        //CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsulting(rut_ejecutivo, this.cuenta, this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));
//        14212287
//        bot-btnPago
//        37
//        63976
        String cuenta2 = "bot-btnPago";
        System.out.println("-->>>DATOSSSSSSSSSSSSSS ");
        System.out.println("***RUT EJECUTIVO ESTE--->>> "+rutPruebaEjecutivo);
         System.out.println(cuenta2);
          System.out.println(       CurrentCondonacion.getNumeroMesesPrimerCastigo());
          System.out.println(               this.condonacion);
        CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsultingV4(rutPruebaEjecutivo, cuenta2, CurrentCondonacion.getNumeroMesesPrimerCastigo(), condonacion));

        this.CurrentCondonacion.setMontoMaximoAtribucionEjecutiva(CurrentCondonacion._condonador.getAtribucionMaxima());
        this.CurrentCondonacion.setMontoAtribucionMaxima(CurrentCondonacion._condonador.getAtribucionMaxima());
       float ppppppp=0;
       float pppp3=0;
       float pppp2=0;
       
        for (final Regla _regla : this.CurrentCondonacion._condonador._reglaList) {
            //list2.add(tipodocto.getDv_NomTipDcto());
            if (_regla.getDesTipoValor().equals("Monto Capital")) {
                ppppppp = _regla.getPorcentajeCondonacion100();
                id_valor_regla_capital = _regla.getIdRegla();
                // id_usrmod_1=_regla.get
            }
            if (_regla.getDesTipoValor().equals("Interes")) {
                pppp2 = _regla.getPorcentajeCondonacion100();
                id_valor_regla = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Honorario Judicial")) {
                pppp3 = _regla.getPorcentajeCondonacion100();
                id_valor_regla_Honorario = _regla.getIdRegla();
            }

        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActual() >= 0) {
            pppp2 = this.CurrentCondonacion._condonador.getF_PorcentajeActualInt();
            id_usrmod_int = this.CurrentCondonacion._condonador.getId_usr_modif_int();
             System.out.println("------@@@@@ pppp2--> "+pppp2);

        }
        if (this.CurrentCondonacion._condonador.getPorcentajeActualCapital() >= 0) {
            ppppppp = this.CurrentCondonacion._condonador.getF_PorcentajeActualCap();
            id_usrmod_cap = this.CurrentCondonacion._condonador.getId_usr_modif_cap();
             System.out.println("------@@@@@ ppppppp--> "+ppppppp);
        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActualHonorario() >= 0) {
            pppp3 = this.CurrentCondonacion._condonador.getF_PorcentajeActualHon();
            id_usrmod_hon = this.CurrentCondonacion._condonador.getId_usr_modif_hon();
             System.out.println("------@@@@@ pppp3--> "+pppp3);

        }
        
        System.out.println("------@@@@@ CurrentCondonacion reglaCapitalPorcentajeCndonacion--> "+ppppppp);
        
        reglaCapitalPorcentajeCndonacion = (float) ((float) ppppppp / (float) 100);
        ReglaInteresPorcentajeCondonacion = (float) ((float) pppp2 / (float) 100);
        ReglaHonorarioPorcentajeCondonacion = (float) ((float) pppp3 / (float) 100);
        String rangoFechaInicio = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRanfoFInicio());
        String rangoFechaFin = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRangoFFin());

        System.out.println("------@@@@@ CurrentCondonacion setPorcentajeCondonaCapital--> "+reglaCapitalPorcentajeCndonacion);
        
        this.CurrentCondonacion.setPorcentajeCondonaCapital(reglaCapitalPorcentajeCndonacion);
        this.CurrentCondonacion.setPorcentajeCondonaHonorario(ReglaHonorarioPorcentajeCondonacion);
        this.CurrentCondonacion.setPorcentajeCondonaInteres(ReglaInteresPorcentajeCondonacion);
        this.CurrentCondonacion.setRangoFechaInicio(rangoFechaInicio);
        this.CurrentCondonacion.setRangoFehaFin(rangoFechaFin);
        this.CurrentCondonacion.setPuedeCondonarEnLinea(false);
        this.CurrentCondonacion.setNumeroDeOperaciones(ListDetOper.size());
        
        System.out.println("***TAMAÑO ARREGLO --> "+ListDetOperModel.getSize());
        
        for(DetalleCliente sd:ListDetOperModel){
           System.out.println("***Operasdasdsad --> "+sd.getOperacion());
        }
        
        
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            SacaBop temp = new SacaBop();
            JSONObject jo = new JSONObject();
            Map<String,String> gfg = new HashMap<String,String>(); 
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setDetalleCredito(ListDetOperModel.get(i).getDetalleCredito());
                temp.setCedente(ListDetOperModel.get(i).getCedente());
                temp.setFechaCastigo(ListDetOperModel.get(i).getFechaCastigo());
                temp.setFechaFencimiento(ListDetOperModel.get(i).getFechaFencimiento());
                temp.setDiasMora(ListDetOperModel.get(i).getDiasMora());
                temp.setMarcaRenegociado(ListDetOperModel.get(i).getMarcaRenegociado());

                //  Calculo del Juicio Activo Para el Conbro de Honorarios Judiciales
                for (int j = 0; j < ListJudClienteModel.getSize(); j++) {
                    String OperacionJud = ListJudClienteModel.get(j).getOperacion() != null ? ListJudClienteModel.get(j).getOperacion() : "0";
                    String OperacionDet = ListDetOperModel.get(i).getOperacionOriginal() != null ? ListDetOperModel.get(i).getOperacionOriginal() : "0";
                    int nnnn = OperacionJud.compareTo(OperacionDet);

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);
                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneJuicio(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneJuicio(false);
                    }

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);

                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneRol(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneRol(false);
                    }

                }

                ////* Fin Juicio Activo
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                System.out.println("#------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                temp.setOperacion(oper);
                temp.setMesesCastigo(MeseCatigo);

                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigo) {
                    this.MaximoMesCastigo = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigo);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
                totalA = (float) totalA + (float) ListDetOperModel.get(i).getSaldoinsoluto();

                temp.setCapital(ListDetOperModel.get(i).getSaldoEnPesosChileno());

                // verificar si el usuario ha ingresado otro interes personalizado
                float interes = 0;
                float montoInteresAjustado = this._operJDBCsiscon.getProcentajeInteresActual(this.RutClienteFormateado, cuenta2, oper, this.condonacion);//fijarse cuenta

                if (montoInteresAjustado >= 0) {
                    interes = montoInteresAjustado;

                } else {
                    interes = (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                }

                InteresBanco = InteresBanco + (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                sumaMoraTotal = sumaMoraTotal + (float) ListDetOperModel.get(i).getMora();
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                float porcentaje_condonacion = (float) this.CurrentCondonacion.getPorcentajeCondonaCapital();
                System.out.println("**********@@@@@@@@@@@@@@@@@@@@@@@@@@@valueRutCliente  "+valueRutCliente);
                System.out.println("**********@@@@@@@@@@@@@@@@@@@@@@@@@@@porcentaje_condonacion valueRutCliente  "+porcentaje_condonacion);
                float porcentaje_condonacion_Interes = (float) this.CurrentCondonacion.getPorcentajeCondonaInteres();
                System.out.println("**********@@@@@@@@@@@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getSaldoinsoluto()  "+ListDetOperModel.get(i).getSaldoinsoluto());
                float capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion;
                
                System.out.println("**********@@@@@@@@@@@@@@@@@@@@@@@@@@@CAPITAL CONDONADO "+capital_condonado);
                totalB = (float) (totalB + capital_condonado);
                float capitalarecibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;
                totalC = (float) (capitalarecibir + totalC);

                /// agregagos el monto a recibir a la clase condonacion
                this.CurrentCondonacion.setMontoARecibir(capitalarecibir);

                float interes_condonado = interes * porcentaje_condonacion_Interes;
                float interesarecibir = interes - interes_condonado;

                totalD = (float) (interes + totalD);
                totalE = (float) (interes_condonado + totalE);
                totalF = (float) (interesarecibir + totalF);

                // se agrega el monto a condonar a la clase sacabop
                temp.setMontoCondonar((long) capital_condonado);
                temp.setMontoCondonarPesos(nf.format(capital_condonado).replaceFirst("Ch", ""));
                temp.setCapitalARecibirPesos(nf.format(capitalarecibir).replaceFirst("Ch", ""));

                temp.setMontoARecibir((long) capitalarecibir);
                float porcentaje_honorjud = 0;
                float honorariojud = 0;
                float honorariojud2 = 0;
                float honorariojud_sobrecondonado = 0;
                float Honor2 = 0;
                float capital_a_recibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;

                // 
                //  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;

                    float c = ((float) this.ufAplica);
                    float a = ((float) capital_a_recibir);
                    float b = (((float) 10 * c));

                    if (a <= b) {

                        honorariojud_sobrecondonado = (a * (float) 0.09);
                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud_sobrecondonado = (hh + ww3);

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) capital_a_recibir;
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud_sobrecondonado = (float) capital_a_recibir * (float) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir && capital_a_recibir <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) capital_a_recibir;
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);
                        honorariojud_sobrecondonado = (hh + ww3);
                    } else if ((float) capital_a_recibir > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) capital_a_recibir;
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;

                    float c = Math.round((float) this.ufAplica);
                    float a = Math.round((float) ListDetOperModel.get(i).getSaldoinsoluto());
                    float b = Math.round(((float) 10 * c));

                    if (a <= b) {

                        honorariojud = Math.round(a * 0.09);

                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud = Math.round(hh + ww3);

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) 0.15;

                    } else if ((500 * this.ufAplica) < ListDetOperModel.get(i).getSaldoinsoluto() && ListDetOperModel.get(i).getSaldoinsoluto() <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);

                        honorariojud = Math.round(hh + ww3);
                    } else if ((float) ListDetOperModel.get(i).getSaldoinsoluto() > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                }

                //  el calculo de los honorarios judiciales esta compuesto del CalculaMontoJudicial(monto a recibir)
                // double honorarioJudCondonado = (double) honorariojud * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudCondonado = (double) honorariojud * (double) ReglaHonorarioPorcentajeCondonacion;

                float honorarioJudReibido = (float) honorariojud - (float) honorarioJudCondonado;

                // double honorarioJudCondonado2 = (double) honorariojud_sobrecondonado * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                float honorarioJudCondonado2 = (float) honorariojud_sobrecondonado * (float) ReglaHonorarioPorcentajeCondonacion;
                float honorarioJudReibido2 = (float) honorariojud_sobrecondonado - (float) honorarioJudCondonado2;

                //Calculo de Totales para la Grilla
                // se muestra sobre el total capital condonado
                totalG = (float) (honorariojud_sobrecondonado + totalG);
                totalH = (float) (honorarioJudCondonado2 + totalH);
                totalI = (float) (honorarioJudReibido2 + totalI);

                // Grid-Column Judicial 
                temp.setHonorarioJuducial((float) honorariojud);

                temp.setHonorarioJudicial2((float) honorariojud_sobrecondonado);

                //Montos en pesos
                String valorPesosrecibe = nf.format(honorariojud).replaceFirst("Ch", "");
                temp.setHonorarioJudicialPesos(valorPesosrecibe);
                temp.setHonorarioJudicialCondonadoPesos(nf.format(honorarioJudCondonado2).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialRecibidoPesos(nf.format(honorarioJudReibido2).replaceFirst("Ch", ""));

                temp.setInteresCondonadoPesos(nf.format(interes_condonado).replaceFirst("Ch", ""));
                temp.setInteresARecibirPesos(nf.format(interesarecibir).replaceFirst("Ch", ""));

                temp.setInteres(nf.format(interes).replaceFirst("Ch", ""));

                //sacaboj.add(temp);
                //ListSa.add(temp);
                /*Pruebas*/
                System.out.println("****VARIABLES****");
                System.out.println(temp.getOperacion());
                System.out.println(temp.getMesesCastigo());
                System.out.println(temp.getCapital());
                System.out.println(temp.getMontoCondonarPesos());
                System.out.println(temp.getCapitalARecibirPesos());
                System.out.println(temp.getInteres());
                System.out.println(temp.getInteresCondonadoPesos());
                System.out.println(temp.getInteresARecibirPesos());
                System.out.println(temp.getHonorarioJudicial2Pesos());
                System.out.println(temp.getHonorarioJudicialCondonadoPesos());
                System.out.println(temp.getHonorarioJudicialRecibidoPesos());
                System.out.println("****FIN VARIABLES****");
                
                jo.put("Operacion", temp.getOperacion());
                jo.put("Meses_Castigo", temp.getMesesCastigo());
                jo.put("Capital", temp.getCapital());
                jo.put("MontoCondPesos", temp.getMontoCondonarPesos());
                jo.put("CapitalRecibirPesos", temp.getCapitalARecibirPesos());
                jo.put("Interes", temp.getInteres());
                jo.put("InteresCondonadoPesos", temp.getInteresCondonadoPesos());
                jo.put("InteresRecibidoPesos", temp.getInteresARecibirPesos());
                jo.put("HonorarioJudicialPesos", temp.getHonorarioJudicial2Pesos());
                jo.put("HonorarioJudicialCondonadoPesos", temp.getHonorarioJudicialCondonadoPesos());
                jo.put("HonorarioJudicialRecibidoPesos", temp.getHonorarioJudicialRecibidoPesos());
               
//                gfg.put("Operacion", temp.getOperacion());
//                gfg.put("Meses_Castigo", temp.getMesesCastigo());
//                gfg.put("Capital", temp.getCapital());
//                gfg.put("MontoCondPesos", temp.getMontoCondonarPesos());
//                gfg.put("CapitalRecibirPesos", temp.getCapitalARecibirPesos());
//                gfg.put("Interes", temp.getInteres());
//                gfg.put("InteresCondonadoPesos", temp.getInteresCondonadoPesos());
//                gfg.put("InteresRecibidoPesos", temp.getInteresARecibirPesos());
//                gfg.put("HonorarioJudicialPesos", temp.getHonorarioJudicial2Pesos());
//                gfg.put("HonorarioJudicialCondonadoPesos", temp.getHonorarioJudicialCondonadoPesos());
//                gfg.put("HonorarioJudicialRecibidoPesos", temp.getHonorarioJudicialRecibidoPesos());
               ja.add(jo);
               
               //jo.clear();
            } else {
                oper = "SOYNULL";
            }

            System.out.println("#------------@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        }
              //sacaboj2=sacaboj;      
              //listModelHono2 = listModelHono; 
               
//              map1.put(valueRutCliente,ListSa);
//                listSaca1.add(map1);
//              
//              List<Map<String,List<SacaBop>>> listSaca1D = listSaca1;
             TotalTotal=SumaTotalProvision + SumaTotalVDEs + totalC + totalF + totalI;
            mainObj.put("Rut_" + valueRutCliente+"_"+valueEstadoCliente+"_"+valueCedenteCliente+"_"+valueIdCliente+"_"+valueFechaCliente+"_"+SumaTotalVDEs+"_"+SumaTotalProvision+"_"+TotalTotal, ja);
           // mainObj.put("Honorarios", ji);
            
            System.out.println("JSON --> " + mainObj.toString());
            System.out.println("****FIN VFR****");
            System.out.println("MAINOBJJ--> "+mainObjj.toString());
            

        }
        
//         NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());
//         String TotalSumaCapital = nff.format(totalA).replaceFirst("Ch", "");
//         String TotalSumaCondonaCapital = nff.format(totalB).replaceFirst("Ch", "");
//         String TotalSumaRecibeCapital = nff.format(this.totalC).replaceFirst("Ch", "");
//         String TotalSumaInteres = nff.format(this.totalD).replaceFirst("Ch", "");
//         String TotalSumaCondonaInteres = nff.format(this.totalE).replaceFirst("Ch", "");
//         String TotalSumaRecibeInteres = nff.format(this.totalF).replaceFirst("Ch", "");
//         String TotalSumaHonorJud = nff.format(this.totalG).replaceFirst("Ch", "");
//         
//         System.out.println("****TOTALES****");
//         System.out.println(TotalSumaCapital);
//         System.out.println(TotalSumaCondonaCapital);
//         System.out.println(TotalSumaRecibeCapital);
//         System.out.println(TotalSumaInteres);
//         System.out.println(TotalSumaCondonaInteres);
//         System.out.println(TotalSumaRecibeInteres);
//         System.out.println(TotalSumaHonorJud);
        
        //customersToExcel(ListSa,ListHonos);
//        for (Object keyStr : mainObj.keySet()) {
//            JSONArray keyvalue = (JSONArray) mainObj.get(keyStr);
//            JSONArray mainObjs = new JSONArray();
//            //Print key and value
//            System.out.println("key: " + keyStr + " value: " + keyvalue);
//
////            for (Object keyvalueS : keyvalue) {
////                JSONObject keyvalued = (JSONObject) keyvalueS;
////                System.out.println("keyvalued--> "+keyvalued);
////            }
//        JsonParser parser = new JsonParser();
//        JsonArray gsonArr = parser.parse(keyvalue.toString()).getAsJsonArray();
//        System.out.println("EXTRACTO DE CONDONACION PARA: "+keyStr);
//         for (JsonElement obj : gsonArr) {
//             JsonObject gsonObj = obj.getAsJsonObject();
//              String Operacion = gsonObj.get("Operacion").getAsString();
//              String MesesCastigo = gsonObj.get("Meses_Castigo").getAsString();
//              String Capital =   gsonObj.get("Capital").getAsString();
//              String MontoCondPeso =   gsonObj.get("MontoCondPesos").getAsString();
//              String CapitalRecibPesos =   gsonObj.get("CapitalRecibirPesos").getAsString();
//              String Interes = gsonObj.get("Interes").getAsString();
//              String InteresCondonadoPeso = gsonObj.get("InteresCondonadoPesos").getAsString();
//              String InteresRecibidoPeso =   gsonObj.get("InteresRecibidoPesos").getAsString();
//              String HonorarioJudiPeso =   gsonObj.get("HonorarioJudicialPesos").getAsString();
//              String HonorarioJudiCondPeso =   gsonObj.get("HonorarioJudicialCondonadoPesos").getAsString();
//              String HonorarioJudRecPeso =   gsonObj.get("HonorarioJudicialRecibidoPesos").getAsString();
//                
//              System.out.println("**************************");  
//              System.out.println("Operacion:-> "+Operacion);
//              System.out.println("Meses Castigo:-> "+MesesCastigo);
//              System.out.println("Capital:-> "+Capital);
//              System.out.println("MontoCondPeso:-> "+MontoCondPeso);
//              System.out.println("CapitalRecibPeso:-> "+CapitalRecibPesos);
//              System.out.println("Interes:-> "+Interes);
//              System.out.println("InteresCondonadoPeso:-> "+InteresCondonadoPeso);
//              System.out.println("InteresesRecibidoPeso:-> "+InteresRecibidoPeso);
//              System.out.println("HonorarioJudiPeso:-> "+HonorarioJudiPeso);
//              System.out.println("HonorarioJudiCOndPeso:-> "+HonorarioJudiCondPeso);
//              System.out.println("HonorarioJudRecPeso:-> "+HonorarioJudRecPeso);
//              
//         }
//            
//        }
        
        if(mainObj.size()==1){
            //customersToExcel(ListSa,ListHonos);
            customersToExcelV3(mainObj,mainObjj);
        }else{
            //customersToExcelV2(mainObj);
            customersToExcelV3(mainObj,mainObjj);
        }
           
            
    }
    
    public void customersToExcelV3(JSONObject mainObj,JSONObject mainObjj) throws IOException, ParseException {
         String[] COLUMNs = { "Rut","Operacion","MesesCastigo", "Capital", "Capital A Condonar", "Capital A Recibir", "Interés", "Interés Cond.", "Interés A Recibir", "Hon. Jud.",
				"Hon. Jud. Cond.", "Hon. Jud Recibir"};
                
                String[] COLUMNs2 = { "Rut","OperaciónOriginal","DíasMora", "Mora", "CódigoOperación", "SaldoInsoluto", "Cedente", "Producto", "F.Vcmto", "F.Castigo"};
                
                //HEADERS SHEET2
                String[] COLUMNsh2 = { "Cedente","Id","Fecha", "RutCliente", "Estado"};
                String[] COLUMNsh3 = { "Capital", "Capital a Condonar", "Capital a Recibir", "Interes", "Interes Cond.",
                                        "Interes a Recibir","Hon.Jud","Hond.Jud.Cond","Hond.Jud.Recibir"};
                String[] COLUMNsh5 = { "VDE/SGN","Provision","Total Recibe"};
                
                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
             
                        Workbook workbook = new XSSFWorkbook(); 
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("DatosCarga");
                        Sheet sheet2 = workbook.createSheet("Resumen");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
                        
                        DataFormat format = workbook.createDataFormat();
                        

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
                        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        CellStyle headerCellStyleInter = workbook.createCellStyle();
			headerCellStyleInter.setFont(headerFont);
                        headerCellStyleInter.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                        headerCellStyleInter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        CellStyle headerCellStyleResu = workbook.createCellStyle();
			headerCellStyleResu.setFont(headerFont);
                        headerCellStyleResu.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
                        headerCellStyleResu.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        
                        XSSFColor orangePast =new XSSFColor(new java.awt.Color(227, 141, 30));
                        CellStyle headerCellDet = workbook.createCellStyle();
			//headerCellDet.setFont(headerFont);
                        //headerCellDet.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
                        //headerCellDet.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        headerCellDet.setDataFormat(format.getFormat("#,##0"));
                        
                        CellStyle headerCellDet2 = workbook.createCellStyle();
			//headerCellDet.setFont(headerFont);
                        headerCellDet2.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
                        headerCellDet2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        CellStyle headerCellHono = workbook.createCellStyle();
			//headerCellDet.setFont(headerFont);
                        //headerCellHono.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);
                        //headerCellHono.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        headerCellHono.setDataFormat(format.getFormat("#,##0"));
                        
                        
                        CellStyle headerCellHono2 = workbook.createCellStyle();
			//headerCellDet.setFont(headerFont);
                        headerCellHono2.setFillForegroundColor(HSSFColor.LIGHT_ORANGE.index);
                        headerCellHono2.setFillPattern(FillPatternType.SOLID_FOREGROUND);


                  
                        //sheet2
                        
                         org.apache.poi.ss.usermodel.Row headerRowSheet2 = sheet2.createRow(0);
                            for (int colSH = 0; colSH < COLUMNsh2.length; colSH++) {
				Cell cell25 = headerRowSheet2.createCell(colSH);
				cell25.setCellValue(COLUMNsh2[colSH]);
				cell25.setCellStyle(headerCellStyle);
                                
                                
                            }
                         int colJ1=5;  
                         //org.apache.poi.ss.usermodel.Row headerRowSheet23 = sheet2.createRow(0);
                                for (int colJ = 0; colJ < COLUMNsh3.length; colJ++) {
				Cell cell23 = headerRowSheet2.createCell(colJ1);
				cell23.setCellValue(COLUMNsh3[colJ]);
				cell23.setCellStyle(headerCellStyleInter);
                                colJ1++;
			}
                                
                         int colJ2=14;  
                         //org.apache.poi.ss.usermodel.Row headerRowSheet23 = sheet2.createRow(0);
                                for (int colJ23 = 0; colJ23 < COLUMNsh5.length; colJ23++) {
				Cell cell23 = headerRowSheet2.createCell(colJ2);
				cell23.setCellValue(COLUMNsh5[colJ23]);
				cell23.setCellStyle(headerCellStyleResu);
                                colJ2++;
			}       
//                                Cell cellSheet2 = headerRowSheet2.createCell(0);
//				cellSheet2.setCellValue("Cedente");
//				cellSheet2.setCellStyle(headerCellStyleInter);
//                                
//                                Cell cellSheet3 = headerRowSheet2.createCell(1);
//				cellSheet3.setCellValue("Id");
//				cellSheet3.setCellStyle(headerCellStyleInter);
                        
                        
  
                        int cont=0;
                       int contSheet2=1;
                       for (Object keyStr : mainObj.keySet()) {
                           
                       ArrayList<Double> TotalSumaCapital = new ArrayList<Double>(); 
                       ArrayList<Double> TotalSumaCondonaCapital = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaRecibeCapital = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaInteres = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaCondonaInteres = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaRecibeInteres = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaHonorJud = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaHonoJudCond = new ArrayList<Double>();
                       ArrayList<Double> TotalSumaHondJudRecibir = new ArrayList<Double>();
                       
                      
                       String rutStringCliente="";
                       String estadoStringCliente="";
                       String cedenteStringCliente="";
                       String idCondStringCliente="";
                       String fecCliStringCliente="";
                       String vDEsStringCliente="";
                       String totalProvision="";
                       String TotalTotalStringCliente="";
                       //mainObj.put("Rut_" + valueRutCliente+"_"+valueEstadoCliente+"_"+valueCedenteCliente+"_"+valueIdCliente+"_"+valueFechaCliente, ja);
                      
                        if(cont==0){
                            cont=0;
                        }else{
                            cont = cont;
                        }
                        System.out.println("cont--> "+cont);
                        org.apache.poi.ss.usermodel.Row headerRowInter = sheet.createRow(cont);
                        
                        String string4rut = keyStr.toString();
                        String[] parts24rut = string4rut.split("_");
                        String part12parts24rut = parts24rut[0]; // 123
                        String part22parts24rut = parts24rut[1]; // 654321
                        
				Cell cell = headerRowInter.createCell(0);
				cell.setCellValue("Extracto Condonacion Para: "+part22parts24rut);
				cell.setCellStyle(headerCellStyleInter);
                                
		
                         
			// Row for Header
                        
                        
                        cont=cont+1;
			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(cont);
                        int contAux =cont+1;
                        System.out.println("contAux"+contAux);
                        

			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell2 = headerRow.createCell(col);
				cell2.setCellValue(COLUMNs[col]);
				cell2.setCellStyle(headerCellStyle);
                                
			}

                        for (int columnIndex = 0; columnIndex < 50; columnIndex++) {
                        sheet.autoSizeColumn(columnIndex);
                        sheet2.autoSizeColumn(columnIndex);
                     }   
                          // Formato campos numericos
			CellStyle ageCellStyle = workbook.createCellStyle();
			ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#")); 
                           
                        JSONArray keyvalue = (JSONArray) mainObj.get(keyStr);
                        
                        //Print key and value
                        System.out.println("key: " + keyStr + " value: " + keyvalue);

                        JsonParser parser = new JsonParser();
                        JsonArray gsonArr = parser.parse(keyvalue.toString()).getAsJsonArray();
                        
                        
                        for (JsonElement obj : gsonArr) {
                         
                        Double intTotalSumaCapital;
                        Double intTotalSumaCondonaCapital;
                        Double intTotalSumaRecibeCapital;
                        Double intInteres;
                        Double intInteresCondonadoPeso;
                        Double intInteresRecibidoPeso;
                        Double intTotalSumaHonorJud;
                        Double intTotalSumaHonoJudCond;
                        Double intTotalSumaHondJudRecibir;
                            
                        JsonObject gsonObj = obj.getAsJsonObject();
                        //String Operacion = gsonObj.get("Operacion").getAsString();
                        String Operacion = gsonObj.get("Operacion").toString();
                        //String MesesCastigo = gsonObj.get("Meses_Castigo").getAsString();
                        String MesesCastigo = gsonObj.get("Meses_Castigo").toString();
                        //String Capital =   gsonObj.get("Capital").getAsString();
                        String Capital =   gsonObj.get("Capital").toString();
                        //String MontoCondPeso =   gsonObj.get("MontoCondPesos").getAsString();
                        String MontoCondPeso =   gsonObj.get("MontoCondPesos").toString();
                        //String CapitalRecibPesos =   gsonObj.get("CapitalRecibirPesos").getAsString();
                        String CapitalRecibPesos =   gsonObj.get("CapitalRecibirPesos").toString();
                        //String Interes = gsonObj.get("Interes").getAsString();
                        String Interes = gsonObj.get("Interes").toString();
                        //String InteresCondonadoPeso = gsonObj.get("InteresCondonadoPesos").getAsString();
                        String InteresCondonadoPeso = gsonObj.get("InteresCondonadoPesos").toString();
                        //String InteresRecibidoPeso =   gsonObj.get("InteresRecibidoPesos").getAsString();
                        String InteresRecibidoPeso =   gsonObj.get("InteresRecibidoPesos").toString();
                        //String HonorarioJudiPeso =   gsonObj.get("HonorarioJudicialPesos").getAsString();
                        String HonorarioJudiPeso =   gsonObj.get("HonorarioJudicialPesos").toString();
                        //String HonorarioJudiCondPeso =   gsonObj.get("HonorarioJudicialCondonadoPesos").getAsString();
                        String HonorarioJudiCondPeso =   gsonObj.get("HonorarioJudicialCondonadoPesos").toString();
                        //String HonorarioJudRecPeso =   gsonObj.get("HonorarioJudicialRecibidoPesos").getAsString();
                        String HonorarioJudRecPeso =   gsonObj.get("HonorarioJudicialRecibidoPesos").toString();
                        
                        /*Validaciones*/
                        if(Operacion.equals(null) || Operacion.equals("null") || Operacion == "null"){
                                Operacion="Sin Registro";
                            }
                        Operacion = Operacion.replace("\"", "");
                        
                        if(MesesCastigo.equals(null) || MesesCastigo.equals("null") || MesesCastigo == "null"){
                                MesesCastigo="Sin Registro";
                            }
                        MesesCastigo = MesesCastigo.replace("\"", "");
                        
                        if(Capital.equals(null) || Capital.equals("null") || Capital == "null"){
                                Capital="Sin Registro";
                            }
                        Capital = Capital.replace("\"", "");
                        
                        if(MontoCondPeso.equals(null) || MontoCondPeso.equals("null") || MontoCondPeso == "null"){
                                MontoCondPeso="Sin Registro";
                            }
                        MontoCondPeso = MontoCondPeso.replace("\"", "");
                        
                        if(CapitalRecibPesos.equals(null) || CapitalRecibPesos.equals("null") || CapitalRecibPesos == "null"){
                                CapitalRecibPesos="Sin Registro";
                            }
                        CapitalRecibPesos = CapitalRecibPesos.replace("\"", "");
                        
                        if(Interes.equals(null) || Interes.equals("null") || Interes == "null"){
                                Interes="Sin Registro";
                            }
                        Interes = Interes.replace("\"", "");
                        
                        if(InteresCondonadoPeso.equals(null) || InteresCondonadoPeso.equals("null") || InteresCondonadoPeso == "null"){
                                InteresCondonadoPeso="Sin Registro";
                            }
                        InteresCondonadoPeso = InteresCondonadoPeso.replace("\"", "");
                        
                        if(InteresRecibidoPeso.equals(null) || InteresRecibidoPeso.equals("null") || InteresRecibidoPeso == "null"){
                                InteresRecibidoPeso="Sin Registro";
                            }
                        InteresRecibidoPeso = InteresRecibidoPeso.replace("\"", "");
                        
                        if(HonorarioJudiPeso.equals(null) || HonorarioJudiPeso.equals("null") || HonorarioJudiPeso == "null"){
                                HonorarioJudiPeso="Sin Registro";
                            }
                        HonorarioJudiPeso = HonorarioJudiPeso.replace("\"", "");
                        
                        if(HonorarioJudiCondPeso.equals(null) || HonorarioJudiCondPeso.equals("null") || HonorarioJudiCondPeso == "null"){
                                HonorarioJudiCondPeso="Sin Registro";
                            }
                        HonorarioJudiCondPeso = HonorarioJudiCondPeso.replace("\"", "");
                        
                        if(HonorarioJudRecPeso.equals(null) || HonorarioJudRecPeso.equals("null") || HonorarioJudRecPeso == "null"){
                                HonorarioJudRecPeso="Sin Registro";
                            }
                        HonorarioJudRecPeso = HonorarioJudRecPeso.replace("\"", "");
                        
                        /*fin validaciones*/
                        
                        
                        System.out.println("contAux2"+cont);
                         if(cont==0){
                            cont=2;
                            System.out.println("contAux3"+cont);
                        }else{
                            cont = cont+1;
                        }
                        
                        org.apache.poi.ss.usermodel.Row row = sheet.createRow(cont);
                        
                        String string2 = keyStr.toString();
                        String[] parts2 = string2.split("_");
                        String part12 = parts2[0]; // 123
                        String part22 = parts2[1]; // 654321
                        String part23estado = parts2[2]; // 654321
                        String part24cedente = parts2[3]; // 654321
                        String part25id = parts2[4]; // 654321
                        String part26fecha = parts2[5]; // 654321
                        String part26VDE = parts2[6]; // 654321
                        String part26prov = parts2[7]; // 654321
                        String part26total = parts2[8]; // 654321
                        
                        estadoStringCliente=part23estado;
                        cedenteStringCliente=part24cedente;
                        idCondStringCliente=part25id;
                        fecCliStringCliente=part26fecha;
                        vDEsStringCliente=part26VDE;
                        totalProvision=part26prov;
                        TotalTotalStringCliente=part26total;
                       //mainObj.put("Rut_" + valueRutCliente+"_"+valueEstadoCliente+"_"+valueCedenteCliente+"_"+valueIdCliente+"_"+valueFechaCliente, ja);
                        
                        
                        
                        rutStringCliente=part22;
                        
                        Cell cellR = row.createCell(0);
			cellR.setCellValue(part22);
			cellR.setCellStyle(headerCellDet2);
                        
                        Cell cell0 = row.createCell(1);
			cell0.setCellValue(Operacion);
			cell0.setCellStyle(headerCellDet);
                        
                        Cell cell1 = row.createCell(2);
			cell1.setCellValue(MesesCastigo);
			cell1.setCellStyle(headerCellDet);
                        
                        Cell cell2 = row.createCell(3);
			//cell2.setCellValue(formatea.format(new BigDecimal(Capital.substring(0, Capital.length() - 5).replace(".", "")).doubleValue()));
                        if(Capital.endsWith("€")){
                            cell2.setCellValue(new Double(Capital.substring(0, Capital.length() - 5).replace(".", "")));
                            intTotalSumaCapital=new Double(Capital.substring(0, Capital.length() - 5).replace(".", ""));
                        }else{
                            cell2.setCellValue(new Double(Capital.substring(1).replace(".", "")));
                            intTotalSumaCapital=new Double(Capital.substring(1).replace(".", ""));
                        }
                        cell2.setCellStyle(headerCellDet);
                        //cell2.setCellType(Cell.CELL_TYPE_NUMERIC);
                        cell2.setCellType(CellType.NUMERIC);
                        
                        
                        Cell cell3 = row.createCell(4);
			//cell3.setCellValue(MontoCondPeso);
                        if(MontoCondPeso.endsWith("€")){
                            cell3.setCellValue(new Double(MontoCondPeso.substring(0, MontoCondPeso.length() - 5).replace(".", "")));
                            intTotalSumaCondonaCapital=new Double(MontoCondPeso.substring(0, MontoCondPeso.length() - 5).replace(".", ""));
                        }else{
                            cell3.setCellValue(new Double(MontoCondPeso.substring(1).replace(".", "")));
                            intTotalSumaCondonaCapital=new Double(MontoCondPeso.substring(1).replace(".", ""));
                        }
                        //cell3.setCellValue(new Double(MontoCondPeso.substring(0, MontoCondPeso.length() - 5).replace(".", "")));
			cell3.setCellStyle(headerCellDet);
                        cell3.setCellType(CellType.NUMERIC);
                        
                        Cell cell4 = row.createCell(5);
                        if(CapitalRecibPesos.endsWith("€")){
                            cell4.setCellValue(new Double(CapitalRecibPesos.substring(0, CapitalRecibPesos.length() - 5).replace(".", "")));
                            intTotalSumaRecibeCapital=new Double(CapitalRecibPesos.substring(0, CapitalRecibPesos.length() - 5).replace(".", ""));
                        }else{
                            cell4.setCellValue(new Double(CapitalRecibPesos.substring(1).replace(".", "")));
                            intTotalSumaRecibeCapital=new Double(CapitalRecibPesos.substring(1).replace(".", ""));
                        }
			//cell4.setCellValue(new Double(CapitalRecibPesos.substring(0, CapitalRecibPesos.length() - 5).replace(".", "")));
			cell4.setCellStyle(headerCellDet);
                        cell4.setCellType(CellType.NUMERIC);
                        
                        Cell cell5 = row.createCell(6);
                        if(Interes.endsWith("€")){
                            cell5.setCellValue(new Double(Interes.substring(0, Interes.length() - 5).replace(".", "")));
                            intInteres=new Double(Interes.substring(0, Interes.length() - 5).replace(".", ""));
                        }else{
                            cell5.setCellValue(new Double(Interes.substring(1).replace(".", "")));
                            intInteres=new Double(Interes.substring(1).replace(".", ""));
                        }
			//cell5.setCellValue(new Double(Interes.substring(0, Interes.length() - 5).replace(".", "")));
			cell5.setCellStyle(headerCellDet);
                        cell5.setCellType(CellType.NUMERIC);
                        
                        Cell cell6 = row.createCell(7);
                        if(InteresCondonadoPeso.endsWith("€")){
                            cell6.setCellValue(new Double(InteresCondonadoPeso.substring(0, InteresCondonadoPeso.length() - 5).replace(".", "")));
                            intInteresCondonadoPeso=new Double(InteresCondonadoPeso.substring(0, InteresCondonadoPeso.length() - 5).replace(".", ""));
                        }else{
                            cell6.setCellValue(new Double(InteresCondonadoPeso.substring(1).replace(".", "")));
                            intInteresCondonadoPeso=new Double(InteresCondonadoPeso.substring(1).replace(".", ""));
                        }
			//cell6.setCellValue(new Double(InteresCondonadoPeso.substring(0, InteresCondonadoPeso.length() - 5).replace(".", "")));
			cell6.setCellStyle(headerCellDet);
                        cell6.setCellType(CellType.NUMERIC);
                        
                        Cell cell7 = row.createCell(8);
                        if(InteresRecibidoPeso.endsWith("€")){
                            cell7.setCellValue(new Double(InteresRecibidoPeso.substring(0, InteresRecibidoPeso.length() - 5).replace(".", "")));
                            intInteresRecibidoPeso=new Double(InteresRecibidoPeso.substring(0, InteresRecibidoPeso.length() - 5).replace(".", ""));
                        }else{
                            cell7.setCellValue(new Double(InteresRecibidoPeso.substring(1).replace(".", "")));
                            intInteresRecibidoPeso=new Double(InteresRecibidoPeso.substring(1).replace(".", ""));
                        }
			//cell7.setCellValue(new Double(InteresRecibidoPeso.substring(0, InteresRecibidoPeso.length() - 5).replace(".", "")));
			cell7.setCellStyle(headerCellDet);
                        cell7.setCellType(CellType.NUMERIC);
                        
                        Cell cell8 = row.createCell(9);
                        if(HonorarioJudiPeso.endsWith("€")){
                            cell8.setCellValue(new Double(HonorarioJudiPeso.substring(0, HonorarioJudiPeso.length() - 5).replace(".", "")));
                            intTotalSumaHonorJud=new Double(HonorarioJudiPeso.substring(0, HonorarioJudiPeso.length() - 5).replace(".", ""));
                        }else{
                            cell8.setCellValue(new Double(HonorarioJudiPeso.substring(1).replace(".", "")));
                            intTotalSumaHonorJud=new Double(HonorarioJudiPeso.substring(1).replace(".", ""));
                        }
			//cell8.setCellValue(new Double(HonorarioJudiPeso.substring(0, HonorarioJudiPeso.length() - 5).replace(".", "")));
			cell8.setCellStyle(headerCellDet);
                        cell8.setCellType(CellType.NUMERIC);
                        
                        Cell cell9 = row.createCell(10);
                        if(HonorarioJudiCondPeso.endsWith("€")){
                            cell9.setCellValue(new Double(HonorarioJudiCondPeso.substring(0, HonorarioJudiCondPeso.length() - 5).replace(".", "")));
                            intTotalSumaHonoJudCond=new Double(HonorarioJudiCondPeso.substring(0, HonorarioJudiCondPeso.length() - 5).replace(".", ""));
                        }else{
                            cell9.setCellValue(new Double(HonorarioJudiCondPeso.substring(1).replace(".", "")));
                            intTotalSumaHonoJudCond=new Double(HonorarioJudiCondPeso.substring(1).replace(".", ""));
                        }
			//cell9.setCellValue(new Double(HonorarioJudiCondPeso.substring(0, HonorarioJudiCondPeso.length() - 5).replace(".", "")));
			cell9.setCellStyle(headerCellDet);
                        cell9.setCellType(CellType.NUMERIC);
                        
                        Cell cell10 = row.createCell(11);
                        if(HonorarioJudRecPeso.endsWith("€")){
                            cell10.setCellValue(new Double(HonorarioJudRecPeso.substring(0, HonorarioJudRecPeso.length() - 5).replace(".", "")));
                            intTotalSumaHondJudRecibir=new Double(HonorarioJudRecPeso.substring(0, HonorarioJudRecPeso.length() - 5).replace(".", ""));
                        }else{
                            cell10.setCellValue(new Double(HonorarioJudRecPeso.substring(1).replace(".", "")));
                            intTotalSumaHondJudRecibir=new Double(HonorarioJudRecPeso.substring(1).replace(".", ""));
                        }
			//cell10.setCellValue(new Double(HonorarioJudRecPeso.substring(0, HonorarioJudRecPeso.length() - 5).replace(".", "")));
			cell10.setCellStyle(headerCellDet);
                        cell10.setCellType(CellType.NUMERIC);

                        //SUMA DE TOTALES
                        
                        TotalSumaCapital.add(intTotalSumaCapital);
                        TotalSumaCondonaCapital.add(intTotalSumaCondonaCapital);
                        TotalSumaRecibeCapital.add(intTotalSumaRecibeCapital);
                        TotalSumaInteres.add(intInteres);
                        TotalSumaCondonaInteres.add(intInteresCondonadoPeso);
                        TotalSumaRecibeInteres.add(intInteresRecibidoPeso);
                        TotalSumaHonorJud.add(intTotalSumaHonorJud);
                        TotalSumaHonoJudCond.add(intTotalSumaHonoJudCond);
                        TotalSumaHondJudRecibir.add(intTotalSumaHondJudRecibir);
                        
                           
                       
                       }//fin for priemr
                        
                       double sumTotalSumaCapital = 0;
                       for (Double d : TotalSumaCapital) {
                                sumTotalSumaCapital += d;
                       } 
                       double sumTotalSumaCondonaCapital = 0;
                       for (Double d : TotalSumaCondonaCapital) {
                                sumTotalSumaCondonaCapital += d;
                       }
                       double sumTotalSumaRecibeCapital = 0;
                       for (Double d : TotalSumaRecibeCapital) {
                                sumTotalSumaRecibeCapital += d;
                       }
                       double sumTotalSumaInteres = 0;
                       for (Double d : TotalSumaInteres) {
                                sumTotalSumaInteres += d;
                       }
                       double sumTotalSumaCondonaInteres = 0;
                       for (Double d : TotalSumaCondonaInteres) {
                                sumTotalSumaCondonaInteres += d;
                       }
                       double sumTotalSumaRecibeInteres = 0;
                       for (Double d : TotalSumaRecibeInteres) {
                                sumTotalSumaRecibeInteres += d;
                       }
                       double sumTotalSumaHonorJud = 0;
                       for (Double d : TotalSumaHonorJud) {
                                sumTotalSumaHonorJud += d;
                       }
                       double sumTotalSumaHonoJudCond = 0;
                       for (Double d : TotalSumaHonoJudCond) {
                                sumTotalSumaHonoJudCond += d;
                       }
                       double sumTotalSumaHondJudRecibir = 0;
                       for (Double d : TotalSumaHondJudRecibir) {
                                sumTotalSumaHondJudRecibir += d;
                       }
                       
                       System.out.println("sumTotalSumaCapital--> "+sumTotalSumaCapital);
                       System.out.println("sumTotalSumaCondonaCapital--> "+sumTotalSumaCondonaCapital);
                       System.out.println("sumTotalSumaRecibeCapital--> "+sumTotalSumaRecibeCapital);
                       System.out.println("sumTotalSumaInteres--> "+sumTotalSumaInteres);
                       System.out.println("sumTotalSumaCondonaInteres--> "+sumTotalSumaCondonaInteres);
                       System.out.println("sumTotalSumaRecibeInteres--> "+sumTotalSumaRecibeInteres);
                       System.out.println("sumTotalSumaHonorJud--> "+sumTotalSumaHonorJud);
                       System.out.println("sumTotalSumaHonoJudCond--> "+sumTotalSumaHonoJudCond);
                       System.out.println("sumTotalSumaHondJudRecibir--> "+sumTotalSumaHondJudRecibir);
                        
                       
                       org.apache.poi.ss.usermodel.Row rowSheet2 = sheet2.createRow(contSheet2);
                       
                       
                       
                       Cell cellcedenteStringCliente = rowSheet2.createCell(0);
                       cellcedenteStringCliente.setCellValue(cedenteStringCliente);
                       cellcedenteStringCliente.setCellStyle(headerCellDet2);
                       
                       Cell cellidCondStringCliente = rowSheet2.createCell(1);
                       cellidCondStringCliente.setCellValue(idCondStringCliente);
                       cellidCondStringCliente.setCellStyle(headerCellDet2);
                       
                       Cell cellfecCliStringCliente = rowSheet2.createCell(2);
                       cellfecCliStringCliente.setCellValue(fecCliStringCliente);
                       cellfecCliStringCliente.setCellStyle(headerCellDet2);
                       
                       Cell cellrutStringCliente = rowSheet2.createCell(3);
                       cellrutStringCliente.setCellValue(rutStringCliente);
                       cellrutStringCliente.setCellStyle(headerCellDet2);
                       
                       Cell cellestadoStringCliente = rowSheet2.createCell(4);
                       cellestadoStringCliente.setCellValue(estadoStringCliente);
                       cellestadoStringCliente.setCellStyle(headerCellDet2);
                       
                       Cell cellsumTotalSumaCapital = rowSheet2.createCell(5);
                       cellsumTotalSumaCapital.setCellValue(sumTotalSumaCapital);
                       cellsumTotalSumaCapital.setCellStyle(headerCellDet);
                       cellsumTotalSumaCapital.setCellType(CellType.NUMERIC);
                       
                       Cell cellsumTotalSumaCondonaCapital = rowSheet2.createCell(6);
                       cellsumTotalSumaCondonaCapital.setCellValue(sumTotalSumaCondonaCapital);
                       cellsumTotalSumaCondonaCapital.setCellStyle(headerCellDet);
                       cellsumTotalSumaCondonaCapital.setCellType(CellType.NUMERIC);
                               
                       Cell cellsumTotalSumaRecibeCapital = rowSheet2.createCell(7);
                       cellsumTotalSumaRecibeCapital.setCellValue(sumTotalSumaRecibeCapital);
                       cellsumTotalSumaRecibeCapital.setCellStyle(headerCellDet);
                       cellsumTotalSumaRecibeCapital.setCellType(CellType.NUMERIC);
                               
                       Cell cellsumTotalSumaInteres = rowSheet2.createCell(8);
                       cellsumTotalSumaInteres.setCellValue(sumTotalSumaInteres);
                       cellsumTotalSumaInteres.setCellStyle(headerCellDet);  
                       cellsumTotalSumaInteres.setCellType(CellType.NUMERIC);
                               
                       Cell cellsumTotalSumaCondonaInteres = rowSheet2.createCell(9);
                       cellsumTotalSumaCondonaInteres.setCellValue(sumTotalSumaCondonaInteres);
                       cellsumTotalSumaCondonaInteres.setCellStyle(headerCellDet);
                       cellsumTotalSumaCondonaInteres.setCellType(CellType.NUMERIC);
                       
                       Cell cellsumTotalSumaRecibeInteres = rowSheet2.createCell(10);
                       cellsumTotalSumaRecibeInteres.setCellValue(sumTotalSumaRecibeInteres);
                       cellsumTotalSumaRecibeInteres.setCellStyle(headerCellDet);
                       cellsumTotalSumaRecibeInteres.setCellType(CellType.NUMERIC);
                       
                       Cell cellsumTotalSumaHonorJud = rowSheet2.createCell(11);
                       cellsumTotalSumaHonorJud.setCellValue(sumTotalSumaHonorJud);
                       cellsumTotalSumaHonorJud.setCellStyle(headerCellDet);
                       cellsumTotalSumaHonorJud.setCellType(CellType.NUMERIC);
                       
                       Cell cellsumTotalSumaHonoJudCond = rowSheet2.createCell(12);
                       cellsumTotalSumaHonoJudCond.setCellValue(sumTotalSumaHonoJudCond);
                       cellsumTotalSumaHonoJudCond.setCellStyle(headerCellDet);
                       cellsumTotalSumaHonoJudCond.setCellType(CellType.NUMERIC);
                       
                       Cell cellsumTotalSumaHondJudRecibir = rowSheet2.createCell(13);
                       cellsumTotalSumaHondJudRecibir.setCellValue(sumTotalSumaHondJudRecibir);
                       cellsumTotalSumaHondJudRecibir.setCellStyle(headerCellDet);
                       cellsumTotalSumaHondJudRecibir.setCellType(CellType.NUMERIC);
                       
                       Cell cellvDEsStringCliente = rowSheet2.createCell(14);
                           if (vDEsStringCliente == null || vDEsStringCliente.isEmpty()) {
                               cellvDEsStringCliente.setCellValue(new Double(0.0));
                           } else {
                               //return Double.parseDouble(s);
                               cellvDEsStringCliente.setCellValue(new Double(vDEsStringCliente));
                           }
                       //cellvDEsStringCliente.setCellValue(new Double(vDEsStringCliente));
                       cellvDEsStringCliente.setCellStyle(headerCellDet);
                       cellvDEsStringCliente.setCellType(CellType.NUMERIC);
                       
                       Cell celltotalProvision = rowSheet2.createCell(15);
                       if (totalProvision == null || totalProvision.isEmpty()) {
                               celltotalProvision.setCellValue(new Double(0.0));
                           } else {
                               //return Double.parseDouble(s);
                              celltotalProvision.setCellValue(new Double(totalProvision));
                           }
                       //celltotalProvision.setCellValue(new Double(totalProvision));
                       celltotalProvision.setCellStyle(headerCellDet);
                       celltotalProvision.setCellType(CellType.NUMERIC);
                       
                       Cell cellTotalTotalStringCliente = rowSheet2.createCell(16);
                       if (TotalTotalStringCliente == null || TotalTotalStringCliente.isEmpty()) {
                               cellTotalTotalStringCliente.setCellValue(new Double(0.0));
                           } else {
                               //return Double.parseDouble(s);
                              cellTotalTotalStringCliente.setCellValue(new Double(TotalTotalStringCliente));
                           }
                       //cellTotalTotalStringCliente.setCellValue(new Double(TotalTotalStringCliente));
                       cellTotalTotalStringCliente.setCellStyle(headerCellDet);
                       cellTotalTotalStringCliente.setCellType(CellType.NUMERIC);
                       
                       
                         
                       contSheet2++;
                               
                         cont=cont+1;
                       
                        //HONORARIOS
                        org.apache.poi.ss.usermodel.Row headerRow2 = sheet.createRow(cont);
                        for (int col = 0; col < COLUMNs2.length; col++) {
				Cell cell3 = headerRow2.createCell(col);
				cell3.setCellValue(COLUMNs2[col]);
				cell3.setCellStyle(headerCellStyle);
                        } 
                    for (Object keyStr2 : mainObjj.keySet()) {
                        
                            JSONArray keyvalue2 = (JSONArray) mainObjj.get(keyStr2);
                        
                        //Print key and value
                        System.out.println("key: " + keyStr2 + " value: " + keyvalue2);

                        JsonParser parser2 = new JsonParser();
                        JsonArray gsonArr2 = parser2.parse(keyvalue2.toString()).getAsJsonArray();
                        

                        
                        for (JsonElement obj : gsonArr2) {
                            JsonObject gsonObj = obj.getAsJsonObject();

                            
                            //String Cedente = gsonObj.get("Cedente").getAsString();
                            String Cedente = gsonObj.get("Cedente").toString();
                            //String CodigoOperacion = gsonObj.get("CodigoOperacion").getAsString();
                            String CodigoOperacion = gsonObj.get("CodigoOperacion").toString();
                            //String OperacionOriginal = gsonObj.get("OperacionOriginal").getAsString();
                            String OperacionOriginal = gsonObj.get("OperacionOriginal").toString();
                            //String Mora = gsonObj.get("Mora").getAsString();
                            String Mora = gsonObj.get("Mora").toString();
                            //String SaldoInsoluto = gsonObj.get("SaldoInsoluto").getAsString();
                            String SaldoInsoluto = gsonObj.get("SaldoInsoluto").toString();
                            //String DiasMora = gsonObj.get("DiasMora").getAsString();
                             String DiasMora = gsonObj.get("DiasMora").toString();
                            //String Producto = gsonObj.get("Producto").getAsString();
                            String Producto = gsonObj.get("Producto").toString();
                            String FecVenci = gsonObj.get("FecVenci").toString();
                            String FecCastigo = gsonObj.get("FecCastigo").toString();
                            
                            /*Validaciones*/
                            if(Cedente.equals(null) || Cedente.equals("null") || Cedente == "null"){
                                Cedente="Sin Registro";
                            }
                            Cedente = Cedente.replace("\"", "");
                            
                            if(CodigoOperacion.equals(null) || CodigoOperacion.equals("null") || CodigoOperacion == "null"){
                                CodigoOperacion="Sin Registro";
                            }
                            CodigoOperacion = CodigoOperacion.replace("\"", "");
                            
                            if(OperacionOriginal.equals(null) || OperacionOriginal.equals("null") || OperacionOriginal == "null"){
                                OperacionOriginal="Sin Registro";
                            }
                            OperacionOriginal = OperacionOriginal.replace("\"", "");
                            
                            if(Mora.equals(null) || Mora.equals("null") || Mora == "null"){
                                Mora="Sin Registro";
                            }
                            Mora = Mora.replace("\"", "");
                            
                            if(SaldoInsoluto.equals(null) || SaldoInsoluto.equals("null") || SaldoInsoluto == "null"){
                                SaldoInsoluto="Sin Registro";
                            }
                            SaldoInsoluto = SaldoInsoluto.replace("\"", "");
                            
                            if(DiasMora.equals(null) || DiasMora.equals("null") || DiasMora == "null"){
                                DiasMora="Sin Registro";
                            }
                            DiasMora = DiasMora.replace("\"", "");
                            
                            if(Producto.equals(null) || Producto.equals("null") || Producto == "null"){
                                Producto="Sin Registro";
                            }
                            Producto = Producto.replace("\"", "");
                            
                            
                            
                            if(FecVenci.equals(null) || FecVenci.equals("null") || FecVenci == "null"){
                                FecVenci="Sin Registro";
                            }
                            
                            if(FecCastigo.equals(null) || FecCastigo.equals("null") || FecCastigo == "null"){
                                FecCastigo="Sin Registro";
                            }
                            FecVenci = FecVenci.replace("\"", "");
                            FecCastigo = FecCastigo.replace("\"", "");
                            
                            /*fin validaciones*/
                            
                            
                        if(cont==0){
                            cont=2;
                            
                        }else{
                            cont = cont+1;
                        }
                        
                        String string2 = keyStr.toString();
                        String[] parts2 = string2.split("_");
                        String part12 = parts2[0]; // 123
                        String part22 = parts2[1]; // 654321
                        
                        
                        String string = keyStr2.toString();
                        String[] parts = string.split("_");
                        String part1 = parts[0]; // 123
                        String part2 = parts[1]; // 654321
                        
                        System.out.println("CONCILI-> "+part2);
                        System.out.println("CONCILI2-> "+part22);
                        
                        
                        if(part2==part22 || part2.equals(part22) ){
                        org.apache.poi.ss.usermodel.Row row = sheet.createRow(cont);
                        
                        Cell cellRut = row.createCell(0);
			cellRut.setCellValue(part22);
			cellRut.setCellStyle(headerCellHono2);
                        
                        Cell cell2 = row.createCell(1);
			cell2.setCellValue(OperacionOriginal);
			cell2.setCellStyle(headerCellHono);
                        
                        Cell cell5 = row.createCell(2);
			cell5.setCellValue(DiasMora);
			cell5.setCellStyle(headerCellHono);
                        
                        Cell cell3 = row.createCell(3);
                        if(Mora.endsWith("€")){
                            cell3.setCellValue(new Double(Mora.substring(0, Mora.length() - 5).replace(".", "")));
                        }else{
                            cell3.setCellValue(new Double(Mora.substring(1).replace(".", "")));
                        }
			//cell3.setCellValue(new Double(Mora.substring(0, Mora.length() - 5).replace(".", "")));
			cell3.setCellStyle(headerCellHono);
                        cell3.setCellType(CellType.NUMERIC);
                        
                        Cell cell1 = row.createCell(4);
			cell1.setCellValue(CodigoOperacion);
			cell1.setCellStyle(headerCellHono);
                        
                        Cell cell4 = row.createCell(5);
                        if(SaldoInsoluto.endsWith("€")){
                            cell4.setCellValue(new Double(SaldoInsoluto.substring(0, SaldoInsoluto.length() - 5).replace(".", "")));
                        }else{
                            cell4.setCellValue(new Double(SaldoInsoluto.substring(1).replace(".", "")));
                        }
			//cell4.setCellValue(new Double(SaldoInsoluto.substring(0, SaldoInsoluto.length() - 5).replace(".", "")));
			cell4.setCellStyle(headerCellHono);
                        cell4.setCellType(CellType.NUMERIC);
                        
                        Cell cell0 = row.createCell(6);
			cell0.setCellValue(Cedente);
			cell0.setCellStyle(headerCellHono);
                        
                        Cell cell6 = row.createCell(7);
			cell6.setCellValue(Producto);
			cell6.setCellStyle(headerCellHono);
                        
                        Cell cell7 = row.createCell(8);
			cell7.setCellValue(FecVenci);
			cell7.setCellStyle(headerCellHono);
                        
                        Cell cell8 = row.createCell(9);
			cell8.setCellValue(FecCastigo);
			cell8.setCellStyle(headerCellHono);
                        


                        }
                        
			}
                        
                        }
                        
                         cont=cont+1;
                         
                         
                    }
                    

        //CODIGO PARA RECORRER SHEET Y ELIMINAR CAMPOS VACIOS DEJADOS POR EL ARREGLO DE HONORARIOS
                       boolean isRowEmpty = false;
        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            if (sheet.getRow(i) == null) {
                isRowEmpty = true;
                sheet.shiftRows(i + 1, sheet.getLastRowNum(), -1);
                i--;
                continue;
            }
            for (int j = 0; j < sheet.getRow(i).getLastCellNum(); j++) {
                if (sheet.getRow(i).getCell(j).toString().trim().equals("")) {
                    isRowEmpty = true;
                } else {
                    isRowEmpty = false;
                    break;
                }
            }
            if (isRowEmpty == true) {
                sheet.shiftRows(i + 1, sheet.getLastRowNum(), -1);
                i--;
            }
        }
                        //SE ESCRIBE ARCHIVO Y SE DESCARGA
                        DateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
                        String _per =dft.format(db0.getValue());
			
                        workbook.write(out);
                        AMedia amedia = new AMedia("Extracto_Condonacion_Rut "+"DATOS"+_per+".xlsx", "xls", "application/file", out.toByteArray());
                        Filedownload.save(amedia);
                        out.close();
			//return new ByteArrayInputStream(out.toByteArray());
		
	}
    
     public void customersToExcelV2(JSONObject mainObj) throws IOException {
         String[] COLUMNs = { "Operacion","MesesCastigo", "Capital", "Capital A Condonar", "Capital A Recibir", "Interés", "Interés Cond.", "Interés A Recibir", "Hon. Jud.",
				"Hon. Jud. Cond.", "Hon. Jud Recibir"};
                
                String[] COLUMNs2 = { "Cedente","CódigoOperación", "OperaciónOriginal", "Mora", "SaldoInsoluto", "DíasMora", "Producto", "F.Vcmto", "F.Castigo"};
                
               
               
//                String[] COLUMNsInter = { "Extracto Condonacion Para Rut: "+sdas};
		
                        Workbook workbook = new XSSFWorkbook(); 
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("DatosCarga");
                        Sheet sheet2 = workbook.createSheet("Resumen");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
                        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        CellStyle headerCellStyleInter = workbook.createCellStyle();
			headerCellStyleInter.setFont(headerFont);
                        headerCellStyleInter.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                        headerCellStyleInter.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                        //INICIA FOR
//                        System.out.println("**ListRutsInter.size()--> "+ListRutsInter.size());
//                        for (int INTR = 0; INTR < ListRutsInter.size(); INTR++) {
                            
//                        for (CondonacionTabla2 datoHono : ListRutsInter) {
//                            sdas = datoHono.getRutcCliente();
//                            }
                        
                  
               
                        
                        
  
                        int cont=0;
                        int cont1=0;
                        int cont2=0;
                        int cont3=0;
                        int rowIDXn = 0;
			int rowIdx = 2;
                       for (Object keyStr : mainObj.keySet()) {
                       if(rowIdx==2){
                           cont=0;
                       }
                       else{
                           cont=rowIdx;
                       }
                        
                       
                        org.apache.poi.ss.usermodel.Row headerRowInter = sheet.createRow(cont);
                        
				Cell cell = headerRowInter.createCell(0);
				cell.setCellValue("Extracto Condonacion Para: "+keyStr);
				cell.setCellStyle(headerCellStyleInter);
		
                        
			// Row for Header
                        
                        if(rowIdx==2){
                           cont1=1;
                       }
                       else{
                           cont1=rowIdx+1;
                       }
			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(cont1);
                        

			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell2 = headerRow.createCell(col);
				cell2.setCellValue(COLUMNs[col]);
				cell2.setCellStyle(headerCellStyle);
                                
			}

                        for (int columnIndex = 0; columnIndex < 30; columnIndex++) {
                        sheet.autoSizeColumn(columnIndex);
                     }   
                           
                           
                        JSONArray keyvalue = (JSONArray) mainObj.get(keyStr);
                        
                        //Print key and value
                        System.out.println("key: " + keyStr + " value: " + keyvalue);

                        JsonParser parser = new JsonParser();
                        JsonArray gsonArr = parser.parse(keyvalue.toString()).getAsJsonArray();
                        
                        
                        for (JsonElement obj : gsonArr) {
                        JsonObject gsonObj = obj.getAsJsonObject();
                        String Operacion = gsonObj.get("Operacion").getAsString();
                        String MesesCastigo = gsonObj.get("Meses_Castigo").getAsString();
                        String Capital =   gsonObj.get("Capital").getAsString();
                        String MontoCondPeso =   gsonObj.get("MontoCondPesos").getAsString();
                        String CapitalRecibPesos =   gsonObj.get("CapitalRecibirPesos").getAsString();
                        String Interes = gsonObj.get("Interes").getAsString();
                        String InteresCondonadoPeso = gsonObj.get("InteresCondonadoPesos").getAsString();
                        String InteresRecibidoPeso =   gsonObj.get("InteresRecibidoPesos").getAsString();
                        String HonorarioJudiPeso =   gsonObj.get("HonorarioJudicialPesos").getAsString();
                        String HonorarioJudiCondPeso =   gsonObj.get("HonorarioJudicialCondonadoPesos").getAsString();
                        String HonorarioJudRecPeso =   gsonObj.get("HonorarioJudicialRecibidoPesos").getAsString();
              
                        System.out.println("cont2--> "+cont2);
                        
                        if(rowIdx==2){
                           cont2=2;
                       }
                       else{
                           cont2=rowIdx+2;
                           System.out.println("cont2+1--> "+cont2);
                       }
                        
                        org.apache.poi.ss.usermodel.Row row = sheet.createRow(cont2);
                        row.createCell(0).setCellValue(Operacion);
                        row.createCell(1).setCellValue(MesesCastigo);
                        row.createCell(2).setCellValue(Capital);
                        row.createCell(3).setCellValue(MontoCondPeso);
                        row.createCell(4).setCellValue(CapitalRecibPesos);
                        row.createCell(5).setCellValue(Interes);
                        row.createCell(6).setCellValue(InteresCondonadoPeso);
                        row.createCell(7).setCellValue(InteresRecibidoPeso);
                        row.createCell(8).setCellValue(HonorarioJudiPeso);
                        row.createCell(9).setCellValue(HonorarioJudiCondPeso);
                        row.createCell(10).setCellValue(HonorarioJudRecPeso);
                        rowIdx++; //queda en 5 deberia ser 7
                
                        }
                        
                       
                        
                        
                        rowIdx=rowIdx+2;
//                        org.apache.poi.ss.usermodel.Row headerRow2 = sheet.createRow(rowIdx);
//                        for (int col = 0; col < COLUMNs2.length; col++) {
//				Cell cell3 = headerRow2.createCell(col);
//				cell3.setCellValue(COLUMNs2[col]);
//				cell3.setCellStyle(headerCellStyle);
//			}
                        
                        
           
                    }
                        
                        
                   
                        
//                        
//			for (SacaBop datoCarga : mainObj) {
//				org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowIdx++);
//                                row.createCell(0).setCellValue(datoCarga.getOperacion());
//                                row.createCell(1).setCellValue(datoCarga.getMesesCastigo());
//                                row.createCell(2).setCellValue(datoCarga.getCapital());
//                                row.createCell(3).setCellValue(datoCarga.getMontoCondonarPesos());
//                                row.createCell(4).setCellValue(datoCarga.getCapitalARecibirPesos());
//                                row.createCell(5).setCellValue(datoCarga.getInteres());
//                                row.createCell(6).setCellValue(datoCarga.getInteresCondonadoPesos());
//                                row.createCell(7).setCellValue(datoCarga.getInteresARecibirPesos());
//                                row.createCell(8).setCellValue(datoCarga.getHonorarioJudicial2Pesos());
//                                row.createCell(9).setCellValue(datoCarga.getHonorarioJudicialCondonadoPesos());
//                                row.createCell(10).setCellValue(datoCarga.getHonorarioJudicialRecibidoPesos());
//                                rowIDXn = rowIdx;
//
//
//				/* Se Asigna formato para valor numerico */
////				Cell ageCell = row.createCell(3);
////				ageCell.setCellValue(datoCarga.getAge());
////				ageCell.setCellStyle(ageCellStyle);
//
//			}
                        
//                        org.apache.poi.ss.usermodel.Row headerRow2 = sheet.createRow(rowIDXn);
//                        for (int col = 0; col < COLUMNs2.length; col++) {
//				Cell cell = headerRow2.createCell(col);
//				cell.setCellValue(COLUMNs2[col]);
//				cell.setCellStyle(headerCellStyle);
//			}
                        
                        
//                        rowIDXn=rowIDXn+1;
//                        for (DetalleCliente datoCarga : ListHonos) {
//				org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowIDXn++);
//                                row.createCell(0).setCellValue(datoCarga.getCedente());
//                                row.createCell(1).setCellValue(datoCarga.getTipoCedente());
//                                row.createCell(2).setCellValue(datoCarga.getOperacion());
//                                row.createCell(3).setCellValue(datoCarga.getMoraEnPesosChileno());
//                                row.createCell(4).setCellValue(datoCarga.getSaldoEnPesosChileno());
//                                row.createCell(5).setCellValue(datoCarga.getDiasMora());
//                                row.createCell(6).setCellValue(datoCarga.getProductos());
//                                row.createCell(7).setCellValue(datoCarga.getFechaFencimiento());
//                                row.createCell(8).setCellValue(datoCarga.getFechaCastigo());
//                                
//
//			}
                        
//                        }

//                        for (int col = 0; col < COLUMNs2.length; col++) {
//				Cell cell = headerRow2.createCell(col);
//				cell.setCellValue(COLUMNs2[col]);
//				cell.setCellStyle(headerCellStyle);
//			}
                        
			workbook.write(out);
                        AMedia amedia = new AMedia("Extracto_Condonacion_Rut "+"DATOS"+".xlsx", "xls", "application/file", out.toByteArray());
                        Filedownload.save(amedia);
                        out.close();
			//return new ByteArrayInputStream(out.toByteArray());
		
	}
    public void customersToExcel(List<SacaBop> datosCarga,List<DetalleCliente> ListHonos) throws IOException {
		String[] COLUMNs = { "Operacion","MesesCastigo", "Capital", "Capital A Condonar", "Capital A Recibir", "Interés", "Interés Cond.", "Interés A Recibir", "Hon. Jud.",
				"Hon. Jud. Cond.", "Hon. Jud Recibir"};
                
                String[] COLUMNs2 = { "Cedente","CódigoOperación", "OperaciónOriginal", "Mora", "SaldoInsoluto", "DíasMora", "Producto", "F.Vcmto", "F.Castigo"};
                
               String sdas =""; 
               
                String[] COLUMNsInter = { "Extracto Condonacion Para Rut: "+sdas};
		
                        Workbook workbook = new XSSFWorkbook(); 
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("DatosCarga");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
                        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
                        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                        
                        CellStyle headerCellStyleInter = workbook.createCellStyle();
			headerCellStyleInter.setFont(headerFont);
                        headerCellStyleInter.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
                        headerCellStyleInter.setFillPattern(FillPatternType.SOLID_FOREGROUND);

                        //INICIA FOR
                        System.out.println("**ListRutsInter.size()--> "+ListRutsInter.size());
//                        for (int INTR = 0; INTR < ListRutsInter.size(); INTR++) {
                            
                        for (CondonacionTabla2 datoHono : ListRutsInter) {
                            sdas = datoHono.getRutcCliente();
                            }
                        
                        org.apache.poi.ss.usermodel.Row headerRowInter = sheet.createRow(0);
                        for (int col = 0; col < COLUMNsInter.length; col++) {
				Cell cell = headerRowInter.createCell(col);
				cell.setCellValue("Extracto Condonacion Para Rut: "+sdas);
				cell.setCellStyle(headerCellStyleInter);
			}
                            
//                            Cell cellS = headerRowInter.createCell(0);
//                            cellS.setCellValue("NUMER");
//                            cellS.setCellStyle(headerCellStyle);
                        
			// Row for Header
			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(1);
                        

			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
                                
			}

                        for (int columnIndex = 0; columnIndex < 30; columnIndex++) {
                        sheet.autoSizeColumn(columnIndex);
                     }
                        
			// Formato campos numericos
			CellStyle ageCellStyle = workbook.createCellStyle();
			ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));

                        int rowIDXn = 0;
			int rowIdx = 2;
			for (SacaBop datoCarga : datosCarga) {
				org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowIdx++);
                                row.createCell(0).setCellValue(datoCarga.getOperacion());
                                row.createCell(1).setCellValue(datoCarga.getMesesCastigo());
                                row.createCell(2).setCellValue(datoCarga.getCapital());
                                row.createCell(3).setCellValue(datoCarga.getMontoCondonarPesos());
                                row.createCell(4).setCellValue(datoCarga.getCapitalARecibirPesos());
                                row.createCell(5).setCellValue(datoCarga.getInteres());
                                row.createCell(6).setCellValue(datoCarga.getInteresCondonadoPesos());
                                row.createCell(7).setCellValue(datoCarga.getInteresARecibirPesos());
                                row.createCell(8).setCellValue(datoCarga.getHonorarioJudicial2Pesos());
                                row.createCell(9).setCellValue(datoCarga.getHonorarioJudicialCondonadoPesos());
                                row.createCell(10).setCellValue(datoCarga.getHonorarioJudicialRecibidoPesos());
                                rowIDXn = rowIdx;


				/* Se Asigna formato para valor numerico */
//				Cell ageCell = row.createCell(3);
//				ageCell.setCellValue(datoCarga.getAge());
//				ageCell.setCellStyle(ageCellStyle);

			}
                        
                        org.apache.poi.ss.usermodel.Row headerRow2 = sheet.createRow(rowIDXn);
                        for (int col = 0; col < COLUMNs2.length; col++) {
				Cell cell = headerRow2.createCell(col);
				cell.setCellValue(COLUMNs2[col]);
				cell.setCellStyle(headerCellStyle);
			}
                        
                        
                        rowIDXn=rowIDXn+1;
                        for (DetalleCliente datoCarga : ListHonos) {
				org.apache.poi.ss.usermodel.Row row = sheet.createRow(rowIDXn++);
                                row.createCell(0).setCellValue(datoCarga.getCedente());
                                row.createCell(1).setCellValue(datoCarga.getTipoCedente());
                                row.createCell(2).setCellValue(datoCarga.getOperacion());
                                row.createCell(3).setCellValue(datoCarga.getMoraEnPesosChileno());
                                row.createCell(4).setCellValue(datoCarga.getSaldoEnPesosChileno());
                                row.createCell(5).setCellValue(datoCarga.getDiasMora());
                                row.createCell(6).setCellValue(datoCarga.getProductos());
                                row.createCell(7).setCellValue(datoCarga.getFechaFencimiento());
                                row.createCell(8).setCellValue(datoCarga.getFechaCastigo());
                                
				/* Se Asigna formato para valor numerico */
//				Cell ageCell = row.createCell(3);
//				ageCell.setCellValue(datoCarga.getAge());
//				ageCell.setCellStyle(ageCellStyle);

			}
                        
//                        }

//                        for (int col = 0; col < COLUMNs2.length; col++) {
//				Cell cell = headerRow2.createCell(col);
//				cell.setCellValue(COLUMNs2[col]);
//				cell.setCellStyle(headerCellStyle);
//			}
                        
			workbook.write(out);
                        AMedia amedia = new AMedia("Extracto_Condonacion_Rut "+rutPruebaStringGlobal+".xlsx", "xls", "application/file", out.toByteArray());
                        Filedownload.save(amedia);
                        out.close();
			//return new ByteArrayInputStream(out.toByteArray());
		
	}

}
