/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Window;

/**
 *
 * @author exesilr
 */
public class SbifGarantias extends SelectorComposer<Window> {

    private static final long serialVersionUID = 1L;
    
    Window capturawin;
    
    @Wire
    private Window resultWin2;
    @Wire
    private Window idWinGarantiasPoput;
    
    @Wire
    Grid idGridSbif;    
    
    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        
        try {
            this.capturawin = (Window) comp;
            this.idGridSbif.setFocus(true);
        } catch (Exception ex) {
            
            System.err.print("ERROR");
        }
        
    }
    
}
