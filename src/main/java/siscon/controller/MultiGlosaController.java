/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox;
import siscon.entidades.GlosaDET;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import config.MvcConfig;

/**
 *
 * @author excosoc
 *
 *
 */
@SuppressWarnings("serial")
public class MultiGlosaController extends SelectorComposer<Window> {

    /*Ejemplo integraci�n glosa en Padre:
    
    -- Variable
    
      private EventQueue eq;
      private List<Glosa> glosa; 
    
        -- Adjuntar al doAfterCompose o donde declaren los eventos del controller
    
        //Herencia ente Glosa y Padre
        eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                List<GlosaDET> glosaDet = new ArrayList<GlosaDET>();
                glosaDet = (List<GlosaDET>) map.get("GlosaSS");

                if (glosaDet != null) {
                    if (!glosaDet.isEmpty()) {
                        glosa.setCant_glosas(glosaDet.size());
                        glosa.setDetalle(glosaDet);
                    }
                }
            }
        });
    
    --Evento que levanta el PopUp
    
    public void Glosa() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("GlosaSS", List<Glosa>);

        try {
            Window window = (Window) Executions.createComponents(
                    "Adjuntador/Glosa.zul", null, map
            );
            window.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar la glosa. \nError: " + e);
        }
    }
     */
    ////////////////////////////////////////////////////////////////////////////
    @Wire
    Textbox txt_Glosa;
    @Wire
    Window wnd_Glosa;
    @Wire
    Button btn_Cancelar;
    @Wire
    Button btn_Guardar;
    @Wire
    Listbox Lbox_Glosas;
    @Wire
    Button btn_Grabar;

    private GlosaDET glosa;
    private EventQueue eq;
    private List<GlosaDET> listGlosaFinal = new ArrayList<GlosaDET>();
    private ListModelList<GlosaDET> listLbox_Glosas = new ListModelList<GlosaDET>();
    private List<GlosaDET> listGlosaNew;
    private Session sess;
    private String Usuario = "admin";
    private GlosaDET glosaModif;
    private String modulo;
    private int id_colaborador;
    private int index_list;
    private UsuarioDAO uDAO;
    private MvcConfig mC;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        try {

            cargaPag();

        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            wnd_Glosa.detach();
        }

    }

    private void cargaPag() {

        try {
            glosa = new GlosaDET();
            sess = Sessions.getCurrent();
            modulo = "";
            final Execution exec = Executions.getCurrent();
            mC = new MvcConfig();
            uDAO = new UsuarioJDBC(mC.getDataSource());

            eventos();

            UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

            Usuario = permisos.getCuenta();

            id_colaborador = uDAO.buscarColaborador(Usuario);

            if (exec.getArg().get("GlosaSS") != null) {
                listGlosaFinal = (List<GlosaDET>) exec.getArg().get("GlosaSS");
            }
            if (exec.getArg().get("modulo") != null) {
                modulo = (String) exec.getArg().get("modulo");
            }

            validaModulo();
            cargaList();

            txt_Glosa.setPlaceholder("Glosa.");
            txt_Glosa.setFocus(true);

        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), error al adjuntar glosa.\nError: " + ex.toString());
        }
    }

    private void eventos() {

        txt_Glosa.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_Glosa.select();
            }

        });

        wnd_Glosa.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                map.put("GlosaSS", listGlosaFinal);
                eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", wnd_Glosa, map));
            }

        });

        btn_Guardar.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                try {
                    if (txt_Glosa.getValue().isEmpty()) {
                        Messagebox.show("Sr(a). Usuario(a), la glosa no se puede guardar en blanco. \n�Est� seguro(a) que desea adjuntar una glosa al sacabop?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_Glosa.setFocus(true);
                                } else if (Messagebox.ON_NO.equals(event.getName())) {
                                    wnd_Glosa.detach();
                                }
                            }
                        });
                    } else {
                        if (glosaModif != null) {

                            glosaModif.setGlosa(txt_Glosa.getValue());

                            listGlosaFinal.set(index_list, glosaModif);

                            glosaModif = null;
                            index_list = 0;

                        } else {

                            glosa = new GlosaDET();

                            glosa.setGlosa(txt_Glosa.getValue());
                            glosa.setUser(Usuario);
                            glosa.setEdit(true);
                            glosa.setModulo(modulo);
                            glosa.setId_user(id_colaborador);
                            listGlosaFinal.add(glosa);
                        }
                        RefreshList();

                        txt_Glosa.setValue("");
//                                    map.put("GlosaSS", glosa);
//
//                                    eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
//                                    eq.publish(new Event("onButtonClick", btn_Guardar, map));
//
//                                    wnd_Glosa.detach();

                    }
                } catch (WrongValueException e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } catch (Exception e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } finally {

                }

            }

        });

        btn_Cancelar.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                map.put("GlosaSS", listGlosaFinal);
                eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_Cancelar, map));

                wnd_Glosa.detach();
            }

        });

        btn_Grabar.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                try {
                    if (listGlosaFinal == null) {
                        Messagebox.show("Sr(a). Usuario(a), no hay glosas ingresadas. \n�Est� seguro(a) que desea adjuntar una glosa al sacabop?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_Glosa.setFocus(true);
                                } else if (Messagebox.ON_NO.equals(event.getName())) {
                                    wnd_Glosa.detach();
                                }
                            }
                        });
                    } else if (listGlosaFinal.isEmpty()) {
                        Messagebox.show("Sr(a). Usuario(a), la glosa no se puede guardar en blanco. \n�Est� seguro(a) que desea adjuntar una glosa al sacabop?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_Glosa.setFocus(true);
                                } else if (Messagebox.ON_NO.equals(event.getName())) {
                                    wnd_Glosa.detach();
                                }
                            }
                        });
                    } else {
                        Messagebox.show("Sr(a). Usuario(a), �Est� seguro(a) que desea guardar?.", "Mensaje Administrador", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_OK.equals(event.getName())) {
                                    map.put("GlosaSS", listGlosaFinal);

                                    eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                                    eq.publish(new Event("onButtonClick", btn_Guardar, map));

                                    wnd_Glosa.detach();
                                } else if (Messagebox.ON_CANCEL.equals(event.getName())) {
                                    txt_Glosa.setFocus(true);
                                    txt_Glosa.select();
                                }
                            }
                        });

                    }
                } catch (WrongValueException e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } catch (Exception e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } finally {

                }
            }

        });

    }

    private void RefreshList() {
        listLbox_Glosas.clear();
        listLbox_Glosas = new ListModelList<GlosaDET>(listGlosaFinal);
        Lbox_Glosas.setModel(listLbox_Glosas);
        Lbox_Glosas.setVisible(false);
        Lbox_Glosas.setVisible(true);

    }

    private void cargaList() {

        listLbox_Glosas = new ListModelList<GlosaDET>(listGlosaFinal);
        Lbox_Glosas.setModel(listLbox_Glosas);

        Lbox_Glosas.setItemRenderer(new ListitemRenderer<GlosaDET>() {

            Listcell cell;

            public void render(Listitem item, final GlosaDET data, int index) throws Exception {

                item.setId(Integer.toString(index));
                cell = new Listcell();
                final Label lbl_Id = new Label();

                lbl_Id.setValue(Integer.toString(index + 1));
                cell.setStyle("text-align:center;");
                lbl_Id.setParent(cell);
                item.appendChild(cell);
                cell.setParent(item);

                cell = new Listcell();
                final Label lbl_Glosa = new Label();
                lbl_Glosa.setHeight("20px");
                lbl_Glosa.setValue(data.getGlosa());
                cell.setStyle("text-align:left;");
                lbl_Glosa.setParent(cell);
                item.appendChild(cell);

                cell = new Listcell();

                final Label lbl_User = new Label();

                lbl_User.setValue(data.getUser());
                cell.setStyle("text-align:left;");
                lbl_User.setParent(cell);
                item.appendChild(cell);

                cell = new Listcell();

                final Button btn_EliminarArch = new Button();

                if (data.isEdit()) {
                    btn_EliminarArch.setLabel("Eliminar");
                    btn_EliminarArch.setClass("btn btn-danger");

                    btn_EliminarArch.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                        public void onEvent(Event event) throws Exception {
                            Button btn = (Button) event.getTarget();

                            Listitem itemId = (Listitem) btn.getParent().getParent();

                            int idEliminar = Integer.parseInt(itemId.getId());
                            Label nameEliminar = (Label) itemId.getChildren().get(1).getChildren().get(0);
                            String glosa_Det = nameEliminar.getValue();
                            List<GlosaDET> listaFiltro = new ArrayList<GlosaDET>();

                            for (GlosaDET glosaSola : listGlosaFinal) {

                                if (glosaSola.getGlosa().equals(glosa_Det)) {
                                } else {
                                    listaFiltro.add(glosaSola);
                                }
                            }

                            if (listaFiltro.isEmpty()) {
                                listGlosaFinal = listaFiltro;
                            } else {
                                listGlosaFinal = listaFiltro;
                            }

//                            listLbox_Glosas = new ListModelList<GlosaDET>(listGlosaFinal);
                            RefreshList();

                        }

                    });

                    btn_EliminarArch.setParent(cell);
                } else {
                    {
                        final Label lbl_Eli = new Label();

                        lbl_Eli.setValue("Bloqueado");
                        lbl_Eli.setParent(cell);
                    }
                }

                item.appendChild(cell);

                item.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        glosaModif = new GlosaDET();

                        int arreglo = listGlosaFinal.size();

                        for (int indexx = 0; indexx < arreglo; indexx++) {
                            if (listGlosaFinal.get(indexx).getGlosa().equals(data.getGlosa())) {
                                glosaModif = listGlosaFinal.get(indexx);
                                txt_Glosa.setValue(glosaModif.getGlosa());
                                index_list = indexx;
                                break;
                            }
                        }
                    }

                });

            }

        });
    }

    private void validaModulo() {
        if (modulo.equals("AnSacabopView")) {
            btn_Guardar.setVisible(false);
            btn_Grabar.setVisible(false);
            txt_Glosa.setDisabled(true);
        }

    }

}
