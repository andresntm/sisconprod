/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.UsuariosSM;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.ReparoJDBC;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author exesilr
 */
public class UsrAddPerfilPoputController extends SelectorComposer<Window> {

    @Wire
    Grid BanEntrAnalista;
    //final CondonacionInterfaz cond;
    final UsuariosSMInterfaz cond;
    //ListModelList<CondonacionTabla> bandeCondTerminada;

   // private List<CondonacionTabla> listCondonaciones;
    ListModelList<UsuariosSM> bandeCondTerminada;
    private List<UsuariosSM> listCondonaciones;
    
    @Wire
    Textbox motivoReparo;
    @Wire
    Textbox id_condonacion;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    ReparoJDBC _reparo;
    MetodosGenerales MT;
    @Wire
    Textbox txt_filtra;
    private List<UsuariosSM> lCondFinal;
    @Wire
    Textbox usruaios_oldpass2;
    Session sess;
    UsuarioPermiso permisos;
    String cuenta;
    String Nombre;
    @Wire
    Window capturawin;
    @Wire 
     Label lbl_idCodPerfil;
    private List<UsuariosSM> lCondFilter;
    @Wire
    private String sTextLocalResp;
    private String sTextLocal;
     ListModelList<usuario> bandeusuarios;
 List<usuario> _usuarios = new ArrayList<usuario>();
 UsuarioDAO _usu;
    public UsrAddPerfilPoputController() throws SQLException {

        //this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.cond = new UsuariosSMImpl(mmmm.getDataSource());
        this._reparo = new ReparoJDBC(mmmm.getDataSource());
         this._usu = new UsuarioJDBC(mmmm.getDataSource());
         this.MT = new MetodosGenerales();

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = comp;

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cuenta = permisos.getCuenta();//user.getAccount();
        Nombre = permisos.getNombre();
        _usuarios = this._usu.UsuariosSinPerfil();
        bandeusuarios = new ListModelList<usuario>(_usuarios);

        listCondonaciones = cond.getUsuariosSM();
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);

        lCondFinal = new ArrayList<UsuariosSM>();
        lCondFinal = listCondonaciones;
        lCondFilter = lCondFinal;
        BanEntrAnalista.setModel(bandeCondTerminada);
        
        lCondFinal = new ArrayList<UsuariosSM>();
        lCondFinal = listCondonaciones;
        lCondFilter = lCondFinal;
        
        
       // BanEntrAnalista.setModel(bandeusuarios);
       // BanEntrAnalista.renderAll();

    }

 //   @Listen("onClick=#_idReparoDoc")
    public void AgregarUsuario(Object[] aaa) {
        //id_winSacabobAnalista.detach();
        
   int result=0;
        Map<String, Object> arguments = new HashMap<String, Object>();

        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];
        // List<Object> result =(List<Object>) aaa.list();
        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            //strings[i]=((String)aaa[i]).split("=");
            coupleArray[i] = ss.split("=");
            // Messagebox.show("KSKSKS["+Arrays.toString(coupleArray[i])+"]");

        }
         
        String aliass = (coupleArray[0][1]);
        String rut = (coupleArray[1][1]);
        
        boolean resultado;
        //Messagebox.show("Reparando...Condonación Numero :[" + id_condonacion.getValue() + "]");

        if ((result=this._usu.guardaUsuarioEnPerfilds(aliass,rut,lbl_idCodPerfil.getValue()))>0) {
           // this.cond.SetCondonacionesReparadaAnalista(Integer.parseInt(id_condonacion.getValue()));
            //resultado = this.cond.SetCambiaEstadoCondonaciones("Analista.Recepcion", "Ejecutivo.Reparadas", cuenta, "EjecutivoDestino", Integer.parseInt(id_condonacion.getValue()), "Estado.Recepcion", "Estado.Reparada.Analista");

            Messagebox.show("Se guardo la relacion:[" + result + "]");

            //Messagebox.show("Prueba fellow2222{" + capturawin.getParent().getFellows().toString() + "}");
            //Include inc = (Include) capturawin.getParent().getParent().getParent().getFellow("pageref");
            // inc.setSrc(null);
            //Sessions.getCurrent().setAttribute("rutcliente", rutcliente);
            //inc.setSrc("Ejecutivo/EjeReparadas.zul");
            Events.sendEvent(new Event("onClick", (Button) ((Window) capturawin.getParent()).getFellow("btn_refresh")));
            this.capturawin.detach();
            // ligarWin.detach();

        } else {
            Messagebox.show("Error Guardando Relacion... con Error]");
        }

    }

    @Listen(Events.ON_OK + " = #txt_filtra")
    public void onEnterPressed(Event event) {
        System.out.println("entra ok preseed enter ");
        sTextLocal = txt_filtra.getText();
        String glosanom = sTextLocal;
        
        System.out.println("glosanom: " + glosanom);
        listCondonaciones = cond.getUsuariosSM();
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
        BanEntrAnalista.setModel(bandeCondTerminada);


        if (glosanom == "" || glosanom.isEmpty()) {

          listCondonaciones = cond.getUsuariosSM();
          bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
          BanEntrAnalista.setModel(bandeCondTerminada);
        } else {

          
          lCondFilter= listCondonaciones;
          lCondFilter = filterGrid();
          cargaGrid(lCondFilter);
        }
    }
    

    public void buscarOper() throws SQLException {

        
        sTextLocal = txt_filtra.getText();
        String glosanom = sTextLocal;
        
        System.out.println("glosanom: " + glosanom);
        listCondonaciones = cond.getUsuariosSM();
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
        BanEntrAnalista.setModel(bandeCondTerminada);


        if (glosanom == "" || glosanom.isEmpty()) {

          listCondonaciones = cond.getUsuariosSM();
          bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);
          BanEntrAnalista.setModel(bandeCondTerminada);
        } else {

          
          lCondFilter= listCondonaciones;
          lCondFilter = filterGrid();
          cargaGrid(lCondFilter);
        }

    }
    
     private List<UsuariosSM> filterGrid() {
        List<UsuariosSM> lFilterGrid = new ArrayList<UsuariosSM>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            System.out.println("sTextLocalResp--> "+sTextLocalResp);
        if (sTextLocal == null) {
            lFilterGrid = listCondonaciones;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = listCondonaciones;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (UsuariosSM cT2 : listCondonaciones) {
                        String cadena = cT2.getStrNombre() + ";"
                                + cT2.getStrApellidoP() + ";"
                                + cT2.getStrApellidoM() + ";"
                                + cT2.getStrUsuario() + ";"
                                + cT2.getStrRut() ;

                        if (MT.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        //sTextLocalResp = sTextLocal;
        return lFilterGrid;
    }
    
    private void cargaGrid(List<UsuariosSM> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<UsuariosSM>(listCondonaciones);

        BanEntrAnalista.setModel(bandeCondTerminada);

    }
    
    
}
