/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;
import siscon.entidades.APPResumenCondonacionesInforme;
import siscon.entidades.BandejaCondonacionesInforme;
import siscon.entidades.ConDetOper;
import siscon.entidades.Condonacion;
import siscon.entidades.DetalleCliente;
import siscon.entidades.Periodo;
import siscon.entidades.ResumenCondonacionesInforme;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.PeriodoJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class APPResumenInformeController extends SelectorComposer<Component> {

@Wire
    Grid grd_GridInfor;
    //////////////Variables y controles filtros///////////////
    @Wire
    Textbox txt_filtra;
    @Wire
    Listbox lts_Columnas;
    @Wire
    Span btn_search;
    private EventQueue eq;//cosoriosound
    private ListModelList<Column> lMlcmb_Columns;
    private List<Column> lCol;
    private List<Listitem> lItemFull;
    private MetodosGenerales mG = new MetodosGenerales();
    private List<Column> lColFilter = new ArrayList<Column>();
    private List<Listitem> lItemSelect;
    private List<Listitem> lItemNotSelect = new ArrayList<Listitem>();
    private List<APPResumenCondonacionesInforme> lCondFilter;

//    private Date dDesdeLocal;
//    private Date dHastaLocal;
    private String sTextLocal;
//    private Date dDesdeLocalResp;
//    private Date dHastaLocalResp;
    private String sTextLocalResp;
    private List<APPResumenCondonacionesInforme> lCondFinal;
    ////////////////////////////////////////////////////////
    final CondonacionInterfaz cond;
    ListModelList<APPResumenCondonacionesInforme> bandeCondTerminada;
    Condonacion CurrentCondonacion;
    private List<APPResumenCondonacionesInforme> listCondonaciones;
    Window window;
    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();
    int idCondonacion;
    Session sess;
    UsuarioPermiso permisos;
    String cuenta;
    String Nombre;
    MetodosGenerales MT;
    DetalleOperacionesClientes detoper;
    ListModelList<DetalleCliente> ListDetOperModel;
    private static List<BandejaCondonacionesInforme> DataGridList = new ArrayList<BandejaCondonacionesInforme>();
    Session session;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    NumberFormat nfff;
    @Wire
    Textbox a1;
        @Wire
    Textbox a2;
            @Wire
    Textbox a3;
                @Wire
    Textbox a4;
                    @Wire
    Textbox a5;
                        @Wire
    Textbox a6;
                            @Wire
    Textbox a7;
                                @Wire
    Textbox a8;
                                    @Wire
    Textbox a9;
                                    
                                    
                                    
           @Wire
    Combobox cmb_Periodo;     
          private ListModelList lCmb_Periodo;
          private List<Periodo> lPeriodo = new ArrayList<Periodo>(); 
          
          
    PeriodoJDBC _periodo;       
          
    public APPResumenInformeController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());
        listCondonaciones = null;
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
         this._periodo=new PeriodoJDBC(mmmm.getDataSource());
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();
        Nombre = permisos.getNombre();
        session = Sessions.getCurrent();
        Locale.setDefault(new Locale("es", "CL"));
//NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
nfff = NumberFormat.getCurrencyInstance(Locale.getDefault());


 cargaPeriodo();

        
         lCondFilter = new ArrayList<APPResumenCondonacionesInforme>();
        
        lCondFinal = this.cond.GetCondonacionesAplicadasAndResumenInforme(cuenta);
            for (APPResumenCondonacionesInforme kk:lCondFinal){
            
            a1.setValue(kk.getTotal_rut());
            a2.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_CASTIGADO())).replaceFirst("Ch", ""));
            a3.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_VDE_SGN())).replaceFirst("Ch", ""));
           // a4.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_RECIBIDO_1())).replaceFirst("Ch", ""));
            a5.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_RECIBIDO_2())).replaceFirst("Ch", ""));
            a6.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_CAPITAL_CONDONADO())).replaceFirst("Ch", ""));
            a7.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_HONORARIO_RECIBE())).replaceFirst("Ch", ""));
           // a8.setValue(kk.get);
           a9.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_COSTAS_PROVISION())).replaceFirst("Ch", ""));
           int x;
           
           x=1+1;
            
            }
        eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {
                listCondonaciones = cond.GetCondonacionesAplicadasAndResumenInforme(cuenta);
                bandeCondTerminada = new ListModelList<APPResumenCondonacionesInforme>(listCondonaciones);

                grd_GridInfor.setModel(bandeCondTerminada);
                txt_filtra.setText("");
            }
        });
    }

    private void cargaPag() {

        try {
          //  sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
           // sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

          ////  eventos();

            lCondFinal = new ArrayList<APPResumenCondonacionesInforme>();
            lCondFilter = new ArrayList<APPResumenCondonacionesInforme>();
            lCondFinal = this.cond.GetCondonacionesAplicadasAndResumenInforme(cuenta);
            lCondFilter = lCondFinal;
            
            for (APPResumenCondonacionesInforme kk:lCondFinal){
            
            a1.setValue(kk.getTotal_rut());
            
            }

           // cargaGrid(lCondFilter);
           // muestraCol();
        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones aplicadas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    private void cargaGrid(List<APPResumenCondonacionesInforme> lCondT2) {

        listCondonaciones = lCondT2;
        bandeCondTerminada = new ListModelList<APPResumenCondonacionesInforme>(listCondonaciones);

        grd_GridInfor.setModel(bandeCondTerminada);

    }

    private void muestraCol() {
        cargaListColumnas();
    }

    private void cargaListColumnas() {
        lCol = new ArrayList<Column>();
        lItemFull = new ArrayList<Listitem>();
        lCol = grd_GridInfor.getColumns().getChildren();

        lMlcmb_Columns = new ListModelList<Column>(lCol);
        lMlcmb_Columns.setMultiple(true);
        lMlcmb_Columns.setSelection(lCol);

        lts_Columnas.setItemRenderer(new ListitemRenderer<Object>() {
            public void render(Listitem item, Object data, int index) throws Exception {
                String col;
                Column column = (Column) data;
                Listcell cell = new Listcell();

                item.appendChild(cell);

                col = column.getLabel();
                cell.appendChild(new Label(col));
                item.setValue(data);

            }
        });

        ((Selectable<Column>) lMlcmb_Columns).getSelectionControl().setSelectAll(true);
        lts_Columnas.setModel(lMlcmb_Columns);

        lItemFull = lts_Columnas.getItems();

//        if (lItemNotSelect != null) {
//
//            for (Listitem lItem : lItemNotSelect) {
//                lts_Columnas.setSelectedItem(lItem);
//            }
//        }
    }

    private void eventos() {

        lts_Columnas.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Column xcol = new Column();
                lItemSelect = new ArrayList<Listitem>();
                lColFilter = new ArrayList<Column>();
                lItemNotSelect = new ArrayList<Listitem>();

                for (Listitem rItem : lst_Xcol.getSelectedItems()) {
                    lColFilter.add((Column) rItem.getValue());
                    lItemSelect.add(rItem);
                }

                for (Listitem xcolprov : lItemFull) {
                    xcol = (Column) xcolprov.getValue();
                    if (lItemSelect.contains(xcolprov)) {
                        xcol.setVisible(true);
                    } else {
                        lItemNotSelect.add(xcolprov);
                        xcol.setVisible(false);
                    }
                }

            }
        });

        txt_filtra.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_filtra.select();
            }
        });

//        bd_filtra.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
//            public void onEvent(final Event event) throws Exception {
//                Bandbox bBox = (Bandbox) event.getTarget();
//                sTextLocal = bBox.getText();
//
//                lCondFilter = filterGrid();
//                cargaGrid(lCondFilter);
//
//            }
//        });
        btn_search.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                sTextLocal = txt_filtra.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
            }
        });
        txt_filtra.addEventListener(Events.ON_OK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Textbox tBox = (Textbox) event.getTarget();
                sTextLocal = tBox.getText();

                lCondFilter = filterGrid();
                cargaGrid(lCondFilter);
                txt_filtra.select();

            }
        });

        //        dbx_desde.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dDesdeLocal = dbx_desde.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
        //        dbx_hasta.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
        //            public void onEvent(Event event) throws Exception {
        ////                Date dDesde = dbx_desde.getValue();
        //                dHastaLocal = dbx_hasta.getValue();
        ////                DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        ////                List<CondonacionTabla2> lFilterGrid = new ArrayList<CondonacionTabla2>();
        //
        ////                for (CondonacionTabla2 cT2 : lCondFilter) {
        ////                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
        ////
        ////                    if (isDesde.after(dDesde)) {
        ////                        lFilterGrid.add(cT2);
        ////                    }
        ////                }
        //                lCondFilter = filterGrid();
        //                cargaGrid(lCondFilter);
        //
        //            }
        //        });
    }

    private List<APPResumenCondonacionesInforme> filterGrid() {
        List<APPResumenCondonacionesInforme> lFilterGrid = new ArrayList<APPResumenCondonacionesInforme>();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        if (sTextLocal == null) {
            lFilterGrid = lCondFinal;
        } else {
            if (sTextLocal.trim().equals("")) {
                lFilterGrid = lCondFinal;
            } else {
                if (sTextLocal != sTextLocalResp) {
                    for (APPResumenCondonacionesInforme cT2 : lCondFinal) {
                        String cadena = cT2.getTotal_rut() ;// + ";"
//                                + cT2.getId_condonacion() + ";"
//                                + cT2.getTimestap() + ";"
//                                + cT2.getEstado() + ";"
//                                + cT2.getUsuariocondona() + ";"
//                                + cT2.getRegla() + ";"
//                                + cT2.getTipocondonacion() + ";"
//                                + cT2.getEstado() + ";"
//                                + cT2.getDi_num_opers() + ";"
//                                + cT2.getComentario_resna() + ";"
//                                + cT2.getMonto_total_capitalS().replace(".", "").replace(",", "") + ";"
//                                + cT2.getMonto_total_condonadoS().replace(".", "").replace(",", "") + ";"
//                                + cT2.getMonto_total_recibitS().replace(".", "").replace(",", "") + ";"
//                                + cT2.getRutcCliente();

                        if (mG.like(cadena.toLowerCase(), "%" + sTextLocal.toLowerCase() + "%")) {
                            lFilterGrid.add(cT2);
                        }
                    }
                } else {
                    lFilterGrid = lCondFilter;
                }
            }
        }

//        if (dDesdeLocal != dDesdeLocalResp) {
//            if (dDesdeLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.after(dDesdeLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
//
//        if (dHastaLocal != dHastaLocalResp) {
//            if (dHastaLocal != null) {
//                for (CondonacionTabla2 cT2 : lCondFilter) {
//                    Date isDesde = new Date(cT2.getFecIngreso().getTime());
//
//                    if (isDesde.before(dHastaLocal)) {
//                        lFilterGrid.add(cT2);
//                    }
//                }
//            }
//        }
        if (lFilterGrid.isEmpty() || lFilterGrid.size() <= 0) {
            Messagebox.show("Sr(a). Usuario(a), no se encontraron coincidencias para '" + sTextLocal + "'.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
            lFilterGrid = lCondFinal;
        }

        sTextLocalResp = sTextLocal;
//        dDesdeLocalResp = dDesdeLocal;
//        dHastaLocalResp = dHastaLocal;

        return lFilterGrid;
    }

    public void MostrarCondonacion(Object[] aaa) {
        window = null;

        String ced = null;
        int id_operacion_documento = 0;
        List<ConDetOper> _condetoper = new ArrayList<ConDetOper>();
        Map<String, Object> arguments = new HashMap<String, Object>();
        String template;
        String[] strings = new String[aaa.length];
        String[][] coupleArray = new String[aaa.length][];

        for (int i = 0; i < aaa.length; i++) {
            String ss = aaa[i].toString();
            coupleArray[i] = ss.split("=");

        }

        idCondonacion = Integer.parseInt(coupleArray[0][1]);
        int rutejecutivo = Integer.parseInt(coupleArray[1][1]);
        int rutcliente = this.cond.getClienteEnCondonacion(idCondonacion);
        arguments.put("id_condonacion", idCondonacion);
        arguments.put("rut", "14212287-1");
        arguments.put("rutEjecutivo", rutejecutivo);

        char uuuu = MT.CalculaDv(rutcliente);

        arguments.put("rutcliente", rutcliente + "-" + uuuu);

        _condetoper = this.cond.getListDetOperCondonacion(Integer.parseInt(coupleArray[0][1]));
        String Operaciones = "";

        for (ConDetOper _ConDetOper : _condetoper) {
            Operaciones = Operaciones + " [" + _ConDetOper.getOpracionOriginal() + "]";

        }
        ListDetOper = detoper.Cliente(rutcliente, cuenta);
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            String Tcedente = detCliHono.getTipoCedente();
            String TipoOperacionExclude = "VDE";
            if (Tcedente.toLowerCase().contains(TipoOperacionExclude)) {
                listModelHono.add(detCliHono);

            } else {

                listModelCondo.add(detCliHono);

            }
        }
        ConDetOper elimina = null;
        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (DetalleCliente rownn : listModelCondo) {
            if (elimina != null) {
                _condetoper.remove(elimina);
                elimina = null;
            }

            for (ConDetOper _ConDetOper : _condetoper) {

                if (_ConDetOper.getOpracionOriginal().equals(rownn.getOperacionOriginal())) {
                    detClientCond.add(rownn);
                    elimina = _ConDetOper;
                }

            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);
        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), La condonación Generada no Contiene Operaciones.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        session.setAttribute("rutcliente", rutcliente + "-" + uuuu);
        session.setAttribute("rutEjecutivo", rutejecutivo);
        session.setAttribute("idcondonacion", idCondonacion);

        for (APPResumenCondonacionesInforme tC2 : listCondonaciones) {
            if (Integer.parseInt(tC2.getTotal_rut()) == idCondonacion) {
                //ced = tC2.getCedente();
               // ced= tC2.getFld_ced();
                break;
            }
        }

        if (ced.equals("PYME")) {
            template = "ZonalPyme/Poput/ZonalSacabopMostrar.zul";
        } else {
            template = "Analista/Poput/AnSacabopMostrar.zul";
        }

        window = (Window) Executions.createComponents(template, null, arguments);
        window.addEventListener("onItemAdded", new EventListener() {
            @Override
            public void onEvent(Event event) {

                Messagebox.show("Me EJEcuto######## onItemAdded####");

                window.detach();

            }
        });

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    
    
            public void cargaPeriodo() {

        try {
            lPeriodo.clear();
            lPeriodo = _periodo.getALLPeriodo();
            lCmb_Periodo = new ListModelList<Periodo>(lPeriodo);
//      

            cmb_Periodo.setModel(lCmb_Periodo);
            cmb_Periodo.applyProperties();
            cmb_Periodo.setVisible(false);
            cmb_Periodo.setVisible(true);
            cmb_Periodo.setPlaceholder("Seleccione Periodo.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los Bacnas. \nError:" + e.toString() + "\nContactar a área tecnica.");
        }

    }
            
            @Listen("onSelect = #cmb_Periodo")
    public void onSelect$cmb_Periodo(Event event) {
        cmb_Periodo.getSelectedIndex();
        String _per=cmb_Periodo.getValue();
        for (final Periodo lrf : lPeriodo) {
            if (lrf.getPeriodo().equals(_per)) {
               // txt_DesdeFecha.setValue(Integer.toString(lrf.getRango_ini()));
               // txt_HastaFecha.setValue(Integer.toString(lrf.getRango_fin()));
              // Messagebox.show("Cargamos Periodo :["+_per+"]");
               this.cargaBandeja(_per);
               
               
               
            }
        }
    
    
    }

    
        private void cargaBandeja(String Periodo) {

        try {
           // sTextLocal = null;
//            dDesdeLocal = null;
//            dHastaLocal = null;
          //  sTextLocalResp = null;
//            dDesdeLocalResp = null;
//            dHastaLocalResp = null;

          //  eventos();

            lCondFinal = new ArrayList<APPResumenCondonacionesInforme>();
            lCondFilter = new ArrayList<APPResumenCondonacionesInforme>();
            lCondFinal = this.cond.GetCondonacionesAplicadasAndResumenInforme(cuenta,Periodo);
            lCondFilter = lCondFinal;
            
            for (APPResumenCondonacionesInforme kk:lCondFinal){
            
                        a1.setValue(kk.getTotal_rut());
            a2.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_CASTIGADO())).replaceFirst("Ch", ""));
            a3.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_VDE_SGN())).replaceFirst("Ch", ""));
           // a4.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_RECIBIDO_1())).replaceFirst("Ch", ""));
            a5.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_RECIBIDO_2())).replaceFirst("Ch", ""));
            a6.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_CAPITAL_CONDONADO())).replaceFirst("Ch", ""));
            a7.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_HONORARIO_RECIBE())).replaceFirst("Ch", ""));
           // a8.setValue(kk.get);
           a9.setValue(nfff.format(Double.parseDouble(kk.getTOTAL_COSTAS_PROVISION())).replaceFirst("Ch", ""));
            
            }

          //  cargaGrid(lCondFilter);
        //  //  muestraCol();
        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), no es posible mostrar las condonaciones aplicadas en este momento.", "Siscon-Administración", Messagebox.OK, Messagebox.INFORMATION);
        }
    }
   
    
    
    
}
