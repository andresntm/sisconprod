/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author excosoc
 */
public class MainFramePrincipalController extends SelectorComposer<Component> {

    //cosorio modificaci�n cierre minuta en sistema
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        SimpleDateFormat sdfFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date finMinuta = sdfFormat.parse("22/11/2018");
        Date hoy = new Date();

        int diasFaltantes = 0;
        long fltQuedan = (finMinuta.getTime() - hoy.getTime());

        if (fltQuedan > 0) {
            diasFaltantes = (int) (fltQuedan / 86400000);

            String strMensajeFinMinuta = "";

            Messagebox.show(strMensajeFinMinuta + "\n"
                    + "Quedan " + diasFaltantes + " d�as para el Dejar Opcional la minuta de condonaci�n."
                    + "Ay�danos a verificar el correcto funcionamiento del sistema ingresando la condonaci�n en Siscon y enviando adem�s la minuta para validar los datos ingresados", "Admin-Siscon", Messagebox.OK, Messagebox.INFORMATION);
        }
        /*else {
            diasFaltantes = (int) (fltQuedan / 86400000);

            String strMensajeFinMinuta = "";

            Messagebox.show(strMensajeFinMinuta + "\n"
                    + "quedan " + diasFaltantes + " d�as", "Admin-Siscon", Messagebox.OK, Messagebox.INFORMATION);
        }*/

    }

}
