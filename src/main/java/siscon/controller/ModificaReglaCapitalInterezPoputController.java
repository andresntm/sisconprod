/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author exesilr
 */
public class ModificaReglaCapitalInterezPoputController extends SelectorComposer<Component> {

    @Wire
    Grid BanEntrAnalista;
    final CondonacionInterfaz cond;
    ListModelList<CondonacionTabla> bandeCondTerminada;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox valueneg2;
    String ValorOriginal;
    String ValorOriginalInterez;

    @Wire
    Label idPorcentageCalculadoInterzqueda;

    @Wire
    Textbox id_nuevoporcentaje;
    @Wire
    Textbox id_nuevoporcentajeCapital;
    @Wire
    Textbox id_nuevoAbonoCapital;
    @Wire
    Textbox idInterezQueda;
    @Wire
    Textbox id_nuevoporcentajeHonorarios;
    @Wire
    Label idPorcentageCalculadoCap;
    @Wire
    Label valueneg3;

    @Wire
    Label idmuestra;
    
    
    @Wire
    Label id_cond;

    @Wire
    Label idInterezQueda2;
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
    @Wire
    Label idPorcentageCalculadoCapQueda;
    int interesPersonalizado;
    private List<CondonacionTabla> listCondonaciones;

    @WireVariable
    ListModelList<BandejaAnalista> myListModel;
    MvcConfig mmmm = new MvcConfig();

    public ModificaReglaCapitalInterezPoputController() throws SQLException {

        this.cond = new CondonacionImpl(mmmm.getDataSource());

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

        // id_nuevoporcentajeCapital
        // id_nuevoporcentajeCapital.setConstraint("no negative, min 0 max 90");
        //  id_nuevoporcentaje.setConstraint("no negative, min 0 max 100");
        interesPersonalizado = this.cond.getSumaInterezPersonalizado(interesPersonalizado, interesPersonalizado);
        ValorOriginal = valueneg.getValue().trim().length() > 0 ? valueneg.getValue() : "";
        ValorOriginalInterez = idInterezQueda.getValue().trim().length() > 0 ? idInterezQueda.getValue() : "";
        id_nuevoporcentajeCapital.setValue("0");
        id_nuevoporcentajeHonorarios.setValue("0");
        id_nuevoporcentaje.setValue("0");
        idPorcentageCalculadoCap.setVisible(false);
        idPorcentageCalculadoCapQueda.setVisible(false);
        idPorcentageCalculadoInterzqueda.setVisible(false);

    }

    @Listen("onChanging = textbox#id_nuevoAbonoCapital")
    public void change(InputEvent event) {
        //authService.logout();		
        //Executions.sendRedirect("/Login");
        if (event.getValue().trim().length() == 0) {

            idPorcentageCalculadoCapQueda.setVisible(false);
            idPorcentageCalculadoCap.setVisible(false);
            valueneg.setValue(this.ValorOriginal);
            return;
        }
        //String jj=valueneg.getValue();

        String jj = this.ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();
        float valor1 = Float.parseFloat(nn2);
        String iiiiii = id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);
        float resta = valor1 - valor2;

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        float porcentageNum = valor2 * 100 / valor1;
        float PorcentageQuedaNum = 100 - porcentageNum;
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        String porcentageStr = "%" + porcentageNum;

        /// aautocalculo del porcenjate interes,capital,honorario
        //restar vdes y sgn
        String jj1 = valueneg3.getValue();
        String nn11 = jj1.replace("$", "");
        String nn21 = nn11.replace(".", "");

        float valor11 = Float.parseFloat(nn21);

        float toalMenosVdes = valor2 - valor11;
        idmuestra.setValue(Float.toString(toalMenosVdes));

//        idInterezQueda2.
        //   if(toalMenosVdes-)
        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCap.setValue(porcentageStr);
        idPorcentageCalculadoCap.setVisible(true);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

    }

    @Listen("onChanging = textbox#id_nuevoporcentaje")
    public void changeinterez(InputEvent event) {

        if (event.getValue().trim().length() == 0) {
            idPorcentageCalculadoInterzqueda.setVisible(false);

            return;
        }

        // aceptamos Enteros como datos de ingreso
//        if (Integer.parseInt(event.getValue()) <= 0 || Integer.parseInt(event.getValue()) >= 101) {
//            id_nuevoporcentaje.setValue("0");
//            Messagebox.show("Rango Valido 0 a 100 %");
//
//            return;
//        }
        //aceptamos Float como datos de ingreso
        if (Float.parseFloat(event.getValue()) <= Float.parseFloat("0") || Float.parseFloat(event.getValue()) >= Float.parseFloat("101")) {
            id_nuevoporcentaje.setValue("0");
            Messagebox.show("Rango Valido 0 a 100 %");

            return;
        }

        String jj = ValorOriginalInterez;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();

        // valor ineteres $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        float PorcentageQuedaNum = 100 - Float.parseFloat(event.getValue());
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;
        String peso_format = nf.format(resta).replaceFirst("Ch", "");
        //    idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        //idPorcentageCalculadoCap.setValue(porcentageStr);
        //idPorcentageCalculadoCap.setVisible(true);
        //idPorcentageCalculadoCapQueda.setVisible(true);
        idPorcentageCalculadoInterzqueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoInterzqueda.setVisible(true);
        idInterezQueda.setValue(peso_format);

    }

    @Listen("onChanging = textbox#id_nuevoporcentajeCapital")
    public void changePorCapital(InputEvent event) {

        if (event.getValue().trim().length() == 0) {

            return;
        }
        if (Integer.parseInt(event.getValue()) <= 0 || Integer.parseInt(event.getValue()) >= 90) {
            id_nuevoporcentajeCapital.setValue("0");

            Messagebox.show("Rango Valido 0 a 90 %");

            return;
        }
        String jj = ValorOriginal;
        String nn1 = jj.replace("$", "");
        String nn2 = nn1.replace(".", "");
        String valuellll = event.getValue();

        // valor Capital $$
        float valor1 = Float.parseFloat(nn2);
        //String iiiiii=id_nuevoAbonoCapital.getValue();
        float valor2 = Float.parseFloat(valuellll);

        float porcentageToNum = valor2 * valor1 / 100;
        int PorcentageQuedaNum = 100 - Integer.parseInt(event.getValue());
        String PorcentageQuedaStr = "%" + PorcentageQuedaNum;
        // String porcentageStr = "%"+porcentageNum;
        float resta = valor1 - porcentageToNum;

        /// if hay abono
        if (id_nuevoAbonoCapital.getValue().trim().length() > 0) {

            resta = resta - Float.parseFloat(id_nuevoAbonoCapital.getValue());
        }

        String peso_format = nf.format(resta).replaceFirst("Ch", "");

        idPorcentageCalculadoCapQueda.setValue(PorcentageQuedaStr);
        idPorcentageCalculadoCapQueda.setVisible(true);
        valueneg.setValue(peso_format);

    }

}
