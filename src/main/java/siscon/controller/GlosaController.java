/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox;
import siscon.entidades.GlosaDET;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import config.MvcConfig;
import org.zkoss.zk.ui.event.InputEvent;

/**
 *
 * @author excosoc
 *
 *
 */
@SuppressWarnings("serial")
public class GlosaController extends SelectorComposer<Window> {

    /*Ejemplo integraci�n glosa en Padre:
    
    -- Variable
    
      private EventQueue eq;
      private List<Glosa> glosa; 
    
        -- Adjuntar al doAfterCompose o donde declaren los eventos del controller
    
        //Herencia ente Glosa y Padre
        eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
        eq.subscribe(new EventListener() {

            @Override
            @SuppressWarnings("unused")
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                List<GlosaDET> glosaDet = new ArrayList<GlosaDET>();
                glosaDet = (List<GlosaDET>) map.get("GlosaSS");

                if (glosaDet != null) {
                    if (!glosaDet.isEmpty()) {
                        glosa.setCant_glosas(glosaDet.size());
                        glosa.setDetalle(glosaDet);
                    }
                }
            }
        });
    
    --Evento que levanta el PopUp
    
    public void Glosa() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("GlosaSS", List<Glosa>);

        try {
            Window window = (Window) Executions.createComponents(
                    "Adjuntador/Glosa.zul", null, map
            );
            window.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar la glosa. \nError: " + e);
        }
    }
     */
    ////////////////////////////////////////////////////////////////////////////
    @Wire
    Textbox txt_Glosa;
    @Wire
    Window wnd_Glosa;
    @Wire
    Button btn_Cancelar;
    @Wire
    Button btn_Guardar;
    @Wire
    Label lbl_maxlength;
    ////////////////////////////////////////////////////////////////////////////
    private GlosaDET glosa;
    private EventQueue eq;
    private List<GlosaDET> listGlosaFinal = new ArrayList<GlosaDET>();
    private List<GlosaDET> listGlosaNew;
    private Session sess;
    private String Usuario = "admin";
    private GlosaDET glosaModif;
    private String modulo;
    private int id_colaborador;
    private int index_list;
    private UsuarioDAO uDAO;
    private MvcConfig mC;
    private int maxLenght;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        try {

            cargaPag();

        } catch (WrongValueException ex) {
            Messagebox.show("Sr(a). Usuario(a), Sysboj encontro un error al cargar la pagina .\nError: " + ex);
            wnd_Glosa.detach();
        }

    }

    private void cargaPag() {

        try {
            glosa = new GlosaDET();
            sess = Sessions.getCurrent();
            modulo = "";
            final Execution exec = Executions.getCurrent();
            mC = new MvcConfig();
            uDAO = new UsuarioJDBC(mC.getDataSource());

            eventos();

            UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

            Usuario = permisos.getCuenta();

            id_colaborador = uDAO.buscarColaborador(Usuario);

            if (exec.getArg().get("GlosaSS") != null) {
                glosa = (GlosaDET) exec.getArg().get("GlosaSS");
            }
            if (exec.getArg().get("modulo") != null) {
                modulo = (String) exec.getArg().get("modulo");
            }

            validaModulo();

            glosa.setUser(Usuario);
            glosa.setEdit(true);
            glosa.setModulo(modulo);
            glosa.setId_user(id_colaborador);

            if (glosa.getGlosa() != null) {
                if (!glosa.getGlosa().equals("")) {
                    txt_Glosa.setValue(glosa.getGlosa());
                    lbl_maxlength.setValue(Integer.toString(500 - glosa.getGlosa().length()));
                }
            }

            txt_Glosa.setPlaceholder("Glosa.");
            txt_Glosa.setFocus(true);

        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), error al adjuntar glosa.\nError: " + ex.toString());
        }
    }

    private void eventos() {

        txt_Glosa.addEventListener(Events.ON_FOCUS, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                txt_Glosa.select();
            }

        });

        txt_Glosa.addEventListener(Events.ON_CHANGING, new EventListener<InputEvent>() {
            public void onEvent(InputEvent event) throws Exception {
                Textbox tb = new Textbox();
                String valor = "";
                valor = event.getValue();
                maxLenght = 500;

                if ((valor.length() + 1) <= maxLenght) {
                    maxLenght = maxLenght - valor.length();
                    lbl_maxlength.setValue(Integer.toString(maxLenght));
                } else if (valor.length() == maxLenght) {
                    maxLenght = maxLenght - valor.length();
                    lbl_maxlength.setValue(Integer.toString(maxLenght));
                }

            }

        });

        wnd_Glosa.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                map.put("GlosaSS", glosa);
                eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", wnd_Glosa, map));
            }

        });

        btn_Guardar.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                try {
                    if (txt_Glosa.getValue().isEmpty()) {
                        Messagebox.show("Sr(a). Usuario(a), la glosa no se puede guardar en blanco. \n�Est� seguro(a) que desea adjuntar una glosa al sacabop?.", "Mensaje Administrador", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            @Override
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_Glosa.setFocus(true);
                                } else if (Messagebox.ON_NO.equals(event.getName())) {
                                    wnd_Glosa.detach();
                                }
                            }
                        });
                        return;
                    } else {
                        glosa.setGlosa(txt_Glosa.getValue());
                    }

                    map.put("GlosaSS", glosa);
                    eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onButtonClick", btn_Guardar, map));

                    wnd_Glosa.detach();
                } catch (WrongValueException e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } catch (Exception e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error al actualizar el registro. \n Error: " + e.toString());
                } finally {

                }

            }

        });

        btn_Cancelar.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                map.put("GlosaSS", glosa);
                eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_Cancelar, map));

                wnd_Glosa.detach();
            }

        });

    }

    private void validaModulo() {
        if (modulo.equals("AnSacabopView")) {
            btn_Guardar.setVisible(false);
            txt_Glosa.setDisabled(true);
            txt_Glosa.setStyle(txt_Glosa.getStyle() + ";color : black !important; font-weight : bold");
        }

    }

}
