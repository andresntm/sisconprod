/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import java.util.function.Consumer;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Div;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.Cond_Prorrogadas;
import siscon.entidades.Tipo_Prorroga;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;

/**
 *
 * @author excosoc
 */
public class EjeGesProrrogaController extends SelectorComposer<Component> {

    @Wire
    Window win_GesPror;
    @Wire
    Textbox txt_glosa;
    @Wire
    Combobox cmb_tipoProrroga;
    @Wire
    Textbox txt_userProrr;
    @Wire
    Datebox db_fechaComp;
    @Wire
    Textbox txt_dias;
    @Wire
    Textbox txt_gestion;
    @Wire
    Button btn_GesPror;
    @Wire
    Button btn_close;
    @Wire
    Caption cptn_titulo;
    @Wire
    Datebox db_prorro;
    @Wire
    Div div_datosProrroga;
    @Wire
    Div div_gestProrrEje;

    MvcConfig mvcC = new MvcConfig();
    Cond_Prorrogadas cP;
    private List<Tipo_Prorroga> lTP;
    private Tipo_Prorroga tProrroga;
    private ListModelList<Tipo_Prorroga> lML_TP;
    private List<Comboitem> lItemFull;
    private boolean formOk = false;
    private UsuarioPermiso permisos;
    private Session sess;
    private final CondonacionInterfaz cond;
    private int idcondonacion;
    private EventQueue eq;//cosoriosound
    private String modulo = null;

    public EjeGesProrrogaController() throws SQLException {
        cP = new Cond_Prorrogadas();
        tProrroga = new Tipo_Prorroga();
        this.cond = new CondonacionImpl(mvcC.getDataSource());
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        eventos();
        cargaPag();
        filtraVista();
    }

    private void filtraVista() {
        if (modulo.equals("Aplicacion")) {
            txt_gestion.setDisabled(true);
            db_fechaComp.setDisabled(true);
            btn_GesPror.setVisible(false);
        } else {
            txt_gestion.setDisabled(false);
            db_fechaComp.setDisabled(false);
            btn_GesPror.setVisible(true);
        }
    }

    private void cargaPag() {
        final Execution exec = Executions.getCurrent();
        Tipo_Prorroga tP = new Tipo_Prorroga();
        Comboitem final_Ci = new Comboitem();

        cP.get_X_Condonacion((Integer) exec.getArg().get("Id_Condonacion"));
        modulo = (String) exec.getArg().get("modulo");
        idcondonacion = cP.getFk_idCondonacion();
        cargaListTipProrrogas();

        for (Comboitem cI : lItemFull) {
            Tipo_Prorroga rTP = new Tipo_Prorroga();
            rTP = (Tipo_Prorroga) cI.getValue();

            if (rTP.getId_TipoProrroga() == cP.getFk_idTipoProrroga()) {
                final_Ci = cI;
                break;
            }
        }

        txt_glosa.setText(cP.getDetalle());
        cptn_titulo.setLabel("Gestionar prorroga de condonaci�n N�: " + cP.getFk_idCondonacion() + ".");
        txt_userProrr.setText(cP.getProrrogador());
        db_prorro.setValue(cP.getFecIngreso());

        if (cP.getGestionEje() != null) {
            if (!cP.getGestionEje().isEmpty()) {
                txt_gestion.setText(cP.getGestionEje());

            }
        }
        if (cP.getFecCompromiso() != null) {
            db_fechaComp.setValue(cP.getFecCompromiso());
        }
//        cmb_tipoProrroga.setSelectedItem(final_Ci);
    }

//    private void setVisible(){ pendiente de desarrollo
//        txt_glosa.setDisabled(true);
//        txt_userProrr.setDisabled(true);
//        cmb_tipoProrroga.setDisabled(true);
//        
//        if(cP.get)
//        
//    }
//    
    private void cargaListTipProrrogas() {
        lTP = new ArrayList<Tipo_Prorroga>();
        lTP = tProrroga.listAll();
        lML_TP = new ListModelList<Tipo_Prorroga>(lTP);
        lItemFull = new ArrayList<Comboitem>();
        int recorreTP = 0;

        for (Tipo_Prorroga tPLML : lML_TP) {
            if (tPLML.getId_TipoProrroga() == cP.getFk_idTipoProrroga()) {
                lML_TP.addSelection(lML_TP.get(recorreTP));
                break;
            }

            recorreTP += 1;
        }

//        lML_TP.addSelection(lML_TP.get(1));
//        cmb_tipoProrroga.setInplace(true);
        cmb_tipoProrroga.setReadonly(true);
        cmb_tipoProrroga.setAutodrop(true);
        cmb_tipoProrroga.setWidth("100%");
        cmb_tipoProrroga.setClass("input-group-block");
        cmb_tipoProrroga.setPlaceholder("Seleccione un tipo de prorroga.");
        cmb_tipoProrroga.setStyle("color : black !important; font-weight : bold");

        cmb_tipoProrroga.setModel(lML_TP);

        cmb_tipoProrroga.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String prorroga;
                Tipo_Prorroga tR = (Tipo_Prorroga) data;

                prorroga = tR.getDetalle();
                item.setLabel(prorroga);
                item.setValue(data);
                lItemFull.add(item);

            }

        });

    }

    private void eventos() {

        btn_GesPror.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                boolean resultado = false;
                if (validaForm()) {
                    cP.setGestionEje(txt_gestion.getText());
                    cP.setFecCompromiso(new java.sql.Date(db_fechaComp.getValue().getTime()));
                    if (cP.ingresaGesEje()) {

                        resultado = cond.SetCambiaEstadoCondonaciones("Ejecutivo.Prorrogadas", "Aplicador.Recepcion", permisos, idcondonacion, "Estado.ProrrogadaAplicador", "Estado.ProrrogaGestionada");
                        Messagebox.show("Sr(a) usuario(a), se gestiono correctamente la condonaci�n:" + cP.getId_Prorroga());
                    } else {
                        Messagebox.show("Sr(a) usuario(a), no se pudo gestionar la condonaci�n:" + cP.getId_Prorroga() + "\nContactar con efectividad comercial.", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
                    }
                    eq = EventQueues.lookup("GestProrroga", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onButtonClick", btn_GesPror, null));
                    win_GesPror.detach();
                }
            }

        });
        win_GesPror.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("GestProrroga", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", win_GesPror, map));

            }

        });

        btn_close.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                eq = EventQueues.lookup("GestProrroga", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_close, null));
                win_GesPror.detach();
            }

        });
        db_fechaComp.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Datebox db_valida = (Datebox) event.getTarget();
                SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
                String fecha_salida;

                fecha_salida = dt.format(db_valida.getValue());
                if (db_valida.getValue().before(new Date()) || db_valida.getValue().equals(new Date())) {
                    Messagebox.show("Sr(a) usuario(a), no se puede comprometer un pago para la fecha ingresada '" + fecha_salida + "' ya que es menor o igual a la fecha de hoy.");
                    db_fechaComp.setFocus(true);
                    db_fechaComp.setText(null);
                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(db_valida.getValue());

                    if (!isNotDayOfWeekend(calendar)) {
                        Messagebox.show("Sr(a) usuario(a), no se puede comprometer un pago para la fecha ingresada '" + fecha_salida + "' ya que es fin de semana.");
                        db_fechaComp.setFocus(true);
                        db_fechaComp.setText(null);
                    }
                }
            }

        });
    }

    private static boolean isNotDayOfWeekend(final Calendar fecha) {
        boolean bResultado = false;

        if (fecha.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            bResultado = true;
        } else if (fecha.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && fecha.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            bResultado = true;
        } else {
            bResultado = false;
        }

        return bResultado;
    }

    private void validaMsg(boolean msg) {
        this.formOk = msg;
    }

    private boolean validaForm() {
        boolean ok = false;

        ok = this.formOk;

        try {
            if (db_fechaComp.getText().isEmpty()) {
                Messagebox.show("Sr(a) usuario(a), debe agendar una fecha de pago para el cliente ");
                db_fechaComp.setFocus(true);
                ok = false;
                return ok;
            } else {
                ok = true;
            }

            if (txt_gestion.getText().isEmpty()) {
                Messagebox.show("Sr(a) usuario(a), la gesti�n se enviara sin una gesti�n.\n\n�Desea continuar?", "Siscon-Admin", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_YES.equals(event.getName())) {
                            validaMsg(true);
                        } else if (Messagebox.ON_YES.equals(event.getName())) {
                            txt_gestion.setFocus(true);
                            validaMsg(false);
                        }
                    }
                });

                ok = this.formOk;
            } else {
                ok = true;
            }
        } catch (Exception ex) {
            ok = false;
        } finally {

            return ok;
        }

    }
}
