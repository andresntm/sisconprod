/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Slider;
import org.zkoss.zul.Textbox;
import config.MvcConfig;
import java.sql.SQLException;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Div;
import org.zkoss.zul.Popup;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Spinner;
import siscon.entidades.Campania;
import siscon.entidades.Perfil;
import siscon.entidades.RangoFecha;
import siscon.entidades.RangoMonetario;
import siscon.entidades.ReglaVista;
import siscon.entidades.ReglasDet;
import siscon.entidades.ReglasEnc;
import siscon.entidades.Rel_campana_rel_perfil_regla;
import siscon.entidades.Rel_perfil_regla;
import siscon.entidades.Unidad_Medida;
import siscon.entidades.implementaciones.CampaniaImpl;
import siscon.entidades.implementaciones.PerfilImpl;
import siscon.entidades.implementaciones.RangoFechasImpl;
import siscon.entidades.implementaciones.RangoMonetarioImpl;
import siscon.entidades.implementaciones.ReglasImpl;
import siscon.entidades.implementaciones.Rel_campana_rel_perfil_reglaImpl;
import siscon.entidades.implementaciones.Rel_perfil_reglaImpl;
import siscon.entidades.implementaciones.Unidad_MedidaImpl;
import siscon.entidades.interfaces.CampaniaInterfaz;
import siscon.entidades.interfaces.PerfilInterfaz;
import siscon.entidades.interfaces.RangoFechasInterfaz;
import siscon.entidades.interfaces.RangoMonetarioInterfaz;
import siscon.entidades.interfaces.ReglasInterfaz;
import siscon.entidades.interfaces.Rel_campana_rel_perfil_reglaInterfaz;
import siscon.entidades.interfaces.Rel_perfil_reglaInterfaz;
import siscon.entidades.interfaces.Unidad_MedidaInterfaz;

/**
 *
 * @author excosoc
 */
public class MotorCondController extends SelectorComposer<Component> {

    /**
     * @return the lCmb_ReglasTotal
     */
    public ListModelList getlCmb_ReglasTotal() {
        return lCmb_ReglasTotal;
    }

    /**
     * @param lCmb_ReglasTotal the lCmb_ReglasTotal to set
     */
    public void setlCmb_ReglasTotal(ListModelList lCmb_ReglasTotal) {
        this.lCmb_ReglasTotal = lCmb_ReglasTotal;
    }

    @Wire
    Button btn_PerFec;
    @Wire
    Button btn_PerMont;
    @Wire
    Textbox txt_DesdeFecha;
    @Wire
    Textbox txt_HastaFecha;
    @Wire
    Textbox txt_DesdeMonto;
    @Wire
    Textbox txt_HastaMonto;
    @Wire
    Combobox cmb_Perfil;
    @Wire
    Combobox cmb_Campania;
    @Wire
    Combobox cmb_PerFechaRegla;
    @Wire
    Combobox cmb_PerMontoRegla;
    @Wire
    Button btn_AddReg;
    @Wire
    Grid grd_Reglas;
    @Wire
    Label lbl_IdRegla;
    @Wire
    Textbox txt_Regla;
    @Wire
    Slider sld_Capital;
    @Wire
    Label lbl_Capital;
    @Wire
    Slider sld_Interes;
    @Wire
    Label lbl_Interes;
    @Wire
    Slider sld_Honorario;
    @Wire
    Label lbl_Honorario;
    @Wire
    Row row;
    @Wire
    Rows rows;
    @Wire
    private Popup pop_DetalleRangoF;
    @Wire
    private Label lbl_DesdeF;
    @Wire
    private Label lbl_HastaF;
    @Wire
    private Popup pop_DetalleRangoM;
    @Wire
    private Label lbl_DesdeM;
    @Wire
    private Label lbl_HastaM;

    /**
     * @return the lCmb_RangFec
     */
    public ListModelList getlCmb_RangFec() {
        return lCmb_RangFec;
    }

    /**
     * @param lCmb_RangFec the lCmb_RangFec to set
     */
    public void setlCmb_RangFec(ListModelList lCmb_RangFec) {
        this.lCmb_RangFec = lCmb_RangFec;
    }

    /**
     * @return the lCmb_RangMoney
     */
    public ListModelList getlCmb_RangMoney() {
        return lCmb_RangMoney;
    }

    /**
     * @param lCmb_RangMoney the lCmb_RangMoney to set
     */
    public void setlCmb_RangMoney(ListModelList lCmb_RangMoney) {
        this.lCmb_RangMoney = lCmb_RangMoney;
    }

    /**
     * @return the lRangoFecha
     */
    public List<RangoFecha> getlRangoFecha() {
        return lRangoFecha;
    }

    /**
     * @param lRangoFecha the lRangoFecha to set
     */
    public void setlRangoFecha(List<RangoFecha> lRangoFecha) {
        this.lRangoFecha = lRangoFecha;
    }

    /**
     * @return the lRangoMonetario
     */
    public List<RangoMonetario> getlRangoMonetario() {
        return lRangoMonetario;
    }

    /**
     * @param lRangoMonetario the lRangoMonetario to set
     */
    public void setlRangoMonetario(List<RangoMonetario> lRangoMonetario) {
        this.lRangoMonetario = lRangoMonetario;
    }

    ////////////////////////////////////////////////////////////////////////////
    private MvcConfig conex = new MvcConfig();
    private ReglasEnc reglasEnc;
    private List<ReglasEnc> lReglasEnc = new ArrayList<ReglasEnc>();
    private ReglasDet reglasDet;
    private List<ReglasDet> lReglasDet = new ArrayList<ReglasDet>();
    private RangoFecha rangoFecha;
    private List<RangoFecha> lRangoFecha = new ArrayList<RangoFecha>();
    private RangoMonetario rangoMonetario;
    private List<RangoMonetario> lRangoMonetario = new ArrayList<RangoMonetario>();
    private Unidad_Medida unidadMenida;
    private List<Unidad_Medida> lUnidadMenida = new ArrayList<Unidad_Medida>();
    private Perfil perfil;
    private List<Perfil> lPerfil = new ArrayList<Perfil>();
    private Rel_perfil_regla relPerRel;
    private List<Rel_perfil_regla> lRelPerRel = new ArrayList<Rel_perfil_regla>();
    private Rel_campana_rel_perfil_regla campPerReg;
    private List<Rel_campana_rel_perfil_regla> lCampPerReg = new ArrayList<Rel_campana_rel_perfil_regla>();
    private Campania campanaia;
    private List<Campania> lCampania = new ArrayList<Campania>();
    private List<ReglaVista> lRegla = new ArrayList<ReglaVista>();
    private List<ReglaVista> lReglaTotal = new ArrayList<ReglaVista>();
    private int id_Campania = 0;
    private int id_x100Cap;
    private int id_x100Int;
    private int id_x100Hon;

    ////////////////////////////////////////////////////////////////////////////
    private ListModelList lCmb_RangFec;
    private ListModelList lCmb_RangMoney;
    private ListModelList lGrd_Reglas;
    private ListModelList lCmb_Perfil;
    private ListModelList lCmb_Campania;
    private ListModelList lCmb_ReglasTotal;
    ////////////////////////////////////////////////////////////////////////////
    private ReglasInterfaz reglasInterfaz;
    private RangoFechasInterfaz RangoFechaInterfaz;
    private RangoMonetarioInterfaz rangoMonetarioInterfaz;
    private Unidad_MedidaInterfaz unidadMedidaInterfaz;
    private PerfilInterfaz perfilInterfaz;
    private Rel_perfil_reglaInterfaz relPerRelInterfaz;
    private Rel_campana_rel_perfil_reglaInterfaz campPerRegInterfaz;
    private CampaniaInterfaz campaniaInterfaz;
    ////////////////////////////////////////////////////////////////////////////
    private ReglasImpl reglasImpl;
    private RangoFechasImpl RangoFechaImpl;
    private RangoMonetarioImpl rangoMonetarioImpl;
    private Unidad_MedidaImpl unidadMedidaImpl;
    private Rel_perfil_reglaImpl relPerRelImpl;
    private PerfilImpl perfilImpl;
    private Rel_campana_rel_perfil_reglaImpl campPerRegImpl;
    private CampaniaImpl campaniaImpl;

    public MotorCondController() throws SQLException {
        reglasInterfaz = new ReglasImpl(conex.getDataSource());
        RangoFechaInterfaz = new RangoFechasImpl(conex.getDataSource());
        rangoMonetarioInterfaz = new RangoMonetarioImpl(conex.getDataSource());
        unidadMedidaInterfaz = new Unidad_MedidaImpl(conex.getDataSource());
        perfilInterfaz = new PerfilImpl(conex.getDataSource());
        relPerRelInterfaz = new Rel_perfil_reglaImpl(conex.getDataSource());
        campPerRegInterfaz = new Rel_campana_rel_perfil_reglaImpl(conex.getDataSource());
        campaniaInterfaz = new CampaniaImpl(conex.getDataSource());
        this.id_x100Cap = conex.getParmCond().getId_x100Capital();
        this.id_x100Int = conex.getParmCond().getId_x100Interes();
        this.id_x100Hon = conex.getParmCond().getId_x100Honorario();

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        cargaTotalReglas();
        cargaPerfil();
        cargaCampania();
        cargaPerCond();
        cargaMontCond();
    }

    public void cargaReglasGrid(int id_ReglaEnc) {
        int _id = 0;
        String _det = "";
        boolean _activo = false;
        int _porCap = 0;
        int _porInt = 0;
        int _porHon = 0;
        int _rangoF = 0;
        int _rangoM = 0;

        reglasEnc = reglasInterfaz.getReglasXIdEnc(id_ReglaEnc);
        lReglasDet = reglasInterfaz.getReglasDetXIdEnc(id_ReglaEnc);

        _id = reglasEnc.getId_regla();
        _det = reglasEnc.getDesc();
        _rangoF = reglasEnc.getId_rangof();
        _rangoM = reglasEnc.getId_rangom();
        _activo = reglasEnc.isActivo();

        for (ReglasDet relD : lReglasDet) {

            if (relD.getId_regla() == reglasEnc.getId_regla()) {
                //Porcentaje capital
                if (relD.getId_valor() == id_x100Cap) {
                    _porCap = Math.round(relD.getPorcentaje_condonacion());
                }

                //Porcentaje capital
                if (relD.getId_valor() == id_x100Int) {
                    _porInt = Math.round(relD.getPorcentaje_condonacion());
                }

                //Porcentaje capital
                if (relD.getId_valor() == id_x100Hon) {
                    _porHon = Math.round(relD.getPorcentaje_condonacion());
                }
            }

        }
        lRegla.add(new ReglaVista(_id, _det, _activo, _porCap, _porInt, _porHon, _rangoF, _rangoM, getlRangoFecha(), getlRangoMonetario(), getlCmb_ReglasTotal()));
    }

    public void cargaPerfil() {

        try {
            lPerfil.clear();
            lPerfil = perfilInterfaz.getPerfil();
            lCmb_Perfil = new ListModelList<Perfil>(lPerfil);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_Perfil.setModel(lCmb_Perfil);
            cmb_Perfil.applyProperties();
            cmb_Perfil.setVisible(false);
            cmb_Perfil.setVisible(true);
            cmb_Perfil.setPlaceholder("Seleccione perfil.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaPerfilXCampania() {
        int id_Rep = 0;
        try {
            lPerfil.clear();
            lCmb_Perfil.clear();
            //Carga combo con perfiles en campa�as
            for (Rel_perfil_regla perReg : lRelPerRel) {
                if (perReg.getId_perfil() != id_Rep) {
                    lPerfil.add(perfilInterfaz.getPerfilXId(perReg.getId_perfil()));
                    id_Rep = perReg.getId_perfil();
                }
            }

//            lPerfil = perfilInterfaz.getPerfil();
            lCmb_Perfil = new ListModelList<Perfil>(lPerfil);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_Perfil.setModel(lCmb_Perfil);
            cmb_Perfil.applyProperties();
            cmb_Perfil.setVisible(false);
            cmb_Perfil.setVisible(true);
            cmb_Perfil.setPlaceholder("Seleccione perfil.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaTotalReglas() {
        int _id = 0;
        String _det = "";
        boolean _activo = false;
        int _porCap = 0;
        int _porInt = 0;
        int _porHon = 0;
        int _rangoF = 0;
        int _rangoM = 0;

        try {
            lReglaTotal.clear();
            lReglasEnc = reglasInterfaz.getReglasEnc();
            lReglasDet = reglasInterfaz.getReglasDet();

            for (ReglasEnc relE : lReglasEnc) {

                _id = relE.getId_regla();
                _det = relE.getDesc();
                _rangoF = relE.getId_rangof();
                _rangoM = relE.getId_rangom();
                _activo = relE.isActivo();

                for (ReglasDet relD : lReglasDet) {

                    if (relD.getId_regla() == relE.getId_regla()) {
                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Cap) {
                            _porCap = Math.round(relD.getPorcentaje_condonacion());
                        }

                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Int) {
                            _porInt = Math.round(relD.getPorcentaje_condonacion());
                        }

                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Hon) {
                            _porHon = Math.round(relD.getPorcentaje_condonacion());
                        }
                    }
                }

                lReglaTotal.add(new ReglaVista(_id, _det, _activo, _porCap, _porInt, _porHon, _rangoF, _rangoM, getlRangoFecha(), getlRangoMonetario()));
            }
            setlCmb_ReglasTotal(new ListModelList<ReglaVista>(lReglaTotal));
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaCampania() {

        try {
            lCampania.clear();
            lCampania = campaniaInterfaz.getCampania();
            lCmb_Campania = new ListModelList<Campania>(lCampania);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_Campania.setModel(lCmb_Campania);
            cmb_Campania.applyProperties();
            cmb_Campania.setVisible(false);
            cmb_Campania.setVisible(true);
            cmb_Campania.setPlaceholder("Seleccione campa�a.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar las camapa�as. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaPerCond() {

        try {
            getlRangoFecha().clear();
            setlRangoFecha(RangoFechaInterfaz.getRangoFecha());
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los periodos de condonaci�n. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaMontCond() {

        try {
            getlRangoMonetario().clear();
            setlRangoMonetario(rangoMonetarioInterfaz.getRangoMonetario());
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los rangos monetarios. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    @Listen("onClick = #btn_AddReg")
    public void onClick$btn_AddReg(Event event) {
        List<ReglaVista> lRelVistFiltro = new ArrayList<ReglaVista>();
        ListModelList<ReglaVista> lRelVistTotalNewReg = new ListModelList<ReglaVista>();
        int cont = 0;
        try {

            for (ReglaVista relV : lRegla) {
                if (relV.getId_regla() == 0) {
                    Messagebox.show("Estimado(a) Colaborador(a), Ya hay una fila disponible para anexar una regla al perfil.");
                    return;
                }
            }
            //Filtramos los registros ya asociados
            for (ReglaVista relVFiltro : lReglaTotal) {
                if (lRegla.size() >= (cont + 1)) {
                    if (lRegla.get(cont).getId_regla() != relVFiltro.getId_regla()) {

                        lRelVistFiltro.add(relVFiltro);

                    } else if (lRegla.get(cont).getId_regla() == relVFiltro.getId_regla()) {
                        cont += 1;
                    }
                }

            }

            lReglaTotal.clear();
            lReglaTotal = lRelVistFiltro;

            lRelVistTotalNewReg = new ListModelList<ReglaVista>(lReglaTotal);

            lRegla.add(new ReglaVista(0, "", false, 0, 0, 0, 0, 0, getlRangoFecha(), getlRangoMonetario(), lRelVistTotalNewReg));
            lGrd_Reglas.clear();
            lGrd_Reglas = new ListModelList<ReglaVista>(lRegla);
            grd_Reglas.setModel(lGrd_Reglas);
            grd_Reglas.setVisible(false);
            grd_Reglas.setVisible(true);

        } catch (Exception ex) {
            Messagebox.show("Estimado(a) Colaborador(a), ha ocurrido un error al agregar una fila a las reglas.");
        }

    }

    @Listen("onSelect = #cmb_Perfil")
    public void onSelect$cmb_Perfil(Event event) {
        cmb_Perfil.getSelectedIndex();

        for (final Perfil per : lPerfil) {
            if (per.getDescripcion().equals(cmb_Perfil.getValue())) {
                lRegla.clear();
                for (Rel_perfil_regla perReg : lRelPerRel) {
                    //El perfil seleccionado esta dentro de la campa�a
                    if (perReg.getId_perfil() == per.getId_perfil()) {
                        cargaReglasGrid(perReg.getId_regla());

                    }
                }
            }
        }

        lGrd_Reglas = new ListModelList<ReglaVista>(lRegla);

        grd_Reglas.setModel(lGrd_Reglas);
        grd_Reglas.setRowRenderer(new RowRenderer<ReglaVista>() {
            private Div dv_1;
            private Div dv_2;
            private Cell cel;
            private Label lbl_Porc;

            @SuppressWarnings("unchecked")
            public void render(Row row, final ReglaVista data, int index) throws Exception {
                Div dv_carga;
                ///////////////////////////////////////////////////////////////////////
                //cargamos atributos a los controles de la grilla
                // cargamos controles a fila de la grilla para mostrar y trabajar
                row.setId(Integer.toString(index));

                final Label lbl_id = new Label();
                lbl_id.setValue(Integer.toString(data.getId_regla()));
                int _id = Integer.parseInt(lbl_id.getValue());
                cel = new Cell();
                cel.setAlign("right");
                //cel.appendChild(lbl_id);
                lbl_id.setParent(cel);

                row.appendChild(cel);
                ///////////
                final Combobox cmb_ReglasTotal = new Combobox();
                cmb_ReglasTotal.setModel(data.getlReglaVistaTotal());
                cmb_ReglasTotal.setInplace(true);
                cmb_ReglasTotal.setReadonly(true);
                cmb_ReglasTotal.setAutodrop(true);
                cmb_ReglasTotal.setWidth("100%");
                cmb_ReglasTotal.setClass("input-group-block");
                cmb_ReglasTotal.setPlaceholder("Seleccione una regla para el Perfil.");
                cmb_ReglasTotal.setStyle("color : black !important; font-weight : bold");

                //no se permite modificar el nombre de la regla
                if (data.getId_regla() != 0) {
                    cmb_ReglasTotal.setDisabled(true);
                } else {
                    cmb_ReglasTotal.setDisabled(false);
                }

                cmb_ReglasTotal.setItemRenderer(new ComboitemRenderer() {
                    public void render(Comboitem item, Object data1, int index) throws Exception {
                        item.setLabel(data.getlReglaVistaTotal().get(index).getDescripcion());
                    }

                });

                cel = new Cell();
                cel.setAlign("left");
                //cel.setParent(cmb_RanF);
                cmb_ReglasTotal.setParent(cel);
                cmb_ReglasTotal.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        try {
                            Combobox fechaR = (Combobox) event.getTarget();
                            String detRV = fechaR.getValue();
                            relPerRel = new Rel_perfil_regla();
                            campPerReg = new Rel_campana_rel_perfil_regla();
                            Row rRV = (Row) fechaR.getParent().getParent();
                            Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                            int id_regis = Integer.parseInt(id_registro.getValue());

                            for (ReglaVista relVTotal : lReglaTotal) {
                                if (relVTotal.getDescripcion().equals(detRV)) {

                                    for (Perfil insPer : lPerfil) {
                                        if (insPer.getDescripcion().equals(cmb_Perfil.getValue())) {
                                            relPerRel.setId_perfil(insPer.getId_perfil());
                                            break;
                                        }
                                    }

                                    relPerRel.setId_regla(relVTotal.getId_regla());

                                    relPerRel.setId_perfil_regla(relPerRelInterfaz.insertRel_perfil_regla(relPerRel));

                                    campPerReg.setId_rel_perfil_regla(relPerRel.getId_perfil_regla());

                                    for (Campania campPR : lCampania) {
                                        if (campPR.getDescripcion().equals(cmb_Campania.getValue())) {
                                            campPerReg.setId_campana(campPR.getId_campania());
                                            break;
                                        }
                                    }

                                    campPerRegInterfaz.insertRel_campana_rel_perfil_regla(campPerReg);

                                    break;
                                }
                            }

                            lRelPerRel = relPerRelInterfaz.getPerRegXId(id_Campania);

                            for (final Perfil per : lPerfil) {
                                if (per.getDescripcion().equals(cmb_Perfil.getValue())) {
                                    lRegla.clear();
                                    for (Rel_perfil_regla perReg : lRelPerRel) {
                                        //El perfil seleccionado esta dentro de la campa�a
                                        if (perReg.getId_perfil() == per.getId_perfil()) {
                                            cargaReglasGrid(perReg.getId_regla());

                                        }
                                    }
                                }
                            }

                            lGrd_Reglas = new ListModelList<ReglaVista>(lRegla);

                            grd_Reglas.setModel(lGrd_Reglas);
                            grd_Reglas.setVisible(false);
                            grd_Reglas.setVisible(true);

                        } catch (NumberFormatException ex) {
                            Messagebox.show("Estimado(a) Colaborador(a), error al grabar la regla en la campa�a y el perfil.");
                        } catch (WrongValueException ex) {
                            Messagebox.show("Estimado(a) Colaborador(a), error al grabar la regla en la campa�a y el perfil.");
                        } catch (Exception ex) {
                            Messagebox.show("Estimado(a) Colaborador(a), error al grabar la regla en la campa�a y el perfil.");
                        }

                    }
                });
                row.appendChild(cel);
                ///////////

                dv_2 = new Div();
                dv_2.setClass("col-md-2");
                dv_2.setAlign("left");
                dv_2.setStyle("margin-left:-20px");
                lbl_Porc = new Label("%");
                lbl_Porc.setParent(dv_2);

                dv_1 = new Div();
                dv_1.setClass("col-md-10");
                final Spinner spn_cap = new Spinner(data.getPorCap());
                spn_cap.setInplace(true);
                spn_cap.setWidth("100%");

                if (_id == 0) {
                    spn_cap.setDisabled(true);
                } else {
                    spn_cap.setDisabled(false);
                }

                spn_cap.setConstraint("no negative, min 0 max 100");
                //dv_1.setParent(spn_cap);
                spn_cap.setParent(dv_1);
                spn_cap.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        Spinner cap = (Spinner) event.getTarget();
                        float detRV = cap.getValue();

                        Row rRV = (Row) cap.getParent().getParent().getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());
                        boolean actualiza = false;

                        if (id_regis != 0) { //actualiza

                            for (ReglasDet detReg : lReglasDet) {

                                if (detReg.getId_regla() == id_regis
                                        && detReg.getId_valor() == id_x100Cap) {
                                    reglasDet = new ReglasDet();
                                    reglasDet.setId_regla(id_regis);
                                    reglasDet.setId_valor(id_x100Cap);
                                    reglasDet.setPorcentaje_condonacion(detRV);
                                    reglasDet.setId_valor_regla(detReg.getId_valor_regla());

                                    UpdRegla(reglasDet);
                                    actualiza = true;
                                    break;
                                }
                            }

                            if (!actualiza) {
                                reglasDet = new ReglasDet();
                                reglasDet.setId_regla(id_regis);
                                reglasDet.setId_valor(id_x100Cap);
                                reglasDet.setPorcentaje_condonacion(detRV);
                                addRegla(reglasDet);
                            }

                        }

                    }
                });
                dv_carga = new Div();
                dv_carga.setClass("row");
                //dv_carga.setParent(dv_1);
                dv_1.setParent(dv_carga);
                //dv_carga.setParent(dv_2);
                dv_2 = new Div();
                dv_2.setClass("col-md-2");
                dv_2.setAlign("left");
                dv_2.setStyle("margin-left:-20px");
                lbl_Porc = new Label("%");
                lbl_Porc.setParent(dv_2);
                dv_2.setParent(dv_carga);
                cel = new Cell();
                //cel.setParent(dv_carga);
                dv_carga.setParent(cel);

                row.appendChild(cel);
                ///////////
                dv_1 = new Div();
                dv_1.setClass("col-md-10");
                final Spinner spn_Int = new Spinner(data.getPorInt());
                spn_Int.setInplace(true);
                spn_Int.setWidth("100%");

                if (_id == 0) {
                    spn_Int.setDisabled(true);
                } else {
                    spn_Int.setDisabled(false);
                }

                spn_Int.setConstraint("no negative, min 0 max 100");
                //dv_1.setParent(spn_Int);
                spn_Int.setParent(dv_1);
                spn_Int.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        Spinner inte = (Spinner) event.getTarget();
                        float detRV = inte.getValue();

                        Row rRV = (Row) inte.getParent().getParent().getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());
                        boolean actualiza = false;

                        if (id_regis != 0) { //actualiza

                            for (ReglasDet detReg : lReglasDet) {

                                if (detReg.getId_regla() == id_regis
                                        && detReg.getId_valor() == id_x100Int) {
                                    reglasDet = new ReglasDet();
                                    reglasDet.setId_regla(id_regis);
                                    reglasDet.setId_valor(id_x100Int);
                                    reglasDet.setPorcentaje_condonacion(detRV);
                                    reglasDet.setId_valor_regla(detReg.getId_valor_regla());

                                    UpdRegla(reglasDet);
                                    actualiza = true;
                                    break;
                                }
                            }

                            if (!actualiza) {
                                reglasDet = new ReglasDet();
                                reglasDet.setId_regla(id_regis);
                                reglasDet.setId_valor(id_x100Int);
                                reglasDet.setPorcentaje_condonacion(detRV);
                                addRegla(reglasDet);
                            }

                        }

                    }
                });
                dv_carga = new Div();
                dv_carga.setClass("row");
                dv_1.setParent(dv_carga);
                //dv_carga.setParent(dv_1);
                //dv_carga.setParent(dv_2);
                dv_2 = new Div();
                dv_2.setClass("col-md-2");
                dv_2.setAlign("left");
                dv_2.setStyle("margin-left:-20px");
                lbl_Porc = new Label("%");
                lbl_Porc.setParent(dv_2);
                dv_2.setParent(dv_carga);
                cel = new Cell();
                //cel.setParent(dv_carga);
                dv_carga.setParent(cel);

                row.appendChild(cel);
                ///////////
                dv_1 = new Div();
                dv_1.setClass("col-md-10");
                final Spinner spn_Hon = new Spinner(data.getPorHon());
                spn_Hon.setInplace(true);
                spn_Hon.setWidth("100%");

                if (_id == 0) {
                    spn_Hon.setDisabled(true);
                } else {
                    spn_Hon.setDisabled(false);
                }

                spn_Hon.setConstraint("no negative, min 0 max 100");
                //dv_1.setParent(spn_Hon);
                spn_Hon.setParent(dv_1);
                spn_Hon.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        Spinner hon = (Spinner) event.getTarget();
                        float detRV = hon.getValue();

                        Row rRV = (Row) hon.getParent().getParent().getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());
                        boolean actualiza = false;

                        if (id_regis != 0) { //actualiza

                            for (ReglasDet detReg : lReglasDet) {

                                if (detReg.getId_regla() == id_regis
                                        && detReg.getId_valor() == id_x100Hon) {
                                    reglasDet = new ReglasDet();
                                    reglasDet.setId_regla(id_regis);
                                    reglasDet.setId_valor(id_x100Hon);
                                    reglasDet.setPorcentaje_condonacion(detRV);
                                    reglasDet.setId_valor_regla(detReg.getId_valor_regla());

                                    UpdRegla(reglasDet);
                                    actualiza = true;
                                    break;
                                }
                            }

                            if (!actualiza) {
                                reglasDet = new ReglasDet();
                                reglasDet.setId_regla(id_regis);
                                reglasDet.setId_valor(id_x100Hon);
                                reglasDet.setPorcentaje_condonacion(detRV);
                                addRegla(reglasDet);
                            }

                        }

                    }
                });
                dv_carga = new Div();
                dv_carga.setClass("row");
                //dv_carga.setParent(dv_1);
                dv_1.setParent(dv_carga);
                //dv_carga.setParent(dv_2);
                dv_2 = new Div();
                dv_2.setClass("col-md-2");
                dv_2.setAlign("left");
                dv_2.setStyle("margin-left:-20px");
                lbl_Porc = new Label("%");
                lbl_Porc.setParent(dv_2);
                dv_2.setParent(dv_carga);
                cel = new Cell();
                //cel.setParent(dv_carga);
                dv_carga.setParent(cel);

                row.appendChild(cel);
                ///////////

                final Combobox cmb_RanF = new Combobox();
                cmb_RanF.setModel(data.getlRangoF());
                cmb_RanF.setInplace(true);
                cmb_RanF.setReadonly(true);
                cmb_RanF.setAutodrop(true);
                cmb_RanF.setWidth("130px");

                if (_id == 0) {
                    cmb_RanF.setDisabled(true);
                } else {
                    cmb_RanF.setDisabled(false);
                }

                cmb_RanF.setClass("input-group-block");
                cmb_RanF.setPlaceholder("Periodo Cond.");
                cmb_RanF.setItemRenderer(new ComboitemRenderer() {
                    public void render(Comboitem item, Object data1, int index) throws Exception {
                        item.setLabel(data.getlRangoF().get(index).getDesc());
                        item.setDescription("De " + data.getlRangoF().get(index).getRango_ini()
                                + " a " + data.getlRangoF().get(index).getRango_fin() + " meses.");

                    }

                });

                cel = new Cell();

                cel.setAlign(
                        "left");
                //cel.setParent(cmb_RanF);
                cmb_RanF.setParent(cel);

                cmb_RanF.addEventListener(Events.ON_SELECT, new EventListener<Event>() {

                    public void onEvent(Event event) throws Exception {

                        Combobox fechaR = (Combobox) event.getTarget();

                        String detRV = fechaR.getValue();

                        Row rRV = (Row) fechaR.getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());

                        for (final RangoFecha lrf : lRangoFecha) {
                            if (lrf.getDesc().equals(fechaR.getValue())) {
                                reglasEnc = new ReglasEnc();
                                reglasEnc.setId_rangof(lrf.getId_rangof());

                                for (ReglaVista regVi : lRegla) {
                                    if (regVi.getId_regla() == id_regis) {
                                        reglasEnc.setId_regla(id_regis);
                                        reglasEnc.setDesc(regVi.getDescripcion());
                                        reglasEnc.setActivo(regVi.isActivo());
                                        reglasEnc.setId_rangom(regVi.getRangoM());
                                        break;
                                    }
                                }

                                UpdRegla(reglasEnc);
                                break;
                            }
                        }

                    }
                });

                row.appendChild(cel);
                ///////////

                final Combobox cmb_RanM = new Combobox();

                cmb_RanM.setModel(data.getlRangoM());
                cmb_RanM.setInplace(true);
                cmb_RanM.setReadonly(true);
                cmb_RanM.setAutodrop(true);
                cmb_RanM.setWidth("130px");

                if (_id == 0) {
                    cmb_RanM.setDisabled(true);
                } else {
                    cmb_RanM.setDisabled(false);
                }

                cmb_RanM.setClass("input-group-block");
                cmb_RanM.setPlaceholder("Monto Cond.");
                cmb_RanM.setItemRenderer(new ComboitemRenderer() {

                    public void render(Comboitem item, Object data1, int index) throws Exception {
                        item.setLabel(data.getlRangoM().get(index).getDesc());
                        item.setDescription("Desde " + data.getlRangoM().get(index).getMonto_ini()
                                + "$ hasta " + data.getlRangoM().get(index).getMonto_fin() + "$.");
                    }

                });

                cel = new Cell();
                cel.setAlign("left");
                //cel.setParent(cmb_RanM);
                cmb_RanM.setParent(cel);
                cmb_RanM.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        Combobox fechaM = (Combobox) event.getTarget();
                        String detRV = fechaM.getValue();

                        Row rRV = (Row) fechaM.getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());

                        for (final RangoMonetario lrm : lRangoMonetario) {
                            if (lrm.getDesc().equals(fechaM.getValue())) {
                                reglasEnc = new ReglasEnc();
                                reglasEnc.setId_rangom(lrm.getId_rangom());

                                for (ReglaVista regVi : lRegla) {
                                    if (regVi.getId_regla() == id_regis) {
                                        reglasEnc.setId_regla(id_regis);
                                        reglasEnc.setDesc(regVi.getDescripcion());
                                        reglasEnc.setActivo(regVi.isActivo());
                                        reglasEnc.setId_rangof(regVi.getRangoF());
                                        break;
                                    }
                                }

                                UpdRegla(reglasEnc);
                                break;
                            }
                        }

                    }
                });

                row.appendChild(cel);
                ///////////
                final Checkbox chb_Activo = new Checkbox();

                if (_id == 0) {
                    chb_Activo.setChecked(true);
                } else {
                    chb_Activo.setChecked(data.isActivo());
                }

                cel = new Cell();
                cel.setAlign("center");
                //cel.setParent(chb_Activo);
                chb_Activo.setParent(cel);
                chb_Activo.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
                    public void onEvent(Event event) throws Exception {
                        Checkbox activo = (Checkbox) event.getTarget();
                        boolean actRV = activo.isChecked();

                        Row rRV = (Row) activo.getParent().getParent();
                        Label id_registro = (Label) rRV.getChildren().get(0).getChildren().get(0);
                        int id_regis = Integer.parseInt(id_registro.getValue());

                        for (final ReglaVista act : lRegla) {
                            if (id_regis != 0) { //actualiza

                                if (act.getId_regla() == id_regis) {
                                    reglasEnc = new ReglasEnc();
                                    reglasEnc.setId_regla(id_regis);
                                    reglasEnc.setDesc(act.getDescripcion());
                                    reglasEnc.setActivo(actRV);
                                    reglasEnc.setId_rangof(act.getRangoF());
                                    reglasEnc.setId_rangom(act.getRangoM());
                                    UpdRegla(reglasEnc);
                                    break;
                                }
                            } else if (id_regis == 0) { //nuevo registro

                            }
                        }

                    }
                });
                row.appendChild(cel);

            }
        }
        );

        grd_Reglas.renderAll();
        grd_Reglas.setVisible(false);
        grd_Reglas.setVisible(true);
    }

    @Listen("onSelect = #cmb_PerFechaRegla")
    public void onSelect$cmb_PerFechaRegla(Event event) {
        cmb_PerFechaRegla.getSelectedIndex();

        for (final RangoFecha lrf : lRangoFecha) {
            if (lrf.getDesc().equals(cmb_PerFechaRegla.getValue())) {
                txt_DesdeFecha.setValue(Integer.toString(lrf.getRango_ini()));
                txt_HastaFecha.setValue(Integer.toString(lrf.getRango_fin()));
            }
        }

    }

    @Listen("onSelect = #cmb_PerMontoRegla")
    public void onSelect$cmb_PerMontoRegla(Event event) {
        cmb_PerMontoRegla.getSelectedIndex();

        for (final RangoMonetario lrm : lRangoMonetario) {
            if (lrm.getDesc().equals(cmb_PerMontoRegla.getValue())) {
                txt_DesdeMonto.setValue(Integer.toString(lrm.getMonto_ini()));
                txt_HastaMonto.setValue(Integer.toString(lrm.getMonto_fin()));
            }
        }

    }

    @Listen("onSelect = #cmb_Campania")
    public void onSelect$cmb_Campania(Event event) {
        //Al cargar la campa�a esta busca los perfiles que estan relacionados con ella,
        //y carga los perfiles en el cmb de perfiles
        cmb_Campania.getSelectedIndex();

        for (final Campania camp : lCampania) {
            if (camp.getDescripcion().equals(cmb_Campania.getValue())) {
                lRelPerRel = relPerRelInterfaz.getPerRegXId(camp.getId_campania());
                id_Campania = camp.getId_campania();
                cargaPerfilXCampania();

            }
        }

    }

    private void addRegla(ReglasEnc regEnc) {
        reglasInterfaz.insertReglasEnc(regEnc);
    }

    private void addRegla(ReglasDet regDet) {
        reglasInterfaz.insertReglasDet(regDet);
    }

    private void UpdRegla(ReglasEnc regEnc) {
        reglasInterfaz.updateReglasEnc(regEnc);
    }

    private void UpdRegla(ReglasDet regDet) {
        reglasInterfaz.updateReglasDet(regDet);
    }

}
