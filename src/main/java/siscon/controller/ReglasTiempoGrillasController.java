/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;


import siscon.controller.*;
import config.MvcConfig;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.sourceforge.jtds.jdbc.DateTime;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.DetalleCliente;
import siscon.entidades.ReglaTiempoGrilla;
import siscon.entidades.Reparos;
import siscon.entidades.SisconinfoperacionV2;
import siscon.entidades.TrackingRowTomadaUsr;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.UsuariosSM;
import siscon.entidades.usuario;
import siscore.entidades.implementaciones.UsuariosSMImpl;
import siscore.entidades.interfaces.UsuariosSMInterfaz;
import siscore.genral.MetodosGenerales;

/**
 *
 * @author excosoc
 */
public class ReglasTiempoGrillasController extends SelectorComposer<Window> {

    @Wire
    private Grid grd_InformeCond;
    @Wire
    private Radiogroup rdg_tip1;
    @Wire
    private Radio rdb_retail;
    @Wire
    private Radio rdb_pyme;
    ListModelList<UsuariosSM> bandeCondTerminada;
    private List<UsuariosSM> listCondonaciones;
    private List<CondonacionTabla2> listCondonaciones2;
    @Wire
    private Column clm_0;
    @Wire
    private Column clm_1;
    @Wire
    private Column clm_2;
    @Wire
    private Column clm_3;
    @Wire
    private Column clm_4;
    @Wire
    private Column clm_5;
    @Wire
    private Column clm_6;
    @Wire
    private Column clm_7;
    @Wire
    private Column clm_8;
    @Wire
    private Column clm_9;
    String modulo;
    @Wire
    Span btn_search;
    
    @Wire
    Textbox id_tiempo;
    @Wire
    Label spanTiempo;
    
   @Wire
    Combobox  cmb_tipoTime;
   @Wire
    Grid grd_Reglas;
   
   @Wire
    Grid grd_Reglas2;

    private List<UsuariosSM> lCondFinal;
    private String sTextLocal;
    @Wire
    private String sTextLocalResp;
    @Wire
    Textbox txt_filtra;
    public String AreaTrabajo;
    MetodosGenerales MT;
    ListModelList<Reparos> ListReparosCondonacionModel;
    ListModelList<SisconinfoperacionV2> ListOperacionesInformeModel;
    Session session;
    String cuenta;
    UsuarioPermiso permisos;
    ListModelList<DetalleCliente> ListDetOperModel;
    Window window;
    int idCondonacion;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    final UsuariosSMInterfaz cond;
    DetalleOperacionesClientes detoper;
    Session sess;
    private List<UsuariosSM> lCondFilter;
    
    int idUsuarioRow;
    private List<TrackingRowTomadaUsr> listTrackRow;
    private List<TrackingRowTomadaUsr> insertTrackRow;
    ListModelList<TrackingRowTomadaUsr> listTrackRow2;
    String rutaTracking="siscore.controller";
    String bandejaTracking="ColaboradoresBCI.zul";

    UsuarioDAO _usu;
    List<UsuariosSM> lRep = new ArrayList<UsuariosSM>();
//lRep= lRep = new ArrayList<UsuariosSM>();

    MvcConfig mmmm = new MvcConfig();
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private List<UsuariosSM> lInforme;
    private ListModelList<UsuariosSM> lMLInforme;
    private UsuariosSM informe;
    
    private List<usuario> strListName;
    private String strUserTake;
    
    ListModelList<ReglaTiempoGrilla> bandeCondTerminada2;
    private List<ReglaTiempoGrilla> listCondonaciones22;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.

        cargaPag();

        
        listCondonaciones22 = cond.getReglaGrilla();
        bandeCondTerminada2 = new ListModelList<ReglaTiempoGrilla>(listCondonaciones22);
        
        listTrackRow = cond.getTrackingRow();
        listTrackRow2 = new ListModelList<TrackingRowTomadaUsr>(listTrackRow);

        grd_Reglas.setModel(bandeCondTerminada2);
        grd_Reglas2.setModel(listTrackRow2);

        session = Sessions.getCurrent();
        sess = Sessions.getCurrent();
        //     lRep = cond.getInfClienV2();
        //muestra = new ListModelList<UsuariosSM>(lRep);
        //  grillainformes.setModel(muestra);
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuenta = permisos.getCuenta();//user.getAccount(); 
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        System.out.println("AreaTrabajo-> "+AreaTrabajo);
        System.out.println("cuenta-> "+cuenta);
       // eventos();

    }

    public void cargaPag() {

        int getTime = cond.getTimeRow();
        String tiempo=String.valueOf(getTime);
        spanTiempo.setValue(tiempo);
        lInforme = new ArrayList<UsuariosSM>();
        informe = new UsuariosSM();
        //  lInforme = informe.list_Informe_Pyme();
        
        
        
         cmb_tipoTime.setModel(comboTiempos());
 
       
        // eventos();
    }

   public ListModelList comboTiempos(){
       
        ListModelList lm3 = new ListModelList();
        lm3.add("5 Minutos");
        lm3.add("10 Minutos");
        lm3.add("20 Minutos");
        lm3.add("30 Minutos");
        lm3.add("40 Minutos");
        
        return lm3;
       
   }

    

    public ReglasTiempoGrillasController() throws SQLException {

        // this.cond = new CondonacionImpl(mmmm.getDataSourceProduccionMYSQL());
        this.cond = new UsuariosSMImpl(mmmm.getDataSource());
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this.MT = new MetodosGenerales();
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.modulo = "EfectividadComercial";

    }

   

   
    public void asignacion() {
//        String val1 = id_tiempo.getValue();
//        String val2 = id_tiempo.getText();
        int colab = permisos.getId_colaborador();
        //int tiempo = Integer.parseInt(val2);
//        System.out.println("LLega Asignacion1 " + val1);
//        System.out.println("LLega Asignacion2 " + val2);
        int tiempo = 0;
        
        int timpoCOmbo = cmb_tipoTime.getSelectedIndex();
        System.out.println("INDEX--> "+timpoCOmbo);
        
        
        if(timpoCOmbo == 0){
             tiempo = 5;
        }else if(timpoCOmbo == 1){
             tiempo = 10;
        }else if(timpoCOmbo == 2){
             tiempo = 20;
        }else if(timpoCOmbo == 3){
             tiempo = 30;
        }else if(timpoCOmbo == 4){
             tiempo = 40;
        }
        
        boolean updFlag = cond.updateTimeRow(colab, tiempo);

        if (updFlag) {
            System.out.println("UPDATEA!");
              Messagebox.show("Registro Actualizado!" );
              int getTime = cond.getTimeRow();
              String tiempos=String.valueOf(getTime);
        spanTiempo.setValue(tiempos);
        listCondonaciones22 = cond.getReglaGrilla();
        bandeCondTerminada2 = new ListModelList<ReglaTiempoGrilla>(listCondonaciones22);

        grd_Reglas.setModel(bandeCondTerminada2);
        } else {
            System.out.println("NO UPDATEA!");
            Messagebox.show("Registro No Actualizado!" );
        }

    }
    
    
    public void SeteoTracking(int idCond) throws ParseException {
        
        System.out.println("LLEGO SeteoTracking "+idCond);
        int estado=0;
        boolean setEstado = cond.updateEstadoRow(idCond,estado);
        if(setEstado){
            listTrackRow = cond.getTrackingRow();
            listTrackRow2 = new ListModelList<TrackingRowTomadaUsr>(listTrackRow);

            grd_Reglas2.setModel(listTrackRow2);
            Messagebox.show("Registro Liberado", "Informacion", Messagebox.OK, Messagebox.INFORMATION);
        }else{
            Messagebox.show("Error al liberar registro", "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }
    
    
                
             
        
      
            
            
}
