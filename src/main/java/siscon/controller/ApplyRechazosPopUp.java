/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;
import siscon.entidades.Cond_Rechazadas;
import siscon.entidades.Tipo_Rechazo;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.interfaces.CondonacionInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
@SuppressWarnings("serial")
public class ApplyRechazosPopUp extends SelectorComposer<Component> {

    @Wire
    private Window win_rechazos;
    @Wire
    private Grid grid_rechazo;
    @Wire
    private Label lbl_idCond;
    @Wire
    private Textbox txt_detalle;
    @Wire
    private Button btn_rechazo;
    @Wire
    private Button btn_close;
    @Wire
    private Combobox cmb_tipoRechazo;
    ////////////////////////////////////////////////////////////////////////////
    UsuarioPermiso permisos;
    Session sess;
    final CondonacionInterfaz cond;
    MvcConfig mvcC = new MvcConfig();
    int id_condonacionRechazo = 0;
    private EventQueue eq;//cosoriosound
    Window thisWindow;
    private List<Tipo_Rechazo> lTR;
    private Tipo_Rechazo tRechazo;
    private ListModelList<Tipo_Rechazo> lMLTR;
    private List<Comboitem> lItemFull;
    private Cond_Rechazadas cRechazo;
    private boolean validForm;

    private int id_int,id_cap,id_hon;

    public ApplyRechazosPopUp() throws SQLException {
        this.cond = new CondonacionImpl(mvcC.getDataSource());

    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        thisWindow = (Window) comp;
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");

        cargaPag();
    }

    private void cargaPag() {
        final Execution exec = Executions.getCurrent();
        cRechazo = new Cond_Rechazadas();
        tRechazo = new Tipo_Rechazo();
        try {
            eventos();

            if (exec.getArg().get("id_condonacion") != null) {
                id_condonacionRechazo = (Integer) exec.getArg().get("id_condonacion");
                this.setId_int((Integer) exec.getArg().get("id_int"));
                this.setId_cap((Integer) exec.getArg().get("id_cap"));
                this.setId_hon((Integer) exec.getArg().get("id_hon"));
            }

            if (id_condonacionRechazo != 0) { //percarga rechazo
                cRechazo.setFk_idCondonacion(id_condonacionRechazo);
                cRechazo.setFk_idColaborador(permisos.getId_colaborador());
            }

            cargaListTipRechazos();

        } catch (Exception ex) {
            SisCorelog("Error al cargar popUp rechazos. Error:" + ex.getMessage());
        }
    }

    public void rechaza() {
        boolean resultado = false;
        int idcondonacion;
        idcondonacion = 0;

        cRechazo.setDetalle(txt_detalle.getText());
        idcondonacion = id_condonacionRechazo;
        ActualizaTrackinEstado(idcondonacion);
        resultado = this.cond.SetCambiaEstadoCondonaciones("Aplicacion.Recepcion", "Ejecutivo.Rechazadas", permisos, idcondonacion, "Estado.Aprobada", "Estado.RechazadaAplicador");

        if (resultado = true) {
            cRechazo.setDetalle(txt_detalle.getText());
             cRechazo.insert();
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),this.getId_int());
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),this.getId_cap());
            cRechazo.insertPorcentajes(cRechazo.getId_Rechazo(),this.getId_hon());
           // Messagebox.show("Guardo los Id de Condonacion.: int  [" + this.getId_int() + "] cap ["+this.getId_cap()+"] hon ["+this.getId_hon()+"]");
        } else {
            Messagebox.show("Error al rechazar  la Condonaci�n Nro.: [" + idcondonacion + "]");
        }

    }

    private void cargaListTipRechazos() {
        lTR = new ArrayList<Tipo_Rechazo>();
        lTR = tRechazo.listAll();
        lMLTR = new ListModelList<Tipo_Rechazo>(lTR);
//        cmb_tipoRechazo.setInplace(true);
        cmb_tipoRechazo.setReadonly(true);
        cmb_tipoRechazo.setAutodrop(true);
        cmb_tipoRechazo.setWidth("100%");
        cmb_tipoRechazo.setClass("input-group-block");
        cmb_tipoRechazo.setPlaceholder("Seleccione un tipo de rechazo.");
        cmb_tipoRechazo.setStyle("color : black !important; font-weight : bold");

        cmb_tipoRechazo.setItemRenderer(new ComboitemRenderer<Object>() {
            public void render(Comboitem item, Object data, int index) throws Exception {
                String rechazo;
                Tipo_Rechazo tR = (Tipo_Rechazo) data;

                rechazo = tR.getDetalle();
                item.setLabel(rechazo);
                item.setValue(data);
            }

        });

        cmb_tipoRechazo.setModel(lMLTR);

        lItemFull = cmb_tipoRechazo.getItems();

    }

    //Cosorio
    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }

    private void eventos() {
        cmb_tipoRechazo.addEventListener(Events.ON_SELECT, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                Combobox cB = (Combobox) event.getTarget();
//                final Listbox lst_Xcol = (Listbox) event.getTarget();
                Tipo_Rechazo xTR = new Tipo_Rechazo();
                Comboitem lITR = new Comboitem();
                tRechazo = new Tipo_Rechazo();

                lITR = cB.getSelectedItem();
                xTR = (Tipo_Rechazo) lITR.getValue();

                tRechazo = xTR;
                cRechazo.settR(xTR);

            }
        });

        win_rechazos.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("Rechazo", EventQueues.DESKTOP, false);
                eq.publish(new Event("onClose", win_rechazos, map));

            }

        });

        btn_close.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                final HashMap<String, Object> map = new HashMap<String, Object>();

                eq = EventQueues.lookup("Rechazo", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_close, map));
                win_rechazos.detach();
            }
        });

        btn_rechazo.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {

                validaForm();

            }

        });
    }

    private void valido(boolean estado) {
        if (estado) {
            validForm = estado;
            validaForm();
        }
    }

    private void validaForm() {
        int valido = 0;//0 = false; 1 = true; 2  = valida contra variable validForm
        final Event ev;
        try {
            if (validForm) {
                valido = 1;
            } else {
                if (cmb_tipoRechazo.getSelectedItem() == null) {
                    Messagebox.show("Sr(a) usuario(a), para continuar debe seleccionar un tipo de rechazo.", "Siscon-rechazos", Messagebox.OK, Messagebox.EXCLAMATION);
                    cmb_tipoRechazo.setFocus(true);
                    valido = 0;
                } else {
                    if (txt_detalle.getText().equals("")) {
                        Messagebox.show("Sr(a) usuario(a), la condonaci�n se rechazara sin un detalle.\n�Desea ingresar un detalle?", "Siscon-rechazos", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener<Event>() {
                            public void onEvent(Event event) throws Exception {
                                if (Messagebox.ON_YES.equals(event.getName())) {
                                    txt_detalle.setFocus(true);
                                    txt_detalle.select();
                                } else {
                                    valido(true);
                                }
                            }
                        });
                        valido = 2;
                    } else {
                        valido = 1;
                    }
                }
            }

        } catch (Exception ex) {
            valido = 0;
        } finally {
            if (valido == 1) {
                final HashMap<String, Object> map = new HashMap<String, Object>();
                rechaza();
                map.put("rechazoCond", cRechazo);
                eq = EventQueues.lookup("Rechazo", EventQueues.DESKTOP, false);
                eq.publish(new Event("onButtonClick", btn_rechazo, map));
                win_rechazos.detach();
            }
        }

    }

    
    
    
    public int getId_int() {
        return id_int;
    }

    public void setId_int(int id_int) {
        this.id_int = id_int;
    }

    public int getId_cap() {
        return id_cap;
    }

    public void setId_cap(int id_cap) {
        this.id_cap = id_cap;
    }

    public int getId_hon() {
        return id_hon;
    }

    public void setId_hon(int id_hon) {
        this.id_hon = id_hon;
    }
    
}
