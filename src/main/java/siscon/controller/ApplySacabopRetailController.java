/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import config.MvcConfig;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sourceforge.jtds.jdbc.DateTime;
import org.zkoss.lang.Objects;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.AdjuntarDET;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Cliente;
import siscon.entidades.Colaborador;
import siscon.entidades.Cond_Prorrogadas;
import siscon.entidades.Cond_Rechazadas;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionSiscon;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.DetalleCliente;
import siscon.entidades.GarantiasCliente;
import siscon.entidades.GlosaDET;
import siscon.entidades.GlosaENC;
import siscon.entidades.JudicialCliente;
import siscon.entidades.Regla;
import siscon.entidades.SacaBop;
import siscon.entidades.SbifCliente;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsrProvision;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.ClienteSisconJDBC;
import siscon.entidades.implementaciones.ColaboradorJDBC;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;
import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.CondonadorJDBC;
import siscon.entidades.implementaciones.EstadosJDBC;
import siscon.entidades.implementaciones.GarantiasClienteJDBC;
import siscon.entidades.implementaciones.GeneralAppJDBC;
import siscon.entidades.implementaciones.OperacionJDBC;
import siscon.entidades.implementaciones.ProvisionJDBC;
import siscon.entidades.implementaciones.ReglasImpl;
import siscon.entidades.implementaciones.SbifClienteJDBC;
import siscon.entidades.implementaciones.TrackinJDBC;
import siscon.entidades.implementaciones.UsuarioJDBC;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.GarantiasClienteInterfaz;
import siscon.entidades.interfaces.ReglasInterfaz;
import siscon.entidades.interfaces.SbifClienteInterfaz;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import siscore.genral.MetodosGenerales;
import wstokenPJ.NewJerseyClient;

/**
 *
 * @author exesilr
 */
@SuppressWarnings("serial")
public class ApplySacabopRetailController extends SelectorComposer<Window> {

    @Wire
    Label lbl_TioAux2;
    @Wire
    Label lbl_OpOrg2;
    @Wire
    Label lbl_CuMor2;
    @Wire
    Label lbl_SalIns2;
    @Wire
    Label lbl_CuPag2;
    @Wire
    Label lbl_CuPact2;
    @Wire
    Label lbl_DiaMor2;
    @Wire
    Label lbl_TioAux;
    @Wire
    Label lbl_OpOrg;
    @Wire
    Label lbl_CuMor;
    @Wire
    Label lbl_SalIns;
    @Wire
    Label lbl_CuPag;
    @Wire
    Label lbl_CuPact;
    @Wire
    Label lbl_DiaMor;
    @Wire
    Label lbl_Condona;
    @Wire
    Label negoc;
    @Wire
    Textbox valueneg;
    @Wire
    Textbox nro_condonacion;
    @Wire
    Label viewrutcliente;
    @Wire
    Grid Grid_Sacabop;
    @Wire
    Grid Grid_SacabopXX;
    @Wire
    Textbox id_fechahoy;
    @Wire
    Textbox id_nomcliente;
    @Wire
    Textbox id_oficinaorigen;
    @Wire
    Textbox id_TotalSumaCondonaCapital;
    @Wire
    Textbox id_TotalSumaCapital;
    @Wire
    Textbox id_TotalSumaRecibeCapital;
    @Wire
    Textbox id_saldototalmoroso;
    float id_saldototalmorosoFloat;
    @Wire
    Label idTotalRecibe;
    @Wire
    Label id_TotalTotal;
    @Wire
    Label id_TotalCondona;
    @Wire
    Label idValorUF;
    @Wire
    Label id_TotlaCapital;
    @Wire
    Label idtotalhonor;
    @Wire
    Textbox id_TotalSumaInteres;
    @Wire
    Textbox id_TotalSumaCondonaInteres;
    @Wire
    Textbox id_TotalSumaRecibeInteres;

    // identificadores de textbod judiciales en la grilla
    @Wire
    Textbox id_TotalSumaHonorJud;
    @Wire
    Textbox id_TotalSumaHonorCondona;
    @Wire
    Textbox id_TotalSumaHonorRecibe;

    // Totoales Parciales Recibe Color GREEN
    @Wire
    Textbox id_TotoalParcialCapital;
    @Wire
    Textbox id_TotoalParcialInteres;
    @Wire
    Textbox id_TotoalParcialhonor;

    //Totoales Parciales Condona Color Red
    @Wire
    Textbox id_TotoalParcialCondonaCapital;
    @Wire
    Textbox id_TotoalParcialCondonaInteres;
    @Wire
    Textbox id_TotoalParcialCondonaHono;

    /// Totoales parciales Capital YELLOW
    @Wire
    Textbox id_TotoalParcialCapita;
    @Wire
    Textbox id_TotoalParcialCapitaInteres;
    @Wire
    Textbox id_TotoalParcialCapitaHonor;

    /// variables del llenado de info de condonacion
    @Wire
    Textbox id_PrimerMesDeCondonacion;
    @Wire
    Textbox id_MesMasAntiguoCastigo;

    @Wire
    Textbox id_AtribucionEjecutiva;
    @Wire
    Textbox id_porcentajeCondoEjecutiva;

    @Wire
    Textbox id_PuedeCondonarOnline;

    @Wire
    Textbox id_rangoFechaCondonacion;
    @Wire
    Panel panelgridddd;

    @Wire
    Label id_msgeCona;

    @Wire
    Button btn_enviaSacaBopg;
    @Wire
    Button id_modificaRegla;
    @Wire
    Button id_modificaRegla2;
    @Wire
    Button btn_GenrarCondonacion;
    @Wire
    Button id_Prorroga;
    @Wire
    Vlayout vlayoutmensajje;
    @Wire
    Label id_TotalVDEs;
    @Wire
    Button btn_aplica;
    @Wire
    Window id_windowsMessajje;

    @Wire
    Grid grd_GastosHono;
    @Wire
    Label id_SumaProvision;
    @Wire
    Textbox txt_TipoEspecial;
    @Wire
    Checkbox chk_especial;
    MvcConfig mmmm = new MvcConfig();
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    @WireVariable
    ListModelList<SacaBop> sacaboj;
    final DetalleOperacionesClientes detoper;
    final JudicialClienteInterz JudCliente;
    final CondonacionInterfaz cond;
    final ReglasInterfaz _reglas;
    final GarantiasClienteInterfaz _garantias_cliente;
    final SbifClienteInterfaz _deuda_sbif;
    String FechaPrimerCastigo;
    int mesescastigomasantiguo;
    Condonacion CurrentCondonacion;
    List<DetalleCliente> ListDetOper = null;
    //variables sumatorias totales de cada columna por orden 1->A  etc...
    int RutClienteFormateado;
    float totalA;
    int MaximoMesCastigo;
    int MaximoMesCastigoOnPrevio;
    float totalB;
    float totalC;
    float totalD;
    float totalE;
    float totalF;
    float totalG;
    float totalH;
    float totalI;
    float sumaMoraTotal;
    final ClienteInterfazImpl clienteinfo;
    @WireVariable
    Cliente InfoCliente;
    ClienteSisconJDBC _cliente;
    GeneralAppJDBC _ggJDBC;
    TrackinJDBC _track;
    CondonadorJDBC _condonadorJDBC;
    OperacionJDBC _operJDBC;
    OperacionJDBC _operJDBCsiscon;
    Session sess;
    String AreaTrabajo;
    String cuenta;
    String Nombre;
    @Wire
    Window capturawin;
    String rutcliente;
    float ReglaInteresPorcentajeCondonacion;
    float reglaCapitalPorcentajeCndonacion;
    float ReglaHonorarioPorcentajeCondonacion;
    List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();
    List<DetalleCliente> detClientSacabop = new ArrayList<DetalleCliente>();
    UsuarioPermiso permisos;
    @Wire
    Window id_wSacabop;
    Window window;
    float pppp2;
    float SumaTotalVDEs = 0;
    float InteresBanco = 0;
    int id_valor_regla = 0;
    int id_valor_regla_capital = 0;
    int id_valor_regla_Honorario = 0;
    float UfDia;
    float ufAplica;
    int OpCont;
    float ppppppp;
    float pppp3;
    int condonacion = 0;
    NewJerseyClient _token;
    UsuarioDAO _usu;
    ColaboradorJDBC _colabo;
    private MetodosGenerales metodo;
    @WireVariable
    ListModelList<GarantiasCliente> ListGarantiasClienteModel;
    ListModelList<SbifCliente> ListSbifClienteClienteModel;
    //SbifCliente
    NumberFormat nf;
    float SumaTotalProvision = 0;
    ProvisionJDBC _prov;
    private List<UsrProvision> listProvisiones;

    private EventQueue eq;//cosorio
    private GlosaENC glosa; //cosorio
    private String modulo = "AnSacabopView";//cosorio
    private AdjuntarENC adjunta;
    private Cliente client;
    private List<Media> fileAdjuntos;//cosorio
    private float sizeFiles = 0;
    ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();
    private GlosaDET glosaDet = new GlosaDET();
    private CondonacionSiscon cS = new CondonacionSiscon();
    EstadosJDBC estado;
    int id_usrmod_int, id_usrmod_cap, id_usrmod_hon;
    private CondonacionesEspeciales cE;
    Session session;
    @Wire
    Window GG;
        @Wire
    Div dangercamp;
        
        String NombreModulo;
        String cuentaModulo;
         String AreaTrabajoModulo;
            @Wire
    Label labeldangerofer;
    // float _tmpHonorariocondona=0;
    // float _tmpHonorariocondona=0;
    int puedeEvaluar = 0;

    private void cargaVista() {

        cS.setId_condonacion(condonacion);
        cS.getCondonacion();

        if ((cS.getId_estado() == estado._buscarIDEstado("EJE-GES-PRO")) || (cS.getId_estado() == estado._buscarIDEstado("EJE-GES-PRO"))) {
            id_Prorroga.setVisible(true);
        } else {
            id_Prorroga.setVisible(false);
        }

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        



        
        
        estado = new EstadosJDBC();

        capturawin = (Window) comp;
        Locale.setDefault(new Locale("es", "CL"));

        eventos();

        lbl_TioAux2.setValue("C�digo <br>Operaci�n".replace("<br>", "\r\n"));
        lbl_OpOrg2.setValue("Operaci�n <br>original".replace("<br>", "\r\n"));
        lbl_SalIns2.setValue("Saldo <br>Insoluto".replace("<br>", "\r\n"));
        lbl_DiaMor2.setValue("D�as <br>Mora".replace("<br>", "\r\n"));
        nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
        sess = Sessions.getCurrent();
        permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        cuentaModulo = permisos.getCuenta();//user.getAccount();
        NombreModulo = permisos.getNombre();
        AreaTrabajoModulo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        usuario usu;
        Colaborador colabo;
        /// traemos data del ejecutivo negociados
        int rut_ejecutivo = Integer.parseInt(Sessions.getCurrent().getAttribute("rutEjecutivo").toString());
        if (rut_ejecutivo > 0) {
            usu = this._usu.buscarUsuarioRutDi(rut_ejecutivo);

            this.condonacion = Integer.parseInt(Sessions.getCurrent().getAttribute("idcondonacion").toString());
            colabo = this._colabo.buscarColaboradorRutDi(rut_ejecutivo);
        } else {

            Messagebox.show("AnSacabopController1 Error no hay rut ejecutivo");
            return;

        }

        cuenta = usu.getAccount();//user.getAccount();
        Nombre = usu.getFullName();
        AreaTrabajo = ((AreaTrabajo) permisos.getArea()).getCodigo();
        detClientCond = (List<DetalleCliente>) sess.getAttribute("detClientCond");

        rutcliente = Sessions.getCurrent().getAttribute("rutcliente").toString();//Executions.getCurrent().getParameter("rutcliente");
        String rutS = Sessions.getCurrent().getAttribute("rutEjecutivo").toString();
        String mm = rutcliente.replace(".", "");
        String[] ParteEntera = mm.split("-");
        puedeEvaluar = 2; //cond.isCliente(rutS);
        
        ListDetOper = detoper.Cliente(Integer.parseInt(ParteEntera[0]), cuentaModulo, (puedeEvaluar == 2) ? 1 : 0);
        if (ListDetOper == null) {
            Messagebox.show("Error No Existe Detalle Operaciones.");
            return;

        }
        //// calculo de suma vdes

        for (DetalleCliente detCliHono : ListDetOper) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {

                listModelHono.add(detCliHono);
                SumaTotalVDEs = SumaTotalVDEs + (float) detCliHono.getSaldoinsoluto();

            }
        }
        this.listProvisiones = this._prov.listProvisionXrut(rutcliente);
        for (UsrProvision prov : listProvisiones) {

            //  listModelHono.add(detCliHono);
            SumaTotalProvision = SumaTotalProvision + (float) prov.getMonto();

        }

        grd_GastosHono.setModel(listModelHono);

        grd_GastosHono.setVisible(false);
        grd_GastosHono.setVisible(true);

        glosa = new GlosaENC();//cosorio incia variable limpia
        adjunta = new AdjuntarENC();

        //Herencia entre Sacabop e hijos
        {//Bloque anonimo creado por cosorio

            //Herencia ente Glosa y Sacabop
            eq = EventQueues.lookup("Glosa", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    glosaDet = (GlosaDET) map.get("GlosaSS");

                    if (glosaDet != null) {
                        glosa.setCant_glosas(1);

                    } else {
                        glosa.setCant_glosas(0);
                    }
                }
            });

            //Herencia ente Adjuntar y Sacabop
            eq = EventQueues.lookup("Adjuntar", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    List<AdjuntarDET> det = new ArrayList<AdjuntarDET>();
                    det = (List<AdjuntarDET>) map.get("ListAdjuntoSS");
                    sizeFiles = (Float) map.get("tama�o");

                    if (det != null) {
                        if (!det.isEmpty()) {
                            adjunta.setNumero_archivos(det.size());
                            adjunta.setTama�o_total(String.format("%.4f", sizeFiles) + "Mb");
                            adjunta.setaDet(det);
                        }
                    }

                }

            });

            //Herencia ente Rechazos y Sacabop
            eq = EventQueues.lookup("Rechazo", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    Cond_Rechazadas det = new Cond_Rechazadas();
                    det = (Cond_Rechazadas) map.get("rechazoCond");

                    eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onClose", id_wSacabop, null));
                    capturawin.detach();
                }
            });

            //Herencia ente Prorrogados y Sacabop
            eq = EventQueues.lookup("Prorroga", EventQueues.DESKTOP, true);
            eq.subscribe(new EventListener() {

                @Override
                @SuppressWarnings("unused")
                public void onEvent(Event event) throws Exception {
                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
                    Cond_Prorrogadas det = new Cond_Prorrogadas();
                    det = (Cond_Prorrogadas) map.get("prorrogaCond");

                    eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onClose", id_wSacabop, null));
                    capturawin.detach();
                }
            });
            //Herencia ente Prorrogados y Sacabop
            eq = EventQueues.lookup("GestProrroga", EventQueues.DESKTOP, true);
//            eq.subscribe(new EventListener() {
//
//                @Override
//                @SuppressWarnings("unused")
//                public void onEvent(Event event) throws Exception {
//                    final HashMap<String, Object> map = (HashMap<String, Object>) event.getData();
//                    Cond_Prorrogadas det = new Cond_Prorrogadas();
//                    det = (Cond_Prorrogadas) map.get("prorrogaCond");
//
//                    eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
//                    eq.publish(new Event("onClose", id_wSacabop, null));
//                    capturawin.detach();
//                }
//            });
        };

        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        int ii = 0;

        List<String> Operaciones = new ArrayList<String>();
        // Messagebox.show("FloatUF [+"+this.UfDia+"+]");
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;
        if (detClientCond.size() > 0) {

            for (DetalleCliente seleccion : detClientCond) {

                if (!detClientSacabop.contains(seleccion)) {
                    Operaciones.add(seleccion.getOperacion());
                    detClientSacabop.add(seleccion);

                }

            }
        }

        ListDetOper.clear();
        ListDetOper = detClientSacabop;

        RutClienteFormateado = Integer.parseInt(ParteEntera[0]);
        List<JudicialCliente> ListJudCliente = JudCliente.JudClie(Integer.parseInt(ParteEntera[0]), cuenta);
        InfoCliente = clienteinfo.infocliente(Integer.parseInt(ParteEntera[0]));

        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
        ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

        sacaboj = new ListModelList<SacaBop>();
        String oper = "NULL";
        String MeseCatigo = "NULL";
        String FechaCastigo = "14-01-2012 09:29:58";
        //SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = new Date();
        String[] parse;
        //d2=format.parse(d2.toString());
        DateTime NN;
        // creamos instancia de condonacion      
String OO="";
        CurrentCondonacion = new Condonacion(rutcliente, cuenta, sess.getWebApp().toString());

        /// Busqueda de Fecha Castigo mas Antigua
        //////##### AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            if (ListDetOperModel.get(i) != null) {
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                OO=OO+"------------"+ddddd;
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigoOnPrevio) {
                    this.MaximoMesCastigoOnPrevio = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigoOnPrevio);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
            }
        }
     //  Messagebox.show("MaximoMesCastigoOnPrevio-:{"+MaximoMesCastigoOnPrevio+"} MeseCatigo:{"+MeseCatigo+"}d2 :{"+d2+"}d1:["+d1+"] FechaCastigo: {"+FechaCastigo+"+    OO :{"+OO+"}");
        CurrentCondonacion.setAdjuntar(cond.GetAdjuntoCond(condonacion));
        adjunta = CurrentCondonacion.getAdjuntar();
        CurrentCondonacion.setGlosa(cond.GetGlosaCond(condonacion));
        glosa = CurrentCondonacion.getGlosa();

        //////##### FIN-AQUI SE BUSCA LA FECHA CASTIGO MAS ANTIGUA ####//////
        //////######   AQUI SE BUSCA Y GUARDA LA INFORMACION DEL EJECUTIVO####///////
        //CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsulting(rut_ejecutivo, this.cuenta, this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));
        CurrentCondonacion.setCondonador(this._condonadorJDBC.GetCondonadorConsultingV4(rut_ejecutivo, this.cuenta, this.CurrentCondonacion.getNumeroMesesPrimerCastigo(), this.condonacion));

        this.CurrentCondonacion.setMontoMaximoAtribucionEjecutiva(CurrentCondonacion._condonador.getAtribucionMaxima());
        this.CurrentCondonacion.setMontoAtribucionMaxima(CurrentCondonacion._condonador.getAtribucionMaxima());
        ppppppp = 0;
        pppp2 = 0;
        pppp3 = 0;
        for (final Regla _regla : this.CurrentCondonacion._condonador._reglaList) {
            //list2.add(tipodocto.getDv_NomTipDcto());
            if (_regla.getDesTipoValor().equals("Monto Capital")) {
                ppppppp = _regla.getPorcentajeCondonacion100();
                id_valor_regla_capital = _regla.getIdRegla();
                // id_usrmod_1=_regla.get
            }
            if (_regla.getDesTipoValor().equals("Interes")) {
                pppp2 = _regla.getPorcentajeCondonacion100();
                id_valor_regla = _regla.getIdRegla();
            }
            if (_regla.getDesTipoValor().equals("Honorario Judicial")) {
                pppp3 = _regla.getPorcentajeCondonacion100();
                id_valor_regla_Honorario = _regla.getIdRegla();
            }

        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActual() >= 0) {
            pppp2 = this.CurrentCondonacion._condonador.getF_PorcentajeActualInt();
            id_usrmod_int = this.CurrentCondonacion._condonador.getId_usr_modif_int();

        }
        if (this.CurrentCondonacion._condonador.getPorcentajeActualCapital() >= 0) {
            ppppppp = this.CurrentCondonacion._condonador.getF_PorcentajeActualCap();
            id_usrmod_cap = this.CurrentCondonacion._condonador.getId_usr_modif_cap();
        }

        if (this.CurrentCondonacion._condonador.getPorcentajeActualHonorario() >= 0) {
            pppp3 = this.CurrentCondonacion._condonador.getF_PorcentajeActualHon();
            id_usrmod_hon = this.CurrentCondonacion._condonador.getId_usr_modif_hon();

        }

        reglaCapitalPorcentajeCndonacion = (float) ((float) ppppppp / (float) 100);
        ReglaInteresPorcentajeCondonacion = (float) ((float) pppp2 / (float) 100);
        ReglaHonorarioPorcentajeCondonacion = (float) ((float) pppp3 / (float) 100);
        String rangoFechaInicio = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRanfoFInicio());
        String rangoFechaFin = Integer.toString(this.CurrentCondonacion._condonador._reglaList.get(0).getRangoFFin());

        this.CurrentCondonacion.setPorcentajeCondonaCapital(reglaCapitalPorcentajeCndonacion);
        this.CurrentCondonacion.setPorcentajeCondonaHonorario(ReglaHonorarioPorcentajeCondonacion);
        this.CurrentCondonacion.setPorcentajeCondonaInteres(ReglaInteresPorcentajeCondonacion);
        this.CurrentCondonacion.setRangoFechaInicio(rangoFechaInicio);
        this.CurrentCondonacion.setRangoFehaFin(rangoFechaFin);
        this.CurrentCondonacion.setPuedeCondonarEnLinea(false);
        this.CurrentCondonacion.setNumeroDeOperaciones(ListDetOper.size());

        /// Si esta en  juicio la operacion o no le cobramos el porcentaje correspondiente
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            SacaBop temp = new SacaBop();
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setDetalleCredito(ListDetOperModel.get(i).getDetalleCredito());
                temp.setCedente(ListDetOperModel.get(i).getCedente());
                temp.setFechaCastigo(ListDetOperModel.get(i).getFechaCastigo());
                temp.setFechaFencimiento(ListDetOperModel.get(i).getFechaFencimiento());
                temp.setDiasMora(ListDetOperModel.get(i).getDiasMora());
                temp.setMarcaRenegociado(ListDetOperModel.get(i).getMarcaRenegociado());

                //  Calculo del Juicio Activo Para el Conbro de Honorarios Judiciales
                for (int j = 0; j < ListJudClienteModel.getSize(); j++) {
                    String OperacionJud = ListJudClienteModel.get(j).getOperacion() != null ? ListJudClienteModel.get(j).getOperacion() : "0";
                    String OperacionDet = ListDetOperModel.get(i).getOperacionOriginal() != null ? ListDetOperModel.get(i).getOperacionOriginal() : "0";
                    int nnnn = OperacionJud.compareTo(OperacionDet);

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);
                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneJuicio(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneJuicio(false);
                    }

                    if (nnnn == 0) {
                        String EstadoJuicio = ListJudClienteModel.get(j).getEstado_juicio();
                        String Compara = "Activo";
                        int IsActivo = EstadoJuicio.compareTo(Compara);

                        if (IsActivo == 0) {
                            this.CurrentCondonacion.setTieneRol(true);
                        }
                    } else {
                        this.CurrentCondonacion.setTieneRol(false);
                    }

                }

                ////* Fin Juicio Activo
                String ddddd = "";
                String compare = "1900-01-01";
                ddddd = ListDetOperModel.get(i).getFechaCastigo();
                System.out.println("#------------%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                if (ddddd != null && !Objects.equals(ddddd, compare)) {
                    FechaCastigo = ListDetOperModel.get(i).getFechaCastigo();
                    parse = FechaCastigo.split("-");
                    FechaCastigo = parse[2] + "-" + parse[1] + "-" + parse[0] + " 01:01:01";
                } else if (ddddd == null) {
                    FechaCastigo = "14-01-2012 09:29:58";
                } else if (Objects.equals(ddddd, compare)) {
                    FechaCastigo = "14-01-2012 09:29:58";
                    System.out.println("#------------%ELELELELELELELELELEListDetOperModel.get(i).getFechaCastigo() " + i + "FechaCastigo[" + FechaCastigo + "]ddddd.length()[" + ddddd + "]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%---------#");
                }

                d1 = format.parse(FechaCastigo);
                long diff = d2.getTime() - d1.getTime();
                long diffMonths = (long) (diff / (60 * 60 * 1000 * 24 * 30.41666666));
                MeseCatigo = Long.toString(diffMonths);
                temp.setOperacion(oper);
                temp.setMesesCastigo(MeseCatigo);

                // encontramos el mes de castigo mas antiguo
                if ((int) diffMonths > this.MaximoMesCastigo) {
                    this.MaximoMesCastigo = (int) diffMonths;
                    this.CurrentCondonacion.setNumeroMesesPrimerCastigo(this.MaximoMesCastigo);
                    this.CurrentCondonacion.setFechaPrimerCatigo(format.format(d1));

                }
                this.totalA = (float) this.totalA + (float) ListDetOperModel.get(i).getSaldoinsoluto();

                temp.setCapital(ListDetOperModel.get(i).getSaldoEnPesosChileno());

                // verificar si el usuario ha ingresado otro interes personalizado
                float interes = 0;
                float montoInteresAjustado = this._operJDBCsiscon.getProcentajeInteresActual(this.RutClienteFormateado, this.cuenta, oper, this.condonacion);

                if (montoInteresAjustado >= 0) {
                    interes = montoInteresAjustado;

                } else {
                    interes = (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                }

                InteresBanco = InteresBanco + (float) ListDetOperModel.get(i).getMora() - (float) ListDetOperModel.get(i).getSaldoinsoluto();

                sumaMoraTotal = sumaMoraTotal + (float) ListDetOperModel.get(i).getMora();
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                float porcentaje_condonacion = (float) this.CurrentCondonacion.getPorcentajeCondonaCapital();
                float porcentaje_condonacion_Interes = (float) this.CurrentCondonacion.getPorcentajeCondonaInteres();
                float capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion;
                this.totalB = (float) (this.totalB + capital_condonado);
                float capitalarecibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;
                this.totalC = (float) (capitalarecibir + this.totalC);

                /// agregagos el monto a recibir a la clase condonacion
                this.CurrentCondonacion.setMontoARecibir(capitalarecibir);

                float interes_condonado = interes * porcentaje_condonacion_Interes;
                float interesarecibir = interes - interes_condonado;

                this.totalD = (float) (interes + this.totalD);
                this.totalE = (float) (interes_condonado + this.totalE);
                this.totalF = (float) (interesarecibir + this.totalF);

                // se agrega el monto a condonar a la clase sacabop
                temp.setMontoCondonar((long) capital_condonado);
                temp.setMontoCondonarPesos(nf.format(capital_condonado).replaceFirst("Ch", ""));
                temp.setCapitalARecibirPesos(nf.format(capitalarecibir).replaceFirst("Ch", ""));

                temp.setMontoARecibir((long) capitalarecibir);
                float porcentaje_honorjud = 0;
                float honorariojud = 0;
                float honorariojud2 = 0;
                float honorariojud_sobrecondonado = 0;
                float Honor2 = 0;
                float capital_a_recibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;

                // 
                //  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;

                    float c = ((float) this.ufAplica);
                    float a = ((float) capital_a_recibir);
                    float b = (((float) 10 * c));

                    if (a <= b) {

                        honorariojud_sobrecondonado = (a * (float) 0.09);
                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud_sobrecondonado = (hh + ww3);

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) capital_a_recibir;
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud_sobrecondonado = (float) capital_a_recibir * (float) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir && capital_a_recibir <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) capital_a_recibir;
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);
                        honorariojud_sobrecondonado = (hh + ww3);
                    } else if ((float) capital_a_recibir > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) capital_a_recibir;
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

                if (!this._operJDBC.TieneRol(this.RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;

                    float c = Math.round((float) this.ufAplica);
                    float a = Math.round((float) ListDetOperModel.get(i).getSaldoinsoluto());
                    float b = Math.round(((float) 10 * c));

                    if (a <= b) {

                        honorariojud = Math.round(a * 0.09);

                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud = Math.round(hh + ww3);

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) 0.15;

                    } else if ((500 * this.ufAplica) < ListDetOperModel.get(i).getSaldoinsoluto() && ListDetOperModel.get(i).getSaldoinsoluto() <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);

                        honorariojud = Math.round(hh + ww3);
                    } else if ((float) ListDetOperModel.get(i).getSaldoinsoluto() > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud = uno + dos + tres;

                    }

                }

                //  el calculo de los honorarios judiciales esta compuesto del CalculaMontoJudicial(monto a recibir)
                // double honorarioJudCondonado = (double) honorariojud * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                double honorarioJudCondonado = (double) honorariojud * (double) ReglaHonorarioPorcentajeCondonacion;

                float honorarioJudReibido = (float) honorariojud - (float) honorarioJudCondonado;

                // double honorarioJudCondonado2 = (double) honorariojud_sobrecondonado * (double) this.CurrentCondonacion.getPorcentajeCondonaHonorario();
                float honorarioJudCondonado2 = (float) honorariojud_sobrecondonado * (float) ReglaHonorarioPorcentajeCondonacion;
                float honorarioJudReibido2 = (float) honorariojud_sobrecondonado - (float) honorarioJudCondonado2;

                //Calculo de Totales para la Grilla
                // se muestra sobre el total capital condonado
                this.totalG = (float) (honorariojud_sobrecondonado + this.totalG);
                this.totalH = (float) (honorarioJudCondonado2 + this.totalH);
                this.totalI = (float) (honorarioJudReibido2 + this.totalI);

                // Grid-Column Judicial 
                temp.setHonorarioJuducial((float) honorariojud);

                temp.setHonorarioJudicial2((float) honorariojud_sobrecondonado);

                //Montos en pesos
                String valorPesosrecibe = nf.format(honorariojud).replaceFirst("Ch", "");
                temp.setHonorarioJudicialPesos(valorPesosrecibe);
                temp.setHonorarioJudicialCondonadoPesos(nf.format(honorarioJudCondonado2).replaceFirst("Ch", ""));
                temp.setHonorarioJudicialRecibidoPesos(nf.format(honorarioJudReibido2).replaceFirst("Ch", ""));

                temp.setInteresCondonadoPesos(nf.format(interes_condonado).replaceFirst("Ch", ""));
                temp.setInteresARecibirPesos(nf.format(interesarecibir).replaceFirst("Ch", ""));

                temp.setInteres(nf.format(interes).replaceFirst("Ch", ""));

                sacaboj.add(temp);
            } else {
                oper = "SOYNULL";
            }

            System.out.println("#------------@@@@@@@@@@@@@@@@@ListDetOperModel.get(i).getFechaCastigo() " + i + "[" + ListDetOperModel.get(i).getFechaCastigo() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        }

        this.CurrentCondonacion.setHonorarioJudicial2((double) this.totalG);
        Grid_SacabopXX.setModel(sacaboj);
        Grid_SacabopXX.setVisible(false);
        Grid_SacabopXX.setVisible(true);
        idValorUF.setValue(Float.toString(this.UfDia));

        System.out.println("#------------@@@@@@@@@@@@@@@@@ ListDetOperModel.getSize()[" + ListDetOperModel.getSize() + "]Grid_SacabopXX [" + Grid_SacabopXX.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        // sacaboj
        try {
            //String fechahoy;
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());

            /// Agregagamos valores totales a la clase condonacion
            CurrentCondonacion.setCapitalClass(this.totalA + this.totalD + this.totalG);
            CurrentCondonacion.setTotalCondonado(this.totalB + this.totalE + this.totalH);

            //totales de la Grilla
            id_TotalSumaCapital.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotalSumaCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotalSumaRecibeCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));

            id_TotalSumaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotalSumaCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));
            id_TotalSumaRecibeInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            id_TotalSumaHonorJud.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));

            id_TotalSumaHonorCondona.setValue(nff.format(this.totalH).replaceFirst("Ch", ""));
            id_TotalSumaHonorRecibe.setValue(nff.format(this.totalI).replaceFirst("Ch", ""));

            // totoales de Sumatorias
            idTotalRecibe.setValue(nff.format(this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            //Guarda en condonacion
            this.CurrentCondonacion.setTotoalRecibe(this.totalC + this.totalF + this.totalI);

            id_TotalCondona.setValue(nff.format(this.totalB + this.totalE + this.totalH).replaceFirst("Ch", ""));
            id_TotlaCapital.setValue(CurrentCondonacion.TotoalCapital.getValorPesos());

            // totales Parciales en La Suma de Resultados Por Color GREEN
            id_TotoalParcialCapital.setValue(nff.format(this.totalC).replaceFirst("Ch", ""));
            id_TotoalParcialInteres.setValue(nff.format(this.totalF).replaceFirst("Ch", ""));

            // Suma de VDEs para green
            id_TotalVDEs.setValue(nff.format(SumaTotalVDEs).replaceFirst("Ch", ""));

            this.id_SumaProvision.setValue(nff.format(SumaTotalProvision).replaceFirst("Ch", ""));
            id_TotalTotal.setValue(nff.format(this.SumaTotalProvision + SumaTotalVDEs + this.totalC + this.totalF + this.totalI).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color RED
            id_TotoalParcialCondonaCapital.setValue(nff.format(this.totalB).replaceFirst("Ch", ""));
            id_TotoalParcialCondonaInteres.setValue(nff.format(this.totalE).replaceFirst("Ch", ""));

            // totales Parciales en La Suma de Resultados Por Color YELLOW
            id_TotoalParcialCapita.setValue(nff.format(this.totalA).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaInteres.setValue(nff.format(this.totalD).replaceFirst("Ch", ""));
            id_TotoalParcialCapitaHonor.setValue(nff.format(this.totalG).replaceFirst("Ch", ""));
            // totales Parciales en La Suma de Resultados Por Color RED

// fin totales Grilla
            id_fechahoy.setValue(dateFormat.format(date));
            nro_condonacion.setValue(Integer.toString(condonacion));

            //Cosorio tipos especiales
            cE = new CondonacionesEspeciales();
            cE = cE.getCondonacionesEspeciales_X_IdCond(condonacion);
            chk_especial.setDisabled(true);
            txt_TipoEspecial.setDisabled(true);

            if (cE != null) {
                if (cE.getId_TipoEspecial() > 0) {
//                    cE.settCE(new TipoCondEspecial());
//                    cE.gettCE().getTipoCondEspecial_X_Id(cE.getId_TipoEspecial());
                    chk_especial.setChecked(true);
                    if (cE.gettCE().getCodigo().equals("CONDPORCUOTAS")) {
                        txt_TipoEspecial.setText(cE.gettCE().getDescripcion() + " (" + cE.getNumCuotas() + "");
                    } else {
                        txt_TipoEspecial.setText(cE.gettCE().getDescripcion());
                    }
                } else {
                    chk_especial.setChecked(false);
                    txt_TipoEspecial.setText("Sin marca especial");
                }
            } else {
                chk_especial.setChecked(false);
                txt_TipoEspecial.setText("Sin marca especial");
            }
            /////////////////////////////////////////

            valueneg.setValue(Nombre);
            this.CurrentCondonacion.setNombreEjecutiva(Nombre);
            viewrutcliente.setValue(rutcliente);
            this.CurrentCondonacion.setRutCliente(rutcliente);
            id_nomcliente.setValue(InfoCliente.getNombreCOmpleto());
            this.CurrentCondonacion.setNombreCliente(InfoCliente.getNombreCOmpleto());
            id_oficinaorigen.setValue(InfoCliente.getOficina());
            id_saldototalmoroso.setValue(InfoCliente.getSaldoTotalMoraPesos());
            id_saldototalmorosoFloat = (float) Float.parseFloat(InfoCliente.getSaldototal());
            // llenado de informacuion de valores Utilizados en condonacion       
            id_PrimerMesDeCondonacion.setValue(this.CurrentCondonacion.getFechaPrimerCatigo());
            id_MesMasAntiguoCastigo.setValue(Integer.toString(this.CurrentCondonacion.getNumeroMesesPrimerCastigo()));
            //id_AtribucionEjecutiva.setValue("MAT");
            id_AtribucionEjecutiva.setValue(this.CurrentCondonacion.getMontoAtribucionMaxima().getValorPesos());
            DecimalFormat df = new DecimalFormat("####0.00");

            // id_porcentajeCondoEjecutiva.setValue("Ca:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaCapital()) + "  In:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaInteres()) + "  Ho:" + df.format(this.CurrentCondonacion.getPorcentajeCondonaHonorario()));
            id_porcentajeCondoEjecutiva.setValue("Ca:" + String.format("%.2f", reglaCapitalPorcentajeCndonacion * 100) + "%  In:" + String.format("%.2f", ReglaInteresPorcentajeCondonacion * 100) + "%  Ho:" + String.format("%.2f", ReglaHonorarioPorcentajeCondonacion * 100) + "%");

            id_PuedeCondonarOnline.setValue(Boolean.toString(this.CurrentCondonacion.isPuedeCondonarEnLinea()));
            id_rangoFechaCondonacion.setValue("FI:" + this.CurrentCondonacion.getRangoFechaInicio() + " FF:" + this.CurrentCondonacion.getRangoFehaFin());

            float TotalCapital = (float) this.CurrentCondonacion.TotoalCapital.getValor();
            float MontoStribucionMaxima = (float) this.CurrentCondonacion.MontoAtribucionMaxima.getValor();
            if (TotalCapital < MontoStribucionMaxima) {
                btn_GenrarCondonacion.setVisible(true);
                id_modificaRegla.setVisible(true);
                id_modificaRegla2.setVisible(false);
                id_windowsMessajje.setClass("alert alert-success");
                Label temp;
                temp = (Label) id_windowsMessajje.getFellow("id_msgeCona");
                temp.setValue("Ejecutivo " + Nombre + " Cumple Los Requisitos para Condonar Online");
            } else {

            }

            String jjj = Executions.getCurrent().getAttributes().toString();
            System.out.println("#------------@@@@@@@@@@@@@@@@@Tratando de accesar other panels" + jjj + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        } catch (WrongValueException e) {
            System.out.println("EXCEPTION: " + e);

        }
        cargaVista();
        CargaVisiBleCampana(this.RutClienteFormateado,condonacion);
    }
    
    
    
    public void CargaVisiBleCampana(int rutclieentero,int condonacion_i){
                    if (this.clienteinfo.GetIsClienteCampana(rutclieentero) == 0) {

 
                    int kk = 45;
                    //String oferta = clienteinfo.getClienteOferta(rutclieentero);
                    /// aca cargamos el congelado de campa�a   
                    String oferta = clienteinfo.getClienteOferta(rutclieentero);
                    Caption ction = (Caption) Path.getComponent("//GG/cptn_tituloo");
                    //  Messagebox.show("GGFOLLOW[[["+GG.getFellows());
                    Caption ctionc = (Caption) GG.getFellow("cptn_tituloo");
                    GG.setVisible(true);
                    dangercamp.setVisible(true);
                    ////  Messagebox.show("GGFOLLOW[[["+ctionc.getLabel().toString());
                    ctionc.setLabel("CAMPA�A : [" + oferta + "]");
                    labeldangerofer.setValue("CAMPA�A : [" + oferta + "]");
                    //      ction.setLabel("Gestionar prorroga de condonaci�n N�: " + kk+ ".");

                } else {
                    this.GG.setVisible(false);

                }
    
    
    }

    public ApplySacabopRetailController() throws SQLException {
        this.MaximoMesCastigo = 0;
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceLucy());
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceLucy());
        this.cond = new CondonacionImpl(mmmm.getDataSource());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceLucy());
        _cliente = new ClienteSisconJDBC(mmmm.getDataSource());
        _track = new TrackinJDBC(mmmm.getDataSource());
        _condonadorJDBC = new CondonadorJDBC(mmmm.getDataSource());
        _reglas = new ReglasImpl(mmmm.getDataSource());
        _garantias_cliente = new GarantiasClienteJDBC(mmmm.getDataSourceProduccion());
        _deuda_sbif = new SbifClienteJDBC(mmmm.getDataSourceProduccion());
        _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
        _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());
        this._usu = new UsuarioJDBC(mmmm.getDataSource());
        this._colabo = new ColaboradorJDBC(mmmm.getDataSource());
        this._operJDBCsiscon = new OperacionJDBC(mmmm.getDataSource(), "siscon");
        _prov = new ProvisionJDBC(mmmm.getDataSource());

        this.sumaMoraTotal = 0;
        this.metodo = new MetodosGenerales();

        this.UfDia = (float) 0.0;
        OpCont = 0;
    }

    public void MostrarGarantias() {
        // TOS should be checked before accepting order

        ListGarantiasClienteModel = new ListModelList<GarantiasCliente>(_garantias_cliente.GarantiasCliente(RutClienteFormateado, cuenta));

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListGarantiasClienteModel);
        arguments.put("totalSumaGarantias", this._garantias_cliente.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/GarantiasPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                windowx.detach();

            }
        });

        try {
            windowx.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void MostrarSbif() {
        // TOS should be checked before accepting order

        ListSbifClienteClienteModel = new ListModelList<SbifCliente>(_deuda_sbif.SbifCliente(RutClienteFormateado, cuenta));

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListSbifClienteClienteModel);
        arguments.put("totalSumaGarantias", this._deuda_sbif.SumaTotalGarantiasPesos());
        arguments.put("RutEntero", this.RutClienteFormateado);
        String template = "Ejecutivo/SbifPoput.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                windowx.detach();

            }
        });

        try {
            windowx.doModal();
            windowx.setFocus(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void Adjuntar() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        if (adjunta == null) {
            Messagebox.show("Sr(a). Usuario(a), no existe archivos adjuntos para esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
        } else {
            map.put("ListAdjuntoSS", adjunta.getaDet());

            map.put("modulo", modulo);

            try {
                Window fileWindow = (Window) Executions.createComponents(
                        "Ejecutivo/AdjuntarPoput.zul", null, map
                );
                fileWindow.doModal();

            } catch (Exception e) {
                Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
            }
        }
    }

    public void Glosa() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        if (glosa == null) {
            Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
            return;
        } else {
            if (glosa.getDetalle() == null) {
                Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
                return;
            } else {
                if (glosa.getDetalle().isEmpty()) {
                    Messagebox.show("Sr(a). Usuario(a), no existe comentarios sobre esta condonaci�n por parte del ejecutivo.", "Sacabop", Messagebox.OK, Messagebox.INFORMATION);
                    return;
                } else {
                    glosaDet = glosa.getDetalle().get(0);

                    map.put("GlosaSS", glosaDet);
                    map.put("modulo", modulo);

                    try {
                        Window glosaWin = (Window) Executions.createComponents(
                                "EjecutivoPyme/include/Glosa.zul", null, map
                        );
                        glosaWin.doModal();

                    } catch (Exception e) {
                        Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar la glosa. \nError: " + e);
                    }
                }
            }
        }
    }

    public void prorrogaGestionada() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("Id_Condonacion", condonacion);

        map.put("modulo", "Aplicacion");

        try {
            Window fileWindow = (Window) Executions.createComponents(
                    "Ejecutivo/Poput/ProrrogasGesEjePopUp.zul", null, map
            );
            fileWindow.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de adjuntar archivos. \nError: " + e);
        }

    }

    private void eventos() { //cosorio
        id_wSacabop.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {

            @Override
            public void onEvent(Event arg0) throws InterruptedException {
                try {
                    final HashMap<String, Object> map = new HashMap<String, Object>();
                    if (EventQueues.exists("Adjuntar", EventQueues.DESKTOP)) {
                        EventQueues.remove("Adjuntar", EventQueues.DESKTOP);
                    }
                    if (EventQueues.exists("Glosa", EventQueues.DESKTOP)) {
                        EventQueues.remove("Glosa", EventQueues.DESKTOP);
                    }
                    if (EventQueues.exists("Rechazo", EventQueues.DESKTOP)) {
                        EventQueues.remove("Rechazo", EventQueues.DESKTOP);
                    }
                    if (EventQueues.exists("Prorroga", EventQueues.DESKTOP)) {
                        EventQueues.remove("Prorroga", EventQueues.DESKTOP);
                    }
                    if (EventQueues.exists("GestProrroga", EventQueues.DESKTOP)) {
                        EventQueues.remove("GestProrroga", EventQueues.DESKTOP);
                    }

                    eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
                    eq.publish(new Event("onClose", id_wSacabop, null));
                } catch (WrongValueException e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error en el proceso. \n Error: " + e.toString());
                } catch (Exception e) {
                    Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error en el proceso. \n Error: " + e.toString());
                } finally {
                    id_wSacabop.setAction("onClose");
                }
            }
        });
    }

    public void AplicarCondonacion() {
        boolean resultado;
        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;

        resultado = this.cond.SetCambiaEstadoCondonaciones("Aplicacion.Recepcion", "Ejecutivo.Aplicadas", permisos, idcondonacion, "Estado.AprobadaAnalista", "Estado.Aplicada");
        if (resultado = true) {
            Messagebox.show("Se ha Aplicado la Condonaci�n Nro.: [" + idcondonacion + "]");
        } else {

            Messagebox.show("Error al Aplicar  la Condonaci�n Nro.: [" + idcondonacion + "]");

        }
        eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
        eq.publish(new Event("onClose", id_wSacabop, null));
        capturawin.detach();

    }

    public void ReversarCondonacion() {
        boolean resultado;
        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;

        ActualizaTrackinEstado(idcondonacion);
        resultado = this.cond.SetCambiaEstadoCondonaciones("Aplicacion.Recepcion", "Ejecutivo.Reversadas", permisos, idcondonacion, "Estado.AprobadaZonal", "Estado.ReversadaAplicador");

        if (resultado = true) {
            Messagebox.show("Se ha reversado la Condonaci�n Nro.: identificador :[" + idcondonacion + "]");
        } else {
            Messagebox.show("Error al reversar  la Condonaci�n Nro.: identificador :[" + idcondonacion + "]");
        }
        eq = EventQueues.lookup("Sacabop", EventQueues.DESKTOP, false);
        eq.publish(new Event("onClose", id_wSacabop, null));
        capturawin.detach();

    }

    public void RechazarCondonacion() {

        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;
        Messagebox.show("Sr(a) Usuario(a), esta seguro(a) que desea rechazar la condonaci�n N�:" + idcondonacion,
                "Rechazo en linea", Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION,
                new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    procesoRechazo();
                } else if (Messagebox.ON_NO.equals(e.getName())) {
                }
            }
        });

    }

    private void procesoRechazo() {
       final Map<String, Object> map = new HashMap<String, Object>();

        map.put("id_condonacion", condonacion);
        map.put("id_int", id_usrmod_int);
        map.put("id_cap", id_usrmod_cap);
        map.put("id_hon", id_usrmod_hon);
        try {
            Window glosaWin = (Window) Executions.createComponents(
                    "AnAplicacion/Poput/rechazosPopUp.zul", null, map
            );
            glosaWin.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar los rechazos. \nError: " + e);
        }
    }

    public void ProrrogarCondonacion() {

        int idcondonacion;
        idcondonacion = 0;

        idcondonacion = condonacion;
        Messagebox.show("Sr(a) Usuario(a), esta seguro(a) que desea Prorrogar la condonaci�n N�:" + idcondonacion,
                "Rechazo en linea", Messagebox.YES | Messagebox.NO,
                Messagebox.QUESTION,
                new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    procesoProrroga();
                } else if (Messagebox.ON_NO.equals(e.getName())) {
                }
            }
        });

    }

    private void procesoProrroga() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("id_condonacion", condonacion);
        map.put("modulo", "Aplicacion");

        try {
            Window glosaWin = (Window) Executions.createComponents(
                    "AnAplicacion/Poput/ProrrogasPopUp.zul", null, map
            );
            glosaWin.doModal();

        } catch (Exception e) {
            Messagebox.show("Sr(a). Usuario(a), se encontro un error al tratar de cargar los rechazos. \nError: " + e);
        }
    }

    //Cosorio
    private void ActualizaTrackinEstado(int idcondonacion) {
        Trackin_Estado est = new Trackin_Estado();

        est.buscaTrackin_Pendiente(idcondonacion);

        est.setDi_fK_ColaboradorDestino(permisos.getId_colaborador());
        est.setDv_CuentaDestino(permisos.getCuenta());

        est.actualizaUsuarioAprob();
    }

}
