/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.controller;

import java.util.ArrayList;
import java.util.List;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Slider;
import org.zkoss.zul.Textbox;
import config.MvcConfig;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Window;
import siscon.entidades.Banca;
import siscon.entidades.Campania;
import siscon.entidades.Perfil;
import siscon.entidades.RangoFecha;
import siscon.entidades.RangoMonetario;
import siscon.entidades.ReglaVista;
import siscon.entidades.ReglasDet;
import siscon.entidades.ReglasEnc;
import siscon.entidades.Rel_campana_rel_perfil_regla;
import siscon.entidades.Rel_perfil_regla;
import siscon.entidades.Unidad_Medida;
import siscon.entidades.implementaciones.BancaJDBC;
import siscon.entidades.implementaciones.CampaniaImpl;
import siscon.entidades.implementaciones.PerfilImpl;
import siscon.entidades.implementaciones.RangoFechasImpl;
import siscon.entidades.implementaciones.RangoMonetarioImpl;
import siscon.entidades.implementaciones.ReglasImpl;
import siscon.entidades.implementaciones.Rel_campana_rel_perfil_reglaImpl;
import siscon.entidades.implementaciones.Rel_perfil_reglaImpl;
import siscon.entidades.implementaciones.Unidad_MedidaImpl;
import siscon.entidades.interfaces.CampaniaInterfaz;
import siscon.entidades.interfaces.PerfilInterfaz;
import siscon.entidades.interfaces.RangoFechasInterfaz;
import siscon.entidades.interfaces.RangoMonetarioInterfaz;
import siscon.entidades.interfaces.ReglasInterfaz;
import siscon.entidades.interfaces.Rel_campana_rel_perfil_reglaInterfaz;
import siscon.entidades.interfaces.Rel_perfil_reglaInterfaz;
import siscon.entidades.interfaces.Unidad_MedidaInterfaz;

/**
 *
 * @author excosoc
 */
public class AgregarPerfilBancaController extends SelectorComposer<Component> {

    /**
     * @return the lCmb_ReglasTotal
     */
    public ListModelList getlCmb_ReglasTotal() {
        return lCmb_ReglasTotal;
    }

    /**
     * @param lCmb_ReglasTotal the lCmb_ReglasTotal to set
     */
    public void setlCmb_ReglasTotal(ListModelList lCmb_ReglasTotal) {
        this.lCmb_ReglasTotal = lCmb_ReglasTotal;
    }

    @Wire
    Button btn_PerFec;
    @Wire
    Button btn_PerMont;
    @Wire
    Textbox txt_DesdeFecha;
    @Wire
    Textbox txt_HastaFecha;
    @Wire
    Textbox txt_DesdeMonto;
    @Wire
    Textbox txt_HastaMonto;
    @Wire
    Combobox cmb_Perfil;
    @Wire
    Combobox cmb_Campania;
    @Wire
    Combobox cmb_PerFechaRegla;
    @Wire
    Combobox cmb_PerMontoRegla;
    @Wire
    Button btn_AddReg;
    @Wire
    Grid grd_Reglas;
    @Wire
    Label lbl_IdRegla;
    @Wire
    Textbox txt_Regla;
    @Wire
    Slider sld_Capital;
    @Wire
    Label lbl_Capital;
    @Wire
    Slider sld_Interes;
    @Wire
    Label lbl_Interes;
    @Wire
    Slider sld_Honorario;
    @Wire
    Label lbl_Honorario;
    @Wire
    Row row;
    @Wire
    Rows rows;
    @Wire
    private Popup pop_DetalleRangoF;
    @Wire
    private Label lbl_DesdeF;
    @Wire
    private Label lbl_HastaF;
    @Wire
    private Popup pop_DetalleRangoM;
    @Wire
    private Label lbl_DesdeM;
    @Wire
    private Label lbl_HastaM;

    private String perfilSelec;
    
    MvcConfig mmmm = new MvcConfig();
    BancaJDBC _banca;
    /**
     * @return the lCmb_RangFec
     */
    public ListModelList getlCmb_RangFec() {
        return lCmb_RangFec;
    }

    /**
     * @param lCmb_RangFec the lCmb_RangFec to set
     */
    public void setlCmb_RangFec(ListModelList lCmb_RangFec) {
        this.lCmb_RangFec = lCmb_RangFec;
    }

    /**
     * @return the lCmb_RangMoney
     */
    public ListModelList getlCmb_RangMoney() {
        return lCmb_RangMoney;
    }

    /**
     * @param lCmb_RangMoney the lCmb_RangMoney to set
     */
    public void setlCmb_RangMoney(ListModelList lCmb_RangMoney) {
        this.lCmb_RangMoney = lCmb_RangMoney;
    }

    /**
     * @return the lRangoFecha
     */
    public List<RangoFecha> getlRangoFecha() {
        return lRangoFecha;
    }

    /**
     * @param lRangoFecha the lRangoFecha to set
     */
    public void setlRangoFecha(List<RangoFecha> lRangoFecha) {
        this.lRangoFecha = lRangoFecha;
    }

    /**
     * @return the lRangoMonetario
     */
    public List<RangoMonetario> getlRangoMonetario() {
        return lRangoMonetario;
    }

    /**
     * @param lRangoMonetario the lRangoMonetario to set
     */
    public void setlRangoMonetario(List<RangoMonetario> lRangoMonetario) {
        this.lRangoMonetario = lRangoMonetario;
    }

    ////////////////////////////////////////////////////////////////////////////
    private MvcConfig conex = new MvcConfig();
    private ReglasEnc reglasEnc;
    private List<ReglasEnc> lReglasEnc = new ArrayList<ReglasEnc>();
    private ReglasDet reglasDet;
    private List<ReglasDet> lReglasDet = new ArrayList<ReglasDet>();
    private RangoFecha rangoFecha;
    private List<RangoFecha> lRangoFecha = new ArrayList<RangoFecha>();
    private RangoMonetario rangoMonetario;
    private List<RangoMonetario> lRangoMonetario = new ArrayList<RangoMonetario>();
    private Unidad_Medida unidadMenida;
    private List<Unidad_Medida> lUnidadMenida = new ArrayList<Unidad_Medida>();
    private Perfil perfil;
    private List<Perfil> lPerfil = new ArrayList<Perfil>();
    private Rel_perfil_regla relPerRel;
    private List<Rel_perfil_regla> lRelPerRel = new ArrayList<Rel_perfil_regla>();
    private Rel_campana_rel_perfil_regla campPerReg;
    private List<Rel_campana_rel_perfil_regla> lCampPerReg = new ArrayList<Rel_campana_rel_perfil_regla>();
    private Campania campanaia;
    private List<Campania> lCampania = new ArrayList<Campania>();
    private List<ReglaVista> lRegla = new ArrayList<ReglaVista>();
    private List<Banca> lBanca = new ArrayList<Banca>();
    private List<ReglaVista> lReglaTotal = new ArrayList<ReglaVista>();
    private int id_Campania = 0;
    private int id_x100Cap;
    private int id_x100Int;
    private int id_x100Hon;

    ////////////////////////////////////////////////////////////////////////////
    private ListModelList lCmb_RangFec;
    private ListModelList lCmb_RangMoney;
    private ListModelList lGrd_Reglas;
    private ListModelList lCmb_Perfil;
    private ListModelList lCmb_Campania;
    private ListModelList lCmb_ReglasTotal;
    ////////////////////////////////////////////////////////////////////////////
    private ReglasInterfaz reglasInterfaz;
    private RangoFechasInterfaz RangoFechaInterfaz;
    private RangoMonetarioInterfaz rangoMonetarioInterfaz;
    private Unidad_MedidaInterfaz unidadMedidaInterfaz;
    private PerfilInterfaz perfilInterfaz;
    private Rel_perfil_reglaInterfaz relPerRelInterfaz;
    private Rel_campana_rel_perfil_reglaInterfaz campPerRegInterfaz;
    private CampaniaInterfaz campaniaInterfaz;
    ////////////////////////////////////////////////////////////////////////////
    private ReglasImpl reglasImpl;
    private RangoFechasImpl RangoFechaImpl;
    private RangoMonetarioImpl rangoMonetarioImpl;
    private Unidad_MedidaImpl unidadMedidaImpl;
    private Rel_perfil_reglaImpl relPerRelImpl;
    private PerfilImpl perfilImpl;
    private Rel_campana_rel_perfil_reglaImpl campPerRegImpl;
    private CampaniaImpl campaniaImpl;
     Window window;

    public AgregarPerfilBancaController() throws SQLException {
        reglasInterfaz = new ReglasImpl(conex.getDataSource());
        RangoFechaInterfaz = new RangoFechasImpl(conex.getDataSource());
        rangoMonetarioInterfaz = new RangoMonetarioImpl(conex.getDataSource());
        unidadMedidaInterfaz = new Unidad_MedidaImpl(conex.getDataSource());
        perfilInterfaz = new PerfilImpl(conex.getDataSource());
        relPerRelInterfaz = new Rel_perfil_reglaImpl(conex.getDataSource());
        campPerRegInterfaz = new Rel_campana_rel_perfil_reglaImpl(conex.getDataSource());
        campaniaInterfaz = new CampaniaImpl(conex.getDataSource());
        this.id_x100Cap = conex.getParmCond().getId_x100Capital();
        this.id_x100Int = conex.getParmCond().getId_x100Interes();
        this.id_x100Hon = conex.getParmCond().getId_x100Honorario();
        
        this._banca=new BancaJDBC(mmmm.getDataSource());
        //_cliente = new ClienteSisconJDBC(mmmm.getDataSource());
        

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp); //To change body of generated methods, choose Tools | Templates.
        cargaTotalReglas();
        cargaPerfil();
     
        cargaPerCond();
        cargaMontCond();
    }

    public void cargaReglasGrid(int id_ReglaEnc) {
        int _id = 0;
        String _det = "";
        boolean _activo = false;
        int _porCap = 0;
        int _porInt = 0;
        int _porHon = 0;
        int _rangoF = 0;
        int _rangoM = 0;

        reglasEnc = reglasInterfaz.getReglasXIdEnc(id_ReglaEnc);
        lReglasDet = reglasInterfaz.getReglasDetXIdEnc(id_ReglaEnc);

        _id = reglasEnc.getId_regla();
        _det = reglasEnc.getDesc();
        _rangoF = reglasEnc.getId_rangof();
        _rangoM = reglasEnc.getId_rangom();
        _activo = reglasEnc.isActivo();

        for (ReglasDet relD : lReglasDet) {

            if (relD.getId_regla() == reglasEnc.getId_regla()) {
                //Porcentaje capital
                if (relD.getId_valor() == id_x100Cap) {
                    _porCap = Math.round(relD.getPorcentaje_condonacion());
                }

                //Porcentaje capital
                if (relD.getId_valor() == id_x100Int) {
                    _porInt = Math.round(relD.getPorcentaje_condonacion());
                }

                //Porcentaje capital
                if (relD.getId_valor() == id_x100Hon) {
                    _porHon = Math.round(relD.getPorcentaje_condonacion());
                }
            }

        }
        lRegla.add(new ReglaVista(_id, _det, _activo, _porCap, _porInt, _porHon, _rangoF, _rangoM, getlRangoFecha(), getlRangoMonetario(), getlCmb_ReglasTotal()));
    }

    public void cargaPerfil() {

        try {
            lPerfil.clear();
            lPerfil = perfilInterfaz.getPerfil();
            lCmb_Perfil = new ListModelList<Perfil>(lPerfil);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_Perfil.setModel(lCmb_Perfil);
            cmb_Perfil.applyProperties();
            cmb_Perfil.setVisible(false);
            cmb_Perfil.setVisible(true);
            cmb_Perfil.setPlaceholder("Seleccione perfil.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaPerfilXCampania() {
        int id_Rep = 0;
        try {
            lPerfil.clear();
            lCmb_Perfil.clear();
            //Carga combo con perfiles en campa�as
            for (Rel_perfil_regla perReg : lRelPerRel) {
                if (perReg.getId_perfil() != id_Rep) {
                    lPerfil.add(perfilInterfaz.getPerfilXId(perReg.getId_perfil()));
                    id_Rep = perReg.getId_perfil();
                }
            }

//            lPerfil = perfilInterfaz.getPerfil();
            lCmb_Perfil = new ListModelList<Perfil>(lPerfil);
//            lCmb_Perfil.addSelection(lCmb_Perfil.get(0));

            cmb_Perfil.setModel(lCmb_Perfil);
            cmb_Perfil.applyProperties();
            cmb_Perfil.setVisible(false);
            cmb_Perfil.setVisible(true);
            cmb_Perfil.setPlaceholder("Seleccione perfil.");
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaTotalReglas() {
        int _id = 0;
        String _det = "";
        boolean _activo = false;
        int _porCap = 0;
        int _porInt = 0;
        int _porHon = 0;
        int _rangoF = 0;
        int _rangoM = 0;

        try {
            lReglaTotal.clear();
            lReglasEnc = reglasInterfaz.getReglasEnc();
            lReglasDet = reglasInterfaz.getReglasDet();

            for (ReglasEnc relE : lReglasEnc) {

                _id = relE.getId_regla();
                _det = relE.getDesc();
                _rangoF = relE.getId_rangof();
                _rangoM = relE.getId_rangom();
                _activo = relE.isActivo();

                for (ReglasDet relD : lReglasDet) {

                    if (relD.getId_regla() == relE.getId_regla()) {
                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Cap) {
                            _porCap = Math.round(relD.getPorcentaje_condonacion());
                        }

                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Int) {
                            _porInt = Math.round(relD.getPorcentaje_condonacion());
                        }

                        //Porcentaje capital
                        if (relD.getId_valor() == id_x100Hon) {
                            _porHon = Math.round(relD.getPorcentaje_condonacion());
                        }
                    }
                }

                lReglaTotal.add(new ReglaVista(_id, _det, _activo, _porCap, _porInt, _porHon, _rangoF, _rangoM, getlRangoFecha(), getlRangoMonetario()));
            }
            setlCmb_ReglasTotal(new ListModelList<ReglaVista>(lReglaTotal));
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los perfiles. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

  
    public void cargaPerCond() {

        try {
            getlRangoFecha().clear();
            setlRangoFecha(RangoFechaInterfaz.getRangoFecha());
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los periodos de condonaci�n. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    public void cargaMontCond() {

        try {
            getlRangoMonetario().clear();
            setlRangoMonetario(rangoMonetarioInterfaz.getRangoMonetario());
        } catch (Exception e) {
            Messagebox.show("Estimado(a) Colaborador(a), error al cargar los rangos monetarios. \nError:" + e.toString() + "\nContactar a �rea tecnica.");
        }

    }

    @Listen("onClick = #btn_AddReg")
    public void onClick$btn_AddReg(Event event) {
      window = null;
        
        String template = "Mantenedores/Poput/Bancas.zul";
        int id_operacion_documento = 0;
        
        Map<String, Object> arguments = new HashMap<String, Object>();
   
        
        arguments.put("CodPerfil", cmb_Perfil.getValue());
        arguments.put("DescPerfil", "gg");

        
        window = (Window) Executions.createComponents(template, null, arguments);
        Button printButton = (Button) window.getFellow("btn_closeButton3");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                window.detach();

            }
        });
        
        Button printButton2 = (Button) window.getFellow("btn_addBanca");

        printButton2.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                // Messagebox.show("Me EJEcuto printButton");
                //  UpdateGridDoc();
                lBanca = _banca.getBancaXPerfil(cmb_Perfil.getValue());
          lGrd_Reglas = new ListModelList<Banca>(lBanca);
          grd_Reglas.setModel(lGrd_Reglas);
          grd_Reglas.renderAll();
          grd_Reglas.setVisible(false);
          grd_Reglas.setVisible(true);
                window.detach();

            }
        });
        
        
        

        try {
            window.doModal();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    

    @Listen("onSelect = #cmb_Perfil")
    public void onSelect$cmb_Perfil(Event event) {
        cmb_Perfil.getSelectedIndex();
        perfilSelec=cmb_Perfil.getText();
        System.out.println("LLEGA PERFIL "+cmb_Perfil.getValue());
        // Messagebox.show("Value ComboBox ["+cmb_Perfil.getValue()+"]");
        this.lBanca = this._banca.getBancaXPerfil(cmb_Perfil.getValue());

       // bandejacustodia = new ListModelList<BandejaCustodia>(listSol);


        
        lGrd_Reglas = new ListModelList<Banca>(lBanca);

        grd_Reglas.setModel(lGrd_Reglas);


        grd_Reglas.renderAll();
        grd_Reglas.setVisible(false);
        grd_Reglas.setVisible(true);
    }

    @Listen("onSelect = #cmb_PerFechaRegla")
    public void onSelect$cmb_PerFechaRegla(Event event) {
        cmb_PerFechaRegla.getSelectedIndex();

        for (final RangoFecha lrf : lRangoFecha) {
            if (lrf.getDesc().equals(cmb_PerFechaRegla.getValue())) {
                txt_DesdeFecha.setValue(Integer.toString(lrf.getRango_ini()));
                txt_HastaFecha.setValue(Integer.toString(lrf.getRango_fin()));
            }
        }

    }

    @Listen("onSelect = #cmb_PerMontoRegla")
    public void onSelect$cmb_PerMontoRegla(Event event) {
        cmb_PerMontoRegla.getSelectedIndex();

        for (final RangoMonetario lrm : lRangoMonetario) {
            if (lrm.getDesc().equals(cmb_PerMontoRegla.getValue())) {
                txt_DesdeMonto.setValue(Integer.toString(lrm.getMonto_ini()));
                txt_HastaMonto.setValue(Integer.toString(lrm.getMonto_fin()));
            }
        }

    }

    @Listen("onSelect = #cmb_Campania")
    public void onSelect$cmb_Campania(Event event) {
        //Al cargar la campa�a esta busca los perfiles que estan relacionados con ella,
        //y carga los perfiles en el cmb de perfiles
        cmb_Campania.getSelectedIndex();

        for (final Campania camp : lCampania) {
            if (camp.getDescripcion().equals(cmb_Campania.getValue())) {
                lRelPerRel = relPerRelInterfaz.getPerRegXId(camp.getId_campania());
                id_Campania = camp.getId_campania();
                cargaPerfilXCampania();

            }
        }

    }
    
    public void DeleteBancaPerfil(String cod,int id){
        System.out.println("id-> "+id);
        System.out.println("cod-> "+cod);
        String perfilCombo = cmb_Perfil.getValue();
       // _banca.getBancaXPerfil(cmb_Perfil.getValue());
      boolean deleteIf =  this._banca.EliminarBancaPerfil(perfilCombo, cod);
      
      if(deleteIf){
          this.lBanca = this._banca.getBancaXPerfil(cmb_Perfil.getValue());
          lGrd_Reglas = new ListModelList<Banca>(lBanca);
          grd_Reglas.setModel(lGrd_Reglas);
          grd_Reglas.renderAll();
          grd_Reglas.setVisible(false);
          grd_Reglas.setVisible(true);
          Messagebox.show("Estimado(a) Colaborador(a), Banca Eliminada Correctamente");
      }else{
           Messagebox.show("Estimado(a) Colaborador(a), error al Eliminar banca \nContactar a �rea tecnica.");
      }
      
    }

    private void addRegla(ReglasEnc regEnc) {
        reglasInterfaz.insertReglasEnc(regEnc);
    }

    private void addRegla(ReglasDet regDet) {
        reglasInterfaz.insertReglasDet(regDet);
    }

    private void UpdRegla(ReglasEnc regEnc) {
        reglasInterfaz.updateReglasEnc(regEnc);
    }

    private void UpdRegla(ReglasDet regDet) {
        reglasInterfaz.updateReglasDet(regDet);
    }

}
