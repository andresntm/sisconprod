package siscon.panelbusqueda;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import org.zkoss.lang.Threads;
import static org.zkoss.lang.Threads.sleep;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;

import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;
import siscon.entidades.Banca;
import siscon.entidades.Cliente;

import siscon.entidades.DetalleCliente;
import siscon.entidades.GesSmall;
import siscon.entidades.JudicialCliente;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.ClienteInterfazImpl;
import siscon.entidades.implementaciones.CondonacionImpl;
import siscon.entidades.implementaciones.DetalleOperacionesClientesImp;

import siscon.entidades.implementaciones.JudicialClienteImpl;
import siscon.entidades.implementaciones.MotorValidacionJDBC;
import siscon.entidades.implementaciones.SmallCoreJDBC;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import siscon.entidades.interfaces.JudicialClienteInterz;
import siscon.entidades.interfaces.SmallCoreInterfaz;
import siscore.genral.MetodosGenerales;

public class SearchRutController extends SelectorComposer<Window> {

    Session session;

    @Wire
    Grid BanEntrClientee;
    @Wire
    Grid BanEntrJuducial;
    private boolean firstGridVisible;
    @Wire
    Grid griddos;
    @Wire
    Textbox buscarinput;

    @Wire
    Button btn_condona;
    @Wire
    Button btn_sacabop;
    //MvcConfig mmmm = new MvcConfig();
    MvcConfig mmmm = new MvcConfig();
    @Wire
    Include pageref;
    Hlayout nn;
    @Wire
    Vlayout contenido;
    @Wire
    Label lbl_TioAux;
    @Wire
    Label lbl_OpOrg;
    @Wire
    Label lbl_CuMor;
    @Wire
    Label lbl_SalIns;
    @Wire
    Label lbl_CuPag;
    @Wire
    Label lbl_CuPact;
    @Wire
    Label lbl_DiaMor;
    @Wire
    Label lbl_Condona;
    Div yBHQle;
    @Wire
    Grid grd_GastosHono;
    @Wire
    Label lbl_TioAux2;
    @Wire
    Label lbl_OpOrg2;
    @Wire
    Label lbl_CuMor2;
    @Wire
    Label lbl_SalIns2;
    @Wire
    Label lbl_CuPag2;
    @Wire
    Label lbl_CuPact2;
    @Wire
    Label lbl_DiaMor2;
    @Wire
    Panel pnl_PanelHono;
    @Wire
    Div Id_search;
    @Wire
    Checkbox chk_checkAll;
    @WireVariable
    ListModelList<DetalleCliente> ListDetOperModel;
    @WireVariable
    ListModelList<JudicialCliente> ListJudClienteModel;
    DetalleOperacionesClientes detoper;
    DetalleOperacionesClientes detOperTermino;
    CondonacionInterfaz condInt;
    final JudicialClienteInterz JudCliente;
    List<DetalleCliente> ListDetOper = new ArrayList<DetalleCliente>();
    List<JudicialCliente> ListJudCliente = new ArrayList<JudicialCliente>();
    int rutClienteEnCond = 0;
    char dvFinal = '\0';
    private MetodosGenerales metodo;
    Window capturawin;
    public MotorValidacionJDBC _valida;
    ListModelList<GesSmall> ListGesSmallModel;
    UsuarioPermiso permisos;
    String Account;
    int rutClienteEntero;
    Cliente InfoCliente;
    ClienteInterfazImpl clienteinfo;
    int puedeEvaluar = 0;

    final SmallCoreInterfaz _consultaSmallCore;
String cuenta;
    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        capturawin = (Window) comp;
        session = Sessions.getCurrent();
        //sess = Sessions.getCurrent();
        //permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
       
        permisos = (UsuarioPermiso) session.getAttribute("UsuarioPermisos");
        
         cuenta = permisos.getCuenta();
        Account = permisos.getCuenta();

        Locale.setDefault(new Locale("es", "CL"));
        lbl_TioAux.setValue("C�digo <br>Operaci�n".replace("<br>", "\r\n"));
        lbl_OpOrg.setValue("Operaci�n <br>original".replace("<br>", "\r\n"));
        lbl_CuMor.setValue("Cuotas <br>Morosas".replace("<br>", "\r\n"));
        lbl_SalIns.setValue("Saldo <br>Insoluto".replace("<br>", "\r\n"));
        lbl_CuPag.setValue("Cuotas <br>Pagadas".replace("<br>", "\r\n"));
        lbl_CuPact.setValue("Cuotas <br>Pactadas".replace("<br>", "\r\n"));
        lbl_DiaMor.setValue("D�as <br>Mora".replace("<br>", "\r\n"));
        lbl_Condona.setValue("�Condona? <br>SI / NO".replace("<br>", "\r\n"));

        lbl_TioAux2.setValue("C�digo <br>Operaci�n".replace("<br>", "\r\n"));
        lbl_OpOrg2.setValue("Operaci�n <br>original".replace("<br>", "\r\n"));
        lbl_CuMor2.setValue("Cuotas <br>Morosas".replace("<br>", "\r\n"));
        lbl_SalIns2.setValue("Saldo <br>Insoluto".replace("<br>", "\r\n"));
        lbl_CuPag2.setValue("Cuotas <br>Pagadas".replace("<br>", "\r\n"));
        lbl_CuPact2.setValue("Cuotas <br>Pactadas".replace("<br>", "\r\n"));
        lbl_DiaMor2.setValue("D�as <br>Mora".replace("<br>", "\r\n"));

//        Tabla_Condonacion tC = new Tabla_Condonacion();
//        tC.prueba();
        chk_checkAll.addEventListener(Events.ON_CHECK, new EventListener<Event>() {
            public void onEvent(Event event) throws Exception {
                checkAll();
            }
        });
    }

    public SearchRutController() throws SQLException {
        this.detoper = new DetalleOperacionesClientesImp(mmmm.getDataSourceProduccion());
        this.JudCliente = new JudicialClienteImpl(mmmm.getDataSourceProduccion());
        this.detOperTermino = new DetalleOperacionesClientesImp(mmmm.getDataSource());
        this.condInt = new CondonacionImpl(mmmm.getDataSource());
        this.metodo = new MetodosGenerales();
        _valida = new MotorValidacionJDBC(mmmm.getDataSourceProduccion());
        this.clienteinfo = new ClienteInterfazImpl(mmmm.getDataSourceSisCon());
        _consultaSmallCore = new SmallCoreJDBC(mmmm.getDataSourceSisCon());
    }

    @Listen("onClick=#btn-find-rut1; onOK=#buscarinput")
    public void doLogin() throws SQLException, Exception {

        int cont = 0;
        String nm = "";
        boolean condonado = false;

        String valor;
        boolean ifbanca = false;
        // Funciona en el Botton, Pero se debe cambiar el Template

        //limpia variable
        chk_checkAll.setChecked(false);
        // si el perfil ejecutivo coincide con el del cliente seguir
        if (buscarinput.getValue().isEmpty()) {
            Messagebox.show("Sr(a) Usuario(a), debe ingresar un rut para continuar.");
            buscarinput.setFocus(true);
            Clients.clearBusy();
            return;
        } else {

            valor = buscarinput.getValue();
            buscarinput.setValue(valor.replace(".", ""));

            if (metodo.validarRut(buscarinput.getValue())) {
                nm = buscarinput.getValue();
                puedeEvaluar = condInt.isCliente(buscarinput.getValue());
//                condonado = condInt.isClinete(buscarinput.getValue());
                Clients.clearBusy();
            } else {
                Messagebox.show("#4Sr(a) Usuario(a), el rut ingresado no es valido (" + buscarinput.getValue() + "), "
                        + "favor ingresar nuevamente el rut.", "Mensaje", Messagebox.OK, Messagebox.INFORMATION, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_OK.equals(event.getName())) {
                            buscarinput.setFocus(true);
                            buscarinput.select();
                        }
                    }
                });
                Clients.clearBusy();
                return;
            }

        }

        if (nm != null || nm.isEmpty() || nm.length() > 0) {

            String mm = nm.replace(".", "");
            String[] ParteEntera = mm.split("-");

            //Respaldo el Rut del cliente para generar tabla de respaldo
            rutClienteEnCond = Integer.parseInt(ParteEntera[0]);

            // 
            if (_valida.EsFallecido(rutClienteEnCond)) {

                Messagebox.show("Cliente Fallecido");
                return;
            }
            dvFinal = ParteEntera[1].charAt(0);

            if (puedeEvaluar == 0 || puedeEvaluar == 2) {//SI 0 se puede evaluar cliente || SI 2 se puede ya que es rechazo
                ListDetOper = detoper.Cliente(rutClienteEnCond, Account, (puedeEvaluar == 2) ? 1 : 0); // cambiar por usuario
                ListJudCliente = JudCliente.JudClie(rutClienteEnCond, Account);
                InfoCliente = clienteinfo.infocliente(rutClienteEnCond, Account);
                for (final Banca __banca : permisos.getBanca()) {
                    String CodBanca = InfoCliente.getFld_cod_banca();
                    String UserCodBanca = __banca.getCod_banca();
                    if (UserCodBanca.equals(CodBanca)) {

                        ifbanca = true;
                    }

                }

                if (!ifbanca) {
                    Messagebox.show("#3Sr(a) Ejecutivo, el cliente que intenta condonar pertenece a "
                            + "la Banca: " + InfoCliente.getFld_cod_banca() + ", Usted no tiene esta banca Asignada, "
                            + "Favor contactar a los administradores del sistema.");
                    return;
                }

            } else if (puedeEvaluar == 1) { //Cliente condonado

                Messagebox.show("#5Sr(a) Usuario(a), el rut ingresado ya fue condonado anteriormente. "
                        + "(" + buscarinput.getText() + "), favor ingresar nuevamente el rut.",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_OK.equals(event.getName())) {
                            buscarinput.setFocus(true);
                            buscarinput.select();
                        }
                    }
                });
                return;
            }
            if (ListJudCliente == null || ListDetOper == null) {

                Messagebox.show("Usuario no encontrado en mora");

            }

            if (!ListDetOper.isEmpty()) {

                if (puedeEvaluar == 1) {
                    Messagebox.show("#7Sr(a) Usuario(a), el rut ingresado ya fue condonado anteriormente. "
                            + "(" + buscarinput.getText() + "), favor ingresar nuevamente el rut.", "Mensaje",
                            Messagebox.OK, Messagebox.INFORMATION, new EventListener<Event>() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            if (Messagebox.ON_OK.equals(event.getName())) {
                                buscarinput.setFocus(true);
                                buscarinput.select();
                            }
                        }
                    });
                } else {

                    btn_sacabop.setVisible(true);
                }

                ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
                ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

                cargaDetHonGrid();
                cargaDetCliGrid();

                pnl_PanelHono.setWidth(BanEntrClientee.getWidth());
                pnl_PanelHono.setVisible(true);

                System.out.println("#------------&&&&&&&&&&&&&SearchController ListDetOperModel.getSize()"
                        + "[" + ListDetOperModel.getSize() + "]Grid_SacabopXX ["
                        + BanEntrClientee.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
                Clients.clearBusy(this.getSelf());

            } else if (puedeEvaluar == 2) {
                Messagebox.show("#8Sr(a) usuario(a), el cliente con rut:" + buscarinput.getText() + " aun n#1o esta disponible para condonaci�n.", "Siscon-Admin.", Messagebox.OK, Messagebox.INFORMATION);
                Clients.clearBusy();
            } else {
                Messagebox.show("#9Sr(a) usuario(a), el cliente con rut:" + buscarinput.getText() + " no tiene informaci�n de operaciones castigadas en Normaliza.", "Siscon-Admin.", Messagebox.OK, Messagebox.INFORMATION);
                Clients.clearBusy();
            }
        } else {
            Messagebox.show("KSDJAKLDJKLSJDKL----USER[VACIO]");
            Clients.clearBusy();
        }

    }

    public void checkAll() {

        final boolean chk_Condona = chk_checkAll.isChecked();

        ListDetOperModel.forEach(new Consumer<DetalleCliente>() {
            public void accept(DetalleCliente t) {
                t.setCondona(chk_Condona);
            }
        });

        cargaDetCliGrid();
    }

    private void cargaDetCliGrid() {
        ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
        ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {
            } else {
                listModelCondo.add(detCliHono);
            }
        }

        BanEntrClientee.setModel(listModelCondo);
        ListDetOperModel.clear();
        ListDetOperModel = listModelCondo;
        BanEntrClientee.setVisible(true);
    }

    private void cargaDetHonGrid() {
        ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();
        ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

        for (DetalleCliente detCliHono : ListDetOperModel) {
            if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {
                listModelHono.add(detCliHono);
            }
        }

        grd_GastosHono.setModel(listModelHono);
        grd_GastosHono.setVisible(true);

        BanEntrJuducial.setModel(ListJudClienteModel);
        BanEntrJuducial.setVisible(true);

    }

    public void doLongOperation() {
        Messagebox.show("KKKKK1");
        Threads.sleep(5000); //simulate a 5 sec operation
        Messagebox.show("KKKKK");
    }

    //Metodo que generera y respalda sacabop
    @Listen("onClick=#btn_sacabop")
    public void onSacabop() throws SQLException {

        List<DetalleCliente> detClientCond = new ArrayList<DetalleCliente>();

        for (Component rownn : BanEntrClientee.getRows().getChildren()) {
            rownn.getClass();
            Row nnn = (Row) rownn;
            Checkbox chk_Condona = (Checkbox) nnn.getChildren().get(12).getChildren().get(0);
            DetalleCliente detClient = new DetalleCliente();
            if (chk_Condona.isChecked()) {
                detClient = (DetalleCliente) nnn.getValue();
                detClient.setCondona(true);
                detClientCond.add(detClient);
            }

        }
        Set<DetalleCliente> citySet = new HashSet<DetalleCliente>(detClientCond);
        detClientCond.clear();
        detClientCond.addAll(citySet);

        if (detClientCond == null || detClientCond.isEmpty()) {
            Messagebox.show("Sr(a). Usuario(a), Debe seleccionar al menos una operaci�n para simular la condonaci�n.");
            return;
        }
        session.setAttribute("detClientCond", detClientCond);
        Include inc = (Include) capturawin.getParent().getParent().getFellow("pageref");
        inc.setSrc(null);
        Sessions.getCurrent().setAttribute("rutcliente", buscarinput.getValue());
        Sessions.getCurrent().setAttribute("rechazo", (puedeEvaluar == 2) ? 1 : 0);
        inc.setSrc("Ejecutivo/sacabop.zul");

    }

    @Listen("onClick=#btn_condona")
    public void simulacondonacion() throws SQLException {
        System.out.println("#------------@@@@SESIONNNNNNNNNNn"
                + Executions.getCurrent().getDesktop().getPages().toString() + " @@@@@@@@@---------#");
        String jjjjj = Executions.getCurrent().getDesktop().getPages().toString();
        String[] hhhh = jjjjj.split(" ");
        String xxx = hhhh[1].replace("_]]", "");

        String iddiv = xxx.concat("le");
        System.out.println("#------------@@@@SESIONNNNNNNNNNn[" + xxx + "] @@@@@@@@@---------#");

        System.out.println("#------------@@@@Intentificador del Div de Include[" + iddiv + "] @@@@@@@@@---------#");
        Div nnnn = null;
        nnnn.setId(iddiv);

        Include headerPage = new Include();
        headerPage.setSrc("usuarios.zul");
        headerPage.setParent(nnnn);

    }

    @Listen("onClick=#idBuscarDatos")
    public void doLogin2() throws SQLException {

        int cont = 0;
        String nm = "";
        boolean condonado = false;

        if (buscarinput.getValue().isEmpty()) {
            Messagebox.show("Sr(a) Usuario(a), debe ingresar un rut para continuar.");
            buscarinput.setFocus(true);
            Clients.clearBusy();
            return;
        } else {
            if (metodo.validarRut(buscarinput.getValue())) {
                nm = buscarinput.getValue();

                if (condInt.isClinete(buscarinput.getValue())) {
                    condonado = true;
                } else {
                    condonado = false;
                }
                Clients.clearBusy();
            } else {
                Messagebox.show("#9Sr(a) Usuario(a), el rut ingresado no es valido "
                        + "(" + buscarinput.getValue() + "), favor ingresar nuevamente el rut.",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (Messagebox.ON_OK.equals(event.getName())) {
                            buscarinput.setFocus(true);
                            buscarinput.select();
                        }
                    }
                });
                Clients.clearBusy();
                return;
            }

        }

        if (nm != null || nm.isEmpty() || nm.length() > 0) {

            String mm = nm.replace(".", "");
            String[] ParteEntera = mm.split("-");

            //Respaldo el Rut del cliente para generar tabla de respaldo
            rutClienteEnCond = Integer.parseInt(ParteEntera[0]);
            dvFinal = ParteEntera[1].charAt(0);

            ListDetOper = detoper.Cliente(Integer.parseInt(ParteEntera[0]), cuenta);
            ListJudCliente = JudCliente.JudClie(Integer.parseInt(ParteEntera[0]), cuenta);

            if (ListJudCliente == null || ListDetOper == null) {

                Messagebox.show("Usuario No encontrado en mora");

            }

            if (!ListDetOper.isEmpty()) {

                if (condonado) {
                    Messagebox.show("#11Sr(a) Usuario(a), el rut ingresado ya fue condonado anteriormente. "
                            + "(" + buscarinput.getValue() + "), favor ingresar nuevamente el rut.",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION, new EventListener<Event>() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            if (Messagebox.ON_OK.equals(event.getName())) {
                                buscarinput.setFocus(true);
                                buscarinput.select();
                            }
                        }
                    });
                } else {

                    btn_sacabop.setVisible(true);
                }

                ListModelList<DetalleCliente> listModelCondo = new ListModelList<DetalleCliente>();
                ListModelList<DetalleCliente> listModelHono = new ListModelList<DetalleCliente>();

                ListDetOperModel = new ListModelList<DetalleCliente>(ListDetOper);
                ListJudClienteModel = new ListModelList<JudicialCliente>(ListJudCliente);

                for (DetalleCliente detCliHono : ListDetOperModel) {
                    if (metodo.like(detCliHono.getTipoCedente(), "%VDE%") || metodo.like(detCliHono.getTipoCedente(), "%SGN%")) {

                        listModelHono.add(detCliHono);

                    } else {

                        listModelCondo.add(detCliHono);

                    }
                }
                grd_GastosHono.setModel(listModelHono);
                BanEntrClientee.setModel(listModelCondo);
                ListDetOperModel.clear();
                ListDetOperModel = listModelCondo;
                BanEntrJuducial.setModel(ListJudClienteModel);
                // BanEntrClientee.setc
                pnl_PanelHono.setWidth(BanEntrClientee.getWidth());
                pnl_PanelHono.setVisible(true);
                BanEntrClientee.setVisible(true);
                grd_GastosHono.setVisible(true);
                System.out.println("#------------&&&&&&&&&&&&&SearchController ListDetOperModel.getSize()"
                        + "[" + ListDetOperModel.getSize() + "]Grid_SacabopXX ["
                        + BanEntrClientee.getModel().toString() + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

                BanEntrJuducial.setVisible(true);
                Clients.clearBusy();
            } else {
                Messagebox.show("NO EXISTEN DATOS DEL CLIENTE");
                Clients.clearBusy();
            }
        } else {
            Messagebox.show("KSDJAKLDJKLSJDKL----USER[VACIO]");
            Clients.clearBusy();
        }
        Clients.clearBusy();
    }

    public void kkkkkkkk() {

        sleep(10000);
        Clients.clearBusy();
        Messagebox.show("JJJJJJJJJJJJJJJJJJ");
    }

    public void GesSmallCore() {

        String rut;

        rut = buscarinput.getValue();

        String mm = rut.replace(".", "");
        String[] ParteEntera = rut.split("-");
        rutClienteEnCond = Integer.parseInt(ParteEntera[0]);
        ListGesSmallModel = new ListModelList<GesSmall>(_consultaSmallCore.GestionesSmallCore(rutClienteEnCond, Account));

        Map<String, Object> arguments = new HashMap<String, Object>();
        arguments.put("orderItems", ListGesSmallModel);
        String template = "Ejecutivo/include/GesSmallCore.zul";
        final Window windowx = (Window) Executions.createComponents(template, null, arguments);

        Button printButton = (Button) windowx.getFellow("closeButton2");

        printButton.addEventListener(Events.ON_CLICK, new EventListener() {
            @Override
            public void onEvent(Event event) {

                windowx.detach();

            }
        });

        try {
            windowx.doModal();
            windowx.setFocus(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
