/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.viewmodel;


import java.util.LinkedHashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;

        

public class SidebarViewModel {
	NavegacionPaginaSidebar currentPage;
        String VarPrueba="usuarios.zul";
	private Map<String, Map<String, NavegacionPaginaSidebar>> pageMap;
	
	@Init
	public void init() {
		initPageMap();
		currentPage = pageMap.get("ZK's Pizza").get("About Us");
                
                System.out.print("SIDEBAR ATUMATICO[[[["+currentPage.toString()+"]]]dsfsd]");
                System.out.print("PageMat[[[["+pageMap.values().toString()+"]]]dsfsd]");
	}

	@Command
	public void navigatePage(@BindingParam("target") NavegacionPaginaSidebar targetPage) {
		BindUtils.postNotifyChange(null, null, currentPage, "selected");
		currentPage = targetPage;
                  System.out.print("navigatePage[[[["+currentPage.getIncludeUri().toString()+"]]]dsfsd]");
		BindUtils.postNotifyChange(null, null, this, "currentPage");
                // pageref.setSrc("usuarios.zul");*/
                
	}
	
	public NavegacionPaginaSidebar getCurrentPage() {
		return currentPage;
	}

	public Map<String, Map<String, NavegacionPaginaSidebar>> getPageMap() {
		return pageMap;
	}
	
	private void initPageMap() {
		pageMap = new LinkedHashMap<String, Map<String, NavegacionPaginaSidebar>>();
		
		addPage("ZK's Pizza", "About Us", "usuarios.zul");
		addPage("ZK's Pizza", "Menu", "/home/menu.zul");
		addPage("ZK's Pizza", "FAQ", "/home/faq.zul");
		addPage("ZK's Pizza", "Press", "/home/press.zul");

		addPage("Customers", "Active Members", "/customers/customers.zul", "active");
		addPage("Customers", "Inactive Members", "/customers/customers.zul", "inactive");

		addPage("Orders", "In Preparation", "/orders/orders.zul", "in-preparation");
		addPage("Orders", "Ready for Shipping", "/orders/orders.zul", "ready");
		addPage("Orders", "Shipping", "/orders/orders.zul", "shipping");
		addPage("Orders", "Specified for Later", "/orders/orders.zul", "later");
		
		addPage("Fan Service", "Events", "/fan_service/events.zul");
		addPage("Fan Service", "Promotion", "/fan_service/promotion.zul");
	}

	private void addPage(String title, String subTitle, String includeUri) {
		addPage(title, subTitle, includeUri, null);
	}
	
	private void addPage(String title, String subTitle, String includeUri, String data) {
		String folder = "";
		Map<String, NavegacionPaginaSidebar> subPageMap = pageMap.get(title);
                
		if(subPageMap == null) {
			subPageMap = new LinkedHashMap<String, NavegacionPaginaSidebar>();
			pageMap.put(title, subPageMap);
		}
		NavegacionPaginaSidebar navigationPage = new NavegacionPaginaSidebar(title, subTitle, folder + includeUri + "?random=" + Math.random(), data) {
			@Override
			public boolean isSelected() {
				return currentPage == this;
			}
		};
		subPageMap.put(subTitle, navigationPage);
	}
}
