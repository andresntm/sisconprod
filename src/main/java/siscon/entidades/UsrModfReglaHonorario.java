/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class UsrModfReglaHonorario {

    int id_usr_modif;
    private float f_PorcentajeActual;
    private float f_PorcentajeAnterior;

    private int PorcentajeActual;
    private int PorcentajeAnterior;
    // String filian;

    public float getF_PorcentajeActual() {
        return f_PorcentajeActual;
    }

    public void setF_PorcentajeActual(float f_PorcentajeActual) {
        this.f_PorcentajeActual = f_PorcentajeActual;
    }

    public float getF_PorcentajeAnterior() {
        return f_PorcentajeAnterior;
    }

    public void setF_PorcentajeAnterior(float f_PorcentajeAnterior) {
        this.f_PorcentajeAnterior = f_PorcentajeAnterior;
    }

    // String filian;  
    public int getPorcentajeActual() {
        return PorcentajeActual;
    }

    public void setPorcentajeActual(int PorcentajeActual) {
        this.PorcentajeActual = PorcentajeActual;
    }

    public int getPorcentajeAnterior() {
        return PorcentajeAnterior;
    }

    public void setPorcentajeAnterior(int PorcentajeAnterior) {
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaHonorario(int PorcentajeActual, int PorcentajeAnterior) {
        this.PorcentajeActual = PorcentajeActual;
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaHonorario() {

        this.PorcentajeActual = 0;
        this.PorcentajeAnterior = 0;
    }

    public int getId_usr_modif() {
        return id_usr_modif;
    }

    public void setId_usr_modif(int id_usr_modif) {
        this.id_usr_modif = id_usr_modif;
    }

}
