/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import siscon.entidades.implementaciones.TipoProvisionJDBC;

/**
 *
 * @author exesilr
 */
public class TipoProvision {

    public TipoProvision(String dv_codtipocon, String dv_desc, String registrado, int di_id) {
        this.dv_codtipocon = dv_codtipocon;
        this.dv_desc = dv_desc;
        this.registrado = registrado;
        this.di_id = di_id;
    }

    public TipoProvision() {
    }

    public String getDv_codtipocon() {
        return dv_codtipocon;
    }

    public void setDv_codtipocon(String dv_codtipocon) {
        this.dv_codtipocon = dv_codtipocon;
    }

    public String getDv_desc() {
        return dv_desc;
    }

    public void setDv_desc(String dv_desc) {
        this.dv_desc = dv_desc;
    }

    public String getRegistrado() {
        return registrado;
    }

    public void setRegistrado(String registrado) {
        this.registrado = registrado;
    }

    public int getDi_id() {
        return di_id;
    }

    public void setDi_id(int di_id) {
        this.di_id = di_id;
    }

    private String dv_codtipocon;
    private String dv_desc;
    private String registrado;
    private int di_id;
    private TipoProvisionJDBC tPJDBC;
    private MvcConfig beans;
    
    public TipoProvision getTipoProv_X_Codigo(String codigo) {

        TipoProvision tP = null;
        try {
            beans = new MvcConfig();
            tPJDBC = new TipoProvisionJDBC(beans.getDataSource());
            tP = tPJDBC.getTipoProvision_X_Codigo(codigo);
        } catch (SQLException ex) {
            Logger.getLogger(TipoProvision.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            tP = null;
        } finally {
            return tP;
        }

    }
    
}
