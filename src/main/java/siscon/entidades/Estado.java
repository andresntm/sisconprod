/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.implementaciones.EstadoImplement;
import siscon.entidades.interfaces.EstadoInterfaz;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author excosoc
 */
public class Estado implements RowMapper<Estado> {

    /**
     * @return the di_id_Estado
     */
    public int getDi_id_Estado() {
        return di_id_Estado;
    }

    /**
     * @param di_id_Estado the di_id_Estado to set
     */
    public void setDi_id_Estado(int di_id_Estado) {
        this.di_id_Estado = di_id_Estado;
    }

    /**
     * @return the dv_cod_estado
     */
    public String getDv_cod_estado() {
        return dv_cod_estado;
    }

    /**
     * @param dv_cod_estado the dv_cod_estado to set
     */
    public void setDv_cod_estado(String dv_cod_estado) {
        this.dv_cod_estado = dv_cod_estado;
    }

    /**
     * @return the dv_lugar_estado
     */
    public String getDv_lugar_estado() {
        return dv_lugar_estado;
    }

    /**
     * @param dv_lugar_estado the dv_lugar_estado to set
     */
    public void setDv_lugar_estado(String dv_lugar_estado) {
        this.dv_lugar_estado = dv_lugar_estado;
    }

    /**
     * @return the dv_estado
     */
    public String getDv_estado() {
        return dv_estado;
    }

    /**
     * @param dv_estado the dv_estado to set
     */
    public void setDv_estado(String dv_estado) {
        this.dv_estado = dv_estado;
    }
    private int di_id_Estado;
    private String dv_cod_estado;
    private String dv_lugar_estado;
    private String dv_estado;
    private EstadoInterfaz eIntz;

    public Estado() {
        inicializa();
    }

    public Estado(int di_id_Estado, String dv_cod_estado, String dv_lugar_estado, String dv_estado) {
        this.di_id_Estado = di_id_Estado;
        this.dv_cod_estado = dv_cod_estado;
        this.dv_lugar_estado = dv_lugar_estado;
        this.dv_estado = dv_estado;
        inicializa();
    }

    @Override
    public Estado mapRow(ResultSet rs, int rowNum) throws SQLException {
        this.di_id_Estado = rs.getInt("di_id_Estado");
        this.dv_cod_estado = rs.getString("dv_cod_estado");
        this.dv_lugar_estado = rs.getString("dv_lugar_estado");
        this.dv_estado = rs.getString("dv_estado");
        return this;
    }

    private void inicializa() {
        eIntz = new EstadoImplement();
    }

    public void getEstado_X_IdEstado() {
        Estado est = new Estado();

        est = eIntz.getEstado_X_IdEstado(this.di_id_Estado);

        this.di_id_Estado = est.getDi_id_Estado();
        this.dv_cod_estado = est.getDv_cod_estado();
        this.dv_lugar_estado = est.getDv_lugar_estado();
        this.dv_estado = est.getDv_estado();
    }

    public void getEstado_X_DvCodEstado() {
        Estado est = new Estado();

        est = eIntz.getEstado_X_dvCodEstado(this.dv_cod_estado);

        this.di_id_Estado = est.getDi_id_Estado();
        this.dv_cod_estado = est.getDv_cod_estado();
        this.dv_lugar_estado = est.getDv_lugar_estado();
        this.dv_estado = est.getDv_estado();
    }

    public List<Estado> listEstados() {
        List<Estado> lEst = new ArrayList<Estado>();

        lEst = eIntz.listEstado();

        return lEst;
    }
}
