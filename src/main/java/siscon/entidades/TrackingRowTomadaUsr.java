/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.Date;
import java.sql.Timestamp;
import net.sourceforge.jtds.jdbc.DateTime;



/**
 *
 * @author exaesan
 */
public class TrackingRowTomadaUsr {
    
   int id_tracking;
   int id_colaborador;
   String ruta;
   String bandeja;
   java.sql.Timestamp fecha_evento ;
   java.sql.Timestamp fecha_evento_final ;
   int idRow;
   int estado;

    public Timestamp getFecha_evento_final() {
        return fecha_evento_final;
    }

    public void setFecha_evento_final(Timestamp fecha_evento_final) {
        this.fecha_evento_final = fecha_evento_final;
    }

    public int getId_tracking() {
        return id_tracking;
    }

    public void setId_tracking(int id_tracking) {
        this.id_tracking = id_tracking;
    }

    public int getId_colaborador() {
        return id_colaborador;
    }

    public void setId_colaborador(int id_colaborador) {
        this.id_colaborador = id_colaborador;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getBandeja() {
        return bandeja;
    }

    public void setBandeja(String bandeja) {
        this.bandeja = bandeja;
    }

    public Timestamp getFecha_evento() {
        return fecha_evento;
    }

    public void setFecha_evento(Timestamp fecha_evento) {
        this.fecha_evento = fecha_evento;
    }

    public int getIdRow() {
        return idRow;
    }

    public void setIdRow(int idRow) {
        this.idRow = idRow;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

   
   
   
    
}
