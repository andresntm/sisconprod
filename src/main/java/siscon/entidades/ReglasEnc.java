/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author excosoc
 */
public class ReglasEnc {

    /**
     * @return the activo
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    private int id_regla;
    private String desc;
    private int id_rangof;
    private int id_rangom;
    private boolean activo;

    public String getDescValor() {
        return DescValor;
    }

    public void setDescValor(String DescValor) {
        this.DescValor = DescValor;
    }
    private String DescValor;
    private List<ReglasDet> lValorReg;

    /**
     * @return the id_regla
     */
    public int getId_regla() {
        return id_regla;
    }

    /**
     * @param id_regla the id_regla to set
     */
    public void setId_regla(int id_regla) {
        this.id_regla = id_regla;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the id_rangof
     */
    public int getId_rangof() {
        return id_rangof;
    }

    /**
     * @param id_rangof the id_rangof to set
     */
    public void setId_rangof(int id_rangof) {
        this.id_rangof = id_rangof;
    }

    /**
     * @return the id_rangom
     */
    public int getId_rangom() {
        return id_rangom;
    }

    /**
     * @param id_rangom the id_rangom to set
     */
    public void setId_rangom(int id_rangom) {
        this.id_rangom = id_rangom;
    }

    /**
     * @return the lValorReg
     */
    public List<ReglasDet> getlValorReg() {
        return lValorReg;
    }

    /**
     * @param lValorReg the lValorReg to set
     */
    public void setlValorReg(List<ReglasDet> lValorReg) {
        this.lValorReg = lValorReg;
    }

    public ReglasEnc() {
    }

    public ReglasEnc(int id_regla,
            String desc,
            int id_rangof,
            int id_rangom,
            List<ReglasDet> lValorReg) {

        this.id_regla = id_regla;
        this.id_rangof = id_rangof;
        this.id_rangom = id_rangom;
        this.lValorReg = lValorReg;

    }

}
