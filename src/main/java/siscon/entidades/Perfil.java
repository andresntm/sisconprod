/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class Perfil {

    int id_perfil;
    String Descripcion;
    String CodPerfil;
    int id_jefe;
    
    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    
    
    public Perfil(String Descripcion, String CodPerfil, int id_jefe) {
        this.Descripcion = Descripcion;
        this.CodPerfil = CodPerfil;
        this.id_jefe = id_jefe;
    }

    public Perfil() {
    }

 
    

    
       public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getCodPerfil() {
        return CodPerfil;
    }

    public void setCodPerfil(String CodPerfil) {
        this.CodPerfil = CodPerfil;
    }

    public int getId_jefe() {
        return id_jefe;
    }

    public void setId_jefe(int id_jefe) {
        this.id_jefe = id_jefe;
    }
}
