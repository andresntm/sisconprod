/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

public class InformeEfecv2 {


    
  
public float total_interes_condona;
public float total_honorarios_condona;
public int id_condonacion;
public String ejecutivo_condona;
public String fecha_aprobacion;
public String fecha_aplicacion;
public int numero_reparos;
public String aprovado_por;
public String cedente;	
public int numero_garantias;
public int notificado;
public float sgn;
public float porcentaje_cond_cap;
public float porcentaje_cond_honor;
public float porcentaje_cond_int;
public String Nombre_Colab;
public String Unidad;
public String Nombre_Lugar_de_Trabajo;
public String Nombre_Jefe_Completo;
public int id;
public int rut_cliente;
public float total_capital;
public float total_interes;
public float total_honorarios;
public float vdes;
public float total_capital_recibe;
public float total_interes_recibe;
public float total_honorarios_recibe;
public float total_capital_condona;
public String creacion; 


    
    
      public InformeEfecv2() {
    }

    
    public InformeEfecv2(float total_interes_condona, float total_honorarios_condona, int id_condonacion, String ejecutivo_condona, String fecha_aprobacion, String fecha_aplicacion, int numero_reparos, String aprovado_por, String cedente, int numero_garantias, int notificado, float sgn, float porcentaje_cond_cap, float porcentaje_cond_honor, float porcentaje_cond_int, String Nombre_Colab, String Unidad, String Nombre_Lugar_de_Trabajo, String Nombre_Jefe_Completo, int id, int rut_cliente, float total_capital, float total_interes, float total_honorarios, float vdes, float total_capital_recibe, float total_interes_recibe, float total_honorarios_recibe, float total_capital_condona,String creacion) {
        this.creacion=creacion;
        this.total_interes_condona = total_interes_condona;
        this.total_honorarios_condona = total_honorarios_condona;
        this.id_condonacion = id_condonacion;
        this.ejecutivo_condona = ejecutivo_condona;
        this.fecha_aprobacion = fecha_aprobacion;
        this.fecha_aplicacion = fecha_aplicacion;
        this.numero_reparos = numero_reparos;
        this.aprovado_por = aprovado_por;
        this.cedente = cedente;
        this.numero_garantias = numero_garantias;
        this.notificado = notificado;
        this.sgn = sgn;
        this.porcentaje_cond_cap = porcentaje_cond_cap;
        this.porcentaje_cond_honor = porcentaje_cond_honor;
        this.porcentaje_cond_int = porcentaje_cond_int;
        this.Nombre_Colab = Nombre_Colab;
        this.Unidad = Unidad;
        this.Nombre_Lugar_de_Trabajo = Nombre_Lugar_de_Trabajo;
        this.Nombre_Jefe_Completo = Nombre_Jefe_Completo;
        this.id = id;
        this.rut_cliente = rut_cliente;
        this.total_capital = total_capital;
        this.total_interes = total_interes;
        this.total_honorarios = total_honorarios;
        this.vdes = vdes;
        this.total_capital_recibe = total_capital_recibe;
        this.total_interes_recibe = total_interes_recibe;
        this.total_honorarios_recibe = total_honorarios_recibe;
        this.total_capital_condona = total_capital_condona;
    }
  
    
    
    
    
    public String getCreacion() {
        return creacion;
    }

    public void setCreacion(String creacion) {
        this.creacion = creacion;
    }


    public String getNombre_Colab() {
        return Nombre_Colab;
    }

    public void setNombre_Colab(String Nombre_Colab) {
        this.Nombre_Colab = Nombre_Colab;
    }

    public String getUnidad() {
        return Unidad;
    }

    public void setUnidad(String Unidad) {
        this.Unidad = Unidad;
    }

    public String getNombre_Lugar_de_Trabajo() {
        return Nombre_Lugar_de_Trabajo;
    }

    public void setNombre_Lugar_de_Trabajo(String Nombre_Lugar_de_Trabajo) {
        this.Nombre_Lugar_de_Trabajo = Nombre_Lugar_de_Trabajo;
    }

    public String getNombre_Jefe_Completo() {
        return Nombre_Jefe_Completo;
    }

    public void setNombre_Jefe_Completo(String Nombre_Jefe_Completo) {
        this.Nombre_Jefe_Completo = Nombre_Jefe_Completo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRut_cliente() {
        return rut_cliente;
    }

    public void setRut_cliente(int rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    public float getTotal_capital() {
        return total_capital;
    }

    public void setTotal_capital(float total_capital) {
        this.total_capital = total_capital;
    }

    public float getTotal_interes() {
        return total_interes;
    }

    public void setTotal_interes(float total_interes) {
        this.total_interes = total_interes;
    }

    public float getTotal_honorarios() {
        return total_honorarios;
    }

    public void setTotal_honorarios(float total_honorarios) {
        this.total_honorarios = total_honorarios;
    }

    public float getVdes() {
        return vdes;
    }

    public void setVdes(float vdes) {
        this.vdes = vdes;
    }

    public float getTotal_capital_recibe() {
        return total_capital_recibe;
    }

    public void setTotal_capital_recibe(float total_capital_recibe) {
        this.total_capital_recibe = total_capital_recibe;
    }

    public float getTotal_interes_recibe() {
        return total_interes_recibe;
    }

    public void setTotal_interes_recibe(float total_interes_recibe) {
        this.total_interes_recibe = total_interes_recibe;
    }

    public float getTotal_honorarios_recibe() {
        return total_honorarios_recibe;
    }

    public void setTotal_honorarios_recibe(float total_honorarios_recibe) {
        this.total_honorarios_recibe = total_honorarios_recibe;
    }

    public float getTotal_capital_condona() {
        return total_capital_condona;
    }

    public void setTotal_capital_condona(float total_capital_condona) {
        this.total_capital_condona = total_capital_condona;
    }

    public float getTotal_interes_condona() {
        return total_interes_condona;
    }

    public void setTotal_interes_condona(float total_interes_condona) {
        this.total_interes_condona = total_interes_condona;
    }

    public float getTotal_honorarios_condona() {
        return total_honorarios_condona;
    }

    public void setTotal_honorarios_condona(float total_honorarios_condona) {
        this.total_honorarios_condona = total_honorarios_condona;
    }

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getEjecutivo_condona() {
        return ejecutivo_condona;
    }

    public void setEjecutivo_condona(String ejecutivo_condona) {
        this.ejecutivo_condona = ejecutivo_condona;
    }

    public String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    public void setFecha_aprobacion(String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    public String getFecha_aplicacion() {
        return fecha_aplicacion;
    }

    public void setFecha_aplicacion(String fecha_aplicacion) {
        this.fecha_aplicacion = fecha_aplicacion;
    }

    public int getNumero_reparos() {
        return numero_reparos;
    }

    public void setNumero_reparos(int numero_reparos) {
        this.numero_reparos = numero_reparos;
    }

    public String getAprovado_por() {
        return aprovado_por;
    }

    public void setAprovado_por(String aprovado_por) {
        this.aprovado_por = aprovado_por;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public int getNumero_garantias() {
        return numero_garantias;
    }

    public void setNumero_garantias(int numero_garantias) {
        this.numero_garantias = numero_garantias;
    }

    public int getNotificado() {
        return notificado;
    }

    public void setNotificado(int notificado) {
        this.notificado = notificado;
    }

    public float getSgn() {
        return sgn;
    }

    public void setSgn(float sgn) {
        this.sgn = sgn;
    }

    public float getPorcentaje_cond_cap() {
        return porcentaje_cond_cap;
    }

    public void setPorcentaje_cond_cap(float porcentaje_cond_cap) {
        this.porcentaje_cond_cap = porcentaje_cond_cap;
    }

    public float getPorcentaje_cond_honor() {
        return porcentaje_cond_honor;
    }

    public void setPorcentaje_cond_honor(float porcentaje_cond_honor) {
        this.porcentaje_cond_honor = porcentaje_cond_honor;
    }

    public float getPorcentaje_cond_int() {
        return porcentaje_cond_int;
    }

    public void setPorcentaje_cond_int(float porcentaje_cond_int) {
        this.porcentaje_cond_int = porcentaje_cond_int;
    }



}
