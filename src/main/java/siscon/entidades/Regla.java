/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class Regla {

    String Perfil;
    String DescRegla;

    int RanfoFInicio;
    int RangoFFin;
    String UndMedF;
    float RangoMontoI;
    float RangoMontoM;
    String DesTipoValor;
    int PorcentajeCondonacion100;
    float porcentaje0_100;
    String PorcentajeStringCOndonacion;
    int idRegla;

    public Regla(String Perfil, String DescRegla, int RanfoFInicio, int RangoFFin, String UndMedF, float RangoMontoI, float RangoMontoM, String DesTipoValor, int PorcentajeCondonacion100, float porcentaje0_100, String PorcentajeStringCOndonacion) {
        this.Perfil = Perfil;
        this.DescRegla = DescRegla;
        this.RanfoFInicio = RanfoFInicio;
        this.RangoFFin = RangoFFin;
        this.UndMedF = UndMedF;
        this.RangoMontoI = RangoMontoI;
        this.RangoMontoM = RangoMontoM;
        this.DesTipoValor = DesTipoValor;
        this.PorcentajeCondonacion100 = PorcentajeCondonacion100;
        this.porcentaje0_100 = porcentaje0_100;
        this.PorcentajeStringCOndonacion = PorcentajeStringCOndonacion;
    }

    public Regla() {
    }

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }

    public String getDescRegla() {
        return DescRegla;
    }

    public void setDescRegla(String DescRegla) {
        this.DescRegla = DescRegla;
    }

    public int getRanfoFInicio() {
        return RanfoFInicio;
    }

    public void setRanfoFInicio(int RanfoFInicio) {
        this.RanfoFInicio = RanfoFInicio;
    }

    public int getRangoFFin() {
        return RangoFFin;
    }

    public void setRangoFFin(int RangoFFin) {
        this.RangoFFin = RangoFFin;
    }

    public String getUndMedF() {
        return UndMedF;
    }

    public void setUndMedF(String UndMedF) {
        this.UndMedF = UndMedF;
    }

    public float getRangoMontoI() {
        return RangoMontoI;
    }

    public void setRangoMontoI(float RangoMontoI) {
        this.RangoMontoI = RangoMontoI;
    }

    public float getRangoMontoM() {
        return RangoMontoM;
    }

    public void setRangoMontoM(float RangoMontoM) {
        this.RangoMontoM = RangoMontoM;
    }

    public String getDesTipoValor() {
        return DesTipoValor;
    }

    public void setDesTipoValor(String DesTipoValor) {
        this.DesTipoValor = DesTipoValor;
    }

    public int getPorcentajeCondonacion100() {
        return PorcentajeCondonacion100;
    }

    public void setPorcentajeCondonacion100(int PorcentajeCondonacion100) {
        this.PorcentajeCondonacion100 = PorcentajeCondonacion100;
    }

    public float getPorcentaje0_100() {
        return porcentaje0_100;
    }

    public void setPorcentaje0_100(float porcentaje0_100) {
        this.porcentaje0_100 = porcentaje0_100;
    }

    public String getPorcentajeStringCOndonacion() {
        return PorcentajeStringCOndonacion;
    }

    public void setPorcentajeStringCOndonacion(String PorcentajeStringCOndonacion) {
        this.PorcentajeStringCOndonacion = PorcentajeStringCOndonacion;
    }

    public int getIdRegla() {
        return idRegla;
    }

    public void setIdRegla(int idRegla) {
        this.idRegla = idRegla;
    }

}
