/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class UsrMontoAtribucion {

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGlosaAtribucion() {
        return GlosaAtribucion;
    }

    public void setGlosaAtribucion(String GlosaAtribucion) {
        this.GlosaAtribucion = GlosaAtribucion;
    }

    public float getMonto_minimo() {
        return monto_minimo;
    }

    public void setMonto_minimo(float monto_minimo) {
        this.monto_minimo = monto_minimo;
    }

    public float getMonto_maximo() {
        return monto_maximo;
    }

    public void setMonto_maximo(float monto_maximo) {
        this.monto_maximo = monto_maximo;
    }

    public UsrMontoAtribucion(String alias, String GlosaAtribucion, float monto_minimo, float monto_maximo) {
        this.alias = alias;
        this.GlosaAtribucion = GlosaAtribucion;
        this.monto_minimo = monto_minimo;
        this.monto_maximo = monto_maximo;
    }

    public UsrMontoAtribucion() {
                this.alias = "";
        this.GlosaAtribucion = "";
        this.monto_minimo = 0;
        this.monto_maximo = 0;
    }




    
   private String alias;
   private String GlosaAtribucion;
   private float monto_minimo;
   private float monto_maximo;
   // String filian;
    
    
}
