/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class SbifCliente {



    public SbifCliente() {
    }

    public String getFld_per() {
        return fld_per;
    }

    public void setFld_per(String fld_per) {
        this.fld_per = fld_per;
    }

    public String getFld_lin() {
        return fld_lin;
    }

    public void setFld_lin(String fld_lin) {
        this.fld_lin = fld_lin;
    }

    public String getFld_num_ins() {
        return fld_num_ins;
    }

    public void setFld_num_ins(String fld_num_ins) {
        this.fld_num_ins = fld_num_ins;
    }

    public String getFld_dir_tot() {
        return fld_dir_tot;
    }

    public void setFld_dir_tot(String fld_dir_tot) {
        this.fld_dir_tot = fld_dir_tot;
    }

    public String getFld_deu_con() {
        return fld_deu_con;
    }

    public void setFld_deu_con(String fld_deu_con) {
        this.fld_deu_con = fld_deu_con;
    }

    public String getFld_deu_hip() {
        return fld_deu_hip;
    }

    public void setFld_deu_hip(String fld_deu_hip) {
        this.fld_deu_hip = fld_deu_hip;
    }

    public String getFld_deu_com() {
        return fld_deu_com;
    }

    public void setFld_deu_com(String fld_deu_com) {
        this.fld_deu_com = fld_deu_com;
    }

    public String getFld_deu_ind() {
        return fld_deu_ind;
    }

    public void setFld_deu_ind(String fld_deu_ind) {
        this.fld_deu_ind = fld_deu_ind;
    }

    public String getFld_dir_mor() {
        return fld_dir_mor;
    }

    public void setFld_dir_mor(String fld_dir_mor) {
        this.fld_dir_mor = fld_dir_mor;
    }

    public String getFld_dir_vda() {
        return fld_dir_vda;
    }

    public void setFld_dir_vda(String fld_dir_vda) {
        this.fld_dir_vda = fld_dir_vda;
    }

    public String getFld_dir_cas() {
        return fld_dir_cas;
    }

    public void setFld_dir_cas(String fld_dir_cas) {
        this.fld_dir_cas = fld_dir_cas;
    }

    public String getFld_ind_vda() {
        return fld_ind_vda;
    }

    public void setFld_ind_vda(String fld_ind_vda) {
        this.fld_ind_vda = fld_ind_vda;
    }

    public String getFld_ind_cas() {
        return fld_ind_cas;
    }

    public void setFld_ind_cas(String fld_ind_cas) {
        this.fld_ind_cas = fld_ind_cas;
    }

    public SbifCliente(String fld_per, String fld_lin, String fld_num_ins, String fld_dir_tot, String fld_deu_con, String fld_deu_hip, String fld_deu_com, String fld_deu_ind, String fld_dir_mor, String fld_dir_vda, String fld_dir_cas, String fld_ind_vda, String fld_ind_cas) {
        this.fld_per = fld_per;
        this.fld_lin = fld_lin;
        this.fld_num_ins = fld_num_ins;
        this.fld_dir_tot = fld_dir_tot;
        this.fld_deu_con = fld_deu_con;
        this.fld_deu_hip = fld_deu_hip;
        this.fld_deu_com = fld_deu_com;
        this.fld_deu_ind = fld_deu_ind;
        this.fld_dir_mor = fld_dir_mor;
        this.fld_dir_vda = fld_dir_vda;
        this.fld_dir_cas = fld_dir_cas;
        this.fld_ind_vda = fld_ind_vda;
        this.fld_ind_cas = fld_ind_cas;
    }

String fld_per;		
String fld_lin;	
String fld_num_ins;
String fld_dir_tot;
String fld_deu_con;
String fld_deu_hip;
String fld_deu_com;
String fld_deu_ind;
String fld_dir_mor;
String fld_dir_vda;
String fld_dir_cas;
String fld_ind_vda;
String fld_ind_cas;

  
    
    
    


}
