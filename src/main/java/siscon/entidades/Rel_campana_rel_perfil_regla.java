/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class Rel_campana_rel_perfil_regla {

    private int id;
    private int id_campana;
    private int id_rel_perfil_regla;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_campana
     */
    public int getId_campana() {
        return id_campana;
    }

    /**
     * @param id_campana the id_campana to set
     */
    public void setId_campana(int id_campana) {
        this.id_campana = id_campana;
    }

    /**
     * @return the id_rel_perfil_regla
     */
    public int getId_rel_perfil_regla() {
        return id_rel_perfil_regla;
    }

    /**
     * @param id_rel_perfil_regla the id_rel_perfil_regla to set
     */
    public void setId_rel_perfil_regla(int id_rel_perfil_regla) {
        this.id_rel_perfil_regla = id_rel_perfil_regla;
    }

    public Rel_campana_rel_perfil_regla() {
    }

    public Rel_campana_rel_perfil_regla(int id,
            int id_campana,
            int id_rel_perfil_regla) {
        this.id = id;
        this.id_campana = id_campana;
        this.id_rel_perfil_regla = id_rel_perfil_regla;
    }
}
