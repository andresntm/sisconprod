/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class HornorariosJudicialesCliente {

    private String cedente;
    private String operacion1;
    private String operacion2;
    private String rut;
    float capital;
    float porcentajeHonoratiocondonado;
    float porcentajeAtribucion;
    float capital_arecibir;
    int rutcliente;
    float Honorario_condona;
    float Honorario_recibe;
    float capitalCondonado;
    float porcentajeCapital;

    float HONOR_F1;
    float HONOR_F2;
    float HONOR_F3;

    public float getSaldoInsoluto() {
        return SaldoInsoluto;
    }

    public void setSaldoInsoluto(float SaldoInsoluto) {
        this.SaldoInsoluto = SaldoInsoluto;
    }
    float SaldoInsoluto;
    

    public String getHONOR_F1S() {
        return HONOR_F1S;
    }

    public void setHONOR_F1S(String HONOR_F1S) {
        this.HONOR_F1S = HONOR_F1S;
    }

    public String getHONOR_F2S() {
        return HONOR_F2S;
    }

    public void setHONOR_F2S(String HONOR_F2S) {
        this.HONOR_F2S = HONOR_F2S;
    }

    public String getHONOR_F3S() {
        return HONOR_F3S;
    }

    public void setHONOR_F3S(String HONOR_F3S) {
        this.HONOR_F3S = HONOR_F3S;
    }
    String HONOR_F1S;
    String HONOR_F2S;
    String HONOR_F3S;
    
    
    public float getHONOR_F1() {
        return HONOR_F1;
    }

    public void setHONOR_F1(float HONOR_F1) {
        this.HONOR_F1 = HONOR_F1;
    }

    public float getHONOR_F2() {
        return HONOR_F2;
    }

    public void setHONOR_F2(float HONOR_F2) {
        this.HONOR_F2 = HONOR_F2;
    }

    public float getHONOR_F3() {
        return HONOR_F3;
    }

    public void setHONOR_F3(float HONOR_F3) {
        this.HONOR_F3 = HONOR_F3;
    }
    



    public float getCapitalCondonado() {
        return capitalCondonado;
    }

    public void setCapitalCondonado(float capitalCondonado) {
        this.capitalCondonado = capitalCondonado;
    }  
        public float getPorcentajeCapital() {
        return porcentajeCapital;
    }

    public void setPorcentajeCapital(float porcentajeCapital) {
        this.porcentajeCapital = porcentajeCapital;
    }
    
    public int getRutcliente() {
        return rutcliente;
    }

    public void setRutcliente(int rutcliente) {
        this.rutcliente = rutcliente;
    }
    
    
    public HornorariosJudicialesCliente() {
      this.HONOR_F1=0;
      this.HONOR_F2=0;
      this.HONOR_F3=0;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getOperacion1() {
        return operacion1;
    }

    public void setOperacion1(String operacion1) {
        this.operacion1 = operacion1;
    }

    public String getOperacion2() {
        return operacion2;
    }

    public void setOperacion2(String operacion2) {
        this.operacion2 = operacion2;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public float getCapital() {
        return capital;
    }

    public void setCapital(float capital) {
        this.capital = capital;
    }

    public float getPorcentajeHonoratiocondonado() {
        return porcentajeHonoratiocondonado;
    }

    public void setPorcentajeHonoratiocondonado(float porcentajeHonoratiocondonado) {
        this.porcentajeHonoratiocondonado = porcentajeHonoratiocondonado;
    }

    public float getPorcentajeAtribucion() {
        return porcentajeAtribucion;
    }

    public void setPorcentajeAtribucion(float porcentajeAtribucion) {
        this.porcentajeAtribucion = porcentajeAtribucion;
    }

    public float getCapital_arecibir() {
        return capital_arecibir;
    }

    public void setCapital_arecibir(float capital_arecibir) {
        this.capital_arecibir = capital_arecibir;
    }



        public float getHonorario_condona() {
        return Honorario_condona;
    }

    public void setHonorario_condona(float Honorario_condona) {
        this.Honorario_condona = Honorario_condona;
    }

    public float getHonorario_recibe() {
        return Honorario_recibe;
    }

    public void setHonorario_recibe(float Honorario_recibe) {
        this.Honorario_recibe = Honorario_recibe;
    }
    
  
    
    


}
