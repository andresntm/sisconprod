/**
 * Condonacion.java - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscon.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author exesilr
 */
@Entity
public class Condonacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private List<DetalleCliente> ListaOperaciones;
    //Informacion Ejecutiva
    String NombreEjecutiva;
    String EdadEjecutiva;
    String RutEjecutiva;
    String DireccionEjecutiva;
    String NumerodeCondonacionEjecutiva;
    double MontoMaximoAtribucionEjecutiva;
    int nuemrodeComitesAbiertos;
    String FechaActual;
    String rangoFechaInicio;
    String RangoFehaFin;
    double PorcentajeCondonaCapital;
    double PorcentajeCondonaInteres;
    double PorcentajeCondonaHonorario;

    /// Informacion del Cliente
    String NombreCliente;
    String Direcciones;
    String NumeroTelefono;
    String RutCliente;

    //Informacion de la Condonacion
    boolean PuedeCondonarEnLinea;
    String FechaPrimerCatigo;
    int NumeroMesesPrimerCastigo;
    double CapitalTotal;
    double MontoCondonado;
    double MontoARecibir;
    double CapitalCondonado;
    double InteresCondonado;
    public ValorMonto TotoalCapital;
    public ValorMonto TotoalCondonado;
    public ValorMonto TotoalRecibe;
    public ValorMonto MontoAtribucionMaxima;
    public ValorMonto HonorarioJudicial;
    public ValorMonto TotalTotal;
    boolean TieneJuicioActivo;
    boolean TieneRol;
    String acceso;
    public Condonador _condonador;
    public ValorMonto TotalHonoJudsobreCondonado;
    int numeroDeOperaciones;
    private GlosaENC glosa;
    private AdjuntarENC adjuntar;
    float f_porcentajeIntz;
    float f_porcentajeCapl;
    float f_porcentajeHono;
    public HonorInfo _InfHon;

    public float getMonto_VDE_SGN() {
        return monto_VDE_SGN;
    }

    public void setMonto_VDE_SGN(float monto_VDE_SGN) {
        this.monto_VDE_SGN = monto_VDE_SGN;
    }

    public float getHonorarios_condonados() {
        return honorarios_condonados;
    }

    public void setHonorarios_condonados(float honorarios_condonados) {
        this.honorarios_condonados = honorarios_condonados;
    }
    float monto_VDE_SGN;
    float honorarios_condonados;

    public Condonacion(String rutclie, String RutEje, String idAcces) {

        this.RutCliente = rutclie;
        this.RutEjecutiva = RutEje;
        this.acceso = idAcces;
        _condonador = new Condonador();
        this.NumeroMesesPrimerCastigo = 0;
        this.TotalHonoJudsobreCondonado = new ValorMonto(0);
        this.honorarios_condonados = 0;
        this.monto_VDE_SGN = 0;
        this.HonorarioJudicial=new ValorMonto(0);
    }

    public float getF_porcentajeIntz() {
        return f_porcentajeIntz;
    }

    public void setF_porcentajeIntz(float f_porcentajeIntz) {
        this.f_porcentajeIntz = f_porcentajeIntz;
    }

    public float getF_porcentajeCapl() {
        return f_porcentajeCapl;
    }

    public void setF_porcentajeCapl(float f_porcentajeCapl) {
        this.f_porcentajeCapl = f_porcentajeCapl;
    }

    public float getF_porcentajeHono() {
        return f_porcentajeHono;
    }

    public void setF_porcentajeHono(float f_porcentajeHono) {
        this.f_porcentajeHono = f_porcentajeHono;
    }

    public Condonador getCondonador() {
        return _condonador;
    }

    public void setCondonador(Condonador _condonador) {
        this._condonador = _condonador;
    }

    public Condonacion() {
        _condonador = new Condonador();
        this.NumeroMesesPrimerCastigo = 0;
    }

    public ValorMonto getHonorarioJudicial2() {
        return this.TotalHonoJudsobreCondonado;
    }

    public void setHonorarioJudicial2(double monto) {
        this.TotalHonoJudsobreCondonado = new ValorMonto(monto);
    }

    public String getHonorarioJudicial2Pesos() {

        return this.TotalHonoJudsobreCondonado.getValorPesos();

    }

    public double getHonorarioJudicial2Float() {

        return this.TotalHonoJudsobreCondonado.getValor();

    }

    public boolean isTieneRol() {
        return TieneRol;
    }

    public void setTieneRol(boolean TieneRol) {
        this.TieneRol = TieneRol;
    }

    public int getNumeroDeOperaciones() {
        return numeroDeOperaciones;
    }

    public void setNumeroDeOperaciones(int numeroDeOperaciones) {
        this.numeroDeOperaciones = numeroDeOperaciones;
    }

    public ValorMonto getTotoalRecibe() {
        return this.TotoalRecibe;
    }

    public void setTotoalRecibe(double monto) {
        this.TotoalRecibe = new ValorMonto(monto);
    }

    public ValorMonto getTotalTotal() {
        return this.TotalTotal;
    }

    public void setTotalTotal(double monto) {
        this.TotalTotal = new ValorMonto(monto);
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    int idCliente;

    public boolean isTieneJuicio() {
        return TieneJuicioActivo;
    }

    public void setTieneJuicio(boolean TieneJuicioActivo) {
        this.TieneJuicioActivo = TieneJuicioActivo;
    }

    public List<DetalleCliente> getListaOperaciones() {
        return ListaOperaciones;
    }

    public void setListaOperaciones(List<DetalleCliente> ListaOperaciones) {
        this.ListaOperaciones = ListaOperaciones;
    }

    public ValorMonto getTotalCondonado() {
        return this.TotoalCondonado;
    }

    public void setTotalCondonado(double monto) {
        this.TotoalCondonado = new ValorMonto(monto);
    }

    /**
     * Retorna El Valor Calculado del monto honorarios Judiciales.
     *
     * @return Retorna un Valor de Tipo . When {
     * @paramref a} is null, we rely on b for the discombobulation.
     */
    public ValorMonto getHonorarioJudicial() {
        return this.HonorarioJudicial;
        //   return this.h
    }

    /**
     * Establece el valor . * When {
     *
     * @paramref a} si es null, se establece un valor por defecto 0.
     * @param monto es el parametro de entrada de tipo double
     *
     */
    public void setHonorarioJudicial(double monto) {
        // this.TotoalCondonado = new ValorMonto(monto);
        this.HonorarioJudicial = new ValorMonto(monto);
    }

    public ValorMonto getMontoAtribucionMaxima() {
        return this.MontoAtribucionMaxima;
    }

    public void setMontoAtribucionMaxima(double monto) {
        this.MontoAtribucionMaxima = new ValorMonto(monto);
    }

    public ValorMonto getCapitalClass() {
        return TotoalCapital;
    }

    public void setCapitalClass(double MontoARecibir3www) {
        this.TotoalCapital = new ValorMonto(MontoARecibir3www);
    }

    public String getNombreEjecutiva() {
        return NombreEjecutiva;
    }

    public void setNombreEjecutiva(String NombreEjecutiva) {
        this.NombreEjecutiva = NombreEjecutiva;
    }

    public String getEdadEjecutiva() {
        return EdadEjecutiva;
    }

    public void setEdadEjecutiva(String EdadEjecutiva) {
        this.EdadEjecutiva = EdadEjecutiva;
    }

    public String getDireccionEjecutiva() {
        return DireccionEjecutiva;
    }

    public void setDireccionEjecutiva(String DireccionEjecutiva) {
        this.DireccionEjecutiva = DireccionEjecutiva;
    }

    public String getNumerodeCondonacionEjecutiva() {
        return NumerodeCondonacionEjecutiva;
    }

    public void setNumerodeCondonacionEjecutiva(String NumerodeCondonacionEjecutiva) {
        this.NumerodeCondonacionEjecutiva = NumerodeCondonacionEjecutiva;
    }

    public double getMontoMaximoAtribucionEjecutiva() {
        return MontoMaximoAtribucionEjecutiva;
    }

    public void setMontoMaximoAtribucionEjecutiva(double MontoMaximoAtribucionEjecutiva) {
        this.MontoMaximoAtribucionEjecutiva = MontoMaximoAtribucionEjecutiva;
    }

    public int getNuemrodeComitesAbiertos() {
        return nuemrodeComitesAbiertos;
    }

    public void setNuemrodeComitesAbiertos(int nuemrodeComitesAbiertos) {
        this.nuemrodeComitesAbiertos = nuemrodeComitesAbiertos;
    }

    public String getFechaActual() {
        return FechaActual;
    }

    public void setFechaActual(String FechaActual) {
        this.FechaActual = FechaActual;
    }

    public String getRangoFechaInicio() {
        return rangoFechaInicio;
    }

    public void setRangoFechaInicio(String rangoFechaInicio) {
        this.rangoFechaInicio = rangoFechaInicio;
    }

    public String getRangoFehaFin() {
        return RangoFehaFin;
    }

    public void setRangoFehaFin(String RangoFehaFin) {
        this.RangoFehaFin = RangoFehaFin;
    }

    public double getPorcentajeCondonaCapital() {
        return PorcentajeCondonaCapital;
    }

    public void setPorcentajeCondonaCapital(double PorcentajeCondonaCapital) {
        this.PorcentajeCondonaCapital = PorcentajeCondonaCapital;
    }

    public double getPorcentajeCondonaInteres() {
        return PorcentajeCondonaInteres;
    }

    public void setPorcentajeCondonaInteres(double PorcentajeCondonaInteres) {
        this.PorcentajeCondonaInteres = PorcentajeCondonaInteres;
    }

    public double getPorcentajeCondonaHonorario() {
        return PorcentajeCondonaHonorario;
    }

    public void setPorcentajeCondonaHonorario(double PorcentajeCondonaHonorario) {
        this.PorcentajeCondonaHonorario = PorcentajeCondonaHonorario;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String NombreCliente) {
        this.NombreCliente = NombreCliente;
    }

    public String getDirecciones() {
        return Direcciones;
    }

    public void setDirecciones(String Direcciones) {
        this.Direcciones = Direcciones;
    }

    public String getNumeroTelefono() {
        return NumeroTelefono;
    }

    public void setNumeroTelefono(String NumeroTelefono) {
        this.NumeroTelefono = NumeroTelefono;
    }

    public String getRutCliente() {
        return RutCliente;
    }

    public void setRutCliente(String RutCliente) {
        this.RutCliente = RutCliente;
    }

    public boolean isPuedeCondonarEnLinea() {
        return PuedeCondonarEnLinea;
    }

    public void setPuedeCondonarEnLinea(boolean PuedeCondonarEnLinea) {
        this.PuedeCondonarEnLinea = PuedeCondonarEnLinea;
    }

    public String getFechaPrimerCatigo() {
        return FechaPrimerCatigo;
    }

    public void setFechaPrimerCatigo(String FechaPrimerCatigo) {
        this.FechaPrimerCatigo = FechaPrimerCatigo;
    }

    public int getNumeroMesesPrimerCastigo() {
        return NumeroMesesPrimerCastigo;
    }

    public void setNumeroMesesPrimerCastigo(int NumeroMesesPrimerCastigo) {
        this.NumeroMesesPrimerCastigo = NumeroMesesPrimerCastigo;
    }

    public double getCapitalTotal() {
        return CapitalTotal;
    }

    public void setCapitalTotal(double CapitalTotal) {
        this.CapitalTotal = CapitalTotal;
    }

    public double getMontoCondonado() {
        return MontoCondonado;
    }

    public void setMontoCondonado(double MontoCondonado) {
        this.MontoCondonado = MontoCondonado;
    }

    public double getMontoARecibir() {
        return MontoARecibir;
    }

    public void setMontoARecibir(double MontoARecibir) {
        this.MontoARecibir = MontoARecibir;
    }

    public double getCapitalCondonado() {
        return CapitalCondonado;
    }

    public void setCapitalCondonado(double CapitalCondonado) {
        this.CapitalCondonado = CapitalCondonado;
    }

    public double getInteresCondonado() {
        return InteresCondonado;
    }

    public void setInteresCondonado(double InteresCondonado) {
        this.InteresCondonado = InteresCondonado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Condonacion)) {
            return false;
        }
        Condonacion other = (Condonacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "siscon.entidades.condonacion[ id=" + id + " ]";
    }

    /**
     * @return the glosa
     */
    public GlosaENC getGlosa() {
        return glosa;
    }

    /**
     * @param glosa the glosa to set
     */
    public void setGlosa(GlosaENC glosa) {
        this.glosa = glosa;
    }

    /**
     * @return the adjuntar
     */
    public AdjuntarENC getAdjuntar() {
        return adjuntar;
    }

    /**
     * @param adjuntar the adjuntar to set
     */
    public void setAdjuntar(AdjuntarENC adjuntar) {
        this.adjuntar = adjuntar;
    }

}
