/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.Date;

/**
 *
 * @author excosoc
 */
public class MarcaBanco {

    /**
     * @return the id_marcaBanco
     */
    public int getId_marcaBanco() {
        return id_marcaBanco;
    }

    /**
     * @param id_marcaBanco the id_marcaBanco to set
     */
    public void setId_marcaBanco(int id_marcaBanco) {
        this.id_marcaBanco = id_marcaBanco;
    }

    /**
     * @return the di_Id_fk_idCliente
     */
    public int getDi_Id_fk_idCliente() {
        return di_Id_fk_idCliente;
    }

    /**
     * @param di_Id_fk_idCliente the di_Id_fk_idCliente to set
     */
    public void setDi_Id_fk_idCliente(int di_Id_fk_idCliente) {
        this.di_Id_fk_idCliente = di_Id_fk_idCliente;
    }

    /**
     * @return the registrado
     */
    public Date getRegistrado() {
        return registrado;
    }

    /**
     * @param registrado the registrado to set
     */
    public void setRegistrado(Date registrado) {
        this.registrado = registrado;
    }

    /**
     * @return the db_marcaRechaso
     */
    public boolean isDb_marcaRechaso() {
        return db_marcaRechaso;
    }

    /**
     * @param db_marcaRechaso the db_marcaRechaso to set
     */
    public void setDb_marcaRechaso(boolean db_marcaRechaso) {
        this.db_marcaRechaso = db_marcaRechaso;
    }

    /**
     * @return the fk_idCondonacion
     */
    public int getFk_idCondonacion() {
        return fk_idCondonacion;
    }

    /**
     * @param fk_idCondonacion the fk_idCondonacion to set
     */
    public void setFk_idCondonacion(int fk_idCondonacion) {
        this.fk_idCondonacion = fk_idCondonacion;
    }

    private int id_marcaBanco;
    private int di_Id_fk_idCliente;
    private Date registrado;
    private boolean db_marcaRechaso;
    private int fk_idCondonacion;

    public MarcaBanco() {
    }

    public MarcaBanco(int id_marcaBanco,
            int di_Id_fk_idCliente,
            Date registrado,
            boolean db_marcaRechaso,
            int fk_idCondonacion) {

        this.id_marcaBanco = id_marcaBanco;
        this.di_Id_fk_idCliente = di_Id_fk_idCliente;
        this.registrado = registrado;
        this.db_marcaRechaso = db_marcaRechaso;
        this.fk_idCondonacion = fk_idCondonacion;

    }

}
