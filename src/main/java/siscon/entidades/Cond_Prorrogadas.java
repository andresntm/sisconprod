/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

//import java.sql.Timestamp;
import config.MvcConfig;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.implementaciones.Cond_ProrrogadasImplements;
import siscon.entidades.interfaces.Cond_ProrrogadasInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Cond_Prorrogadas implements RowMapper<Cond_Prorrogadas> {

    /**
     * @return the Prorrogador
     */
    public String getProrrogador() {
        return Prorrogador;
    }

    /**
     * @param Prorrogador the Prorrogador to set
     */
    public void setProrrogador(String Prorrogador) {
        this.Prorrogador = Prorrogador;
    }

    /**
     * @return the FecCompromiso
     */
    public Date getFecCompromiso() {
        return FecCompromiso;
    }

    /**
     * @param FecCompromiso the FecCompromiso to set
     */
    public void setFecCompromiso(Date FecCompromiso) {
        this.FecCompromiso = FecCompromiso;
    }

    /**
     * @return the FecGestEjecutivo
     */
    public Date getFecGestEjecutivo() {
        return FecGestEjecutivo;
    }

    /**
     * @param FecGestEjecutivo the FecGestEjecutivo to set
     */
    public void setFecGestEjecutivo(Date FecGestEjecutivo) {
        this.FecGestEjecutivo = FecGestEjecutivo;
    }

    /**
     * @return the DiasProrrogados
     */
    public int getDiasProrrogados() {
        return DiasProrrogados;
    }

    /**
     * @param DiasProrrogados the DiasProrrogados to set
     */
    public void setDiasProrrogados(int DiasProrrogados) {
        this.DiasProrrogados = DiasProrrogados;
    }

    /**
     * @return the tR
     */
    public Tipo_Prorroga gettR() {
        return tR;
    }

    /**
     * @param tR the tR to set
     */
    public void settR(Tipo_Prorroga tR) {
        this.tR = tR;
        setFk_idTipoProrroga(tR.getId_TipoProrroga());
    }

    /**
     * @return the id_Prorroga
     */
    public int getId_Prorroga() {
        return id_Prorroga;
    }

    /**
     * @param id_Prorroga the id_Prorroga to set
     */
    public void setId_Prorroga(int id_Prorroga) {
        this.id_Prorroga = id_Prorroga;
    }

    /**
     * @return the fk_idCondonacion
     */
    public int getFk_idCondonacion() {
        return fk_idCondonacion;
    }

    /**
     * @param fk_idCondonacion the fk_idCondonacion to set
     */
    public void setFk_idCondonacion(int fk_idCondonacion) {
        this.fk_idCondonacion = fk_idCondonacion;
    }

    /**
     * @return the fk_idColabProrrogador
     */
    public int getFk_idColabProrrogador() {
        return fk_idColabProrrogador;
    }

    /**
     * @param fk_idColabProrrogador the fk_idColabProrrogador to set
     */
    public void setFk_idColabProrrogador(int fk_idColabProrrogador) {
        this.fk_idColabProrrogador = fk_idColabProrrogador;
    }

    /**
     * @return the fk_idTipProrroga
     */
    public int getFk_idTipoProrroga() {
        return fk_idTipProrroga;
    }

    /**
     * @param fk_idTipProrroga the fk_idTipProrroga to set
     */
    public void setFk_idTipoProrroga(int fk_idTipProrroga) {
        this.fk_idTipProrroga = fk_idTipProrroga;
    }

    /**
     * @return the Detalle
     */
    public String getDetalle() {
        return Detalle;
    }

    /**
     * @param Detalle the Detalle to set
     */
    public void setDetalle(String Detalle) {
        this.Detalle = Detalle;
    }

    /**
     * @return the fecIngreso
     */
    public Date getFecIngreso() {
        return fecIngreso;
    }

    /**
     * @param fecIngreso the fecIngreso to set
     */
    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    private int id_Prorroga;
    private int fk_idCondonacion;
    private int fk_idColabProrrogador;
    private int fk_idTipProrroga;
    private String Detalle;
    private Date fecIngreso;
    private Tipo_Prorroga tR;
    private MvcConfig conex;
    private Cond_ProrrogadasInterfaz cPI;
    private Date FecGestEjecutivo;
    private int DiasProrrogados;
    private Date FecCompromiso;
    private String GestionEje;
    private String Prorrogador;

    private void inicializa() {
        try {
            conex = new MvcConfig();
            cPI = new Cond_ProrrogadasImplements(conex.getDataSource());
            settR(new Tipo_Prorroga());
        } catch (SQLException ex) {
            SisCorelog("Error al cargar la entidad Cond_Prorrogadas");
        }
    }

    public Cond_Prorrogadas() {
        inicializa();
    }

    public Cond_Prorrogadas(int id_Prorroga, int fk_idCondonacion, int fk_idColabProrrogador, int fk_idTipProrroga, String Detalle, Date fecIngreso, Date FecGestEjecutivo, int DiasProrrogados) {
        this.id_Prorroga = id_Prorroga;
        this.fk_idCondonacion = fk_idCondonacion;
        this.fk_idColabProrrogador = fk_idColabProrrogador;
        this.fk_idTipProrroga = fk_idTipProrroga;
        this.Detalle = Detalle;
        this.fecIngreso = fecIngreso;
        this.FecGestEjecutivo = FecGestEjecutivo;
        this.DiasProrrogados = DiasProrrogados;
        inicializa();
    }

    public Cond_Prorrogadas mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cond_Prorrogadas cR = new Cond_Prorrogadas();

        cR.setId_Prorroga(rs.getInt("id_Prorroga"));
        cR.setFk_idCondonacion(rs.getInt("fk_idCondonacion"));
        cR.setFk_idColabProrrogador(rs.getInt("fk_idColabProrrogador"));
        cR.setFk_idTipoProrroga(rs.getInt("fk_idTipoProrroga"));
        cR.setDetalle(rs.getString("Detalle"));
        cR.setFecIngreso(rs.getDate("fecIngreso"));
        cR.setFecGestEjecutivo(rs.getDate("FecGestEjecutivo"));
        cR.setDiasProrrogados(rs.getInt("DiasProrrogados"));
        cR.setFecCompromiso(rs.getDate("FecCompromiso"));
        cR.setGestionEje(rs.getString("GestionEje"));
        return cR;
    }

    public Cond_Prorrogadas insert() {
        int id = 0;
        try {
            id = cPI.insertProrroga(this);

            this.setId_Prorroga(id);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden grabar los datos de la prorroga.", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al insertar Error:" + ex.getMessage() + ", datos:" + this.toString());
            id = 0;
        }
        return this;
    }

    public boolean update() {
        boolean up = false;
        try {

            up = cPI.updateProrroga(this);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden actualizar los datos de la prorroga.", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al actualizar Error:" + ex.getMessage() + ", datos:" + this.toString());
            up = false;
        }

        return up;
    }

    public Cond_Prorrogadas get(final int id_Prorroga) {
        Cond_Prorrogadas cR = new Cond_Prorrogadas();

        try {

            cR = cPI.getProrroga(id_Prorroga);

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden extraer el registro de la base datos", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al traer registro Error:" + ex.getMessage() + ", datos:" + this.toString());
            cR = null;
        }

        return cR;
    }

    public List<Cond_Prorrogadas> listAll() {
        List<Cond_Prorrogadas> lCR = new ArrayList<Cond_Prorrogadas>();

        try {

            lCR = cPI.listProrrogas();

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puedenen listar todos los registros de la prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            lCR = null;
        }

        return lCR;
    }

    public List<Cond_Prorrogadas> list_X_Colaborador(final int fk_idColabProrrogador) {
        List<Cond_Prorrogadas> lCR = new ArrayList<Cond_Prorrogadas>();

        try {

            lCR = cPI.listProrrogas_X_Colaborador(fk_idColabProrrogador);

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden listar todos los registros de la prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            lCR = null;
        }

        return lCR;
    }

    public void get_X_Condonacion(final int fk_idCondonacion) {
        Cond_Prorrogadas cP = new Cond_Prorrogadas();

        try {

            cP = cPI.getProrroga_X_Condonacion(fk_idCondonacion);

            this.id_Prorroga = cP.id_Prorroga;
            this.fk_idCondonacion = cP.fk_idCondonacion;
            this.fk_idColabProrrogador = cP.fk_idColabProrrogador;
            this.fk_idTipProrroga = cP.fk_idTipProrroga;
            this.Detalle = cP.Detalle;
            this.fecIngreso = cP.fecIngreso;
            this.FecGestEjecutivo = cP.FecGestEjecutivo;
            this.DiasProrrogados = cP.DiasProrrogados;
            this.GestionEje = cP.GestionEje;
            this.FecCompromiso = cP.FecCompromiso;
            this.tR.get(fk_idTipProrroga);
            this.Prorrogador = cPI.getAlias_Prorrogador(fk_idCondonacion);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden listar todos los registros de la prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            cP = null;
        }

    }

    public boolean delete() {
        boolean up = false;
        try {

            up = cPI.deleteProrroga(this.getId_Prorroga());
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puede eliminar el registro de la prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al eliminar Error:" + ex.getMessage() + ", datos:" + this.toString());
            up = false;
        }

        return up;
    }

    /**
     * @return the GestionEje
     */
    public String getGestionEje() {
        return GestionEje;
    }

    /**
     * @param GestionEje the GestionEje to set
     */
    public void setGestionEje(String GestionEje) {
        this.GestionEje = GestionEje;
    }

    public boolean ingresaGesEje() {
        boolean ok = false;

        try {
            ok = cPI.grabaGestionEjeProrroga(this);
        } catch (Exception ex) {
            ok = false;
        }
        return ok;
    }

}
