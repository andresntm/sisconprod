/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

//import java.sql.Timestamp;
import config.MvcConfig;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.implementaciones.Cond_RechazadasImplements;
import siscon.entidades.interfaces.Cond_RechazadasInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Cond_Rechazadas implements RowMapper<Cond_Rechazadas> {

    /**
     * @return the tR
     */
    public Tipo_Rechazo gettR() {
        return tR;
    }

    /**
     * @param tR the tR to set
     */
    public void settR(Tipo_Rechazo tR) {
        this.tR = tR;
        setFk_idTipRechazo(tR.getId_TipRechazo());
    }

    /**
     * @return the id_Rechazo
     */
    public int getId_Rechazo() {
        return id_Rechazo;
    }

    /**
     * @param id_Rechazo the id_Rechazo to set
     */
    public void setId_Rechazo(int id_Rechazo) {
        this.id_Rechazo = id_Rechazo;
    }

    /**
     * @return the fk_idCondonacion
     */
    public int getFk_idCondonacion() {
        return fk_idCondonacion;
    }

    /**
     * @param fk_idCondonacion the fk_idCondonacion to set
     */
    public void setFk_idCondonacion(int fk_idCondonacion) {
        this.fk_idCondonacion = fk_idCondonacion;
    }

    /**
     * @return the fk_idColaborador
     */
    public int getFk_idColaborador() {
        return fk_idColaborador;
    }

    /**
     * @param fk_idColaborador the fk_idColaborador to set
     */
    public void setFk_idColaborador(int fk_idColaborador) {
        this.fk_idColaborador = fk_idColaborador;
    }

    /**
     * @return the fk_idTipRechazo
     */
    public int getFk_idTipRechazo() {
        return fk_idTipRechazo;
    }

    /**
     * @param fk_idTipRechazo the fk_idTipRechazo to set
     */
    public void setFk_idTipRechazo(int fk_idTipRechazo) {
        this.fk_idTipRechazo = fk_idTipRechazo;
    }

    /**
     * @return the Detalle
     */
    public String getDetalle() {
        return Detalle;
    }

    /**
     * @param Detalle the Detalle to set
     */
    public void setDetalle(String Detalle) {
        this.Detalle = Detalle;
    }

    /**
     * @return the fecIngreso
     */
    public Date getFecIngreso() {
        return fecIngreso;
    }

    /**
     * @param fecIngreso the fecIngreso to set
     */
    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    private int id_Rechazo;
    private int fk_idCondonacion;
    private int fk_idColaborador;
    private int fk_idTipRechazo;
    private String Detalle;
    private Date fecIngreso;
    private Tipo_Rechazo tR;
    private MvcConfig conex;
    private Cond_RechazadasInterfaz cRI;

    private void inicializa() {
        try {
            conex = new MvcConfig();
            cRI = new Cond_RechazadasImplements(conex.getDataSource());
            settR(new Tipo_Rechazo());
        } catch (SQLException ex) {
            SisCorelog("Error al cargar la entidad Cond_Rechazadas");
        }
    }

    public Cond_Rechazadas() {
        inicializa();
    }

    public Cond_Rechazadas(int id_Rechazo, int fk_idCondonacion, int fk_idColaborador, int fk_idTipRechazo, String Detalle, Date fecIngreso) {
        this.id_Rechazo = id_Rechazo;
        this.fk_idCondonacion = fk_idCondonacion;
        this.fk_idColaborador = fk_idColaborador;
        this.fk_idTipRechazo = fk_idTipRechazo;
        this.Detalle = Detalle;
        this.fecIngreso = fecIngreso;

        inicializa();
    }

    public Cond_Rechazadas mapRow(ResultSet rs, int rowNum) throws SQLException {
        Cond_Rechazadas cR = new Cond_Rechazadas();

        cR.setId_Rechazo(rs.getInt("fk_idTipRechazo"));
        cR.setFk_idCondonacion(rs.getInt("fk_idCondonacion"));
        cR.setFk_idColaborador(rs.getInt("fk_idColaborador"));
        cR.setFk_idTipRechazo(rs.getInt("fk_idTipRechazo"));
        cR.setFecIngreso(rs.getDate("fecIngreso"));
        cR.setDetalle(rs.getString("Detalle"));

        return cR;
    }

    public Cond_Rechazadas insert() {
        int id = 0;
        try {
            id = cRI.insertRechazo(this);

            this.setId_Rechazo(id);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden grabar los datos del rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al insertar Error:" + ex.getMessage() + ", datos:" + this.toString());
            id = 0;
        }
        return this;
    }
    public int insertPorcentajes(int id_condonacion,int id_porcetajes) {
        int id = 0;
        try {
            id = cRI.insertPorcentajeRechazo(id_condonacion,id_porcetajes);

            //this.setId_Rechazo(id);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden grabar los datos del rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al insertar Error:" + ex.getMessage() + ", datos:" + this.toString());
            id = 0;
        }
        return id;
    }
    public boolean update() {
        boolean up = false;
        try {

            up = cRI.updateRechazo(this);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden actualizar los datos del rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al actualizar Error:" + ex.getMessage() + ", datos:" + this.toString());
            up = false;
        }

        return up;
    }

    public Cond_Rechazadas get(final int id_TipRechazo) {
        Cond_Rechazadas cR = new Cond_Rechazadas();

        try {

            cR = cRI.getRechazo(id_TipRechazo);

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden extraer el registro de la base datos", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al traer registro Error:" + ex.getMessage() + ", datos:" + this.toString());
            cR = null;
        }

        return cR;
    }

    public List<Cond_Rechazadas> listAll() {
        List<Cond_Rechazadas> lCR = new ArrayList<Cond_Rechazadas>();

        try {

            lCR = cRI.listRechazos();

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puedenen listar todos los registros de tipo de rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            lCR = null;
        }

        return lCR;
    }

    public List<Cond_Rechazadas> list_X_Colaborador(final int fk_idColaborador) {
        List<Cond_Rechazadas> lCR = new ArrayList<Cond_Rechazadas>();

        try {

            lCR = cRI.listRechazos_X_Colaborador(fk_idColaborador);

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puedenen listar todos los registros de tipo de rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            lCR = null;
        }

        return lCR;
    }

    public boolean delete() {
        boolean up = false;
        try {

            up = cRI.deleteRechazo(this.getId_Rechazo());
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puede eliminar el registro del rechazo", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al eliminar Error:" + ex.getMessage() + ", datos:" + this.toString());
            up = false;
        }

        return up;
    }

}
