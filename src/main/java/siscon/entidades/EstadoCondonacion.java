/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.implementaciones.EstadoCondonacionImplement;
import siscon.entidades.interfaces.EstadoCondonacionInterfaz;

/**
 *
 * @author excosoc
 */
public class EstadoCondonacion implements RowMapper<EstadoCondonacion> {

    /**
     * @return the cS
     */
    public CondonacionSiscon getcS() {
        return cS;
    }

    /**
     * @param cS the cS to set
     */
    public void setcS(CondonacionSiscon cS) {
        this.cS = cS;
    }

    /**
     * @return the est
     */
    public Estado getEst() {
        return est;
    }

    /**
     * @param est the est to set
     */
    public void setEst(Estado est) {
        this.est = est;
    }

    /**
     * @return the di_idEstadoCondonacion
     */
    public int getDi_idEstadoCondonacion() {
        return di_idEstadoCondonacion;
    }

    /**
     * @param di_idEstadoCondonacion the di_idEstadoCondonacion to set
     */
    public void setDi_idEstadoCondonacion(int di_idEstadoCondonacion) {
        this.di_idEstadoCondonacion = di_idEstadoCondonacion;
    }

    /**
     * @return the di_fk_IdEstado
     */
    public int getDi_fk_IdEstado() {
        return di_fk_IdEstado;
    }

    /**
     * @param di_fk_IdEstado the di_fk_IdEstado to set
     */
    public void setDi_fk_IdEstado(int di_fk_IdEstado) {
        this.di_fk_IdEstado = di_fk_IdEstado;
    }

    /**
     * @return the di_fk_IdCondonacion
     */
    public int getDi_fk_IdCondonacion() {
        return di_fk_IdCondonacion;
    }

    /**
     * @param di_fk_IdCondonacion the di_fk_IdCondonacion to set
     */
    public void setDi_fk_IdCondonacion(int di_fk_IdCondonacion) {
        this.di_fk_IdCondonacion = di_fk_IdCondonacion;
    }

    /**
     * @return the ddt_fechaReistro
     */
    public Timestamp getDdt_fechaReistro() {
        return ddt_fechaReistro;
    }

    /**
     * @param ddt_fechaReistro the ddt_fechaReistro to set
     */
    public void setDdt_fechaReistro(Timestamp ddt_fechaReistro) {
        this.ddt_fechaReistro = ddt_fechaReistro;
    }

    private int di_idEstadoCondonacion;
    private int di_fk_IdEstado;
    private int di_fk_IdCondonacion;
    private Timestamp ddt_fechaReistro;
    private Estado est;
    private CondonacionSiscon cS;
    private EstadoCondonacionInterfaz eCIntz;

    public EstadoCondonacion() {
        inicializa();
    }

    public EstadoCondonacion(int di_idEstadoCondonacion, int di_fk_IdEstado, int di_fk_IdCondonacion, Timestamp ddt_fechaReistro) {
        this.di_idEstadoCondonacion = di_idEstadoCondonacion;
        this.di_fk_IdEstado = di_fk_IdEstado;
        this.di_fk_IdCondonacion = di_fk_IdCondonacion;
        this.ddt_fechaReistro = ddt_fechaReistro;
        inicializa();
    }

    @Override
    public EstadoCondonacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        this.di_idEstadoCondonacion = rs.getInt("di_idEstadoCondonacion");
        this.di_fk_IdEstado = rs.getInt("di_fk_IdEstado");
        this.di_fk_IdCondonacion = rs.getInt("di_fk_IdCondonacion");
        this.ddt_fechaReistro = rs.getTimestamp("ddt_fechaReistro");

        //Carga objetos heredados
        this.getcS().setId_condonacion(this.di_fk_IdCondonacion);
        this.getcS().getCondonacion();

        this.getEst().setDi_id_Estado(this.di_fk_IdEstado);
        this.getEst().getEstado_X_IdEstado();
        return this;
    }

    private void inicializa() {
        eCIntz = new EstadoCondonacionImplement();
        this.setcS(new CondonacionSiscon());
        this.setEst(new Estado());
    }

    public void getEstadoCondonacion() {
        EstadoCondonacion eC = new EstadoCondonacion();

        eC = eCIntz.getEstadoCondonacion(this.di_idEstadoCondonacion);

        this.di_idEstadoCondonacion = eC.getDi_idEstadoCondonacion();
        this.di_fk_IdEstado = eC.getDi_fk_IdEstado();
        this.di_fk_IdCondonacion = eC.getDi_fk_IdCondonacion();
        this.ddt_fechaReistro = eC.getDdt_fechaReistro();
        this.cS = eC.getcS();
        this.est = eC.getEst();
    }

    public List<EstadoCondonacion> listEstCond() {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        lEC = eCIntz.listEstadoCondonacion();

        return lEC;
    }

    public List<EstadoCondonacion> listEstCond_X_IdCondonacion(final int id_Condonacion) {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        lEC = eCIntz.listEstadoCondonacion_X_IdCondonacion(id_Condonacion);

        return lEC;
    }

    public List<EstadoCondonacion> listEstCond_X_IdEstado(final int id_estado) {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        lEC = eCIntz.listEstadoCondonacion_X_IdEstado(id_estado);

        return lEC;
    }

}
