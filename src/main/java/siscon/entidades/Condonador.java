/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author esilvestre
 */
public class Condonador {

    private int id_usr_modif_int;
    private int id_usr_modif_hon;
    private int id_usr_modif_cap;
    public List<Regla> _reglaList;
    float _atribucionMaxima;
    public Colaborador _colaborador;
    Perfil _perfil;
    Regla _reglas;
    int PorcentajeActualHonorario;
    int PorcentajeActualCapital;
    int PorcentajeActual;
    float f_PorcentajeActualInt;
    float f_PorcentajeActualHon;
    float f_PorcentajeActualCap;
    float InterezActual;

    public float getF_PorcentajeActualInt() {
        return f_PorcentajeActualInt;
    }

    public void setF_PorcentajeActualInt(float f_PorcentajeActualInt) {
        this.f_PorcentajeActualInt = f_PorcentajeActualInt;
    }

    public float getF_PorcentajeActualHon() {
        return f_PorcentajeActualHon;
    }

    public void setF_PorcentajeActualHon(float f_PorcentajeActualHon) {
        this.f_PorcentajeActualHon = f_PorcentajeActualHon;
    }

    public float getF_PorcentajeActualCap() {
        return f_PorcentajeActualCap;
    }

    // valores float para los Procentajes
    public void setF_PorcentajeActualCap(float f_PorcentajeActualCap) {
        this.f_PorcentajeActualCap = f_PorcentajeActualCap;
    }

    public float getInterezActual() {
        return InterezActual;
    }

    public void setInterezActual(float InterezActual) {
        this.InterezActual = InterezActual;
    }

    public Perfil getPerfil() {
        return _perfil;
    }

    public void setPerfil(Perfil _perfil) {
        this._perfil = _perfil;
    }

    public Regla getReglas() {
        return _reglas;
    }

    public void setReglas(Regla _reglas) {
        this._reglas = _reglas;
    }

    public float getAtribucionMaxima() {
        return _atribucionMaxima;
    }

    public void setAtribucionMaxima(float _atribucionMaxima) {
        this._atribucionMaxima = _atribucionMaxima;
    }

    public Colaborador getColaborador() {
        return _colaborador;
    }

    public void setColaborador(Colaborador _colaborador) {
        this._colaborador = _colaborador;
    }

    public Condonador(Perfil _perfil, Regla _reglas, float _atribucionMaxima, Colaborador _colaborador) {
        this._perfil = _perfil;
        this._reglas = _reglas;
        this._atribucionMaxima = _atribucionMaxima;
        this._colaborador = _colaborador;
        this._atribucionMaxima = 0;
    }

    public Condonador() {
        _perfil = new Perfil();
        _reglas = new Regla();
        _colaborador = new Colaborador();
        _reglaList = new ArrayList<Regla>();
        this._atribucionMaxima = 0;
        this.id_usr_modif_int = 0;
    }

    public List<Regla> getReglaList() {
        return _reglaList;
    }

    public void setReglaList(List<Regla> _reglaList) {
        this._reglaList = _reglaList;
    }

    public int getPorcentajeActual() {
        return PorcentajeActual;
    }

    public void setPorcentajeActual(int PorcentajeActual) {
        this.PorcentajeActual = PorcentajeActual;
    }

    public int getPorcentajeActualCapital() {
        return PorcentajeActualCapital;
    }

    public void setPorcentajeActualCapital(int PorcentajeActualCapital) {
        this.PorcentajeActualCapital = PorcentajeActualCapital;
    }

    public int getPorcentajeActualHonorario() {
        return PorcentajeActualHonorario;
    }

    public void setPorcentajeActualHonorario(int PorcentajeActualHonorario) {
        this.PorcentajeActualHonorario = PorcentajeActualHonorario;
    }

    public int getId_usr_modif_int() {
        return id_usr_modif_int;
    }

    public void setId_usr_modif_int(int id_usr_modif) {
        this.id_usr_modif_int = id_usr_modif;
    }
    public int getId_usr_modif_hon() {
        return id_usr_modif_hon;
    }

    public void setId_usr_modif_hon(int id_usr_modif_hon) {
        this.id_usr_modif_hon = id_usr_modif_hon;
    }

    public int getId_usr_modif_cap() {
        return id_usr_modif_cap;
    }

    public void setId_usr_modif_cap(int id_usr_modif_cap) {
        this.id_usr_modif_cap = id_usr_modif_cap;
    }
}
