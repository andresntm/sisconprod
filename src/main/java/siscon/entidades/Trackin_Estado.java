/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.implementaciones.Trackin_EstadoImplements;
import siscon.entidades.interfaces.Trackin_EstadoInterfaz;

/**
 *
 * @author excosoc
 */
public class Trackin_Estado implements RowMapper<Trackin_Estado> {

    /**
     * @return the ejeOrigen
     */
    public usuario getEjeOrigen() {
        return ejeOrigen;
    }

    /**
     * @param ejeOrigen the ejeOrigen to set
     */
    public void setEjeOrigen(usuario ejeOrigen) {
        this.ejeOrigen = ejeOrigen;
    }

    /**
     * @return the userOrigen
     */
    public usuario getUserOrigen() {
        return userOrigen;
    }

    /**
     * @param userOrigen the userOrigen to set
     */
    public void setUserOrigen(usuario userOrigen) {
        this.userOrigen = userOrigen;
    }

    /**
     * @return the id_trakinestado
     */
    public int getId_trakinestado() {
        return id_trakinestado;
    }

    /**
     * @param id_trakinestado the id_trakinestado to set
     */
    public void setId_trakinestado(int id_trakinestado) {
        this.id_trakinestado = id_trakinestado;
    }

    /**
     * @return the dv_estadoInicio
     */
    public String getDv_estadoInicio() {
        return dv_estadoInicio;
    }

    /**
     * @param dv_estadoInicio the dv_estadoInicio to set
     */
    public void setDv_estadoInicio(String dv_estadoInicio) {
        this.dv_estadoInicio = dv_estadoInicio;
    }

    /**
     * @return the dv_estadoFin
     */
    public String getDv_estadoFin() {
        return dv_estadoFin;
    }

    /**
     * @param dv_estadoFin the dv_estadoFin to set
     */
    public void setDv_estadoFin(String dv_estadoFin) {
        this.dv_estadoFin = dv_estadoFin;
    }

    /**
     * @return the registrado
     */
    public Timestamp getRegistrado() {
        return registrado;
    }

    /**
     * @param registrado the registrado to set
     */
    public void setRegistrado(Timestamp registrado) {
        this.registrado = registrado;
    }

    /**
     * @return the di_fk_idEstadoCondonacion
     */
    public int getDi_fk_idEstadoCondonacion() {
        return di_fk_idEstadoCondonacion;
    }

    /**
     * @param di_fk_idEstadoCondonacion the di_fk_idEstadoCondonacion to set
     */
    public void setDi_fk_idEstadoCondonacion(int di_fk_idEstadoCondonacion) {
        this.di_fk_idEstadoCondonacion = di_fk_idEstadoCondonacion;
    }

    /**
     * @return the di_fk_ColaboradorOrigen
     */
    public int getDi_fk_ColaboradorOrigen() {
        return di_fk_ColaboradorOrigen;
    }

    /**
     * @param di_fk_ColaboradorOrigen the di_fk_ColaboradorOrigen to set
     */
    public void setDi_fk_ColaboradorOrigen(int di_fk_ColaboradorOrigen) {
        this.di_fk_ColaboradorOrigen = di_fk_ColaboradorOrigen;
    }

    /**
     * @return the di_fK_ColaboradorDestino
     */
    public int getDi_fK_ColaboradorDestino() {
        return di_fK_ColaboradorDestino;
    }

    /**
     * @param di_fK_ColaboradorDestino the di_fK_ColaboradorDestino to set
     */
    public void setDi_fK_ColaboradorDestino(int di_fK_ColaboradorDestino) {
        this.di_fK_ColaboradorDestino = di_fK_ColaboradorDestino;
    }

    /**
     * @return the dv_ModuloOrigen
     */
    public String getDv_ModuloOrigen() {
        return dv_ModuloOrigen;
    }

    /**
     * @param dv_ModuloOrigen the dv_ModuloOrigen to set
     */
    public void setDv_ModuloOrigen(String dv_ModuloOrigen) {
        this.dv_ModuloOrigen = dv_ModuloOrigen;
    }

    /**
     * @return the dv_ModuloDestino
     */
    public String getDv_ModuloDestino() {
        return dv_ModuloDestino;
    }

    /**
     * @param dv_ModuloDestino the dv_ModuloDestino to set
     */
    public void setDv_ModuloDestino(String dv_ModuloDestino) {
        this.dv_ModuloDestino = dv_ModuloDestino;
    }

    /**
     * @return the dv_CunetaOrigen
     */
    public String getDv_CunetaOrigen() {
        return dv_CunetaOrigen;
    }

    /**
     * @param dv_CunetaOrigen the dv_CunetaOrigen to set
     */
    public void setDv_CunetaOrigen(String dv_CunetaOrigen) {
        this.dv_CunetaOrigen = dv_CunetaOrigen;
    }

    /**
     * @return the dv_CuentaDestino
     */
    public String getDv_CuentaDestino() {
        return dv_CuentaDestino;
    }

    /**
     * @param dv_CuentaDestino the dv_CuentaDestino to set
     */
    public void setDv_CuentaDestino(String dv_CuentaDestino) {
        this.dv_CuentaDestino = dv_CuentaDestino;
    }
    //Atributos de Objecto
    private int id_trakinestado;
    private String dv_estadoInicio;
    private String dv_estadoFin;
    private Timestamp registrado;
    private int di_fk_idEstadoCondonacion;
    private int di_fk_ColaboradorOrigen;
    private int di_fK_ColaboradorDestino;
    private String dv_ModuloOrigen;
    private String dv_ModuloDestino;
    private String dv_CunetaOrigen;
    private String dv_CuentaDestino;
    private usuario userOrigen;
    private usuario ejeOrigen;
    // Variables de clase
    private MvcConfig conex;
    private Trackin_EstadoInterfaz tEI;
    private EstadoCondonacion eC;

    private void InitVariables() {
        try {
            conex = new MvcConfig();
            tEI = new Trackin_EstadoImplements(conex.getDataSource());
            eC = new EstadoCondonacion();
        } catch (SQLException ex) {

        }
    }

    public Trackin_Estado() {
        InitVariables();
    }

    public Trackin_Estado(int id_trakinestado,
            String dv_estadoInicio,
            String dv_estadoFin,
            Timestamp registrado,
            int di_fk_idEstadoCondonacion,
            int di_fk_ColaboradorOrigen,
            int di_fK_ColaboradorDestino,
            String dv_ModuloOrigen,
            String dv_ModuloDestino,
            String dv_CunetaOrigen,
            String dv_CuentaDestino
    ) {
        InitVariables();
        this.id_trakinestado = id_trakinestado;
        this.dv_estadoInicio = dv_estadoInicio;
        this.dv_estadoFin = dv_estadoFin;
        this.registrado = registrado;
        this.di_fk_idEstadoCondonacion = di_fk_idEstadoCondonacion;
        this.di_fk_ColaboradorOrigen = di_fk_ColaboradorOrigen;
        this.di_fK_ColaboradorDestino = di_fK_ColaboradorDestino;
        this.dv_ModuloOrigen = dv_ModuloOrigen;
        this.dv_ModuloDestino = dv_ModuloDestino;
        this.dv_CunetaOrigen = dv_CunetaOrigen;
        this.dv_CuentaDestino = dv_CuentaDestino;

    }

    public Trackin_Estado mapRow(ResultSet rs, int rowNum) throws SQLException {
        Trackin_Estado tE = new Trackin_Estado();
        tE.setId_trakinestado(rs.getInt("id_trakinestado"));
        tE.setDv_estadoInicio(rs.getString("dv_estadoInicio"));
        tE.setDv_estadoFin(rs.getString("dv_estadoFin"));
        tE.setRegistrado(rs.getTimestamp("registrado"));
        tE.setDi_fk_idEstadoCondonacion(rs.getInt("di_fk_idEstadoCondonacion"));
        tE.setDi_fk_ColaboradorOrigen(rs.getInt("di_fk_ColaboradorOrigen"));
        tE.setDi_fK_ColaboradorDestino(rs.getInt("di_fK_ColaboradorDestino"));
        tE.setDv_ModuloOrigen(rs.getString("dv_ModuloOrigen"));
        tE.setDv_ModuloDestino(rs.getString("dv_ModuloDestino"));
        tE.setDv_CunetaOrigen(rs.getString("dv_CunetaOrigen"));
        tE.setDv_CuentaDestino(rs.getString("dv_CuentaDestino"));

        this.eC.setDi_idEstadoCondonacion(this.di_fk_idEstadoCondonacion);
        this.eC.getEstadoCondonacion();
        return tE;
    }

    public Trackin_Estado insertTrackin_Estado() {
        int Id;

        try {

            if (this.di_fK_ColaboradorDestino != 0) {
                Id = tEI.insertTrackin_Estado(this);
            } else {
                Id = tEI.insertTrackin_Estado_sinDestino(this);
            }
        } catch (Exception ex) {
            Id = 0;
        }

        this.setId_trakinestado(Id);

        return this;
    }

    public Trackin_Estado getTrackin_Estado(int id) {
        Trackin_Estado tE = new Trackin_Estado();

        try {

            tE = tEI.getTrackin_EstadoXId(id);

        } catch (Exception ex) {
            tE = new Trackin_Estado();
        }
        return tE;
    }

    public List<Trackin_Estado> listTrackin_Estado() {
        List<Trackin_Estado> lTE = new ArrayList<Trackin_Estado>();

        try {

            lTE = tEI.listTrackin_Estados();

        } catch (Exception ex) {
            lTE = new ArrayList<Trackin_Estado>();
        }

        return lTE;
    }

    public List<Trackin_Estado> listTrackin_Estado_X_IdCond(int id_condoncion) {
        List<Trackin_Estado> lTE = new ArrayList<Trackin_Estado>();

        try {

            lTE = tEI.getTrackin_EstadoXIdCondonacion(id_condoncion);

        } catch (Exception ex) {
            lTE = new ArrayList<Trackin_Estado>();
        }

        return lTE;
    }

    public boolean deleteTrackin_Estado() {
        boolean del = false;

        try {

            del = tEI.DeleteTrackin_Estado(this);

        } catch (Exception ex) {
            del = false;
        }

        return del;
    }

    public boolean updateTrackin_Estado() {
        boolean upd = false;

        try {
            upd = tEI.updateTrackin_Estado(this);
        } catch (Exception ex) {
            upd = false;
        }

        return upd;
    }

    public void ejecutivoDestino(int id_condonacion) {
        ejeOrigen = new usuario();
        try {

            if (id_condonacion != 0) {
                ejeOrigen = tEI.ejecutivoDestinoXCond(id_condonacion);
                /*Si regresa al Ejecutivo se agrega el ejecutivo que curso la condonacion*/
                this.setDi_fK_ColaboradorDestino(ejeOrigen.getId_colaborador());
                this.setDv_CuentaDestino(ejeOrigen.getAlias());

            } else if (id_condonacion == 0) {
                /*En caso contrario quiere decir que el ejecutivo esta recien 
            enviando la condonación a aprobación por ende va en null el campo*/
                ejeOrigen = null;
            }
        } catch (Exception ex) {
            ejeOrigen = null;
        }

    }

    public void actualizaUsuarioAprob() {

        try {
            tEI.updateUsuarioAproTrackin_Estado(this);
        } catch (Exception ex) {

        }

    }

    public void buscaTrackin_Pendiente(int id_condonacion) {

        Trackin_Estado tE = new Trackin_Estado();

        try {

            tE = tEI.getTrackin_EstadoXcond_Actual(id_condonacion);

            this.setDi_fk_ColaboradorOrigen(tE.getDi_fk_ColaboradorOrigen());
            this.setDi_fk_idEstadoCondonacion(tE.getDi_fk_idEstadoCondonacion());
            this.setDv_CunetaOrigen(tE.getDv_CunetaOrigen());
            this.setDv_ModuloDestino(tE.getDv_ModuloDestino());
            this.setDv_ModuloOrigen(tE.getDv_ModuloOrigen());
            this.setDv_estadoInicio(tE.getDv_estadoInicio());
            this.setDv_estadoFin(tE.dv_estadoFin);
            this.setId_trakinestado(tE.id_trakinestado);
            this.setRegistrado(tE.getRegistrado());

        } catch (Exception ex) {
            tE = new Trackin_Estado();
        }

    }

    public void usuarioOrigen(String alias) {
        usuario user = new usuario();

        try {
            user = tEI.usuarioOrigen(alias);

            this.setUserOrigen(user);
            this.setDi_fk_ColaboradorOrigen(user.getId_colaborador());
            this.setDv_CunetaOrigen(user.getAlias());
        } catch (Exception ex) {
            user = null;
        }

    }

    public void usuarioDestino(int idcondonacion) {
        Trackin_Estado tE = new Trackin_Estado();

        try {
            tE = tEI.getUltimoTrackin_EstadoXcond(idcondonacion);

            this.setDi_fK_ColaboradorDestino(tE.getDi_fk_ColaboradorOrigen());
            this.setDv_CuentaDestino(tE.getDv_CunetaOrigen());
        } catch (Exception ex) {
            tE = null;
        }

    }

}
