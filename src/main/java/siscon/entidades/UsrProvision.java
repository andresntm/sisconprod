/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.implementaciones.ProvisionJDBC;

/**
 *
 * @author exesilr
 */
public class UsrProvision implements RowMapper<UsrProvision> {

    public UsrProvision(int id, int fk_idColaborador, String rut_cliente, int nfk_idCondonacion, String registrado, String dv_desc, float monto, int fk_idTipoProvision, boolean vigente, int vigentex) {
        this.id = id;
        this.fk_idColaborador = fk_idColaborador;
        this.rut_cliente = rut_cliente;
        this.nfk_idCondonacion = nfk_idCondonacion;
        this.registrado = registrado;
        this.dv_desc = dv_desc;
        this.monto = monto;
        this.fk_idTipoProvision = fk_idTipoProvision;
        this.vigente = vigente;
        this.vigentex = vigentex;
    }

    public UsrProvision() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFk_idColaborador() {
        return fk_idColaborador;
    }

    public void setFk_idColaborador(int fk_idColaborador) {
        this.fk_idColaborador = fk_idColaborador;
    }

    public String getRut_cliente() {
        return rut_cliente;
    }

    public void setRut_cliente(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    public int getNfk_idCondonacion() {
        return nfk_idCondonacion;
    }

    public void setNfk_idCondonacion(int nfk_idCondonacion) {
        this.nfk_idCondonacion = nfk_idCondonacion;
    }

    public String getRegistrado() {
        return registrado;
    }

    public void setRegistrado(String registrado) {
        this.registrado = registrado;
    }

    public String getDv_desc() {
        return dv_desc;
    }

    public void setDv_desc(String dv_desc) {
        this.dv_desc = dv_desc;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public int getFk_idTipoProvision() {
        return fk_idTipoProvision;
    }

    public void setFk_idTipoProvision(int fk_idTipoProvision) {
        this.fk_idTipoProvision = fk_idTipoProvision;
    }

    public boolean isVigente() {
        return vigente;
    }

    public void setVigente(boolean vigente) {
        this.vigente = vigente;
    }

    public int getVigentex() {
        return vigentex;
    }

    public void setVigentex(int vigentex) {
        this.vigentex = vigentex;
    }

    private int id;
    private int fk_idColaborador;
    private String rut_cliente;
    private int nfk_idCondonacion;
    private String registrado;
    private Timestamp registradoDate;
    private String dv_desc;
    private float monto;
    private int fk_idTipoProvision;
    private boolean vigente;
    private int vigentex;
    private ProvisionJDBC pJDBC;
    private MvcConfig mvcCon;

    public String getTdesc() {
        return tdesc;
    }

    public void setTdesc(String tdesc) {
        this.tdesc = tdesc;
    }
    private String tdesc;
    // String filian;

    public UsrProvision mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrProvision uProvMapper = new UsrProvision();

        uProvMapper.id = rs.getInt("id");
        uProvMapper.fk_idColaborador = rs.getInt("fk_idColaborador");
        uProvMapper.rut_cliente = rs.getString("rut_cliente");
        uProvMapper.nfk_idCondonacion = rs.getInt("nfk_idCondonacion");
        uProvMapper.registradoDate = rs.getTimestamp("registrado");
        uProvMapper.dv_desc = rs.getString("dv_desc");
        uProvMapper.monto = rs.getFloat("monto");
        uProvMapper.fk_idTipoProvision = rs.getInt("fk_idTipoProvision");
        uProvMapper.vigente = rs.getBoolean("vigente");

        return uProvMapper;
    }

    public UsrProvision getProvision_X_Id(int id_prov) {
        UsrProvision uProv = null;
        try {
            mvcCon = new MvcConfig();
            pJDBC = new ProvisionJDBC(mvcCon.getDataSource());
        } catch (SQLException ex) {
            Logger.getLogger(UsrProvision.class.getName()).log(Level.SEVERE, null, ex);
        }

        uProv = pJDBC.selectProvision_X_Id_Provision(id_prov);

        return uProv;
    }
}
