/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Tipo_Prorroga;
import siscon.entidades.interfaces.Tipo_ProrrogaInterfaz;

/**
 *
 * @author excosoc
 */
public class Tipo_ProrrogaImplements implements Tipo_ProrrogaInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public Tipo_ProrrogaImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public Tipo_ProrrogaImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public int insertTipo_Prorroga(final Tipo_Prorroga tR) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO dbo.Tipo_Prorroga\n"
                + "(\n"
                + "    --id_TipoProrroga - this column value is auto-generated\n"
                + "    Detalle\n"
                + "    --fecIngreso - this column value is auto-generated\n"
                + "    --bit_Activo - this column value is auto-generated\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "    -- id_TipoProrroga - int\n"
                + "    ?, -- Detalle - varchar\n"
                + "    -- fecIngreso - datetime\n"
                + "    -- bit_Activo - bit\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"id_TipoProrroga"});

                    ps.setString(1, tR.getDetalle());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateTipo_Prorroga(final Tipo_Prorroga tR) {
        String SQL = "UPDATE dbo.Tipo_Prorroga\n"
                + "SET\n"
                + "    --id_TipoProrroga - this column value is auto-generated\n"
                + "    Detalle = ? -- varchar\n"
                + "    -- fecIngreso - this column value is auto-generated\n"
                + "    -- bit_Activo - this column value not modify\n"
                + "WHERE id_TipoProrroga = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tR.getDetalle(),
                tR.getId_TipoProrroga()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Tipo_Prorroga getTipo_Prorroga(final int id_TipoProrroga) {
        Tipo_Prorroga tR = new Tipo_Prorroga();

        final String sql = "SELECT tr.id_TipoProrroga\n"
                + "    ,tr.Detalle\n"
                + "    ,tr.fecIngreso\n"
                + "    ,tr.bit_Activo \n"
                + "FROM dbo.Tipo_Prorroga tr\n"
                + "WHERE tr.id_TipoProrroga = ?;";
        try {
            tR = jdbcTemplateObject.queryForObject(sql, new Object[]{id_TipoProrroga}, new Tipo_Prorroga());

        } catch (DataAccessException ex) {
            tR = null;
        }

        return tR;

    }

    public List<Tipo_Prorroga> listTipo_Prorrogas() {
        List<Tipo_Prorroga> lTR = new ArrayList<Tipo_Prorroga>();

        final String sql = "SELECT tr.id_TipoProrroga\n"
                + "    ,tr.Detalle\n"
                + "    ,tr.fecIngreso\n"
                + "    ,tr.bit_Activo \n"
                + "FROM dbo.Tipo_Prorroga tr;";

        try {
            lTR = jdbcTemplateObject.query(sql, new Tipo_Prorroga());
        } catch (DataAccessException ex) {
            lTR = null;
        }

        return lTR;
    }

    public boolean activa_InactivaTipo_Prorroga(final Tipo_Prorroga tR) {
        String SQL = "UPDATE dbo.Tipo_Prorroga\n"
                + "SET\n"
                + "    --id_TipoProrroga - this column value is auto-generated\n"
                + "    --Detalle - this column value not modify in this process\n"
                + "    -- fecIngreso - this column value is auto-generated\n"
                + "    bit_Activo = ? -- this column value not modify in this process\n"
                + "WHERE id_TipoProrroga = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tR.isBit_Activo(),
                tR.getId_TipoProrroga()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

}
