/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import siscon.entidades.CondonacionSiscon;
import siscon.entidades.interfaces.CondonacionSisconInterfaz;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author excosoc
 */
public class CondonacionSisconImplement implements CondonacionSisconInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public CondonacionSisconImplement() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public CondonacionSisconImplement(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public CondonacionSiscon getCondonacion(int id_condonacion) {
        CondonacionSiscon cS = new CondonacionSiscon();

        final String sql = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion\n"
                + "WHERE id_condonacion = ?;";
        try {
            cS = jdbcTemplateObject.queryForObject(sql, new Object[]{id_condonacion}, new CondonacionSiscon());

        } catch (DataAccessException ex) {
            cS = null;
        }

        return cS;
    }

    public List<CondonacionSiscon> listCondonacion() {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

    public List<CondonacionSiscon> listCondonacion_X_idColadorador(int idColadorador) {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion\n"
                + "WHERE di_fk_idColadorador = ?\n";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new Object[]{idColadorador}, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

    public List<CondonacionSiscon> listCondonacion_X_IdCliente(int idCliente) {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion"
                + "WHERE di_fk_IdCliente = ?\n";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new Object[]{idCliente}, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

    public List<CondonacionSiscon> listCondonacion_X_IdTipoCondonacion(int idTipoCondonacion) {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion"
                + "WHERE di_fk_tipocondonacion = ?\n";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new Object[]{idTipoCondonacion}, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

    public List<CondonacionSiscon> listCondonacion_X_IdEstado(int idEstado) {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion"
                + "WHERE id_estado = ?\n";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new Object[]{idEstado}, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

    public List<CondonacionSiscon> listCondonacion_X_IdRegla(int idRegla) {
        List<CondonacionSiscon> lCondSis = new ArrayList<CondonacionSiscon>();

        final String SQL = "SELECT id_condonacion\n"
                + ",timestap\n"
                + ",id_regla\n"
                + ",id_estado\n"
                + ",comentario_resna\n"
                + ",monto_total_condonado\n"
                + ",monto_total_recibit\n"
                + ",di_num_opers\n"
                + ",monto_total_capital\n"
                + ",di_fk_tipocondonacion\n"
                + ",di_fk_idColadorador\n"
                + ",di_fk_IdCliente\n"
                + ",monto_condonado_capital\n"
                + ",monto_condonado_recibe\n"
                + ",monto_honorario_capital\n"
                + ",monto_honorario_capitalRecibe\n"
                //                + ",monto_condonado_honorario\n"
                //                + ",monto_VDE_SGN\n"
                + "FROM condonacion"
                + "WHERE id_regla = ?\n";
        try {
            lCondSis = jdbcTemplateObject.query(SQL, new Object[]{idRegla}, new CondonacionSiscon());
        } catch (DataAccessException ex) {
            lCondSis = null;
        }

        return lCondSis;
    }

}
