/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Rel_perfil_regla;
import siscon.entidades.interfaces.Rel_perfil_reglaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Rel_perfil_reglaImpl implements Rel_perfil_reglaInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public Rel_perfil_reglaImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertRel_perfil_regla(final Rel_perfil_regla rel_perfil_regla) {
        int id;

        final String SQL = "INSERT INTO [dbo].[rel_perfil_regla]\n"
                + "           ([id_perfil]\n"
                + "           ,[id_regla])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_rangof"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, rel_perfil_regla.getId_perfil());
                    ps.setInt(2, rel_perfil_regla.getId_regla());
                    return ps;
                }
            },
                    keyHolder);

            id = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id = 0;

        }

        return id;
    }

    public boolean updateRel_perfil_regla(final Rel_perfil_regla rel_perfil_regla) {
        final String SQL = "UPDATE [dbo].[rel_perfil_regla]\n"
                + "   SET [id_perfil] = ?\n"
                + "      ,[id_regla] = ?\n"
                + " WHERE id_perfil_regla = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                rel_perfil_regla.getId_perfil(),
                rel_perfil_regla.getId_regla(),
                rel_perfil_regla.getId_perfil_regla(),});
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Rel_perfil_regla getRel_perfil_reglaXId(int id_regla) {
        Rel_perfil_regla relPerReg = new Rel_perfil_regla();
        final String SQL = "SELECT * FROM rel_perfil_regla WHERE id_regla = ?";

        try {
            relPerReg = jdbcTemplate.queryForObject(SQL, new Object[]{id_regla}, new RowMapper<Rel_perfil_regla>() {
                public Rel_perfil_regla mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Rel_perfil_regla relPerReg = new Rel_perfil_regla();

                    relPerReg.setId_perfil_regla(rs.getInt("id_perfil_regla"));
                    relPerReg.setId_perfil(rs.getInt("id_perfil"));
                    relPerReg.setId_regla(rs.getInt("id_regla"));
                    return relPerReg;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return relPerReg;

    }

    public List<Rel_perfil_regla> getRel_perfil_regla() {
        List<Rel_perfil_regla> lRelPerReg = new ArrayList<Rel_perfil_regla>();
        final String SQL = "SELECT * FROM rel_perfil_regla";

        try {
            lRelPerReg = jdbcTemplate.query(SQL, new RowMapper<Rel_perfil_regla>() {
                public Rel_perfil_regla mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Rel_perfil_regla relPerReg = new Rel_perfil_regla();

                    relPerReg.setId_perfil_regla(rs.getInt("id_perfil_regla"));
                    relPerReg.setId_perfil(rs.getInt("id_perfil"));
                    relPerReg.setId_regla(rs.getInt("id_regla"));
                    return relPerReg;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return lRelPerReg;

    }
    
    public List<Rel_perfil_regla> getPerRegXId(int id_campania) {
        List<Rel_perfil_regla> lPerReg = new ArrayList<Rel_perfil_regla>();

        final String SQL = "SELECT			perReg.* \n"
                + "FROM			campania camp\n"
                + "				INNER JOIN		rel_campana_rel_perfil_regla campPerReg \n"
                + "				ON				campPerReg.id_campana =  camp.id_campania\n"
                + "				INNER JOIN		rel_perfil_regla perReg \n"
                + "				ON				perReg.id_perfil_regla = campPerReg.id_rel_perfil_regla\n"
                + "WHERE			camp.id_campania = ?";

        try {
            lPerReg = jdbcTemplate.query(SQL, new Object[]{id_campania}, new RowMapper<Rel_perfil_regla>() {
                public Rel_perfil_regla mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Rel_perfil_regla perReg = new Rel_perfil_regla();

                    perReg.setId_perfil_regla(rs.getInt("id_perfil_regla"));
                    perReg.setId_perfil(rs.getInt("id_perfil"));
                    perReg.setId_regla(rs.getInt("id_regla"));
                    return perReg;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return lPerReg;
    }
    
}
