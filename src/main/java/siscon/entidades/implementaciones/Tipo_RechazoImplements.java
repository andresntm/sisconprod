/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Tipo_Rechazo;
import siscon.entidades.interfaces.Tipo_RechazoInterfaz;

/**
 *
 * @author excosoc
 */
public class Tipo_RechazoImplements implements Tipo_RechazoInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public Tipo_RechazoImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public Tipo_RechazoImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            Messagebox.show("ERROR de inicializaicion Tipo_RechazoImplements line: 57");
                    //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public int insertTipo_Rechazo(final Tipo_Rechazo tR) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO dbo.Tipo_Rechazo\n"
                + "(\n"
                + "    --id_TipRechazo - this column value is auto-generated\n"
                + "    Detalle\n"
                + "    --fecIngreso - this column value is auto-generated\n"
                + "    --bit_Activo - this column value is auto-generated\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "    -- id_TipRechazo - int\n"
                + "    ?, -- Detalle - varchar\n"
                + "    -- fecIngreso - datetime\n"
                + "    -- bit_Activo - bit\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"id_TipRechazo"});

                    ps.setString(1, tR.getDetalle());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateTipo_Rechazo(final Tipo_Rechazo tR) {
        String SQL = "UPDATE dbo.Tipo_Rechazo\n"
                + "SET\n"
                + "    --id_TipRechazo - this column value is auto-generated\n"
                + "    Detalle = ? -- varchar\n"
                + "    -- fecIngreso - this column value is auto-generated\n"
                + "    -- bit_Activo - this column value not modify\n"
                + "WHERE id_TipRechazo = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tR.getDetalle(),
                tR.getId_TipRechazo()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Tipo_Rechazo getTipo_Rechazo(final int id_TipRechazo) {
        Tipo_Rechazo tR = new Tipo_Rechazo();

        final String sql = "SELECT tr.id_TipRechazo\n"
                + "    ,tr.Detalle\n"
                + "    ,tr.fecIngreso\n"
                + "    ,tr.bit_Activo \n"
                + "FROM dbo.Tipo_Rechazo tr\n"
                + "WHERE tr.id_TipRechazo = ?;";
        try {
            tR = jdbcTemplateObject.queryForObject(sql, new Object[]{id_TipRechazo}, new Tipo_Rechazo());

        } catch (DataAccessException ex) {
            tR = null;
        }

        return tR;

    }

    public List<Tipo_Rechazo> listTipo_Rechazos() {
        List<Tipo_Rechazo> lTR = new ArrayList<Tipo_Rechazo>();

        final String sql = "SELECT tr.id_TipRechazo\n"
                + "    ,tr.Detalle\n"
                + "    ,tr.fecIngreso\n"
                + "    ,tr.bit_Activo \n"
                + "FROM dbo.Tipo_Rechazo tr;";

        try {
            lTR = jdbcTemplateObject.query(sql, new Tipo_Rechazo());
        } catch (DataAccessException ex) {
            lTR = null;
        }

        return lTR;
    }

    public boolean activa_InactivaTipo_Rechazo(final Tipo_Rechazo tR) {
        String SQL = "UPDATE dbo.Tipo_Rechazo\n"
                + "SET\n"
                + "    --id_TipRechazo - this column value is auto-generated\n"
                + "    --Detalle - this column value not modify in this process\n"
                + "    -- fecIngreso - this column value is auto-generated\n"
                + "    bit_Activo = ? -- this column value not modify in this process\n"
                + "WHERE id_TipRechazo = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tR.isBit_Activo(),
                tR.getId_TipRechazo()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

}
