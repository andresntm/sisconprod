/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import siscon.entidades.Estado;
import siscon.entidades.interfaces.EstadoInterfaz;

/**
 *
 * @author excosoc
 */
public class EstadoImplement implements EstadoInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public EstadoImplement() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public EstadoImplement(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public Estado getEstado_X_IdEstado(final int id_Estado) {
        Estado est = new Estado();

        final String sql = "SELECT di_id_Estado\n"
                + ",dv_cod_estado\n"
                + ",dv_lugar_estado\n"
                + ",dv_estado\n"
                + "FROM estado\n"
                + "WHERE di_id_Estado = ?;";
        try {
            est = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Estado}, new Estado());

        } catch (DataAccessException ex) {
            est = null;
        }

        return est;
    }

    public Estado getEstado_X_dvCodEstado(String dvCodEstado) {
        Estado est = new Estado();

        final String sql = "SELECT di_id_Estado\n"
                + ",dv_cod_estado\n"
                + ",dv_lugar_estado\n"
                + ",dv_estado\n"
                + "FROM estado\n"
                + "WHERE dv_cod_estado = ?;";
        try {
            est = jdbcTemplateObject.queryForObject(sql, new Object[]{dvCodEstado}, new Estado());

        } catch (DataAccessException ex) {
            est = null;
        }

        return est;
    }

    public List<Estado> listEstado() {
        List<Estado> lEst = new ArrayList<Estado>();

        final String SQL = "SELECT di_id_Estado\n"
                + ",dv_cod_estado\n"
                + ",dv_lugar_estado\n"
                + ",dv_estado\n"
                + "FROM estado\n";
        try {
            lEst = jdbcTemplateObject.query(SQL, new Estado());
        } catch (DataAccessException ex) {
            lEst = null;
        }

        return lEst;
    }

}
