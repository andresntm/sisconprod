/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Perfil;
import siscon.entidades.PerfilBancas;
import siscon.entidades.interfaces.PerfilInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class PerfilImpl implements PerfilInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public PerfilImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertPerfil(final Perfil perfil) {
        int id_perfil;

        final String SQL = "INSERT INTO [dbo].[perfil]\n"
                + "           ([descripcion]\n"
                + "           ,[id_jefe]\n"
                + "           ,[dv_CodPerfil])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_perfil"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, perfil.getDescripcion());
                    ps.setInt(2, perfil.getId_jefe());
                    ps.setString(3, perfil.getCodPerfil());
                    return ps;
                }
            },
                    keyHolder);

            id_perfil = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_perfil = 0;

        }

        return id_perfil;
    }

    public boolean updatePerfil(final Perfil perfil) {
        final String SQL = "UPDATE [dbo].[perfil]\n"
                + "   SET [descripcion] = <descripcion, varchar(50),>\n"
                + "      ,[id_jefe] = ?\n"
                + "      ,[dv_CodPerfil] = ?\n"
                + " WHERE id_perfil = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                perfil.getDescripcion(),
                perfil.getId_jefe(),
                perfil.getCodPerfil(),
                perfil.getId_perfil(),});
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Perfil getPerfilXId(int id_perfil) {
        Perfil perfil = new Perfil();
        
        final String SQL = "SELECT * FROM perfil WHERE id_perfil = ?";
        
        try {
            perfil = jdbcTemplate.queryForObject(SQL, new Object[]{id_perfil}, new RowMapper<Perfil>() {
                public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Perfil rangoFecha = new Perfil();

                    rangoFecha.setId_perfil(rs.getInt("id_perfil"));
                    rangoFecha.setDescripcion(rs.getString("descripcion"));
                    rangoFecha.setId_jefe(rs.getInt("id_jefe"));
                    rangoFecha.setCodPerfil(rs.getString("dv_CodPerfil"));
                    return rangoFecha;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return perfil;
    }

    public List<Perfil> getPerfil() {
        List<Perfil> lPerfil = new ArrayList<Perfil>();
        
        final String SQL = "SELECT * FROM perfil";
        
        try {
            lPerfil = jdbcTemplate.query(SQL, new RowMapper<Perfil>() {
                public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Perfil rangoFecha = new Perfil();

                    rangoFecha.setId_perfil(rs.getInt("id_perfil"));
                    rangoFecha.setDescripcion(rs.getString("descripcion"));
                    rangoFecha.setId_jefe(rs.getInt("id_jefe"));
                    rangoFecha.setCodPerfil(rs.getString("dv_CodPerfil"));
                    return rangoFecha;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
        }
        return lPerfil;
        
    }
    
    
        public List<PerfilBancas> getPerfilXBanca(String perfil) {
        List<PerfilBancas> lPerfil = new ArrayList<PerfilBancas>();
        
        final String SQL = "select t.dv_CodPerfil,t.descripcion,\n" +
                                "stuff( ( select ', ' + cod_banca\n" +
                                "                from  (\n" +
                                "\n" +
                                "SELECT pe.dv_CodPerfil,pe.descripcion,ba.cod_banca\n" +
                                "  FROM perfil as pe \n" +
                                "   inner join  perfil_bojBanca peo on peo.fk_id_perfil=pe.id_perfil\n" +
                                "   inner join banca ba on ba.id = peo.fkIdBanca\n" +
                                "   ) as hh\n" +
                                "                where dv_CodPerfil = t.dv_CodPerfil\n" +
                                "                order by cod_banca\n" +
                                "                for xml path('') ), 1, 1, '') AS bancas \n" +
                                "\n" +
                                " from  (\n" +
                                "SELECT pe.dv_CodPerfil,pe.descripcion,ba.cod_banca\n" +
                                "  FROM perfil as pe \n" +
                                "   inner join  perfil_bojBanca peo on peo.fk_id_perfil=pe.id_perfil\n" +
                                "   inner join banca ba on ba.id = peo.fkIdBanca\n" +
                                "   ) as t\n" +
                                "    where t.dv_CodPerfil='"+perfil+"'\n" +
                                "   group by t.dv_CodPerfil,t.descripcion\n" +
                                "   ";
        
        try {
            lPerfil = jdbcTemplate.query(SQL, new RowMapper<PerfilBancas>() {
                public PerfilBancas mapRow(ResultSet rs, int rowNum) throws SQLException {
                    PerfilBancas peb = new PerfilBancas();

                    peb.setCodPerfil(rs.getString("dv_CodPerfil"));
                    peb.setDescripcion(rs.getString("descripcion"));
                    peb.setBancas(rs.getString("bancas"));
                    return peb;
                }
            }); 

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
           Messagebox.show("Error Mostrando Perfiles Ejecutivos x BAnca");
        }
        return lPerfil;
        
    }
            public List<PerfilBancas> getPerfilALLBanca() {
        List<PerfilBancas> lPerfil = new ArrayList<PerfilBancas>();
        
        final String SQL = "select t.dv_CodPerfil,t.descripcion,\n" +
                                "stuff( ( select ', ' + cod_banca\n" +
                                "                from  (\n" +
                                "\n" +
                                "SELECT pe.dv_CodPerfil,pe.descripcion,ba.cod_banca\n" +
                                "  FROM perfil as pe \n" +
                                "   inner join  perfil_bojBanca peo on peo.fk_id_perfil=pe.id_perfil\n" +
                                "   inner join banca ba on ba.id = peo.fkIdBanca\n" +
                                "   ) as hh\n" +
                                "                where dv_CodPerfil = t.dv_CodPerfil\n" +
                                "                order by cod_banca\n" +
                                "                for xml path('') ), 1, 1, '') AS bancas \n" +
                                "\n" +
                                " from  (\n" +
                                "SELECT pe.dv_CodPerfil,pe.descripcion,ba.cod_banca\n" +
                                "  FROM perfil as pe \n" +
                                "   inner join  perfil_bojBanca peo on peo.fk_id_perfil=pe.id_perfil\n" +
                                "   inner join banca ba on ba.id = peo.fkIdBanca\n" +
                                "   ) as t\n" +
                          
                                "   group by t.dv_CodPerfil,t.descripcion\n" +
                                "   ";
        
        try {
            lPerfil = jdbcTemplate.query(SQL, new RowMapper<PerfilBancas>() {
                public PerfilBancas mapRow(ResultSet rs, int rowNum) throws SQLException {
                    PerfilBancas peb = new PerfilBancas();

                    peb.setCodPerfil(rs.getString("dv_CodPerfil"));
                    peb.setDescripcion(rs.getString("descripcion"));
                    peb.setBancas(rs.getString("bancas"));
                    return peb;
                }
            }); 

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
           Messagebox.show("Error Mostrando Perfiles Ejecutivos ALL Error:["+ex.getMessage()+"] Query : [ "+SQL+"]");
        }
        return lPerfil;
        
    }

}
