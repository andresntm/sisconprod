/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Cond_Prorrogadas;
import siscon.entidades.interfaces.Cond_ProrrogadasInterfaz;
import org.springframework.jdbc.core.PreparedStatementCreator;
import java.sql.PreparedStatement;
import org.springframework.dao.DataAccessException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Trackin_Estado;

/**
 *
 * @author excosoc
 */
public class Cond_ProrrogadasImplements implements Cond_ProrrogadasInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public Cond_ProrrogadasImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public Cond_ProrrogadasImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public int insertProrroga(final Cond_Prorrogadas cR) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO dbo.Cond_Prorrogadas\n"
                + "(\n"
                + "    --id_Prorroga - this column value is auto-generated\n"
                + "    fk_idCondonacion,\n"
                + "    fk_idColabProrrogador,\n"
                + "    fk_idTipoProrroga,\n"
                + "    Detalle\n"
                + "    --fecIngreso - this column value is auto-generated\n"
                + "    --FecGestEjecutivo - columna del ejecutivo\n"
                + "    --DiasProrrogados - columna del ejecutivo\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "    -- id_Prorroga - int\n"
                + "    ?, -- fk_idCondonacion - int\n"
                + "    ?, -- fk_idColabProrrogador - int\n"
                + "    ?, -- fk_idTipoProrroga - int\n"
                + "    ? -- Detalle - varchar\n"
                + "    -- fecIngreso - datetime\n"
                + "    --FecGestEjecutivo - datetime\n"
                + "    --DiasProrrogados - int\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"id_Prorroga"});

//                    ps.setInt(1, cR.getId_Prorroga());
                    ps.setInt(1, cR.getFk_idCondonacion());
                    ps.setInt(2, cR.getFk_idColabProrrogador());
                    ps.setInt(3, cR.getFk_idTipoProrroga());
                    ps.setString(4, cR.getDetalle());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateProrroga(Cond_Prorrogadas cR) {
        String SQL = " update Cond_Prorrogadas\n"
                + " SET\n"
                + "     --id_Prorroga - this column value is auto-generated\n"
                + "     fk_idCondonacion = ?, -- int\n"
                + "     fk_idColabProrrogador = ?, -- int\n"
                + "     fk_idTipoProrroga = ?, -- int\n"
                + "     Detalle = ? -- varchar\n"
                + "     --fecIngreso - this column value is auto-generated\n"
                + "WHERE id_Prorroga = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                cR.getFk_idCondonacion(),
                cR.getFk_idColabProrrogador(),
                cR.getFk_idTipoProrroga(),
                cR.getDetalle(),
                cR.getId_Prorroga()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public boolean grabaGestionEjeProrroga(Cond_Prorrogadas cR) {
        String SQL = "UPDATE Cond_Prorrogadas SET\n"
                + "FecGestEjecutivo = (GETDATE())\n"
                + ",FecCompromiso = ?\n"
                + ",GestionEje = ?\n"
                + "WHERE id_Prorroga = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                cR.getFecCompromiso(),
                cR.getGestionEje(),
                cR.getId_Prorroga()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Cond_Prorrogadas getProrroga(int id_Prorroga) {
        Cond_Prorrogadas cR = new Cond_Prorrogadas();

        final String sql = "SELECT cr.id_Prorroga\n"
                + "     ,cr.fk_idCondonacion\n"
                + "     ,cr.fk_idColabProrrogador\n"
                + "     ,cr.fk_idTipoProrroga\n"
                + "     ,cr.Detalle\n"
                + "     ,cr.fecIngreso\n"
                + "     ,cr.FecGestEjecutivo\n"
                + "     ,cr.DiasProrrogados\n"
                + "     ,cr.FecCompromiso\n"
                + "     ,cr.GestionEje \n"
                + "FROM dbo.Cond_Prorrogadas cr\n"
                + "WHERE cr.id_Prorroga = ?;";
        try {
            cR = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Prorroga}, new Cond_Prorrogadas());

        } catch (DataAccessException ex) {
            cR = null;
        }

        return cR;
    }

    public List<Cond_Prorrogadas> listProrrogas() {
        List<Cond_Prorrogadas> lCP = new ArrayList<Cond_Prorrogadas>();

        final String sql = "SELECT cr.id_Prorroga\n"
                + "     ,cr.fk_idCondonacion\n"
                + "     ,cr.fk_idColabProrrogador\n"
                + "     ,cr.fk_idTipoProrroga\n"
                + "     ,cr.Detalle\n"
                + "     ,cr.fecIngreso\n"
                + "     ,cr.FecGestEjecutivo\n"
                + "     ,cr.DiasProrrogados\n"
                + "     ,cr.FecCompromiso\n"
                + "     ,cr.GestionEje \n"
                + "FROM dbo.Cond_Prorrogadas cr;";

        try {
            lCP = jdbcTemplateObject.query(sql, new Cond_Prorrogadas());
        } catch (DataAccessException ex) {
            lCP = null;
        }

        return lCP;
    }

    public List<Cond_Prorrogadas> listProrrogas_X_Colaborador(int id_colaborador) {
        List<Cond_Prorrogadas> lCP = new ArrayList<Cond_Prorrogadas>();

        final String sql = "SELECT cr.id_Prorroga\n"
                + "     ,cr.fk_idCondonacion\n"
                + "     ,cr.fk_idColabProrrogador\n"
                + "     ,cr.fk_idTipoProrroga\n"
                + "     ,cr.Detalle\n"
                + "     ,cr.fecIngreso\n"
                + "     ,cr.FecGestEjecutivo\n"
                + "     ,cr.DiasProrrogados\n"
                + "     ,cr.FecCompromiso\n"
                + "     ,cr.GestionEje \n"
                + "FROM dbo.Cond_Prorrogadas cr\n"
                + "WHERE cr.fk_idColabProrrogador = ?;";

        try {
            lCP = jdbcTemplateObject.query(sql, new Object[]{id_colaborador}, new Cond_Prorrogadas());
        } catch (DataAccessException ex) {
            lCP = null;
        }

        return lCP;
    }

    public boolean deleteProrroga(int id_Prorroga) {
        try {
            final String sql = "DELETE FROM dbo.Cond_Prorrogadas\n"
                    + "WHERE ID_PRORROGA = ?;";

            jdbcTemplateObject.update(sql, new Object[]{id_Prorroga});

            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Cond_Prorrogadas getProrroga_X_Condonacion(int id_Condonacion) {
        Cond_Prorrogadas cP = new Cond_Prorrogadas();

        final String sql = "SELECT cr.id_Prorroga\n"
                + "     ,cr.fk_idCondonacion\n"
                + "     ,cr.fk_idColabProrrogador\n"
                + "     ,cr.fk_idTipoProrroga\n"
                + "     ,cr.Detalle\n"
                + "     ,cr.fecIngreso\n"
                + "     ,cr.FecGestEjecutivo\n"
                + "     ,cr.DiasProrrogados\n"
                + "     ,cr.FecCompromiso\n"
                + "     ,cr.GestionEje \n"
                + "FROM dbo.Cond_Prorrogadas cr\n"
                + "WHERE cr.fk_idCondonacion = ?;";

        try {
            cP = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Condonacion}, new Cond_Prorrogadas());
        } catch (DataAccessException ex) {
            cP = null;
        }

        return cP;
    }

    public String getAlias_Prorrogador(int id_Condonacion) {
        String cP = null;

        final String sql = "SELECT us.Alias\n"
                + "FROM dbo.Cond_Prorrogadas cr\n"
                + "INNER JOIN dbo.Colaborador cl on cr.fk_idColabProrrogador = cl.id\n"
                + "INNER JOIN dbo.Usuario us on cl.id_usuario = us.id_usuario\n"
                + "WHERE cr.fk_idCondonacion = ?;";

        try {
            cP = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Condonacion}, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    String alias = null;

                    alias = rs.getString("Alias");

                    return alias;
                }
            });
        } catch (DataAccessException ex) {
            cP = null;
        }

        return cP;
    }

    public List<Cond_Prorrogadas> listProrrogas_X_TipoProrroga(int id_TipoProrroga) {
        List<Cond_Prorrogadas> lCP = new ArrayList<Cond_Prorrogadas>();

        final String sql = "SELECT cr.id_Prorroga\n"
                + "     ,cr.fk_idCondonacion\n"
                + "     ,cr.fk_idColabProrrogador\n"
                + "     ,cr.fk_idTipoProrroga\n"
                + "     ,cr.Detalle\n"
                + "     ,cr.fecIngreso\n"
                + "     ,cr.FecGestEjecutivo\n"
                + "     ,cr.DiasProrrogados\n"
                + "     ,cr.FecCompromiso\n"
                + "     ,cr.GestionEje \n"
                + "FROM dbo.Cond_Prorrogadas cr\n"
                + "WHERE cr.fk_idTipoProrroga = ?;";

        try {
            lCP = jdbcTemplateObject.query(sql, new Object[]{id_TipoProrroga}, new Cond_Prorrogadas());
        } catch (DataAccessException ex) {
            lCP = null;
        }

        return lCP;
    }

}
