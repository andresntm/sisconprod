/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.GesSmall;
import siscon.entidades.OfeSmall;
import siscon.entidades.ValorMonto;
import siscon.entidades.interfaces.SmallCoreInterfaz;

/**
 *
 * @author esilves
 */
public class SmallCoreJDBC implements SmallCoreInterfaz {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<GesSmall> gclie = new ArrayList<GesSmall>();
    private List<OfeSmall> _ofertas = new ArrayList<OfeSmall>();
    public ValorMonto SumaMontoTotalGarantias;

    public SmallCoreJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public List<GesSmall> GestionesSmallCore(int rut, String cuenta) {
        //  List<GarantiasCliente> gclie = null;
        Date xx = new Date();
        Map<String, Object> out;
        SqlParameterSource in;
        // DetalleOperacionesCliente.set(0, element)

        //List<Contacto> listContact
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_SmallCore_ges").
                returningResultSet("rs", new ParameterizedRowMapper<GesSmall>() {

                    public GesSmall mapRow(ResultSet rs, int i) throws SQLException {

                        GesSmall sclie = new GesSmall();
                        sclie.setFecha(rs.getString("FECHA"));
                        sclie.setCopa(rs.getString("COPA"));
                        sclie.setAgenda(rs.getString("AGENDA"));
                        sclie.setRespuesta(rs.getString("RESPUESTA"));
                        sclie.setTelefono(rs.getString("TELEFONO"));
                        sclie.setCanal(rs.getString("CANAL"));
                        sclie.setUsuario(rs.getString("USUARIO"));
                        sclie.setDireccion(rs.getString("DIRECCION"));
                        sclie.setComuna(rs.getString("COMUNA"));

                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

                        return sclie;//rs.getString(1);   

                    }
                ;
        });
        
        in = new MapSqlParameterSource().addValue("rut", rut).addValue("col", 5).addValue("usuario", cuenta).addValue("origen", "Ficha Cliente").addValue("eje", "Ficha Cliente").addValue("FECDESDE", xx).addValue("FECHASTA", xx);
        try {
            out = jdbcCall.execute(in);

            gclie = (List<GesSmall>) out.get("rs");

        } catch (DataAccessException ex) {

            Messagebox.show("ERROR SP CALL :[" + ex.getMessage() + "]");
            return gclie;
        }

        //  System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");
        return gclie;

    }

    
    
    
    
     public List<OfeSmall> OfertasSmallCore(int rut, String cuenta) {
        //  List<GarantiasCliente> gclie = null;
        Date xx = new Date();
        Map<String, Object> out;
        SqlParameterSource in;
        // DetalleOperacionesCliente.set(0, element)

        //List<Contacto> listContact
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_SmallCore_ofe").
                returningResultSet("rs", new ParameterizedRowMapper<OfeSmall>() {

                    public OfeSmall mapRow(ResultSet rs, int i) throws SQLException {

                        OfeSmall sclie = new OfeSmall();
                        sclie.setPeriodo(rs.getString("PERIODO"));
                        sclie.setEstado(rs.getString("ESTADO"));
                        sclie.setDescOferta(rs.getString("DETALLE_OFERTA"));
                        sclie.setOferta_valida(rs.getString("OFERTA_VAL"));


                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

                        return sclie;//rs.getString(1);   

                    }
                ;
        });
            String usu = "jliguen";
        in = new MapSqlParameterSource().addValue("fld_rut", rut).addValue("usuario", cuenta);
        try {
            out = jdbcCall.execute(in);

            _ofertas = (List<OfeSmall>) out.get("rs");

        } catch (DataAccessException ex) {

            Messagebox.show("ERROR SP CALL :[" + ex.getMessage() + "]");
            return _ofertas;
        }

        //  System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");
        return _ofertas;

    }   
    
     
     
     
     
          public boolean IsEmptyOferta(int rut, String cuenta) {
        //  List<GarantiasCliente> gclie = null;
        Date xx = new Date();
        Map<String, Object> out;
        SqlParameterSource in;
        // DetalleOperacionesCliente.set(0, element)

        //List<Contacto> listContact
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_SmallCore_ofe").
                returningResultSet("rs", new ParameterizedRowMapper<OfeSmall>() {

                    public OfeSmall mapRow(ResultSet rs, int i) throws SQLException {

                        OfeSmall sclie = new OfeSmall();
                        sclie.setPeriodo(rs.getString("PERIODO"));
                        sclie.setEstado(rs.getString("ESTADO"));
                        sclie.setDescOferta(rs.getString("DETALLE_OFERTA"));
                        sclie.setOferta_valida(rs.getString("OFERTA_VAL"));


                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

                        return sclie;//rs.getString(1);   

                    }
                ;
        });
            String usu = "jliguen";
        in = new MapSqlParameterSource().addValue("fld_rut", rut).addValue("usuario", cuenta);
        try {
            out = jdbcCall.execute(in);

            _ofertas = (List<OfeSmall>) out.get("rs");

        } catch (DataAccessException ex) {

            Messagebox.show("ERROR SP CALL :[" + ex.getMessage() + "]");
            return false;
        }

        //  System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");
        if(_ofertas.size()>0)return true;
                else return false;
//        return _ofertas;

    } 
     
     
}
