/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.RangoMonetario;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.interfaces.RangoMonetarioInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class RangoMonetarioImpl implements RangoMonetarioInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    
    public RangoMonetarioImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }
    
    public int insertRangoMonetario(final RangoMonetario rangoMonetario) {
        int id_rangom;

        final String SQL = "INSERT INTO [dbo].[rango_monto]\n"
                + "           ([desc]\n"
                + "           ,[monto_ini]\n"
                + "           ,[monto_fin]\n"
                + "           ,[id_unidad])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_rangom"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, rangoMonetario.getDesc());
                    ps.setInt(2, rangoMonetario.getMonto_ini());
                    ps.setInt(3, rangoMonetario.getMonto_fin());
                    ps.setInt(4, rangoMonetario.getId_unidad());
                    return ps;
                }
            },
                    keyHolder);

            id_rangom = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_rangom = 0;

        }

        return id_rangom;
    }

    public boolean updateRangoMonetario(final RangoMonetario rangoMonetario) {
        final String SQL = "UPDATE [dbo].[rango_monto]\n"
                + "   SET [desc] = ?\n"
                + "      ,[monto_ini] = ?\n"
                + "      ,[monto_fin] = ?\n"
                + "      ,[id_unidad] = ?\n"
                + " WHERE id_rangom = ?";
        try {
            jdbcTemplate.update(SQL, new Object[]{
                rangoMonetario.getDesc(),
                rangoMonetario.getMonto_ini(),
                rangoMonetario.getMonto_fin(),
                rangoMonetario.getId_unidad(),
                rangoMonetario.getId_rangom()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public RangoMonetario getRangoMonetarioXId(int id_rangom) {
        RangoMonetario rangoMonetario = new RangoMonetario();

        final String SQL = "SELECT * FROM rango_monto WHERE id_rangom = ?";

        try {
            rangoMonetario = jdbcTemplate.queryForObject(SQL, new Object[]{id_rangom}, new RowMapper<RangoMonetario>() {
                public RangoMonetario mapRow(ResultSet rs, int rowNum) throws SQLException {
                    RangoMonetario rangoMonetario = new RangoMonetario();

                    rangoMonetario.setId_rangom(rs.getInt("id_rangom"));
                    rangoMonetario.setDesc(rs.getString("[desc]"));
                    rangoMonetario.setMonto_ini(rs.getInt("monto_ini"));
                    rangoMonetario.setMonto_fin(rs.getInt("monto_fin"));
                    rangoMonetario.setId_unidad(rs.getInt("id_unidad"));
                    return rangoMonetario;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
        }
        return rangoMonetario;
    }

    public List<RangoMonetario> getRangoMonetario() {
        List<RangoMonetario> lRangoMonetario = new ArrayList<RangoMonetario>();

        final String SQL = "SELECT * FROM rango_monto";

        try {
            lRangoMonetario = jdbcTemplate.query(SQL, new RowMapper<RangoMonetario>() {
                public RangoMonetario mapRow(ResultSet rs, int rowNum) throws SQLException {
                    RangoMonetario rangoMonetario = new RangoMonetario();

                    rangoMonetario.setId_rangom(rs.getInt("id_rangom"));
                    rangoMonetario.setDesc(rs.getString("desc"));
                    rangoMonetario.setMonto_ini(rs.getInt("monto_ini"));
                    rangoMonetario.setMonto_fin(rs.getInt("monto_fin"));
                    rangoMonetario.setId_unidad(rs.getInt("id_unidad"));
                    return rangoMonetario;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
        }
        return lRangoMonetario;
    }

}
