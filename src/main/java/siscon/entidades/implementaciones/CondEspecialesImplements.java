/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.TipoCondEspecial;
import siscon.entidades.interfaces.CondEspecialesInterfaz;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.Session.TrackingSesion;

/**
 *
 * @author excosoc
 */
public class CondEspecialesImplements implements CondEspecialesInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private MvcConfig conex;

    public CondEspecialesImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        } catch (SQLException SQLex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, SQLex.getMessage(), SQLex);
        }
    }

    public int insertCondEspecial(final CondonacionesEspeciales condEsp) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO COND_ESPECIALES\n"
                + "(\n"
                + "--ID_COND_ESP\n"
                + "ID_TIPOESPECIAL\n"
                + ",ID_CONDONACION\n"
                + "--,FECHAINGRESO\n"
                + ",NUMCUOTAS\n"
                + ",ID_PROVISION\n"
                + "--,ACTIVO\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "?\n"
                + ",?\n"
                + ",?\n"
                + "," + (condEsp.getId_Provision() == 0 ? "NULL" : condEsp.getId_Provision()) + "\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID_COND_ESP"});

                    ps.setInt(1, condEsp.getId_TipoEspecial());
                    ps.setInt(2, condEsp.getId_Condonacion());
                    ps.setInt(3, condEsp.getNumCuotas());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            return 0;
        }
    }

    public int insertTipCondEspecial(final TipoCondEspecial tipCondEsp) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO TIPO_COND_ESPECIALES\n"
                + "(\n"
                + "--ID_CONDESPECIAL\n"
                + "CODIGO\n"
                + ",DESCRIPCION\n"
                + ",DETALLE\n"
                + "--,FECHAINGRESO\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "?\n"
                + ",?\n"
                + ",?\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID_CONDESPECIAL"});

                    ps.setString(1, tipCondEsp.getCodigo());
                    ps.setString(2, tipCondEsp.getDescripcion());
                    ps.setString(3, tipCondEsp.getDetalle());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            return 0;
        }
    }

    public TipoCondEspecial selectTipCondEspecial_X_Id_TipCondEsp(int id_TipConEsp) {
        TipoCondEspecial tipCondEsp = new TipoCondEspecial();

        final String sql = "SELECT ID_CONDESPECIAL\n"
                + ",CODIGO\n"
                + ",DESCRIPCION\n"
                + ",DETALLE\n"
                + ",FECHAINGRESO\n"
                + "FROM TIPO_COND_ESPECIALES\n"
                + "WHERE ID_CONDESPECIAL = ?";
        try {
            tipCondEsp = jdbcTemplateObject.queryForObject(sql, new Object[]{id_TipConEsp}, new TipoCondEspecial());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            tipCondEsp = null;
        }

        return tipCondEsp;
    }

    public CondonacionesEspeciales selectCondEspecial_X_Id_CondEsp(int id_ConEsp) {
        CondonacionesEspeciales condEsp = new CondonacionesEspeciales();

        final String sql = "SELECT ID_COND_ESP\n"
                + ",ID_TIPOESPECIAL\n"
                + ",ID_CONDONACION\n"
                + ",FECHAINGRESO\n"
                + ",NUMCUOTAS\n"
                + ",ID_PROVISION\n"
                + ",ACTIVO\n"
                + "FROM COND_ESPECIALES\n"
                + "WHERE ID_COND_ESP = ?";
        try {
            condEsp = jdbcTemplateObject.queryForObject(sql, new Object[]{id_ConEsp}, new CondonacionesEspeciales());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            condEsp = null;
        }

        return condEsp;
    }

    public CondonacionesEspeciales selectCondEspecial_X_Id_Condonacion(int id_Condonacion) {
        CondonacionesEspeciales condEsp = new CondonacionesEspeciales();

        final String sql = "SELECT ID_COND_ESP\n"
                + ",ID_TIPOESPECIAL\n"
                + ",ID_CONDONACION\n"
                + ",FECHAINGRESO\n"
                + ",NUMCUOTAS\n"
                + ",ID_PROVISION\n"
                + ",ACTIVO\n"
                + "FROM COND_ESPECIALES\n"
                + "WHERE ID_CONDONACION = ?";
        try {
            condEsp = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Condonacion}, new CondonacionesEspeciales());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            condEsp = null;
        }

        return condEsp;
    }

    public List<CondonacionesEspeciales> listCondEspecial() {
        List<CondonacionesEspeciales> lCondEsp = new ArrayList<CondonacionesEspeciales>();

        final String sql = "SELECT ID_COND_ESP\n"
                + ",ID_TIPOESPECIAL\n"
                + ",ID_CONDONACION\n"
                + ",FECHAINGRESO\n"
                + ",NUMCUOTAS\n"
                + ",ID_PROVISION\n"
                + ",ACTIVO\n"
                + "FROM COND_ESPECIALES\n";
        try {
            lCondEsp = jdbcTemplateObject.query(sql, new CondonacionesEspeciales());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            lCondEsp = null;
        }

        return lCondEsp;
    }

    public List<TipoCondEspecial> listTipCondEspecial() {
        List<TipoCondEspecial> lCondEsp = new ArrayList<TipoCondEspecial>();

        final String sql = "SELECT ID_CONDESPECIAL\n"
                + ",CODIGO\n"
                + ",DESCRIPCION\n"
                + ",DETALLE\n"
                + ",FECHAINGRESO\n"
                + "FROM TIPO_COND_ESPECIALES\n ";
        try {
            lCondEsp = jdbcTemplateObject.query(sql, new TipoCondEspecial());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            lCondEsp = null;
        }

        return lCondEsp;
    }

    public List<CondonacionesEspeciales> listCondEspecial_X_Id_TipCondEsp(int id_TipConEsp) {
        List<CondonacionesEspeciales> lCondEsp = new ArrayList<CondonacionesEspeciales>();

        final String sql = "SELECT ID_COND_ESP\n"
                + ",ID_TIPOESPECIAL\n"
                + ",ID_CONDONACION\n"
                + ",FECHAINGRESO\n"
                + ",NUMCUOTAS\n"
                + ",ID_PROVISION\n"
                + ",ACTIVO\n"
                + "FROM COND_ESPECIALES\n"
                + "WHERE ID_CONDONACION = ?";
        try {
            lCondEsp = jdbcTemplateObject.query(sql, new Object[]{id_TipConEsp}, new CondonacionesEspeciales());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            lCondEsp = null;
        }

        return lCondEsp;
    }

    public List<TipoCondEspecial> listTipCondEspecial_Usadas() {
        List<TipoCondEspecial> lCondEsp = new ArrayList<TipoCondEspecial>();

        final String sql = "SELECT TCS.ID_CONDESPECIAL\n"
                + ",TCS.CODIGO\n"
                + ",TCS.DESCRIPCION\n"
                + ",TCS.DETALLE\n"
                + ",TCS.FECHAINGRESO\n"
                + "FROM TIPO_COND_ESPECIALES TCS\n"
                + "INNER JOIN COND_ESPECIALES CE ON TCS.ID_CONDESPECIAL = CE.ID_TIPOESPECIAL\n";
        try {
            lCondEsp = jdbcTemplateObject.query(sql, new TipoCondEspecial());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            lCondEsp = null;
        }

        return lCondEsp;
    }

    public boolean updateCondEspecial(final CondonacionesEspeciales condEsp) {
        String SQL = "UPDATE COND_ESPECIALES\n"
                + "SET ID_CONDONACION = ?\n"
                + "WHERE ID_COND_ESP = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                condEsp.getId_TipoEspecial(),
                condEsp.getId_Cond_Esp()
            });
            return true;
        } catch (DataAccessException e) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, e.getMessage(), e);
            return false;
        }
    }

    public boolean deleteCondEspecial(CondonacionesEspeciales condEsp) {
        try {
            String SQL = "UPDATE COND_ESPECIALES\n"
                    + "SET ACTIVO = 0\n"
                    + "WHERE ID_COND_ESP = ?;";

            jdbcTemplateObject.update(SQL, new Object[]{
                condEsp.getId_Cond_Esp()
            });

            return true;
        } catch (DataAccessException e) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, e.getMessage(), e);
            return false;
        }
    }

}
