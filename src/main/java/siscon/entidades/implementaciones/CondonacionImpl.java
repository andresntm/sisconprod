/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.ConDetOperMapper;
import siscon.RowMapper.CondonacionCountReparoMapper;
import siscon.RowMapper.CondonacionMapper;
import siscon.RowMapper.CondonacionMapper2;
import siscon.RowMapper.CondonacionMapper3;
import siscon.RowMapper.CondonacionMapperApplyRechazadas;
import siscon.RowMapper.CondonacionMapperApply;
import siscon.RowMapper.CondonacionMapperApplyProrrogadas;
import siscon.RowMapper.CondonacionMapperCambios_de_Estado;
import siscon.RowMapper.CondonacionMapperContab;
import siscon.RowMapper.CondonacionMapperInformes;
import siscon.RowMapper.CondonacionMapperResumenInformes;
import siscon.RowMapper.CondonacionReparosMapper;
import siscon.RowMapper.ReparosMapper;
import siscon.Session.TrackingSesion;
import siscon.entidades.APPResumenCondonacionesInforme;
import siscon.entidades.AccesosEjecutivo;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.BandejaCondonacionesInforme;
import siscon.entidades.ConDetOper;
import siscon.entidades.CondReparadas;
import siscon.entidades.Condonacion;
import siscon.entidades.CondonacionTabla;
import siscon.entidades.CondonacionTabla2;
import siscon.entidades.CondonacionesAPP;
import siscon.entidades.CondonacionesEjecutivos;
import siscon.entidades.DetalleCliente;
import siscon.entidades.GlosaENC;
import siscon.entidades.InformeEfecv2;
import siscon.entidades.MarcaBanco;
import siscon.entidades.Reparos;
import siscon.entidades.ResumenCondonacionesInforme;
import siscon.entidades.SisconinfoperacionV2;
import siscon.entidades.Tabla_Condonacion;
import siscon.entidades.TipoDatoDate;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.interfaces.AdjuntarInterfaz;
import siscon.entidades.interfaces.CondonacionInterfaz;
import siscon.entidades.interfaces.GlosaInterfaz;
import siscon.entidades.siscon_inf_clienteV2;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author exesilr
 */
public class CondonacionImpl implements CondonacionInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<DetalleCliente> ListaOperaciones;
    private JdbcTemplate jdbcTemplateObject;
    private int id_con = 0;
    private int estado_destino = 0;
    private String[] nombs;
    private EstadosJDBC _estados;
    private MvcConfig mmmm = new MvcConfig();
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CondonacionImpl.class);
    private Condonacion CurrentCondonacion;
    private String insertSqlDetalleCondonacion,
            insertSqlEstado_Condonacion,
            insertSqlOperaciones,
            insertSqlTrakingCondonacionOperacion,
            updateSql,
            insertSqlCondonacion_retail;

    {

        insertSqlTrakingCondonacionOperacion = "INSERT INTO dbo.tracking_con_operacion\n"
                + "(\n"
                + "    --id_traking_oper - this column value is auto-generated\n"
                + "    id_trakin_cond,\n"
                + "    tipo_rango,\n"
                + "    varlo_rango,\n"
                + "    monto_original,\n"
                + "    rango_fecha_ini,\n"
                + "    rango_fecha_fin,\n"
                + "    cod_operacion,\n"
                + "    monto_rango_procentaje,\n"
                + "    monto_recibe,\n"
                + "    monto_condonado,\n"
                + "    tipo_monto\n"
                + ")\n"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        insertSqlOperaciones = "INSERT INTO dbo.operacion\n"
                + "(\n"
                + "    --id_operacion - this column value is auto-generated\n"
                + "    tipo_operacion,\n"
                + "    codigo_operacion\n"
                + ")\n"
                + "VALUES (?,?)";
        insertSqlCondonacion_retail = "INSERT INTO dbo.condonacion\n"
                + "(\n"
                + "    --id_condonacion - this column value is auto-generated\n"
                + "    timestap,\n"
                + "    id_regla,\n"
                + "    id_estado,\n"
                + "    comentario_resna,\n"
                + "    monto_total_condonado,\n"
                + "    monto_total_recibit,\n"
                + "    di_num_opers,\n"
                + "    monto_total_capital,\n"
                + "    di_fk_tipocondonacion,\n"
                + "    di_fk_idColadorador,\n"
                + "    di_fk_IdCliente,\n"
                + "    monto_condonado_capital,\n"
                + "    monto_condonado_recibe,\n"
                + "    monto_honorario_capital,\n"
                + "    monto_honorario_capitalRecibe\n"
                + ")"
                + "VALUES\n"
                + "(getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        insertSqlEstado_Condonacion = "INSERT INTO dbo.estado_condonacion\n"
                + "(\n"
                + "    --di_idEstadoCondonacion - this column value is auto-generated\n"
                + "    di_fk_IdEstado,\n"
                + "    di_fk_IdCondonacion,\n"
                + "    ddt_fechaReistro\n"
                + ")\n"
                + "VALUES(?,?,getdate())";
        insertSqlDetalleCondonacion = "INSERT INTO dbo.detalle_condonacion\n"
                + "(\n"
                + "    --id_operacion_cliente - this column value is auto-generated\n"
                + "    registro_tiempo,\n"
                + "    total_capital,\n"
                + "    id_cliente,\n"
                + "    id_operacion,\n"
                + "    estado_operacion,\n"
                + "    id_condonacion,\n"
                + "    capital_condona,\n"
                + "    capital_recibe,\n"
                + "    totoal_interes,\n"
                + "    interes_condona,\n"
                + "    interes_recibe,\n"
                + "    total_honorario,\n"
                + "    honorario_condona,\n"
                + "    honorario_recibe\n"
                + ")\n"
                + "VALUES(getdate(),?,?,?,?,?,?,?,?,?,?,?,?,?)";

    }
    private final GlosaInterfaz gi;
    private final AdjuntarInterfaz ai;
    
     String SQLCOND="";

    /**
     *
     * @return
     */
    public Condonacion getCurrentCondonacion() {
        return CurrentCondonacion;
    }

    public void setCurrentCondonacion(Condonacion CurrentCondonacion) {
        this.CurrentCondonacion = CurrentCondonacion;
    }

    public CondonacionImpl(DataSource dataSource) throws SQLException {

        this.dataSource = dataSource;
        this.gi = new GlosaAPI();//cosorio
        this.ai = new AdjuntarAPI();
        _estados = new EstadosJDBC(mmmm.getDataSource());
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void setDettaleOPeraciones(List<DetalleCliente> detOpers) {

        this.CurrentCondonacion.setListaOperaciones(detOpers);

    }

    /// esta QUery esta mala y OPbsoleta en desuso
    public int consulta_id() {
        // int id=0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select max(id_condonacion) from condonacion";
        int id_condonacion = 0;
        try {
            id_condonacion = jdbcTemplate.queryForInt(sql);

        } catch (DataAccessException ex) {

            System.out.print("####### ---- ERROR de EJECUCION de Query///id condonacion/// ERR:[" + ex.getMessage() + "]----#######");

        }
        System.out.println("#------------@@@@@@@@@@@@@@@@@ Condonacion-Get-id_condonacion [" + id_condonacion + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        return id_condonacion;

    }

    private int traking_id() {
        // int id=0;
        JdbcTemplate jdbcTemplateXX = new JdbcTemplate(dataSource);
        String sql = "select max(id_condonacion) from trackin_condonaciones";
        int id_condonacion_traking = 0;
        try {
            id_condonacion_traking = jdbcTemplateXX.queryForInt(sql);

        } catch (DataAccessException ex) {

            System.out.print("####### ---- ERROR de EJECUCION de Query///id condonacion/// ERR:[" + ex.getMessage() + "]----#######");

        }
        System.out.println("#------------@@@@@@@@@@@@@@@@@ id_condonacion_traking [" + id_condonacion_traking + "]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");

        return id_condonacion_traking;

    }

    /**
     * Retorna el valor totoal condonado.
     *
     * @return Retorna Un True o False dependiendo de del exito o fracaso de
     * guardar la informacion de condonacion.
     * @exception IndexOutOfBoundsException if the <code>offset</code> and
     * <code>count</code> arguments index characters outside the bounds of the
     * <code>value</code> array. When {
     * @paramref a} sin parametros.
     */
    public boolean Guardar() {
        int returnTraking, returnTrakingCondonacion;
        //Informacion al Cliente
        boolean returnGuardar = false;
        boolean isclient = false;
        boolean agregacliente = true;
        final String NombreCliente = this.CurrentCondonacion.getNombreCliente();
        final String RutCliente = this.CurrentCondonacion.getRutCliente();
        final String NombreEjecutivo = this.CurrentCondonacion.getNombreEjecutiva();
        final String Rutejecutivo = this.CurrentCondonacion.getRutCliente();
        final String Rango_Fecha_Inicio = this.CurrentCondonacion.getRangoFechaInicio();
        final String Rango_Fecha_Fin = this.CurrentCondonacion.getRangoFehaFin();
        final String unidad_medida_rango = "meses";
        final java.sql.Date date = getCurrentDatetime();
        //final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date parsed = null;
        final Date d2 = new Date();

        // Preguntar si existe la operacion y el cliente,de no existir agregar a la nueva DB
        isclient = this.isClinete(this.CurrentCondonacion.getRutCliente());
        agregacliente = this.GuardarOperacionesAndCliente();

        if (!agregacliente) {
            Messagebox.show("ERROR CONNACION-GuardandoDetalles-IMPL linea : 156 ");
            return false;
        }

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();

            try {
                jdbcTemplate.update(
                        new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("INSERT INTO trackin_condonaciones (id_condonacion,id_cliente,id_colaborador,nombreCliente,nombre_ejecutivo,rutcliente,rutejecutivo,reg_con,reg_con_inicio,unidad_rango,reg_con_fin,id_regla,rango_fecha,estado_condonacion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new String[]{"id_traking"});
                        // ps.setString(1, person.getName());
                        ps.setInt(1, consulta_id());
                        ps.setInt(2, 1);
                        ps.setInt(3, 1);
                        ps.setString(4, NombreCliente);
                        ps.setString(5, NombreEjecutivo);
                        ps.setString(6, RutCliente);
                        ps.setString(7, Rutejecutivo);
                        ps.setTimestamp(8, new java.sql.Timestamp(d2.getTime()));
                        ps.setInt(9, Integer.parseInt(Rango_Fecha_Inicio));
                        ps.setString(10, unidad_medida_rango);
                        ps.setInt(11, Rango_Fecha_Fin.equals("infinito") ? 999999999 : Integer.parseInt(Rango_Fecha_Fin));
                        ps.setInt(12, 5);
                        ps.setString(13, "0-60 meses");
                        ps.setString(14, "CERRADA");
                        return ps;
                    }
                },
                        keyHolder);
            } catch (DataAccessException ex) {

                SisCorelog(ex.getMessage());
                Messagebox.show("ERROR CONNACION-FuncionGuardar-IMPL linea : 193 [" + ex.getMessage() + "]");
                return false;
                //System.out.print("####### ---- ERROR de EJECUCION de Query///isClienteBci/// ERR:[" + ex.getMessage() + "] NumeroLinea["+NumeroLinea+"]----#######");
            }

            returnTraking = keyHolder.getKey().intValue();
            SisCorelog("returnTraking: [" + returnTraking + "]");

            try {

                for (int j = 0; j < this.CurrentCondonacion.getListaOperaciones().size(); j++) {
                    jdbcTemplate.update(insertSqlTrakingCondonacionOperacion, new Object[]{
                        returnTraking,
                        "RnagoCondonaCapital",
                        "0-20000000", // limiteinferior y superior del rangoocupado
                        this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto(), // Monto a condonar para capital
                        new Date(), // Rango de Fecha inicial Ocupado par la condonacion
                        new Date(), ///Rango Fecha Final Ocupado para la condonacion
                        this.CurrentCondonacion.getListaOperaciones().get(j).getOperacionOriginal(),
                        0.7,
                        this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7,
                        this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7,
                        "Monto Capital"

                    });

                    double interes = this.CurrentCondonacion.getListaOperaciones().get(j).getMora() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto();
                    jdbcTemplate.update(insertSqlTrakingCondonacionOperacion, new Object[]{
                        returnTraking,
                        "RnagoCondonaInteres",
                        "0-20000000", // limiteinferior y superior del rangoocupado
                        interes, // Monto a condonar para Interes
                        new Date(), // Rango de Fecha inicial Ocupado par la condonacion
                        new Date(), ///Rango Fecha Final Ocupado para la condonacion
                        this.CurrentCondonacion.getListaOperaciones().get(j).getOperacionOriginal(),
                        0.7,
                        interes - interes * 0.7,
                        interes * 0.7,
                        "Monto Interes"

                    });

                    double MontoRecibe = this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7;
                    double HonorarioJudicial = MontoRecibe * 0.20;
                    jdbcTemplate.update(insertSqlTrakingCondonacionOperacion, new Object[]{
                        returnTraking,
                        "RnagoCondonaHonorarioJudicial",
                        "0-20000000", // limiteinferior y superior del rangoocupado
                        HonorarioJudicial, // Monto a condonar para Interes
                        new Date(), // Rango de Fecha inicial Ocupado par la condonacion
                        new Date(), ///Rango Fecha Final Ocupado para la condonacion
                        this.CurrentCondonacion.getListaOperaciones().get(j).getOperacionOriginal(),
                        0.7,
                        HonorarioJudicial - HonorarioJudicial * 0.7,
                        HonorarioJudicial * 0.7,
                        "Monto Honorario"

                    });

                }

                returnGuardar = true;

            } catch (DataAccessException ex) {

                SisCorelog(ex.getMessage());
                Messagebox.show("ERROR CONNACION2-FuncionGuardar-IMPL linea : 261 [" + ex.getMessage() + "]");
                return false;

            }

            SisCorelog("returnTraking: [" + returnTraking + "]");

        } catch (DataAccessException ex) {
            Messagebox.show("ERROR CONNACION3-FuncionGuardar-IMPL linea : 269 [" + ex.getMessage() + "]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            returnGuardar = false;
        }
        SisCorelog("CONDONACION: [" + this.CurrentCondonacion.TotoalCondonado.getValorPesos() + "]Nombre Cliente[" + NombreCliente + "]");
        System.out.println("#------------@@@@@@@@@@@@@@@@@AKA TENGO LOS DATOS A GIARDAR DEL OBJETC CONDONACION[" + this.CurrentCondonacion.TotoalCondonado.getValorPesos() + "]Nombre Cliente" + NombreCliente + "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@---------#");
        return returnGuardar;

    }
    public int GuardarCondonacion(final int estado, final int tipoOperacion, final String TipoCon) {

        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();

        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();

        nombs = CurrentCondonacion.getNombreCliente().split(" ");

        final String AppMaterno = StringUtils.isEmpty(nombs[0]) ? nombs[0] : "SIN AppMaterno";
        final String AppPaterno = StringUtils.isEmpty(nombs[1]) ? nombs[1] : "SIN AppPaterno";
        final String Nombres = StringUtils.isEmpty(nombs[2]) ? nombs[2] + (StringUtils.isEmpty(nombs[3]) ? nombs[3] : "") : "SIN Nombres";
        final String Alias = "NO HAY";
        final String rutCliente = this.CurrentCondonacion.getRutCliente();

        KeyHolder id_clientekey = new GeneratedKeyHolder();

        try {
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO cliente\n"
                            + "(apellido_materno,\n"
                            + "apellido_paterno,\n"
                            + "nombre,\n"
                            + "alias,\n"
                            + "rut)\n "
                            + "VALUES\n "
                            + "(?,\n"
                            + "?,\n"
                            + "?,\n"
                            + "?,\n"
                            + "?)", 
                            new String[]{"id_cliente"});
                    ps.setString(1, AppMaterno);
                    ps.setString(2, AppPaterno);
                    ps.setString(3, Nombres);
                    ps.setString(4, Alias);
                    ps.setString(5, rutCliente);

                    return ps;
                }
            },
                    id_clientekey);

        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;

        }

        this.CurrentCondonacion.setIdCliente(id_clientekey.getKey().intValue());

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertSqlCondonacion_retail, new String[]{"id_condonacion"});

                    ps.setInt(1, 1);//regla de condonacion applicaa
                    ps.setInt(2, estado);// estado condonacion
                    ps.setString(3, TipoCon);// tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoToalRecibido); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setInt(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                    // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion linea 271:[" + ex.getMessage() + "]");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);
        KeyHolder keyHolder2 = new GeneratedKeyHolder();

        CurrentCondonacion.getAdjuntar().setFk_idCliente(idCliente);
        CurrentCondonacion.getAdjuntar().setFk_idCondonacion(id_con);

        if (CurrentCondonacion.getAdjuntar().getaDet() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (ai.api(CurrentCondonacion.getAdjuntar(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }

        //graba glosa
        CurrentCondonacion.getGlosa().setId_client(idCliente);
        CurrentCondonacion.getGlosa().setId_cond(id_con);

        if (CurrentCondonacion.getGlosa().getDetalle() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (gi.api(CurrentCondonacion.getGlosa(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }
        /// condonasound insert

        return id_con;
    }

    
    
    
    
    
    
    public int GuardarCondonacion(final String estado, final String tipoOperacion, final String TipoCon) {

        
        
        
       
        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();

        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();

        nombs = CurrentCondonacion.getNombreCliente().split(" ");

        
        
        
        //Aqui vamos ha generar ir a buscar a produccion los datos a la tabla indbc
        
        
        final String AppMaterno = StringUtils.isEmpty(nombs[0]) ? nombs[0] : "SIN AppMaterno";
        final String AppPaterno = StringUtils.isEmpty(nombs[1]) ? nombs[1] : "SIN AppPaterno";
        final String Nombres = StringUtils.isEmpty(nombs[2]) ? nombs[2] + (StringUtils.isEmpty(nombs[3]) ? nombs[3] : "") : "SIN Nombres";
        final String Alias = "NO HAY";
        final String rutCliente = this.CurrentCondonacion.getRutCliente();

        KeyHolder id_clientekey = new GeneratedKeyHolder();

        
        
        
        
        
        
        
        
        try {
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO cliente\n"
                            + "(apellido_materno,\n"
                            + "apellido_paterno,\n"
                            + "nombre,\n"
                            + "alias,\n"
                            + "rut)\n "
                            + "VALUES\n "
                            + "(?,\n"
                            + "?,\n"
                            + "?,\n"
                            + "?,\n"
                            + "?)", 
                            new String[]{"id_cliente"});
                    ps.setString(1, AppMaterno);
                    ps.setString(2, AppPaterno);
                    ps.setString(3, Nombres);
                    ps.setString(4, Alias);
                    ps.setString(5, rutCliente);

                    return ps;
                }
            },
                    id_clientekey);

        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;

        }

        this.CurrentCondonacion.setIdCliente(id_clientekey.getKey().intValue());

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {
            
            
      SQLCOND =          "INSERT INTO [dbo].[condonacion]\n"
                    + "           ([timestap]\n"
                    + "           ,[id_regla]\n"
                    + "           ,[id_estado]\n"
                    + "           ,[comentario_resna]\n"
                    + "           ,[monto_total_condonado]\n"
                    + "           ,[monto_total_recibit]\n"
                    + "           ,[di_num_opers]\n"
                    + "           ,[monto_total_capital]\n"
                    + "           ,[di_fk_tipocondonacion]\n"
                    + "           ,[di_fk_idColadorador]\n"
                    + "           ,[di_fk_IdCliente]\n"
                    + "           ,[monto_condonado_capital]\n"
                    + "           ,[monto_condonado_recibe]\n"
                    + "           ,[monto_honorario_capital]\n"
                    + "           ,[monto_honorario_capitalRecibe])\n"
                    + "     VALUES\n"
                    + "  (getdate()                                                                 \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_id_Estado from estado where dv_cod_estado=?)                   \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_idtipoCondonacion from tipo_condonacion where dv_codtipocon=?) \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + " ,?)                                                                         ";
            
            
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQLCOND, new String[]{"id_condonacion"});

                    ps.setInt(1, 1);//regla de condonacion applicaa
                    ps.setString(2, estado);// estado condonacion
                    ps.setString(3, TipoCon);// tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoToalRecibido); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setString(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                    // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion linea 271:[" + ex.getMessage() + "]");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);
        KeyHolder keyHolder2 = new GeneratedKeyHolder();

 



        return id_con;
    }


    
    
    public int GuardarCondonacion(final int estado, final int tipoOperacion, final String TipoCon, final int rechazo) {

        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();

        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();

        nombs = CurrentCondonacion.getNombreCliente().split(" ");

        final String AppMaterno = StringUtils.isEmpty(nombs[0]) ? nombs[0] : "SIN AppMaterno";
        final String AppPaterno = StringUtils.isEmpty(nombs[1]) ? nombs[1] : "SIN AppPaterno";
        final String Nombres = StringUtils.isEmpty(nombs[2]) ? nombs[2] + (StringUtils.isEmpty(nombs[3]) ? nombs[3] : "") : "SIN Nombres";
        final String Alias = "NO HAY";
        final String rutCliente = this.CurrentCondonacion.getRutCliente();
        int id_cliente_rechazado = 0;

        KeyHolder id_clientekey = new GeneratedKeyHolder();

        try {
             //##ActualizacionData
            if (rechazo == 0) {
                jdbcTemplate.update(
                        new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("INSERT INTO cliente\n"
                                + "(apellido_materno,\n"
                                + "apellido_paterno,\n"
                                + "nombre,\n"
                                + "alias,\n"
                                + "rut)\n "
                                + "VALUES\n "
                                + "(?,\n"
                                + "?,\n"
                                + "?,\n"
                                + "?,\n"
                                + "?)", new String[]{"id_cliente"});
                        ps.setString(1, AppMaterno);
                        ps.setString(2, AppPaterno);
                        ps.setString(3, Nombres);
                        ps.setString(4, Alias);
                        ps.setString(5, rutCliente);

                        return ps;
                    }
                }, id_clientekey);
            } else {

                String SQL = "select id_cliente from cliente where rut = ?";
                id_cliente_rechazado = jdbcTemplate.queryForInt(SQL, new Object[]{rutCliente});

            }
        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;

        }
 //##ActualizacionData
        this.CurrentCondonacion.setIdCliente((rechazo == 0) ? id_clientekey.getKey().intValue() : id_cliente_rechazado);

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertSqlCondonacion_retail, new String[]{"id_condonacion"});

                    ps.setInt(1, 1);//regla de condonacion applicaa
                    ps.setInt(2, estado);// estado condonacion
                    ps.setString(3, TipoCon);// tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoToalRecibido); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setInt(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                    // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion linea 271:[" + ex.getMessage() + "]");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);

        CurrentCondonacion.getAdjuntar().setFk_idCliente(idCliente);
        CurrentCondonacion.getAdjuntar().setFk_idCondonacion(id_con);

        if (CurrentCondonacion.getAdjuntar().getaDet() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (ai.api(CurrentCondonacion.getAdjuntar(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }

        //graba glosa
        CurrentCondonacion.getGlosa().setId_client(idCliente);
        CurrentCondonacion.getGlosa().setId_cond(id_con);

        if (CurrentCondonacion.getGlosa().getDetalle() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (gi.api(CurrentCondonacion.getGlosa(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }
        /// condonasound insert

        return id_con;
    }

    /**
     * GuardarCondonacionPM(final int estado, final int tipoOperacion, final
     * String TipoCon)
     *
     * @param estado
     * @param tipoOperacion
     * @param TipoCon
     * @return
     */
    @SuppressWarnings("empty-statement")
    public int GuardarCondonacionPMV2(final String estado, final String tipoOperacion, final String TipoCon) {
        final String SQLG;
        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();
        final float MontoTotalTotal = (float) this.CurrentCondonacion.getTotalTotal().getValor();
        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float monto_honorario_condonado=(float) this.CurrentCondonacion.getHonorarios_condonados();
        final float monto_Vde_SGN=(float) this.CurrentCondonacion.getMonto_VDE_SGN();
        
        String nombre = CurrentCondonacion.getNombreCliente();
        nombs = nombre.split(" ");

        KeyHolder id_clientekey = new GeneratedKeyHolder();

        try {
            final String AppMaterno = nombs[3].isEmpty() ? nombs[3] : "SIN AppMaterno";
            final String AppPaterno = nombs[2].length() > 0 ? nombs[2] : "SIN AppPaterno";
            final String Nombres = nombs[0].length() > 0 ? nombs[0] + " " + (nombs[1].length() > 0 ? nombs[1] : "") : "SIN Nombres";
            final String Alias = "NO-EXISTE";
            final String rutCliente = this.CurrentCondonacion.getRutCliente();

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("insert into cliente (apellido_materno,apellido_paterno,nombre,alias,rut) VALUES (?,?,?,?,?)", new String[]{"id_cliente"});
                    ps.setString(1, AppMaterno);
                    ps.setString(2, AppPaterno);
                    ps.setString(3, Nombres);
                    ps.setString(4, Alias);
                    ps.setString(5, rutCliente);
                    // ps.setString(6, "NULL");
                    return ps;
                }
            },
                    id_clientekey);

        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;

        }

        this.CurrentCondonacion.setIdCliente(id_clientekey.getKey().intValue());

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {

            SQLG = "INSERT INTO [dbo].[condonacion]\n"
                    + "           ([timestap]\n"
                    + "           ,[id_regla]\n"
                    + "           ,[id_estado]\n"
                    + "           ,[comentario_resna]\n"
                    + "           ,[monto_total_condonado]\n"
                    + "           ,[monto_total_recibit]\n"
                    + "           ,[di_num_opers]\n"
                    + "           ,[monto_total_capital]\n"
                    + "           ,[di_fk_tipocondonacion]\n"
                    + "           ,[di_fk_idColadorador]\n"
                    + "           ,[di_fk_IdCliente]\n"
                    + "           ,[monto_condonado_capital]\n"
                    + "           ,[monto_condonado_recibe]\n"
                    + "           ,[monto_honorario_capital]\n"
                    + "           ,[monto_honorario_capitalRecibe]\n"
                    + "           ,[monto_condonado_honorario]\n"
                     + "          ,[monto_VDE_SGN])\n"
                    + "     VALUES\n"
                    + "  (getdate()                                                                 \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_id_Estado from estado where dv_cod_estado=?)                   \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_idtipoCondonacion from tipo_condonacion where dv_codtipocon=?) \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + " ,?)                                                                         ";

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQLG, new String[]{"id_condonacion"});
                    // ps.setString(1, person.getName());
                    // ps.setInt(1, consulta_id());

                    ps.setInt(1, 1);   //regla de condonacion applicaa
                    ps.setString(2, estado); // estado condonacion
                    ps.setString(3, TipoCon); // // tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoTotalTotal); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setString(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                                 // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    ps.setFloat(15,monto_honorario_condonado);
                    ps.setFloat(16,monto_Vde_SGN);
                    //  ps.execute();
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion  linea 528:[" + ex.getMessage() + "] Estado:[" + estado + "]tipoOperacion :" + tipoOperacion + " ");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);
        KeyHolder keyHolder2 = new GeneratedKeyHolder();

        CurrentCondonacion.getAdjuntar().setFk_idCliente(idCliente);
        CurrentCondonacion.getAdjuntar().setFk_idCondonacion(id_con);

        if (CurrentCondonacion.getAdjuntar().getaDet() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (ai.api(CurrentCondonacion.getAdjuntar(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            };
        };

        //graba glosa
        CurrentCondonacion.getGlosa().setId_client(idCliente);
        CurrentCondonacion.getGlosa().setId_cond(id_con);

        if (CurrentCondonacion.getGlosa().getDetalle() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (gi.api(CurrentCondonacion.getGlosa(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }
        /// condonasound insert

        return id_con;
    }

    
    /**
     * GuardarCondonacionPM(final int estado, final int tipoOperacion, final
     * String TipoCon)
     *
     * @param estado
     * @param tipoOperacion
     * @param TipoCon
     * @return
     */
    @SuppressWarnings("empty-statement")
    public int GuardarCondonacionPM(final String estado, final String tipoOperacion, final String TipoCon) {
        final String SQLG;
        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();
        final float MontoTotalTotal = (float) this.CurrentCondonacion.getTotalTotal().getValor();
        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        String nombre = CurrentCondonacion.getNombreCliente();
        nombs = nombre.split(" ");

        KeyHolder id_clientekey = new GeneratedKeyHolder();

        try {
            final String AppMaterno = nombs[3].isEmpty() ? nombs[3] : "SIN AppMaterno";
            final String AppPaterno = nombs[2].length() > 0 ? nombs[2] : "SIN AppPaterno";
            final String Nombres = nombs[0].length() > 0 ? nombs[0] + " " + (nombs[1].length() > 0 ? nombs[1] : "") : "SIN Nombres";
            final String Alias = "NO-EXISTE";
            final String rutCliente = this.CurrentCondonacion.getRutCliente();

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("insert into cliente (apellido_materno,apellido_paterno,nombre,alias,rut) VALUES (?,?,?,?,?)", new String[]{"id_cliente"});
                    ps.setString(1, AppMaterno);
                    ps.setString(2, AppPaterno);
                    ps.setString(3, Nombres);
                    ps.setString(4, Alias);
                    ps.setString(5, rutCliente);
                    // ps.setString(6, "NULL");
                    return ps;
                }
            },
                    id_clientekey);

        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;

        }

        this.CurrentCondonacion.setIdCliente(id_clientekey.getKey().intValue());

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {

            SQLG = "INSERT INTO [dbo].[condonacion]\n"
                    + "           ([timestap]\n"
                    + "           ,[id_regla]\n"
                    + "           ,[id_estado]\n"
                    + "           ,[comentario_resna]\n"
                    + "           ,[monto_total_condonado]\n"
                    + "           ,[monto_total_recibit]\n"
                    + "           ,[di_num_opers]\n"
                    + "           ,[monto_total_capital]\n"
                    + "           ,[di_fk_tipocondonacion]\n"
                    + "           ,[di_fk_idColadorador]\n"
                    + "           ,[di_fk_IdCliente]\n"
                    + "           ,[monto_condonado_capital]\n"
                    + "           ,[monto_condonado_recibe]\n"
                    + "           ,[monto_honorario_capital]\n"
                    + "           ,[monto_honorario_capitalRecibe])\n"
                    + "     VALUES\n"
                    + "  (getdate()                                                                 \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_id_Estado from estado where dv_cod_estado=?)                   \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_idtipoCondonacion from tipo_condonacion where dv_codtipocon=?) \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + " ,?)                                                                         ";

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQLG, new String[]{"id_condonacion"});
                    // ps.setString(1, person.getName());
                    // ps.setInt(1, consulta_id());

                    ps.setInt(1, 1);   //regla de condonacion applicaa
                    ps.setString(2, estado); // estado condonacion
                    ps.setString(3, TipoCon); // // tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoTotalTotal); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setString(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                                 // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    //  ps.execute();
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion  linea 528:[" + ex.getMessage() + "] Estado:[" + estado + "]tipoOperacion :" + tipoOperacion + " ");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);
        KeyHolder keyHolder2 = new GeneratedKeyHolder();

        CurrentCondonacion.getAdjuntar().setFk_idCliente(idCliente);
        CurrentCondonacion.getAdjuntar().setFk_idCondonacion(id_con);

        if (CurrentCondonacion.getAdjuntar().getaDet() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (ai.api(CurrentCondonacion.getAdjuntar(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            };
        };

        //graba glosa
        CurrentCondonacion.getGlosa().setId_client(idCliente);
        CurrentCondonacion.getGlosa().setId_cond(id_con);

        if (CurrentCondonacion.getGlosa().getDetalle() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (gi.api(CurrentCondonacion.getGlosa(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }
        /// condonasound insert

        return id_con;
    }

    
    public int GuardarCondonacionPM(final String estado, final String tipoOperacion, final String TipoCon, final int rechazo) {
        final String SQLG;
        final float MontoToalcondonado = (float) this.CurrentCondonacion.getTotalCondonado().getValor();
        final float MontoToalRecibido = (float) this.CurrentCondonacion.getTotoalRecibe().getValor();
        final float MontoToalCapital = (float) this.CurrentCondonacion.getCapitalClass().getValor();
        final float MontoTotalTotal = (float) this.CurrentCondonacion.getTotalTotal().getValor();
        final int cantidadOperaciones = this.CurrentCondonacion.getNumeroDeOperaciones();
        final float MontoHonoratios_sobreCapitalRecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        final float montocondonadocapital = (float) this.CurrentCondonacion.getMontoCondonado();
        final float montocondonadorecibe = (float) this.CurrentCondonacion.getCapitalTotal() - (float) this.CurrentCondonacion.getMontoCondonado();
        final float montohonorariosobrecapital = (float) this.CurrentCondonacion.getHonorarioJudicial().getValor();
        final float montohonorariosobremontocapiutalrecibe = (float) this.CurrentCondonacion.getHonorarioJudicial2Float();
        String nombre = CurrentCondonacion.getNombreCliente();
        nombs = nombre.split(" ");

        final String AppMaterno = nombs[3].length() > 0 ? nombs[3] : "SIN AppMaterno";
        final String AppPaterno = nombs[2].length() > 0 ? nombs[2] : "SIN AppPaterno";
        final String Nombres = nombs[0].length() > 0 ? nombs[0] + " " + (nombs[1].length() > 0 ? nombs[1] : "") : "SIN Nombres";
        final String Alias = "NO-EXISTE";
        final String rutCliente = this.CurrentCondonacion.getRutCliente();
        int id_cliente_rechazado = 0;
        KeyHolder id_clientekey = new GeneratedKeyHolder();

        try {
            if (rechazo == 0) {
                jdbcTemplate.update(
                        new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement("insert into cliente (apellido_materno,apellido_paterno,nombre,alias,rut) VALUES (?,?,?,?,?)", new String[]{"id_cliente"});
                        ps.setString(1, AppMaterno);
                        ps.setString(2, AppPaterno);
                        ps.setString(3, Nombres);
                        ps.setString(4, Alias);
                        ps.setString(5, rutCliente);
                        // ps.setString(6, "NULL");
                        return ps;
                    }
                },
                        id_clientekey);
            } else {
                String SQL = "select id_cliente from cliente where rut = ?";
                id_cliente_rechazado = jdbcTemplate.queryForInt(SQL, new Object[]{rutCliente});
            }
        } catch (DataAccessException ex) {

            SisCorelog(ex.getMessage());
            Messagebox.show("ERROR Al Insertar Cliente linea : 353 [" + ex.getMessage() + "]");
            return -1;
            //System.out.print("####### ---- ERROR de EJECUCION de Query///isClienteBci/// ERR:[" + ex.getMessage() + "] NumeroLinea["+NumeroLinea+"]----#######");
        }

        
         //##ActualizacionData
        this.CurrentCondonacion.setIdCliente((rechazo == 0) ? id_clientekey.getKey().intValue() : id_cliente_rechazado);

        final int idCliente = this.CurrentCondonacion.getIdCliente();

        KeyHolder keyHolder = new GeneratedKeyHolder();

        final int idColaborador = this.CurrentCondonacion._condonador._colaborador.getIdColaborador();
        try {

            SQLG = "INSERT INTO [dbo].[condonacion]\n"
                    + "           ([timestap]\n"
                    + "           ,[id_regla]\n"
                    + "           ,[id_estado]\n"
                    + "           ,[comentario_resna]\n"
                    + "           ,[monto_total_condonado]\n"
                    + "           ,[monto_total_recibit]\n"
                    + "           ,[di_num_opers]\n"
                    + "           ,[monto_total_capital]\n"
                    + "           ,[di_fk_tipocondonacion]\n"
                    + "           ,[di_fk_idColadorador]\n"
                    + "           ,[di_fk_IdCliente]\n"
                    + "           ,[monto_condonado_capital]\n"
                    + "           ,[monto_condonado_recibe]\n"
                    + "           ,[monto_honorario_capital]\n"
                    + "           ,[monto_honorario_capitalRecibe])\n"
                    + "     VALUES\n"
                    + "  (getdate()                                                                 \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_id_Estado from estado where dv_cod_estado=?)                   \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,(select di_idtipoCondonacion from tipo_condonacion where dv_codtipocon=?) \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + "  ,?                                                                         \n"
                    + " ,?)                                                                         ";

            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQLG, new String[]{"id_condonacion"});
                    // ps.setString(1, person.getName());
                    // ps.setInt(1, consulta_id());

                    ps.setInt(1, 1);   //regla de condonacion applicaa
                    ps.setString(2, estado); // estado condonacion
                    ps.setString(3, TipoCon); // // tipo condonacion 
                    ps.setFloat(4, (float) MontoToalcondonado); // monto total condonado
                    ps.setFloat(5, (float) MontoTotalTotal); //  monto total recibido
                    ps.setInt(6, cantidadOperaciones);           // numero operaciones
                    ps.setFloat(7, (float) MontoToalCapital);     // monto total capitasl
                    ps.setString(8, tipoOperacion);                    // tipo condonacion           
                    ps.setInt(9, idColaborador);                                 // getid colaboradot
                    ps.setInt(10, idCliente);
                    ps.setFloat(11, montocondonadocapital);
                    ps.setFloat(12, montocondonadorecibe);
                    ps.setFloat(13, montohonorariosobrecapital);
                    ps.setFloat(14, montohonorariosobremontocapiutalrecibe);
                    //  ps.execute();
                    return ps;
                }
            },
                    keyHolder);

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error al Guardar Condonacion  linea 528:[" + ex.getMessage() + "] Estado:[" + estado + "]tipoOperacion :" + tipoOperacion + " ");
            return 0;
        }

        id_con = keyHolder.getKey().intValue();
        SisCorelog("returnTraking: [" + id_con + "]");
        this.CurrentCondonacion.setId((long) id_con);
        KeyHolder keyHolder2 = new GeneratedKeyHolder();

        CurrentCondonacion.getAdjuntar().setFk_idCliente(idCliente);
        CurrentCondonacion.getAdjuntar().setFk_idCondonacion(id_con);

        if (CurrentCondonacion.getAdjuntar().getaDet() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (ai.api(CurrentCondonacion.getAdjuntar(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            };
        };

        //graba glosa
        CurrentCondonacion.getGlosa().setId_client(idCliente);
        CurrentCondonacion.getGlosa().setId_cond(id_con);

        if (CurrentCondonacion.getGlosa().getDetalle() != null) {
            //el retorno cuando la accion es 1 tiene que ser null
            if (gi.api(CurrentCondonacion.getGlosa(), 1) == null) {
                String mssje = "Error Guardando la información (Archivos) del Detalle Condonación.";
                Messagebox.show(mssje, "Error", Messagebox.OK, Messagebox.ERROR);
                return 0;
            }
        }
        /// condonasound insert

        return id_con;
    }

    public boolean GuardarOperacionesAndCliente() {

        KeyHolder id_operKey = new GeneratedKeyHolder();
        for (int j = 0; j < this.CurrentCondonacion.getListaOperaciones().size(); j++) {
            final String OperacionCliente = CurrentCondonacion.getListaOperaciones().get(j).getOperacionOriginal();
            final String ProductoOperacionCliente = CurrentCondonacion.getListaOperaciones().get(j).getProductos();
            try {

                jdbcTemplate.update(
                        new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(insertSqlOperaciones, new String[]{"id_operacion"});
                        ps.setString(1, OperacionCliente);
                        ps.setString(2, ProductoOperacionCliente);

                        return ps;
                    }
                },
                        id_operKey);
            } catch (DataAccessException ex) {

                SisCorelog(ex.getMessage());
                Messagebox.show("ERROR CONNACIONIMPL linea : 339 [" + ex.getMessage() + "]");
                return false;

            }
            int idcondonacion = this.id_con;
            double interes = this.CurrentCondonacion.getListaOperaciones().get(j).getMora() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto();
            double MontoRecibe = this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7;
            double HonorarioJudicial = MontoRecibe * 0.20;

            try {
                jdbcTemplate.update(insertSqlDetalleCondonacion, new Object[]{
                    this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto(),
                    this.CurrentCondonacion.getIdCliente(),
                    id_operKey.getKey().intValue(), // limiteinferior y superior del rangoocupado
                    "GENERADA", // Monto a condonar para Interes
                    idcondonacion, // Rango de Fecha inicial Ocupado par la condonacion
                    this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7,
                    this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() - this.CurrentCondonacion.getListaOperaciones().get(j).getSaldoinsoluto() * 0.7, ///Rango Fecha Final Ocupado para la condonacion
                    interes,
                    interes * 0.7,
                    interes - interes * 0.7,
                    HonorarioJudicial,
                    HonorarioJudicial * 0.7,
                    HonorarioJudicial - HonorarioJudicial * 0.7
                });
            } catch (DataAccessException ex) {

                SisCorelog(ex.getMessage());
                Messagebox.show("ERROR CONNACION-GuardandoDoc-IMPL linea : 442 [" + ex.getMessage() + "]");
                return false;

            }

        }

        return true;

    }

    public boolean GuardarCliente() {
        return false;
    }

    public boolean isClinete(String rut) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select count(id_cliente) from cliente where rut='" + rut + "'";
        int isCliente = 0;
        int count = 0;
        try {
            isCliente = jdbcTemplate.queryForInt(sql);
        } catch (DataAccessException ex) {

            SisCorelog("QUery :[" + sql + "]" + ex.getMessage());

        }

        SisCorelog("QUery :[" + sql + "]");
        return isCliente > 0;

    }

    protected String getDateToDBFormat(Date fechaCreacion) {
        return "TO_TIMESTAMP('"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaCreacion)
                + "', 'yyyy-mm-dd hh24:mi:ss')";
    }

    public java.sql.Date getCurrentDatetime() {
        java.util.Date today = new Date(System.currentTimeMillis());
        return new java.sql.Date(today.getTime());
    }

    public List<CondonacionTabla2> GetCondonacionesAplicada() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + " inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("APPLY") + "," + _estados._buscarIDEstado("CE-AN-EJE") + ") \n";

        List<CondonacionTabla2> dsol = null;
        try {

            dsol = jdbcTemplate.query(SQL, new CondonacionMapper2());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 434: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla> GetCondonacionesAplicada(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("APP-APLICADA") + "," + _estados._buscarIDEstado("CE-AN-EJE") + ") and usu.alias ='" + cuenta + "' \n";

        List<CondonacionTabla> dsol = null;
        try {

            dsol = jdbcTemplate.query(SQL, new CondonacionMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 434: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesAplicada2() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        // CE-AN-EJE  :
        String estados_considerados = _estados._buscarIDEstado("APP-APLICADA") + ","
                + _estados._buscarIDEstado("CE-AN-EJE");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("APP-APLICADA") + "," + _estados._buscarIDEstado("CE-AN-EJE") + ")\n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aplicadas de retail.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesAplicada2(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        // CE-AN-EJE  :
        String estados_considerados = _estados._buscarIDEstado("APP-APLICADA") + ","
                + _estados._buscarIDEstado("CE-AN-EJE");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("APP-APLICADA") + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aplicadas de retail para el usuario: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesRechazadas(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        String SQL = "SELECT \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    count(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "    cr.fecIngreso fec_rechazo,\n"
                + "    tr.Detalle tipo_rechazo,\n"
                + "    GG.fld_ced \n"
                + "FROM (\n"
                + "	   SELECT \n"
                + "		  cli.rut,\n"
                + "		  cli.nombre,\n"
                + "		  con.id_condonacion,\n"
                + "		  con.timestap,\n"
                + "		  re.[desc] 'regla',\n"
                + "		  est.dv_estado,\n"
                + "		  con.comentario_resna,\n"
                + "		  con.monto_total_condonado,\n"
                + "		  con.monto_total_recibit,\n"
                + "		  con.di_num_opers,\n"
                + "		  con.monto_total_capital,\n"
                + "		  tpcon.dv_desc,\n"
                + "		  usu.di_rut,\n"
                + "		  cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "	   FROM condonacion con \n"
                + "        INNER JOIN regla re on re.id_regla=con.id_regla \n"
                + "        INNER JOIN estado est on est.di_id_Estado=con.id_estado \n"
                + "        INNER JOIN tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "        INNER JOIN colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "        INNER JOIN usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "        INNER JOIN cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                // + "        INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "        WHERE usu.alias = ?\n"
                + "    ) as GG          \n"
                + "LEFT JOIN reparo rep on GG.id_condonacion = rep.di_fk_IdCondonacion \n"
                + "INNER JOIN dbo.Cond_Rechazadas cr ON GG.id_condonacion = cr.fk_idCondonacion\n"
                + "INNER JOIN dbo.Tipo_Rechazo tr ON cr.fk_idTipRechazo = tr.id_TipRechazo\n"
                + "GROUP BY \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    cr.fecIngreso,\n"
                + "    tr.Detalle,\n"
                + "    GG.fld_ced \n"
                + "ORDER BY cr.fecIngreso DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new Object[]{cuenta}, new CondonacionMapperApplyRechazadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones rechazadas para: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    /**
     * trae ambos cedentes para aprobadores
     *
     * @param cuenta String, cuenta de usuario
     * @param cedente int, 1=RETAIL; 2=PYME
     * @return lista de tipo CondonacionTabla2
     */
    public List<CondonacionTabla2> GetCondonacionesRechazadasAprobadores(String cuenta, int cedente) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int id_estadoAprob = 0;

        if (cedente == 1) {
            id_estadoAprob = _estados._buscarIDEstado("PAA");
        } else if (cedente == 2) {
            id_estadoAprob = _estados._buscarIDEstado("APRO_ZONAL");
        }

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    CR.FECINGRESO FEC_RECHAZO,\n"
                + "    TR.DETALLE TIPO_RECHAZO,\n"
                + "    GG.FLD_CED \n"
                + "    FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "          ,TE.DV_CUNETAORIGEN  \n"
                + "        FROM CONDONACION CON \n"
                + "        INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "        INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "        INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "        INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "        INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "        INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "        INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "        WHERE EC.DI_FK_IDESTADO  in (" + _estados._buscarIDEstado("RECH") + " ," + _estados._buscarIDEstado("APP-RECHA") + "," + _estados._buscarIDEstado("AN-RECHA") + "," + _estados._buscarIDEstado("RECH-ANA") + ")   \n"
                // + "        AND TE.DV_CUNETAORIGEN = ?\n"
                + "         ) AS GG          \n"
                + "         LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "         INNER JOIN DBO.COND_RECHAZADAS CR ON GG.ID_CONDONACION = CR.FK_IDCONDONACION\n"
                + "         INNER JOIN DBO.TIPO_RECHAZO TR ON CR.FK_IDTIPRECHAZO = TR.ID_TIPRECHAZO\n"
                + "         GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    CR.FECINGRESO,\n"
                + "    TR.DETALLE,\n"
                + "    GG.FLD_CED \n"
                + "   ,GG.DV_CUNETAORIGEN  \n"
                + "ORDER BY GG.RUT DESC";

        List<CondonacionTabla2> dsol = null;
        try {

            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApplyRechazadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones rechazadas aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    /**
     * trae ambos cedentes para aprobadores
     *
     * @param cuenta String, cuenta de usuario
     * @param cedente int, 1=RETAIL; 2=PYME
     * @return lista de tipo CondonacionTabla2
     */
    public List<CondonacionTabla2> GetCondonacionesRechazadasAnalista(String cuenta, int cedente) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int id_estadoAprob = 0;

        if (cedente == 1) {
            id_estadoAprob = _estados._buscarIDEstado("PAA");
        } else if (cedente == 2) {
            id_estadoAprob = _estados._buscarIDEstado("APRO_ZONAL");
        }

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    CR.FECINGRESO FEC_RECHAZO,\n"
                + "    TR.DETALLE TIPO_RECHAZO,\n"
                + "    GG.FLD_CED \n"
                + "    FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
             //   + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                   + " isnull((select top 1 cedr.cedente from  cedente_rut cedr where cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1))),'NODATA') as fld_ced \n"
                + "          ,TE.DV_CUNETAORIGEN  \n"
                + "        FROM CONDONACION CON \n"
                + "        INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "        INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "        INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "        INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "        INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "        INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "        INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "        WHERE EC.DI_FK_IDESTADO  in (" + _estados._buscarIDEstado("RECH") + " ," + _estados._buscarIDEstado("APP-RECHA") + "," + _estados._buscarIDEstado("AN-RECHA") + "," + _estados._buscarIDEstado("RECH-ANA") + ")   \n"
                // + "        AND TE.DV_CUNETAORIGEN = ?\n"
                + "         ) AS GG          \n"
                + "         LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "         INNER JOIN DBO.COND_RECHAZADAS CR ON GG.ID_CONDONACION = CR.FK_IDCONDONACION\n"
                + "         INNER JOIN DBO.TIPO_RECHAZO TR ON CR.FK_IDTIPRECHAZO = TR.ID_TIPRECHAZO\n"
                + "         GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    CR.FECINGRESO,\n"
                + "    TR.DETALLE,\n"
                + "    GG.FLD_CED \n"
                + "   ,GG.DV_CUNETAORIGEN  \n"
                + "ORDER BY GG.RUT ASC";

        List<CondonacionTabla2> dsol = null;
        try {

            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApplyRechazadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones rechazadas aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesProrrogadasAprobadores(String cuenta, int cedente) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int id_estadoAprob = 0;
        if (cedente == 1) {
            id_estadoAprob = _estados._buscarIDEstado("PAA");
        } else if (cedente == 2) {
            id_estadoAprob = _estados._buscarIDEstado("APRO_ZONAL");
        }

        String SQL = "SELECT \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    count(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "    cp.fecIngreso fec_prorroga,\n"
                + "    tp.Detalle tipo_prorroga,\n"
                + "    GG.fld_ced \n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "        FROM CONDONACION CON \n"
                + "        INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "        INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "        INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "        INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "        INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "        INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "        INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION\n"
                + "        INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "        WHERE EC.DI_FK_IDESTADO =  ?\n"
                + "	   AND TE.DV_CUNETAORIGEN = ?\n"
                + "    ) AS GG          \n"
                + "LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "INNER JOIN DBO.COND_PRORROGADAS CP ON GG.ID_CONDONACION = CP.FK_IDCONDONACION\n"
                + "INNER JOIN DBO.TIPO_PRORROGA TP ON CP.FK_IDTIPOPRORROGA = TP.ID_TIPOPRORROGA\n"
                + "GROUP BY \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    cp.fecIngreso ,\n"
                + "    tp.Detalle,\n"
                + "    GG.fld_ced \n"
                + "ORDER BY cp.fecIngreso  DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new Object[]{id_estadoAprob, cuenta}, new CondonacionMapperApplyProrrogadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones prorrogadas aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesProrrogadas(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    CP.FECINGRESO FEC_PRORROGA,\n"
                + "    TP.DETALLE TIPO_PRORROGA,\n"
                + "    GG.FLD_CED \n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "        FROM CONDONACION CON \n"
                + "        INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "        INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "        INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "        INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "        INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "        INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "        INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "        WHERE USU.ALIAS = ? \n"
                + "        AND CON.ID_ESTADO IN (" + _estados._buscarIDEstado("APP-PRORROGA") + ")\n"
                + "    ) AS GG          \n"
                + "LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "INNER JOIN DBO.COND_PRORROGADAS CP ON GG.ID_CONDONACION = CP.FK_IDCONDONACION\n"
                + "INNER JOIN DBO.TIPO_PRORROGA TP ON CP.FK_IDTIPOPRORROGA = TP.ID_TIPOPRORROGA\n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    CP.FECINGRESO,\n"
                + "    TP.DETALLE,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY CP.FECINGRESO DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new Object[]{cuenta}, new CondonacionMapperApplyProrrogadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones prorrogadas para: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesCerradasPm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
                + "               from (\n"
                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + "                inner join regla re on re.id_regla=con.id_regla \n"
                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + "               \n"
                + "                where con.id_estado in (" + _estados._buscarIDEstado("CERR_EJEPYME") + "," + _estados._buscarIDEstado("CERR_ZONALPYME") + ") and usu.alias ='" + cuenta + "' \n"
                + "                ) as GG\n"
                + "               \n"
                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapper3());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesAplicada2Pm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        // CERR_EJEPYME  :
        // CERR_ZONALPYME  :
        String estados_considerados = _estados._buscarIDEstado("APP-APLICADA") + ","
                + _estados._buscarIDEstado("CERR_EJEPYME") + ","
                + _estados._buscarIDEstado("CERR_ZONALPYME");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("APP-APLICADA") + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aplicadas de pyme para el usuario: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }

    }

    public List<CondonacionTabla> GetCondonacionesPendientesCountReparo() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + ",count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
                + "from (\n"
                + "select cola.id, con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "\n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("PEN") + "," + _estados._buscarIDEstado("PA") + "," + _estados._buscarIDEstado("ANRECEP") + ")\n"
                + " ) as GG\n"
                + "\n"
                + "  left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "  group by GG.id_condonacion,GG.id,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut \n";

        List<CondonacionTabla> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionCountReparoMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + " inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("PEN") + "," + _estados._buscarIDEstado("PA") + "," + _estados._buscarIDEstado("ANRECEP") + ") \n";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapper2());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2Pm() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + " inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("PEN-ZONAL") + "," + _estados._buscarIDEstado("REP-EJEPYME") + ") \n";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapper2());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2CountReparo() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // pen : Pendientes
        // pa  :
        // ANRECEP : Recepcion, modulo_analista
        String estados_considerados = _estados._buscarIDEstado("PEN") + ","
                + _estados._buscarIDEstado("PA") + ","
                + _estados._buscarIDEstado("ANRECEP") /*+ ","
                + _estados._buscarIDEstado("EJE-GES-PRO")*/
              //  + "," +_estados._buscarIDEstado("PEN-ZONAL")
                ;
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */

//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("PEN") + "," + _estados._buscarIDEstado("PA") + "," + _estados._buscarIDEstado("ANRECEP") + ")\n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            ISNULL (cedr.cedente,'OTRO') fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //   + "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "           LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";

        List<CondonacionTabla2> dsol = null;

        try {
            //Messagebox.show(SQL);
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones pendientes.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2CountReparoPm() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        List<CondonacionTabla2> dsol = null;
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PEN-ZONAL : Pendientes
        // EJEPM-GES-PRO  :prorroga pendiente de aplicacion o rechazo  
        // REP-EJEPYME : Recepcion, modulo_analista
        String estados_considerados = _estados._buscarIDEstado("PEN-ZONAL") + "," + _estados._buscarIDEstado("REP-EJEPYME") /*+ ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO")*/;
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "SELECT GG.rut,\n"
//                + "       GG.nombre,\n"
//                + "       GG.id_condonacion,\n"
//                + "       GG.timestap,\n"
//                + "       GG.regla,\n"
//                + "       GG.dv_estado,\n"
//                + "       GG.comentario_resna,\n"
//                + "       GG.monto_total_condonado,\n"
//                + "       GG.monto_total_recibit,\n"
//                + "       GG.di_num_opers,\n"
//                + "       GG.monto_total_capital,\n"
//                + "       GG.dv_desc,\n"
//                + "       GG.di_rut,\n"
//                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos'\n"
//                + "FROM\n"
//                + "(\n"
//                + "    SELECT cli.rut,\n"
//                + "           cli.nombre,\n"
//                + "           con.id_condonacion,\n"
//                + "           con.timestap,\n"
//                + "           re.[desc] 'regla',\n"
//                + "           est.dv_estado,\n"
//                + "           con.comentario_resna,\n"
//                + "           con.monto_total_condonado,\n"
//                + "           con.monto_total_recibit,\n"
//                + "           con.di_num_opers,\n"
//                + "           con.monto_total_capital,\n"
//                + "           tpcon.dv_desc,\n"
//                + "           usu.di_rut\n"
//                + "    FROM condonacion con\n"
//                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
//                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
//                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
//                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
//                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
//                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
//                + "    WHERE (con.id_estado IN(" + _estados._buscarIDEstado("PEN-ZONAL") + ", "
//                + _estados._buscarIDEstado("REP-EJEPYME") + ", "
//                + _estados._buscarIDEstado("EJEPM-GES-PRO") + "))\n"
//                + ") AS GG\n"
//                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
//                + "GROUP BY GG.rut,\n"
//                + "         GG.nombre,\n"
//                + "         GG.id_condonacion,\n"
//                + "         GG.timestap,\n"
//                + "         GG.regla,\n"
//                + "         GG.dv_estado,\n"
//                + "         GG.comentario_resna,\n"
//                + "         GG.monto_total_condonado,\n"
//                + "         GG.monto_total_recibit,\n"
//                + "         GG.di_num_opers,\n"
//                + "         GG.monto_total_capital,\n"
//                + "         GG.dv_desc,\n"
//                + "         GG.di_rut\n"
//                + "ORDER BY GG.timestap DESC;";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //+ "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";

        try {
            // Messagebox.show(SQL);
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones pendientes.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PEN : Pendientes
        // PA  :
        // ANRECEP : Recepcion, modulo_analista
        //EJE-GES-PRO : prorroga pendiente de aplicacion o rechazo
        String estados_considerados = _estados._buscarIDEstado("PEN") + ","
                + _estados._buscarIDEstado("PA") + ","
                + _estados._buscarIDEstado("ANRECEP") + ","
                + _estados._buscarIDEstado("EJE-GES-PRO");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + estados_considerados + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones pendientes.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesPendientes2Pm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PEN-ZONAL : Pendientes
        // EJEPM-GES-PRO  :prorroga pendiente de aplicacion o rechazo  
        // REP-EJEPYME : Recepcion, modulo_analista
        String estados_considerados = _estados._buscarIDEstado("PEN-ZONAL") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("REP-EJEPYME");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + estados_considerados + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones pendientes.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesReparadMotivos() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // RA : 
        String estados_considerados = _estados._buscarIDEstado("RA") + ","+_estados._buscarIDEstado("AN-REP-CAN-PYME")+"";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,\n"
//                + "     GG.nombre,\n"
//                + "     GG.id_condonacion,\n"
//                + "     GG.timestap,\n"
//                + "     GG.regla,\n"
//                + "     GG.dv_estado,\n"
//                + "     GG.comentario_resna,\n"
//                + "     GG.monto_total_condonado,\n"
//                + "     GG.monto_total_recibit,\n"
//                + "     GG.di_num_opers,\n"
//                + "     GG.monto_total_capital,\n"
//                + "     GG.dv_desc,\n"
//                + "     GG.di_rut,\n"
//                + "     count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "     from (\n"
//                + "     select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "      inner join regla re on re.id_regla=con.id_regla \n"
//                + "      inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "      inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "      inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "      inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "      inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "\n"
//                + "      where con.id_estado in (" + _estados._buscarIDEstado("RA") + ")\n"
//                + "      ) as GG\n"
//                + "\n"
//                + "       left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "       group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc \n";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            Messagebox.show(SQL);
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }
    public List<CondReparadas> GetCondonacionesReparadasV2() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // RA : 
        String estados_considerados = _estados._buscarIDEstado("RA") + ","+_estados._buscarIDEstado("AN-REP-CAN-PYME")+"";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */

        String SQL = "SELECT\n" +
"            CLI.rut 'rut',\n" +
"            CLI.nombre 'nombre',\n" +
"            CON.id_condonacion 'id_condonacion',\n" +
"            CON.timestap 'timestap',\n" +
"            EST.dv_estado 'dv_estado',\n" +
"            CON.COMENTARIO_RESNA 'COMENTARIO_RESNA',\n" +
"            CON.MONTO_TOTAL_CONDONADO 'MONTO_TOTAL_CONDONADO',\n" +
"            CON.MONTO_TOTAL_RECIBIT 'MONTO_TOTAL_RECIBIT',\n" +
"            CON.DI_NUM_OPERS 'DI_NUM_OPERS',\n" +
"            CON.MONTO_TOTAL_CAPITAL 'MONTO_TOTAL_CAPITAL',\n" +
"            con.di_fk_idColadorador 'di_fk_idColadorador',   \n" +
"            isnull((select top 1 cedr.cedente from  cedente_rut cedr where cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1))),'NODATA') as fld_ced\n" +
"            FROM CONDONACION CON\n" +
"            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE\n" +
"            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO\n" +
                 "WHERE EST.DI_ID_ESTADO IN ("+estados_considerados+")\n" +
"            order by timestap desc";

        List<CondReparadas> dsol =  new ArrayList<CondReparadas>();
        try {
          //  Messagebox.show(SQL+"---data["+dataSource.getConnection().getMetaData().getURL()+"]");
        
      //    Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,SQL +  " DATASOURCE"+dataSource.getConnection().getSchema());
            dsol = jdbcTemplate.query(SQL, new BeanPropertyRowMapper(CondReparadas.class));
         //   Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,"SQLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
        } catch (DataAccessException e) {
             Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, e.getMessage(), e);
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol =  new ArrayList<CondReparadas>();
        } finally {
        
             Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,"FINALMENTEEEEEEEEEEEEEEEEEEEEEE");
            return dsol;
        }
    }

     public List<CondReparadas> GetCondonacionesReparadasV3() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // RA : 
        String estados_considerados = _estados._buscarIDEstado("RA") + ","+_estados._buscarIDEstado("AN-REP-CAN-PYME")+"";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */

        String SQL = "SELECT\n" +
"            CLI.rut 'rut',\n" +
"            CLI.nombre 'nombre',\n" +
"            CON.id_condonacion 'id_condonacion',\n" +
"            CON.timestap 'timestap',\n" +
"            EST.dv_estado 'dv_estado',\n" +
"            CON.COMENTARIO_RESNA 'COMENTARIO_RESNA',\n" +
"            CON.MONTO_TOTAL_CONDONADO 'MONTO_TOTAL_CONDONADO',\n" +
"            CON.MONTO_TOTAL_RECIBIT 'MONTO_TOTAL_RECIBIT',\n" +
"            CON.DI_NUM_OPERS 'DI_NUM_OPERS',\n" +
"            CON.MONTO_TOTAL_CAPITAL 'MONTO_TOTAL_CAPITAL',\n" +
"            con.di_fk_idColadorador 'di_fk_idColadorador',   \n" +
"          ISNULL (cedr.cedente,'OTRO') fld_ced\n" +
"            FROM CONDONACION CON\n" +
"            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE\n" +
"            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO\n" +
                "     LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))\n" +
                 "WHERE EST.DI_ID_ESTADO IN ("+estados_considerados+")\n" +
"            order by timestap desc";

        List<CondReparadas> dsol =  new ArrayList<CondReparadas>();
        try {
          //  Messagebox.show(SQL+"---data["+dataSource.getConnection().getMetaData().getURL()+"]");
        
      //    Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,SQL +  " DATASOURCE"+dataSource.getConnection().getSchema());
            dsol = jdbcTemplate.query(SQL, new CondonacionReparosMapper());
         //   Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,"SQLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL");
        } catch (DataAccessException e) {
             Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, e.getMessage(), e);
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol =  new ArrayList<CondReparadas>();
        } finally {
        
             Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO,"FINALMENTEEEEEEEEEEEEEEEEEEEEEE");
            return dsol;
        }
    }

    
    public List<CondonacionTabla2> GetCondonacionesReparadMotivosPm() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // REP_ZONALPYME : 
        String estados_considerados = _estados._buscarIDEstado("REP_ZONALPYME") + "";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "     ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "     from (\n"
//                + "     select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "      inner join regla re on re.id_regla=con.id_regla \n"
//                + "      inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "      inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "      inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "      inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "      inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "\n"
//                + "      where con.id_estado in (" + _estados._buscarIDEstado("REP_ZONALPYME") + ")\n"
//                + "      ) as GG\n"
//                + "\n"
//                + "       left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "       group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc \n";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "            EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "            INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "            INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "            INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "            INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "            INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "            INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "            INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "            INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "        WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "                	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesReparadMotivosPm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // REP_ZONALPYME : 
        String estados_considerados = _estados._buscarIDEstado("REP_ZONALPYME") + "";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "SELECT GG.rut,\n"
//                + "       GG.nombre,\n"
//                + "       GG.id_condonacion,\n"
//                + "       GG.timestap,\n"
//                + "       GG.regla,\n"
//                + "       GG.dv_estado,\n"
//                + "       GG.comentario_resna,\n"
//                + "       GG.monto_total_condonado,\n"
//                + "       GG.monto_total_recibit,\n"
//                + "       GG.di_num_opers,\n"
//                + "       GG.monto_total_capital,\n"
//                + "       GG.dv_desc,\n"
//                + "       GG.di_rut,\n"
//                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos'\n"
//                + "FROM\n"
//                + "(\n"
//                + "    SELECT cli.rut,\n"
//                + "           cli.nombre,\n"
//                + "           con.id_condonacion,\n"
//                + "           con.timestap,\n"
//                + "           re.[desc] 'regla',\n"
//                + "           est.dv_estado,\n"
//                + "           con.comentario_resna,\n"
//                + "           con.monto_total_condonado,\n"
//                + "           con.monto_total_recibit,\n"
//                + "           con.di_num_opers,\n"
//                + "           con.monto_total_capital,\n"
//                + "           tpcon.dv_desc,\n"
//                + "           usu.di_rut\n"
//                + "    FROM condonacion con\n"
//                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
//                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
//                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
//                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
//                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
//                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
//                + "	    INNER JOIN dbo.estado_condonacion ec ON con.id_condonacion = ec.di_fk_IdCondonacion\n"
//                + "	    INNER JOIN dbo.trackin_estado te ON EC.di_idEstadoCondonacion = TE.di_fk_idEstadoCondonacion\n"
//                + "    WHERE TE.dv_CunetaOrigen = '" + cuenta + "'\n"
//                + "    AND  con.Id_Estado IN(" + _estados._buscarIDEstado("REP_ZONALPYME") + ")\n"
//                + "        \n"
//                + ") AS GG\n"
//                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
//                + "GROUP BY GG.rut,\n"
//                + "         GG.nombre,\n"
//                + "         GG.id_condonacion,\n"
//                + "         GG.timestap,\n"
//                + "         GG.regla,\n"
//                + "         GG.dv_estado,\n"
//                + "         GG.comentario_resna,\n"
//                + "         GG.monto_total_condonado,\n"
//                + "         GG.monto_total_recibit,\n"
//                + "         GG.di_num_opers,\n"
//                + "         GG.monto_total_capital,\n"
//                + "         GG.dv_desc,\n"
//                + "         GG.di_rut\n"
//                + "ORDER BY GG.timestap DESC;";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "		  INNER JOIN TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND TE.DV_CUNETAORIGEN = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas para el usuario: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public boolean SetCondonacionesAprobadaAnalista(int idcondonacion) {

        try {
            String SQL = "update condonacion set id_estado=" + _estados._buscarIDEstado("PAA") + " where id_condonacion = " + idcondonacion;
            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public boolean SetCondonacionesReparadaAnalista(int idcondonacion) {

        try {
            String SQL = "update condonacion set id_estado=" + _estados._buscarIDEstado("RA") + " where id_condonacion = " + idcondonacion;
            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public boolean SetCondonacionesReparadaZonal(int idcondonacion) {

        try {
            String SQL = "update condonacion set id_estado=" + _estados._buscarIDEstado("REP_ZONALPYME") + " where id_condonacion = " + idcondonacion;
            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public boolean SetCambiaEstadoCondonaciones(
            final String ModuloOrigen,
            final String ModuloDestino,
            final UsuarioPermiso UsuarioActual,
            final int Condonacion,
            final String EstadoOrigen,
            final String EstadoDestino) {

        Trackin_Estado tE = new Trackin_Estado();
        int returnTraking;
        int id_estadoOrigen = 0;
        int id_estadoDestino = 0;

        //detectamos area de trabajo y tipo de estado para realizar ingreso de trackin
        //PYME
        if ((UsuarioActual.getAreaTrab().equals("ZONALSISCONPYME")) || (UsuarioActual.getAreaTrab().equals("EJESISCONPYME"))) {
            if ((ModuloOrigen.equals("ZonalPyme.Recepcion") && ModuloDestino.equals("EjecutivoPyme.Reparadas")) || (ModuloOrigen.equals("ZonalPyme.Reparadas.Recepcion") && ModuloDestino.equals("EjecutivoPyme.Reparadas"))) {
                estado_destino = _estados._buscarIDEstado("REP_ZONALPYME"); //20;   // reparado zonal
            } else if (ModuloOrigen.equals("EjecutivoPyme.Reparadas") && ModuloDestino.equals("ZonalPyme.Reparadas.Recepcion")) {
                estado_destino = _estados._buscarIDEstado("REP-EJEPYME"); //19;   // envio a Reparados
            } else if (ModuloOrigen.equals("EjecutivoPyme.Nueva") && ModuloDestino.equals("ZonalPyme.Recepcion")) {
                estado_destino = _estados._buscarIDEstado("PEN-ZONAL"); //17;   // pendiente zonal
            } else if (ModuloOrigen.equals("EjecutivoPyme.Nueva") && ModuloDestino.equals("EjecutivoPyme.Aprobadas")) {
                estado_destino = _estados._buscarIDEstado("APRO-EJEPYME"); //24;   // aprueba ejecutivo
            } else if (ModuloOrigen.equals("ZonalPyme.Recepcion") && ModuloDestino.equals("ZonalPyme.Aprobadas")) {
                estado_destino = _estados._buscarIDEstado("APRO_ZONAL"); //21;   // aprueba zonal
            } else if (ModuloOrigen.equals("EjecutivoPyme.Aprobadas") && ModuloDestino.equals("EjecutivoPyme.Cerradas")) {
                estado_destino = _estados._buscarIDEstado("CERR_EJEPYME"); //22;   // Cerradas 
            } else if (ModuloOrigen.equals("Ejecutivo.Prorrogadas") && ModuloDestino.equals("Aplicador.Recepcion")) {
                estado_destino = _estados._buscarIDEstado("EJEPM-GES-PRO"); //22;   // Cerradas 
            }
        } else ////   estados para gente del HERNAN ROA 
        if ((UsuarioActual.getAreaTrab().equals("AN_APPSISCON")) || (UsuarioActual.getAreaTrab().equals("JEFE_APPSISCON"))) {

            if (ModuloOrigen.equals("Aplicacion.Recepcion") && ModuloDestino.equals("Ejecutivo.Aplicadas")) {
                estado_destino = _estados._buscarIDEstado("APP-APLICADA"); // envio a Reparados
            } else if (ModuloOrigen.equals("Aplicacion.Recepcion") && ModuloDestino.equals("Ejecutivo.Rechazadas")) {
                estado_destino = _estados._buscarIDEstado("APP-RECHA"); // envio a rechazadas
            } else if (ModuloOrigen.equals("Aplicacion.Recepcion") && ModuloDestino.equals("Ejecutivo.Prorrogadas")) {
                estado_destino = _estados._buscarIDEstado("APP-PRORROGA"); // envio a prorrogadas
            } else if (ModuloOrigen.equals("Aplicacion.Recepcion") && ModuloDestino.equals("Ejecutivo.Reversadas")) {
                estado_destino = _estados._buscarIDEstado("APP-REVERSA"); // envio a reversadas
            }

        } else // esto es para las analista de can que Aprueban Condonaciones PYME   ///"ZonalPyme.Reparadas.Recepcion", "EjecutivoPyme.Reparadas"
        if (((UsuarioActual.getAreaTrab().equals("ANSICON")) && ModuloOrigen.equals("ZonalPyme.Recepcion")) || ((UsuarioActual.getAreaTrab().equals("ANSICON")) && ModuloOrigen.equals("ZonalPyme.Reparadas.Recepcion")) ) {

            if ((ModuloOrigen.equals("ZonalPyme.Recepcion") && ModuloDestino.equals("EjecutivoPyme.Reparadas")) || (ModuloOrigen.equals("ZonalPyme.Reparadas.Recepcion") && ModuloDestino.equals("EjecutivoPyme.Reparadas")))
            {
                estado_destino = _estados._buscarIDEstado("AN-REP-CAN-PYME"); //20;   // reparado zonal
                
            } else if (ModuloOrigen.equals("ZonalPyme.Recepcion") && ModuloDestino.equals("ZonalPyme.Aprobadas")) 
            {
                estado_destino = _estados._buscarIDEstado("AN-APRO-CAN-PYME"); //32;   // aprueba zonal
            }

        }
        {
            //RETAIL
            if (ModuloOrigen.equals("Ejecutivo.Campaña") && ModuloDestino.equals("Aplicacion.Pendientes")) 
            {
                estado_destino = _estados._buscarIDEstado("APP-PEND-CAMPANA"); //37;   // envio a Reparados
            }
            if (ModuloOrigen.equals("Analista.Recepcion") && ModuloDestino.equals("Ejecutivo.Reparadas")) 
            {
                estado_destino = _estados._buscarIDEstado("RA"); //11;   // envio a Reparados
            }
            if (ModuloOrigen.equals("Ejecutivo.Nueva") && ModuloDestino.equals("Ejecutivo.Aprobadas")) 
            {
                estado_destino = _estados._buscarIDEstado("AP"); //2;   // aprobado ejecutivo
            }
            if (ModuloOrigen.equals("Ejecutivo.Nueva") && ModuloDestino.equals("Analista.Recepcion")) 
            {
                estado_destino = _estados._buscarIDEstado("PA"); //9;   // pendiente Analista
            }
            if (ModuloOrigen.equals("Ejecutivo.Reparadas") && ModuloDestino.equals("Analista.Recepcion")) 
            {
                estado_destino = _estados._buscarIDEstado("ANRECEP"); //15;   // envio a Reparados
            }
            if (ModuloOrigen.equals("Analista.Recepcion") && ModuloDestino.equals("Analista.Aprobadas")) 
            {
                estado_destino = _estados._buscarIDEstado("PAA"); //10;   // envio a Reparados
            }
            if (ModuloOrigen.equals("Analista.Recepcion") && ModuloDestino.equals("Ejecutivo.Rechazadas")) 
            {
                estado_destino = _estados._buscarIDEstado("AN-RECHA"); //31;   // envio de analista a rechazo en integracion es 31
            }
            if (ModuloOrigen.equals("Ejecutivo.Prorrogadas") && ModuloDestino.equals("Aplicador.Recepcion")) 
            {
                estado_destino = _estados._buscarIDEstado("EJE-GES-PRO"); //22;   // Cerradas 
            }
        }
        // guardamos el cambio de estado y luego cambiamos la condonacion de estado tracking estado 
        KeyHolder keyHolder = new GeneratedKeyHolder();

        if (estado_destino > 0) {
            try {
                jdbcTemplate.update(new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(insertSqlEstado_Condonacion, new String[]{"di_idEstadoCondonacion"});
                        ps.setInt(1, estado_destino);
                        ps.setInt(2, Condonacion);

                        return ps;
                    }
                },
                        keyHolder);
            } catch (DataAccessException ex) {

                SisCorelog(ex.getMessage());
                Messagebox.show("ERROR CONNACION-FuncionGuardar-IMPL linea : 193 [" + ex.getMessage() + "]");
                return false;

            }

            returnTraking = keyHolder.getKey().intValue();
            SisCorelog("returnTraking: [" + returnTraking + "]");

            ////  inserta tracking 
            tE.usuarioOrigen(UsuarioActual.getCuenta());

            tE.setDv_estadoInicio(EstadoOrigen);
            tE.setDv_estadoFin(EstadoDestino);
            tE.setDv_ModuloOrigen(ModuloOrigen);
            tE.setDv_ModuloDestino(ModuloDestino);
            tE.setDi_fk_idEstadoCondonacion(returnTraking);

            if ((estado_destino == _estados._buscarIDEstado("AP"))//aprueba ejecutivo retail
                    ||(estado_destino == _estados._buscarIDEstado("APP-PEND-CAMPANA")) 
                    || (estado_destino == _estados._buscarIDEstado("APRO-EJEPYME"))//aprueba ejecutivo pyme
                    || (estado_destino == _estados._buscarIDEstado("CERR_EJEPYME"))) {//cierra ejecutivo
                //ejecutivo aprueba y cierra
                tE.setDi_fK_ColaboradorDestino(tE.getDi_fk_ColaboradorOrigen());
                tE.setDv_CuentaDestino(tE.getDv_CunetaOrigen());
            } else if (/*(estado_destino == _estados._buscarIDEstado("APRO_ZONAL")) //aprobada zonal
                    || */(estado_destino == _estados._buscarIDEstado("REP_ZONALPYME")) //reparada zonal
                    || (estado_destino == _estados._buscarIDEstado("RA")) //reparada analista
                    || (estado_destino == _estados._buscarIDEstado("AN-RECHA")) //Rechazada analista
                    //|| (estado_destino == _estados._buscarIDEstado("PAA"))//aprobada analista
                    || (estado_destino == _estados._buscarIDEstado("APP-APLICADA")) //aplicada aplicador
                    || (estado_destino == _estados._buscarIDEstado("APP-RECHA")) //rechazada aplicador
                    || (estado_destino == _estados._buscarIDEstado("APP-REVERSA"))) {//reversa aplicador

                tE.ejecutivoDestino(Condonacion); //retorna al ejecutivo que curso la condonación

            } else if ((estado_destino == _estados._buscarIDEstado("REP-EJEPYME")) //reparo ejecutivo pyme
                    || (estado_destino == _estados._buscarIDEstado("ANRECEP"))) { //reparo ejecutivo retail
                tE.usuarioDestino(Condonacion);
            }

            tE.insertTrackin_Estado();

            //cambio estado condonacion
            try {
                String SQL = "update condonacion set id_estado=" + estado_destino + " where id_condonacion = " + Condonacion;
                jdbcTemplate.update(SQL);
                System.out.println("Updated Record with ID = " + Condonacion);
                return true;
            } catch (DataAccessException e) {
                Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean SetCondonacionesAprobadaZonal(int idcondonacion) {

        try {
            String SQL = "update condonacion set id_estado=" + _estados._buscarIDEstado("APRO_ZONAL") + " where id_condonacion = " + idcondonacion;
            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public List<CondonacionTabla2> GetCondonacionesReparads() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // RA : 
        String estados_considerados = _estados._buscarIDEstado("RA") + "";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + estados_considerados + ") \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "		  EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas de retail.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesReparads(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // RA : 
        String estados_considerados = _estados._buscarIDEstado("RA") + "";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + estados_considerados + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "		  EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS ='" + cuenta + "' \n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas de retail para el usuario: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesReparadsPm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // REP_ZONALPYME : 
        String estados_considerados = _estados._buscarIDEstado("REP_ZONALPYME") + ","+ _estados._buscarIDEstado("AN-REP-CAN-PYME")+"";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("REP_ZONALPYME") + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "		  EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS ='" + cuenta + "' \n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
             Messagebox.show("SQL Query For Reparos Pyme[ " + SQL+"]");  
            dsol = jdbcTemplate.query(SQL, new CondonacionMapper3());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones reparadas de pyme para el usuario: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla> GetCondonacionesAprobadas() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut \n"
                + "from condonacion con \n"
                + " inner join regla re on re.id_regla=con.id_regla \n"
                + " inner join estado est on est.di_id_Estado=con.id_estado \n"
                + " inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + " inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + " inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + " where con.id_estado in (" + _estados._buscarIDEstado("PAA") + "," + _estados._buscarIDEstado("AP") + ") \n";

        List<CondonacionTabla> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginated() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PAA : 
        // AP  :
        String estados_considerados = _estados._buscarIDEstado("PAA") + "," + _estados._buscarIDEstado("AP") + "," +_estados._buscarIDEstado("AN-APRO-CAN-PYME");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "     ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "     from (\n"
//                + "     select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "      inner join regla re on re.id_regla=con.id_regla \n"
//                + "      inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "      inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "      inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "      inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "      inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "\n"
//                + "      where con.id_estado in (" + estados_considerados + ")\n"
//                + "      ) as GG\n"
//                + "\n"
//                + "       left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "       group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc \n";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //   + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                  + "		  LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aprobadas de retail.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginatedPm() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PAA : 
        // APRO-EJEPYME  :
        // APRO_ZONAL  :
        String estados_considerados = _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + ","
                + _estados._buscarIDEstado("APRO_ZONAL");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "     ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "     from (\n"
//                + "     select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "      inner join regla re on re.id_regla=con.id_regla \n"
//                + "      inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "      inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "      inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "      inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "      inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "\n"
//                + "      where con.id_estado in (" + estados_considerados + ")\n"
//                + "      ) as GG\n"
//                + "\n"
//                + "       left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "       group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc \n";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                // + "		  LEFT JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aprobadas de pyme.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesAprobadasPaginatedPm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PAA : 
        // APRO-EJEPYME  :
        // APRO_ZONAL  :
        String estados_considerados = _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + ","
                + _estados._buscarIDEstado("APRO_ZONAL");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO, COSORIO
         */
//        String SQL = "SELECT GG.rut,\n" //Cosorio modificación considerando trackin estados de condonación
//                + "       GG.nombre,\n"
//                + "       GG.id_condonacion,\n"
//                + "       GG.timestap,\n"
//                + "       GG.regla,\n"
//                + "       GG.dv_estado,\n"
//                + "       GG.comentario_resna,\n"
//                + "       GG.monto_total_condonado,\n"
//                + "       GG.monto_total_recibit,\n"
//                + "       GG.di_num_opers,\n"
//                + "       GG.monto_total_capital,\n"
//                + "       GG.dv_desc,\n"
//                + "       GG.di_rut,\n"
//                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos'\n"
//                + "FROM\n"
//                + "(\n"
//                + "    SELECT cli.rut,\n"
//                + "           cli.nombre,\n"
//                + "           con.id_condonacion,\n"
//                + "           con.timestap,\n"
//                + "           re.[desc] 'regla',\n"
//                + "           est.dv_estado,\n"
//                + "           con.comentario_resna,\n"
//                + "           con.monto_total_condonado,\n"
//                + "           con.monto_total_recibit,\n"
//                + "           con.di_num_opers,\n"
//                + "           con.monto_total_capital,\n"
//                + "           tpcon.dv_desc,\n"
//                + "           usu.di_rut\n"
//                + "    FROM condonacion con\n"
//                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
//                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
//                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
//                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
//                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
//                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
//                + "	    INNER JOIN dbo.estado_condonacion ec ON con.id_condonacion = ec.di_fk_IdCondonacion\n"
//                + "	    INNER JOIN dbo.trackin_estado te ON EC.di_idEstadoCondonacion = TE.di_fk_idEstadoCondonacion\n"
//                + "    WHERE TE.dv_CunetaOrigen = '" + cuenta + "'\n"
//                + "    AND  con.id_estado IN(" + _estados._buscarIDEstado("PAA") + "," + _estados._buscarIDEstado("APRO_ZONAL") + "," + _estados._buscarIDEstado("APRO-EJEPYME") + ")\n"
//                + "        \n"
//                + ") AS GG\n"
//                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
//                + "GROUP BY GG.rut,\n"
//                + "         GG.nombre,\n"
//                + "         GG.id_condonacion,\n"
//                + "         GG.timestap,\n"
//                + "         GG.regla,\n"
//                + "         GG.dv_estado,\n"
//                + "         GG.comentario_resna,\n"
//                + "         GG.monto_total_condonado,\n"
//                + "         GG.monto_total_recibit,\n"
//                + "         GG.di_num_opers,\n"
//                + "         GG.monto_total_capital,\n"
//                + "         GG.dv_desc,\n"
//                + "         GG.di_rut\n"
//                + "ORDER BY GG.timestap DESC;";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND TE.DV_CUNETAORIGEN = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al listar las condonaciones aprobadas de pyme para el usuario:" + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesCerradasAproZonal(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        String estados_considerados = _estados._buscarIDEstado("APP-APLICADA") + "";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "SELECT GG.rut,\n"
//                + "       GG.nombre,\n"
//                + "       GG.id_condonacion,\n"
//                + "       GG.timestap,\n"
//                + "       GG.regla,\n"
//                + "       GG.dv_estado,\n"
//                + "       GG.comentario_resna,\n"
//                + "       GG.monto_total_condonado,\n"
//                + "       GG.monto_total_recibit,\n"
//                + "       GG.di_num_opers,\n"
//                + "       GG.monto_total_capital,\n"
//                + "       GG.dv_desc,\n"
//                + "       GG.di_rut,\n"
//                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos'\n"
//                + "FROM\n"
//                + "(\n"
//                + "    SELECT cli.rut,\n"
//                + "           cli.nombre,\n"
//                + "           con.id_condonacion,\n"
//                + "           con.timestap,\n"
//                + "           re.[desc] 'regla',\n"
//                + "           est.dv_estado,\n"
//                + "           con.comentario_resna,\n"
//                + "           con.monto_total_condonado,\n"
//                + "           con.monto_total_recibit,\n"
//                + "           con.di_num_opers,\n"
//                + "           con.monto_total_capital,\n"
//                + "           tpcon.dv_desc,\n"
//                + "           usu.di_rut\n"
//                + "    FROM condonacion con\n"
//                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
//                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
//                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
//                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
//                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
//                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
//                + "         INNER JOIN dbo.estado_condonacion ec ON con.id_condonacion = ec.di_fk_IdCondonacion\n"
//                + "         INNER JOIN dbo.trackin_estado te ON EC.di_idEstadoCondonacion = TE.di_fk_idEstadoCondonacion\n"
//                + "    WHERE TE.dv_CunetaOrigen = '" + cuenta + "'\n"
//                + "          AND con.id_estado IN (" + _estados._buscarIDEstado("APP-APLICADA") + ")\n"
//                + ") AS GG\n"
//                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
//                + "GROUP BY GG.rut,\n"
//                + "         GG.nombre,\n"
//                + "         GG.id_condonacion,\n"
//                + "         GG.timestap,\n"
//                + "         GG.regla,\n"
//                + "         GG.dv_estado,\n"
//                + "         GG.comentario_resna,\n"
//                + "         GG.monto_total_condonado,\n"
//                + "         GG.monto_total_recibit,\n"
//                + "         GG.di_num_opers,\n"
//                + "         GG.monto_total_capital,\n"
//                + "         GG.dv_desc,\n"
//                + "         GG.di_rut\n"
//                + "ORDER BY GG.timestap DESC;";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND TE.DV_CUNETAORIGEN = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones aplicadas de pyme aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesCerradasAproAnalista(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        //_estados._buscarIDEstado("APP-APLICADA") +","+
        //para ls bandejas de aprobada analista
        //APP-APLICADA
        String estados_considerados = _estados._buscarIDEstado("APP-APLICADA")+","+_estados._buscarIDEstado("APPLY")+"";
        //_estados._buscarIDEstado("AP")+ ","+_estados._buscarIDEstado("AA")+","+_estados._buscarIDEstado("PAA")+","+_estados._buscarIDEstado("APRO_ZONAL")+","+_estados._buscarIDEstado("APRO-EJEPYME")+","+_estados._buscarIDEstado("AN-APRO-CAN-PYME")+"";
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "SELECT GG.rut,\n"
//                + "       GG.nombre,\n"
//                + "       GG.id_condonacion,\n"
//                + "       GG.timestap,\n"
//                + "       GG.regla,\n"
//                + "       GG.dv_estado,\n"
//                + "       GG.comentario_resna,\n"
//                + "       GG.monto_total_condonado,\n"
//                + "       GG.monto_total_recibit,\n"
//                + "       GG.di_num_opers,\n"
//                + "       GG.monto_total_capital,\n"
//                + "       GG.dv_desc,\n"
//                + "       GG.di_rut,\n"
//                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos'\n"
//                + "FROM\n"
//                + "(\n"
//                + "    SELECT cli.rut,\n"
//                + "           cli.nombre,\n"
//                + "           con.id_condonacion,\n"
//                + "           con.timestap,\n"
//                + "           re.[desc] 'regla',\n"
//                + "           est.dv_estado,\n"
//                + "           con.comentario_resna,\n"
//                + "           con.monto_total_condonado,\n"
//                + "           con.monto_total_recibit,\n"
//                + "           con.di_num_opers,\n"
//                + "           con.monto_total_capital,\n"
//                + "           tpcon.dv_desc,\n"
//                + "           usu.di_rut\n"
//                + "    FROM condonacion con\n"
//                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
//                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
//                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
//                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
//                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
//                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
//                + "         INNER JOIN dbo.estado_condonacion ec ON con.id_condonacion = ec.di_fk_IdCondonacion\n"
//                + "         INNER JOIN dbo.trackin_estado te ON EC.di_idEstadoCondonacion = TE.di_fk_idEstadoCondonacion\n"
//                + "    WHERE TE.dv_CunetaOrigen = '" + cuenta + "'\n"
//                + "          AND con.id_estado IN (" + estados_considerados + ")\n"
//                + ") AS GG\n"
//                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
//                + "GROUP BY GG.rut,\n"
//                + "         GG.nombre,\n"
//                + "         GG.id_condonacion,\n"
//                + "         GG.timestap,\n"
//                + "         GG.regla,\n"
//                + "         GG.dv_estado,\n"
//                + "         GG.comentario_resna,\n"
//                + "         GG.monto_total_condonado,\n"
//                + "         GG.monto_total_recibit,\n"
//                + "         GG.di_num_opers,\n"
//                + "         GG.monto_total_capital,\n"
//                + "         GG.dv_desc,\n"
//                + "         GG.di_rut\n"
//                + "ORDER BY GG.timestap DESC;";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
               // + "        AND TE.DV_CUNETAORIGEN = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        
        
        
       // Messagebox.show(SQL);
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones aplicadas de retail aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    
    
    
       public List<CondonacionTabla2> GetCondonacionesCampañasAproAnalista(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // APP-APLICADA : 
        //_estados._buscarIDEstado("APP-APLICADA") +","+
        //para ls bandejas de aprobada analista
        //APP-APLICADA
        String estados_considerados = ""+_estados._buscarIDEstado("APP-PEND-CAMPANA")+"";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            ISNULL (cedr.cedente,'OTRO') fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "               LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
               // + "        AND TE.DV_CUNETAORIGEN = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        
        
        
     //   Messagebox.show(SQL);
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones aplicadas de retail aprobadas por: " + cuenta + ".\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

  
    
    public List<CondonacionTabla2> GetCondonacionesAprobadas(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PAA : 
        // AP : 
        String estados_considerados = _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("AP");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + _estados._buscarIDEstado("PAA") + "," + _estados._buscarIDEstado("AP") + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA = CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO = CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION = CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID = CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO = COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE = CON.DI_FK_IDCLIENTE \n"
                // + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones aprobadas de retail.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesAprobadasPm(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // se agrega esta variable para visibilizar los estados considerados en la bandeja pendientes del ejecutivo
        // PAA : 
        // APRO-EJEPYME : 
        // APRO_ZONAL : 
        String estados_considerados = _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + ","
                + _estados._buscarIDEstado("APRO_ZONAL")+ ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME");
        /**
         * SE ACTUALIZA QUERY PARA TRAER CEDENTE, FECHA CAMBIO ESTADO. COSORIO
         */
//        String SQL = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
//                + "               ,count(rep.di_fk_IdCondonacion) 'num_reparos' \n"
//                + "               from (\n"
//                + "               select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
//                + "                inner join regla re on re.id_regla=con.id_regla \n"
//                + "                inner join estado est on est.di_id_Estado=con.id_estado \n"
//                + "                inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
//                + "                inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
//                + "                inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
//                + "                inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
//                + "               \n"
//                + "                where con.id_estado in (" + estados_considerados + ") and usu.alias ='" + cuenta + "' \n"
//                + "                ) as GG\n"
//                + "               \n"
//                + "                 left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
//                + "                 group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";

        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    GG.FLD_CED,\n"
                + "    MAX(GG.DDT_FECHAREISTRO) FECHA_CAMBIO_ESTADO\n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced,\n"
                + "	       EC.DDT_FECHAREISTRO\n"
                + "        FROM CONDONACION CON \n"
                + "		  INNER JOIN REGLA RE ON RE.ID_REGLA = CON.ID_REGLA \n"
                + "		  INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO = CON.ID_ESTADO \n"
                + "		  INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION = CON.DI_FK_TIPOCONDONACION \n"
                + "		  INNER JOIN COLABORADOR COLA ON COLA.ID = CON.DI_FK_IDCOLADORADOR  \n"
                + "		  INNER JOIN USUARIO USU ON USU.ID_USUARIO = COLA.ID_USUARIO \n"
                + "		  INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE = CON.DI_FK_IDCLIENTE \n"
                //  + "		  INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "		  INNER JOIN ESTADO_CONDONACION EC ON CON.ID_CONDONACION = EC.DI_FK_IDCONDONACION AND CON.ID_ESTADO = EC.DI_FK_IDESTADO\n"
                + "	          INNER JOIN DBO.TRACKIN_ESTADO TE ON EC.DI_IDESTADOCONDONACION = TE.DI_FK_IDESTADOCONDONACION\n"
                + "	   WHERE EST.DI_ID_ESTADO IN (" + estados_considerados + ")\n"
                + "        AND USU.ALIAS = '" + cuenta + "'\n"
                + "	   \n"
                + "    ) AS GG          \n"
                + "    LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,\n"
                + "    GG.DI_RUT,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY MAX(GG.DDT_FECHAREISTRO) DESC ";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperCambios_de_Estado());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones aprobadas de pyme.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public boolean insertMarca(final MarcaBanco marBan) {
        boolean retorna = false;
        jdbcTemplateObject = new JdbcTemplate(dataSource);

        final String SQL = "INSERT INTO [MarcaBanco]\n"
                + "           ([di_Id_fk_idCliente]\n"
                + ",           [registrado]\n"
                + ",           [db_marcaRechaso]\n"
                + ",           [fk_idCondonacion])"
                + "     VALUES\n"
                + "            (?\n"
                + "            ,(CONVERT(datetime,GETDATE(),120))\n"
                + "            ,?\n"
                + "            ,?)";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_cli_Cond_Det"});
                    ps.setInt(1, marBan.getDi_Id_fk_idCliente());
                    ps.setBoolean(2, marBan.isDb_marcaRechaso());
                    ps.setInt(3, marBan.getFk_idCondonacion());
                    return ps;
                }
            }, key);

            retorna = true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = false;
        }
        return retorna;
    }

    public MarcaBanco selectMarca(int id_marcaBanco) {

        jdbcTemplateObject = new JdbcTemplate(dataSource);

        String SQL = "select * from MarcaBanco where id_marcaBanco = ?";
        MarcaBanco marcBanc = jdbcTemplateObject.queryForObject(SQL, new Object[]{id_marcaBanco}, new RowMapper<MarcaBanco>() {
            public MarcaBanco mapRow(ResultSet rs, int rowNum) throws SQLException {
                MarcaBanco marcBanco = new MarcaBanco();
                marcBanco.setId_marcaBanco(rs.getInt("id_marcaBanco"));
                marcBanco.setDi_Id_fk_idCliente(rs.getInt("di_Id_fk_idCliente"));
                marcBanco.setRegistrado(rs.getDate("registrado"));
                marcBanco.setDb_marcaRechaso(rs.getBoolean("db_marcaRechaso"));
                marcBanco.setFk_idCondonacion(rs.getInt("fk_idCondonacion"));

                return marcBanco;
            }
        });
        return marcBanc;
    }

    public int getSumaInterezPersonalizado(int rutcolaborador, int rutcliente) {
        String SQL = "select isnull(sum(INTRR.valor_actual),0) from usr_modif_interes as INTRR where vigente='True' and fk_idColaborador=" + rutcolaborador + " and rut_usuario=" + rutcliente;
        if (rutcolaborador == 0) {
            return 0;
        }
        int SiNo = 0;

        try {
            //   Messagebox.show("SUMA:["+SQL+"]");
            SiNo = jdbcTemplate.queryForInt(SQL);

            if (SiNo <= 0) {
                return 0;
            }

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
            SiNo = 0;
        }

        return SiNo;

    }

    public boolean SetCondonacionIntoRegla(int id_colaborador, int idcondonacion, int id_regla) {

        try {
            String SQL = "update usr_modif_relvalregla_con set idCondonacion=" + idcondonacion + " where fk_idColaborador = " + id_colaborador + " and fk_idRelValRegla =" + id_regla + " and  vigente='True'";

            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public boolean SetCondonacionIntoReglaV2Decimal(int id_colaborador, int idcondonacion, int id_regla) {

        try {
            String SQL = "update usr_modif_relvalregla_con2 set idCondonacion=" + idcondonacion + " where fk_idColaborador = " + id_colaborador + " and fk_idRelValRegla =" + id_regla + " and  vigente='True'";

            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + idcondonacion);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public List<ConDetOper> getListDetOperCondonacion(int idCondonacion) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select op.tipo_operacion'operacion',detco.estado_operacion'estadoOperacion' from operacion op \n"
                + "inner join detalle_condonacion detco on detco.id_operacion=op.id_operacion \n"
                + " where detco.id_condonacion=" + idCondonacion + " \n";

        List<ConDetOper> _condetoper = null;
        try {
            _condetoper = jdbcTemplate.query(SQL, new ConDetOperMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return _condetoper;
        }
        return _condetoper;
    }

    public int getClienteEnCondonacion(int condonacion) 
    {

        String jjj = "1-9";
        List<Map<String, Object>> ff = null;

        String SQL = "select cli.rut from  condonacion co \n"
                + "   inner join cliente cli on cli.id_cliente=co.di_fk_IdCliente \n"
                + "where co.id_condonacion=" + condonacion + " \n";
        int rut = 0;

        try {
            ff = jdbcTemplate.queryForList(SQL);

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
            rut = -1;
        }
        jjj = ff.get(0).values().toString();
        String mm = jjj.replace(".", "");
        String mm2 = mm.replace("[", "");
        String mm3 = mm2.replace("]", "");
        String[] ParteEntera = mm3.split("-");
        return Integer.parseInt(ParteEntera[0]);

    }
    public String getClienteEnCondonacionString(int condonacion) 
    {

        String jjj = "1-9";
        List<Map<String, Object>> ff = null;

        String SQL = "select cli.rut from  condonacion co \n"
                + "   inner join cliente cli on cli.id_cliente=co.di_fk_IdCliente \n"
                + "where co.id_condonacion=" + condonacion + " \n";
        int rut = 0;

        try {
            ff = jdbcTemplate.queryForList(SQL);

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
            rut = -1;
        }
        jjj = ff.get(0).values().toString();
        String mm = jjj.replace(".", "");
        String mm2 = mm.replace("[", "");
        String mm3 = mm2.replace("]", "");
        //String[] ParteEntera = mm3.split("-");
        return mm3;

    }
    
     public int getEjecutivoEnCondonacionInterger(int condonacion) 
    {

        String jjj = "1-9";
        List<Map<String, Object>> ff = null;

        String SQL = "  	 	  select usu.dv_rut from condonacion con\n" +
"	 inner join colaborador col on col.id=con.di_fk_idColadorador\n" +
"	 inner join usuario usu on usu.id_usuario=col.id_usuario\n" +
"	 where con.id_condonacion=\n" +
"    " + condonacion + " \n";
        int rut = 0;

        try {
            ff = jdbcTemplate.queryForList(SQL);

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
            rut = -1;
        }
        jjj = ff.get(0).values().toString();
        String mm = jjj.replace(".", "");
        String mm2 = mm.replace("[", "");
        String mm3 = mm2.replace("]", "");
        //String[] ParteEntera = mm3.split("-");
        //return mm3;
        String[] ParteEntera = mm3.split("-");
        return Integer.parseInt(ParteEntera[0]);
    }
    
    public int insertUsrProvision(final int id_colaborador, final int idCondonacion, final float monto, String dv_desc, final String rutcliente) {
        int id_provision;
        final String desc = "prueba";
        final String SQL = "INSERT INTO usr_provision\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[rut_cliente]\n"
                + "           ,[nfk_idCondonacion]\n"
                + "           ,[registrado]\n"
                + "           ,[dv_desc]\n"
                + "           ,[monto] \n"
                + "           ,[fk_idTipoProvision]\n"
                + "           ,[vigente] )\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            Messagebox.show(SQL);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setString(2, rutcliente);
                    ps.setInt(3, idCondonacion);
                    ps.setString(4, desc);
                    ps.setFloat(5, monto);
                    ps.setInt(6, 1);
                    ps.setInt(7, 1);
                    return ps;
                }
            },
                    keyHolder);

            id_provision = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            Messagebox.show("Error Guardando provision: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_provision = 0;

        }

        return id_provision;
    }

    public int insertUsrProvision(final int id_colaborador, final int idCondonacion,
            final float monto, String dv_desc,
            final String rutcliente, final int diid) {

        int id_provision;
        String defaul = "prueba";

        if (dv_desc != null) {
            if (!dv_desc.equals("")) {
                defaul = dv_desc;
            }
        }

        final String desc = defaul;

        jdbcTemplate = new JdbcTemplate(dataSource);
        //  dv_desc="sadad";
        final String SQL = "INSERT INTO usr_provision\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[rut_cliente]\n"
                + "           ,[nfk_idCondonacion]\n"
                + "           ,[registrado]\n"
                + "           ,[dv_desc]\n"
                + "           ,[monto] \n"
                + "           ,[fk_idTipoProvision]\n"
                + "           ,[vigente] )\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            // Messagebox.show(SQL);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setString(2, rutcliente);
                    ps.setInt(3, idCondonacion);
                    ps.setString(4, desc);
                    ps.setFloat(5, monto);
                    ps.setInt(6, diid);
                    ps.setInt(7, 1);
                    return ps;
                }
            },
                    keyHolder);

            id_provision = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            Messagebox.show("Error Guardando provision: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_provision = 0;

        }

        return id_provision;
    }

    public AdjuntarENC GetAdjuntoCond(int id_cond) {
        AdjuntarENC aEnc = new AdjuntarENC();
        aEnc.setFk_idCondonacion(id_cond);
        aEnc = ai.api(aEnc, 2) != null ? ai.api(aEnc, 2) : null;

        return aEnc;
    }

    public GlosaENC GetGlosaCond(int id_cond) {
        GlosaENC gEnc = new GlosaENC();
        gEnc.setId_cond(id_cond);
        gEnc = gi.api(gEnc, 2) != null ? gi.api(gEnc, 2) : null;

        return gEnc;
    }

    public List<Reparos> GetReparosXcondonacion(int id_condonacion, String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        // String SQL = " select * from reparo rep where rep.di_fk_IdCondonacion ='" + id_condonacion + "' \n";
        final String SQL = "select UU.*,'" + cuenta + "' 'cuenta' from ( select rep.*,usu.alias,col.nombres,at.cod from reparo rep \n"
                + "           inner join colaborador col on (col.id=rep.di_fk_IdColaborador) \n"
                + "           inner join usuario usu on (usu.id_usuario=col.id_usuario)\n"
                + "           inner join AreaTrabajo at on (at.id=col.fk_di_id_AreaTrabajo) \n"
                + "           where rep.di_fk_IdCondonacion ='" + id_condonacion + "'  ) as UU  order by UU.registrado desc \n";
        //   Messagebox.show("Query["+SQL+"]");
        List<Reparos> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new ReparosMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<Reparos> GetReparosXcondonacion_ZonalPyme(int id_condonacion) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        final String SQL = "SELECT UU.*,\n"
                + "       'ZONALSISCONPYME' cuenta\n"
                + "FROM\n"
                + "(\n"
                + "    SELECT rep.*,\n"
                + "           usu.alias,\n"
                + "           col.nombres,\n"
                + "           at.cod\n"
                + "    FROM reparo rep\n"
                + "         INNER JOIN colaborador col ON(col.id = rep.di_fk_IdColaborador)\n"
                + "         INNER JOIN usuario usu ON(usu.id_usuario = col.id_usuario)\n"
                + "         INNER JOIN AreaTrabajo at ON(at.id = col.fk_di_id_AreaTrabajo)\n"
                + "    WHERE rep.di_fk_IdCondonacion = " + id_condonacion + "\n"
                + ") AS UU\n"
                + "ORDER BY UU.registrado DESC; \n";
        List<Reparos> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new ReparosMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<Reparos> GetReparosXcondonacion(int id_condonacion) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        final String SQL = " select rep.*,usu.alias,col.nombres,at.cod,'EJESICON'cuenta from reparo rep \n"
                + "           inner join colaborador col on (col.id=rep.di_fk_IdColaborador) \n"
                + "           inner join usuario usu on (usu.id_usuario=col.id_usuario)\n"
                + "           inner join AreaTrabajo at on (at.id=col.fk_di_id_AreaTrabajo) \n"
                + "           where rep.di_fk_IdCondonacion ='" + id_condonacion + "' order by registrado desc   \n";
        List<Reparos> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new ReparosMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public Boolean GetUltimoReparosXcondonacion(int id_condonacion, String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        int _UltimoReparoEjecutivo = 0;
        // String SQL = " select * from reparo rep where rep.di_fk_IdCondonacion ='" + id_condonacion + "' \n";
        final String SQL = "select count(*) from (\n"
                + "select top 1 usu.* from reparo rep \n"
                + "                   inner join colaborador col on (col.id=rep.di_fk_IdColaborador) \n"
                + "                   inner join usuario usu on (usu.id_usuario=col.id_usuario)\n"
                + "                   where rep.di_fk_IdCondonacion ='" + id_condonacion + "' \n"
                + "				   order by registrado desc \n"
                + "\n"
                + "				   ) as HH \n"
                + "\n"
                + "where HH.alias='" + cuenta + "'";

        try {
            //   dsol = jdbcTemplate.query(SQL, new ReparosMapper());

            _UltimoReparoEjecutivo = jdbcTemplate.queryForInt(SQL);

        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return false;
        }
        return _UltimoReparoEjecutivo > 0;
    }

    public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacion(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                   + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "          LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") +  ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
              //  + "where HH.fld_ced not in ('MYPE')\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
         //    Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }
 public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacion_old(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "       LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced not in ('MYPE')\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
         //    Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

        
           public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacionBtnPgn(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "        LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE usu.alias='bot-btnPago' and con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                 + _estados._buscarIDEstado("BOT-APP-PEND") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced not in ('MYPE')\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
           //  Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

   public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacionBtnPgnV3(String cuenta,String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        //periodo="2020-11-26";
        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "        LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE usu.alias='bot-btnPago' and con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                 + _estados._buscarIDEstado("BOT-APP-PEND") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced not in ('MYPE')\n"
                //+ "and HH.timestap >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'),101)  and HH.timestap <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'\" + periodo + \"'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101 \n"//+CONVERT(datetime,+'00:00:00.000',101)  
                //+ "and HH.timestap >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and HH.timestap <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101)  \n"
                + "and HH.timestap >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'),101)+CONVERT(datetime,+'00:00:00.000',101)    and HH.timestap <= CONVERT(datetime,+CONVERT (date, dateadd(day,+1,CONVERT(datetime,+CONVERT (date,'" + periodo + "'),101)+CONVERT(datetime,+'00:00:00.000',101))),101)+CONVERT(datetime,+'00:00:00.000',101)  \n"
                + "ORDER BY HH.timestap DESC";                                                                                                                                           

        List<CondonacionTabla2> dsol = null;
        try {
             //Messagebox.show("Query : ["+SQL+"]");
            System.out.println("GetCondonacionesPendientesDeAplicacionBtnPgnV3---> "+SQL); 
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }
    
    
     public List<CondonacionesAPP> GetCondonacionesPendientesDeAplicacionBtnPgnV2(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "	                             SELECT\n" +
"                                                     cli.rut\n" +
"                                                   , con.id_condonacion\n" +
"                                                   , con.timestap\n" +
"                                                   , est.dv_estado\n" +
"                                                   , con.comentario_resna\n" +
"                                                   , con.monto_total_recibit\n" +
"                                                   , con.monto_total_capital\n" +
"                                                  , tpcon.dv_desc\n" +
"                                          FROM\n" +
"                                                     condonacion con\n" +
"                                                     INNER JOIN  estado est    ON  est.di_id_Estado = con.id_estado\n" +
"                                                     INNER JOIN   tipo_condonacion tpcon     ON   tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n" +
"                                                     INNER JOIN    colaborador cola        ON    cola.id = con.di_fk_idColadorador\n" +
"                                                     INNER JOIN   usuario usu    ON   usu.id_usuario = cola.id_usuario\n" +
"                                                     INNER JOIN  cliente cli   ON   cli.id_cliente = con.di_fk_IdCliente\n" +
"                \n" +
"                                          WHERE   usu.alias='bot-btnPago'      and con.id_estado in ('2','10','21','24' ,'32' ,'33' ,'35' ,'37' ,'38' ,'40')"
                + "ORDER BY timestap DESC";

        List<CondonacionesAPP> dsol = null;
        try {
             //Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL,new BeanPropertyRowMapper(CondonacionesAPP.class));
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

   
    
    
    
    
        public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacion(String cuenta,String cedente) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                 + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "          LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced like '%"+cedente+"%'\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            // Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

   
    
    
    
    
        
        public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacion_old(String cuenta,String cedente) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "          LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1))) \n"
                + "    WHERE con.id_estado in (" + _estados._buscarIDEstado("EJE-GES-PRO") + ","
                + _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                + _estados._buscarIDEstado("AP") + ","
                + _estados._buscarIDEstado("PAA") + ","
                + _estados._buscarIDEstado("APRO_ZONAL") + ","
                + _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                + _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced like '%"+cedente+"%'\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            // Messagebox.show("Query : ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

   
    
    
    
    
    
    
    
    public List<CondonacionTabla2> GetCondonacionesPendientesDeAplicacionCamp(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "select * from (\n"
                + "SELECT GG.rut,\n"
                + "       GG.nombre,\n"
                + "       GG.id_condonacion,\n"
                + "       GG.timestap,\n"
                + "       GG.regla,\n"
                + "       GG.dv_estado,\n"
                + "       GG.comentario_resna,\n"
                + "       GG.monto_total_condonado,\n"
                + "       GG.monto_total_recibit,\n"
                + "       GG.di_num_opers,\n"
                + "       GG.monto_total_capital,\n"
                + "       GG.dv_desc,\n"
                + "       GG.di_rut,\n"
                + "       COUNT(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "       GG.fld_ced \n"
                + "FROM\n"
                + "(\n"
                + "    SELECT cli.rut,\n"
                + "           cli.nombre,\n"
                + "           con.id_condonacion,\n"
                + "           con.timestap,\n"
                + "           re.[desc] 'regla',\n"
                + "           est.dv_estado,\n"
                + "           con.comentario_resna,\n"
                + "           con.monto_total_condonado,\n"
                + "           con.monto_total_recibit,\n"
                + "           con.di_num_opers,\n"
                + "           con.monto_total_capital,\n"
                + "           tpcon.dv_desc,\n"
                + "           usu.di_rut,\n"
                //   + "           cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "           ISNULL (cedr.cedente,'OTRO') fld_ced  \n"
                + "    FROM condonacion con\n"
                + "         INNER JOIN regla re ON re.id_regla = con.id_regla\n"
                + "         INNER JOIN estado est ON est.di_id_Estado = con.id_estado\n"
                + "         INNER JOIN tipo_condonacion tpcon ON tpcon.di_idtipoCondonacion = con.di_fk_tipocondonacion\n"
                + "         INNER JOIN colaborador cola ON cola.id = con.di_fk_idColadorador\n"
                + "         INNER JOIN usuario usu ON usu.id_usuario = cola.id_usuario\n"
                + "         INNER JOIN cliente cli ON cli.id_cliente = con.di_fk_IdCliente\n"
                // + "         INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "        LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))  \n"
                + "    WHERE con.id_estado in (" 
                //+ _estados._buscarIDEstado("EJE-GES-PRO") + ","
                //+ _estados._buscarIDEstado("EJEPM-GES-PRO") + ","
                //+ _estados._buscarIDEstado("AP") + ","
                //+ _estados._buscarIDEstado("PAA") + ","
                //+ _estados._buscarIDEstado("APRO_ZONAL") + ","
                //+ _estados._buscarIDEstado("AN-APRO-CAN-PYME") + ","
                //+ _estados._buscarIDEstado("APRO-EJEPYME") + "\n"+ ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA") + ","
                + _estados._buscarIDEstado("APP-PEND-CAMPANA-PYME") + ")\n"
                + ") AS GG\n"
                + "LEFT JOIN reparo rep ON rep.di_fk_IdCondonacion = GG.id_condonacion\n"
                + "GROUP BY GG.rut,\n"
                + "         GG.nombre,\n"
                + "         GG.id_condonacion,\n"
                + "         GG.timestap,\n"
                + "         GG.regla,\n"
                + "         GG.dv_estado,\n"
                + "         GG.comentario_resna,\n"
                + "         GG.monto_total_condonado,\n"
                + "         GG.monto_total_recibit,\n"
                + "         GG.di_num_opers,\n"
                + "         GG.monto_total_capital,\n"
                + "         GG.dv_desc,\n"
                + "         GG.di_rut,\n"
                + "         GG.fld_ced\n"
                + "\n"
                + ") as HH \n"
                //   + "where HH.fld_ced not in ('NOVA','MYPE')\n"
                + "where HH.fld_ced not in ('MYPE')\n"
                + "ORDER BY HH.timestap DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            //Messagebox.show("Query  Campana: ["+SQL+"]dfsdfsd");
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }


    
    
    public List<CondonacionTabla2> GetCondonacionesAplicadas(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL1 = "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + " ,count(rep.di_fk_IdCondonacion) 'num_reparos', cast((select top 1 * from [dbo].[Concatena_CED] (GG.rut)) as varchar)  as  fld_ced\n"
                + " from (\n"
                + " select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + "  inner join regla re on re.id_regla=con.id_regla \n"
                + "  inner join estado est on est.di_id_Estado=con.id_estado \n"
                + "  inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "  inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "  inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "  inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                // + "  INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + " \n"
                + "  where con.id_estado in (" + _estadoAplicadas + ")\n"
                + "  ) as GG\n"
                + " \n"
                + "   left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "   group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut order by GG.timestap desc";
      
        String SQL = " select\n" +
"GG.rut,GG.nombre,\n" +
"GG.id_condonacion,\n" +
"GG.timestap,\n" +
"GG.regla,\n" +
"GG.dv_estado,\n" +
"GG.comentario_resna,\n" +
"GG.monto_total_condonado,\n" +
"GG.monto_total_recibit,\n" +
"GG.di_num_opers,\n" +
"GG.monto_total_capital,\n" +
"GG.dv_desc\n" +
",GG.di_rut\n" +
",count(rep.di_fk_IdCondonacion) 'num_reparos'\n" +
", GG.fld_ced\n" +
"from (\n" +
"select cli.rut,\n" +
"cli.nombre,\n" +
"con.id_condonacion,\n" +
"con.timestap,\n" +
"re.[desc] 'regla',\n" +
"est.dv_estado,\n" +
"con.comentario_resna,\n" +
"con.monto_total_condonado,\n" +
"con.monto_total_recibit,\n" +
"con.di_num_opers,\n" +
"con.monto_total_capital,\n" +
"tpcon.dv_desc,usu.di_rut ,\n" +
" ISNULL (cedr.cedente,'OTRO') fld_ced\n" +
"from condonacion con\n" +
"inner join regla re on re.id_regla=con.id_regla\n" +
"inner join estado est on est.di_id_Estado=con.id_estado\n" +
"inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n" +
"inner join colaborador cola on cola.id=con.di_fk_idColadorador\n" +
"inner join usuario usu on usu.id_usuario=cola.id_usuario\n" +
"inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente\n" +
"LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1)))\n" +
"where con.id_estado in (" + _estadoAplicadas + ")\n" +
") as GG\n" +
"\n" +
"left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n" +
"group by\n" +
"GG.rut,\n" +
"GG.nombre,\n" +
"GG.id_condonacion,\n" +
"GG.timestap,\n" +
"GG.regla,\n" +
"GG.dv_estado,\n" +
"GG.comentario_resna,\n" +
"GG.monto_total_condonado,\n" +
"GG.monto_total_recibit,\n" +
"GG.di_num_opers,\n" +
"GG.monto_total_capital,\n" +
"GG.dv_desc,\n" +
"GG.di_rut,\n" +
"GG.fld_ced\n" +
"order by\n" +
"GG.timestap desc";
        
       // Messagebox.show(SQL);
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApply());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<BandejaCondonacionesInforme> GetCondonacionesAplicadasForContab(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select tmc.fld_num_ofi 'numero_oficina',tmc.fld_ofi 'desc_oficina',UU.* from (\n"
                + "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + "                 ,count(rep.di_fk_IdCondonacion) 'num_reparos', cast((select top 1 * from [dbo].[Concatena_CED] (GG.rut)) as varchar)  as  fld_ced\n"
                + "                 from (\n"
                + "                 select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + "                  inner join regla re on re.id_regla=con.id_regla \n"
                + "                  inner join estado est on est.di_id_Estado=con.id_estado \n"
                + "                  inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "                  inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "                  inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "                  inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + "                 \n"
                + "                 \n"
                + "                  where con.id_estado in (" + _estadoAplicadas + ")\n"
                + "                  ) as GG\n"
                + "                 \n"
                + "                   left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                   group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut \n"
                + "\n"
                + "				   ) as UU\n"
                + "				    INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n"
                + "   where  timestap >= CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'00:00:00.000',101)   and timestap <=  CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'23:59:59.000',101) \n"
                + "					order by timestap desc";

        List<BandejaCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperContab());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<BandejaCondonacionesInforme> GetCondonacionesAplicadasForContab(String cuenta, String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select tmc.fld_num_ofi 'numero_oficina',tmc.fld_ofi 'desc_oficina',UU.* from (\n"
                + "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + "                 ,count(rep.di_fk_IdCondonacion) 'num_reparos', cast((select top 1 * from [dbo].[Concatena_CED] (GG.rut)) as varchar)  as  fld_ced\n"
                + "                 from (\n"
                + "                 select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + "                  inner join regla re on re.id_regla=con.id_regla \n"
                + "                  inner join estado est on est.di_id_Estado=con.id_estado \n"
                + "                  inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "                  inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "                  inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "                  inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + "                 \n"
                + "                 \n"
                + "                  where con.id_estado in (" + _estadoAplicadas + ")\n"
                + "                  ) as GG\n"
                + "                 \n"
                + "                   left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                   group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut \n"
                + "\n"
                + "				   ) as UU\n"
                + "				    INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n"
                + "where   timestap >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and timestap <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101)  \n"
                + "					order by timestap desc";

        List<BandejaCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperContab());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<BandejaCondonacionesInforme> GetCondonacionesAplicadasForContabV2(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select tmc.fld_num_ofi 'numero_oficina',tmc.fld_ofi 'desc_oficina',UU.* from (\n"
                + "select GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut\n"
                + "                 ,count(rep.di_fk_IdCondonacion) 'num_reparos', cast((select top 1 * from [dbo].[Concatena_CED] (GG.rut)) as varchar)  as  fld_ced\n"
                + "                 from (\n"
                + "                 select cli.rut,cli.nombre,con.id_condonacion,con.timestap,re.[desc] 'regla',est.dv_estado,con.comentario_resna,con.monto_total_condonado,con.monto_total_recibit,con.di_num_opers,con.monto_total_capital,tpcon.dv_desc,usu.di_rut from condonacion con \n"
                + "                  inner join regla re on re.id_regla=con.id_regla \n"
                + "                  inner join estado est on est.di_id_Estado=con.id_estado \n"
                + "                  inner join tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "                  inner join colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "                  inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "                  inner join cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + "                 \n"
                + "                 \n"
                + "                  where con.id_estado in (" + _estadoAplicadas + ")\n"
                + "                  ) as GG\n"
                + "                 \n"
                + "                   left join reparo rep on rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                   group by GG.rut,GG.nombre,GG.id_condonacion,GG.timestap,GG.regla,GG.dv_estado,GG.comentario_resna,GG.monto_total_condonado,GG.monto_total_recibit,GG.di_num_opers,GG.monto_total_capital,GG.dv_desc,GG.di_rut \n"
                + "\n"
                + "				   ) as UU\n"
                + "				    INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n"
                + "					order by timestap desc";

        List<BandejaCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperContab());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInforme(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n"
                + "           OO.*\n"
                + "         , ff.[SALDO_TOTAL]\n"
                + "         , ff.[%_COND_CAP]\n"
                + "         , ff.[SALDO_CONDONADO]\n"
                + "         , ff.[%_PAGO_CAP]\n"
                + "         , ff.[SALDO_PAGO]\n"
                + "         , ff.[INTERES]\n"
                + "         , ff.[%_COND_INT]\n"
                + "         , ff.[INT_CONDONADO]\n"
                + "         , ff.[%_PAGO_INT]\n"
                + "         , ff.[INT_PAGADO]\n"
                + "         , ff.[%_PAGO_HON]\n"
                + "         , ff.[HON_PAGO]\n"
                + "         , ff.[MONTO_A_RECIBIR]\n"
                + "         , ff.[VDE_SGN]\n"
                + "         , ff.[PROVICION]\n"
                + "         , ff.[MONTO_TOTAL_PAGO]\n"
                + "          , concat (OO.[Demora-dias],' Dias = ',OO.[Demora-horas],' Horas = ',OO.[Demora-segundos],' Seg') 'KPI'\n"
                + "from\n"
                + "           (\n"
                + "                  select\n"
                + "                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n"
                + "                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n"
                + "                       , DATEDIFF(week, tiempo_aprobacion,tiempo_aplicacion)'Demora-dias'\n"
                + "                       , LL.*\n"
                + "                  from\n"
                + "                         (\n"
                + "                                    select\n"
                + "                                               (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado in ('Aprobado'\n"
                + "                                                                                         ,'Aprob Analista'\n"
                + "                                                                                         ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'tiempo_aprobacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado       ='Aplicada Aplicacion'\n"
                + "                                               )\n"
                + "                                               'tiempo_aplicacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(max(te.dv_CunetaOrigen),'ONLINE')\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and es.dv_estado in ('Aprobado'\n"
                + "                                                                                        ,'Aprob Analista'\n"
                + "                                                                                        ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'Aprovado_Por'\n"
                + "                                             , tmc.fld_num_ofi 'numero_oficina'\n"
                + "                                             , tmc.fld_ofi 'desc_oficina'\n"
                + "                                             , (\n"
                + "                                                      select\n"
                + "                                                             min(mop.fld_fec_cas)\n"
                + "                                                      from\n"
                + "                                                             tracking_morahoy_ope mop\n"
                + "                                                      where\n"
                + "                                                             mop.fk_idTrkMorClie=tmc.id\n"
                + "                                               )\n"
                + "                                               'Minima Fecha Castigo'\n"
                + "                                             , UU.monto_total_recibit\n"
                + "                                             , UU.fld_ced\n"
                + "                                             , UU.id_condonacion\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(min(te.dv_CunetaOrigen), (\n"
                + "                                                                                select\n"
                + "                                                                                           min(us.alias)\n"
                + "                                                                                from\n"
                + "                                                                                           condonacion cooo\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      colaborador colll\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 colll.id=cooo.di_fk_idColadorador\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      usuario us\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 us.id_usuario=colll.id_usuario\n"
                + "                                                                                where\n"
                + "                                                                                           cooo.id_condonacion=UU.id_condonacion\n"
                + "                                                                     )\n"
                + "                                                                     )\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and\n"
                + "                                                                     (\n"
                + "                                                                                te.dv_estadoInicio in ('Estado.Nueva'\n"
                + "                                                                                                     ,'Estado.Nuevo')\n"
                + "                                                                                or te.dv_ModuloOrigen = 'Ejecutivo.Reparadas'\n"
                + "                                                                     )\n"
                + "                                               )\n"
                + "                                               'Ejecutivo Origen'\n"
                + "                                    from\n"
                + "                                               (\n"
                + "                                                         select\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                                                 , count(rep.di_fk_IdCondonacion) 'num_reparos'\n"
                + "                                                                 , cast(\n"
                + "                                                                         (\n"
                + "                                                                                select\n"
                + "                                                                                       top 1 *\n"
                + "                                                                                from\n"
                + "                                                                                       [dbo].[Concatena_CED] (GG.rut)\n"
                + "                                                                        )\n"
                + "                                                                        as varchar) as fld_ced\n"
                + "                                                         from\n"
                + "                                                                   (\n"
                + "                                                                              select\n"
                + "                                                                                         cli.rut\n"
                + "                                                                                       , cli.nombre\n"
                + "                                                                                       , con.id_condonacion\n"
                + "                                                                                       , con.timestap\n"
                + "                                                                                       , re.[desc] 'regla'\n"
                + "                                                                                       , est.dv_estado\n"
                + "                                                                                       , con.comentario_resna\n"
                + "                                                                                       , con.monto_total_condonado\n"
                + "                                                                                       , con.monto_total_recibit\n"
                + "                                                                                       , con.di_num_opers\n"
                + "                                                                                       , con.monto_total_capital\n"
                + "                                                                                       , tpcon.dv_desc\n"
                + "                                                                                       , usu.di_rut\n"
                + "                                                                              from\n"
                + "                                                                                         condonacion con\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    regla re\n"
                + "                                                                                                    on\n"
                + "                                                                                                               re.id_regla=con.id_regla\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    estado est\n"
                + "                                                                                                    on\n"
                + "                                                                                                               est.di_id_Estado=con.id_estado\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    tipo_condonacion tpcon\n"
                + "                                                                                                    on\n"
                + "                                                                                                               tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    colaborador cola\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cola.id=con.di_fk_idColadorador\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    usuario usu\n"
                + "                                                                                                    on\n"
                + "                                                                                                               usu.id_usuario=cola.id_usuario\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    cliente cli\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cli.id_cliente=con.di_fk_IdCliente\n"
                + "                                                                              where\n"
                + "                                                                                         con.id_estado in\n"
                + "                                                                                         (\n"
                + "                                                                                                select\n"
                + "                                                                                                       est.di_id_Estado\n"
                + "                                                                                                from\n"
                + "                                                                                                       estado est\n"
                + "                                                                                                where\n"
                + "                                                                                                       est.dv_estado ='Aplicada Aplicacion'\n"
                + "                                                                                         )\n"
                + "                                                                   )\n"
                + "                                                                   as GG\n"
                + "                                                                   left join\n"
                + "                                                                             reparo rep\n"
                + "                                                                             on\n"
                + "                                                                                       rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                                                         group by\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                               )\n"
                + "                                               as UU\n"
                + "                                               INNER JOIN\n"
                + "                                                          dbo.traking_morahoy_clie tmc\n"
                + "                                                          ON\n"
                + "                                                                     id_condonacion = tmc.fk_idCondonacion\n"
                + "                         )\n"
                + "                         as LL\n"
                + "                  where\n"
                + "                         LL.tiempo_aplicacion is not null\n"
                + "           )\n"
                + "           as OO\n"
                + "           inner join\n"
                + "                      tbl_siscon_condonaciones as ff\n"
                + "                      on\n"
                + "                                 ff.id_condonacion=OO.id_condonacion"
                + "   where fld_ced = 'BCI'   and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'00:00:00.000',101)   and tiempo_aplicacion <=  CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'23:59:59.000',101) ";

        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    
    
        public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInformeV2(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n" +
"                           OO.*\n" +
"                         , ff.[SALDO_TOTAL]\n" +
"                         , ff.[%_COND_CAP]\n" +
"                         , ff.[SALDO_CONDONADO]\n" +
"                         , ff.[%_PAGO_CAP]\n" +
"                         , ff.[SALDO_PAGO]\n" +
"                         , ff.[INTERES]\n" +
"                         , ff.[%_COND_INT]\n" +
"                         , ff.[INT_CONDONADO]\n" +
"                         , ff.[%_PAGO_INT]\n" +
"                         , ff.[INT_PAGADO]\n" +
"                         , ff.[%_PAGO_HON]\n" +
"                         , ff.[HON_PAGO]\n" +
"                         , ff.[MONTO_A_RECIBIR]\n" +
"                         , ff.[VDE_SGN]\n" +
"                         , ff.[PROVICION]\n" +
"                         , ff.[MONTO_TOTAL_PAGO]\n" +
"                          , concat (OO.[Demora-dias],' Dias  ') 'KPI'\n" +
"					    ,  FORMAT(tiempo_aplicacion,'yyyy-MM') 'tapsssp'\n" +
"						 \n" +
"                from\n" +
"                           (\n" +
" \n" +
"\n" +
"\n" +
"                                 select\n" +
"                                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n" +
"                                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n" +
//"                                       , convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24)  'Demora-dias'\n" +
"                                       , (convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24) - (DATEDIFF(WK,tiempo_aprobacion,tiempo_aplicacion)* 2) )  'Demora-dias'\n" +                
"                                       , LL.*\n" +
"                                  from\n" +
"                                         (\n" +
"                                                    select\n" +
"                                                               (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado in ('Aprobado' ,'Aprob Analista','Aprobada Zonal')) 'tiempo_aprobacion'\n" +
"                                                             , (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc  inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado   ='Aplicada Aplicacion') 'tiempo_aplicacion'\n" +
"                                                             , UU.aprovado_por   'Aprovado_Por'\n" +
"                                                             , tmc.fld_num_ofi 'numero_oficina'\n" +
"                                                             , tmc.fld_ofi 'desc_oficina'\n" +
"                                                             , (select    min(mop.fld_fec_cas)  from   tracking_morahoy_ope mop   where  mop.fk_idTrkMorClie=tmc.id )  'Minima Fecha Castigo'\n" +
"                                                             , UU.total_capital_recibe  'monto_total_recibit'  \n" +
"                                                             , UU.cedente       'fld_ced'          \n" +
"                                                             , UU.id_condonacion       'id_condonacion'     \n" +
"                                                             ,UU.ejecutivo_condona          'Ejecutivo Origen'\n" +
"                                                    from     siscon_inf_clienteV2   as UU\n" +
"                                                               INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n" +
"                                         )\n" +
"                                         as LL\n" +
"                                  where\n" +
"                                         LL.tiempo_aplicacion is not null\n" +
"                           )\n" +
"                           as OO\n" +
"                           inner join\n" +
"                                      tbl_siscon_condonaciones as ff\n" +
"                                      on\n" +
"                                                 ff.id_condonacion=OO.id_condonacion\n" +
"where  fld_ced not like '%NOVA%' and  tiempo_aplicacion >= ( CONVERT(datetime,+CONVERT (date, FORMAT(getdate(),'yyyy-MM') +'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)) ";

        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    
        //// se Agrega el rut_cliente 
        public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInformeV3(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n" +
"                           OO.*\n" +
"                         , ff.[SALDO_TOTAL]\n" +
"                         , ff.[%_COND_CAP]\n" +
"                         , ff.[SALDO_CONDONADO]\n" +
"                         , ff.[%_PAGO_CAP]\n" +
"                         , ff.[SALDO_PAGO]\n" +
"                         , ff.[INTERES]\n" +
"                         , ff.[%_COND_INT]\n" +
"                         , ff.[INT_CONDONADO]\n" +
"                         , ff.[%_PAGO_INT]\n" +
"                         , ff.[INT_PAGADO]\n" +
"                         , ff.[%_PAGO_HON]\n" +
"                         , ff.[HON_PAGO]\n" +
"                         , ff.[MONTO_A_RECIBIR]\n" +
"                         , ff.[VDE_SGN]\n" +
"                         , ff.[PROVICION]\n" +
"                         , ff.[MONTO_TOTAL_PAGO]\n" +
"                          , concat (OO.[Demora-dias],' Dias  ') 'KPI'\n" +
"					    ,  FORMAT(tiempo_aplicacion,'yyyy-MM') 'tapsssp'\n" +
"						 \n" +
"                from\n" +
"                           (\n" +
" \n" +
"\n" +
"\n" +
"                                 select\n" +
"                                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n" +
"                                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n" +
//"                                       , convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24)  'Demora-dias'\n" +
"                                       , (convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24) - (DATEDIFF(WK,tiempo_aprobacion,tiempo_aplicacion)* 2) )  'Demora-dias'\n" +                
"                                       , LL.*\n" +
"                                  from\n" +
"                                         (\n" +
"                                                    select\n" +
"                                                               (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado in ('Aprobado' ,'Aprob Analista','Aprobada Zonal')) 'tiempo_aprobacion'\n" +
"                                                             , (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc  inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado   ='Aplicada Aplicacion') 'tiempo_aplicacion'\n" +
"                                                             , UU.aprovado_por   'Aprovado_Por'\n" +
"                                                             , tmc.fld_num_ofi 'numero_oficina'\n" +
"                                                             , tmc.fld_ofi 'desc_oficina'\n" +
"                                                             , (select    min(mop.fld_fec_cas)  from   tracking_morahoy_ope mop   where  mop.fk_idTrkMorClie=tmc.id )  'Minima Fecha Castigo'\n" +
"                                                             , UU.total_capital_recibe  'monto_total_recibit'  \n" +
"                                                             , UU.cedente       'fld_ced'          \n" +
"                                                             , UU.id_condonacion       'id_condonacion'     \n" +
"                                                             ,UU.ejecutivo_condona          'Ejecutivo Origen'\n" +
"                                                             ,tmc.fld_rut_ent 'rut_cliente'  \n"+
"                                                    from     siscon_inf_clienteV2   as UU\n" +
"                                                               INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n" +
"                                         )\n" +
"                                         as LL\n" +
"                                  where\n" +
"                                         LL.tiempo_aplicacion is not null\n" +
"                           )\n" +
"                           as OO\n" +
"                           inner join\n" +
"                                      tbl_siscon_condonaciones as ff\n" +
"                                      on\n" +
"                                                 ff.id_condonacion=OO.id_condonacion\n" +
"where  fld_ced not like '%NOVA%' and  tiempo_aplicacion >= ( CONVERT(datetime,+CONVERT (date, FORMAT(getdate(),'yyyy-MM') +'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)) ";

        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

        /// Se agrega el rut_cliente
        public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInformeV3(String cuenta, String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n" +
"                           OO.*\n" +
"                         , ff.[SALDO_TOTAL]\n" +
"                         , ff.[%_COND_CAP]\n" +
"                         , ff.[SALDO_CONDONADO]\n" +
"                         , ff.[%_PAGO_CAP]\n" +
"                         , ff.[SALDO_PAGO]\n" +
"                         , ff.[INTERES]\n" +
"                         , ff.[%_COND_INT]\n" +
"                         , ff.[INT_CONDONADO]\n" +
"                         , ff.[%_PAGO_INT]\n" +
"                         , ff.[INT_PAGADO]\n" +
"                         , ff.[%_PAGO_HON]\n" +
"                         , ff.[HON_PAGO]\n" +
"                         , ff.[MONTO_A_RECIBIR]\n" +
"                         , ff.[VDE_SGN]\n" +
"                         , ff.[PROVICION]\n" +
"                         , ff.[MONTO_TOTAL_PAGO]\n" +
//"                          , concat (OO.[Demora-dias],' Dias = ',OO.[Demora-horas],' Horas = ',OO.[Demora-segundos],' Seg') 'KPI'\n" +
"                        , concat (OO.[Demora-dias],' Dias ') 'KPI'\n" +
"					  --  ,  FORMAT(tiempo_aplicacion,'yyyy-MM') 'tapsssp'\n" +
"						 \n" +
"                from\n" +
"                           (\n" +
" \n" +
"\n" +
"\n" +
"                                 select\n" +
"                                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n" +
"                                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n" +
"                                       , convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24)'Demora-dias'\n" +
"                                       , LL.*\n" +
"                                  from\n" +
"                                         (\n" +
"                                                    select\n" +
"                                                               (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado in ('Aprobado' ,'Aprob Analista','Aprobada Zonal')) 'tiempo_aprobacion'\n" +
"                                                             , (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc  inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado   ='Aplicada Aplicacion') 'tiempo_aplicacion'\n" +
"                                                             , UU.aprovado_por   'Aprovado_Por'\n" +
"                                                             , tmc.fld_num_ofi 'numero_oficina'\n" +
"                                                             , tmc.fld_ofi 'desc_oficina'\n" +
"                                                             , (select    min(mop.fld_fec_cas)  from   tracking_morahoy_ope mop   where  mop.fk_idTrkMorClie=tmc.id )  'Minima Fecha Castigo'\n" +
"                                                             , UU.total_capital_recibe  'monto_total_recibit'  \n" +
"                                                             , UU.cedente             'fld_ced'    \n" +
"                                                             , UU.id_condonacion       'id_condonacion'     \n" +
"                                                             ,UU.ejecutivo_condona          'Ejecutivo Origen'\n" +
"                                                            ,tmc.fld_rut_ent 'rut_cliente'\n" +                
"                                                    from     siscon_inf_clienteV2   as UU\n" +
"                                                               INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n" +
"                                         )\n" +
"                                         as LL\n" +
"                                  where\n" +
"                                         LL.tiempo_aplicacion is not null\n" +
"                           )\n" +
"                           as OO\n" +
"                           inner join\n" +
"                                      tbl_siscon_condonaciones as ff\n" +
"                                      on\n" +
"                                                 ff.id_condonacion=OO.id_condonacion"
                + "   where  fld_ced not like '%NOVA%'   and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and tiempo_aplicacion <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101)";

        //Messagebox.show("SQL INFORME Periodo : ["+SQL+"]");
        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 6055: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    
    public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInforme(String cuenta, String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n"
                + "           OO.*\n"
                + "         , ff.[SALDO_TOTAL]\n"
                + "         , ff.[%_COND_CAP]\n"
                + "         , ff.[SALDO_CONDONADO]\n"
                + "         , ff.[%_PAGO_CAP]\n"
                + "         , ff.[SALDO_PAGO]\n"
                + "         , ff.[INTERES]\n"
                + "         , ff.[%_COND_INT]\n"
                + "         , ff.[INT_CONDONADO]\n"
                + "         , ff.[%_PAGO_INT]\n"
                + "         , ff.[INT_PAGADO]\n"
                + "         , ff.[%_PAGO_HON]\n"
                + "         , ff.[HON_PAGO]\n"
                + "         , ff.[MONTO_A_RECIBIR]\n"
                + "         , ff.[VDE_SGN]\n"
                + "         , ff.[PROVICION]\n"
                + "         , ff.[MONTO_TOTAL_PAGO]\n"
                + "          , concat (OO.[Demora-dias],' Dias = ',OO.[Demora-horas],' Horas = ',OO.[Demora-segundos],' Seg') 'KPI'\n"
                + "from\n"
                + "           (\n"
                + "                  select\n"
                + "                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n"
                + "                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n"
                + "                       , DATEDIFF(week, tiempo_aprobacion,tiempo_aplicacion)'Demora-dias'\n"
                + "                       , LL.*\n"
                + "                  from\n"
                + "                         (\n"
                + "                                    select\n"
                + "                                               (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado in ('Aprobado'\n"
                + "                                                                                         ,'Aprob Analista'\n"
                + "                                                                                         ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'tiempo_aprobacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado       ='Aplicada Aplicacion'\n"
                + "                                               )\n"
                + "                                               'tiempo_aplicacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(max(te.dv_CunetaOrigen),'ONLINE')\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and es.dv_estado in ('Aprobado'\n"
                + "                                                                                        ,'Aprob Analista'\n"
                + "                                                                                        ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'Aprovado_Por'\n"
                + "                                             , tmc.fld_num_ofi 'numero_oficina'\n"
                + "                                             , tmc.fld_ofi 'desc_oficina'\n"
                + "                                             , (\n"
                + "                                                      select\n"
                + "                                                             min(mop.fld_fec_cas)\n"
                + "                                                      from\n"
                + "                                                             tracking_morahoy_ope mop\n"
                + "                                                      where\n"
                + "                                                             mop.fk_idTrkMorClie=tmc.id\n"
                + "                                               )\n"
                + "                                               'Minima Fecha Castigo'\n"
                + "                                             , UU.monto_total_recibit\n"
                + "                                             , UU.fld_ced\n"
                + "                                             , UU.id_condonacion\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(min(te.dv_CunetaOrigen), (\n"
                + "                                                                                select\n"
                + "                                                                                           min(us.alias)\n"
                + "                                                                                from\n"
                + "                                                                                           condonacion cooo\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      colaborador colll\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 colll.id=cooo.di_fk_idColadorador\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      usuario us\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 us.id_usuario=colll.id_usuario\n"
                + "                                                                                where\n"
                + "                                                                                           cooo.id_condonacion=UU.id_condonacion\n"
                + "                                                                     )\n"
                + "                                                                     )\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and\n"
                + "                                                                     (\n"
                + "                                                                                te.dv_estadoInicio in ('Estado.Nueva'\n"
                + "                                                                                                     ,'Estado.Nuevo')\n"
                + "                                                                                or te.dv_ModuloOrigen = 'Ejecutivo.Reparadas'\n"
                + "                                                                     )\n"
                + "                                               )\n"
                + "                                               'Ejecutivo Origen'\n"
                + "                                    from\n"
                + "                                               (\n"
                + "                                                         select\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                                                 , count(rep.di_fk_IdCondonacion) 'num_reparos'\n"
                + "                                                                 , cast(\n"
                + "                                                                         (\n"
                + "                                                                                select\n"
                + "                                                                                       top 1 *\n"
                + "                                                                                from\n"
                + "                                                                                       [dbo].[Concatena_CED] (GG.rut)\n"
                + "                                                                        )\n"
                + "                                                                        as varchar) as fld_ced\n"
                + "                                                         from\n"
                + "                                                                   (\n"
                + "                                                                              select\n"
                + "                                                                                         cli.rut\n"
                + "                                                                                       , cli.nombre\n"
                + "                                                                                       , con.id_condonacion\n"
                + "                                                                                       , con.timestap\n"
                + "                                                                                       , re.[desc] 'regla'\n"
                + "                                                                                       , est.dv_estado\n"
                + "                                                                                       , con.comentario_resna\n"
                + "                                                                                       , con.monto_total_condonado\n"
                + "                                                                                       , con.monto_total_recibit\n"
                + "                                                                                       , con.di_num_opers\n"
                + "                                                                                       , con.monto_total_capital\n"
                + "                                                                                       , tpcon.dv_desc\n"
                + "                                                                                       , usu.di_rut\n"
                + "                                                                              from\n"
                + "                                                                                         condonacion con\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    regla re\n"
                + "                                                                                                    on\n"
                + "                                                                                                               re.id_regla=con.id_regla\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    estado est\n"
                + "                                                                                                    on\n"
                + "                                                                                                               est.di_id_Estado=con.id_estado\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    tipo_condonacion tpcon\n"
                + "                                                                                                    on\n"
                + "                                                                                                               tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    colaborador cola\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cola.id=con.di_fk_idColadorador\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    usuario usu\n"
                + "                                                                                                    on\n"
                + "                                                                                                               usu.id_usuario=cola.id_usuario\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    cliente cli\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cli.id_cliente=con.di_fk_IdCliente\n"
                + "                                                                              where\n"
                + "                                                                                         con.id_estado in\n"
                + "                                                                                         (\n"
                + "                                                                                                select\n"
                + "                                                                                                       est.di_id_Estado\n"
                + "                                                                                                from\n"
                + "                                                                                                       estado est\n"
                + "                                                                                                where\n"
                + "                                                                                                       est.dv_estado ='Aplicada Aplicacion'\n"
                + "                                                                                         )\n"
                + "                                                                   )\n"
                + "                                                                   as GG\n"
                + "                                                                   left join\n"
                + "                                                                             reparo rep\n"
                + "                                                                             on\n"
                + "                                                                                       rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                                                         group by\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                               )\n"
                + "                                               as UU\n"
                + "                                               INNER JOIN\n"
                + "                                                          dbo.traking_morahoy_clie tmc\n"
                + "                                                          ON\n"
                + "                                                                     id_condonacion = tmc.fk_idCondonacion\n"
                + "                         )\n"
                + "                         as LL\n"
                + "                  where\n"
                + "                         LL.tiempo_aplicacion is not null\n"
                + "           )\n"
                + "           as OO\n"
                + "           inner join\n"
                + "                      tbl_siscon_condonaciones as ff\n"
                + "                      on\n"
                + "                                 ff.id_condonacion=OO.id_condonacion"
                + "   where fld_ced = 'BCI'   and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and tiempo_aplicacion <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101)";

         //Messagebox.show("SQL INFORME Periodo : ["+SQL+"]");
        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    
    
    
        public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInformeV2(String cuenta, String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n" +
"                           OO.*\n" +
"                         , ff.[SALDO_TOTAL]\n" +
"                         , ff.[%_COND_CAP]\n" +
"                         , ff.[SALDO_CONDONADO]\n" +
"                         , ff.[%_PAGO_CAP]\n" +
"                         , ff.[SALDO_PAGO]\n" +
"                         , ff.[INTERES]\n" +
"                         , ff.[%_COND_INT]\n" +
"                         , ff.[INT_CONDONADO]\n" +
"                         , ff.[%_PAGO_INT]\n" +
"                         , ff.[INT_PAGADO]\n" +
"                         , ff.[%_PAGO_HON]\n" +
"                         , ff.[HON_PAGO]\n" +
"                         , ff.[MONTO_A_RECIBIR]\n" +
"                         , ff.[VDE_SGN]\n" +
"                         , ff.[PROVICION]\n" +
"                         , ff.[MONTO_TOTAL_PAGO]\n" +
//"                          , concat (OO.[Demora-dias],' Dias = ',OO.[Demora-horas],' Horas = ',OO.[Demora-segundos],' Seg') 'KPI'\n" +
"                        , concat (OO.[Demora-dias],' Dias ') 'KPI'\n" +
"					  --  ,  FORMAT(tiempo_aplicacion,'yyyy-MM') 'tapsssp'\n" +
"						 \n" +
"                from\n" +
"                           (\n" +
" \n" +
"\n" +
"\n" +
"                                 select\n" +
"                                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n" +
"                                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n" +
"                                       , convert(decimal(12,2),convert(decimal(12,2),datediff(second,tiempo_aprobacion,tiempo_aplicacion))/60/60/24)'Demora-dias'\n" +
"                                       , LL.*\n" +
"                                  from\n" +
"                                         (\n" +
"                                                    select\n" +
"                                                               (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado in ('Aprobado' ,'Aprob Analista','Aprobada Zonal')) 'tiempo_aprobacion'\n" +
"                                                             , (select  max(estc.ddt_fechaReistro)  from estado_condonacion estc  inner join estado est on estc.di_fk_IdEstado=est.di_id_Estado  where estc.di_fk_IdCondonacion=UU.id_condonacion  and est.dv_estado   ='Aplicada Aplicacion') 'tiempo_aplicacion'\n" +
"                                                             , UU.aprovado_por   'Aprovado_Por'\n" +
"                                                             , tmc.fld_num_ofi 'numero_oficina'\n" +
"                                                             , tmc.fld_ofi 'desc_oficina'\n" +
"                                                             , (select    min(mop.fld_fec_cas)  from   tracking_morahoy_ope mop   where  mop.fk_idTrkMorClie=tmc.id )  'Minima Fecha Castigo'\n" +
"                                                             , UU.total_capital_recibe  'monto_total_recibit'  \n" +
"                                                             , UU.cedente             'fld_ced'    \n" +
"                                                             , UU.id_condonacion       'id_condonacion'     \n" +
"                                                             ,UU.ejecutivo_condona          'Ejecutivo Origen'\n" +
"                                                    from     siscon_inf_clienteV2   as UU\n" +
"                                                               INNER JOIN dbo.traking_morahoy_clie tmc ON id_condonacion = tmc.fk_idCondonacion\n" +
"                                         )\n" +
"                                         as LL\n" +
"                                  where\n" +
"                                         LL.tiempo_aplicacion is not null\n" +
"                           )\n" +
"                           as OO\n" +
"                           inner join\n" +
"                                      tbl_siscon_condonaciones as ff\n" +
"                                      on\n" +
"                                                 ff.id_condonacion=OO.id_condonacion"
                + "   where  fld_ced not like '%NOVA%'   and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and tiempo_aplicacion <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101)";

         //Messagebox.show("SQL INFORME Periodo : ["+SQL+"]");
        List<ResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 6055: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    
    public List<ResumenCondonacionesInforme> GetCondonacionesAplicadasAndInformeV2(String cuenta, String f_inicio, String f_fin) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n"
                + "           OO.*\n"
                + "         , ff.[SALDO_TOTAL]\n"
                + "         , ff.[%_COND_CAP]\n"
                + "         , ff.[SALDO_CONDONADO]\n"
                + "         , ff.[%_PAGO_CAP]\n"
                + "         , ff.[SALDO_PAGO]\n"
                + "         , ff.[INTERES]\n"
                + "         , ff.[%_COND_INT]\n"
                + "         , ff.[INT_CONDONADO]\n"
                + "         , ff.[%_PAGO_INT]\n"
                + "         , ff.[INT_PAGADO]\n"
                + "         , ff.[%_PAGO_HON]\n"
                + "         , ff.[HON_PAGO]\n"
                + "         , ff.[MONTO_A_RECIBIR]\n"
                + "         , ff.[VDE_SGN]\n"
                + "         , ff.[PROVICION]\n"
                + "         , ff.[MONTO_TOTAL_PAGO]\n"
                + "          , concat (OO.[Demora-dias],' Dias = ',OO.[Demora-horas],' Horas = ',OO.[Demora-segundos],' Seg') 'KPI'\n"
                + "from\n"
                + "           (\n"
                + "                  select\n"
                + "                         DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n"
                + "                       , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n"
                + "                       , DATEDIFF(week, tiempo_aprobacion,tiempo_aplicacion)'Demora-dias'\n"
                + "                       , LL.*\n"
                + "                  from\n"
                + "                         (\n"
                + "                                    select\n"
                + "                                               (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado in ('Aprobado'\n"
                + "                                                                                         ,'Aprob Analista'\n"
                + "                                                                                         ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'tiempo_aprobacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     max(estc.ddt_fechaReistro)\n"
                + "                                                          from\n"
                + "                                                                     estado_condonacion estc\n"
                + "                                                                     inner join\n"
                + "                                                                                estado est\n"
                + "                                                                                on\n"
                + "                                                                                           estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                          where\n"
                + "                                                                     estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and est.dv_estado       ='Aplicada Aplicacion'\n"
                + "                                               )\n"
                + "                                               'tiempo_aplicacion'\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(max(te.dv_CunetaOrigen),'ONLINE')\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and es.dv_estado in ('Aprobado'\n"
                + "                                                                                        ,'Aprob Analista'\n"
                + "                                                                                        ,'Aprobada Zonal')\n"
                + "                                               )\n"
                + "                                               'Aprovado_Por'\n"
                + "                                             , tmc.fld_num_ofi 'numero_oficina'\n"
                + "                                             , tmc.fld_ofi 'desc_oficina'\n"
                + "                                             , (\n"
                + "                                                      select\n"
                + "                                                             min(mop.fld_fec_cas)\n"
                + "                                                      from\n"
                + "                                                             tracking_morahoy_ope mop\n"
                + "                                                      where\n"
                + "                                                             mop.fk_idTrkMorClie=tmc.id\n"
                + "                                               )\n"
                + "                                               'Minima Fecha Castigo'\n"
                + "                                             , UU.monto_total_recibit\n"
                + "                                             , UU.fld_ced\n"
                + "                                             , UU.id_condonacion\n"
                + "                                             , (\n"
                + "                                                          select\n"
                + "                                                                     isnull(min(te.dv_CunetaOrigen), (\n"
                + "                                                                                select\n"
                + "                                                                                           min(us.alias)\n"
                + "                                                                                from\n"
                + "                                                                                           condonacion cooo\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      colaborador colll\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 colll.id=cooo.di_fk_idColadorador\n"
                + "                                                                                           inner join\n"
                + "                                                                                                      usuario us\n"
                + "                                                                                                      on\n"
                + "                                                                                                                 us.id_usuario=colll.id_usuario\n"
                + "                                                                                where\n"
                + "                                                                                           cooo.id_condonacion=UU.id_condonacion\n"
                + "                                                                     )\n"
                + "                                                                     )\n"
                + "                                                          from\n"
                + "                                                                     trackin_estado te\n"
                + "                                                                     inner join\n"
                + "                                                                                estado_condonacion est\n"
                + "                                                                                on\n"
                + "                                                                                           est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                     inner join\n"
                + "                                                                                estado es\n"
                + "                                                                                on\n"
                + "                                                                                           es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                          where\n"
                + "                                                                     est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                     and\n"
                + "                                                                     (\n"
                + "                                                                                te.dv_estadoInicio in ('Estado.Nueva'\n"
                + "                                                                                                     ,'Estado.Nuevo')\n"
                + "                                                                                or te.dv_ModuloOrigen = 'Ejecutivo.Reparadas'\n"
                + "                                                                     )\n"
                + "                                               )\n"
                + "                                               'Ejecutivo Origen'\n"
                + "                                    from\n"
                + "                                               (\n"
                + "                                                         select\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                                                 , count(rep.di_fk_IdCondonacion) 'num_reparos'\n"
                + "                                                                 , cast(\n"
                + "                                                                         (\n"
                + "                                                                                select\n"
                + "                                                                                       top 1 *\n"
                + "                                                                                from\n"
                + "                                                                                       [dbo].[Concatena_CED] (GG.rut)\n"
                + "                                                                        )\n"
                + "                                                                        as varchar) as fld_ced\n"
                + "                                                         from\n"
                + "                                                                   (\n"
                + "                                                                              select\n"
                + "                                                                                         cli.rut\n"
                + "                                                                                       , cli.nombre\n"
                + "                                                                                       , con.id_condonacion\n"
                + "                                                                                       , con.timestap\n"
                + "                                                                                       , re.[desc] 'regla'\n"
                + "                                                                                       , est.dv_estado\n"
                + "                                                                                       , con.comentario_resna\n"
                + "                                                                                       , con.monto_total_condonado\n"
                + "                                                                                       , con.monto_total_recibit\n"
                + "                                                                                       , con.di_num_opers\n"
                + "                                                                                       , con.monto_total_capital\n"
                + "                                                                                       , tpcon.dv_desc\n"
                + "                                                                                       , usu.di_rut\n"
                + "                                                                              from\n"
                + "                                                                                         condonacion con\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    regla re\n"
                + "                                                                                                    on\n"
                + "                                                                                                               re.id_regla=con.id_regla\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    estado est\n"
                + "                                                                                                    on\n"
                + "                                                                                                               est.di_id_Estado=con.id_estado\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    tipo_condonacion tpcon\n"
                + "                                                                                                    on\n"
                + "                                                                                                               tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    colaborador cola\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cola.id=con.di_fk_idColadorador\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    usuario usu\n"
                + "                                                                                                    on\n"
                + "                                                                                                               usu.id_usuario=cola.id_usuario\n"
                + "                                                                                         inner join\n"
                + "                                                                                                    cliente cli\n"
                + "                                                                                                    on\n"
                + "                                                                                                               cli.id_cliente=con.di_fk_IdCliente\n"
                + "                                                                              where\n"
                + "                                                                                         con.id_estado in\n"
                + "                                                                                         (\n"
                + "                                                                                                select\n"
                + "                                                                                                       est.di_id_Estado\n"
                + "                                                                                                from\n"
                + "                                                                                                       estado est\n"
                + "                                                                                                where\n"
                + "                                                                                                       est.dv_estado ='Aplicada Aplicacion'\n"
                + "                                                                                         )\n"
                + "                                                                   )\n"
                + "                                                                   as GG\n"
                + "                                                                   left join\n"
                + "                                                                             reparo rep\n"
                + "                                                                             on\n"
                + "                                                                                       rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                                                         group by\n"
                + "                                                                   GG.rut\n"
                + "                                                                 , GG.nombre\n"
                + "                                                                 , GG.id_condonacion\n"
                + "                                                                 , GG.timestap\n"
                + "                                                                 , GG.regla\n"
                + "                                                                 , GG.dv_estado\n"
                + "                                                                 , GG.comentario_resna\n"
                + "                                                                 , GG.monto_total_condonado\n"
                + "                                                                 , GG.monto_total_recibit\n"
                + "                                                                 , GG.di_num_opers\n"
                + "                                                                 , GG.monto_total_capital\n"
                + "                                                                 , GG.dv_desc\n"
                + "                                                                 , GG.di_rut\n"
                + "                                               )\n"
                + "                                               as UU\n"
                + "                                               INNER JOIN\n"
                + "                                                          dbo.traking_morahoy_clie tmc\n"
                + "                                                          ON\n"
                + "                                                                     id_condonacion = tmc.fk_idCondonacion\n"
                + "                         )\n"
                + "                         as LL\n"
                + "                  where\n"
                + "                         LL.tiempo_aplicacion is not null\n"
                + "           )\n"
                + "           as OO\n"
                + "           inner join\n"
                + "                      tbl_siscon_condonaciones as ff\n"
                + "                      on\n"
                + "                                 ff.id_condonacion=OO.id_condonacion"
                + "   where fld_ced = 'BCI' and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'00:00:00.000',101)   and  and tiempo_aplicacion <=  CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'23:59:59.000',101) ";

        List<ResumenCondonacionesInforme> dsol = null;
         Messagebox.show("SQL Query info 2: " + SQL);
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<APPResumenCondonacionesInforme> GetCondonacionesAplicadasAndResumenInforme(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n"
                + "      isnull(count(HH.id_condonacion),0) 'total_rut'\n"
                + "     , isnull(sum(HH.SALDO_TOTAL),0) 'TOTAL_CASTIGADO'\n"
                + "     , isnull(sum(HH.VDE_SGN ),0) 'TOTAL_VDE_SGN'\n"
                + "     , isnull(sum(HH.monto_total_recibit),0) 'TOTAL_RECIBIDO_1'\n"
                + "     , isnull(sum(HH.MONTO_A_RECIBIR),0) 'TOTAL_RECIBIDO_2'\n"
                + "     , isnull(sum(HH.MONTO_TOTAL_PAGO),0) 'TOTAL_TOTAL_RECIBE'\n"
                + "     , isnull(sum(HH.SALDO_CONDONADO),0) 'TOTAL_CAPITAL_CONDONADO'\n"
                + "     , isnull(sum(HH.INT_CONDONADO),0) 'TOTAL_INTERES_CONDONADO'\n"
                + "     , isnull(sum(HH.HON_PAGO),0) 'TOTAL_HONORARIO_RECIBE'\n"
                + "     , isnull(sum(HH.PROVICION),0) 'TOTAL_COSTAS_PROVISION'\n"
                + "from\n"
                + "       (\n"
                + "                  select\n"
                + "                             OO.*\n"
                + "                           , ff.[SALDO_TOTAL     ]\n"
                + "                           , ff.[%_COND_CAP      ]\n"
                + "                           , ff.[SALDO_CONDONADO ]\n"
                + "                           , ff.[%_PAGO_CAP      ]\n"
                + "                           , ff.[SALDO_PAGO      ]\n"
                + "                           , ff.[INTERES         ]\n"
                + "                           , ff.[%_COND_INT      ]\n"
                + "                           , ff.[INT_CONDONADO   ]\n"
                + "                           , ff.[%_PAGO_INT      ]\n"
                + "                           , ff.[INT_PAGADO      ]\n"
                + "                           , ff.[%_PAGO_HON      ]\n"
                + "                           , ff.[HON_PAGO        ]\n"
                + "                           , ff.[MONTO_A_RECIBIR ]\n"
                + "                           , ff.[VDE_SGN         ]\n"
                + "                           , ff.[PROVICION       ]\n"
                + "                           , ff.[MONTO_TOTAL_PAGO]\n"
                + "                  from\n"
                + "                             (\n"
                + "                                    select\n"
                + "                                           DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n"
                + "                                         , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n"
                + "                                         , DATEDIFF(week, tiempo_aprobacion,tiempo_aplicacion)'Demora-dias'\n"
                + "                                         , LL.*\n"
                + "                                    from\n"
                + "                                           (\n"
                + "                                                      select\n"
                + "                                                                 (\n"
                + "                                                                            select\n"
                + "                                                                                       max(estc.ddt_fechaReistro)\n"
                + "                                                                            from\n"
                + "                                                                                       estado_condonacion estc\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                                            where\n"
                + "                                                                                       estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and est.dv_estado in ('Aprobado'\n"
                + "                                                                                                           ,'Aprob Analista'\n"
                + "                                                                                                           ,'Aprobada Zonal')\n"
                + "                                                                 )\n"
                + "                                                                 'tiempo_aprobacion'\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       max(estc.ddt_fechaReistro)\n"
                + "                                                                            from\n"
                + "                                                                                       estado_condonacion estc\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                                            where\n"
                + "                                                                                       estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and est.dv_estado       ='Aplicada Aplicacion'\n"
                + "                                                                 )\n"
                + "                                                                 'tiempo_aplicacion'\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       isnull(max(te.dv_CunetaOrigen),'ONLINE')\n"
                + "                                                                            from\n"
                + "                                                                                       trackin_estado te\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado_condonacion est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado es\n"
                + "                                                                                                  on\n"
                + "                                                                                                             es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                                            where\n"
                + "                                                                                       est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and es.dv_estado in ('Aprobado'\n"
                + "                                                                                                          ,'Aprob Analista'\n"
                + "                                                                                                          ,'Aprobada Zonal')\n"
                + "                                                                 )\n"
                + "                                                                 'Aprovado Por'\n"
                + "                                                               , tmc.fld_num_ofi 'numero_oficina'\n"
                + "                                                               , tmc.fld_ofi 'desc_oficina'\n"
                + "                                                               , (\n"
                + "                                                                        select\n"
                + "                                                                               min(mop.fld_fec_cas)\n"
                + "                                                                        from\n"
                + "                                                                               tracking_morahoy_ope mop\n"
                + "                                                                        where\n"
                + "                                                                               mop.fk_idTrkMorClie=tmc.id\n"
                + "                                                                 )\n"
                + "                                                                 'Minima Fecha Castigo'\n"
                + "                                                               , UU.monto_total_recibit\n"
                + "                                                               , UU.fld_ced\n"
                + "                                                               , UU.id_condonacion\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       isnull(min(te.dv_CunetaOrigen), (\n"
                + "                                                                                                  select\n"
                + "                                                                                                             min(us.alias)\n"
                + "                                                                                                  from\n"
                + "                                                                                                             condonacion cooo\n"
                + "                                                                                                             inner join\n"
                + "                                                                                                                        colaborador colll\n"
                + "                                                                                                                        on\n"
                + "                                                                                                                                   colll.id=cooo.di_fk_idColadorador\n"
                + "                                                                                                             inner join\n"
                + "                                                                                                                        usuario us\n"
                + "                                                                                                                        on\n"
                + "                                                                                                                                   us.id_usuario=colll.id_usuario\n"
                + "                                                                                                  where\n"
                + "                                                                                                             cooo.id_condonacion=UU.id_condonacion\n"
                + "                                                                                       )\n"
                + "                                                                                       )\n"
                + "                                                                            from\n"
                + "                                                                                       trackin_estado te\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado_condonacion est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado es\n"
                + "                                                                                                  on\n"
                + "                                                                                                             es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                                            where\n"
                + "                                                                                       est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and\n"
                + "                                                                                       (\n"
                + "                                                                                                  te.dv_estadoInicio in ('Estado.Nueva'\n"
                + "                                                                                                                       ,'Estado.Nuevo')\n"
                + "                                                                                                  or te.dv_ModuloOrigen = 'Ejecutivo.Reparadas'\n"
                + "                                                                                       )\n"
                + "                                                                 )\n"
                + "                                                                 'Ejecutivo Origen'\n"
                + "                                                      from\n"
                + "                                                                 (\n"
                + "                                                                           select\n"
                + "                                                                                     GG.rut\n"
                + "                                                                                   , GG.nombre\n"
                + "                                                                                   , GG.id_condonacion\n"
                + "                                                                                   , GG.timestap\n"
                + "                                                                                   , GG.regla\n"
                + "                                                                                   , GG.dv_estado\n"
                + "                                                                                   , GG.comentario_resna\n"
                + "                                                                                   , GG.monto_total_condonado\n"
                + "                                                                                   , GG.monto_total_recibit\n"
                + "                                                                                   , GG.di_num_opers\n"
                + "                                                                                   , GG.monto_total_capital\n"
                + "                                                                                   , GG.dv_desc\n"
                + "                                                                                   , GG.di_rut\n"
                + "                                                                                   , count(rep.di_fk_IdCondonacion) 'num_reparos'\n"
                + "                                                                                   , cast(\n"
                + "                                                                                           (\n"
                + "                                                                                                  select\n"
                + "                                                                                                         top 1 *\n"
                + "                                                                                                  from\n"
                + "                                                                                                         [dbo].[Concatena_CED] (GG.rut)\n"
                + "                                                                                          )\n"
                + "                                                                                          as varchar) as fld_ced\n"
                + "                                                                           from\n"
                + "                                                                                     (\n"
                + "                                                                                                select\n"
                + "                                                                                                           cli.rut\n"
                + "                                                                                                         , cli.nombre\n"
                + "                                                                                                         , con.id_condonacion\n"
                + "                                                                                                         , con.timestap\n"
                + "                                                                                                         , re.[desc] 'regla'\n"
                + "                                                                                                         , est.dv_estado\n"
                + "                                                                                                         , con.comentario_resna\n"
                + "                                                                                                         , con.monto_total_condonado\n"
                + "                                                                                                         , con.monto_total_recibit\n"
                + "                                                                                                         , con.di_num_opers\n"
                + "                                                                                                         , con.monto_total_capital\n"
                + "                                                                                                         , tpcon.dv_desc\n"
                + "                                                                                                         , usu.di_rut\n"
                + "                                                                                                from\n"
                + "                                                                                                           condonacion con\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      regla re\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 re.id_regla=con.id_regla\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      estado est\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 est.di_id_Estado=con.id_estado\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      tipo_condonacion tpcon\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      colaborador cola\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 cola.id=con.di_fk_idColadorador\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      usuario usu\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 usu.id_usuario=cola.id_usuario\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      cliente cli\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 cli.id_cliente=con.di_fk_IdCliente\n"
                + "                                                                                                where\n"
                + "                                                                                                           con.id_estado in\n"
                + "                                                                                                           (\n"
                + "                                                                                                                  select\n"
                + "                                                                                                                         est.di_id_Estado\n"
                + "                                                                                                                  from\n"
                + "                                                                                                                         estado est\n"
                + "                                                                                                                  where\n"
                + "                                                                                                                         est.dv_estado ='Aplicada Aplicacion'\n"
                + "                                                                                                           )\n"
                + "                                                                                     )\n"
                + "                                                                                     as GG\n"
                + "                                                                                     left join\n"
                + "                                                                                               reparo rep\n"
                + "                                                                                               on\n"
                + "                                                                                                         rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                                                                           group by\n"
                + "                                                                                     GG.rut\n"
                + "                                                                                   , GG.nombre\n"
                + "                                                                                   , GG.id_condonacion\n"
                + "                                                                                   , GG.timestap\n"
                + "                                                                                   , GG.regla\n"
                + "                                                                                   , GG.dv_estado\n"
                + "                                                                                   , GG.comentario_resna\n"
                + "                                                                                   , GG.monto_total_condonado\n"
                + "                                                                                   , GG.monto_total_recibit\n"
                + "                                                                                   , GG.di_num_opers\n"
                + "                                                                                   , GG.monto_total_capital\n"
                + "                                                                                   , GG.dv_desc\n"
                + "                                                                                   , GG.di_rut\n"
                + "                                                                 )\n"
                + "                                                                 as UU\n"
                + "                                                                 INNER JOIN\n"
                + "                                                                            dbo.traking_morahoy_clie tmc\n"
                + "                                                                            ON\n"
                + "                                                                                       id_condonacion = tmc.fk_idCondonacion\n"
                + "                                           )\n"
                + "                                           as LL\n"
                + "                                    where\n"
                + "                                           LL.tiempo_aplicacion is not null\n"
                + "                             )\n"
                + "                             as OO\n"
                + "                             inner join\n"
                + "                                        tbl_siscon_condonaciones as ff\n"
                + "                                        on\n"
                + "                                                   ff.id_condonacion=OO.id_condonacion\n"
                + "                  where\n"
                + "                             fld_ced                = 'BCI'\n"
                + "                             and tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'00:00:00.000',101)\n"
                + "                             and tiempo_aplicacion <= CONVERT(datetime,+CONVERT (date, GETDATE()),101)+CONVERT(datetime,+'23:59:59.000',101)\n"
                + "       )\n"
                + "       as HH \n"
                + "";

        List<APPResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperResumenInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<APPResumenCondonacionesInforme> GetCondonacionesAplicadasAndResumenInforme(String cuenta, String periodo) {
        jdbcTemplate = new JdbcTemplate(dataSource);

        int _estadoAplicadas = _estados._buscarIDEstado("APP-APLICADA");
        //// Agregar tracking consulta recepcion Aplicacion

        String SQL = "select\n"
                + "      isnull(count(HH.id_condonacion),0) 'total_rut'\n"
                + "     , isnull(sum(HH.SALDO_TOTAL),0) 'TOTAL_CASTIGADO'\n"
                + "     , isnull(sum(HH.VDE_SGN ),0) 'TOTAL_VDE_SGN'\n"
                + "     , isnull(sum(HH.monto_total_recibit),0) 'TOTAL_RECIBIDO_1'\n"
                + "     , isnull(sum(HH.MONTO_A_RECIBIR),0) 'TOTAL_RECIBIDO_2'\n"
                + "     , isnull(sum(HH.MONTO_TOTAL_PAGO),0) 'TOTAL_TOTAL_RECIBE'\n"
                + "     , isnull(sum(HH.SALDO_CONDONADO),0) 'TOTAL_CAPITAL_CONDONADO'\n"
                + "     , isnull(sum(HH.INT_CONDONADO),0) 'TOTAL_INTERES_CONDONADO'\n"
                + "     , isnull(sum(HH.HON_PAGO),0) 'TOTAL_HONORARIO_RECIBE'\n"
                + "     , isnull(sum(HH.PROVICION),0) 'TOTAL_COSTAS_PROVISION'\n"
                + "from\n"
                + "       (\n"
                + "                  select\n"
                + "                             OO.*\n"
                + "                           , ff.[SALDO_TOTAL     ]\n"
                + "                           , ff.[%_COND_CAP      ]\n"
                + "                           , ff.[SALDO_CONDONADO ]\n"
                + "                           , ff.[%_PAGO_CAP      ]\n"
                + "                           , ff.[SALDO_PAGO      ]\n"
                + "                           , ff.[INTERES         ]\n"
                + "                           , ff.[%_COND_INT      ]\n"
                + "                           , ff.[INT_CONDONADO   ]\n"
                + "                           , ff.[%_PAGO_INT      ]\n"
                + "                           , ff.[INT_PAGADO      ]\n"
                + "                           , ff.[%_PAGO_HON      ]\n"
                + "                           , ff.[HON_PAGO        ]\n"
                + "                           , ff.[MONTO_A_RECIBIR ]\n"
                + "                           , ff.[VDE_SGN         ]\n"
                + "                           , ff.[PROVICION       ]\n"
                + "                           , ff.[MONTO_TOTAL_PAGO]\n"
                + "                  from\n"
                + "                             (\n"
                + "                                    select\n"
                + "                                           DATEDIFF(minute, tiempo_aprobacion,tiempo_aplicacion)'Demora-segundos'\n"
                + "                                         , DATEDIFF(hour, tiempo_aprobacion,tiempo_aplicacion)'Demora-horas'\n"
                + "                                         , DATEDIFF(week, tiempo_aprobacion,tiempo_aplicacion)'Demora-dias'\n"
                + "                                         , LL.*\n"
                + "                                    from\n"
                + "                                           (\n"
                + "                                                      select\n"
                + "                                                                 (\n"
                + "                                                                            select\n"
                + "                                                                                       max(estc.ddt_fechaReistro)\n"
                + "                                                                            from\n"
                + "                                                                                       estado_condonacion estc\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                                            where\n"
                + "                                                                                       estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and est.dv_estado in ('Aprobado'\n"
                + "                                                                                                           ,'Aprob Analista'\n"
                + "                                                                                                           ,'Aprobada Zonal')\n"
                + "                                                                 )\n"
                + "                                                                 'tiempo_aprobacion'\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       max(estc.ddt_fechaReistro)\n"
                + "                                                                            from\n"
                + "                                                                                       estado_condonacion estc\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             estc.di_fk_IdEstado=est.di_id_Estado\n"
                + "                                                                            where\n"
                + "                                                                                       estc.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and est.dv_estado       ='Aplicada Aplicacion'\n"
                + "                                                                 )\n"
                + "                                                                 'tiempo_aplicacion'\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       isnull(max(te.dv_CunetaOrigen),'ONLINE')\n"
                + "                                                                            from\n"
                + "                                                                                       trackin_estado te\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado_condonacion est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado es\n"
                + "                                                                                                  on\n"
                + "                                                                                                             es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                                            where\n"
                + "                                                                                       est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and es.dv_estado in ('Aprobado'\n"
                + "                                                                                                          ,'Aprob Analista'\n"
                + "                                                                                                          ,'Aprobada Zonal')\n"
                + "                                                                 )\n"
                + "                                                                 'Aprovado Por'\n"
                + "                                                               , tmc.fld_num_ofi 'numero_oficina'\n"
                + "                                                               , tmc.fld_ofi 'desc_oficina'\n"
                + "                                                               , (\n"
                + "                                                                        select\n"
                + "                                                                               min(mop.fld_fec_cas)\n"
                + "                                                                        from\n"
                + "                                                                               tracking_morahoy_ope mop\n"
                + "                                                                        where\n"
                + "                                                                               mop.fk_idTrkMorClie=tmc.id\n"
                + "                                                                 )\n"
                + "                                                                 'Minima Fecha Castigo'\n"
                + "                                                               , UU.monto_total_recibit\n"
                + "                                                               , UU.fld_ced\n"
                + "                                                               , UU.id_condonacion\n"
                + "                                                               , (\n"
                + "                                                                            select\n"
                + "                                                                                       isnull(min(te.dv_CunetaOrigen), (\n"
                + "                                                                                                  select\n"
                + "                                                                                                             min(us.alias)\n"
                + "                                                                                                  from\n"
                + "                                                                                                             condonacion cooo\n"
                + "                                                                                                             inner join\n"
                + "                                                                                                                        colaborador colll\n"
                + "                                                                                                                        on\n"
                + "                                                                                                                                   colll.id=cooo.di_fk_idColadorador\n"
                + "                                                                                                             inner join\n"
                + "                                                                                                                        usuario us\n"
                + "                                                                                                                        on\n"
                + "                                                                                                                                   us.id_usuario=colll.id_usuario\n"
                + "                                                                                                  where\n"
                + "                                                                                                             cooo.id_condonacion=UU.id_condonacion\n"
                + "                                                                                       )\n"
                + "                                                                                       )\n"
                + "                                                                            from\n"
                + "                                                                                       trackin_estado te\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado_condonacion est\n"
                + "                                                                                                  on\n"
                + "                                                                                                             est.di_idEstadoCondonacion=te.di_fk_idEstadoCondonacion\n"
                + "                                                                                       inner join\n"
                + "                                                                                                  estado es\n"
                + "                                                                                                  on\n"
                + "                                                                                                             es.di_id_Estado=est.di_fk_IdEstado\n"
                + "                                                                            where\n"
                + "                                                                                       est.di_fk_IdCondonacion=UU.id_condonacion\n"
                + "                                                                                       and\n"
                + "                                                                                       (\n"
                + "                                                                                                  te.dv_estadoInicio in ('Estado.Nueva'\n"
                + "                                                                                                                       ,'Estado.Nuevo')\n"
                + "                                                                                                  or te.dv_ModuloOrigen = 'Ejecutivo.Reparadas'\n"
                + "                                                                                       )\n"
                + "                                                                 )\n"
                + "                                                                 'Ejecutivo Origen'\n"
                + "                                                      from\n"
                + "                                                                 (\n"
                + "                                                                           select\n"
                + "                                                                                     GG.rut\n"
                + "                                                                                   , GG.nombre\n"
                + "                                                                                   , GG.id_condonacion\n"
                + "                                                                                   , GG.timestap\n"
                + "                                                                                   , GG.regla\n"
                + "                                                                                   , GG.dv_estado\n"
                + "                                                                                   , GG.comentario_resna\n"
                + "                                                                                   , GG.monto_total_condonado\n"
                + "                                                                                   , GG.monto_total_recibit\n"
                + "                                                                                   , GG.di_num_opers\n"
                + "                                                                                   , GG.monto_total_capital\n"
                + "                                                                                   , GG.dv_desc\n"
                + "                                                                                   , GG.di_rut\n"
                + "                                                                                   , count(rep.di_fk_IdCondonacion) 'num_reparos'\n"
                + "                                                                                   , cast(\n"
                + "                                                                                           (\n"
                + "                                                                                                  select\n"
                + "                                                                                                         top 1 *\n"
                + "                                                                                                  from\n"
                + "                                                                                                         [dbo].[Concatena_CED] (GG.rut)\n"
                + "                                                                                          )\n"
                + "                                                                                          as varchar) as fld_ced\n"
                + "                                                                           from\n"
                + "                                                                                     (\n"
                + "                                                                                                select\n"
                + "                                                                                                           cli.rut\n"
                + "                                                                                                         , cli.nombre\n"
                + "                                                                                                         , con.id_condonacion\n"
                + "                                                                                                         , con.timestap\n"
                + "                                                                                                         , re.[desc] 'regla'\n"
                + "                                                                                                         , est.dv_estado\n"
                + "                                                                                                         , con.comentario_resna\n"
                + "                                                                                                         , con.monto_total_condonado\n"
                + "                                                                                                         , con.monto_total_recibit\n"
                + "                                                                                                         , con.di_num_opers\n"
                + "                                                                                                         , con.monto_total_capital\n"
                + "                                                                                                         , tpcon.dv_desc\n"
                + "                                                                                                         , usu.di_rut\n"
                + "                                                                                                from\n"
                + "                                                                                                           condonacion con\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      regla re\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 re.id_regla=con.id_regla\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      estado est\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 est.di_id_Estado=con.id_estado\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      tipo_condonacion tpcon\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      colaborador cola\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 cola.id=con.di_fk_idColadorador\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      usuario usu\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 usu.id_usuario=cola.id_usuario\n"
                + "                                                                                                           inner join\n"
                + "                                                                                                                      cliente cli\n"
                + "                                                                                                                      on\n"
                + "                                                                                                                                 cli.id_cliente=con.di_fk_IdCliente\n"
                + "                                                                                                where\n"
                + "                                                                                                           con.id_estado in\n"
                + "                                                                                                           (\n"
                + "                                                                                                                  select\n"
                + "                                                                                                                         est.di_id_Estado\n"
                + "                                                                                                                  from\n"
                + "                                                                                                                         estado est\n"
                + "                                                                                                                  where\n"
                + "                                                                                                                         est.dv_estado ='Aplicada Aplicacion'\n"
                + "                                                                                                           )\n"
                + "                                                                                     )\n"
                + "                                                                                     as GG\n"
                + "                                                                                     left join\n"
                + "                                                                                               reparo rep\n"
                + "                                                                                               on\n"
                + "                                                                                                         rep.di_fk_IdCondonacion=GG.id_condonacion\n"
                + "                                                                           group by\n"
                + "                                                                                     GG.rut\n"
                + "                                                                                   , GG.nombre\n"
                + "                                                                                   , GG.id_condonacion\n"
                + "                                                                                   , GG.timestap\n"
                + "                                                                                   , GG.regla\n"
                + "                                                                                   , GG.dv_estado\n"
                + "                                                                                   , GG.comentario_resna\n"
                + "                                                                                   , GG.monto_total_condonado\n"
                + "                                                                                   , GG.monto_total_recibit\n"
                + "                                                                                   , GG.di_num_opers\n"
                + "                                                                                   , GG.monto_total_capital\n"
                + "                                                                                   , GG.dv_desc\n"
                + "                                                                                   , GG.di_rut\n"
                + "                                                                 )\n"
                + "                                                                 as UU\n"
                + "                                                                 INNER JOIN\n"
                + "                                                                            dbo.traking_morahoy_clie tmc\n"
                + "                                                                            ON\n"
                + "                                                                                       id_condonacion = tmc.fk_idCondonacion\n"
                + "                                           )\n"
                + "                                           as LL\n"
                + "                                    where\n"
                + "                                           LL.tiempo_aplicacion is not null\n"
                + "                             )\n"
                + "                             as OO\n"
                + "                             inner join\n"
                + "                                        tbl_siscon_condonaciones as ff\n"
                + "                                        on\n"
                + "                                                   ff.id_condonacion=OO.id_condonacion\n"
                + "                  where\n"
                + "                             fld_ced                = 'BCI'  and  tiempo_aplicacion >= CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and tiempo_aplicacion <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101) \n"
                + "       )\n"
                + "       as HH \n"
                + "";

        //Messagebox.show("SQLQUERYRESUMEN:["+SQL+"]");
        List<APPResumenCondonacionesInforme> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperResumenInformes());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<CondonacionTabla2> GetCondonacionesRechazadas() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "SELECT \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    count(rep.di_fk_IdCondonacion) 'num_reparos',\n"
                + "    cr.fecIngreso fec_rechazo,\n"
                + "    tr.Detalle tipo_rechazo,\n"
                + "    GG.fld_ced \n"
                + "FROM (\n"
                + "	   SELECT \n"
                + "		  cli.rut,\n"
                + "		  cli.nombre,\n"
                + "		  con.id_condonacion,\n"
                + "		  con.timestap,\n"
                + "		  re.[desc] 'regla',\n"
                + "		  est.dv_estado,\n"
                + "		  con.comentario_resna,\n"
                + "		  con.monto_total_condonado,\n"
                + "		  con.monto_total_recibit,\n"
                + "		  con.di_num_opers,\n"
                + "		  con.monto_total_capital,\n"
                + "		  tpcon.dv_desc,\n"
                + "		  usu.di_rut,\n"
                + "		  ISNULL (cedr.cedente,'OTRO') fld_ced \n"
                + "	   FROM condonacion con \n"
                + "        INNER JOIN regla re on re.id_regla=con.id_regla \n"
                + "        INNER JOIN estado est on est.di_id_Estado=con.id_estado \n"
                + "        INNER JOIN tipo_condonacion tpcon on tpcon.di_idtipoCondonacion=con.di_fk_tipocondonacion \n"
                + "        INNER JOIN colaborador cola on cola.id=con.di_fk_idColadorador  \n"
                + "        INNER JOIN usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "        INNER JOIN cliente cli on cli.id_cliente=con.di_fk_IdCliente \n"
                + "        LEFT JOIN cedente_rut cedr on (cedr.rut=(LEFT(cli.rut,CHARINDEX('-',cli.rut)-1))) \n"
                //   + "        INNER JOIN dbo.traking_morahoy_clie tmc ON con.id_condonacion = tmc.fk_idCondonacion\n"
                + "    ) as GG          \n"
                + "LEFT JOIN reparo rep on GG.id_condonacion = rep.di_fk_IdCondonacion \n"
                + "INNER JOIN dbo.Cond_Rechazadas cr ON GG.id_condonacion = cr.fk_idCondonacion\n"
                + "INNER JOIN dbo.Tipo_Rechazo tr ON cr.fk_idTipRechazo = tr.id_TipRechazo\n"
                + "GROUP BY \n"
                + "    GG.rut,\n"
                + "    GG.nombre,\n"
                + "    GG.id_condonacion,\n"
                + "    GG.timestap,\n"
                + "    GG.regla,\n"
                + "    GG.dv_estado,\n"
                + "    GG.comentario_resna,\n"
                + "    GG.monto_total_condonado,\n"
                + "    GG.monto_total_recibit,\n"
                + "    GG.di_num_opers,\n"
                + "    GG.monto_total_capital,\n"
                + "    GG.dv_desc,GG.di_rut,\n"
                + "    cr.fecIngreso,\n"
                + "    tr.Detalle,\n"
                + "    GG.fld_ced \n"
                + "ORDER BY cr.fecIngreso DESC";
        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApplyRechazadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones rechazadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<CondonacionTabla2> GetCondonacionesProrrogadas() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        //// Agregar tracking consulta recepcion Aplicacion
        String SQL = "SELECT \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    COUNT(REP.DI_FK_IDCONDONACION) 'NUM_REPAROS',\n"
                + "    CP.FECINGRESO FEC_PRORROGA,\n"
                + "    TP.DETALLE TIPO_PRORROGA,\n"
                + "    GG.FLD_CED \n"
                + "FROM (\n"
                + "        SELECT \n"
                + "            CLI.RUT,\n"
                + "            CLI.NOMBRE,\n"
                + "            CON.ID_CONDONACION,\n"
                + "            CON.TIMESTAP,\n"
                + "            RE.[DESC] 'REGLA',\n"
                + "            EST.DV_ESTADO,\n"
                + "            CON.COMENTARIO_RESNA,\n"
                + "            CON.MONTO_TOTAL_CONDONADO,\n"
                + "            CON.MONTO_TOTAL_RECIBIT,\n"
                + "            CON.DI_NUM_OPERS,\n"
                + "            CON.MONTO_TOTAL_CAPITAL,\n"
                + "            TPCON.DV_DESC,\n"
                + "            USU.DI_RUT,\n"
                + "            cast((select top 1 * from [dbo].[Concatena_CED] (cli.rut)) as varchar)  as  fld_ced \n"
                + "        FROM CONDONACION CON \n"
                + "        INNER JOIN REGLA RE ON RE.ID_REGLA=CON.ID_REGLA \n"
                + "        INNER JOIN ESTADO EST ON EST.DI_ID_ESTADO=CON.ID_ESTADO \n"
                + "        INNER JOIN TIPO_CONDONACION TPCON ON TPCON.DI_IDTIPOCONDONACION=CON.DI_FK_TIPOCONDONACION \n"
                + "        INNER JOIN COLABORADOR COLA ON COLA.ID=CON.DI_FK_IDCOLADORADOR  \n"
                + "        INNER JOIN USUARIO USU ON USU.ID_USUARIO=COLA.ID_USUARIO \n"
                + "        INNER JOIN CLIENTE CLI ON CLI.ID_CLIENTE=CON.DI_FK_IDCLIENTE \n"
                //   + "        INNER JOIN DBO.TRAKING_MORAHOY_CLIE TMC ON CON.ID_CONDONACION = TMC.FK_IDCONDONACION\n"
                + "        AND CON.ID_ESTADO IN (" + _estados._buscarIDEstado("APP-PRORROGA") + ")\n"
                + "    ) AS GG          \n"
                + "LEFT JOIN REPARO REP ON GG.ID_CONDONACION = REP.DI_FK_IDCONDONACION \n"
                + "INNER JOIN DBO.COND_PRORROGADAS CP ON GG.ID_CONDONACION = CP.FK_IDCONDONACION\n"
                + "INNER JOIN DBO.TIPO_PRORROGA TP ON CP.FK_IDTIPOPRORROGA = TP.ID_TIPOPRORROGA\n"
                + "GROUP BY \n"
                + "    GG.RUT,\n"
                + "    GG.NOMBRE,\n"
                + "    GG.ID_CONDONACION,\n"
                + "    GG.TIMESTAP,\n"
                + "    GG.REGLA,\n"
                + "    GG.DV_ESTADO,\n"
                + "    GG.COMENTARIO_RESNA,\n"
                + "    GG.MONTO_TOTAL_CONDONADO,\n"
                + "    GG.MONTO_TOTAL_RECIBIT,\n"
                + "    GG.DI_NUM_OPERS,\n"
                + "    GG.MONTO_TOTAL_CAPITAL,\n"
                + "    GG.DV_DESC,GG.DI_RUT,\n"
                + "    CP.FECINGRESO,\n"
                + "    TP.DETALLE,\n"
                + "    GG.FLD_CED \n"
                + "ORDER BY CP.FECINGRESO DESC";

        List<CondonacionTabla2> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new CondonacionMapperApplyProrrogadas());
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) usuario(a), ha ocurrido un error al traer las condonaciones prorrogadas.\n\nError: " + e.getMessage(), "Siscon-Admin", Messagebox.OK, Messagebox.ERROR);
            dsol = null;
        } finally {
            return dsol;
        }
    }

    public List<Tabla_Condonacion> GetCondonaciones() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        final String SQL = " select * from condonacion";
        List<Tabla_Condonacion> dsol = null;
        try {
            dsol = jdbcTemplate.query(SQL, new Tabla_Condonacion());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public int isCliente(String rut) {
        /*La consulta retorna 3 valores
        0 = cliente nuevo (retorna error, en error se captura 0, puede evaluar).
        1 = cliente existe (sistema no permite evaluación).        
        2 = cliente rechazado(puede volver a evaluar).
         */
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "SELECT TOP 1  IIF(COND.ID_ESTADO in (?,?),2,IIF(CLI.RUT IS NOT NULL,1,0))\n"
                + "FROM DBO.CLIENTE CLI \n"
                + "LEFT JOIN DBO.CONDONACION COND ON CLI.ID_CLIENTE = COND.DI_FK_IDCLIENTE \n"
                + "WHERE CLI.RUT = ?\n"
                + "ORDER BY COND.timestap DESC";
        int isCliente = 0;

        try {
            isCliente = jdbcTemplate.queryForInt(sql, new Object[]{_estados._buscarIDEstado("APP-RECHA"), _estados._buscarIDEstado("AN-RECHA"), rut});
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "] estado [:"+_estados._buscarIDEstado("APP-RECHA")+"] estado2 ["+_estados._buscarIDEstado("AN-RECHA")+"] rut : ["+rut+"]" + ex.getMessage());

            if (ex.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
                isCliente = 0;
            }
        }

        SisCorelog("Query :[" + sql + "]");
        return isCliente;

    }

    public List<siscon_inf_clienteV2> getInfClienV2() {

        String sql = "select * from siscon_inf_clienteV2";
        List<siscon_inf_clienteV2> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(siscon_inf_clienteV2.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
    
        public List<siscon_inf_clienteV2> getInfClienV3() {

        String sql = "select hh.* from siscon_inf_clienteV2 hh left join condonacion con on con.id_condonacion=hh.id_condonacion order by con.timestap desc";
        List<siscon_inf_clienteV2> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(siscon_inf_clienteV2.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }

              public List<InformeEfecv2> getInfClienV4() {

        String sql = "	  select colab.Nombre_Colab,colab.Unidad,colab.Nombre_Lugar_de_Trabajo,colab.Nombre_Jefe_Completo, hh.*,siope.porcentaje_cond_cap,siope.porcentaje_cond_honor,siope.porcentaje_cond_int,con.timestap'creacion' from siscon_inf_clienteV2 hh \n" +
"	  left join   condonacion con on con.id_condonacion=hh.id_condonacion \n" +
"	  left join siscon_inf_operacionV2 siope on siope.rut_cliente=hh.rut_cliente\n" +
"	  INNER JOIN In_cbza..TBL_BASE_DATOS_COLABORADORES AS colab on colab.Login_Name = hh.ejecutivo_condona where con.timestap >= CONVERT(datetime,+CONVERT (date, FORMAT(getdate(),'yyyy-MM') +'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) order by con.timestap desc";
        List<InformeEfecv2> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(InformeEfecv2.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
              
              
              
              
              
              public List<InformeEfecv2> getInfClienV4(String Periodo) {

        String sql = "	  select  colab.Nombre_Colab,colab.Unidad,colab.Nombre_Lugar_de_Trabajo,colab.Nombre_Jefe_Completo, hh.*,siope.porcentaje_cond_cap,siope.porcentaje_cond_honor,siope.porcentaje_cond_int,con.timestap'creacion' from siscon_inf_clienteV2 hh \n" +
"	  left join   condonacion con on con.id_condonacion=hh.id_condonacion \n" +
"	  left join siscon_inf_operacionV2 siope on siope.rut_cliente=hh.rut_cliente\n" +
"	  INNER JOIN In_cbza..TBL_BASE_DATOS_COLABORADORES AS colab on colab.Login_Name = hh.ejecutivo_condona \n" +
              "	  where  con.timestap >= CONVERT(datetime,+CONVERT (date,'" + Periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101)    and con.timestap <= CONVERT(datetime,+CONVERT (date, dateadd(day,-1,left(convert(varchar(8),dateadd(month,1, CONVERT(datetime,+CONVERT (date,'" + Periodo + "'+'-01'),101)+CONVERT(datetime,+'00:00:00.000',101) ),112),6)+'01')),101)+CONVERT(datetime,+'23:59:59.000',101) \n" +  
                 " order by con.timestap desc";
        List<InformeEfecv2> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(InformeEfecv2.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
    public List<SisconinfoperacionV2> getInfClienOperacionesV2(int IdCondonacion, int RutCliente) {

        String sql = "select \n"
                + "\n"
                + "dd.[id]\n"
                + "      ,dd.[nro_operacion]\n"
                + "      ,dd.[nro_operacion_ori]\n"
                + "      ,dd.[rut_cliente]\n"
                + "      ,dd.[capital]\n"
                + "      ,dd.[porcentaje_cond_cap]\n"
                + "      ,dd.[interes]\n"
                + "      ,dd.[porcentaje_cond_int]\n"
                + "      ,dd.[honorarios]\n"
                + "      ,dd.[porcentaje_cond_honor]\n"
                + "      ,dd.[fk_id_infcliente]\n"
                + "      ,dd.[capital_recibe]\n"
                + "      ,dd.[interes_recibe]\n"
                + "      ,dd.[honorario_recibe]\n"
                + "      ,dd.[capital_condona]\n"
                + "      ,dd.[interes_condona]\n"
                + "      ,dd.[honorario_condona]\n"
                + "      ,dd.[judicial_prejudicial_operori]\n"
                + "      ,dd.[judicial_prejudicial_oper]\n"
                + "\n"
                + "\n"
                + "\n"
                + "from siscon_inf_clienteV2    uu \n"
                + "inner join siscon_inf_operacionV2 dd on dd.fk_id_infcliente=uu.id\n"
                + "\n"
                + "where uu.id_condonacion=" + IdCondonacion + "";
        List<SisconinfoperacionV2> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(SisconinfoperacionV2.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }

    public List<CondonacionesEjecutivos> getInfEjecutivoCondonacion() {

        String sql = "select infcli.ejecutivo_condona,count(*)numero_condonaciones,max(infcli.fecha_aplicacion) ultimaCondonacionAplicada,max(infcli.fecha_aprobacion) ultimaCondonacionaprovada,sum(infcli.numero_reparos) total_reparos  from siscon_inf_clienteV2 infcli  group by infcli.ejecutivo_condona order by numero_condonaciones desc";
        List<CondonacionesEjecutivos> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(CondonacionesEjecutivos.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }

    public List<AccesosEjecutivo> getInfEjecutivoAccessos() {

        String sql = "select * from (\n"
                + "select HH.cuenta 'ejecutivo_condona',max(HH.fecha) 'fec',count(HH.cuenta)numero_accesos from(\n"
                + "select ht.cuenta,ht.id,count(ht.cuenta)numerointentos,max(ht.registrado)fecha from http_session  ht\n"
                + "inner join http_session_acciones hta on hta.http_sessionID=ht.id\n"
                + "\n"
                + "group by ht.cuenta,ht.id \n"
                + "\n"
                + ") as HH\n"
                + "\n"
                + "group by HH.cuenta\n"
                + "\n"
                + ") as GG\n"
                + "order by GG.fec desc";
        List<AccesosEjecutivo> inf_clie = null;
        jdbcTemplateObject = new JdbcTemplate(dataSource);
        //   Messagebox.show(sql);
        try {
            inf_clie = jdbcTemplateObject.query(sql, new BeanPropertyRowMapper(AccesosEjecutivo.class));
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "]" + ex.getMessage());

        }
        return inf_clie;
    }
    
    
    public Date FechaCondonacion(int condonacion){
           String id_Cli = "NULL",sql_peridocond;
           TipoDatoDate fecha = null;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        int periodo;
        jdbcTemplateObject = new JdbcTemplate(dataSource);

        
                  // buscar periodo de la condonacion
          sql_peridocond = "select  timestap from condonacion con where con.id_condonacion=?";

          try {
         fecha = jdbcTemplateObject.queryForObject(sql_peridocond, new Object[]{condonacion}, new RowMapper<TipoDatoDate>() {
            public TipoDatoDate mapRow(ResultSet rs, int rowNum) throws SQLException {
                TipoDatoDate f = new TipoDatoDate();
                f.setRegistrado(rs.getDate("timestap"));
    

                return f;
            }
        });
          } catch (DataAccessException e) {
              periodo = 0;
              logger.info("#########getClienteOferta_periodocond 430  - {}   sql_peridocond{}", sql_peridocond);
          }

        logger.info("#########PERIDO 430  - {}   sql_peridocond{}", fecha.getRegistrado());
        
          

        return fecha.getRegistrado();
    }
    

}
