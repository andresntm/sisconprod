/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Unidad_Medida;
import siscon.entidades.interfaces.Unidad_MedidaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Unidad_MedidaImpl implements Unidad_MedidaInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public Unidad_MedidaImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertUnidad_Medida(final Unidad_Medida unidad_Medida) {
        int id_rangof;

        final String SQL = "INSERT INTO [dbo].[unidad_medida]\n"
                + "           ([descripcion]\n"
                + "           ,[simbolo])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_undmed"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, unidad_Medida.getDescripcion());
                    ps.setString(2, unidad_Medida.getSimbolo());
                    return ps;
                }
            },
                    keyHolder);

            id_rangof = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_rangof = 0;

        }

        return id_rangof;
    }

    public boolean updateUnidad_Medida(Unidad_Medida unidad_Medida) {
        final String SQL = "UPDATE [dbo].[unidad_medida]\n"
                + "   SET [descripcion] = ?\n"
                + "      ,[simbolo] = ?\n"
                + " WHERE id_undmed = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                unidad_Medida.getDescripcion(),
                unidad_Medida.getSimbolo(),
                unidad_Medida.getId_undmed(),});
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Unidad_Medida getUnidad_MedidaXId(final int id_undmed) {
        Unidad_Medida unidadMedida = new Unidad_Medida();
        final String SQL = "SELECT * FROM unidad_medida WHERE id_undmed = ?";

        try {
            unidadMedida = jdbcTemplate.queryForObject(SQL, new Object[]{id_undmed}, new RowMapper<Unidad_Medida>() {
                public Unidad_Medida mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Unidad_Medida unidadMeidad = new Unidad_Medida();

                    unidadMeidad.setId_undmed(rs.getInt("id_undmed"));
                    unidadMeidad.setDescripcion(rs.getString("descripcion"));
                    unidadMeidad.setSimbolo(rs.getString("simbolo"));
                    return unidadMeidad;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return unidadMedida;
    }

    public List<Unidad_Medida> getUnidad_Medida() {
        List<Unidad_Medida> lUnidadMedida = new ArrayList<Unidad_Medida>();
        final String SQL = "SELECT * FROM unidad_medida WHERE id_undmed = ?";

        try {
            lUnidadMedida = jdbcTemplate.query(SQL, new RowMapper<Unidad_Medida>() {
                public Unidad_Medida mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Unidad_Medida unidadMeidad = new Unidad_Medida();

                    unidadMeidad.setId_undmed(rs.getInt("id_undmed"));
                    unidadMeidad.setDescripcion(rs.getString("descripcion"));
                    unidadMeidad.setSimbolo(rs.getString("simbolo"));
                    return unidadMeidad;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return lUnidadMedida;
    }

}
