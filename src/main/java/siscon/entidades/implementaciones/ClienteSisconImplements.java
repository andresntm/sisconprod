/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.ClienteSiscon;
import siscon.entidades.interfaces.ClienteSisconInterfaz;

/**
 *
 * @author excosoc
 */
public class ClienteSisconImplements implements ClienteSisconInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public ClienteSisconImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public ClienteSisconImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

//    public ClienteSiscon getCliente(int id_cliente) {
//        ClienteSiscon cS = new ClienteSiscon();
//
//        final String sql = "SELECT \n"
//                + "C.ID_CLIENTE\n"
//                + ",C.APELLIDO_MATERNO\n"
//                + ",C.APELLIDO_PATERNO\n"
//                + ",C.NOMBRE\n"
//                + ",C.NOMBRE + ' ' + APELLIDO_PATERNO + ' ' + APELLIDO_MATERNO NOMBRES\n"
//                + ",C.RUT\n"
//                + ",C.ID_USUARIO\n"
//                + ",CD.ID_CEDENTE\n"
//                + "FROM CLIENTE C LEFT JOIN CLIENTE_CEDENTE CD ON C.ID_CLIENTE = CD.ID_CLIENTE\n"
//                + "WHERE C.ID_CLIENTE = ?;";
//        try {
//            cS = jdbcTemplateObject.queryForObject(sql, new Object[]{id_cliente}, new ClienteSiscon());
//
//        } catch (DataAccessException ex) {
//            cS = null;
//        }
//
//        return cS;
//    }
    public ClienteSiscon getCliente(int id_cliente) {
        ClienteSiscon cS = new ClienteSiscon();

        final String sql = "SELECT \n"
                + "C.ID_CLIENTE\n"
                + ",C.APELLIDO_MATERNO\n"
                + ",C.APELLIDO_PATERNO\n"
                + ",C.NOMBRE\n"
                + ",C.NOMBRE + ' ' + APELLIDO_PATERNO + ' ' + APELLIDO_MATERNO NOMBRES\n"
                + ",C.RUT\n"
                + ",C.ID_USUARIO\n"
                + "FROM CLIENTE C\n"
                + "WHERE C.ID_CLIENTE = ?;";
        try {
            cS = jdbcTemplateObject.queryForObject(sql, new Object[]{id_cliente}, new ClienteSiscon());

        } catch (DataAccessException ex) {
            cS = null;
        }

        return cS;
    }

    public ClienteSiscon getCliente(String rut) {
        ClienteSiscon cS = new ClienteSiscon();

        final String sql = "SELECT \n"
                + "C.ID_CLIENTE\n"
                + ",C.APELLIDO_MATERNO\n"
                + ",C.APELLIDO_PATERNO\n"
                + ",C.NOMBRE\n"
                + ",C.NOMBRE + ' ' + APELLIDO_PATERNO + ' ' + APELLIDO_MATERNO NOMBRES\n"
                + ",C.RUT\n"
                + ",C.ID_USUARIO\n"
                + ",CD.ID_CEDENTE\n"
                + "FROM CLIENTE C LEFT JOIN CLIENTE_CEDENTE CD ON C.ID_CLIENTE = CD.ID_CLIENTE\n"
                + "WHERE C.RUT = ?;";
        try {
            cS = jdbcTemplateObject.queryForObject(sql, new Object[]{rut}, new ClienteSiscon());

        } catch (DataAccessException ex) {
            cS = null;
        }

        return cS;
    }

    public ClienteSiscon RegeneraCliente(String rut) {
        ClienteSiscon cS = new ClienteSiscon();

        String sql = "SELECT CASE\n"
                + "        WHEN (LTRIM(RTRIM(NOMBRES)) IS NULL\n"
                + "                OR LEN(NOMBRES) < 1)\n"
                + "        THEN LTRIM(RTRIM(REPLACE( REPLACE(RAZ_SOC,'                  ',' '),'                 ',' ')))\n"
                + "        ELSE LTRIM(RTRIM(NOMBRES))+' '+LTRIM(RTRIM(APEPAT))+' '+LTRIM(RTRIM(APEMAT))\n"
                + "	   END NOMBRES,\n"
                + "    CONVERT(VARCHAR, RUT)+'-'+DV RUT,\n"
                + "    APEPAT APELLIDO_MATERNO,\n"
                + "    APEMAT APELLIDO_PATERNO,\n"
                + "    NOMBRES NOMBRE,\n"
                + "    IN_CBZA.DBO.FN_CED(RUT) CEDENTE\n"
                + "FROM IN_CBZA.DBO.IN_DBC DBC WITH (NOLOCK)\n"
                + "WHERE (CONVERT(VARCHAR, RUT)+'-'+DV) = ?;";
        try {
            cS = jdbcTemplateObject_prod.queryForObject(sql, new Object[]{rut}, new ClienteSiscon(true));

        } catch (DataAccessException ex) {
            cS = null;
        }

        String SQL = " UPDATE CLIENTE_INTEGRA\n"
                + " SET\n"
                + "     --ID_CLIENTE\n"
                + "    APELLIDO_MATERNO = ?\n"
                + ",    APELLIDO_PATERNO = ?\n"
                + ",    NOMBRE = ?\n"
                + ",    fk_id_cedente= ?\n"
                + "WHERE RUT = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                cS.getApellidoMaterno(),
                cS.getApellidoPaterno(),
                cS.getNombre(),
                cS.getFk_id_cedente(),
                cS.getRut()
            });
        } catch (DataAccessException e) {
        }

        return cS;

    }

    public int traeIdCedenteCliente(String cedente) {
        int id_cedente = 0;

        final String sql = "SELECT \n"
                + "ID\n"
                + "FROM CEDENTE\n"
                + "WHERE CODIGO = ?;";
        try {
            id_cedente = jdbcTemplateObject.queryForInt(sql, new Object[]{cedente});

        } catch (DataAccessException ex) {
            id_cedente = 0;
        }

        return id_cedente;
    }

    public List<ClienteSiscon> listClientes() {
        List<ClienteSiscon> lCS = new ArrayList<ClienteSiscon>();

        final String SQL = "SELECT \n"
                + "C.ID_CLIENTE\n"
                + ",C.APELLIDO_MATERNO\n"
                + ",C.APELLIDO_PATERNO\n"
                + ",C.NOMBRE\n"
                + ",C.NOMBRE + ' ' + APELLIDO_PATERNO + ' ' + APELLIDO_MATERNO NOMBRES\n"
                + ",C.RUT\n"
                + ",C.ID_USUARIO\n"
                + ",CD.ID_CEDENTE\n"
                + "FROM CLIENTE_INTEGRA C LEFT JOIN CLIENTE_CEDENTE CD ON C.ID_CLIENTE = CD.ID_CLIENTE\n"
                + "WHERE FK_ID_CEDENTE IS NULL";
        try {
            lCS = jdbcTemplateObject.query(SQL, new ClienteSiscon());
        } catch (DataAccessException ex) {
            lCS = null;
        }

        return lCS;
    }

    public List<ClienteSiscon> listClientes_por_cendete(int id_cedente) {
        List<ClienteSiscon> lCS = new ArrayList<ClienteSiscon>();

        final String SQL = "SELECT \n"
                + "C.ID_CLIENTE\n"
                + ",C.APELLIDO_MATERNO\n"
                + ",C.APELLIDO_PATERNO\n"
                + ",C.NOMBRE\n"
                + ",C.NOMBRE + ' ' + APELLIDO_PATERNO + ' ' + APELLIDO_MATERNO NOMBRES\n"
                + ",C.RUT\n"
                + ",C.ID_USUARIO\n"
                + ",CD.ID_CEDENTE\n"
                + "FROM CLIENTE C LEFT JOIN CLIENTE_CEDENTE CD ON C.ID_CLIENTE = CD.ID_CLIENTE\n"
                + "WHERE CD.ID_CEDENTE = ?;";
        try {
            lCS = jdbcTemplateObject.query(SQL, new Object[]{id_cedente}, new ClienteSiscon(true));
        } catch (DataAccessException ex) {
            lCS = null;
        }

        return lCS;
    }

    public int insertCliente(final ClienteSiscon cS) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO CLIENTE \n"
                + "(\n"
                + "     --ID_CLIENTE\n"
                + "     APELLIDO_MATERNO\n"
                + "     ,APELLIDO_PATERNO\n"
                + "     ,NOMBRE\n"
                + "     ,RUT\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?,\n"
                + "     ?\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID_CLIENTE"});

//                    ps.setInt(1, cS.getId_cliente());
                    ps.setString(1, cS.getApellidoMaterno());
                    ps.setString(2, cS.getApellidoPaterno());
                    ps.setString(3, cS.getNombre());
                    ps.setString(4, cS.getRut());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateCliente(ClienteSiscon cS) {
        String SQL = " UPDATE CLIENTE\n"
                + " SET\n"
                + "     --ID_CLIENTE\n"
                + ",    APELLIDO_MATERNO = ?\n"
                + ",    APELLIDO_PATERNO = ?\n"
                + ",    NOMBRE = ?\n"
                + "WHERE ID_CLIENTE = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                cS.getApellidoMaterno(),
                cS.getApellidoPaterno(),
                cS.getNombre(),
                cS.getId_cliente()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public int insertCliente_Cendente(final ClienteSiscon cS) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO CLIENTE_CEDENTE \n"
                + "(\n"
                + "     --ID\n"
                + "     ID_CLIENTE\n"
                + "     ,ID_CEDENTE\n"
                + "     --,COMENTARIO\n"
                + "     --,REGISTRADO\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "     ?,\n"
                + "     ?\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID"});

//                    ps.setInt(1, cS.getId_cliente());
                    ps.setInt(1, cS.getId_cliente());
                    ps.setInt(2, cS.getFk_id_cedente());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

}
