/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import siscon.entidades.EstadoCondonacion;
import siscon.entidades.interfaces.EstadoCondonacionInterfaz;

/**
 *
 * @author excosoc
 */
public class EstadoCondonacionImplement implements EstadoCondonacionInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public EstadoCondonacionImplement() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public EstadoCondonacionImplement(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public EstadoCondonacion getEstadoCondonacion(int id_EstadoCondonacion) {
        EstadoCondonacion eC = new EstadoCondonacion();

        final String sql = "SELECT di_idEstadoCondonacion\n"
                + ",di_fk_IdEstado\n"
                + ",di_fk_IdCondonacion\n"
                + ",ddt_fechaReistro\n"
                + "FROM estado_condonacion\n"
                + "WHERE di_idEstadoCondonacion = ?;";
        try {
            eC = jdbcTemplateObject.queryForObject(sql, new Object[]{id_EstadoCondonacion}, new EstadoCondonacion());

        } catch (DataAccessException ex) {
            eC = null;
        }

        return eC;
    }

    public List<EstadoCondonacion> listEstadoCondonacion() {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        final String SQL = "SELECT di_idEstadoCondonacion\n"
                + ",di_fk_IdEstado\n"
                + ",di_fk_IdCondonacion\n"
                + ",ddt_fechaReistro\n"
                + "FROM estado_condonacion\n";
        try {
            lEC = jdbcTemplateObject.query(SQL, new EstadoCondonacion());
        } catch (DataAccessException ex) {
            lEC = null;
        }

        return lEC;
    }

    public List<EstadoCondonacion> listEstadoCondonacion_X_IdCondonacion(int IdCondonacion) {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        final String SQL = "SELECT di_idEstadoCondonacion\n"
                + ",di_fk_IdEstado\n"
                + ",di_fk_IdCondonacion\n"
                + ",ddt_fechaReistro\n"
                + "FROM estado_condonacion\n"
                + "WHERE di_fk_IdCondonacion = ?";
        try {
            lEC = jdbcTemplateObject.query(SQL, new Object[]{IdCondonacion}, new EstadoCondonacion());
        } catch (DataAccessException ex) {
            lEC = null;
        }

        return lEC;
    }

    public List<EstadoCondonacion> listEstadoCondonacion_X_IdEstado(int IdEstado) {
        List<EstadoCondonacion> lEC = new ArrayList<EstadoCondonacion>();

        final String SQL = "SELECT di_idEstadoCondonacion\n"
                + ",di_fk_IdEstado\n"
                + ",di_fk_IdCondonacion\n"
                + ",ddt_fechaReistro\n"
                + "FROM estado_condonacion\n"
                + "WHERE di_fk_IdCondonacion = ?";
        try {
            lEC = jdbcTemplateObject.query(SQL, new Object[]{IdEstado}, new EstadoCondonacion());
        } catch (DataAccessException ex) {
            lEC = null;
        }

        return lEC;
    }

}
