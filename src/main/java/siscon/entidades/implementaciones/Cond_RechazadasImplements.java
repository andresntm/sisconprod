/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Cond_Rechazadas;
import siscon.entidades.interfaces.Cond_RechazadasInterfaz;
import org.springframework.jdbc.core.PreparedStatementCreator;
import java.sql.PreparedStatement;
import org.springframework.dao.DataAccessException;
import java.sql.Connection;
import java.util.ArrayList;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author excosoc
 */
public class Cond_RechazadasImplements implements Cond_RechazadasInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public Cond_RechazadasImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public Cond_RechazadasImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public int insertRechazo(final Cond_Rechazadas cR) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO dbo.Cond_Rechazadas\n"
                + "(\n"
                + "    --id_Rechazo - this column value is auto-generated\n"
                + "    fk_idCondonacion,\n"
                + "    fk_idColaborador,\n"
                + "    fk_idTipRechazo,\n"
                + "    Detalle\n"
                + "    --fecIngreso - this column value is auto-generated\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "    -- id_Rechazo - int\n"
                + "    ?, -- fk_idCondonacion - int\n"
                + "    ?, -- fk_idColaborador - int\n"
                + "    ?, -- fk_idTipRechazo - int\n"
                + "    ? -- Detalle - varchar\n"
                + "    -- fecIngreso - datetime\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"id_Rechazo"});

//                    ps.setInt(1, cR.getId_Rechazo());
                    ps.setInt(1, cR.getFk_idCondonacion());
                    ps.setInt(2, cR.getFk_idColaborador());
                    ps.setInt(3, cR.getFk_idTipRechazo());
                    ps.setString(4, cR.getDetalle());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    
    
    
    public int insertPorcentajeRechazo(final int id_rechazo,final int porcentaje_x) {
        KeyHolder key = new GeneratedKeyHolder();

        final String sql = "INSERT INTO dbo.usr_porcentajes_rechazo\n"
                + "(\n"
                + "    id_rechazo,\n"
                + "    id_usr_modif\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
               + "    ?, \n"
                + "    ? \n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"id"});

//                    ps.setInt(1, cR.getId_Rechazo());
                    ps.setInt(1, id_rechazo);
                    ps.setInt(2, porcentaje_x);
   
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            Messagebox.show("Error Guardando Id de Porcentajes ["+ex.getMessage()+"] Query ["+sql+"] id_rechazo ["+id_rechazo+"] id_usrm ["+porcentaje_x+"]");
            return 0;
        }
    }

    
    
    public boolean updateRechazo(Cond_Rechazadas cR) {
        String SQL = " update Cond_Rechazadas\n"
                + " SET\n"
                + "     --id_Rechazo - this column value is auto-generated\n"
                + "     fk_idCondonacion = ?, -- int\n"
                + "     fk_idColaborador = ?, -- int\n"
                + "     fk_idTipRechazo = ?, -- int\n"
                + "     Detalle = ? -- varchar\n"
                + "     --fecIngreso - this column value is auto-generated\n"
                + "WHERE id_Rechazo = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                cR.getFk_idCondonacion(),
                cR.getFk_idColaborador(),
                cR.getFk_idTipRechazo(),
                cR.getDetalle(),
                cR.getId_Rechazo()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Cond_Rechazadas getRechazo(int id_Rechazo) {
        Cond_Rechazadas cR = new Cond_Rechazadas();

        final String sql = "SELECT cr.id_Rechazo\n"
                + "    ,cr.fk_idCondonacion\n"
                + "    ,cr.fk_idColaborador\n"
                + "    ,cr.fk_idTipRechazo\n"
                + "    ,cr.Detalle\n"
                + "    ,cr.fecIngreso \n"
                + "FROM dbo.Cond_Rechazadas cr\n"
                + "WHERE cr.id_Rechazo = ?;";
        try {
            cR = jdbcTemplateObject.queryForObject(sql, new Object[]{id_Rechazo}, new Cond_Rechazadas());

        } catch (DataAccessException ex) {
            cR = null;
        }

        return cR;
    }

    public List<Cond_Rechazadas> listRechazos() {
        List<Cond_Rechazadas> lCR = new ArrayList<Cond_Rechazadas>();

        final String sql = "SELECT cr.id_Rechazo\n"
                + "    ,cr.fk_idCondonacion\n"
                + "    ,cr.fk_idColaborador\n"
                + "    ,cr.fk_idTipRechazo\n"
                + "    ,cr.Detalle\n"
                + "    ,cr.fecIngreso \n"
                + "FROM dbo.Cond_Rechazadas cr;";

        try {
            lCR = jdbcTemplateObject.query(sql, new Cond_Rechazadas());
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

    public List<Cond_Rechazadas> listRechazos_X_Colaborador(int id_colaborador) {
        List<Cond_Rechazadas> lCR = new ArrayList<Cond_Rechazadas>();

        final String sql = "SELECT cr.id_Rechazo\n"
                + "    ,cr.fk_idCondonacion\n"
                + "    ,cr.fk_idColaborador\n"
                + "    ,cr.fk_idTipRechazo\n"
                + "    ,cr.Detalle\n"
                + "    ,cr.fecIngreso \n"
                + "FROM dbo.Cond_Rechazadas cr\n"
                + "WHERE cr.fk_idColaborador = ?;";

        try {
            lCR = jdbcTemplateObject.query(sql, new Object[]{id_colaborador}, new Cond_Rechazadas());
        } catch (DataAccessException ex) {
            lCR = null;
        }

        return lCR;
    }

    public boolean deleteRechazo(int id_Rechazo) {
        try {
            final String sql = "DELETE FROM dbo.trackin_estado\n"
                    + "WHERE ID_TRAKINESTADO = ?;";

            jdbcTemplateObject.update(sql, new Object[]{id_Rechazo});

            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }
    }

}
