/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.ProvisionMapper;
import siscon.RowMapper.TipoProvisionMapper;
import siscon.Session.TrackingSesion;
import siscon.entidades.TipoProvision;
import siscon.entidades.UsrProvision;

/**
 *
 * @author esilvest
 */
public class ProvisionJDBC {

    private DataSource dataSource = null;
    private DataSource dataSourceSisCon = null;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private JdbcTemplate jdbcTemplateSisCon;

    public ProvisionJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);

    }

    // @Override
    //Provision TOtal de la Tabla
    public List<UsrProvision> listProvision() {
        String SQL = "SELECT tprov.dv_desc'tdsc',pro.* FROM \n"//"SELECT * FROM usr_provision where vigente='True'";

                + " usr_provision  pro \n"
                + " inner join [dbo].[tipo_provision] tprov on (pro.fk_idTipoProvision=tprov.di_id) \n"
                + " where vigente='True' \n";

        List<UsrProvision> tDoc = jdbcTemplate.query(SQL, new ProvisionMapper());
        return tDoc;
    }

    // @Override
    /// Provision filtrada por Rut y por Condonacion
    public List<UsrProvision> listProvisionXrut(String rutcliente) {
        String SQL = "SELECT tprov.dv_desc'tdsc',pro.* FROM \n"//"SELECT * FROM usr_provision where vigente='True'";

                + " usr_provision  pro \n"
                + " inner join [dbo].[tipo_provision] tprov on (pro.fk_idTipoProvision=tprov.di_id) \n"
                + " where vigente='True' and pro.rut_cliente= '" + rutcliente + "' \n";

        List<UsrProvision> tDoc = jdbcTemplate.query(SQL, new ProvisionMapper());
        return tDoc;
    }

    // @Override
    public List<UsrProvision> listProvision(String cuenta) {
        String SQL = "SELECT tprov.dv_desc'tdsc',pro.* FROM \n"//"SELECT * FROM usr_provision where vigente='True'";

                + " usr_provision  pro \n"
                + " inner join [dbo].[tipo_provision] tprov on (pro.fk_idTipoProvision=tprov.di_id) \n"
                + " where vigente='True' \n";

        List<UsrProvision> tDoc = jdbcTemplate.query(SQL, new ProvisionMapper());
        return tDoc;
    }

    public boolean EliminaProvision(int id_prov) {

        try {
            String SQL = "update usr_provision set vigente='false' where id = " + id_prov;
            jdbcTemplate.update(SQL);
            System.out.println("Updated Record with ID = " + id_prov);
            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 471: " + e.toString());
            return false;
        }

    }

    public UsrProvision selectProvision_X_Id_Provision(int id_provision) {
        UsrProvision condEsp = new UsrProvision();

        final String sql = "SELECT ID\n"
                + ",FK_IDCOLABORADOR\n"
                + ",RUT_CLIENTE\n"
                + ",NFK_IDCONDONACION\n"
                + ",REGISTRADO\n"
                + ",DV_DESC\n"
                + ",MONTO\n"
                + ",FK_IDTIPOPROVISION\n"
                + ",VIGENTE\n"
                + " FROM USR_PROVISION\n"
                + "WHERE ID = ?";
        try {
            condEsp = jdbcTemplate.queryForObject(sql, new Object[]{id_provision}, new UsrProvision());

        } catch (DataAccessException ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.INFO, ex.getMessage(), ex);
            condEsp = null;
        }

        return condEsp;
    }

}
