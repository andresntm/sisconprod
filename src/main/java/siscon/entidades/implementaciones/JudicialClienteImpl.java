/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.chart.model.CategoryModel;

import org.zkoss.chart.model.DefaultCategoryModel;

import siscon.entidades.JudicialCliente;
import siscon.entidades.interfaces.JudicialClienteInterz;

/**
 *
 * @author exesilr
 */
public class JudicialClienteImpl implements JudicialClienteInterz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;

    public JudicialClienteImpl(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public CategoryModel getModelInformes(String sss) {
        this.jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_web_inf_tot_vis_web");
        //jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("GET_EVENT_BY_TITLE").useInParameterNames("IN_TITLE").returningResultSet("events",ParameterizedBeanPropertyRowMapper.newInstance(Event.class));
        sss = "METROPOLITANA CORDILLERA";
        SqlParameterSource in = new MapSqlParameterSource().addValue("reg", sss);
        Map<String, Object> out = jdbcCall.execute(in);
        CategoryModel model;

        model = new DefaultCategoryModel();
        //private static final CategoryModel model;
        System.out.print("####### ---- aqui imorimimos el ser de output -------#######");
        for (String keys : out.keySet()) {
            System.out.println(keys);
        }

        ArrayList resultList = (ArrayList) out.get("#result-set-1");

        Map resultlMap = (Map) resultList.get(0);

        for (int nn = 0; nn < resultList.size(); nn++) {
            Map resultlMapx = (Map) resultList.get(nn);
            model.setValue("%Cpl. Castigo", ((String) resultlMapx.get("FLD_NOM")).toString(), (Double) resultlMapx.get("fld_Cumpl_Recupero"));
            model.setValue("fld_cumpl_90_vig", ((String) resultlMapx.get("FLD_NOM")).toString(), (Double) resultlMapx.get("fld_cumpl_90_vig"));
            model.setValue("fld_cumpl_180_vig", ((String) resultlMapx.get("FLD_NOM")).toString(), (Double) resultlMapx.get("fld_cumpl_180_vig"));
            model.setValue("%Cpl. Hipotecario", ((String) resultlMapx.get("FLD_NOM")).toString(), (Double) resultlMapx.get("fld_por_cpl_hip"));
            System.out.print("####### i:[" + nn + "]----EJECUTIVO[" + ((String) resultlMapx.get("FLD_NOM")).toString() + "] fld_Cumpl_Recupero :[" + ((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString() + "]  fld_cumpl_90_vig[" + ((Double) resultlMapx.get("fld_cumpl_90_vig")).toString() + "]  fld_cumpl_180_vig[" + ((Double) resultlMapx.get("fld_cumpl_180_vig")).toString() + "] fld_por_cpl_hip[" + ((Double) resultlMapx.get("fld_por_cpl_hip")).toString() + "]-------#######");
        }
//Double fld_Cumpl_Recupero = (Double) resultlMap.get("fld_Cumpl_Recupero");
//Double fld_cumpl_90_vig = (Double) resultlMap.get("fld_cumpl_90_vig");
//Double fld_cumpl_180_vig = (Double) resultlMap.get("fld_cumpl_180_vig");
//Double fld_por_cpl_hip = (Double) resultlMap.get("fld_por_cpl_hip");        

        //System.out.print("####### ---- fld_Cumpl_Recupero :["+fld_Cumpl_Recupero.toString()+"] fld_cumpl_90_vig["+fld_cumpl_90_vig.toString()+"]  fld_cumpl_180_vig["+fld_cumpl_180_vig.toString()+"] fld_por_cpl_hip["+fld_por_cpl_hip.toString()+"]-------#######");  
        System.out.print("####### ---- aqui imorimimos Prueba de Model[[[" + model.getCategories().toString() + "]]]el ser de output FIN-------#######");
        // System.out.print("Este es el contenido del SP Ejecutado["+(String) out.get("out_fld_Cumpl_Recupero")+"]");
        // Student student = new Student();
        // student.setId(id);
        // student.setName((String) out.get("out_name"));
        //student.setAge((Integer) out.get("out_age"));

        return model;
    }

    public void setDataSource(DataSource ds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<JudicialCliente> JudClie(int rut, String user) {
        List<JudicialCliente> ListJudicialCliente = new ArrayList<JudicialCliente>();

        System.out.print("####### ---- Parametros Envio SP AAAAAA//////Judical//// Rut[" + rut + "]---User[" + user + "]----#######");
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_web_jud_siscon").
                returningResultSet("rs", new ParameterizedRowMapper<JudicialCliente>() {

                    public JudicialCliente mapRow(ResultSet rs, int i) throws SQLException {

                        JudicialCliente aDetalleCliente = new JudicialCliente();
                        aDetalleCliente.setRut_cliente(rs.getInt("rut_cliente"));
                        aDetalleCliente.setCedente(rs.getString("cedente"));
                        aDetalleCliente.setOperacion(rs.getString("operacion"));
                        aDetalleCliente.setTipo_operacion(rs.getString("tipo_operacion"));
                        aDetalleCliente.setAbogado(rs.getString("abogado"));
                        aDetalleCliente.setAbogado(rs.getString("estudio"));
                        aDetalleCliente.setContralor(rs.getString("contralor"));
                        aDetalleCliente.setFecha_asignacion(rs.getString("fecha_asignacion"));
                        aDetalleCliente.setFecha_recepcion(rs.getString("fecha_recepcion"));
                        aDetalleCliente.setFecha_ingreso_demanda(rs.getString("fecha_ingreso_demanda"));
                        aDetalleCliente.setFecha_notificacion(rs.getString("fecha_notificacion"));
                        aDetalleCliente.setFecha_remate(rs.getString("fecha_remate"));
                        aDetalleCliente.setFecha_salida(rs.getString("fecha_salida"));
                        aDetalleCliente.setFecha_ultima_gestion(rs.getString("fecha_ult_gestion"));
                        aDetalleCliente.setRol(rs.getString("rol"));
                        aDetalleCliente.setTribunal(rs.getString("tribunal"));
                        aDetalleCliente.setEtapa_juicio(rs.getString("etapa_juicio"));
                        aDetalleCliente.setEstado_juicio(rs.getString("estado_juicio"));
                        aDetalleCliente.setFecha_incautacion(rs.getString("fecha_incautacion"));
                        return aDetalleCliente;//rs.getString(1);   

                    }
                ;//           private static final Logger LOG = Logger.getLogger(.class.getName());
        });
     // SqlParameterSource in = new MapSqlParameterSource().addValue("rut", "10000165").addValue("col", 1).addValue("usuario", "jliguen");
     
             SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("OPERACION", "''").addValue("col", 1).addValue("USUARIO","esilves");
        try {

            Map<String, Object> out = jdbcCall.execute(in);

            ListJudicialCliente = (List<JudicialCliente>) out.get("rs");

        } catch (Exception ex) {

            System.out.print("####### ---- ERROR de EJECUCION de SP///judicial/// ERR:[" + ex.getMessage().toString() + "]----#######");
           return ListJudicialCliente;

        }
        //List l = jdbcCall.executeFunction(List.class, Collections.EMPTY_MAP);

        System.out.print("####### ---- Parametros Envio SP Clientes BBBBBB///judicial/// Rut[" + rut + "]---User[" + user + "]----#######");

        return ListJudicialCliente;
    }

}
