/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import siscon.RowMapper.TipoProvisionMapper;
import siscon.entidades.TipoProvision;

/**
 *
 * @author esilvest
 */
public class TipoProvisionJDBC {

    private DataSource dataSource = null;
    private DataSource dataSourceSisCon = null;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private JdbcTemplate jdbcTemplateSisCon;

    public TipoProvisionJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);

    }

    // @Override
    public List<TipoProvision> listTipProvision() {
        String SQL = "SELECT * FROM tipo_provision";
        List<TipoProvision> tDoc = jdbcTemplate.query(SQL, new TipoProvisionMapper());
        return tDoc;
    }

    public TipoProvision getTipoProvision_X_Codigo(String codigo) {
        String SQL = "SELECT * FROM tipo_provision\n"
                + "WHERE dv_codtipocon = ?";
        TipoProvision tDoc = jdbcTemplate.queryForObject(SQL, new Object[]{codigo}, new TipoProvisionMapper());
        return tDoc;
    }

}
