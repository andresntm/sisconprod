/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Rel_campana_rel_perfil_regla;
import siscon.entidades.interfaces.Rel_campana_rel_perfil_reglaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Rel_campana_rel_perfil_reglaImpl implements Rel_campana_rel_perfil_reglaInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public Rel_campana_rel_perfil_reglaImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertRel_campana_rel_perfil_regla(final Rel_campana_rel_perfil_regla camPerRel) {
        int id;

        final String SQL = "INSERT INTO [dbo].[rel_campana_rel_perfil_regla]\n"
                + "           ([id_campana]\n"
                + "           ,[id_rel_perfil_regla])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, camPerRel.getId_campana());
                    ps.setInt(2, camPerRel.getId_rel_perfil_regla());
                    return ps;
                }
            },
                    keyHolder);

            id = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id = 0;

        }

        return id;
    }

    public boolean updateRel_campana_rel_perfil_regla(Rel_campana_rel_perfil_regla camPerRel) {
        final String SQL = "UPDATE [dbo].[rel_campana_rel_perfil_regla]\n"
                + "   SET [id_campana] = ?\n"
                + "      ,[id_rel_perfil_regla] =?\n"
                + " WHERE id = ";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                camPerRel.getId_campana(),
                camPerRel.getId_rel_perfil_regla(),
                camPerRel.getId()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Rel_campana_rel_perfil_regla getRel_campana_rel_perfil_reglaXId(int id) {
        Rel_campana_rel_perfil_regla camPerReg = new Rel_campana_rel_perfil_regla();

        final String SQL = "SELECT * FROM rel_campana_rel_perfil_regla WHERE id = ?";

        try {
            camPerReg = jdbcTemplate.queryForObject(SQL, new Object[]{id}, new RowMapper<Rel_campana_rel_perfil_regla>() {
                public Rel_campana_rel_perfil_regla mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Rel_campana_rel_perfil_regla camPerReg = new Rel_campana_rel_perfil_regla();

                    camPerReg.setId(rs.getInt("id"));
                    camPerReg.setId_campana(rs.getInt("id_campana"));
                    camPerReg.setId_rel_perfil_regla(rs.getInt("id_rel_perfil_regla"));
                    return camPerReg;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return camPerReg;

    }

    public List<Rel_campana_rel_perfil_regla> getRel_campana_rel_perfil_reglas() {
        List<Rel_campana_rel_perfil_regla> lCamRelReg = new ArrayList<Rel_campana_rel_perfil_regla>();

        final String SQL = "SELECT * FROM rel_campana_rel_perfil_regla";

        try {
            lCamRelReg = jdbcTemplate.query(SQL, new RowMapper<Rel_campana_rel_perfil_regla>() {
                public Rel_campana_rel_perfil_regla mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Rel_campana_rel_perfil_regla camPerReg = new Rel_campana_rel_perfil_regla();

                    camPerReg.setId(rs.getInt("id"));
                    camPerReg.setId_campana(rs.getInt("id_campana"));
                    camPerReg.setId_rel_perfil_regla(rs.getInt("id_rel_perfil_regla"));
                    return camPerReg;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return lCamRelReg;
    }

    public boolean DeleteRel_campana_rel_perfil_regla(Rel_campana_rel_perfil_regla camPerRel) {
        String SQL;

        try {
            SQL = "DELETE FROM rel_campana_rel_perfil_regla WHERE id = ?";

            jdbcTemplate.update(SQL, new Object[]{camPerRel.getId()});

            return true;
        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
            return false;
        }
    }

}
