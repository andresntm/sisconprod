/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.ReglasDet;
import siscon.entidades.ReglasEnc;
import siscon.entidades.interfaces.ReglasInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class ReglasImpl implements ReglasInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public ReglasImpl(DataSource ds) {

        this.jdbcTemplate = new JdbcTemplate(ds);

    }

    public int insertReglasEnc(final ReglasEnc reglaEnc) {
        int id_Regla;

        final String SQL = "INSERT INTO [dbo].[regla]\n"
                + "           ([desc]\n"
                + "           ,[id_rangof]\n"
                + "           ,[id_rangom])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_regla"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, reglaEnc.getDesc());
                    ps.setInt(2, reglaEnc.getId_rangof());
                    ps.setInt(3, reglaEnc.getId_rangom());
                    return ps;
                }
            },
                    keyHolder);

            id_Regla = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_Regla = 0;

        }

        return id_Regla;

    }

    public int insertReglasDet(final ReglasDet reglaDet) {
        int id_valor_regla;

        final String SQL = "INSERT INTO [dbo].[rel_valor_regla]\n"
                + "           ([id_valor]\n"
                + "           ,[id_regla]\n"
                + "           ,[porcentaje_condonacion])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_regla"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, reglaDet.getId_valor());
                    ps.setInt(2, reglaDet.getId_regla());
                    ps.setFloat(3, reglaDet.getPorcentaje_condonacion());
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    public boolean updateReglasEnc(ReglasEnc reglaEnc) {
        String SQL = "UPDATE [dbo].[regla]\n"
                + "   SET [desc] = ?\n"
                + "      ,[id_rangof] = ?\n"
                + "      ,[id_rangom] = ?\n"
                + "      ,[activo] = ?\n"
                + " WHERE id_regla = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                reglaEnc.getDesc(),
                reglaEnc.getId_rangof(),
                reglaEnc.getId_rangom(),
                reglaEnc.isActivo(),
                reglaEnc.getId_regla()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public boolean updateReglasDet(ReglasDet reglaDet) {
        String SQL = "UPDATE [dbo].[rel_valor_regla]\n"
                + "   SET [id_valor] = ?\n"
                + "      ,[id_regla] = ?\n"
                + "      ,[porcentaje_condonacion] = ?\n"
                + " WHERE id_valor_regla = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                reglaDet.getId_valor(),
                reglaDet.getId_regla(),
                reglaDet.getPorcentaje_condonacion(),
                reglaDet.getId_valor_regla()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public ReglasEnc getReglasXIdEnc(final int id_regla) {
        ReglasEnc reglaEnc = new ReglasEnc();

        //  final String SQL = "SELECT * FROM regla WHERE id_regla = ?";
        String SQL = "SELECT re.*,va.[desc]'DescValor' FROM regla re \n"
                + "inner join rel_valor_regla reva  on re.id_regla=reva.id_regla \n"
                + "inner join valor va on va.id_valor=reva.id_valor \n"
                + "WHERE re.id_regla = ?";

        try {
            reglaEnc = jdbcTemplate.queryForObject(SQL, new Object[]{id_regla}, new RowMapper<ReglasEnc>() {
                public ReglasEnc mapRow(ResultSet rs, int rowNum) throws SQLException {
                    ReglasEnc reglaEnc = new ReglasEnc();

                    reglaEnc.setId_regla(rs.getInt("id_regla"));
                    reglaEnc.setDesc(rs.getString("desc"));
                    reglaEnc.setId_rangof(rs.getInt("id_rangof"));
                    reglaEnc.setId_rangom(rs.getInt("id_rangom"));
                    reglaEnc.setActivo(rs.getBoolean("activo"));
                    reglaEnc.setDescValor(rs.getString("DescValor"));
                    return reglaEnc;
                }

            });

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return reglaEnc;

    }

    public List<ReglasDet> getReglasDetXIdEnc(final int id_regla) {
        List<ReglasDet> lReglasDet = new ArrayList<ReglasDet>();
        final String SQL = "SELECT * FROM rel_valor_regla WHERE id_regla = ?";

        try {
            lReglasDet = jdbcTemplate.query(SQL, new Object[]{id_regla}, new RowMapper<ReglasDet>() {
                public ReglasDet mapRow(ResultSet rs, int rowNum) throws SQLException {
                    ReglasDet reglasDet = new ReglasDet();

                    reglasDet.setId_valor_regla(rs.getInt("id_valor_regla"));
                    reglasDet.setId_valor(rs.getInt("id_valor"));
                    reglasDet.setId_regla(rs.getInt("id_regla"));
                    reglasDet.setPorcentaje_condonacion(rs.getFloat("porcentaje_condonacion"));
                    return reglasDet;
                }

            });

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return lReglasDet;

    }

    public List<ReglasEnc> getReglasEnc() {
        List<ReglasEnc> lRegEnc = new ArrayList<ReglasEnc>();

        String SQL = "SELECT * FROM regla";

        try {
            lRegEnc = jdbcTemplate.query(SQL, new RowMapper<ReglasEnc>() {
                public ReglasEnc mapRow(ResultSet rs, int rowNum) throws SQLException {
                    ReglasEnc reglaEnc = new ReglasEnc();

                    reglaEnc.setId_regla(rs.getInt("id_regla"));
                    reglaEnc.setDesc(rs.getString("desc"));
                    reglaEnc.setId_rangof(rs.getInt("id_rangof"));
                    reglaEnc.setId_rangom(rs.getInt("id_rangom"));
                    reglaEnc.setActivo(rs.getBoolean("activo"));
                    return reglaEnc;
                }
            });

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return lRegEnc;
    }

    public List<ReglasDet> getReglasDet() {
        List<ReglasDet> lRegDet = new ArrayList<ReglasDet>();
        String SQL = "SELECT * FROM rel_valor_regla";

        try {
            lRegDet = jdbcTemplate.query(SQL, new RowMapper<ReglasDet>() {
                public ReglasDet mapRow(ResultSet rs, int rowNum) throws SQLException {
                    ReglasDet reglasDet = new ReglasDet();

                    reglasDet.setId_valor_regla(rs.getInt("id_valor_regla"));
                    reglasDet.setId_valor(rs.getInt("id_valor"));
                    reglasDet.setId_regla(rs.getInt("id_regla"));
                    reglasDet.setPorcentaje_condonacion(rs.getFloat("porcentaje_condonacion"));

                    return reglasDet;
                }
            });

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }
        return lRegDet;

    }

    public boolean inactivaRegla(int id_Regla) {
        String SQL = "UPDATE [dbo].[regla]\n"
                + "   SET activo = 0\n" //activo 1 | inactivo 0
                + " WHERE id_regla = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                id_Regla
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public int insertUsrModifRegla(final int id_colaborador, final int id_relValRegla, final int porcenjaAnterior, final int porcentajeActual) {
        int id_valor_regla;

        final String SQL = "INSERT INTO [dbo].[usr_modif_relvalregla_con]\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[fk_idRelValRegla]\n"
                + "           ,[PorcentajeAnterior]\n"
                + "           ,[registrado]\n"
                + "           ,[PorcentajeActual]\n"
                + "           ,[vigente])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,'True')";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setInt(2, id_relValRegla);
                    ps.setInt(3, porcenjaAnterior);
                    ps.setInt(4, porcentajeActual);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

            String SQLF = "UPDATE [dbo].[usr_modif_relvalregla_con]\n"
                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
                    + " WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla + "  ";

            // Messagebox.show(SQLF);
            try {
                jdbcTemplate.update(SQLF);
                // return true;
            } catch (DataAccessException e) {
                SisCorelog("SQL Exception: " + e.toString());
                // return false;
            }

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    float p1 = 0;
    float p2 = 0;
    float p3 = 0;
    float p4 = 0;
    int p5 = 0;
    int p6 = 0;

    public int insertUsrModifReglaDecimales(final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual)
    {
        int id_valor_regla;
        String porcenjaAnterior2 = porcenjaAnterior;
        String porcentajeActual2 = porcentajeActual;
        porcenjaAnterior = porcenjaAnterior.replace(".", ",");
        porcentajeActual = porcentajeActual.replace(".", ",");
        String[] partesPAnterior = porcenjaAnterior.split(",");
        String[] partesPActual = porcentajeActual.split(",");
        // parsear valores string en porcentajes
        if (partesPAnterior.length > 0) {
            p1 = (float) Float.parseFloat(porcenjaAnterior2);
            p2 = (float) Float.parseFloat(partesPAnterior[1]);
            p5 = (int) Integer.parseInt(partesPAnterior[0]);
        }

        if (partesPActual.length > 0) {
            p3 = (float) Float.parseFloat(porcentajeActual2);
            p4 = (float) Float.parseFloat(partesPActual[1]);
            p6 = (int) Integer.parseInt(partesPActual[0]);
        }

        final String SQL = "INSERT INTO [dbo].[usr_modif_relvalregla_con2]\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[fk_idRelValRegla]\n"
                + "           ,[PorcentajeAnterior]\n"
                + "           ,[registrado]\n"
                + "           ,[PorcentajeActual]\n"
                + "           ,[vigente] "
                + "           ,[PorcentajeAnterior_parteDecimal] "
                + "           ,[PorcentajeActual_parteDecimal] "
                + "           ,[PorcentajeActual_Alldecimal] "
                + "           ,[PorcentajeAnterior_Alldecimal] "
                + ")\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,'True'"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + ")";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setInt(2, id_relValRegla);
                    ps.setInt(3, p5);
                    ps.setInt(4, p6);
                    ps.setFloat(5, p2);
                    ps.setFloat(6, p4);
                    ps.setFloat(7, p3);
                    ps.setFloat(8, p1);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

//            String SQLF = "UPDATE [dbo].[usr_modif_relvalregla_con2]\n"
//                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
//                    + " WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla + "  ";
//            String SQLF2 = "UPDATE [dbo].[usr_modif_relvalregla_con]\n"
//                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
//                    + " WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla + "  ";
          
            String SQLF = "UPDATE [dbo].[usr_modif_relvalregla_con2]    SET vigente = 'False'  WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla ;
            String SQLF2= "UPDATE [dbo].[usr_modif_relvalregla_con]    SET vigente = 'False'  WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla ;  

          //  Messagebox.show(SQL);
           //  Messagebox.show(SQLF);
          ///   Messagebox.show(SQLF2);
            try {
                jdbcTemplate.update(SQLF);
                 jdbcTemplate.update(SQLF2);
                // return true;
            } catch (DataAccessException e) {
                Messagebox.show("Error:" + e.getMessage());
                SisCorelog("SQL Exception: " + e.toString());
                // return false;
            }

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error:" + ex.getMessage());
            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    
    
    
        public int insertUsrModifReglaDecimalesCampana(final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual,final int id_condonacion)
    {
        int id_valor_regla;
        String porcenjaAnterior2 = porcenjaAnterior;
        String porcentajeActual2 = porcentajeActual;
        porcenjaAnterior = porcenjaAnterior.replace(".", ",");
        porcentajeActual = porcentajeActual.replace(".", ",");
        String[] partesPAnterior = porcenjaAnterior.split(",");
        String[] partesPActual = porcentajeActual.split(",");
        // parsear valores string en porcentajes
        if (partesPAnterior.length > 0) {
            p1 = (float) Float.parseFloat(porcenjaAnterior2);
            p2 = (float) Float.parseFloat(partesPAnterior[1]);
            p5 = (int) Integer.parseInt(partesPAnterior[0]);
        }

        if (partesPActual.length > 0) {
            p3 = (float) Float.parseFloat(porcentajeActual2);
            p4 = (float) Float.parseFloat(partesPActual[1]);
            p6 = (int) Integer.parseInt(partesPActual[0]);
        }

        final String SQL = "INSERT INTO [dbo].[usr_modif_relvalregla_con2]\n" +
"           ([fk_idColaborador]\n" +
"           ,[fk_idRelValRegla]\n" +
"           ,[idCondonacion]\n" +
"           ,[PorcentajeAnterior]\n" +
"           ,[PorcentajeActual]\n" +
"           ,[registrado]\n" +
"           ,[vigente]\n" +
"           ,[PorcentajeAnterior_parteDecimal]\n" +
"           ,[PorcentajeActual_parteDecimal]\n" +
"           ,[PorcentajeActual_Alldecimal]\n" +
"           ,[PorcentajeAnterior_Alldecimal])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"               
                + "           ,'True'"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + ")";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setInt(2, id_relValRegla);
                    ps.setInt(3, id_condonacion);
                    ps.setInt(4, p5);
                    ps.setInt(5, p6);
                    ps.setFloat(6, p2);
                    ps.setFloat(7, p4);
                    ps.setFloat(8, p3);
                    ps.setFloat(9, p1);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

            String SQLF = "UPDATE [dbo].[usr_modif_relvalregla_con2]    SET vigente = 'False'  WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla +"and idcondonacion is null";
            String SQLF2= "UPDATE [dbo].[usr_modif_relvalregla_con]    SET vigente = 'False'  WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla +" and  idcondonacion is null";  

          //  Messagebox.show(SQL);
           //  Messagebox.show(SQLF);
          ///   Messagebox.show(SQLF2);
            try {
                jdbcTemplate.update(SQLF);
                // return true;
            } catch (DataAccessException e) {
                Messagebox.show("Error:" + e.getMessage());
                SisCorelog("SQL Exception: " + e.toString());
                // return false;
            }
            
           try {

                 jdbcTemplate.update(SQLF2);
                // return true;
            } catch (DataAccessException e) {
                Messagebox.show("Error:" + e.getMessage());
                SisCorelog("SQL Exception: " + e.toString());
                // return false;
            }
            
            
            
            
            

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error:" + ex.getMessage());
            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    
    public int insertEditUsrModifReglaDecimales(final int condonacion, final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual) {
        int id_valor_regla;
        String porcenjaAnterior2 = porcenjaAnterior;
        String porcentajeActual2 = porcentajeActual;
        porcenjaAnterior = porcenjaAnterior.replace(".", ",");
        porcentajeActual = porcentajeActual.replace(".", ",");
        String[] partesPAnterior = porcenjaAnterior.split(",");
        String[] partesPActual = porcentajeActual.split(",");
        // parsear valores string en porcentajes
        if (partesPAnterior.length > 0) {
            p1 = (float) Float.parseFloat(porcenjaAnterior2);
            p2 = (float) Float.parseFloat(partesPAnterior[1]);

            p5 = (int) Integer.parseInt(partesPAnterior[0]);

        }

        if (partesPActual.length > 0) {
            p3 = (float) Float.parseFloat(porcentajeActual2);
            p4 = (float) Float.parseFloat(partesPActual[1]);
            p6 = (int) Integer.parseInt(partesPActual[0]);
        }

        final String SQL = "INSERT INTO [dbo].[usr_modif_relvalregla_con2]\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[fk_idRelValRegla]\n"
                + "           ,[idCondonacion]\n"
                + "           ,[PorcentajeAnterior]\n"
                + "           ,[registrado]\n"
                + "           ,[PorcentajeActual]\n"
                + "           ,[vigente] "
                + "           ,[PorcentajeAnterior_parteDecimal] "
                + "           ,[PorcentajeActual_parteDecimal] "
                + "           ,[PorcentajeActual_Alldecimal] "
                + "           ,[PorcentajeAnterior_Alldecimal] "
                + ")\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,'True'"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + ")";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setInt(2, id_relValRegla);
                    ps.setInt(3, condonacion);
                    ps.setInt(4, p5);
                    ps.setInt(5, p6);
                    ps.setFloat(6, p2);
                    ps.setFloat(7, p4);
                    ps.setFloat(8, p3);
                    ps.setFloat(9, p1);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

            String SQLF = "UPDATE [dbo].[usr_modif_relvalregla_con2]\n"
                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
                    + " WHERE  id <> '" + id_valor_regla + "' and idCondonacion=" + condonacion + " and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla + "  ";
            String SQLF2 = "UPDATE [dbo].[usr_modif_relvalregla_con2]\n"
                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
                    + " WHERE  id <> '" + id_valor_regla + "' and idCondonacion=" + condonacion + " and fk_idColaborador=" + id_colaborador + " and fk_idRelValRegla = " + id_relValRegla + "  ";

            // Messagebox.show(SQLF);
            try {
                jdbcTemplate.update(SQLF);
                 jdbcTemplate.update(SQLF2);
                // return true;
            } catch (DataAccessException e) {
                Messagebox.show("Error:" + e.getMessage());
                SisCorelog("SQL Exception: " + e.toString());
                // return false;
            }

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error:" + ex.getMessage());
            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    public int insertUsrAbonoCapital(final int id_colaborador, final int rutcliente, final float abono, final float montoMora, final float MontoCapital) {
        int id_valor_regla;

        final String SQL = "INSERT INTO [dbo].[usr_abono_CapitalPorRut]\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[rutCliente]\n"
                + "           ,[valorAbonoTotal]\n"
                + "           ,[registrado]\n"
                + "           ,[vigente]\n"
                + "           ,[MontoTotalMora] \n"
                + "           ,[MontoTotalCapital])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,'True' \n"
                + "           ,? \n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    // ps.setString(1, person.getName());
                    ps.setInt(1, id_colaborador);
                    ps.setInt(2, rutcliente);
                    ps.setFloat(3, abono);
                    ps.setFloat(4, montoMora);
                    ps.setFloat(5, MontoCapital);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

            String SQLF = "UPDATE [dbo].[usr_abono_CapitalPorRut]\n"
                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
                    + " WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and rutCliente  = " + rutcliente + "  ";
            //  Messagebox.show("Abono Capital" + SQLF);
            try {
                jdbcTemplate.update(SQLF);
                // return true;
            } catch (DataAccessException e) {
                SisCorelog("SQL Exception: " + e.toString());
                Messagebox.show("Error:" + e.toString());
                // return false;
            }

        } catch (DataAccessException ex) {

            Messagebox.show("Error:" + ex.getMessage());

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

    public boolean getReglaDetSiNo(int id_Regla) {
        String SQL = "select count(*) Si from rel_valor_regla where id_regla = " + id_Regla;

        int SiNo = 0;

        try {
            SiNo = jdbcTemplate.queryForInt(SQL);

        } catch (DataAccessException ex) {
            Messagebox.show(ex.toString());
        }

        return (SiNo != 0);

    }

    public int insertUsrModifInteres(final int id_colaborador, final String operacion, final float InteresAnterior, final float NuevoInteres, final int rutcliente) {
        int id_valor_regla;

        final String SQL = "INSERT INTO [dbo].[usr_modif_interes]\n"
                + "           ([fk_idColaborador]\n"
                + "           ,[Codigo_operacion]\n"
                + "           ,[valor_Anteriro]\n"
                + "           ,[registrado]\n"
                + "           ,[valor_actual]\n"
                + "           ,[vigente]\n"
                + "           ,[rut_usuario])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,getdate()\n"
                + "           ,?\n"
                + "           ,'True' \n"
                + "           ,?)";
        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
                    ps.setInt(1, id_colaborador);
                    ps.setString(2, operacion);
                    ps.setFloat(3, InteresAnterior);
                    ps.setFloat(4, NuevoInteres);
                    ps.setInt(5, rutcliente);
                    return ps;
                }
            },
                    keyHolder);

            id_valor_regla = keyHolder.getKey().intValue();

            String SQLF = "UPDATE [dbo].[usr_modif_interes]\n"
                    + "   SET vigente = 'False' \n" //activo 1 | inactivo 0
                    + " WHERE  id <> '" + id_valor_regla + "' and fk_idColaborador=" + id_colaborador + " and Codigo_operacion = '" + operacion + "' and rut_usuario=" + rutcliente;

            try {
                jdbcTemplate.update(SQLF);
            } catch (DataAccessException e) {
                SisCorelog("SQL Exception: " + e.toString());
                Messagebox.show("Error Guardando Interes: [" + e.getMessage() + "] linea:[486]");
            }

        } catch (DataAccessException ex) {

            Messagebox.show("Error Guardando Interes: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_valor_regla = 0;

        }

        return id_valor_regla;
    }

}
