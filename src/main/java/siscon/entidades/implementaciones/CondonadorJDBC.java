/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.ColaboradorMapper;
import siscon.RowMapper.ReglaPorUsuarioMapper;
import siscon.RowMapper.UsrModfReglaCapitalFloatMapper;
import siscon.RowMapper.UsrModfReglaCapitalMapper;
import siscon.RowMapper.UsrModfReglaHonorarioFloatMapper;
import siscon.RowMapper.UsrModfReglaHonorariolMapper;
import siscon.RowMapper.UsrModfReglaIntMapper;
import siscon.RowMapper.UsrModfReglaIntzFloatMapper;
import siscon.entidades.Colaborador;
import siscon.entidades.Condonador;
import siscon.entidades.Regla;
import siscon.entidades.UsrModfReglaCapital;
import siscon.entidades.UsrModfReglaHonorario;
import siscon.entidades.UsrModfReglaInt;

/**
 *
 * @author esilvestre
 */
public class CondonadorJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private final JdbcTemplate jdbcTemplate;
    float UltPorcentajeConInteres;
private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CondonadorJDBC.class);
    /**
     *
     * @param dataSource
     */
    public CondonadorJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);
        UltPorcentajeConInteres = 0;
    }

    public float getUltPorcentajeConInteres() {
        return UltPorcentajeConInteres;
    }

    public void setUltPorcentajeConInteres(float UltPorcentajeConInteres) {
        this.UltPorcentajeConInteres = UltPorcentajeConInteres;
    }

    public Condonador GetCondonador(int rut_condonador, String alias, int MesesdelPrimerCastigo) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    //  Messagebox.show("Reglas117:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    /*  String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";                   
                     */
                    try {
                        //  Messagebox.show("SQLcapital:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL3 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorariolMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setPorcentajeActual(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualCapital(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualHonorario(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getPorcentajeActual() : 0);
                        } else {
                            _condonador.setPorcentajeActual(0);
                            _condonador.setPorcentajeActualCapital(0);
                            _condonador.setPorcentajeActualHonorario(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetPorcentajeCondonador(int rut_condonador, String alias, int MesesdelPrimerCastigo) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    //  Messagebox.show("Reglas117:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    /*  String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";                   
                     */
                    try {
                        //  Messagebox.show("SQLcapital:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL3 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorariolMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setPorcentajeActual(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualCapital(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualHonorario(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getPorcentajeActual() : 0);
                        } else {
                            _condonador.setPorcentajeActual(0);
                            _condonador.setPorcentajeActualCapital(0);
                            _condonador.setPorcentajeActualHonorario(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetCondonadorV2(int rut_condonador, String alias, int MesesdelPrimerCastigo) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on,k colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    //Messagebox.show("Valores de Las Reglas Reglas117:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    /*  String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";                   
                     */
                    try {
                        //Messagebox.show("SQLcapital GetCondonadorV2:[" + SQL4 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL3 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {

                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);
                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {

                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);

                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetCondonadorV3(int rut_condonador, String alias, int MesesdelPrimerCastigo) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on,k colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    //Messagebox.show("Valores de Las Reglas Reglas117:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    /*  String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";                   
                     */
                    try {
                        // Messagebox.show("SQLcapital GetCondonadorV2:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL3 + "]");
                        // Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetCondonadorConsulting(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion=" + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion=" + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual,umod.PorcentajeAnterior from  [dbo].[usr_modif_relvalregla_con] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion=" + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        // Messagebox.show("SQLcapital:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL3 + "]");
                        // Messagebox.show("SQLInteres:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorariolMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setPorcentajeActual(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualCapital(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getPorcentajeActual() : 0);
                            _condonador.setPorcentajeActualHonorario(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getPorcentajeActual() : 0);
                        } else {
                            _condonador.setPorcentajeActual(0);
                            _condonador.setPorcentajeActualCapital(0);
                            _condonador.setPorcentajeActualHonorario(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetCondonadorConsultingV2(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        //Messagebox.show("SQLcapital GetCondonadorV2:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL3 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV2:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    // este consulting esta echo para la edicion y los valores con id de condonacion
    public Condonador GetCondonadorConsultingV3(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    //si no hay ID de Condonacion es la Primera edicion y traemos los porcentajes de
                    String SQL3 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from  [dbo].[usr_modif_relvalregla_con2] umod  \n"
                            + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                            + " inner join rel_valor_regla  reval on umod.fk_idRelValRegla=reval.id_valor_regla   \n"
                            + "  inner join valor va on va.id_valor=reval.id_valor    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        //Messagebox.show("SQLcapital GetCondonadorV3:[" + SQL4 + "]");
                       // Messagebox.show("SQLInteres GetCondonadorV3:[" + SQL3 + "]");
                       // Messagebox.show("SQLInteres GetCondonadorV3:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    // este consulting esta echo para Utilizar las 2 tablas de porcentajes
    public Condonador GetCondonadorConsultingV4(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
      //  Messagebox.show("SQL:[" + SQL + "]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);
                String WhereQueryRango="";
                
                switch (MesesdelPrimerCastigo) {
                    case 6:
                        WhereQueryRango= "where FF.RanfoFInicio<" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                        logger.info("##################################### linea 1122 if(MesesdelPrimerCastigo == 6    MesesdelPrimerCastigo[{}]",MesesdelPrimerCastigo);
                        break;
                    case 18:
                        WhereQueryRango= "where FF.RanfoFInicio<" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                        logger.info("##################################### linea 1122 if(MesesdelPrimerCastigo == 18 MesesdelPrimerCastigo:[{}]",MesesdelPrimerCastigo);
                        break;
                    default:
                        WhereQueryRango= "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                        logger.info("##################################### linea 1122 else(MesesdelPrimerCastigo == 6 MesesdelPrimerCastigo[{}]",MesesdelPrimerCastigo);
                        break;
                }


                
                
                
                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                      //  + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                        + WhereQueryRango;
                try {
                   // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                   logger.info("##################################### linea 1141 GetCondonadorConsultingV4 where custom: - {}", SQL2);
                   
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }
            logger.info("##################################### linea 1179 Meses de Castigo mas Antiguo :[{}] GetCondonadorConsultingV4 valores de reglas que busco  id_valor_regla_capital: - {}  id_valor_regla_int:[{}]   id_valor_regla_Honorario : [{}]", MesesdelPrimerCastigo,id_valor_regla_capital,id_valor_regla,id_valor_regla_Honorario);
                    //si no hay ID de Condonacion es la Primera edicion y traemos los porcentajes de
                    String SQL3 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor     \n"
                            + " where  usu.di_rut='" + rut_condonador + "' "
                            + "and va.[desc]='Interes' "
                            + "and usu.alias='" + alias + "' "
                            + "and umod.vigente='True' "
                            + "and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal \n"
                            + "from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                      //  Messagebox.show("SQLcapital GetCondonadorV4:[" + SQL4 + "]");
                       // Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL3 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);

                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);

                        } else {
                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);

                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }
    // este consulting esta echo para Utilizar las 2 tablas de porcentajes
    public Condonador GetCondonadorConsultingV4Camp(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
      //  Messagebox.show("SQL:[" + SQL + "]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);
                String WhereQueryRango="";
                
                if(MesesdelPrimerCastigo==6){
                WhereQueryRango= "where FF.RanfoFInicio<" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                }
                else {WhereQueryRango= "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";}

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                      //  + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";
                        + WhereQueryRango;
                try {
                   // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                   logger.info("##################################### linea 1141 GetCondonadorConsultingV4 where custom: - {}", SQL2);
                   
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    //si no hay ID de Condonacion es la Primera edicion y traemos los porcentajes de
                    String SQL3 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor     \n"
                            + " where  usu.di_rut='" + rut_condonador + "' "
                            + "and va.[desc]='Interes' "
                            + "and usu.alias='" + alias + "' "
                            + "and umod.vigente='True' "
                            + "and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal \n"
                            + "from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                       // Messagebox.show("SQLcapital GetCondonadorV4:[" + SQL4 + "]");
                       // Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL3 + "]");
                       // Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {
                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);

                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);

                        } else {
                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);

                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    
    /// este metodo lo replicaremos para los rechazos , dado que quda seteada la variable en False
    // este consulting esta echo para Utilizar las 2 tablas de porcentajes
    public Condonador GetCondonadorConsultingV7(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    //si no hay ID de Condonacion es la Primera edicion y traemos los porcentajes de
                    String SQL3 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor     \n"
                            + " where  usu.di_rut='" + rut_condonador + "' "
                            + "and va.[desc]='Interes' "
                            + "and usu.alias='" + alias + "' "
                            + "and idCondonacion= " + idCondonacion + " "
                            + "and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' "
                            + "and va.[desc]='Monto Capital' "
                            + "and usu.alias='" + alias + "'  "
                            + "and idCondonacion= " + idCondonacion + " "
                            + "and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "'  and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        //Messagebox.show("SQLcapital GetCondonadorV4:[" + SQL4 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL3 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {

                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);

                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {

                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    public Condonador GetCondonadorConsultingV5(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        if (this._estaEnElLimitedelInntervalo(MesesdelPrimerCastigo)) {
            MesesdelPrimerCastigo = MesesdelPrimerCastigo + 1;

        }

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    //Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    String SQL3 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "' and umod.vigente='True' and idCondonacion is null and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        // Messagebox.show("SQLcapital GetCondonadorV5:[" + SQL4 + "]");
                        // Messagebox.show("SQLInteres GetCondonadorV5:[" + SQL3 + "]");
                        // Messagebox.show("SQLInteres GetCondonadorV5:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {

                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);

                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {

                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }

    // este consulting esta echo para Utilizar las 2 tablas de porcentajes
    public Condonador GetCondonadorConsultingV8(int rut_condonador, String alias, int MesesdelPrimerCastigo, int idCondonacion) {

        Condonador _condonador = new Condonador();
        String result = "";
        //Messagebox.show("Ruts["+rut_condonador+"],Nombre:["+alias+"]");

        //// Buscamos Tabla Colaborador
        Colaborador _colabo = new Colaborador();

        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";

        List<Colaborador> _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());

        //  _condonador.setReglaList(_regla);
        _colabo = _colaboList.get(0);

        _condonador.setColaborador(_colabo);

        String SQL = " select colatr.monto_maximo from colaborador cola \n"
                + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut_condonador + " and usu.alias='" + alias + "' \n";
        // Messagebox.show("SQL:["+SQL+"]");
        try {

            Object o = (String) jdbcTemplate.queryForObject(SQL, String.class);
            result = (String) o;
            if (!result.equals("")) {

                float t = Float.parseFloat(result);
                // _condonador=new Condonador();
                _condonador.setAtribucionMaxima(t);

                String SQL2 = "select * from ( \n"
                        + "select va.id_valor_regla,pe.descripcion'Perfil',re.[desc]'DescRegla',raf.rango_ini'RanfoFInicio',raf.rango_fin'RangoFFin',raf.unidad'UndMedF',ram.monto_ini'RangoMontoI',ram.monto_fin'RangoMontoM',vaa.[desc]'DesTipoValor',va.porcentaje_condonacion'PorcentajeCondonacion100' \n"
                        + "from  perfil pe \n"
                        + "inner join rel_perfil_regla repe on repe.id_perfil=pe.id_perfil \n"
                        + "inner join regla re on re.id_regla=repe.id_regla \n"
                        + "inner join rango_fecha raf on raf.id_rangof=re.id_rangof \n"
                        + "inner join rango_monto ram on ram.id_rangom=re.id_rangom \n"
                        + "inner join rel_valor_regla va on va.id_regla=re.id_regla \n"
                        + "inner join valor vaa on vaa.id_valor=va.id_valor \n"
                        + "inner join perfil_usuario peu on peu.id_perfil=pe.id_perfil \n"
                        + "inner join usuario usu on usu.id_usuario=peu.id_usuario \n"
                        + "where     usu.di_rut='" + rut_condonador + "' and usu.alias='" + alias + "'  \n"
                        + ") AS FF \n"
                        + "where FF.RanfoFInicio<=" + MesesdelPrimerCastigo + " and FF.RangoFFin >= " + MesesdelPrimerCastigo + " \n";

                try {
                    // Messagebox.show("Reglas Linea 273:[" + SQL2 + "]");
                    List<Regla> _regla = jdbcTemplate.query(SQL2, new ReglaPorUsuarioMapper());

                    _condonador.setReglaList(_regla);

                    int id_valor_regla_capital = 0;
                    int id_valor_regla = 0;
                    int id_valor_regla_Honorario = 0;
                    for (Regla _reglad : _regla) {
                        //list2.add(tipodocto.getDv_NomTipDcto());
                        if (_reglad.getDesTipoValor().equals("Monto Capital")) {
                            //  ppppppp = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_capital = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Interes")) {
                            //  pppp2 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla = _reglad.getIdRegla();
                        }
                        if (_reglad.getDesTipoValor().equals("Honorario Judicial")) {
                            //  pppp3 = _regla.getPorcentajeCondonacion100();
                            id_valor_regla_Honorario = _reglad.getIdRegla();
                        }

                    }

                    //si no hay ID de Condonacion es la Primera edicion y traemos los porcentajes de
                    String SQL3 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor     \n"
                            + "inner join ( select up.id_usr_modif from Cond_Rechazadas cr  inner join usr_porcentajes_rechazo up on up.id_rechazo=cr.id_Rechazo where cr.fk_idCondonacion=" + idCondonacion + ") as YY on YY.id_usr_modif= umod.id    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Interes' and usu.alias='" + alias + "'  and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla + " \n";

                    String SQL4 = "select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + "inner join ( select up.id_usr_modif from Cond_Rechazadas cr  inner join usr_porcentajes_rechazo up on up.id_rechazo=cr.id_Rechazo where cr.fk_idCondonacion=" + idCondonacion + ") as YY on YY.id_usr_modif= umod.id    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Monto Capital' and usu.alias='" + alias + "'  and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_capital + " \n";

                    String SQL5 = " select umod.id,umod.PorcentajeActual_Alldecimal,umod.PorcentajeAnterior_Alldecimal from (\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "	  ,'0' as [PorcentajeAnterior_parteDecimal]\n"
                            + "	  ,'0' as [PorcentajeActual_parteDecimal]\n"
                            + "	  ,[PorcentajeActual] as [PorcentajeActual_Alldecimal]\n"
                            + "	  ,[PorcentajeAnterior] as [PorcentajeAnterior_Alldecimal]  \n"
                            + "	   from usr_modif_relvalregla_con\n"
                            + "	  union all\n"
                            + "select [id]\n"
                            + "      ,[fk_idColaborador]\n"
                            + "      ,[fk_idRelValRegla]\n"
                            + "      ,[idCondonacion]\n"
                            + "      ,[PorcentajeAnterior]\n"
                            + "      ,[PorcentajeActual]\n"
                            + "      ,[registrado]\n"
                            + "      ,[vigente]\n"
                            + "      ,[PorcentajeAnterior_parteDecimal]\n"
                            + "      ,[PorcentajeActual_parteDecimal]\n"
                            + "      ,[PorcentajeActual_Alldecimal]\n"
                            + "      ,[PorcentajeAnterior_Alldecimal] \n"
                            + "	  from usr_modif_relvalregla_con2\n"
                            + "	  ) as umod\n"
                            + "\n"
                            + "	  inner join colaborador col on col.id=umod.fk_idColaborador \n"
                            + "inner join usuario usu on usu.id_usuario=col.id_usuario \n"
                            + "inner join rel_valor_regla reval on umod.fk_idRelValRegla=reval.id_valor_regla \n"
                            + "inner join valor va on va.id_valor=reval.id_valor   \n"
                            + "inner join ( select up.id_usr_modif from Cond_Rechazadas cr  inner join usr_porcentajes_rechazo up on up.id_rechazo=cr.id_Rechazo where cr.fk_idCondonacion=" + idCondonacion + ") as YY on YY.id_usr_modif= umod.id    \n"
                            + " where  usu.di_rut='" + rut_condonador + "' and va.[desc]='Honorario Judicial' and usu.alias='" + alias + "'  and idCondonacion= " + idCondonacion + " and reval.id_valor_regla= " + id_valor_regla_Honorario + " \n";

                    try {
                        //Messagebox.show("SQLcapital GetCondonadorV4:[" + SQL4 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL3 + "]");
                        //Messagebox.show("SQLInteres GetCondonadorV4:[" + SQL5 + "]");
                        List<UsrModfReglaInt> _usrModfRegla = jdbcTemplate.query(SQL3, new UsrModfReglaIntzFloatMapper());
                        List<UsrModfReglaCapital> _usrModfReglaCapital = jdbcTemplate.query(SQL4, new UsrModfReglaCapitalFloatMapper());
                        List<UsrModfReglaHonorario> _usrModfReglaHonorario = jdbcTemplate.query(SQL5, new UsrModfReglaHonorarioFloatMapper());
                        // List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL6, new UsrModInterezOperacionMapper());
                        if (_usrModfRegla.size() > 0) {

                            _condonador.setId_usr_modif_int(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getId_usr_modf() : 0);
                            _condonador.setId_usr_modif_hon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getId_usr_modif() : 0);
                            _condonador.setId_usr_modif_cap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getIs_usr_modif() : 0);

                            _condonador.setF_PorcentajeActualInt(_usrModfRegla.size() > 0 ? _usrModfRegla.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualCap(_usrModfReglaCapital.size() > 0 ? _usrModfReglaCapital.get(0).getF_PorcentajeActual() : 0);
                            _condonador.setF_PorcentajeActualHon(_usrModfReglaHonorario.size() > 0 ? _usrModfReglaHonorario.get(0).getF_PorcentajeAnterior() : 0);
                        } else {

                            _condonador.setId_usr_modif_int(0);
                            _condonador.setId_usr_modif_hon(0);
                            _condonador.setId_usr_modif_cap(0);
                            _condonador.setF_PorcentajeActualInt(0);
                            _condonador.setF_PorcentajeActualCap(0);
                            _condonador.setF_PorcentajeActualHon(0);
                        }

                    } catch (EmptyResultDataAccessException e) {
                        e.printStackTrace();
                        Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                    }

                } catch (EmptyResultDataAccessException e) {
                    e.printStackTrace();
                    Messagebox.show("ERROR --- Reglas[" + rut_condonador + "],Nombre:[" + alias + "]");

                }
            }
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- Ruts[" + rut_condonador + "],Nombre:[" + alias + "]");

        }
        //return cert;

        return _condonador;

    }
    private static final Logger LOG = Logger.getLogger(CondonadorJDBC.class.getName());

    public int GetRutCondonadorXId(int id_colaborador) {
        int rut_colaborador = 0;

        //  jdbcTemplateObject = new JdbcTemplate(_internodataSource);
        try {

            String SQL = "SELECT di_rut FROM colaborador col inner join usuario us on us.id_usuario=col.id_usuario WHERE col.id = ?";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rut_colaborador = jdbcTemplate.queryForInt(SQL, new Object[]{id_colaborador});

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonación: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rut_colaborador = 0;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rut_colaborador = 0;
        }
        return rut_colaborador;
    }

    public boolean _estaEnElLimitedelInntervalo(int _mesDeCastigo) {
        boolean result = false;
        int _numDocumentos = 0;
        try {

            //jdbcTemplate = new JdbcTemplate(dataSource);
            String SQLtramsito = "select count(*) from rango_fecha raf where raf.rango_ini = " + _mesDeCastigo + " or raf.rango_fin= " + _mesDeCastigo + "";

            _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);

        } catch (Exception ex) {
            //logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            // logger.info("##################################### linea 2092 ex.getMessage(): - {}", ex.getMessage() );
            System.out.println(ex.getMessage());
            result = false;
        }
        return _numDocumentos > 0;
    }
}
