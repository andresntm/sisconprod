/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Banca;
import siscon.entidades.Periodo;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author esilves
 */
public class PeriodoJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public PeriodoJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    public List<Periodo> getALLPeriodo() {
        List<Periodo> lPeriodo = new ArrayList<Periodo>();
        final String SQL = "select\n" +
                                    "         isnull(HH.datet,'1999-01') 'datet' \n" +
                                    "from\n" +
                                    "         (\n" +
                                    "                    select\n" +
                                    "                               FORMAT(estc.ddt_fechaReistro,'yyyy-MM') 'datet'\n" +
                                    "                    from\n" +
                                    "                               estado_condonacion estc\n" +
                                    "                               inner join\n" +
                                    "                                          estado est\n" +
                                    "                                          on\n" +
                                    "                                                     estc.di_fk_IdEstado=est.di_id_Estado\n" +
                                    "                    where\n" +
                                    "                               est.dv_estado ='Aplicada Aplicacion'\n" +
                                    "         )\n" +
                                    "         as HH\n" +
                                    "group by\n" +
                                    "         HH.datet\n" +
                                    "order by\n" +
                                    "         HH.datet asc";
        try {
            lPeriodo = jdbcTemplate.query(SQL, new RowMapper<Periodo>() {
                public Periodo mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Periodo per = new Periodo();

                    //per.setId(rs.getInt("id"));
                   // per.setCod_periodo(rs.getString("cod_banca"));
                    per.setPeriodo(rs.getString("datet"));

                    return per;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error Mostrando Perfiles Ejecutivos ALL Error:[" + ex.getMessage() + "] Query : [ " + SQL + "]");
        }
        return lPeriodo;

    }

        public List<Periodo> getALLPeriodoInfEfec() {
        List<Periodo> lPeriodo = new ArrayList<Periodo>();
        final String SQL = "select\n" +
"                                           isnull(HH.datet,'1999-01') 'datet' \n" +
"                                  from\n" +
"                                           (\n" +
"                                                     select FORMAT(con.timestap,'yyyy-MM') 'datet'  from siscon_inf_clienteV2 hh   left join   condonacion con on con.id_condonacion=hh.id_condonacion \n" +
"                                           )\n" +
"                                           as HH\n" +
"                                  group by\n" +
"                                           HH.datet\n" +
"                                  order by\n" +
"                                           HH.datet desc";
        try {
            lPeriodo = jdbcTemplate.query(SQL, new RowMapper<Periodo>() {
                public Periodo mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Periodo per = new Periodo();

                    //per.setId(rs.getInt("id"));
                   // per.setCod_periodo(rs.getString("cod_banca"));
                    per.setPeriodo(rs.getString("datet"));

                    return per;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
            Messagebox.show("Error Mostrando Perfiles Ejecutivos ALL Error:[" + ex.getMessage() + "] Query : [ " + SQL + "]");
        }
        return lPeriodo;

    }
}
