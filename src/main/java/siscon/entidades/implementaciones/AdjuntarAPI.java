/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.AdjuntarDET;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.AdjuntarENC;
import siscon.entidades.interfaces.AdjuntarInterfaz;

/**
 *
 * @author excosoc
 */
public class AdjuntarAPI implements AdjuntarInterfaz {

    private AdjuntarENC adjEnc;
    private DataSource dS = null;
    private SimpleJdbcCall sJC;
    private JdbcTemplate jT;
    private MvcConfig conex;
    private boolean apiEnc;
    private UsuarioJDBC user;

    public AdjuntarAPI() {
        try {
            conex = new MvcConfig();
            dS = conex.getDataSource();
            user = new UsuarioJDBC(dS);

            jT = new JdbcTemplate(dS);

        } catch (DataAccessException sqlE) {
            Messagebox.show("Error al conectar a la base de datos\nError: " + sqlE.getMessage());
        } catch (SQLException sqlE) {
            Messagebox.show("Error al conectar a la base de datos\nError: " + sqlE.getMessage());
        }

    }

    public AdjuntarENC api(AdjuntarENC enc, int accion) {
        adjEnc = new AdjuntarENC();

        try {
            if (accion == 1) {
                //2 acciones insert / update
                if (enc.getId() != 0) {
                    final int idAdjuntarEnc = apiENC(enc, 2);

                    enc.getaDet().forEach(new Consumer<AdjuntarDET>() {
                        public void accept(AdjuntarDET t) {
                            if (t.isNewFile()) {
                                t.setFk_id_subearchivo(idAdjuntarEnc);
                                apiDET(t);
                            }
                        }

                    });
                } else {
                    final int idAdjuntarEnc = apiENC(enc, 1);

                    enc.getaDet().forEach(new Consumer<AdjuntarDET>() {
                        public void accept(AdjuntarDET t) {
                            if (t.isNewFile()) {
                                t.setFk_id_subearchivo(idAdjuntarEnc);
                                apiDET(t);

                                t.setNewFile(false);
                            }
                        }
                    });
                }
                adjEnc = enc;
            } else if (accion == 2) {
                if (enc.getDi_id_reparo() != 0) {
                    adjEnc = GetENC_Reparo(enc.getFk_idCondonacion(), enc.getDi_id_reparo());
                    adjEnc.setaDet(GetDET_IdEnc(adjEnc.getId()));
                } else {
                    adjEnc = GetENC(enc.getFk_idCondonacion());
                    adjEnc.setaDet(GetDET(enc.getFk_idCondonacion()));
                }
            }
        } catch (Exception e) {
            adjEnc = null;
        }
        return adjEnc;
    }

    private boolean apiDET(AdjuntarDET det) {

        try {
            this.sJC = new SimpleJdbcCall(dS).withProcedureName("SP_SISCON_ADJUNTARDET");

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("ID_ENC", det.getFk_id_subearchivo())
                    .addValue("ID_COLABORADOR", det.getFk_idColaborador())
                    .addValue("PATH", det.getPatch_archivo())
                    .addValue("PESO", det.getTama�o())
                    .addValue("EXTEN", det.getExtension())
                    .addValue("NOMBRE", det.getFileName())
                    .addValue("SIZE", det.getTama�oNum());

            sJC.execute(in);

            return true;
        } catch (DataAccessException E) {
            Messagebox.show("Error en la api detalle adjuntar 5.\nError: " + E.getMessage());
            return false;
        } catch (Exception E) {
            Messagebox.show("Error en la api detalle adjuntar 4.\nError: " + E.getMessage());
            return false;
        }
    }

    private int apiENC(AdjuntarENC enc, int accion) {
        apiEnc = false;
        int idEnc = 0;
        AdjuntarENC retornoENC = new AdjuntarENC();
        List<AdjuntarENC> listRetENC = new ArrayList<AdjuntarENC>();

        try {
            this.sJC = new SimpleJdbcCall(dS).withProcedureName("SP_SISCON_ADJUNTARENC").returningResultSet("rs", new RowMapper<AdjuntarENC>() {
                public AdjuntarENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    AdjuntarENC sqlEnc = new AdjuntarENC();

                    sqlEnc.setId(rs.getInt("id"));

                    return sqlEnc;
                }

            });

            SqlParameterSource in = new MapSqlParameterSource().addValue("ID_CLIENTE", enc.getFk_idCliente())
                    .addValue("ID_COND", enc.getFk_idCondonacion())
                    .addValue("NUM_ADJUNTOS", enc.getNumero_archivos())
                    .addValue("PESO_TOTAL", enc.getTama�o_total())
                    .addValue("ID_REPARO", enc.getDi_id_reparo())
                    .addValue("ACCION", accion)
                    .addValue("ID_ENC", enc.getId());

            Map<String, Object> out = sJC.execute(in);

            listRetENC = (List<AdjuntarENC>) out.get("rs");

            retornoENC = listRetENC.get(0);

            idEnc = retornoENC.getId();

            apiEnc = true;

        } catch (DataAccessException E) {
            Messagebox.show("Error en la api detalle adjuntar 2.\nError: " + E.getMessage());
            apiEnc = false;
            idEnc = 0;
        } catch (Exception E) {
            Messagebox.show("Error en la api detalle adjuntar 1.\nError: " + E.getMessage());
            apiEnc = false;
            idEnc = 0;
        }

        return idEnc;
    }

    private List<AdjuntarDET> GetDET(int id_Condonacion) {
        List<AdjuntarDET> listADet = new ArrayList<AdjuntarDET>();
        try {
            String SQL = "SELECT da.id_det_archivos,\n"
                    + "       da.fk_id_subearchivo,\n"
                    + "       da.patch_archivo,\n"
                    + "       da.extension,\n"
                    + "       da.tama�o,\n"
                    + "       da.registrado,\n"
                    + "       da.fk_idColaborador,\n"
                    + "       da.fileName,\n"
                    + "       da.sizeFloat\n"
                    + "FROM dbo.detalle_archivos da\n"
                    + "     INNER JOIN dbo.usr_sube_archivo usa ON da.fk_id_subearchivo = usa.id\n"
                    + "     INNER JOIN dbo.colaborador c ON da.fk_idColaborador = c.id\n"
                    + "     INNER JOIN dbo.usuario u ON c.id_usuario = u.id_usuario\n"
                    + "WHERE usa.fk_idCondonacion = ?";

            listADet = jT.query(SQL, new Object[]{id_Condonacion}, new RowMapper<AdjuntarDET>() {
                public AdjuntarDET mapRow(ResultSet rs, int rowNum) throws SQLException {
                    AdjuntarDET sqlDet = new AdjuntarDET(rs.getInt("id_det_archivos"),
                            rs.getInt("fk_id_subearchivo"),
                            rs.getString("patch_archivo"),
                            rs.getString("extension"),
                            rs.getString("tama�o"),
                            rs.getInt("fk_idColaborador"),
                            false,
                            rs.getString("fileName"),
                            rs.getFloat("sizeFloat"));

                    return sqlDet;
                }
            });
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det adjuntar\nError: " + e.toString());
            listADet = null;
        } catch (Exception e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det adjuntar\nError: " + e.toString());
            listADet = null;
        }

        return listADet;
    }

    private AdjuntarENC GetENC(int id_condonacion) {
        AdjuntarENC aEnc = new AdjuntarENC();
        List<AdjuntarENC> listAEnc = new ArrayList<AdjuntarENC>();
        try {
            String SQL = "SELECT usa.id,\n"
                    + "       usa.fk_idCliente,\n"
                    + "       usa.fk_idCondonacion,\n"
                    + "       usa.registrado,\n"
                    + "       usa.tama�o_total,\n"
                    + "       usa.numero_archivos,\n"
                    + "       usa.di_id_reparo\n"
                    + "FROM dbo.usr_sube_archivo usa\n"
                    + "WHERE usa.fk_idCondonacion = ?";
            listAEnc = (List<AdjuntarENC>) jT.query(SQL, new Object[]{id_condonacion}, new RowMapper<AdjuntarENC>() {
                public AdjuntarENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    AdjuntarENC sqlEnc = new AdjuntarENC(rs.getInt("id"),
                            rs.getInt("fk_idCliente"),
                            rs.getInt("fk_idCondonacion"),
                            rs.getString("tama�o_total"),
                            rs.getInt("numero_archivos"),
                            rs.getInt("di_id_reparo"));

                    return sqlEnc;
                }
            });

            aEnc = listAEnc.get(0);

//            aEnc = listAEnc.get(0);
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            aEnc = null;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            aEnc = null;
        }
        return aEnc;
    }

    private AdjuntarENC GetENC_Reparo(int id_condonacion, int id_reparo) {
        AdjuntarENC aEnc = new AdjuntarENC();
        List<AdjuntarENC> listAEnc = new ArrayList<AdjuntarENC>();
        try {
            String SQL = "SELECT usa.id,\n"
                    + "       usa.fk_idCliente,\n"
                    + "       usa.fk_idCondonacion,\n"
                    + "       usa.registrado,\n"
                    + "       usa.tama�o_total,\n"
                    + "       usa.numero_archivos,\n"
                    + "       usa.di_id_reparo\n"
                    + "FROM dbo.usr_sube_archivo usa\n"
                    + "WHERE usa.fk_idCondonacion = ? \n"
                    + "AND usa.di_id_reparo = ?;";
            // Messagebox.show("MMMM["+SQL+"]id_reparo"+id_reparo+"}");
            listAEnc = (List<AdjuntarENC>) jT.query(SQL, new Object[]{id_condonacion, id_reparo}, new RowMapper<AdjuntarENC>() {
                public AdjuntarENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    AdjuntarENC sqlEnc = new AdjuntarENC(rs.getInt("id"),
                            rs.getInt("fk_idCliente"),
                            rs.getInt("fk_idCondonacion"),
                            rs.getString("tama�o_total"),
                            rs.getInt("numero_archivos"),
                            rs.getInt("di_id_reparo"));

                    return sqlEnc;
                }
            });

            aEnc = listAEnc.get(0);

//            aEnc = listAEnc.get(0);
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            aEnc = null;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            aEnc = null;
        }
        return aEnc;
    }

    private List<AdjuntarDET> GetDET_IdEnc(int id_enc) {
        List<AdjuntarDET> listADet = new ArrayList<AdjuntarDET>();
        try {
            String SQL = "SELECT da.id_det_archivos,\n"
                    + "       da.fk_id_subearchivo,\n"
                    + "       da.patch_archivo,\n"
                    + "       da.extension,\n"
                    + "       da.tama�o,\n"
                    + "       da.registrado,\n"
                    + "       da.fk_idColaborador,\n"
                    + "       da.fileName,\n"
                    + "       da.sizeFloat\n"
                    + "FROM dbo.detalle_archivos da\n"
                    + "WHERE da.fk_id_subearchivo = ?";

            listADet = jT.query(SQL, new Object[]{id_enc}, new RowMapper<AdjuntarDET>() {
                public AdjuntarDET mapRow(ResultSet rs, int rowNum) throws SQLException {
                    AdjuntarDET sqlDet = new AdjuntarDET(rs.getInt("id_det_archivos"),
                            rs.getInt("fk_id_subearchivo"),
                            rs.getString("patch_archivo"),
                            rs.getString("extension"),
                            rs.getString("tama�o"),
                            rs.getInt("fk_idColaborador"),
                            false,
                            rs.getString("fileName"),
                            rs.getFloat("sizeFloat"));

                    return sqlDet;
                }
            });
        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det adjuntar\nError: " + e.toString());
            listADet = null;
        } catch (Exception e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det adjuntar\nError: " + e.toString());
            listADet = null;
        }

        return listADet;
    }
}
