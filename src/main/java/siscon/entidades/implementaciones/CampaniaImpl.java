/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.Campania;
import siscon.entidades.interfaces.CampaniaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class CampaniaImpl implements CampaniaInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public CampaniaImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertCampania(final Campania campania) {
        int id;

        final String SQL = "INSERT INTO [dbo].[campania]\n"
                + "           ([descripcion]\n"
                + "           ,[fecha_inicio]\n"
                + "           ,[fecha_fin]\n"
                + "           ,[estado])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,CONVERT(datetime,?,120)\n"
                + "           ,CONVERT(datetime,?,120)\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_campania"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, campania.getDescripcion());
                    ps.setDate(2, (Date) campania.getFecha_inicio());
                    ps.setDate(3, (Date) campania.getFecha_fin());
                    ps.setBoolean(4, campania.getEstado());
                    return ps;
                }
            },
                    keyHolder);

            id = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id = 0;

        }

        return id;
    }

    public boolean updateCampania(final Campania campania) {
        final String SQL = "UPDATE [dbo].[campania]\n"
                + "   SET [descripcion] = ?\n"
                + "      ,[fecha_inicio] = ?\n"
                + "      ,[fecha_fin] = ?\n"
                + "      ,[estado] = ?\n"
                + " WHERE id_campania = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                campania.getDescripcion(),
                campania.getFecha_inicio(),
                campania.getFecha_fin(),
                campania.getEstado(),
                campania.getId_campania()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public Campania getCampaniaXId(int id_campania) {
        Campania camapania = new Campania();

        final String SQL = "SELECT * FROM campania WHERE id_campania = ?";

        try {
            camapania = jdbcTemplate.queryForObject(SQL, new Object[]{id_campania}, new RowMapper<Campania>() {
                public Campania mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Campania camapania = new Campania();

                    camapania.setId_campania(rs.getInt("id_campania"));
                    camapania.setDescripcion(rs.getString("descripcion"));
                    camapania.setFecha_inicio(rs.getDate("fecha_inicio"));
                    camapania.setFecha_fin(rs.getDate("fecha_fin"));
                    camapania.setEstado(rs.getBoolean("estado"));
                    return camapania;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return camapania;
    }

    public List<Campania> getCampania() {
        List<Campania> lCampania = new ArrayList<Campania>();

        final String SQL = "SELECT * FROM campania";

        try {
            lCampania = jdbcTemplate.query(SQL, new RowMapper<Campania>() {
                public Campania mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Campania camapania = new Campania();

                    camapania.setId_campania(rs.getInt("id_campania"));
                    camapania.setDescripcion(rs.getString("descripcion"));
                    camapania.setFecha_inicio(rs.getDate("fecha_inicio"));
                    camapania.setFecha_fin(rs.getDate("fecha_fin"));
                    camapania.setEstado(rs.getBoolean("estado"));
                    return camapania;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return lCampania;
    }

    public boolean DeleteCampania(Campania campania) {
        String SQL;

        try {
            SQL = "DELETE FROM campania WHERE id_campania = ?";

            jdbcTemplate.update(SQL, new Object[]{campania.getId_campania()});

            return true;
        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
            return false;
        }
    }

    

}
