/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import configuracion.SisBojConf;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.UsuarioMapper;
import siscon.RowMapper.UsuarioParaPerfilMapper;
import siscon.entidades.interfaces.UsuarioDAO;
import siscon.entidades.usuario;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author exesilr
 */
public class UsuarioJDBC implements UsuarioDAO, Serializable {

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private SisBojConf _bojConfig;
    public usuario usr;
    static protected List<usuario> userList = new ArrayList<usuario>();

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);

    }

    public UsuarioJDBC() {
        _bojConfig = new SisBojConf();
        usr = new usuario();
    }

    public UsuarioJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        //  jdbcTemplate = new JdbcTemplate(dataSource);
        // UltPorcentajeConInteres = 0;
    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access
     *
     *
     * @param account
     * @return
     */
    public synchronized usuario findUser(String account) {
        int s = userList.size();
        for (int i = 0; i < s; i++) {
            usuario u = userList.get(i);
            if (account.equals(u.getAccount())) {
                return usuario.clone(u);
            }
        }
        return null;
    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access
     *
     *
     * @param cuenta
     * @return
     */
    public synchronized usuario buscarUsuario(String cuenta) {
        int _numeroReg = 0;
        // validacion 1 existe el registro en la DB
        String SQL = "SELECT U.*,\n"
                + "       C.ID ID_CONDONACION,\n"
                + "	  AT.COD AREATRAB\n"
                + "FROM USUARIO U\n"
                + "     INNER JOIN DBO.COLABORADOR C ON U.ID_USUARIO = C.ID_USUARIO\n"
                + "	INNER JOIN DBO.AREATRABAJO AT ON C.FK_DI_ID_AREATRABAJO = AT.ID\n"
                + "WHERE U.ALIAS = ?;";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        List<usuario> tempusr = jdbcTemplateObject.query(SQL, new Object[]{cuenta}, new UsuarioMapper());
        this.usr = tempusr.get(0);

        return this.usr;
    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access
     *
     *
     * @param rut
     * @return
     */
    public synchronized usuario buscarUsuarioRutDi(int rut) {
        int _numeroReg = 0;
        // validacion 1 existe el registro en la DB
        String SQL = "SELECT U.*,\n"
                + "       C.ID ID_CONDONACION,\n"
                + "	  AT.COD AREATRAB\n"
                + "FROM USUARIO U\n"
                + "     INNER JOIN DBO.COLABORADOR C ON U.ID_USUARIO = C.ID_USUARIO\n"
                + "	INNER JOIN DBO.AREATRABAJO AT ON C.FK_DI_ID_AREATRABAJO = AT.ID\n"
                + "WHERE u.di_rut=" + rut + " ";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        List<usuario> tempusr = jdbcTemplateObject.query(SQL, new UsuarioMapper());
        this.usr = tempusr.get(0);

        return this.usr;
    }
    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access
     *
     *
     * @param rut
     * @return
     */
    public synchronized usuario buscarUsuarioCuenta(String cuenta) {
        int _numeroReg = 0;
        List<usuario> tempusr= new ArrayList<usuario>();
        // validacion 1 existe el registro en la DB
        String SQL = "SELECT U.*,\n"
                + "       C.ID ID_CONDONACION,\n"
                + "	  AT.COD AREATRAB\n"
                + "FROM USUARIO U\n"
                + "     INNER JOIN DBO.COLABORADOR C ON U.ID_USUARIO = C.ID_USUARIO\n"
                + "	INNER JOIN DBO.AREATRABAJO AT ON C.FK_DI_ID_AREATRABAJO = AT.ID\n"
                + "WHERE u.alias='" + cuenta + "' ";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        try{
       // Messagebox.show("DocJDBC linea 96[" + SQL + "]");
         tempusr= jdbcTemplateObject.query(SQL, new UsuarioMapper());
        this.usr = tempusr.get(0);

        
        
        
                } catch (DataAccessException E) {
            Messagebox.show("Error al cargar el colaborador.\nError: " + E.getMessage());
            //id_colaborador = 0;
            this.usr = new usuario();
        }
        return this.usr;
    }
    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access *
     */
    public synchronized usuario buscarUsuarioRutDv(String rut) {
        int _numeroReg = 0;
        // validacion 1 existe el registro en la DB
        String SQL = "select * from usuario usr  where usr.dv_rut='" + rut + "' ";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        List<usuario> tempusr = jdbcTemplateObject.query(SQL, new UsuarioMapper());
        this.usr = tempusr.get(0);

        
        
        
        
        return this.usr;
    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access *
     */
    public boolean IsExisteUsuario(String cuenta) {
        int _numeroReg = 0;
        // validacion 1 existe el registro en la DB
        String SQLtramsito = "select count(*) from usuario usr  where usr.alias='" + cuenta + "' ";

        _bojConfig.print("UsuarioJDBC linea 77[" + SQLtramsito + "]");
        _numeroReg = jdbcTemplateObject.queryForInt(SQLtramsito);
        return _numeroReg > 0;

    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access *
     */
    public synchronized usuario updateUser(usuario user) {
        int s = userList.size();
        for (int i = 0; i < s; i++) {
            usuario u = userList.get(i);
            if (user.getAccount().equals(u.getAccount())) {
                userList.set(i, u = usuario.clone(user));
                return u;
            }
        }
        throw new RuntimeException("user not found " + user.getAccount());
    }

    /**
     * synchronized is just because we use static userList in this demo to
     * prevent concurrent access
     *
     *
     * @param cuenta
     * @return
     */
    public int buscarColaborador(String cuenta) {
        int id_colaborador = 0;
        // validacion 1 existe el registro en la DB
        String SQL = "SELECT c.id\n"
                + "FROM dbo.colaborador c\n"
                + "     INNER JOIN dbo.usuario u ON c.id_usuario = u.id_usuario\n"
                + "WHERE u.alias = ?";

        try {

            _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
            List<Integer> tempusr = (List<Integer>) jdbcTemplateObject.query(SQL, new Object[]{cuenta}, new RowMapper<Integer>() {
                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                    int id;
                    id = rs.getInt("id");

                    return id;

                }
            });
            id_colaborador = tempusr.get(0);
        } catch (DataAccessException E) {
            Messagebox.show("Error al cargar el colaborador.\nError: " + E.getMessage());
            id_colaborador = 0;
        }
        return id_colaborador;
    }

    public List<usuario> UsuariosPorPerfil(String CodPerfil) {
        //jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select * from usuario usu \n"
                + " inner join perfil_usuario peu on peu.id_usuario=usu.id_usuario\n"
                + " inner join perfil pe on pe.id_perfil=peu.id_perfil\n"
                + " where pe.dv_CodPerfil='" + CodPerfil + "'";

        List<usuario> dsol = null;
        try {
            dsol = jdbcTemplateObject.query(SQL, new UsuarioParaPerfilMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    public List<usuario> UsuariosSinPerfil() {
        //jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = " select * from usuario usu \n"
                + " left join perfil_usuario peu on peu.id_usuario=usu.id_usuario\n"
                + "  where peu.id_usuario is null";

        List<usuario> dsol = null;
        try {
            dsol = jdbcTemplateObject.query(SQL, new UsuarioParaPerfilMapper());
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            return dsol;
        }
        return dsol;
    }

    //   guardaUsuarioEnPerfil(aliass, rut,lbl_idCodPerfil.getValue())
    /**
     *
     * @param aliass
     * @param rut
     * @param CodPerfil
     * @return
     */
    public int guardaUsuarioEnPerfilds(final String aliass, final String rut, final String CodPerfil) {
        int id_provision;
        final String desc = "prueba";
        //  dv_desc="sadad";
        final String SQL = "INSERT INTO perfil_usuario\n"
                + "           ([id_usuario]\n"
                + "           ,[id_perfil] )\n"
                + "     VALUES\n"
                + "          ( (select usu.id_usuario from usuario usu where usu.alias=?)  \n"
                + "           , (select pe.id_perfil from perfil pe where pe.dv_CodPerfil=?) ) ";
        try {
            //Messagebox.show(SQL);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_perfil_usuario"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, aliass);
                    ps.setString(2, CodPerfil);

                    return ps;
                }
            },
                    keyHolder);

            id_provision = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            Messagebox.show("Error Guardando provision: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_provision = 0;

        }

        return id_provision;
    }

    /*
    public int BorrarUsuarioEnPerfilds(final String aliass, final String rut, final String CodPerfil) {
        int id_provision;
        final String desc = "prueba";
        //  dv_desc="sadad";
        final String SQL = "INSERT INTO perfil_usuario\n"
                + "           ([id_usuario]\n"
                + "           ,[id_perfil] )\n"
                + "     VALUES\n"
                + "          ( (select usu.id_usuario from usuario usu where usu.alias=?)  \n"
                + "           , (select pe.id_perfil from perfil pe where pe.dv_CodPerfil=?) ) ";
        try {
            Messagebox.show(SQL);
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplateObject.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_perfil_usuario"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, aliass);
                    ps.setString(2,CodPerfil );

                    return ps;
                }
            },
                    keyHolder);

            id_provision = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            Messagebox.show("Error Guardando provision: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_provision = 0;

        }

        return id_provision;
    }
    
     */
    public boolean BorrarUsuarioEnPerfilds(final String aliass, final String rut, final String CodPerfil) {
        String SQL;

        try {
            SQL = "DELETE from perfil_usuario where id_usuario=(select TOP 1 usu.id_usuario from usuario usu where usu.alias='" + aliass + "')  and id_perfil=(select TOP 1 pe.id_perfil from perfil pe where pe.dv_CodPerfil='" + CodPerfil + "') ";

            jdbcTemplateObject.update(SQL);

            return true;
        } catch (DataAccessException ex) {
            Messagebox.show("Error Eliminando provision: [" + ex.getMessage() + "] linea [492]");
            SisCorelog("SQL Exception: " + ex.toString());
            return false;
        }
    }

}
