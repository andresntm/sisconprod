/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author esilves
 */
public class TrackinJDBC {
            private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
     private JdbcTemplate jdbcTemplate;
    public TrackinJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }
    
    
    
      public boolean guardarTrackinEstado(final int idcondonacion,final String EstadoInicial,final String estadoFinal){
            boolean ok;
            jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO trackin_estado \n"
                + "           (dv_estadoInicio\n"
                + "           ,dv_estadoFin\n"
                + "           ,registrado\n"
                + "           ,di_fk_IdCondonacion)\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,getdate() \n"
                + "           ,?)\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_trakinestado"});
                    
                    ps.setString(1, EstadoInicial);
                    ps.setString(2, estadoFinal);
                    ps.setInt(3, idcondonacion);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Tra-Estado : 62["+e.toString()+"]");
            ok=false;
        }
        return ok;
    
}
 
}
