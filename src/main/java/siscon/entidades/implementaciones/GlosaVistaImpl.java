/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import siscon.RowMappers.VistaGlosaRowMapper;
import siscon.entidades.GlosaVista;
import siscon.entidades.interfaces.GlosaVistaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author exesilr
 */
public class GlosaVistaImpl implements GlosaVistaInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public GlosaVistaImpl(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public List<GlosaVista> getListGlosaVista() {
        List<GlosaVista> nn = null;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String hostId = "1";    /// este valor debe venir en el parametro entrada
        String SELECT = " SELECT * FROM siscore_glosario "
                        + " WHERE id_siscore_vista=?";
        // + " AND id <> ?"
        // + " ORDER BY host_name";

        try {
            SisCorelog("hostId: [" + hostId + "]");
            nn = jdbcTemplate.query(SELECT, new VistaGlosaRowMapper(), new Object[]{hostId});

            SisCorelog("hostId: [" + hostId + "]");

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            //returnGuardar = false;
        }

        return nn;

    }

}
