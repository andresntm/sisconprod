/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import config.SisConGenerales;
import configuracion.SisBojConf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.chart.model.CategoryModel;
import org.zkoss.chart.model.DefaultCategoryModel;
import org.zkoss.zul.Messagebox;
import siscon.entidades.DetalleCliente;
import siscon.entidades.interfaces.DetalleOperacionesClientes;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author exesilr
 */
public class DetalleOperacionesClientesImp implements DetalleOperacionesClientes {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplateObject;
    private DataSource _internodataSource = null;
    private SisBojConf _bojConfig;
    private String nombre_spDetOper;
    List<DetalleCliente> DetalleOperacionesCliente;
    List<DetalleCliente> ListDet;
    MvcConfig _datasource;
    ClienteSisconJDBC _cliente;
  SisConGenerales _control;
    public DetalleOperacionesClientesImp(DataSource dataSource) throws SQLException {
        this.dataSource = dataSource;
        _bojConfig = new SisBojConf();
        DetalleOperacionesCliente = new ArrayList<DetalleCliente>();
 _control = new SisConGenerales();
        try {
            this._datasource = new MvcConfig();
            _internodataSource = _datasource.getDataSource();
            _cliente = new ClienteSisconJDBC(_datasource.getDataSource());
        } catch (SQLException e) {
            Messagebox.show("ERROR MvcConfig:[" + e.getMessage() + "]");
        }

        ListDet = new ArrayList<DetalleCliente>();
        nombre_spDetOper = _bojConfig.Nombresp_detClie();
    }

    public DetalleOperacionesClientesImp() throws SQLException {
        _control = new SisConGenerales();
    }

    public CategoryModel getModelInformes(String sss) {
        this.jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_web_inf_tot_vis_web");
        //jdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("GET_EVENT_BY_TITLE").useInParameterNames("IN_TITLE").returningResultSet("events",ParameterizedBeanPropertyRowMapper.newInstance(Event.class));
        sss = "METROPOLITANA CORDILLERA";
        SqlParameterSource in = new MapSqlParameterSource().addValue("reg", sss);
        Map<String, Object> out = jdbcCall.execute(in);
        CategoryModel model;

        model = new DefaultCategoryModel();
        //private static final CategoryModel model;
        System.out.print("####### ---- aqui imorimimos el ser de output -------#######");
        for (String keys : out.keySet()) {
            System.out.println(keys);
        }

        ArrayList resultList = (ArrayList) out.get("#result-set-1");

        Map resultlMap = (Map) resultList.get(0);

        for (int nn = 0; nn < resultList.size(); nn++) {
            Map resultlMapx = (Map) resultList.get(nn);
            model.setValue("%Cpl. Castigo", ((String) resultlMapx.get("FLD_NOM")), (Double) resultlMapx.get("fld_Cumpl_Recupero"));
            model.setValue("fld_cumpl_90_vig", ((String) resultlMapx.get("FLD_NOM")), (Double) resultlMapx.get("fld_cumpl_90_vig"));
            model.setValue("fld_cumpl_180_vig", ((String) resultlMapx.get("FLD_NOM")), (Double) resultlMapx.get("fld_cumpl_180_vig"));
            model.setValue("%Cpl. Hipotecario", ((String) resultlMapx.get("FLD_NOM")), (Double) resultlMapx.get("fld_por_cpl_hip"));
            System.out.print("####### i:[" + nn + "]----EJECUTIVO[" + ((String) resultlMapx.get("FLD_NOM")) + "] fld_Cumpl_Recupero :[" + ((Double) resultlMapx.get("fld_Cumpl_Recupero")).toString() + "]  fld_cumpl_90_vig[" + ((Double) resultlMapx.get("fld_cumpl_90_vig")).toString() + "]  fld_cumpl_180_vig[" + ((Double) resultlMapx.get("fld_cumpl_180_vig")).toString() + "] fld_por_cpl_hip[" + ((Double) resultlMapx.get("fld_por_cpl_hip")).toString() + "]-------#######");
        }
        System.out.print("####### ---- aqui imorimimos Prueba de Model[[[" + model.getCategories().toString() + "]]]el ser de output FIN-------#######");

        return model;
    }

    public void setDataSource(DataSource ds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<DetalleCliente> Cliente(int rut, String user) {

        // DetalleOperacionesCliente.set(0, element)
        ListDet = new ArrayList<DetalleCliente>();
        try {

            if (_cliente.ExisteCliente(rut)) {
                
                   if(_control.is_NovedadMontosNiOperaciones(rut,user)){
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";

                ////para efectos de Produccioin
                dataSource = _datasource.getDataSource();
                   }
            }

            //List<Contacto> listContact
            this.jdbcCall = new SimpleJdbcCall(dataSource).withFunctionName(nombre_spDetOper).returningResultSet("rs", new ParameterizedRowMapper<DetalleCliente>() {

                public DetalleCliente mapRow(ResultSet rs, int i) throws SQLException {

                    DetalleCliente aDetalleCliente = new DetalleCliente();
                    aDetalleCliente.setCedente(rs.getString("fld_ced"));
                    aDetalleCliente.setOperacion(rs.getString("fld_ope"));
                    aDetalleCliente.setOperacionOriginal(rs.getString("fld_ope_ini"));
                    aDetalleCliente.setTipoCedente(rs.getString("fld_tip_cre"));
                    aDetalleCliente.setDetalleCredito(rs.getString("fld_det_tip_cre_max"));
                    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    aDetalleCliente.setMora(rs.getLong("fld_mor"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));
                    aDetalleCliente.setMoraEnPesosChileno(nf.format(rs.getLong("fld_mor")).replaceFirst("Ch", ""));
                    aDetalleCliente.setSaldoinsoluto(rs.getLong("fld_sdo"));
                    aDetalleCliente.setSaldoEnPesosChileno(nf.format(rs.getLong("fld_sdo")).replaceFirst("Ch", ""));
                    aDetalleCliente.setCuotasMora(rs.getInt("fld_cuo_mor"));
                    aDetalleCliente.setDiasMora(rs.getInt("fld_dia_mor"));
                    aDetalleCliente.setProductos(rs.getString("fld_est"));
                    aDetalleCliente.setFechaFencimiento(rs.getString("fld_fec_vto"));
                    aDetalleCliente.setFechaCastigo(rs.getString("fld_fec_cas"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));

                    
                    
                     // estos parametros no estan en el sp produccion
                    aDetalleCliente.setFLD_MOR(rs.getString("fld_mor"));
                    aDetalleCliente.setFLD_SDO(rs.getString("fld_sdo"));
                    aDetalleCliente.setFLD_DET_TIP_CRE_MIN(rs.getString("FLD_DET_TIP_CRE_MIN"));
                    aDetalleCliente.setPAGADAS(rs.getString("PAGADAS"));
                    aDetalleCliente.setTOTAL_CUOTAS(rs.getString("TOTAL_CUOTAS"));
                    // aDetalleCliente.setNRO_DEMANDAS(rs.getString("NRO_DEMANDAS"));

                    return aDetalleCliente;//rs.getString(1);   

                }
            ;
            });
            
            
            SisCorelog("###############################DEtalle Operaiones Clinete imp linea 158   nombre_spInfoClie: [" + nombre_spDetOper + "] rut :["+rut+"] user :["+user+"]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spDetOper + "]");
            SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("col", 1).addValue("usuario", user);

            Map<String, Object> out = jdbcCall.execute(in);

            DetalleOperacionesCliente = (List<DetalleCliente>) out.get("rs");

            for (DetalleCliente detCli : DetalleOperacionesCliente) {
                if ((!(detCli.getOperacionOriginal() == null)) && (!detCli.getOperacionOriginal().isEmpty()) && (detCli.getOperacionOriginal().length() > 0)) {
                    //Si no tiene mora excluir del proceso
                    if (detCli.getMora() > 0) {
                        ListDet.add(detCli);
                    }
                }
            }
            DetalleOperacionesCliente.clear();
            DetalleOperacionesCliente = ListDet;

        } catch (SQLException ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            return DetalleOperacionesCliente;
        }

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");

        return DetalleOperacionesCliente;

    }

    
    
    
    
    public List<DetalleCliente> ClienteV2(int rut, String user) {

        // DetalleOperacionesCliente.set(0, element)
        ListDet = new ArrayList<DetalleCliente>();
        try {

            if (_cliente.ExisteCliente(rut)) {
                
                   if(_control.is_NovedadMontosNiOperaciones(rut,user)){
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";

                ////para efectos de Produccioin
                dataSource = _datasource.getDataSource();
                   }
            }

            //List<Contacto> listContact
            this.jdbcCall = new SimpleJdbcCall(dataSource).withFunctionName(nombre_spDetOper).returningResultSet("rs", new ParameterizedRowMapper<DetalleCliente>() {

                public DetalleCliente mapRow(ResultSet rs, int i) throws SQLException {

                    DetalleCliente aDetalleCliente = new DetalleCliente();
                    aDetalleCliente.setCedente(rs.getString("fld_ced"));
                    aDetalleCliente.setOperacion(rs.getString("fld_ope"));
                    aDetalleCliente.setOperacionOriginal(rs.getString("fld_ope_ini"));
                    aDetalleCliente.setTipoCedente(rs.getString("fld_tip_cre"));
                    aDetalleCliente.setDetalleCredito(rs.getString("fld_det_tip_cre_max"));
                    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    aDetalleCliente.setMora(rs.getLong("fld_mor"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));
                    aDetalleCliente.setMoraEnPesosChileno(nf.format(rs.getLong("fld_mor")).replaceFirst("Ch", ""));
                    aDetalleCliente.setSaldoinsoluto(rs.getLong("fld_sdo"));
                    aDetalleCliente.setSaldoEnPesosChileno(nf.format(rs.getLong("fld_sdo")).replaceFirst("Ch", ""));
                    aDetalleCliente.setCuotasMora(rs.getInt("fld_cuo_mor"));
                    aDetalleCliente.setDiasMora(rs.getInt("fld_dia_mor"));
                    aDetalleCliente.setProductos(rs.getString("fld_est"));
                    aDetalleCliente.setFechaFencimiento(rs.getString("fld_fec_vto"));
                    aDetalleCliente.setFechaCastigo(rs.getString("fld_fec_cas"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));

                    
                    
                     // estos parametros no estan en el sp produccion
                    aDetalleCliente.setFLD_MOR(rs.getString("fld_mor"));
                    aDetalleCliente.setFLD_SDO(rs.getString("fld_sdo"));
                    aDetalleCliente.setFLD_DET_TIP_CRE_MIN(rs.getString("FLD_DET_TIP_CRE_MIN"));
                    aDetalleCliente.setPAGADAS(rs.getString("PAGADAS"));
                    aDetalleCliente.setTOTAL_CUOTAS(rs.getString("TOTAL_CUOTAS"));
                     aDetalleCliente.setNRO_DEMANDAS(rs.getString("NRO_DEMANDAS"));

                    return aDetalleCliente;//rs.getString(1);   

                }
            ;
            });
            
            
            SisCorelog("###############################DEtalle Operaiones Clinete imp linea 158   nombre_spInfoClie: [" + nombre_spDetOper + "] rut :["+rut+"] user :["+user+"]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spDetOper + "]");
            SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("col", 1).addValue("usuario", user);

            Map<String, Object> out = jdbcCall.execute(in);

            DetalleOperacionesCliente = (List<DetalleCliente>) out.get("rs");

            for (DetalleCliente detCli : DetalleOperacionesCliente) {
                if ((!(detCli.getOperacionOriginal() == null)) && (!detCli.getOperacionOriginal().isEmpty()) && (detCli.getOperacionOriginal().length() > 0)) {
                    //Si no tiene mora excluir del proceso
                    if (detCli.getMora() > 0) {
                        ListDet.add(detCli);
                    }
                }
            }
            DetalleOperacionesCliente.clear();
            DetalleOperacionesCliente = ListDet;

        } catch (SQLException ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            return DetalleOperacionesCliente;
        }

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");

        return DetalleOperacionesCliente;

    }

    
    public List<DetalleCliente> Cliente(int rut, String user, final int rechazo) {

        // DetalleOperacionesCliente.set(0, element)
        ListDet = new ArrayList<DetalleCliente>();
        try {
            //##ActualizacionData
            dataSource = _datasource.getDataSource();
            boolean _iscliente=_cliente.ExisteCliente(rut);
            boolean _hayNovedad=_control.is_NovedadMontosNiOperaciones(rut, user);
            boolean _isejecutivo=_control.is_Ejecutivo(user);  //1 si es ejecutivo 0 si no lo es
            //boolean _ismoduloReparo_ejecutivo =_control.is_moduloReparoEjecutivo(user); 
            System.out.print("####### ----CUENTA:["+user+"] LINEA 308 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"] _isejecutivo : ["+_isejecutivo+"]");
            if (_iscliente && rechazo == 0 && _isejecutivo) {
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                System.out.print("####### ---- LINEA 308 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"] _isejecutivo : ["+_isejecutivo+"]");
                nombre_spDetOper = "sp_web_syscon_det_ope_cli";

                ////para efectos de Produccioin
            } else if (_iscliente) { //else momentanio solo para pruebas favor cometar una vez utilizado
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                if (_hayNovedad && !_isejecutivo) {
                   System.out.print("####### ---- LINEA 315 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                } else 
                    if(!_isejecutivo){
                    System.out.print("####### ---- LINEA 322 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                }
                    else {
                                      System.out.print("####### ---- LINEA 326 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli";
                    
                    }

            }

            //List<Contacto> listContact
            this.jdbcCall = new SimpleJdbcCall(dataSource).withFunctionName(nombre_spDetOper).returningResultSet("rs", new ParameterizedRowMapper<DetalleCliente>() {

                public DetalleCliente mapRow(ResultSet rs, int i) throws SQLException {

                    DetalleCliente aDetalleCliente = new DetalleCliente();
                    aDetalleCliente.setCedente(rs.getString("fld_ced"));
                    aDetalleCliente.setOperacion(rs.getString("fld_ope"));
                    aDetalleCliente.setOperacionOriginal(rs.getString("fld_ope_ini"));
                    aDetalleCliente.setTipoCedente(rs.getString("fld_tip_cre"));
                    aDetalleCliente.setDetalleCredito(rs.getString("fld_det_tip_cre_max"));
                    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    aDetalleCliente.setMora(rs.getLong("fld_mor"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));
                    aDetalleCliente.setMoraEnPesosChileno(nf.format(rs.getLong("fld_mor")).replaceFirst("Ch", ""));
                    aDetalleCliente.setSaldoinsoluto(rs.getLong("fld_sdo"));
                    aDetalleCliente.setSaldoEnPesosChileno(nf.format(rs.getLong("fld_sdo")).replaceFirst("Ch", ""));
                    aDetalleCliente.setCuotasMora(rs.getInt("fld_cuo_mor"));
                    aDetalleCliente.setDiasMora(rs.getInt("fld_dia_mor"));
                    aDetalleCliente.setProductos(rs.getString("fld_est"));
                    aDetalleCliente.setFechaFencimiento(rs.getString("fld_fec_vto"));
                    aDetalleCliente.setFechaCastigo(rs.getString("fld_fec_cas"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));

                    aDetalleCliente.setFLD_MOR(rs.getString("fld_mor"));
                    aDetalleCliente.setFLD_SDO(rs.getString("fld_sdo"));
                    aDetalleCliente.setFLD_DET_TIP_CRE_MIN(rs.getString("FLD_DET_TIP_CRE_MIN"));
                    aDetalleCliente.setPAGADAS(rs.getString("PAGADAS"));
                    aDetalleCliente.setTOTAL_CUOTAS(rs.getString("TOTAL_CUOTAS"));
                    // aDetalleCliente.setNRO_DEMANDAS(rs.getString("NRO_DEMANDAS"));

                    return aDetalleCliente;//rs.getString(1);   

                }
            ;
            });
             SisCorelog("###############################DEtalle Operaiones Clinete imp linea 251   nombre_spInfoClie: [" + nombre_spDetOper + "]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spDetOper + "]");
            SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("col", 1).addValue("usuario", user);

            Map<String, Object> out = jdbcCall.execute(in);

            DetalleOperacionesCliente = (List<DetalleCliente>) out.get("rs");

            for (DetalleCliente detCli : DetalleOperacionesCliente) {
                if ((!(detCli.getOperacionOriginal() == null)) && (!detCli.getOperacionOriginal().isEmpty()) && (detCli.getOperacionOriginal().length() > 0)) {
                    //Si no tiene mora excluir del proceso
                    if (detCli.getMora() > 0) {
                        ListDet.add(detCli);
                    }
                }
            }
            DetalleOperacionesCliente.clear();
            DetalleOperacionesCliente = ListDet;

        } catch (Exception ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            return DetalleOperacionesCliente;
        }

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");

        return DetalleOperacionesCliente;

    }
    
    public List<DetalleCliente> Cliente(int rut, String user, final int rechazo,String modulo) {
System.out.print("####### Estoy en el Modulo de 43--------------------------------------------------------------####");
        // DetalleOperacionesCliente.set(0, element)
        ListDet = new ArrayList<DetalleCliente>();
        try {
            //##ActualizacionData
            dataSource = _datasource.getDataSource();
            boolean _iscliente=_cliente.ExisteCliente(rut);
            boolean _hayNovedad=_control.is_NovedadMontosNiOperaciones(rut, user);
            boolean _isejecutivo=_control.is_Ejecutivo(user);  //1 si es ejecutivo 0 si no lo es
            //boolean _ismoduloReparo_ejecutivo =_control.is_moduloReparoEjecutivo(user); 
            System.out.print("####### ----CUENTA:["+user+"] LINEA 413 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"] _isejecutivo : ["+_isejecutivo+"]");
            if (_iscliente && rechazo == 0 && _isejecutivo && !modulo.equals("MODULOS_EJE_REPARO") ) {
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                System.out.print("####### ---- LINEA 416 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"] _isejecutivo : ["+_isejecutivo+"]");
                nombre_spDetOper = "sp_web_syscon_det_ope_cli";

                ////para efectos de Produccioin
            } else if (_iscliente) { //else momentanio solo para pruebas favor cometar una vez utilizado
                //Messagebox.show("Enviando Formulario de Condonación a Analista Validador");
                if (_hayNovedad && !_isejecutivo) {
                   System.out.print("####### ---- LINEA 423 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                } else 
                    if(!_isejecutivo  ){
                    System.out.print("####### ---- LINEA 427 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                }
                    else if(_isejecutivo &&   modulo.equals("MODULOS_EJE_REPARO") ) {
                     System.out.print("####### \n ----MODULO :["+modulo+"] LINEA 432 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                     nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                    
                    
                    } else {
                     System.out.print("####### ---- LINEA 437 Parametros Envio SP Clientes Operaciones Rut[rechazo]----#######rechazo["+rechazo+"],iscliente["+_iscliente+"],haynovedad["+_hayNovedad+"]");
                    nombre_spDetOper = "sp_web_syscon_det_ope_cli";
                    }

            }

            //List<Contacto> listContact
            this.jdbcCall = new SimpleJdbcCall(dataSource).withFunctionName(nombre_spDetOper).returningResultSet("rs", new ParameterizedRowMapper<DetalleCliente>() {

                public DetalleCliente mapRow(ResultSet rs, int i) throws SQLException {

                    DetalleCliente aDetalleCliente = new DetalleCliente();
                    aDetalleCliente.setCedente(rs.getString("fld_ced"));
                    aDetalleCliente.setOperacion(rs.getString("fld_ope"));
                    aDetalleCliente.setOperacionOriginal(rs.getString("fld_ope_ini"));
                    aDetalleCliente.setTipoCedente(rs.getString("fld_tip_cre"));
                    aDetalleCliente.setDetalleCredito(rs.getString("fld_det_tip_cre_max"));
                    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                    aDetalleCliente.setMora(rs.getLong("fld_mor"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));
                    aDetalleCliente.setMoraEnPesosChileno(nf.format(rs.getLong("fld_mor")).replaceFirst("Ch", ""));
                    aDetalleCliente.setSaldoinsoluto(rs.getLong("fld_sdo"));
                    aDetalleCliente.setSaldoEnPesosChileno(nf.format(rs.getLong("fld_sdo")).replaceFirst("Ch", ""));
                    aDetalleCliente.setCuotasMora(rs.getInt("fld_cuo_mor"));
                    aDetalleCliente.setDiasMora(rs.getInt("fld_dia_mor"));
                    aDetalleCliente.setProductos(rs.getString("fld_est"));
                    aDetalleCliente.setFechaFencimiento(rs.getString("fld_fec_vto"));
                    aDetalleCliente.setFechaCastigo(rs.getString("fld_fec_cas"));
                    aDetalleCliente.setMarcaRenegociado(rs.getString("fld_ren_sn"));

                    aDetalleCliente.setFLD_MOR(rs.getString("fld_mor"));
                    aDetalleCliente.setFLD_SDO(rs.getString("fld_sdo"));
                    aDetalleCliente.setFLD_DET_TIP_CRE_MIN(rs.getString("FLD_DET_TIP_CRE_MIN"));
                    aDetalleCliente.setPAGADAS(rs.getString("PAGADAS"));
                    aDetalleCliente.setTOTAL_CUOTAS(rs.getString("TOTAL_CUOTAS"));
                    // aDetalleCliente.setNRO_DEMANDAS(rs.getString("NRO_DEMANDAS"));

                    return aDetalleCliente;//rs.getString(1);   

                }
            ;
            });
             SisCorelog("###############################DEtalle Operaiones Clinete imp linea 251   nombre_spInfoClie: [" + nombre_spDetOper + "]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spDetOper + "]");
            SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("col", 1).addValue("usuario", user);

            Map<String, Object> out = jdbcCall.execute(in);

            DetalleOperacionesCliente = (List<DetalleCliente>) out.get("rs");

            for (DetalleCliente detCli : DetalleOperacionesCliente) {
                if ((!(detCli.getOperacionOriginal() == null)) && (!detCli.getOperacionOriginal().isEmpty()) && (detCli.getOperacionOriginal().length() > 0)) {
                    //Si no tiene mora excluir del proceso
                    if (detCli.getMora() > 0) {
                        ListDet.add(detCli);
                    }
                }
            }
            DetalleOperacionesCliente.clear();
            DetalleOperacionesCliente = ListDet;

        } catch (Exception ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            return DetalleOperacionesCliente;
        }

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");

        return DetalleOperacionesCliente;

    }

    public boolean insertCliente_condonado_Det(final DetalleCliente detCli) {
        boolean retorna = false;
        jdbcTemplateObject = new JdbcTemplate(dataSource);

        final String SQL = "INSERT INTO [Cliente_En_Condonacion_Det]\n"
                + "           ([id_cli_Cond_Det]\n"
                + "           ,[fk_id_Cli_Cond_Enc]\n"
                + "           ,[cedente]\n"
                + "           ,[operacion]\n"
                + "           ,[operacion_ini]\n"
                + "           ,[tipo_credito]\n"
                + "           ,[det_tipo_cre_max]\n"
                + "           ,[mora]\n"
                + "           ,[saldo]\n"
                + "           ,[cuota_morosa]\n"
                + "           ,[dias_mora]\n"
                + "           ,[estado_producto]\n"
                + "           ,[fecha_vencimiento]\n"
                + "           ,[fecha_castigo]\n"
                + "           ,[marc_renegociado])"
                + "     VALUES\n"
                + "            (?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,(CONVERT(datetime,?,120))\n"
                + "            ,(CONVERT(datetime,?,120))\n"
                + "            ,?)";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_cli_Cond_Det"});
                    ps.setInt(1, detCli.getId_cli_Cond_Det());
                    ps.setInt(2, detCli.getFk_id_Cli_Cond_Enc());
                    ps.setString(3, detCli.getCedente());
                    ps.setString(4, detCli.getOperacion());
                    ps.setString(5, detCli.getOperacionOriginal());
                    ps.setString(6, detCli.getTipoCedente());
                    ps.setString(7, detCli.getDetalleCredito());
                    ps.setLong(8, detCli.getMora());
                    ps.setLong(9, detCli.getSaldoinsoluto());
                    ps.setInt(10, detCli.getCuotasMora());
                    ps.setInt(11, detCli.getDiasMora());
                    ps.setString(12, detCli.getProductos());
                    ps.setString(13, detCli.getFechaFencimiento());
                    ps.setString(14, detCli.getFechaCastigo());
                    ps.setString(15, detCli.getMarcaRenegociado());
                    return ps;
                }
            }, key);

            retorna = true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = false;
        }
        return retorna;
    }

    public boolean insertCliente_condonado_Enc(final int rut, final char dv, final DetalleCliente detCli) {
        boolean retorna = false;
        jdbcTemplateObject = new JdbcTemplate(dataSource);

        final String SQL = "INSERT INTO [Cliente_En_Condonacion_Enc]\n"
                + "           ([id_Cli_Cond_Enc]\n"
                + "           ,[rut]\n"
                + "           ,[digVer]\n"
                + "           ,[Fecha_Creacion])\n"
                + "     VALUES\n"
                + "            (?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,(CONVERT(datetime,GETDATE(),120)))";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_Cli_Cond_Enc"});
                    ps.setInt(1, rut);
                    ps.setString(2, new StringBuffer().append(dv).toString());
                    ps.setInt(3, detCli.getFk_id_Cli_Cond_Enc());
                    return ps;
                }
            }, key);

            retorna = true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = false;
        }
        return retorna;
    }

    public List<DetalleCliente> selectCliente_condonado(int rut, char dv) {
        List<DetalleCliente> detCli = null;
        /* String SQL2 = "select tp.dv_NomTipDcto,est.dv_NomEstado,sd.* from tb_Sys_Documento  as sd \n"
                  + "  left join [dbo].[tb_Sys_TipoDcto] as tp on tp.di_IdTipDcto=sd.di_fk_TipoDcto\n"
                     + "left join  [dbo].[tb_Sys_Estado] as est on est.di_IdEstado=sd.di_fk_IdEstado\n"
                    + " where di_fk_IdOper=? \n";*/
        String SQL2 = "SELECT	det.*, enc.Fecha_Creacion\n"
                + "FROM	Cliente_En_Condonacion_Det AS det\n"
                + "		INNER JOIN Cliente_En_Condonacion_Enc AS enc\n"
                + "		ON det.fk_id_Cli_Cond_Enc = enc.id_Cli_Cond_Enc\n"
                + "WHERE	enc.rut = ?\n"
                + "AND	enc.digVer = ?";
        try {
            detCli = jdbcTemplateObject.query(SQL2, new Object[]{rut, dv}, new RowMapper<DetalleCliente>() {
                public DetalleCliente mapRow(ResultSet rs, int rowNum) throws SQLException {
                    DetalleCliente detClient = new DetalleCliente();
                    detClient.setId_cli_Cond_Det(rs.getInt(""));
                    detClient.setFk_id_Cli_Cond_Enc(rs.getInt(""));
                    detClient.setCedente(rs.getString(""));
                    detClient.setOperacion(rs.getString(""));
                    detClient.setOperacionOriginal(rs.getString(""));
                    detClient.setTipoCedente(rs.getString(""));
                    detClient.setDetalleCredito(rs.getString(""));
                    detClient.setMora(rs.getInt(""));
                    detClient.setSaldoinsoluto(rs.getInt(""));
                    detClient.setCuotasMora(rs.getInt(""));
                    detClient.setDiasMora(rs.getInt(""));
                    detClient.setProductos(rs.getString(""));
                    detClient.setFechaFencimiento(rs.getString(""));
                    detClient.setFechaCastigo(rs.getString(""));
                    detClient.setMarcaRenegociado(rs.getString(""));

                    return detClient;
                }
            });

        } catch (DataAccessException ex) {
            System.out.print("####### ---- ERROR de EJECUCION de Query///listDocumento/// ERR:[" + ex.getMessage() + "]----#######");
        }

        return detCli;
    }

    public boolean insertDetSpOperacionesClientCondonado(final int id_condonacion, final int id_trakingInfoCliente) {
        boolean retorna = false;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        final String SQL = "INSERT INTO [tracking_morahoy_ope]\n"
                + "           ([fk_idCondonacion]\n"
                + "           ,[fk_idTrkMorClie]\n"
                + "           ,[fld_ced]\n"
                + "           ,[fld_ope]\n"
                + "           ,[fld_ope_ini]\n"
                + "           ,[fld_tip_cre]\n"
                + "           ,[fld_det_tip_cre_max]\n"
                + "           ,[fld_det_tip_cre_min]\n"
                + "           ,[fld_mor]\n"
                + "           ,[fld_sdo]\n"
                + "           ,[fld_cuo_mor]\n"
                + "           ,[pagadas]\n"
                + "           ,[total_cuotas]\n"
                + "           ,[fld_dia_mor]\n"
                + "           ,[fld_est]\n"
                + "           ,[fld_fec_vto]\n"
                + "           ,[fld_fec_cas]\n"
                + "           ,[fld_ren_sn])"
                + "     VALUES\n"
                + "            (?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?)";
        KeyHolder key = new GeneratedKeyHolder();
        try {

            for (final DetalleCliente detCli : DetalleOperacionesCliente) {
                jdbcTemplateObject.update(new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                        PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id_cli_Cond_Det"});
                        ps.setInt(1, id_condonacion);
                        ps.setInt(2, id_trakingInfoCliente);
                        ps.setString(3, detCli.getCedente());
                        ps.setString(4, detCli.getOperacion());
                        ps.setString(5, detCli.getOperacionOriginal());
                        ps.setString(6, detCli.getTipoCedente());
                        ps.setString(7, detCli.getDetalleCredito());
                        ps.setString(8, detCli.getFLD_DET_TIP_CRE_MIN());
                        ps.setString(9, detCli.getFLD_MOR());
                        ps.setString(10, detCli.getFLD_SDO());
                        ps.setString(11, Integer.toString(detCli.getCuotasMora()));
                        ps.setString(12, detCli.getPAGADAS());
                        ps.setString(13, detCli.getTOTAL_CUOTAS());
                        ps.setString(14, Integer.toString(detCli.getDiasMora()));
                        ps.setString(15, detCli.getProductos());
                        ps.setString(16, detCli.getFechaFencimiento());
                        ps.setString(17, detCli.getFechaCastigo());
                        ps.setString(18, detCli.getMarcaRenegociado());
                        return ps;
                    }
                }, key);
            }

            retorna = true;
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = false;
        }
        return retorna;
    }

}
