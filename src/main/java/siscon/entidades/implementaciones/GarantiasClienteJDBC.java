/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.GarantiasCliente;
import siscon.entidades.ValorMonto;
import siscon.entidades.interfaces.GarantiasClienteInterfaz;

/**
 *
 * @author esilves
 */
public class GarantiasClienteJDBC  implements GarantiasClienteInterfaz{

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<GarantiasCliente> gclie = new ArrayList<GarantiasCliente>();
    public  ValorMonto SumaMontoTotalGarantias;
    public GarantiasClienteJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }


    
        public List<GarantiasCliente> GarantiasCliente(int rut,String Usuario) {
      //  List<GarantiasCliente> gclie = null;
      
        Map<String, Object> out;
         SqlParameterSource in;
        // DetalleOperacionesCliente.set(0, element)
       
            //List<Contacto> listContact
            this.jdbcCall = new SimpleJdbcCall(dataSource).
                    withFunctionName("sp_web_syscon_gtias").
                    returningResultSet("rs", new ParameterizedRowMapper<GarantiasCliente>() {

                        public GarantiasCliente mapRow(ResultSet rs, int i) throws SQLException {

                            GarantiasCliente gclie = new GarantiasCliente();
                            gclie.setRut(rs.getString("RUT1"));
                            gclie.setTipo(rs.getString("TIPO"));
                            gclie.setSub_tipo(rs.getString("SUB_TIPO"));
                            gclie.setNombre_producto(rs.getString("NOMBRE_PRODUCTO"));
                            gclie.setGarantia(rs.getString("GARANTIA"));
                            gclie.setFecha_transaccion(rs.getString("FECHA_TASACION"));
                            gclie.setMonto(rs.getString("MONTO"));
                            gclie.setDireccion(rs.getString("DIRECCION"));
                            gclie.setDireccion_otro(rs.getString("DIRECCION_OTROS"));
                            gclie.setComuna_rol(rs.getString("COMUNA_ROL"));
                            gclie.setRol_manzana(rs.getString("ROL_MANZANA"));
                            gclie.setRol_predio_prop(rs.getString("ROL_PREDIO_PROP"));
                            gclie.setRol_completo(rs.getString("ROL_COMPLETO"));
                            gclie.setAuto_modelo(rs.getString("AUTO_MODELO"));
                            gclie.setAuto_patente(rs.getString("AUTO_PATENTE"));
                            
                            NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                            
                            return gclie;//rs.getString(1);   

                        }
                    ;
            });
           // String usu="ejesiscon";
            in = new MapSqlParameterSource().addValue("fld_rut",rut).addValue("usuario",Usuario);
 try {
             out= jdbcCall.execute(in);

            gclie = (List<GarantiasCliente>) out.get("rs");
            
            
            
            
            

        } catch (DataAccessException ex) {
            
            
            Messagebox.show("ERROR SP CALL :["+ex.getMessage()+"]");
            return gclie;
        }

      //  System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");

        return gclie;

    }
    
    public String SumaTotalGarantiasPesos(){
             float sumaTotal=0;
             
                     for (final GarantiasCliente Gtias : gclie) {
          //  list2.add(tipodocto.getDv_NomTipDcto());
            sumaTotal=sumaTotal+Gtias.getMontoFloat();
            
            
        }
             
             this.SumaMontoTotalGarantias =new ValorMonto(sumaTotal);
        
     return    this.SumaMontoTotalGarantias.getValorPesos();
}
    
}
