/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.UsuarioMapper;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.interfaces.Trackin_EstadoInterfaz;
import siscon.entidades.usuario;

/**
 *
 * @author excosoc
 */
public class Trackin_EstadoImplements implements Trackin_EstadoInterfaz {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public Trackin_EstadoImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public Trackin_EstadoImplements(DataSource dS) {
        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public int insertTrackin_Estado(final Trackin_Estado tE) {
        KeyHolder key = new GeneratedKeyHolder();
        final String sql = "INSERT INTO DBO.TRACKIN_ESTADO\n"
                + "(\n"
                + "    --ID_TRAKINESTADO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "    DV_ESTADOINICIO,\n"
                + "    DV_ESTADOFIN,\n"
                + "    REGISTRADO,\n"
                + "    DI_FK_IDESTADOCONDONACION,\n"
                + "    DI_FK_COLABORADORORIGEN,\n"
                + "    DI_FK_COLABORADORDESTINO,\n"
                + "    DV_MODULOORIGEN,\n"
                + "    DV_MODULODESTINO,\n"
                + "    DV_CUNETAORIGEN,\n"
                + "    DV_CUENTADESTINO\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "	   -- ID_TRAKINESTADO - INT\n"
                + "    ?, -- DV_ESTADOINICIO - VARCHAR\n"
                + "    ?, -- DV_ESTADOFIN - VARCHAR\n"
                + "    GETDATE(),   -- REGISTRADO - DATETIME\n"
                + "    ?, -- DI_FK_IDESTADOCONDONACION - INT\n"
                + "    ?, -- DI_FK_COLABORADORORIGEN - INT\n"
                + "    ?, -- DI_FK_COLABORADORDESTINO - INT\n"
                + "    ?, -- DV_MODULOORIGEN - VARCHAR\n"
                + "    ?, -- DV_MODULODESTINO - VARCHAR\n"
                + "    ?, -- DV_CUNETAORIGEN - VARCHAR\n"
                + "    ? -- DV_CUENTADESTINO - VARCHAR\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID_TRAKINESTADO"});

                    ps.setString(1, tE.getDv_estadoInicio());
                    ps.setString(2, tE.getDv_estadoFin());
                    ps.setInt(3, tE.getDi_fk_idEstadoCondonacion());
                    ps.setInt(4, tE.getDi_fk_ColaboradorOrigen());
                    ps.setInt(5, tE.getDi_fK_ColaboradorDestino());
                    ps.setString(6, tE.getDv_ModuloOrigen());
                    ps.setString(7, tE.getDv_ModuloDestino());
                    ps.setString(8, tE.getDv_CunetaOrigen());
                    ps.setString(9, tE.getDv_CuentaDestino());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateTrackin_Estado(final Trackin_Estado tE) {
        String SQL = "UPDATE DBO.TRACKIN_ESTADO\n"
                + "  SET\n"
                + "    --ID_TRAKINESTADO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "      DV_ESTADOINICIO = ?, -- VARCHAR\n"
                + "      DV_ESTADOFIN =    ?, -- VARCHAR\n"
                + "    --REGISTRADO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "      DI_FK_IDESTADOCONDONACION = ?, -- INT\n"
                + "      DI_FK_COLABORADORORIGEN = ?, -- INT\n"
                + "      DI_FK_COLABORADORDESTINO = ?, -- INT\n"
                + "      DV_MODULOORIGEN =	   ?, -- VARCHAR\n"
                + "      DV_MODULODESTINO =	   ?, -- VARCHAR\n"
                + "      DV_CUNETAORIGEN =	   ?, -- VARCHAR\n"
                + "      DV_CUENTADESTINO =	   ? -- VARCHAR\n"
                + "WHERE ID_TRAKINESTADO = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tE.getDv_estadoInicio(),
                tE.getDv_estadoFin(),
                tE.getDi_fk_idEstadoCondonacion(),
                tE.getDi_fk_ColaboradorOrigen(),
                tE.getDi_fK_ColaboradorDestino(),
                tE.getDv_ModuloOrigen(),
                tE.getDv_ModuloDestino(),
                tE.getDv_CunetaOrigen(),
                tE.getDv_CuentaDestino(),
                tE.getId_trakinestado()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Trackin_Estado getTrackin_EstadoXId(final int id_trakinestado) {
        Trackin_Estado tE = new Trackin_Estado();

        final String sql = "SELECT TE.ID_TRAKINESTADO,\n"
                + "       TE.DV_ESTADOINICIO,\n"
                + "       TE.DV_ESTADOFIN,\n"
                + "       TE.REGISTRADO,\n"
                + "       TE.DI_FK_IDESTADOCONDONACION,\n"
                + "       TE.DI_FK_COLABORADORORIGEN,\n"
                + "       TE.DI_FK_COLABORADORDESTINO,\n"
                + "       TE.DV_MODULOORIGEN,\n"
                + "       TE.DV_MODULODESTINO,\n"
                + "       TE.DV_CUNETAORIGEN,\n"
                + "       TE.DV_CUENTADESTINO\n"
                + "FROM DBO.TRACKIN_ESTADO TE"
                + "WHERE ID_TRAKINESTADO = ?;";
        try {
            tE = jdbcTemplateObject.queryForObject(sql, new Object[]{id_trakinestado}, new Trackin_Estado());

        } catch (DataAccessException ex) {
            tE = null;
        }

        return tE;
    }

    public List<Trackin_Estado> getTrackin_EstadoXIdCondonacion(final int id_condonacion) {
        List<Trackin_Estado> lTE = new ArrayList<Trackin_Estado>();

        final String sql = "SELECT TE.ID_TRAKINESTADO,\n"
                + "       TE.DV_ESTADOINICIO,\n"
                + "       TE.DV_ESTADOFIN,\n"
                + "       TE.REGISTRADO,\n"
                + "       TE.DI_FK_IDESTADOCONDONACION,\n"
                + "       TE.DI_FK_COLABORADORORIGEN,\n"
                + "       TE.DI_FK_COLABORADORDESTINO,\n"
                + "       TE.DV_MODULOORIGEN,\n"
                + "       TE.DV_MODULODESTINO,\n"
                + "       TE.DV_CUNETAORIGEN,\n"
                + "       TE.DV_CUENTADESTINO\n"
                + "FROM DBO.TRACKIN_ESTADO TE\n"
                + "     INNER JOIN DBO.ESTADO_CONDONACION EC ON TE.DI_FK_IDESTADOCONDONACION = EC.DI_IDESTADOCONDONACION\n"
                + "WHERE EC.DI_FK_IDCONDONACION = ?\n"
                + "ORDER BY TE.REGISTRADO desc;";

        try {
            lTE = jdbcTemplateObject.query(sql, new Object[]{id_condonacion}, new Trackin_Estado());
        } catch (DataAccessException ex) {
            lTE = null;
        }

        return lTE;

    }

    public List<Trackin_Estado> listTrackin_Estados() {
        List<Trackin_Estado> lTE = new ArrayList<Trackin_Estado>();

        final String sql = "SELECT TE.ID_TRAKINESTADO,\n"
                + "       TE.DV_ESTADOINICIO,\n"
                + "       TE.DV_ESTADOFIN,\n"
                + "       TE.REGISTRADO,\n"
                + "       TE.DI_FK_IDESTADOCONDONACION,\n"
                + "       TE.DI_FK_COLABORADORORIGEN,\n"
                + "       TE.DI_FK_COLABORADORDESTINO,\n"
                + "       TE.DV_MODULOORIGEN,\n"
                + "       TE.DV_MODULODESTINO,\n"
                + "       TE.DV_CUNETAORIGEN,\n"
                + "       TE.DV_CUENTADESTINO\n"
                + "FROM DBO.TRACKIN_ESTADO TE;";
        try {
            lTE = jdbcTemplateObject.query(sql, new Trackin_Estado());
        } catch (DataAccessException ex) {
            lTE = null;
        }

        return lTE;
    }

    public boolean DeleteTrackin_Estado(final Trackin_Estado tE) {
        try {
            final String sql = "DELETE FROM dbo.trackin_estado\n"
                    + "WHERE ID_TRAKINESTADO = ?;";

            jdbcTemplateObject.update(sql, new Object[]{tE.getId_trakinestado()});

            return true;
        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception: " + e.toString());
            return false;
        }
    }

    public usuario ejecutivoDestinoXCond(int id_condonacion) {
        usuario user = new usuario();
        final String sql = "SELECT U.*,C.ID ID_CONDONACION,\n"
                + "       AT.COD AREATRAB \n"
                + "FROM DBO.USUARIO U\n"
                + "     INNER JOIN DBO.COLABORADOR C ON U.ID_USUARIO = C.ID_USUARIO\n"
                + "     INNER JOIN DBO.CONDONACION C2 ON C.ID = C2.DI_FK_IDCOLADORADOR\n"
                + "     INNER JOIN DBO.AREATRABAJO AT ON C.FK_DI_ID_AREATRABAJO = AT.ID\n"
                + "WHERE C2.ID_CONDONACION = ?;";

        try {
            user = jdbcTemplateObject.queryForObject(sql, new Object[]{id_condonacion}, new UsuarioMapper());
        } catch (DataAccessException ex) {
            user = null;
        }

        return user;

    }

    public usuario usuarioOrigen(String alias) {
        usuario user = new usuario();
        final String sql = "SELECT U.*,\n"
                + "       C.ID ID_CONDONACION,\n"
                + "	  AT.COD AREATRAB\n"
                + "FROM USUARIO U\n"
                + "     INNER JOIN DBO.COLABORADOR C ON U.ID_USUARIO = C.ID_USUARIO\n"
                + "	INNER JOIN DBO.AREATRABAJO AT ON C.FK_DI_ID_AREATRABAJO = AT.ID\n"
                + "WHERE U.ALIAS = ?;";

        try {
            user = jdbcTemplateObject.queryForObject(sql, new Object[]{alias}, new UsuarioMapper());
        } catch (DataAccessException ex) {
            user = null;
        }

        return user;

    }

    public boolean updateUsuarioAproTrackin_Estado(final Trackin_Estado tE) {
        String SQL = "UPDATE DBO.TRACKIN_ESTADO\n"
                + "  SET\n"
                + "      DI_FK_COLABORADORDESTINO = ?, -- INT\n"
                + "      DV_CUENTADESTINO =	   ? -- VARCHAR\n"
                + "WHERE ID_TRAKINESTADO = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tE.getDi_fK_ColaboradorDestino(),
                tE.getDv_CuentaDestino(),
                tE.getId_trakinestado()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public int insertTrackin_Estado_sinDestino(final Trackin_Estado tE) {
        KeyHolder key = new GeneratedKeyHolder();
        final String sql = "INSERT INTO DBO.TRACKIN_ESTADO\n"
                + "(\n"
                + "    --ID_TRAKINESTADO - THIS COLUMN VALUE IS AUTO-GENERATED\n"
                + "    DV_ESTADOINICIO,\n"
                + "    DV_ESTADOFIN,\n"
                + "    REGISTRADO,\n"
                + "    DI_FK_IDESTADOCONDONACION,\n"
                + "    DI_FK_COLABORADORORIGEN,\n"
                + "    DV_MODULOORIGEN,\n"
                + "    DV_MODULODESTINO,\n"
                + "    DV_CUNETAORIGEN\n"
                + ")\n"
                + "VALUES\n"
                + "(\n"
                + "	   -- ID_TRAKINESTADO - INT\n"
                + "    ?, -- DV_ESTADOINICIO - VARCHAR\n"
                + "    ?, -- DV_ESTADOFIN - VARCHAR\n"
                + "    GETDATE(), -- REGISTRADO - DATETIME\n"
                + "    ?, -- DI_FK_IDESTADOCONDONACION - INT\n"
                + "    ?, -- DI_FK_COLABORADORORIGEN - INT\n"
                + "    ?, -- DV_MODULOORIGEN - VARCHAR\n"
                + "    ?, -- DV_MODULODESTINO - VARCHAR\n"
                + "    ? -- DV_CUNETAORIGEN - VARCHAR\n"
                + ");";

        try {

            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(sql, new String[]{"ID_TRAKINESTADO"});

                    ps.setString(1, tE.getDv_estadoInicio());
                    ps.setString(2, tE.getDv_estadoFin());
                    ps.setInt(3, tE.getDi_fk_idEstadoCondonacion());
                    ps.setInt(4, tE.getDi_fk_ColaboradorOrigen());
                    ps.setString(5, tE.getDv_ModuloOrigen());
                    ps.setString(6, tE.getDv_ModuloDestino());
                    ps.setString(7, tE.getDv_CunetaOrigen());
                    return ps;
                }
            }, key);

            return key.getKey().intValue();

        } catch (DataAccessException ex) {
            return 0;
        }
    }

    public boolean updateTrackin_Estado_Ana_Zon_Recept(final Trackin_Estado tE) {
        String SQL = "UPDATE DBO.TRACKIN_ESTADO\n"
                + "  SET\n"
                + "      DI_FK_COLABORADORDESTINO = ?, -- INT\n"
                + "      DV_CUENTADESTINO =	   ? -- VARCHAR\n"
                + "WHERE ID_TRAKINESTADO = ?;";
        try {
            jdbcTemplateObject.update(SQL, new Object[]{
                tE.getDi_fK_ColaboradorDestino(),
                tE.getDv_CuentaDestino(),
                tE.getId_trakinestado()
            });
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }

    public Trackin_Estado getTrackin_EstadoXcond_Actual(final int id_condonacion) {
        Trackin_Estado tE = new Trackin_Estado();

        final String sql = "SELECT TE.ID_TRAKINESTADO,\n"
                + "       TE.DV_ESTADOINICIO,\n"
                + "       TE.DV_ESTADOFIN,\n"
                + "       TE.REGISTRADO,\n"
                + "       TE.DI_FK_IDESTADOCONDONACION,\n"
                + "       TE.DI_FK_COLABORADORORIGEN,\n"
                + "       TE.DI_FK_COLABORADORDESTINO,\n"
                + "       TE.DV_MODULOORIGEN,\n"
                + "       TE.DV_MODULODESTINO,\n"
                + "       TE.DV_CUNETAORIGEN,\n"
                + "       TE.DV_CUENTADESTINO\n"
                + "FROM DBO.TRACKIN_ESTADO TE\n"
                + "     INNER JOIN DBO.ESTADO_CONDONACION EC ON TE.DI_FK_IDESTADOCONDONACION = EC.DI_IDESTADOCONDONACION\n"
                + "     INNER JOIN DBO.CONDONACION C ON EC.DI_FK_IDCONDONACION = C.ID_CONDONACION\n"
                + "                                     AND EC.DI_FK_IDESTADO = C.ID_ESTADO\n"
                + "WHERE C.ID_CONDONACION = ?;";
        try {
            tE = jdbcTemplateObject.queryForObject(sql, new Object[]{id_condonacion}, new Trackin_Estado());

        } catch (DataAccessException ex) {
            tE = null;
        }

        return tE;
    }

    public Trackin_Estado getUltimoTrackin_EstadoXcond(final int id_condonacion) {
        Trackin_Estado tE = new Trackin_Estado();

        final String sql = "SELECT TOP (1) TE.*\n"
                + "FROM DBO.TRACKIN_ESTADO TE\n"
                + "     INNER JOIN DBO.ESTADO_CONDONACION EC ON TE.DI_FK_IDESTADOCONDONACION = EC.DI_IDESTADOCONDONACION\n"
                + "WHERE EC.DI_FK_IDCONDONACION = ?\n"
                + "ORDER BY TE.REGISTRADO DESC;";
        try {
            tE = jdbcTemplateObject.queryForObject(sql, new Object[]{id_condonacion}, new Trackin_Estado());

        } catch (DataAccessException ex) {
            tE = null;
        }

        return tE;
    }

}
