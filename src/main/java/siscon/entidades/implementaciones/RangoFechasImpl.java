/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import siscon.entidades.RangoFecha;
import siscon.entidades.interfaces.RangoFechasInterfaz;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class RangoFechasImpl implements RangoFechasInterfaz {

    private DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public RangoFechasImpl(DataSource ds) {
        this.jdbcTemplate = new JdbcTemplate(ds);
    }

    public int insertRangoFecha(final RangoFecha rangoFecha) {
        int id_rangof;

        final String SQL = "INSERT INTO [dbo].[rango_fecha]\n"
                + "           ([desc]\n"
                + "           ,[rango_ini]\n"
                + "           ,[rango_fin]\n"
                + "           ,[unidad])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {

            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(
                    new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id_rangof"});
                    // ps.setString(1, person.getName());
                    ps.setString(1, rangoFecha.getDesc());
                    ps.setInt(2, rangoFecha.getRango_ini());
                    ps.setInt(3, rangoFecha.getRango_fin());
                    ps.setString(4, rangoFecha.getUnidad());
                    return ps;
                }
            },
                    keyHolder);

            id_rangof = keyHolder.getKey().intValue();

        } catch (DataAccessException ex) {

            SisCorelog("ERR: [" + ex.getMessage() + "]");

            id_rangof = 0;

        }

        return id_rangof;
    }

    public boolean updateRangoFecha(final RangoFecha rangoFecha) {
        final String SQL = "UPDATE [dbo].[rango_fecha]\n"
                + "   SET [desc] = ?\n"
                + "      ,[rango_ini] = ?\n"
                + "      ,[rango_fin] = ?\n"
                + "      ,[unidad] = ?\n"
                + " WHERE id_rangof = ?";

        try {
            jdbcTemplate.update(SQL, new Object[]{
                rangoFecha.getDesc(),
                rangoFecha.getRango_ini(),
                rangoFecha.getRango_fin(),
                rangoFecha.getUnidad(),
                rangoFecha.getId_rangof()
            });
            return true;
        } catch (DataAccessException e) {
            SisCorelog("SQL Exception: " + e.toString());
            return false;
        }
    }

    public RangoFecha getRangoFechaXId(final int id_rangof) {
        RangoFecha rangoFecha = new RangoFecha();
        final String SQL = "SELECT * FROM rango_monto WHERE id_rangom = ?";

        try {
            rangoFecha = jdbcTemplate.queryForObject(SQL, new Object[]{id_rangof}, new RowMapper<RangoFecha>() {
                public RangoFecha mapRow(ResultSet rs, int rowNum) throws SQLException {
                    RangoFecha rangoFecha = new RangoFecha();

                    rangoFecha.setId_rangof(rs.getInt("id_rangof"));
                    rangoFecha.setDesc(rs.getString("desc"));
                    rangoFecha.setRango_ini(rs.getInt("rango_ini"));
                    rangoFecha.setRango_fin(rs.getInt("rango_fin"));
                    rangoFecha.setUnidad(rs.getString("unidad"));
                    return rangoFecha;
                }

            });

        } catch (DataAccessException ex) {
            SisCorelog("SQL Exception: " + ex.toString());
        }
        return rangoFecha;
    }

    public List<RangoFecha> getRangoFecha() {
        List<RangoFecha> lRangoFecha = new ArrayList<RangoFecha>();
        String SQL = "SELECT * FROM rango_fecha";

        try {
            lRangoFecha = jdbcTemplate.query(SQL, new RowMapper<RangoFecha>() {
                public RangoFecha mapRow(ResultSet rs, int rowNum) throws SQLException {
                    RangoFecha rangoFecha = new RangoFecha();

                    rangoFecha.setId_rangof(rs.getInt("id_rangof"));
                    rangoFecha.setDesc(rs.getString("desc"));
                    rangoFecha.setRango_ini(rs.getInt("rango_ini"));
                    rangoFecha.setRango_fin(rs.getInt("rango_fin"));
                    rangoFecha.setUnidad(rs.getString("unidad"));
                    return rangoFecha;
                }
            });

        } catch (DataAccessException ex) {
            SisCorelog("ERR: [" + ex.getMessage() + "]");
        }
        return lRangoFecha;
    }

}
