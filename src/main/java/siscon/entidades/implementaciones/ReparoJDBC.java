/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author esilves
 */
public class ReparoJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public ReparoJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public boolean guardarReparo(final int idcondonacion, final String Motivo, final String Glosa, final String cuenta) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO reparo\n"
                + "           (di_fk_IdCondonacion\n"
                + "           ,dv_MotivoReparo\n"
                + "           ,dv_GlosaReparo"
                + "           ,di_fk_IdColaborador"
                + "           ,registrado"
                + " )\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,(select cola.id from colaborador cola  inner join usuario usu  on usu.id_usuario=cola.id_usuario   where usu.alias=?) \n"
                + "           ,getdate())\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"di_idReparo"});
                    ps.setInt(1, idcondonacion);
                    ps.setString(2, Motivo);
                    ps.setString(3, Glosa);
                    ps.setString(4, cuenta);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Reparo : 61[" + e.toString() + "]");
            ok = false;
        }
        return ok;

    }

}
