/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.ColaboradorJefeMapper;
import siscon.RowMapper.ColaboradorMapper;
import siscon.RowMapper.UsrMontoAtribucionMapper;
import siscon.entidades.Colaborador;
import siscon.entidades.ColaboradorJefe;
import siscon.entidades.UsrMontoAtribucion;

/**
 *
 * @author esilves
 */
public class ColaboradorJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public ColaboradorJDBC() {
        this.dataSource=null;
    }

    public ColaboradorJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public Boolean ExisteCliente(String rut) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select count(*) from cliente clie where clie.rut= '" + rut + "'";

        _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);

        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

    public String GetRutCliente(int idcondonacion) {
        String jjj = "14212287-1";
        int _numDocumentos = 0;
        List<Map<String, Object>> ff;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select rut from condonacion cond  inner join cliente clie on clie.id_cliente=cond.di_fk_IdCliente where cond.id_condonacion= " + idcondonacion;
        try {
            ff = jdbcTemplate.queryForList(SQLtramsito);
            // jdbcTemplate.query
        } catch (DataAccessException e) {

            Messagebox.show("Error" + e.getMessage());
            return "Error";
        }
        jjj = ff.get(0).values().toString();
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return jjj;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

    public String GetRutEjecutivo(int idcondonacion) {
        String jjj = "14212287-1";
        int _numDocumentos = 0;
        List<Map<String, Object>> ff;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select top 1 nombres from condonacion cond  inner join colaborador cola on cola.id=cond.di_fk_idColadorador  where cond.id_condonacion= " + idcondonacion;
        try {
            ff = jdbcTemplate.queryForList(SQLtramsito);
            // jdbcTemplate.query
        } catch (DataAccessException e) {

            Messagebox.show("Error" + e.getMessage());
            return "Error";
        }
        jjj = ff.get(0).values().toString();
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return jjj;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

    
    
    public List<UsrMontoAtribucion>   getMontoAtribucionCondonacionRut(int rut)
    {
        
        List<UsrMontoAtribucion> dsol = null;
               jdbcTemplate = new JdbcTemplate(dataSource);
        String SQL = "select usu.alias,atr.glosa_atribucion,colat.monto_minimo,colat.monto_maximo from usuario usu \n"
                + "           inner join colaborador cola on cola.id_usuario=usu.id_usuario\n"
                + "		   inner join colaborador_tiene_atribucion colat on colat.id_colaborador=cola.id\n"
                + "		   inner join atribucion_cobranza atr on atr.id=colat.id_atribucion\n"
                + " \n"
                + "		   where usu.alias in ('admin','ejesiscon','mhazbun')";
               try {

            dsol = jdbcTemplate.query(SQL, new UsrMontoAtribucionMapper());
        } catch (DataAccessException e) 
		{
            Messagebox.show("SQL Exception con ColaboradorJDBC 111: " + e.toString());
            return dsol;
        }
        return dsol;
    
    
    
    }
           
public Colaborador   buscarColaboradorRutDi(int rut)
    {
                      jdbcTemplate = new JdbcTemplate(dataSource);
        Colaborador _colabo = null;
        List<Colaborador> _colaboList=null;
 
        String SQLCOLABORADOR = " select cola.* from colaborador cola \n"
                //  + "inner join colaborador_tiene_atribucion colatr on colatr.id_colaborador=cola.id \n"
                //  + "inner join atribucion_cobranza atrco on atrco.id=colatr.id_atribucion \n"
                + "inner join usuario usu on usu.id_usuario=cola.id_usuario \n"
                + "where usu.di_rut=" + rut +" \n";
        
               try {

            _colaboList = jdbcTemplate.query(SQLCOLABORADOR, new ColaboradorMapper());
        } catch (DataAccessException e) 
		{
            Messagebox.show("SQL Exception con ColaboradorJDBC 111: " + e.toString());
            return _colabo;
        }
                _colabo = _colaboList.get(0);
        return _colabo;
    
    
    
    }

    public String getColaborador(int id_colaborador) {

        List<String> listAEnc = new ArrayList<String>();
        String aEnc = new String();
        try {
            String SQL = "SELECT u.alias FROM dbo.colaborador c \n"
                    + "INNER JOIN dbo.usuario u ON c.id_usuario = u.id_usuario\n"
                    + "WHERE c.id = ?";
            aEnc = jdbcTemplate.queryForObject(SQL, new Object[]{id_colaborador}, new RowMapper<String>() {
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    String sqlEnc = new String();

                    sqlEnc = rs.getString("alias");

                    return sqlEnc;
                }
            });

//            aEnc = listAEnc.get(0);
        } catch (DataAccessException e) {
         //   Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos\nError: " + e.toString());
            aEnc = null;
        } catch (Exception e) {
         //   Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos\nError: " + e.toString());
            aEnc = null;
        }
        return aEnc;

    }





    public ColaboradorJefe GetIdJefe(String cuenta) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        int _UltimoReparoEjecutivo = 0;
        List<ColaboradorJefe> _result=new ArrayList<ColaboradorJefe>();
        // String SQL = " select * from reparo rep where rep.di_fk_IdCondonacion ='" + id_condonacion + "' \n";
        final String SQL = "select sacaIDJefe.fk_idFeje'MyIdJefe',coll.*,uss.* from (\n"
                + "select   col.fk_idFeje from usuario us\n"
                + "      inner join colaborador col on col.id_usuario=us.id_usuario\n"
                + "	  inner join perfil_usuario pe on pe.id_usuario=us.id_usuario\n"
                + "	  inner join perfil p on p.id_perfil=pe.id_perfil\n"
                + "where    us.alias='"+cuenta+"' and col.fk_idFeje is not null )\n"
                + " as sacaIDJefe\n"
                + " inner join colaborador coll on coll.id=sacaIDJefe.fk_idFeje\n"
                + " inner join usuario uss on uss.id_usuario=coll.id_usuario";
             // Messagebox.show("QueryJefe:"+SQL);
        try {
            //   dsol = jdbcTemplate.query(SQL, new ReparosMapper());

            _result = jdbcTemplate.query(SQL,new ColaboradorJefeMapper());

        } catch (DataAccessException e) {
            Messagebox.show("SQL Exception con IMPL 455: " + e.toString());
            ColaboradorJefe tt=new ColaboradorJefe();
            return tt;
        }
        
        if(_result.size()>0)
        {
            _result.get(0);
        }
        else {
         ColaboradorJefe tt=new ColaboradorJefe();
            return tt;
        
        
        }
        
        return _result.get(0);
    }
    
    
    
}
