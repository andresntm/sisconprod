/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.SbifCliente;
import siscon.entidades.ValorMonto;
import siscon.entidades.interfaces.SbifClienteInterfaz;

/**
 *
 * @author esilves
 */
public class SbifClienteJDBC implements SbifClienteInterfaz {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private List<SbifCliente> gclie = new ArrayList<SbifCliente>();
    public ValorMonto SumaMontoTotalGarantias;

    public SbifClienteJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public List<SbifCliente> SbifCliente(int rut,String usuario) {
        //  List<GarantiasCliente> gclie = null;

        Map<String, Object> out;
        SqlParameterSource in;
        // DetalleOperacionesCliente.set(0, element)

        //List<Contacto> listContact
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName("sp_web_syscon_sbif").
                returningResultSet("rs", new ParameterizedRowMapper<SbifCliente>() {

                    public SbifCliente mapRow(ResultSet rs, int i) throws SQLException {

                        SbifCliente sclie = new SbifCliente();
                        sclie.setFld_per(rs.getString("fld_per"));
                        sclie.setFld_lin(rs.getString("fld_lin"));
                        sclie.setFld_num_ins(rs.getString("fld_num_ins"));
                        sclie.setFld_dir_tot(rs.getString("fld_dir_tot"));
                        sclie.setFld_deu_con(rs.getString("fld_deu_con"));
                        sclie.setFld_deu_hip(rs.getString("fld_deu_hip"));
                        sclie.setFld_deu_com(rs.getString("fld_deu_com"));
                        sclie.setFld_deu_ind(rs.getString("fld_deu_ind"));
                        sclie.setFld_dir_mor(rs.getString("fld_dir_mor"));
                        sclie.setFld_dir_vda(rs.getString("fld_dir_vda"));
                        sclie.setFld_dir_cas(rs.getString("fld_dir_cas"));
                        sclie.setFld_ind_vda(rs.getString("fld_ind_vda"));
                        sclie.setFld_ind_cas(rs.getString("fld_ind_cas"));

                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

                        return sclie;//rs.getString(1);   

                    }
                ;
        });
            String usu = "jliguen";
        in = new MapSqlParameterSource().addValue("fld_rut", rut).addValue("usuario", usuario);
        try {
            out = jdbcCall.execute(in);

            gclie = (List<SbifCliente>) out.get("rs");

        } catch (DataAccessException ex) {

            Messagebox.show("ERROR SP CALL :[" + ex.getMessage() + "]");
            return gclie;
        }

        //  System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + user + "]----#######");
        return gclie;

    }

    public String SumaTotalGarantiasPesos() {
        float sumaTotal = 0;

        for (final SbifCliente Gtias : gclie) {
            //  list2.add(tipodocto.getDv_NomTipDcto());
            sumaTotal = sumaTotal + Float.parseFloat(Gtias.getFld_dir_tot());

        }

        this.SumaMontoTotalGarantias = new ValorMonto(sumaTotal);

        return this.SumaMontoTotalGarantias.getValorPesos();
    }

}
