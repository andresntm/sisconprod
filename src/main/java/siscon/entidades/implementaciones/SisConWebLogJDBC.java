/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author esilves
 */
public class SisConWebLogJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    String httpsession;
    int idsessionsiscon;
    HttpSession hses;
    Session sess;
    HttpSessionJDBC _httpsession;

    public SisConWebLogJDBC(DataSource dataSource) throws SQLException {
        this.dataSource = dataSource;

        sess = Sessions.getCurrent();
        hses = (HttpSession) sess.getNativeSession();
        _httpsession = new HttpSessionJDBC(this.dataSource);
        httpsession = hses.getId();
        idsessionsiscon = _httpsession.GetSisConIdSession(httpsession);

    }

    public boolean guardarReparo(final int idcondonacion, final String Motivo, final String Glosa, final String cuenta) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO reparo\n"
                + "           (di_fk_IdCondonacion\n"
                + "           ,dv_MotivoReparo\n"
                + "           ,dv_GlosaReparo"
                + "           ,di_fk_IdColaborador"
                + "           ,registrado"
                + " )\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,(select cola.id from colaborador cola  inner join usuario usu  on usu.id_usuario=cola.id_usuario   where usu.alias=?) \n"
                + "           ,getdate())\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"di_idReparo"});
                    ps.setInt(1, idcondonacion);
                    ps.setString(2, Motivo);
                    ps.setString(3, Glosa);
                    ps.setString(4, cuenta);
                    return ps;
                }
            }, key);
            ok = true;

        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Reparo : 61[" + e.toString() + "]");
            ok = false;
        }
        return ok;

    }

    public boolean _logConsulta(final int idcondonacion, final String cuenta, final String cliente, final String Modulo, final String ventana_zul, final String ruta_zul, final String ControllerSpring, final String NombreBoton, final String desc_cambio) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO [dbo].[tracking_web_consulta]\n"
                + "           ([cuenta]\n" //1
                + "           ,[id_condonacion]\n" //2
                + "           ,[cliente]\n" //3
                + "           ,[registrado]\n"
                + "           ,[modulo]\n" //4
                + "           ,[ventana_zul]\n" //5
                + "           ,[ruta_zul]\n" //6
                + "           ,[controlador_spring]\n" //7
                + "           ,[id_boton]\n" //8
                + "           ,[consulta_sql]\n" //9
                + "           ,[id_session]\n" //10
                + "           ,[nombre_boton]"
                + "           ,[desc_cambio]"
                + ")\n" //11
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,getdate() \n"
                + "           ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? )\n";

        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});
                    ps.setString(1, cuenta);
                    ps.setInt(2, idcondonacion);
                    ps.setString(3, cliente);
                    ps.setString(4, Modulo);
                    ps.setString(5, ventana_zul);
                    ps.setString(6, ruta_zul);
                    ps.setString(7, ControllerSpring);
                    ps.setInt(8, 1);
                    ps.setString(9, "consulta");
                    ps.setInt(10, 1);
                    ps.setString(11, NombreBoton);
                    ps.setString(12, desc_cambio);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando _logConsulta : 61[" + e.toString() + "]");
            ok = false;
        }
        return ok;

    }

    public boolean _logEdita(final int idcondonacion, final String cuenta, final String cliente, final String Modulo, final String ventana_zul, final String ruta_zul, final String ControllerSpring, final String NombreBoton, final String desc_cambio) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO [dbo].[traking_web_edicion]\n"
                + "           ([id_session]\n" //1
                + "           ,[id_condonacion]\n" //2
                + "           ,[cliente]\n" //3
                + "           ,[registrado]\n"
                + "           ,[modulo]\n" //4
                + "           ,[ventana_zul]\n" //5
                + "           ,[ruta_zul]\n" //6
                + "           ,[controlador_spring]\n" //7
                + "           ,[nombre_boton]"
                + "           ,[desc_cambio]"
                + ")\n" //11
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,getdate() \n"
                + "           ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? \n"
                + "            ,? )\n";

        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});
                    ps.setInt(1, idsessionsiscon);
                    ps.setInt(2, idcondonacion);
                    ps.setString(3, cliente);
                    ps.setString(4, Modulo);
                    ps.setString(5, ventana_zul);
                    ps.setString(6, ruta_zul);
                    ps.setString(7, ControllerSpring);
                    ps.setString(8, NombreBoton);
                    ps.setString(9, desc_cambio);
                    return ps;
                }
            }, key);
            ok = true;
            _httpsession.setSessionAccion("Edita Porcentaje Condonacion", idsessionsiscon, 2);
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando _logConsulta : 61[" + e.toString() + "]");
            _httpsession.setSessionAccion("Error Edita Porcentaje Condonacion linea 198 SisConWebLogJDBC", idsessionsiscon, 2);
            ok = false;
        }
        return ok;

    }
}
