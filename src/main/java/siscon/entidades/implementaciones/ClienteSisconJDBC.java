/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author esilves
 */
public class ClienteSisconJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public ClienteSisconJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public Boolean ExisteCliente(String rut) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select count(*) from cliente clie where clie.rut= '" + rut + "'";

        _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);

        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }
    public Boolean ExisteCliente(int rut) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select count(*) from cliente clie inner join traking_morahoy_clie trk on trk.fld_rut_ent=clie.rut where trk.fld_rut='" + rut + "'";
         
        
       // String SQLtramsito2 = "select clie.rut from cliente clie inner join traking_morahoy_clie trk on trk.fld_rut_ent=clie.rut where convert(int,SUBSTRING(clie.rut, 1, (CHARINDEX('-', clie.rut)-1)))='" + rut + "'";
        
        try{
        _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);
        }catch(DataAccessException ex){
        
        Messagebox.show("ERROR ClienteSisConJDBC 55 :["+ex.getMessage()+"] este el SQL ["+SQLtramsito+"]");
        }
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }
    public String GetRutCliente(int idcondonacion) {
        String jjj = "14212287-1";
        int _numDocumentos = 0;
        List<Map<String, Object>> ff;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select rut from condonacion cond  inner join cliente clie on clie.id_cliente=cond.di_fk_IdCliente where cond.id_condonacion= " + idcondonacion;
        try {
            ff = jdbcTemplate.queryForList(SQLtramsito);
            // jdbcTemplate.query
        } catch (DataAccessException e) {

            Messagebox.show("Error" + e.getMessage());
            return "Error";
        }
        jjj = ff.get(0).values().toString();
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return jjj;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

    public String GetRutEjecutivo(int idcondonacion) {
        String jjj = "14212287-1";
        int _numDocumentos = 0;
        List<Map<String, Object>> ff;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select top 1 nombres from condonacion cond  inner join colaborador cola on cola.id=cond.di_fk_idColadorador  where cond.id_condonacion= " + idcondonacion;
        try {
            ff = jdbcTemplate.queryForList(SQLtramsito);
            // jdbcTemplate.query
        } catch (DataAccessException e) {

            Messagebox.show("Error" + e.getMessage());
            return "Error";
        }
        jjj = ff.get(0).values().toString();
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return jjj;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

}
