/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author esilves
 */
public class HttpSessionJDBC {

    private final DataSource dataSource;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;

    public HttpSessionJDBC(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    public int setSession(final String cuenta, final String id_session, final String ip, final String zksessionid, final int sistema) {
        boolean ok;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO http_session \n"
                + "           (id_http_session\n"
                + "           ,registrado\n"
                + "           ,cuenta\n"
                + "           ,ip\n"
                + "           ,zk_jsessionid\n"
                + "           ,id_sistema)\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,getdate() \n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)\n";

        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});

                    ps.setString(1, id_session);
                    ps.setString(2, cuenta);
                    ps.setString(3, ip);
                    ps.setString(4, zksessionid);
                    ps.setInt(5, sistema);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Tra-Estado : 62[" + e.toString() + "]");
            ok = false;
        }

        //returnTraking = keyHolder.getKey().intValue();
        return key.getKey().intValue();

    }

    public boolean setSessionAccion(final String accion, final int id_session, final int tipo_accion) {
        boolean ok = false;
        jdbcTemplate = new JdbcTemplate(dataSource);
        final String SQL = "INSERT INTO http_session_acciones \n"
                + "           (http_sessionID\n"
                + "           ,desc_accion\n"
                + "           ,id_tipoAccion\n"
                + "           ,registrado)\n"
                + "     VALUES \n"
                + "           (? \n"
                + "           ,? \n"
                + "           ,? \n"
                + "           ,getdate())\n";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});

                    ps.setInt(1, id_session);
                    ps.setString(2, accion);
                    ps.setInt(3, tipo_accion);
                    return ps;
                }
            }, key);
            ok = true;
        } catch (DataAccessException e) {
            Messagebox.show("Error Guardando Tra-Estado : 62[" + e.toString() + "]");
            ok = false;
        }
        return ok;

    }

    public int GetSisConIdSession(String httpsession) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        // String sql = "select count(id_cliente) from cliente where rut='" + rut + "'";
        int idsession = 0;
        String sql = "SELECT TOP 1 id  FROM http_session  where id_http_session ='" + httpsession + "' ";

        try {
            idsession = jdbcTemplate.queryForInt(sql);
        } catch (DataAccessException ex) {

            //   SisCorelog("QUery :[" + sql + "]" + ex.getMessage());
        }

        // SisCorelog("QUery :[" + sql + "]");
        return idsession;

    }

}
