/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import siscon.entidades.InformeCond_Ap_Re_Pe;
import siscon.entidades.interfaces.InformeCond_Ap_Re_PeInterface;

/**
 *
 * @author excosoc
 */
public class InformeCond_Ap_Re_PeImplements implements InformeCond_Ap_Re_PeInterface {

    private JdbcTemplate jdbcTemplateObject;
    private JdbcTemplate jdbcTemplateObject_prod;
    private DataSource dataSource;
    private DataSource dataSource_prod;
    private MvcConfig conex;

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }

    public InformeCond_Ap_Re_PeImplements() {
        try {
            conex = new MvcConfig();
            this.dataSource = conex.getDataSource();
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }

    }

    public InformeCond_Ap_Re_PeImplements(DataSource dS) {

        try {
            this.dataSource = dS;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);

            conex = new MvcConfig();
            this.dataSource_prod = conex.getDataSourceProduccion();
            jdbcTemplateObject_prod = new JdbcTemplate(dataSource_prod);
        } catch (SQLException SQLex) {
            //
        }
    }

    public List<InformeCond_Ap_Re_Pe> ListInformeCond_Ap_Re_Pe_Pyme() {
        List<InformeCond_Ap_Re_Pe> lInforme = new ArrayList<InformeCond_Ap_Re_Pe>();

        final String sql = "SELECT u.alias alias_usuario,\n"
                + "       u.dv_rut dv_rut,\n"
                + "       col.nombres + ' ' + col.dv_apellidos nombres,\n"
                + "	  U_JEFE.DV_RUT rut_jefe,\n"
                + "       col_jefe.nombres  + ' '+ col_jefe.dv_apellidos nombre_jefe,\n"
                + "       IIF(MAX(c.timestap) IS NULL, 'Sin fecha', CONVERT(VARCHAR, MAX(c.timestap), 120)) fecha_ultima_cond,\n"
                + "       IIF(apro.apro IS NULL\n"
                + "           OR apro.apro = 0, 0, apro.apro) Apro_cond,\n"
                + "       IIF(pend.pend IS NULL\n"
                + "           OR pend.pend = 0, 0, pend.pend) Pend_cond,\n"
                + "       IIF(rech.rech IS NULL\n"
                + "           OR rech.rech = 0, 0, rech.rech) Rech_cond,\n"
                + "		 COUNT(c.id_condonacion) numero_cond\n"
                + "FROM COLABORADOR col\n"
                + "     INNER JOIN dbo.usuario u ON col.id_usuario = u.id_usuario\n"
                + "     LEFT JOIN dbo.condonacion c ON col.id = c.di_fk_idColadorador\n"
                + "     LEFT JOIN colaborador col_jefe ON col.FK_IDFEJE = col_jefe.id\n"
                + "	INNER JOIN USUARIO U_JEFE ON COL_JEFE.id_usuario = U_JEFE.id_usuario\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) apro,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(10, 21, 22, 23) --(10)<- Aprob Analista / (21)<-Aprobada Zonal / (22)<-Cerrada Eje Pyme / (23)<-Cerrada Zo Pyme\n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS apro ON col.id = apro.di_fk_idColadorador\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) pend,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(17, 19)--(17)<-Pendiente Zonal / (20)<-Reparada Zo Pyme\n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS pend ON col.id = pend.di_fk_idColadorador\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) rech,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(20)--(20)<-Reparada Zo Pyme \n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS rech ON col.id = rech.di_fk_idColadorador\n"
                + "WHERE col.FK_IDFEJE IS NOT NULL\n"
                + "GROUP BY u.alias,\n"
                + "         u.dv_rut,\n"
                + "         col.nombres,\n"
                + "         col.dv_apellidos,\n"
                + "         col.fk_idFeje,\n"
                + "         col_jefe.nombres,\n"
                + "         col_jefe.dv_apellidos,\n"
                + "         apro.apro,\n"
                + "         pend.pend,\n"
                + "         rech.rech,\n"
                + "	    U_JEFE.DV_RUT;";
        try {
            lInforme = jdbcTemplateObject.query(sql, new InformeCond_Ap_Re_Pe());
        } catch (DataAccessException ex) {
            lInforme = null;
        }

        return lInforme;
    }

    public List<InformeCond_Ap_Re_Pe> ListInformeCond_Ap_Re_Pe_Retail() {
        List<InformeCond_Ap_Re_Pe> lInforme = new ArrayList<InformeCond_Ap_Re_Pe>();

        final String sql = "SELECT u.alias alias_usuario,\n"
                + "       u.dv_rut dv_rut,\n"
                + "       col.nombres+' '+col.dv_apellidos nombres,\n"
                + "	  '' rut_jefe,\n"
                + "       '' nombre_jefe,\n"
                + "       IIF(MAX(c.timestap) IS NULL, 'Sin fecha', CONVERT(VARCHAR, MAX(c.timestap), 120)) fecha_ultima_cond,\n"
                + "       IIF(apro.apro IS NULL\n"
                + "           OR apro.apro = 0, 0, apro.apro) Apro_cond,\n"
                + "       IIF(pend.pend IS NULL\n"
                + "           OR pend.pend = 0, 0, pend.pend) Pend_cond,\n"
                + "       IIF(rech.rech IS NULL\n"
                + "           OR rech.rech = 0, 0, rech.rech) Rech_cond,\n"
                + "       COUNT(c.id_condonacion) numero_cond\n"
                + "FROM COLABORADOR col\n"
                + "     INNER JOIN dbo.usuario u ON col.id_usuario = u.id_usuario\n"
                + "     LEFT JOIN dbo.condonacion c ON col.id = c.di_fk_idColadorador\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) apro,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(10,6,13) --(10)<- Aprob Analista \n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS apro ON col.id = apro.di_fk_idColadorador\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) pend,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(3,9,15)--\n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS pend ON col.id = pend.di_fk_idColadorador\n"
                + "     LEFT JOIN\n"
                + "(\n"
                + "    SELECT COUNT(id_condonacion) rech,\n"
                + "           di_fk_idColadorador\n"
                + "    FROM condonacion\n"
                + "    WHERE id_estado IN(11)--(20)<-Reparada Zo Pyme \n"
                + "    GROUP BY di_fk_idColadorador\n"
                + ") AS rech ON col.id = rech.di_fk_idColadorador\n"
                + "GROUP BY u.alias,\n"
                + "         u.dv_rut,\n"
                + "         col.nombres,\n"
                + "         col.dv_apellidos,\n"
                + "         apro.apro,\n"
                + "         pend.pend,\n"
                + "         rech.rech;";
        try {
            lInforme = jdbcTemplateObject_prod.query(sql, new InformeCond_Ap_Re_Pe());
        } catch (DataAccessException ex) {
            lInforme = null;
        }

        return lInforme;
    }

}
