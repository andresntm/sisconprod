/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import configuracion.SisBojConf;
import java.io.Serializable;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import siscon.RowMapper.AreaTrabajoMapper;
import siscon.RowMapper.BancasPerfillMapper;
import siscon.RowMapper.PerfilMapper;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Banca;
import siscon.entidades.Perfil;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.interfaces.UsuarioInterfaceDAO;
import siscon.entidades.usuario;
import siscore.RowMapper.SistemaMapper;
import siscore.entidades.Sistema;
import siscore.entidades.implementaciones.LogJDBC;

/**
 *
 * @author exesilr
 */
public class UsuarioPermisoJDBC implements UsuarioInterfaceDAO, Serializable {

    private static final long serialVersionUID = 1L;

    private JdbcTemplate jdbcTemplateObject;
    private DataSource dataSource;
    private final SisBojConf _bojConfig;
    private UsuarioJDBC userJDBC;
    private LogJDBC logJDBC;
    private UsuarioPermiso permisoUsuario = new UsuarioPermiso();

    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        userJDBC = new UsuarioJDBC();
        userJDBC.setDataSource(ds);
        logJDBC = new LogJDBC(ds);
    }

    public UsuarioPermisoJDBC() {
        _bojConfig = new SisBojConf();

    }

    public UsuarioPermiso getUsuarioPermisos() {
        Session sess = Sessions.getCurrent();
        UsuarioPermiso permiso = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        if (permiso == null) {
            permiso = new UsuarioPermiso();//new a anonymous user and set to session
            sess.setAttribute("UsuarioPermisos", permiso);
        }
        return permiso;
    }

    public UsuarioPermiso getUsuarioPermisos(HttpSession sessionx) {
        //Session sess = Sessions.getCurrent();
        UsuarioPermiso permiso = (UsuarioPermiso) sessionx.getAttribute("UsuarioPermisos");
        if (permiso == null) {
            permiso = new UsuarioPermiso();//new a anonymous user and set to session
            sessionx.setAttribute("UsuarioPermisos", permiso);
        }
        return permiso;
    }

    public boolean login(String nm, String pd,String sistema) {

        //  Verificacion existencia del usuario en el sistema
        if (!userJDBC.IsExisteUsuario(nm)) {
             logJDBC.guardaAcceso("no existe usuario",nm,pd,"error en ellogin sin httpsex  usr:["+nm+"]pwd["+pd+"]");
            return false;
        }
        logJDBC.guardaAcceso("Existe Usuario",nm,pd,"login OK:["+nm+"]pwd["+pd+"]");
        usuario user = userJDBC.buscarUsuario(nm);
        
        //Validacion del password sin encriptar    
        if(!"sisboj".equals(sistema)){
            if (user == null || !user.getPassword().equals(pd)) {
                logJDBC.guardaAcceso("Error Password",nm,pd,"login OK:["+nm+"]pwd["+pd+"]");
                return false;
            }
        }
        
        //ahora obtenemos los permisos del usuarios 
         logJDBC.guardaAcceso("Password OK",nm,pd,"login OK:["+nm+"]pwd["+pd+"]");
        Session sess = Sessions.getCurrent();

        this.permisoUsuario = new UsuarioPermiso(user.getAccount(), user.getFullName(), user.getDi_rut());
        this.permisoUsuario.setId_colaborador(user.getId_colaborador());
        this.permisoUsuario.setAreaTrab(user.getAreaTrabajo());
        // aca crearemos el objeto con informacion de los perfiles y cockies
        getPermisoPerfilUsuario();
        getBancasXperfil();
        getUsuarioSistema();
        // UsuarioPermiso permiso = new UsuarioPermiso(user.getAccount(), user.getFullName(), user.getDi_rut());
        //just in case for this demo.
        if (this.permisoUsuario.isInvitado()) {
            return false;
        }
        sess.setAttribute("UsuarioPermisos", this.permisoUsuario);

        //TODO handle the role here for authorization
        return true;
    }

    public boolean login(String nm, String pd, HttpSession sessionx) {

        //  Verificacion existencia del usuario en el sistema
        if (!userJDBC.IsExisteUsuario(nm)) {
            
            logJDBC.guardaAcceso("Acceso Fallido",nm,pd,"error en ellogin sin httpsex  usuario:["+nm+"]["+pd+"]");
            
            return false;
        }

        usuario user = userJDBC.buscarUsuario(nm);

        //Validacion del password sin encriptar
        if (user == null || !user.getPassword().equals(pd)) {
            return false;
        }

        //ahora obtenemos los permisos del usuarios 
        //     sessionx.setAttribute(nm, user);
        Session sess = Sessions.getCurrent();
        Session sess2 = Sessions.getCurrent(true);

        this.permisoUsuario = new UsuarioPermiso(user.getAccount(), user.getFullName(), user.getDi_rut());

        // aca crearemos el objeto con informacion de los perfiles y cockies
        getPermisoPerfilUsuario();
        getBancasXperfil();
        getUsuarioSistema();
        // UsuarioPermiso permiso = new UsuarioPermiso(user.getAccount(), user.getFullName(), user.getDi_rut());
        //just in case for this demo.
        if (this.permisoUsuario.isInvitado()) {
            return false;
        }
        sessionx.setAttribute("UsuarioPermisos", this.permisoUsuario);
        // sess.setAttribute("UsuarioPermisos", this.permisoUsuario);

        //TODO handle the role here for authorization
        return true;
    }

    @Override
    public void logout() {
        Session sess = Sessions.getCurrent();
        sess.removeAttribute("UsuarioPermisos");
    }

    private void getPermisoPerfilUsuario() {

        // validacion 1 existe el registro en la DB
        String SQL = "select pe.* from usuario usr  \n"
                + "inner join perfil_usuario peusr on (peusr.id_usuario=usr.id_usuario) \n"
                + "left join perfil pe on pe.id_perfil=peusr.id_perfil \n"
                + "where usr.alias='" + this.permisoUsuario.getCuenta() + "' ";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        List<Perfil> Perfil = jdbcTemplateObject.query(SQL, new PerfilMapper());
        for (final Perfil perfil : Perfil) {
            this.permisoUsuario.agregarPerfil(perfil);
        }

        // validacion 1 existe el registro en la DB
        String SQL2 = "select cola.id,at.cod,at.descripcion,fi.descripcion as filial from usuario usr \n"
                + "inner join colaborador cola on (cola.id_usuario=usr.id_usuario) \n"
                + "inner join AreaTrabajo at on (at.id=cola.fk_di_id_AreaTrabajo) \n"
                + "inner join filial fi  on fi.id=at.fk_di_id_filial \n"
                + "where usr.alias='" + this.permisoUsuario.getCuenta() + "' ";

        List<AreaTrabajo> area = jdbcTemplateObject.query(SQL2, new AreaTrabajoMapper());
        this.permisoUsuario.setArea(area.get(0));

    }

    private void getUsuarioSistema() {

        // validacion 1 existe el registro en la DB
        String SQL = "select sis.* from usuario usr  \n"
                + "                inner join usuario_sistema ussis on (usr.id_usuario=ussis.fk_idUsuario) \n"
                + "                left join sistema sis on ussis.fk_idSistema=sis.id \n"
                + "                where usr.alias='" + this.permisoUsuario.getCuenta() + "' ";

        _bojConfig.print("Sistemas del Usuario UsuarioPermisoJDBClinea 184[" + SQL + "]");
//        Messagebox.show("SisTemaUsuario[" + SQL + "]");
        List<Sistema> _sistema = jdbcTemplateObject.query(SQL, new SistemaMapper());
        for (final Sistema _sis : _sistema) {
            this.permisoUsuario.agregarSistema(_sis);
        }

    }

    private void getBancasXperfil() {

        // validacion 1 existe el registro en la DB
        String SQL = "select distinct ba.id,ba.cod_banca,ba.descripcion from usuario us\n"
                + "      inner join colaborador col on col.id_usuario=us.id_usuario\n"
                + "	  inner join perfil_usuario pe on pe.id_usuario=us.id_usuario\n"
                + "	  inner join perfil p on p.id_perfil=pe.id_perfil\n"
                + "	  inner join perfil_bojBanca peo on peo.fk_id_perfil=p.id_perfil\n"
                + "	  inner join banca ba on ba.id=peo.fkIdBanca\n"
                + "	  where us.alias='" + this.permisoUsuario.getCuenta() + "' ";

        _bojConfig.print("DocJDBC linea 96[" + SQL + "]");
        List<Banca> _banca = jdbcTemplateObject.query(SQL, new BancasPerfillMapper());
        for (final Banca __banca : _banca) {
            this.permisoUsuario.agregarBanca(__banca);
        }

    }

    
    
    
    
    
}
