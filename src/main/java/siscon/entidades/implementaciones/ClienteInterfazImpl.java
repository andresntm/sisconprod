/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import config.SisConGenerales;
import configuracion.SisBojConf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Cliente;
import siscon.entidades.interfaces.ClienteInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author exesilr
 */
public class ClienteInterfazImpl implements ClienteInterfaz {

    private DataSource dataSource;
    private DataSource _internodataSource = null;
    private SimpleJdbcCall jdbcCall;
    Cliente infocliente = null;
    private JdbcTemplate jdbcTemplateObject;
    ClienteSisconJDBC _cliente;
    String NomspCliente = "sp_web_syscon_con_cli_V2";
    private final SisBojConf _bojConfig;
    private String nombre_spInfoClie;
    //Cliente infocliente = null;
    MvcConfig _datasource;
    SisConGenerales _control;
private static final Logger logger = LoggerFactory.getLogger(ClienteInterfazImpl.class);
    public ClienteInterfazImpl() {
        try {
            this._datasource = new MvcConfig();
            _internodataSource = _datasource.getDataSource();
            _cliente = new ClienteSisconJDBC(_datasource.getDataSource());
             _control = new SisConGenerales();
        } catch (SQLException e) {
            Messagebox.show("ERROR MvcConfig:[" + e.getMessage() + "]");
        }
        _bojConfig = new SisBojConf();
        nombre_spInfoClie = _bojConfig.Nombresp_detClie2();

    }

    public ClienteInterfazImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.infocliente = new Cliente();
        try {
            this._datasource = new MvcConfig();
            _internodataSource = _datasource.getDataSource();
            _cliente = new ClienteSisconJDBC(_datasource.getDataSource());
             _control = new SisConGenerales();
        } catch (SQLException e) {
            Messagebox.show("ERROR MvcConfig:[" + e.getMessage() + "]");
        }
        _bojConfig = new SisBojConf();
        nombre_spInfoClie = _bojConfig.Nombresp_detClie2();
    }

    public Cliente infocliente(final int rut) {

        List<Cliente> dddddd = null;

        try {
 dataSource = _datasource.getDataSource();
            if (_cliente.ExisteCliente(rut) ) {
                //Messagebox.show("Enviando Formulario de Condonaci�n a Analista Validador");
                                 if(_control.is_NovedadMontosNiOperaciones(rut,"ejebot")){
               // nombre_spDetOper = "sp_web_syscon_det_ope_cli_local";
                
                nombre_spInfoClie = "sp_web_syscon_con_cli_local";
               
            
                                 }

            }

        } catch (SQLException ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            // return DetalleOperacionesCliente;
        }
        //private SimpleJdbcCall jdbcCall;
        this.jdbcCall = new SimpleJdbcCall(dataSource).withFunctionName(nombre_spInfoClie).
                returningResultSet("rs", new ParameterizedRowMapper<Cliente>() {

                    public Cliente mapRow(ResultSet rs, int i) throws SQLException {

                        Cliente ainfocliente = new Cliente();
                        ainfocliente.setNombreCOmpleto(rs.getString("fld_nom_cli"));
                        ainfocliente.setRutint(rs.getInt("FLD_RUT"));
                        ainfocliente.setRut(rs.getString("FLD_RUT_ENT"));
                        ainfocliente.setSaldoTotalMoraPesos(rs.getString("FLD_SDO_TOT"));
                        ainfocliente.setSaldovigente(rs.getString("FLD_SDO_VIG"));
                        ainfocliente.setSaldocastigado(rs.getString("FLD_SDO_CAS"));
                        ainfocliente.setNumerooficina(rs.getInt("FLD_NUM_OFI"));
                        ainfocliente.setOficina(rs.getString("fld_ofi"));
                        ainfocliente.setFld_region(rs.getString("FLD_REG"));
                        ainfocliente.setBanca(rs.getString("FLD_BCA"));
                        ainfocliente.setFld_cod_banca(rs.getString("FLD_COD_BCA"));
                        ainfocliente.setEjecutivo(rs.getString("FLD_EJE"));
                        ainfocliente.setFld_cod_eje(rs.getString("fld_cod_eje"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_eje_resp"));
                        ainfocliente.setFld_cod_eje_resp(rs.getString("fld_cod_eje_resp"));
                        ainfocliente.setFld_dia_mor(rs.getString("fld_dia_mor"));
                        ainfocliente.setFld_jud(rs.getString("fld_jud"));
                        ainfocliente.setFld_ced(rs.getString("fld_ced"));
                        ainfocliente.setFld_cae(rs.getString("fld_cae"));
                        ainfocliente.setFld_pep(rs.getString("fld_pep"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_cae_pep"));
                        ainfocliente.setResp(rs.getString("resp"));
                        ainfocliente.setFld_sdo_aldia(rs.getString("fld_sdo_aldia"));
                        ainfocliente.setFld_sdo_morydia(rs.getString("fld_sdo_morydia"));
                        ainfocliente.setFld_mto_mor(rs.getString("fld_mto_mor"));
                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                        ainfocliente.setSaldototal(rs.getString("fld_sdo_tot"));

                        ainfocliente.setSaldoTotalMoraPesos(nf.format(rs.getLong("fld_sdo_tot")).replaceFirst("Ch", ""));

                        return ainfocliente;

                    }
                });
        //Map<String, Object> out = jdbcCall.execute(in);
        //dddddd = (List<Cliente>) out.get("rs");
        //SqlParameterSource in = new MapSqlParameterSource()
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("rut", rut)
                .addValue("userid", 3)
                .addValue("tipolog", "Ficha Condonacion")
                .addValue("user", "ejesiscon")
                .addValue("ip", "000.000.000")
                .addValue("desc", "Consulta al Rut: " + rut);
        try {
            
            
            
              SisCorelog("############################### clienteIMP linea 153  nombre_spInfoClie: [" + nombre_spInfoClie + "]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spInfoClie + "]");
            Map<String, Object> out = jdbcCall.execute(in);

            dddddd = (List<Cliente>) out.get("rs");
            infocliente = (Cliente) dddddd.get(0);
        } catch (SQLException ex) {
            Messagebox.show("ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]");
            System.out.print("####### ----ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]----#######");
            return infocliente;
        } catch (DataAccessException ex) {
            Messagebox.show("ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]");
            System.out.print("####### ----ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]----#######");
            return infocliente;
        } catch (Exception ex) {
            Messagebox.show("ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]");
            System.out.print("####### ----ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]----#######");
            return infocliente;
        }
        //List l = jdbcCall.executeFunction(List.class, Collections.EMPTY_MAP);

        SisCorelog("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + dddddd.get(0).nombreCOmpleto + "]----#######");

        return infocliente;
    }

    public Cliente infocliente(final int rut, String cuenta) {

        List<Cliente> dddddd = null;

        try {
            
            
            
            

            if (_cliente.ExisteCliente(rut)) {
                //Messagebox.show("Enviando Formulario de Condonaci�n a Analista Validador");
               
                if(_control.is_NovedadMontosNiOperaciones(rut,cuenta)){
                nombre_spInfoClie = "sp_web_syscon_con_cli_local";
                dataSource = _datasource.getDataSource();
            }
            }
            

        } catch (SQLException ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            // return DetalleOperacionesCliente;
        }
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName(nombre_spInfoClie).
                returningResultSet("rs", new ParameterizedRowMapper<Cliente>() {

                    public Cliente mapRow(ResultSet rs, int i) throws SQLException {

                        Cliente ainfocliente = new Cliente();
                        ainfocliente.setNombreCOmpleto(rs.getString("FLD_NOM_CLI"));
                        ainfocliente.setRutint(rs.getInt("FLD_RUT"));
                        ainfocliente.setRut(rs.getString("FLD_RUT_ENT"));
                        ainfocliente.setSaldoTotalMoraPesos(rs.getString("FLD_SDO_TOT"));
                        ainfocliente.setSaldovigente(rs.getString("FLD_SDO_VIG"));
                        ainfocliente.setSaldocastigado(rs.getString("FLD_SDO_CAS"));
                        ainfocliente.setNumerooficina(rs.getInt("FLD_NUM_OFI"));
                        ainfocliente.setOficina(rs.getString("fld_ofi"));
                        ainfocliente.setFld_region(rs.getString("FLD_REG"));
                        ainfocliente.setBanca(rs.getString("FLD_BCA"));
                        ainfocliente.setFld_cod_banca(rs.getString("FLD_COD_BCA"));
                        ainfocliente.setEjecutivo(rs.getString("FLD_EJE"));
                        ainfocliente.setFld_cod_eje(rs.getString("fld_cod_eje"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_eje_resp"));
                        ainfocliente.setFld_cod_eje_resp(rs.getString("fld_cod_eje_resp"));
                        ainfocliente.setFld_dia_mor(rs.getString("fld_dia_mor"));
                        ainfocliente.setFld_jud(rs.getString("fld_jud"));
                        ainfocliente.setFld_ced(rs.getString("fld_ced"));
                        ainfocliente.setFld_cae(rs.getString("fld_cae"));
                        ainfocliente.setFld_pep(rs.getString("fld_pep"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_cae_pep"));
                        ainfocliente.setResp(rs.getString("resp"));
                        ainfocliente.setFld_sdo_aldia(rs.getString("fld_sdo_aldia"));
                        ainfocliente.setFld_sdo_morydia(rs.getString("fld_sdo_morydia"));
                        ainfocliente.setFld_mto_mor(rs.getString("fld_mto_mor"));
                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                        ainfocliente.setSaldototal(rs.getString("fld_sdo_tot"));

                        ainfocliente.setSaldoTotalMoraPesos(nf.format(rs.getLong("fld_sdo_tot")).replaceFirst("Ch", ""));

                        return ainfocliente;

                    }
                ;
        });
      
            
             SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("userid", 3).addValue("tipolog", "Ficha Condonacion").addValue("user", cuenta).addValue("ip", "000.000.000").addValue("desc", "Consulta al Rut: " + rut);
        try {
            
            
            
                    SisCorelog("############################### Cliente IMP linea 248   nombre_spInfoClie: [" + nombre_spInfoClie + "]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spInfoClie + "]");
            //Messagebox.show(in.toString());
            Map<String, Object> out = jdbcCall.execute(in);

            dddddd = (List<Cliente>) out.get("rs");
            infocliente = (Cliente) dddddd.get(0);
        } catch (SQLException ex) {
            Messagebox.show("ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]");
            System.out.print("####### ----ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]----#######");
            return infocliente;
        }
        //List l = jdbcCall.executeFunction(List.class, Collections.EMPTY_MAP);

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + dddddd.get(0).nombreCOmpleto + "]----#######");

        return infocliente;
    }
   public Cliente infocliente(final int rut, String cuenta, final int rechazo) {

        List<Cliente> dddddd = null;

        try {
            
            boolean is_cliente=_cliente.ExisteCliente(rut);
            dataSource = _datasource.getDataSource();
            

            if(rechazo==0 && _control.is_NovedadMontosNiOperaciones(rut, cuenta))
            {
            
            nombre_spInfoClie = "sp_web_syscon_con_cli_local";
            
            
            }else if(rechazo!=0 &&  _control.is_NovedadMontosNiOperaciones(rut, cuenta))
            {
             nombre_spInfoClie = "sp_web_syscon_con_cli_local";
            
            }
            
            
            
         /*  if (_cliente.ExisteCliente(rut)) {
                //Messagebox.show("Enviando Formulario de Condonaci�n a Analista Validador");
               
                if(_control.is_NovedadMontosNiOperaciones(rut,cuenta)){
                nombre_spInfoClie = "sp_web_syscon_con_cli_local";
                dataSource = _datasource.getDataSource();
            }
            }*/
            

        } catch (SQLException ex) {
            //  System.out.print("####### ---- error174 Rut[" + ex.getMessage() + "]----#######");
            SisCorelog("MccConfig: [" + ex.getMessage() + "]");
            // return DetalleOperacionesCliente;
        }
        this.jdbcCall = new SimpleJdbcCall(dataSource).
                withFunctionName(nombre_spInfoClie).
                returningResultSet("rs", new ParameterizedRowMapper<Cliente>() {

                    public Cliente mapRow(ResultSet rs, int i) throws SQLException {

                        Cliente ainfocliente = new Cliente();
                        ainfocliente.setNombreCOmpleto(rs.getString("FLD_NOM_CLI"));
                        ainfocliente.setRutint(rs.getInt("FLD_RUT"));
                        ainfocliente.setRut(rs.getString("FLD_RUT_ENT"));
                        ainfocliente.setSaldoTotalMoraPesos(rs.getString("FLD_SDO_TOT"));
                        ainfocliente.setSaldovigente(rs.getString("FLD_SDO_VIG"));
                        ainfocliente.setSaldocastigado(rs.getString("FLD_SDO_CAS"));
                        ainfocliente.setNumerooficina(rs.getInt("FLD_NUM_OFI"));
                        ainfocliente.setOficina(rs.getString("fld_ofi"));
                        ainfocliente.setFld_region(rs.getString("FLD_REG"));
                        ainfocliente.setBanca(rs.getString("FLD_BCA"));
                        ainfocliente.setFld_cod_banca(rs.getString("FLD_COD_BCA"));
                        ainfocliente.setEjecutivo(rs.getString("FLD_EJE"));
                        ainfocliente.setFld_cod_eje(rs.getString("fld_cod_eje"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_eje_resp"));
                        ainfocliente.setFld_cod_eje_resp(rs.getString("fld_cod_eje_resp"));
                        ainfocliente.setFld_dia_mor(rs.getString("fld_dia_mor"));
                        ainfocliente.setFld_jud(rs.getString("fld_jud"));
                        ainfocliente.setFld_ced(rs.getString("fld_ced"));
                        ainfocliente.setFld_cae(rs.getString("fld_cae"));
                        ainfocliente.setFld_pep(rs.getString("fld_pep"));
                        ainfocliente.setFld_cae_pep(rs.getString("fld_cae_pep"));
                        ainfocliente.setResp(rs.getString("resp"));
                        ainfocliente.setFld_sdo_aldia(rs.getString("fld_sdo_aldia"));
                        ainfocliente.setFld_sdo_morydia(rs.getString("fld_sdo_morydia"));
                        ainfocliente.setFld_mto_mor(rs.getString("fld_mto_mor"));
                        NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());
                        ainfocliente.setSaldototal(rs.getString("fld_sdo_tot"));

                        ainfocliente.setSaldoTotalMoraPesos(nf.format(rs.getLong("fld_sdo_tot")).replaceFirst("Ch", ""));

                        return ainfocliente;

                    }
                ;
        });
      
            
             SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut).addValue("userid", 3).addValue("tipolog", "Ficha Condonacion").addValue("user", cuenta).addValue("ip", "000.000.000").addValue("desc", "Consulta al Rut: " + rut);
        try {
            
            
            
                    SisCorelog("############################### Cliente IMP linea 248   nombre_spInfoClie: [" + nombre_spInfoClie + "]");
            SisCorelog("MccConfig-URLLLLL: [" + dataSource.getConnection().getMetaData().getURL() + "]");
            SisCorelog("MccConfig-URLLLLLNombreSP: [" + nombre_spInfoClie + "]");
            //Messagebox.show(in.toString());
            Map<String, Object> out = jdbcCall.execute(in);

            dddddd = (List<Cliente>) out.get("rs");
            infocliente = (Cliente) dddddd.get(0);
        } catch (SQLException ex) {
            Messagebox.show("ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]");
            System.out.print("####### ----ClienteInterfazImpl.infocliente:Exception[" + ex.getMessage() + "]----#######");
            return infocliente;
        }
        //List l = jdbcCall.executeFunction(List.class, Collections.EMPTY_MAP);

        System.out.print("####### ---- Parametros Envio SP Clientes Operaciones Rut[" + rut + "]---User[" + dddddd.get(0).nombreCOmpleto + "]----#######");

        return infocliente;
    }

    public int insertDetSpClienteInfoCondonado(final int id_condonacion) {
        int retorna = -1;

        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        final String SQL = "INSERT INTO [traking_morahoy_clie]\n"
                + "           (\n"
                + "            [fk_idCondonacion]\n"
                + "           ,[fld_nom_cli]\n"
                + "           ,[fld_rut]\n"
                + "           ,[fld_rut_ent]\n"
                + "           ,[fld_sdo_tot]\n"
                + "           ,[fld_sdo_vig]\n"
                + "           ,[fld_sdo_cas]\n"
                + "           ,[fld_num_ofi]\n"
                + "           ,[fld_ofi]\n"
                + "           ,[fld_reg]\n"
                + "           ,[fld_bca]\n"
                + "           ,[fld_cod_bca]\n"
                + "           ,[fld_eje]\n"
                + "           ,[fld_cod_eje]\n"
                + "           ,[fld_eje_resp]\n"
                + "           ,[fld_cod_eje_resp]\n"
                + "           ,[fld_dia_mor]\n"
                + "           ,[fld_jud]\n"
                + "           ,[fld_ced]\n"
                + "           ,[fld_cae]\n"
                + "           ,[fld_pep]\n"
                + "           ,[fld_cae_pep]\n"
                + "           ,[resp]\n"
                + "           ,[fld_sdo_aldia]\n"
                + "           ,[fld_sdo_morydia]\n"
                + "           ,[fld_mto_mor]\n"
                + "           ,[fk_id_periodo])"
                + "     VALUES\n"
                + "            (?,\n"
                + "            ?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?\n"
                + "            ,?)";
        KeyHolder key = new GeneratedKeyHolder();
        try {
            jdbcTemplateObject.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection conex) throws SQLException {
                    PreparedStatement ps = conex.prepareStatement(SQL, new String[]{"id"});
                    ps.setInt(1, id_condonacion);
                    ps.setString(2, infocliente.getNombreCOmpleto());
                    ps.setString(3, Integer.toString(infocliente.getRutint()));
                    ps.setString(4, infocliente.getRut());
                    ps.setString(5, infocliente.getSaldototal());
                    ps.setString(6, infocliente.getSaldovigente());
                    ps.setString(7, infocliente.getSaldocastigado());
                    ps.setString(8, Integer.toString(infocliente.getNumerooficina()));
                    ps.setString(9, infocliente.getOficina());
                    ps.setString(10, infocliente.getFld_region());
                    ps.setString(11, infocliente.getBanca());
                    ps.setString(12, infocliente.getFld_cod_banca());
                    ps.setString(13, infocliente.getEjecutivo());
                    ps.setString(14, infocliente.getFld_cod_eje());
                    ps.setString(15, infocliente.getFld_cae_pep());
                    ps.setString(16, infocliente.getFld_cod_eje_resp());
                    ps.setString(17, infocliente.getFld_dia_mor());
                    ps.setString(18, infocliente.getFld_jud());
                    ps.setString(19, infocliente.getFld_ced());
                    ps.setString(20, infocliente.getFld_cae());
                    ps.setString(21, infocliente.getFld_pep());
                    ps.setString(22, infocliente.getFld_cae_pep());
                    ps.setString(23, infocliente.getResp());
                    ps.setString(24, infocliente.getFld_sdo_aldia());
                    ps.setString(25, infocliente.getFld_sdo_morydia());
                    ps.setString(26, infocliente.getFld_mto_mor());
                    ps.setInt(27, 1);
                    // System.out.print("-------QueryPS["+ps.+"]------");
                    return ps;

                }

            }, key);

            retorna = key.getKey().intValue();
        } catch (DataAccessException e) {
            Messagebox.show(e.toString());
            retorna = -1;
        }
        return retorna;
    }

    public int GetClienteCondonado(int id_condonacion) {
        int id_Cli = 0;

        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            String SQL = "SELECT di_fk_IdCliente FROM condonacion WHERE id_condonacion = ?";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            id_Cli = jdbcTemplateObject.queryForInt(SQL, new Object[]{id_condonacion});

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            id_Cli = 0;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            id_Cli = 0;
        }
        return id_Cli;
    }

    public String GetRutClienteCondonado(int id_cliente) {
        String rut = null;

        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {
            String SQL = "SELECT rut FROM cliente WHERE id_cliente = ?";
            rut = jdbcTemplateObject.queryForObject(SQL, new Object[]{id_cliente}, String.class);

        } catch (DataAccessException e) {
            Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar el rut de cliente para el cliente: " + Integer.toString(id_cliente) + " adjuntar\nError: " + e.toString());
            rut = null;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rut = null;
        }
        return rut;
    }

    public int GetIsClienteCampana(int rut) {

        int id_Cli = 4;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? ) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? ) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "         THEN (\n"
                    + " select top 1 * from ("
                    + "		 SELECT  isnull(cast(RUT_CLIENTE as varchar(100)),'null') 'rut'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "		 UNION ALL\n"
                    + "		 SELECT	 isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? \n"
                    + "		 UNION ALL\n"
                    + "SELECT  isnull(cast(RUT_CLIENTE as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ? \n"
                    + "	) as DD\n"
                    + "		 )\n"
                    + "       ELSE 'nulable'\n"
                    + "      END";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            logger.info("#####################################GetIsClienteCampana 430  - {}", SQL);
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

            if (rutt.equals("nulable")) {
                id_Cli = 4;
            } else {
                id_Cli = 0;
            }
        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            id_Cli = 0;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            id_Cli = 0;
        }
        return id_Cli;
    }

    public String getClienteOferta(int rut) {
        String id_Cli = "NULL";
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE=?) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "          THEN (\n"
                    + " select top 1 * from ("
                    + "            SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "	  UNION ALL\n"
                    + "	  SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? \n"
                    + "		 UNION ALL\n"
                    + "SELECT  isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA NORMAL' ) as varchar(100)),'null')'OFERTA'   FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ? \n"
                    + "	) as DD \n"
                    + "		  )\n"
                    + "        ELSE 'nulable'\n"
                    + "      END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }

    public float getClienteOfertaMontoPaga(int rut) {
        String id_Cli = "NULL";
        String rutt = "0";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	 	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? ) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? ) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "         THEN (\n"
                    + " select top 1 * from ("
                    + "		 SELECT isnull(MONTO_PAGO  ,0) 'OFERTA'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "		 UNION ALL\n"
                    + "		 SELECT	 isnull(TBCA.Oferta_Ren  ,0) 'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? \n"
                    + "		 UNION ALL\n"
                    + "SELECT isnull(MONTO_PAGO  ,0) 'OFERTA' FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ? \n"
                    + "	) as DD \n"
                    + "		 )\n"
                    + "       ELSE 0\n"
                    + "      END";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "-1";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "-1";
        }
        return Float.parseFloat(rutt);
    }

    public String getClienteTipoCondCampana(int rut) {
        String id_Cli = "NULL";
        String rutt = "0";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? ) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? )  or  EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= 8874960 ) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "         THEN (\n"
                    + " select top 1 * from ("
                    + "		 SELECT  isnull(cast('CON-CAMPA�A-NORM' as varchar(100)),'null') 'oferta'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "		 UNION ALL\n"
                    + "		 SELECT	 isnull(cast('CON-CAMPA�A-RIESGO' as varchar(100)),'null') 'oferta' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? \n"
                    + "		 UNION ALL\n"
                    + "SELECT	 isnull(cast('CON-CAMPA�A-NORM-NORM' as varchar(100)),'null') 'oferta' FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?"
                    + "	) as DD \n"
                    + "		 )\n"
                    + "       ELSE 'nulable'\n"
                    + "      END";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "-1";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "-1";
        }
        return rutt;
    }
   
    
    
    
    
    
    /// implementacion 2.0 de la busqueda de campa�a para boton de pago
    
    
    
    public String getClienteOferta_btnpgo(int rut) {
        String id_Cli = "NULL";
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE=?) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "          THEN (\n"
                    + " select top 1 * from ("
                    + "            SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "	  UNION ALL\n"
                    + "	  SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? \n"
                    + "		 UNION ALL\n"
                    + "SELECT  isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA NORMAL' ) as varchar(100)),'null')'OFERTA'   FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ? \n"
                    + "	) as DD \n"
                    + "		  )\n"
                    + "        ELSE 'nulable'\n"
                    + "      END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }

    public float getClienteOfertaMontoPaga_btnpgo(int rut) {
        String id_Cli = "NULL";
        String rutt = "0";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	 	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? ) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? ) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "         THEN (\n"
                    + " select top 1 * from ("
                    + "		 SELECT isnull(MONTO_PAGO  ,0) 'OFERTA'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "		 UNION ALL\n"
                    + "		 SELECT	 isnull(TBCA.Oferta_Ren  ,0) 'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? \n"
                    + "		 UNION ALL\n"
                    + "SELECT isnull(MONTO_PAGO  ,0) 'OFERTA' FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ? \n"
                    + "	) as DD \n"
                    + "		 )\n"
                    + "       ELSE 0\n"
                    + "      END";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "-1";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "-1";
        }
        return Float.parseFloat(rutt);
    }

    public String getClienteTipoCondCampana_btnpgo(int rut) {
        String id_Cli = "NULL";
        String rutt = "0";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        try {

            SQL = "	  	  SELECT CASE\n"
                    + "        WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? ) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? )  or  EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= 8874960 ) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?))\n"
                    + "         THEN (\n"
                    + " select top 1 * from ("
                    + "		 SELECT  isnull(cast('CON-CAMPA�A-NORM' as varchar(100)),'null') 'oferta'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = [IN_CBZA].dbo.FN_PER(GETDATE()) AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n"
                    + "		 UNION ALL\n"
                    + "		 SELECT	 isnull(cast('CON-CAMPA�A-RIESGO' as varchar(100)),'null') 'oferta' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut= ? \n"
                    + "		 UNION ALL\n"
                    + "SELECT	 isnull(cast('CON-CAMPA�A-NORM-NORM' as varchar(100)),'null') 'oferta' FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  [IN_CBZA].dbo.FN_PER(GETDATE()) AND RUT_CLIENTE = ?"
                    + "	) as DD \n"
                    + "		 )\n"
                    + "       ELSE 'nulable'\n"
                    + "      END";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "-1";
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "-1";
        }
        return rutt;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /// problema del colgelado de campa�a
    
    
      public String getClienteOferta_periodocond(int rut,int condonacion) {
        String id_Cli = "NULL";
        String perido_condonacion="",sql_peridocond;
        int periodo=0;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);
        
        
          // buscar periodo de la condonacion
          sql_peridocond = "select SUBSTRING(CONVERT(varchar,timestap,112),1,6) as timestap from condonacion con where con.id_condonacion=?";

          try {

              periodo = jdbcTemplateObject.queryForInt(sql_peridocond, new Object[]{condonacion});
          } catch (DataAccessException e) {
              periodo = 0;
              logger.info("#########getClienteOferta_periodocond 430  - {}   sql_peridocond{}", sql_peridocond);
          }

        logger.info("#########PERIDO 430  - {}   sql_peridocond{}", periodo);

        try {

            SQL = "	  SELECT CASE\n" +
"                  WHEN (EXISTS(SELECT   * from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'  AND  ofe.PERIODO ="+periodo+"  and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO')) 	) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?))\n" +
"                    THEN (\n" +
"                      SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'  AND  ofe.PERIODO = "+periodo+"  and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO'))  \n" +
"                       UNION ALL\n" +
"                      SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? \n" +
"					)\n" +
"                  ELSE 'nulable'\n" +
"                END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
                logger.info("#########PERIDO 430  - {}   SQL{}", SQL);
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
              logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
            
        } catch (Exception e) {
                logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }
    
    /// problema del colgelado de campa�a
    
    
      public String getClienteOferta_periodocondV2(int rut,int condonacion) {
        String id_Cli = "NULL";
        String perido_condonacion="",sql_peridocond;
        int periodo=0;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);
        
        
          // buscar periodo de la condonacion
          sql_peridocond = "select SUBSTRING(CONVERT(varchar,timestap,112),1,6) as timestap from condonacion con where con.id_condonacion=?";

          try {

              periodo = jdbcTemplateObject.queryForInt(sql_peridocond, new Object[]{condonacion});
          } catch (DataAccessException e) {
              periodo = 0;
              logger.info("#########getClienteOferta_periodocond 430  - {}   sql_peridocond{}", sql_peridocond);
          }

        logger.info("#########PERIDO 430  - {}   sql_peridocond{}", periodo);

        try {

            SQL = " SELECT CASE\n" +
"                  WHEN (EXISTS(SELECT   * from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'    and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO')) 	) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?))\n" +
"                    THEN (\n" +
"                      SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'    and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO'))  \n" +
"                       UNION ALL\n" +
"                      SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? \n" +
"           )\n" +
"                  ELSE 'nulable'\n" +
"                END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
                logger.info("#########PERIDO 430  - {}   SQL{}", SQL);
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
              logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
            
        } catch (Exception e) {
                logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }  
    
      
      
      
      
      
          /// problema del colgelado de campa�a deberian verse todas las campa�as
    
    
      public String getClienteOferta_periodocondV3(int rut,int condonacion) {
        String id_Cli = "NULL";
        String perido_condonacion="",sql_peridocond;
        int periodo=0;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);
        
        
          // buscar periodo de la condonacion
          sql_peridocond = "select SUBSTRING(CONVERT(varchar,timestap,112),1,6) as timestap from condonacion con where con.id_condonacion=?";

          try {

              periodo = jdbcTemplateObject.queryForInt(sql_peridocond, new Object[]{condonacion});
          } catch (DataAccessException e) {
              periodo = 0;
              logger.info("#########getClienteOferta_periodocond 430  - {}   sql_peridocond{}", sql_peridocond);
          }

        logger.info("#########PERIDO 430  - {}   sql_peridocond{}", periodo);

        try {

            SQL = " 		  SELECT CASE\n" +
"                  WHEN (EXISTS(SELECT   * from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'    and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO')) 	) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?) or  EXISTS (SELECT  isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA NORMALGuardado' ) as varchar(100)),'null')'OFERTA'   FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE  RUT_CLIENTE = ? and periodo ="+periodo+"))\n" +
"                    THEN (\n" +
"					\n" +
"					\n" +
"					   select case when  (EXISTS(SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'    and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO'))))\n" +
"					        then (SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'    and ofe.RUT_CLIENTE= ?  AND  (ofe.MARCA_SALIDA = 'NO' OR  (ofe.MARCA_SALIDA = 'SI' AND ofe.MOTIVO_SALIDA = '0. CLIENTE INHIBICION POR CANCELACION BOTON DE PAGO'))  )\n" +
"							else (\n" +
"							 select case when (exists(SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'   and ofe.RUT_CLIENTE= ? ))\n" +
"							        then (SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  from [in_cbza].dbo.[TBL_REMESAS_OFERTA] rem inner join in_cbza.dbo.[TBL_REMESAS_OFERTA_DETALLE] remdet on remdet.ID_REMESA=rem.ID inner join in_cbza.dbo.[TBL_BASE_CASTIGOS_OFERTA] ofe on ofe.COD_IDENTIFICADOR=remdet.COD_IDENTIFICADOR  where  remdet.resultado_validacion = 'APROBADO'   and ofe.RUT_CLIENTE= ? )\n" +
"							\n" +
"							else ( select case when(exists(SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? ))\n" +
"                                   then (SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? )\n" +
"								   else \n" +
"                                    ( SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA NORMALGuardado' ) as varchar(100)),'null')'OFERTA'   FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE  RUT_CLIENTE = ? and periodo ="+periodo+"  								   )\n" +
"                                    end\n" +
"                                )\n" +
"						end		        \n" +
"					         \n" +
"							\n" +
"					  \n" +
"					\n" +
"					\n" +
"				)	end )\n" +
"                  ELSE 'nulable'\n" +
"                END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
                logger.info("#########PERIDO 430  - {}   SQL{}", SQL);
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut,rut,rut,rut,rut,rut,rut}, String.class);

        } catch (DataAccessException e) {
              logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
            
        } catch (Exception e) {
                logger.info("#########ERROR  getClienteOferta_periodocond 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }  
      
      
      
      
      
      
      
      
          public String getClienteOfertaCampana(int rut,int condonacion) {
        String id_Cli = "NULL",sql_peridocond;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        int periodo;
        jdbcTemplateObject = new JdbcTemplate(_internodataSource);

        
                  // buscar periodo de la condonacion
          sql_peridocond = "select SUBSTRING(CONVERT(varchar,timestap,112),1,6) as timestap from condonacion con where con.id_condonacion=?";

          try {

              periodo = jdbcTemplateObject.queryForInt(sql_peridocond, new Object[]{condonacion});
          } catch (DataAccessException e) {
              periodo = 0;
              logger.info("#########getClienteOferta_periodocond 430  - {}   sql_peridocond{}", sql_peridocond);
          }

        logger.info("#########PERIDO 430  - {}   sql_peridocond{}", periodo);
        
        
        
        try {

            SQL = "	  SELECT CASE\n" +
"                          WHEN (EXISTS(SELECT   * FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = "+periodo+" AND MARCA_SALIDA = 'NO' and RUT_CLIENTE=?) OR EXISTS (SELECT	isnull(cast(fld_rut as varchar(100)),'null') 'rut' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=?) or EXISTS(select * FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO = "+periodo+" AND RUT_CLIENTE = ?))\n" +
"                            THEN (\n" +
"                   select top 1 * from (\n" +
"                              SELECT   isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA' ) as varchar(100)),'null')'OFERTA'  FROM [IN_CBZA].[DBO].[TBL_BASE_CASTIGOS_OFERTA] WHERE PERIODO = "+periodo+" AND MARCA_SALIDA = 'NO' and RUT_CLIENTE= ? \n" +
"                  	  UNION ALL\n" +
"                  	  SELECT    isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Saldo_Moroso  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST((TBCA.Saldo_Moroso-TBCA.Oferta_Ren)  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(TBCA.Oferta_Ren  AS MONEY), 1)),'    ','OFERTA RIESGO' ) as varchar(100)),'null')'OFERTA' FROM [IN_CBZA].[DBO].TBL_CAR_ASG_RANGOS TBCA WHERE	FLD_CAM_NOM IN ('INIC_CONDONACION') AND	GETDATE() BETWEEN FLD_DES AND FLD_HAS  and fld_rut=? \n" +
"                  		 UNION ALL\n" +
"                  SELECT  isnull(CAST(CONCAT('Capital=','$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(SALDO_CAPITAL  AS MONEY), 1)) ,'- monto condonado = ', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_CONDONADO  AS MONEY), 1)),'=Monto Pago=', '$' + CONVERT(VARCHAR, CONVERT(VARCHAR, CAST(MONTO_PAGO  AS MONEY), 1)),'   ','OFERTA NORMALIZA NORMAL' ) as varchar(100)),'null')'OFERTA'   FROM [IN_CBZA].[DBO].TBL_BASE_CASTIGOS_OFERTA O WHERE PERIODO =  "+periodo+" AND RUT_CLIENTE = ? \n" +
"                  	) as DD \n" +
"                  		  )\n" +
"                          ELSE 'nulable'\n" +
"                        END ";
            //Messagebox.show(SQL+"["+id_condonacion+"]");
            rutt = jdbcTemplateObject.queryForObject(SQL, new Object[]{rut, rut, rut, rut, rut, rut}, String.class);

        } catch (DataAccessException e) {
             logger.info("#########ERROR  getClienteOfertaCampana 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonaci�n: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            rutt = "null";
        } catch (Exception e) {
             logger.info("#########ERROR  getClienteOfertaCampana 430  - {}   SQL{}", e.getMessage());
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            rutt = "null";
        }
        return rutt;
    }
      
      
      
}
