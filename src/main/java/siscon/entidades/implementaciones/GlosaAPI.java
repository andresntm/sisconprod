/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.sql.DataSource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.entidades.GlosaDET;
import siscon.entidades.GlosaENC;
import siscon.entidades.interfaces.GlosaInterfaz;

/**
 *
 * @author excosoc
 */
public class GlosaAPI implements GlosaInterfaz {

    private DataSource dS = null;
    private SimpleJdbcCall sJC;
    private JdbcTemplate jT;
    private MvcConfig conex;
    private UsuarioJDBC user;
    private boolean apiEnc;
    private GlosaENC glosaEnc;

    public GlosaAPI() {

        try {
            conex = new MvcConfig();
            dS = conex.getDataSource();
            user = new UsuarioJDBC(dS);
            jT = new JdbcTemplate(dS);

        } catch (DataAccessException sqlE) {
            Messagebox.show("Error al conectar a la base de datos\nError: " + sqlE.getMessage());
        } catch (SQLException sqlE) {
            Messagebox.show("Error al conectar a la base de datos\nError: " + sqlE.getMessage());
        }
    }

    /**
     *
     * @param enc variable tipo GlosaENC
     * @param accion
     * @return List<GlosaENC>
     * accion, tiene 2 valores accion = 1, insertar glosas accion = 2,
     * seleccionar glosas
     *
     */
    public GlosaENC api(GlosaENC enc, int accion) {
        glosaEnc = new GlosaENC();

        try {
            if (accion == 1) {
                //2 acciones insert / update
                if (enc.getId() != 0) {
                    apiENC(enc, 2);

                    enc.getDetalle().forEach(new Consumer<GlosaDET>() {
                        public void accept(GlosaDET t) {
                            if (t.isEdit()) {
                                apiDET(t);
                            }
                        }

                    });
                } else {
                    final int idGlosaEnc = apiENC(enc, 1);

                    enc.getDetalle().forEach(new Consumer<GlosaDET>() {
                        public void accept(GlosaDET t) {
                            if (t.isEdit()) {
                                t.setId_usrGlosa(idGlosaEnc);
                                apiDET(t);
                                t.setEdit(false);
                            }
                        }
                    });
                }
                glosaEnc = enc;
            } else if (accion == 2) {
                if (enc.getId_reparo() != 0) {
                    glosaEnc = GetENC_Reparo(enc.getId_cond(), enc.getId_reparo());
                    glosaEnc.setDetalle(GetDET(enc.getId_cond()));
                } else {
                    glosaEnc = GetENC(enc.getId_cond());
                    glosaEnc.setDetalle(GetDET(enc.getId_cond()));
                }
            }

        } catch (Exception e) {

            glosaEnc = null;
        }
        return glosaEnc;
    }

    private boolean apiDET(GlosaDET det) {

        try {
            this.sJC = new SimpleJdbcCall(dS).withProcedureName("SP_SISCON_DETGLOSA");

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("ID_USRGLOSA", det.getId_usrGlosa())
                    .addValue("ID_EJE", det.getId_user())
                    .addValue("GLOSA", det.getGlosa())
                    .addValue("MODULO", det.getModulo());

            sJC.execute(in);

            return true;
        } catch (DataAccessException E) {
            Messagebox.show("Error en la api detalle glosa 5.\nError: " + E.getMessage());
            return false;
        } catch (Exception E) {
            Messagebox.show("Error en la api detalle glosa 4.\nError: " + E.getMessage());
            return false;
        }
    }

    private int apiENC(GlosaENC enc, int accion) {
        apiEnc = false;
        int idEnc = 0;
        GlosaENC retornoENC = new GlosaENC();
        List<GlosaENC> listRetENC = new ArrayList<GlosaENC>();

        try {
            this.sJC = new SimpleJdbcCall(dS).withProcedureName("SP_SISCON_ENCGLOSA").returningResultSet("rs", new RowMapper<GlosaENC>() {
                public GlosaENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    GlosaENC sqlEnc = new GlosaENC();

                    sqlEnc.setId(rs.getInt("id"));

                    return sqlEnc;
                }

            });

            SqlParameterSource in = new MapSqlParameterSource().addValue("ID_CLIENTE", enc.getId_client())
                    .addValue("ID_COND", enc.getId_cond())
                    .addValue("CANT_ADJ", enc.getCant_glosas())
                    .addValue("ID_REPARO", enc.getId_reparo())
                    .addValue("ACCION", accion);

            Map<String, Object> out = sJC.execute(in);

            listRetENC = (List<GlosaENC>) out.get("rs");

            retornoENC = listRetENC.get(0);

            idEnc = retornoENC.getId();

            apiEnc = true;

        } catch (DataAccessException E) {
            apiEnc = false;
            idEnc = 0;
        } catch (Exception E) {
            apiEnc = false;
            idEnc = 0;
        }

        return idEnc;
    }

    private List<GlosaDET> GetDET(int id_Condonacion) {
        List<GlosaDET> listGDET = new ArrayList<GlosaDET>();

        try {
            String SQL = "SELECT DG.id,\n"
                    + "       DG.fk_idUsrGlosa,\n"
                    + "       DG.descripcion,\n"
                    + "       DG.registrado,\n"
                    + "       DG.fk_idColaborador,\n"
                    + "       DG.modulo,\n"
                    + "       U.alias\n"
                    + "FROM dbo.DETALLE_GLOSA DG\n"
                    + "     INNER JOIN dbo.USR_GLOSA UG ON DG.fk_idUsrGlosa = UG.ID\n"
                    + "     INNER JOIN dbo.colaborador C ON DG.fk_idColaborador = C.id\n"
                    + "     INNER JOIN dbo.usuario U ON C.id_usuario = U.id_usuario\n"
                    + "WHERE UG.fk_idCondonacion = ?;";
            listGDET = jT.query(SQL, new Object[]{id_Condonacion}, new RowMapper<GlosaDET>() {
                public GlosaDET mapRow(ResultSet rs, int rowNum) throws SQLException {
                    GlosaDET sqlDet = new GlosaDET(rs.getInt("id"),
                            rs.getInt("fk_idUsrGlosa"),
                            rs.getString("descripcion"),
                            rs.getString("alias"),
                            false,
                            rs.getString("modulo"));

                    return sqlDet;
                }
            });
        } catch (DataAccessException e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det glosa\nError: " + e.toString());
            listGDET = null;
        } catch (Exception e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos det glosa\nError: " + e.toString());
            listGDET = null;
        }
        return listGDET;
    }

    private GlosaENC GetENC(int id_condonacion) {
        GlosaENC aEnc = new GlosaENC();
        List<GlosaENC> ListAEnc = new ArrayList<GlosaENC>();
        try {
            String SQL = "SELECT UG.id,\n"
                    + "       UG.fk_idCliente,\n"
                    + "       UG.fk_idCondonacion,\n"
                    + "       UG.registrado,\n"
                    + "       UG.numero_glosas\n"
                    + "FROM dbo.USR_GLOSA UG\n"
                    + "WHERE UG.fk_idCondonacion=?";
            ListAEnc = (List<GlosaENC>) jT.query(SQL, new Object[]{id_condonacion}, new RowMapper<GlosaENC>() {
                public GlosaENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    GlosaENC sqlEnc = new GlosaENC(rs.getInt("id"),
                            rs.getInt("fk_idCliente"),
                            rs.getInt("fk_idCondonacion"),
                            rs.getInt("numero_glosas"));

                    return sqlEnc;
                }
            });

            aEnc = ListAEnc.get(0);
        } catch (DataAccessException e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc glosa\nError: " + e.toString());
            aEnc = null;

        } catch (Exception e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc glosa\nError: " + e.toString());
            aEnc = null;
        }
        return aEnc;
    }

    private GlosaENC GetENC_Reparo(int id_condonacion, int id_reparo) {
        GlosaENC aEnc = new GlosaENC();
        List<GlosaENC> ListAEnc = new ArrayList<GlosaENC>();
        try {
            String SQL = "SELECT UG.ID,\n"
                    + "       UG.FK_IDCLIENTE,\n"
                    + "       UG.FK_IDCONDONACION,\n"
                    + "       UG.REGISTRADO,\n"
                    + "       UG.NUMERO_GLOSAS,\n"
                    + "	  UG.FK_IDREPARO\n"
                    + "FROM DBO.USR_GLOSA UG\n"
                    + "WHERE UG.FK_IDCONDONACION = ?\n"
                    + "AND UG.FK_IDREPARO = ?";
            ListAEnc = (List<GlosaENC>) jT.query(SQL, new Object[]{id_condonacion, id_reparo}, new RowMapper<GlosaENC>() {
                public GlosaENC mapRow(ResultSet rs, int rowNum) throws SQLException {
                    GlosaENC sqlEnc = new GlosaENC(rs.getInt("ID"),
                            rs.getInt("FK_IDCLIENTE"),
                            rs.getInt("FK_IDCONDONACION"),
                            rs.getInt("NUMERO_GLOSAS"),
                            rs.getInt("FK_IDREPARO"));

                    return sqlEnc;
                }
            });

            aEnc = ListAEnc.get(0);
        } catch (DataAccessException e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc glosa\nError: " + e.toString());
            aEnc = null;

        } catch (Exception e) {
//            Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc glosa\nError: " + e.toString());
            aEnc = null;
        }
        return aEnc;
    }

}
