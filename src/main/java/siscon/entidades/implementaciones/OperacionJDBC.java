/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.implementaciones;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zul.Messagebox;
import siscon.RowMapper.UsrModInterezOperacionMapper;
import siscon.entidades.UsrModInteresOperacion;

/**
 *
 * @author esilves
 */
public class OperacionJDBC {

    private DataSource dataSource = null;
    private DataSource dataSourceSisCon = null;
    private SimpleJdbcCall jdbcCall;
    private JdbcTemplate jdbcTemplate;
    private JdbcTemplate jdbcTemplateSisCon;

    public OperacionJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
        jdbcTemplate = new JdbcTemplate(dataSource);

    }

    public OperacionJDBC(DataSource dataSource, String sistema) {

        this.dataSourceSisCon = sistema.equals("siscon") ? dataSource : null;
        jdbcTemplate = new JdbcTemplate(dataSource);

    }

    public Boolean TieneRol_old(int rutCliente, String NumeroOperacion) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        String SQLtramsito = "select count(*) from ju_judicial  where rut_cliente='" + rutCliente + "' and ROL is not null and operacion='" + NumeroOperacion + "'";
        try {
            _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);
        } catch (EmptyResultDataAccessException e) {
            Messagebox.show("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]");

        }
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }

    /**
     * version 2.0 con nueva consulta plg 17.02.2020
     *
     * @param rutCliente
     * @param NumeroOperacion
     * @return
     */
    public Boolean TieneRol(int rutCliente, String NumeroOperacion) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        // String SQLtramsito = "select count(*) from ju_judicial  where rut_cliente='" + rutCliente + "' and ROL is not null and operacion='" + NumeroOperacion + "'";

        String SQLtramsito = "select count(*) from  [PCJBCI_PROD].[dbo].ju_juicios  juic  \n"
                + "inner join [PCJBCI_PROD].[dbo].op_operacionesjuicios  opjui                 ON opjui.id_juicio       = juic.id_juicio \n"
                + "INNER JOIN [PCJBCI_PROD].[dbo].op_operaciones  op   on  op.id_operacion=opjui.id_operacion\n"
                + "INNER JOIN [PCJBCI_PROD].[dbo].ju_juiciosEstados    juiep      ON juic.id_juicioestado            = juiep.id_juicioestado\n"
                + "where  op.op_codoperacion = REPLACE(LTRIM(REPLACE('" + NumeroOperacion + "','0',' ')),' ','0')   and juiep.je_juicioestado   in ('Activo','Eliminado','Suspendido','Suspendido desde embargo','En Avenimiento o Cuenta de Pago')  and juic.cl_rut like '%" + rutCliente + "%'";
        try {
            _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);
           // SisCorelog("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
           System.out.println("QUERYHONORARIOS:["+SQLtramsito+"]");
                System.out.println("Ok Honorarios --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
            
        } catch (EmptyResultDataAccessException e) {
             System.out.println("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
            Messagebox.show("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");

        }
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }
        /**
     * version 3.0 con nueva consulta plg 12.06.2020
     *
     * @param rutCliente
     * @param NumeroOperacion
     * @return
     */
    public Boolean TieneRol_NEW(int rutCliente, String NumeroOperacion) {

        int _numDocumentos = 0;
        jdbcTemplate = new JdbcTemplate(dataSource);
        // String SQLtramsito = "select count(*) from ju_judicial  where rut_cliente='" + rutCliente + "' and ROL is not null and operacion='" + NumeroOperacion + "'";

        String SQLtramsito = "select count(*) from (\n" +
" 			 select   (CASE\n" +
"							 WHEN (isnull((SELECT * FROM [dbo].[_tipoOperacion] ("+rutCliente+","+NumeroOperacion+") ),'N')<>'N')\n" +
"							 then (SELECT * FROM [dbo].[_tipoOperacion] ("+rutCliente+","+NumeroOperacion+"))\n" +
"							 else 'NOEXISTE' end) As ee ,juic.cl_rut,op.op_codoperacion  from  [PCJBCI_PROD].[dbo].ju_juicios  juic  \n" +
"					inner join [PCJBCI_PROD].[dbo].op_operacionesjuicios  opjui                 ON opjui.id_juicio       = juic.id_juicio \n" +
"					INNER JOIN [PCJBCI_PROD].[dbo].op_operaciones  op   on  op.id_operacion=opjui.id_operacion\n" +
"					INNER JOIN [PCJBCI_PROD].[dbo].ju_juiciosEstados    juiep      ON juic.id_juicioestado            = juiep.id_juicioestado\n" +
"				   where  juiep.je_juicioestado   in ('Activo','Eliminado','Suspendido','Suspendido desde embargo','En Avenimiento o Cuenta de Pago')  and juic.cl_rut like '%"+rutCliente+"%'\n" +
"\n" +
"\n" +
"				   ) as JJ\n" +
"				   where JJ.op_codoperacion = (case when ee ='Hipotecario' then LTRIM("+NumeroOperacion+") else REPLACE(LTRIM(REPLACE("+NumeroOperacion+",'0',' ')),' ','0') end)";
        
        try {
            _numDocumentos = jdbcTemplate.queryForInt(SQLtramsito);
           // SisCorelog("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
           System.out.println("QUERYHONORARIOS:["+SQLtramsito+"]");
           System.out.println("Ok Honorarios --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
            
        } catch (EmptyResultDataAccessException e) 
        {
            
            System.out.println("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");
            Messagebox.show("ERROR --- Obteniendo Rol[" + rutCliente + "],Nombre:[" + NumeroOperacion + "]  si  tiene mayor que cero _numDocumentos: ["+_numDocumentos+"]");

        }
        //  this.lot.setN_DocumentosPikeacos(_numActualDocumentosPickeados);
        return _numDocumentos > 0;
        //if(_numActualDocumentosPickeados>0)    return false;
        //     return false;

    }
    /**
     *
     * @param rut
     * @param alias
     * @param operacion
     * @param id_condonacion
     * @return
     */
    public float getProcentajeInteresActual(int rut, String alias, String operacion, int id_condonacion) {

        float result = -1;
        String SQL = "";
        if (id_condonacion > 0) {

            SQL = " select umod.valor_Anteriro,umod.valor_actual,cli.rut,umod.Codigo_operacion from  usr_modif_interes umod  \n"
                    + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                    + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                    + " inner join cliente cli on CAST( SUBSTRING(cli.rut, 1, CHARINDEX('-', cli.rut)-1) as INT)=umod.rut_usuario\n"
                    + " where  CAST( SUBSTRING(cli.rut, 1, CHARINDEX('-', cli.rut)-1) as INT)='" + rut + "'  and usu.alias='" + alias + "' and umod.vigente='True' and umod.Codigo_operacion='" + operacion + "'; \n";

        } else {

            SQL = " select umod.valor_Anteriro,umod.valor_actual,'00000000' rut,umod.Codigo_operacion from  usr_modif_interes umod  \n"
                    + " inner join colaborador col on col.id=umod.fk_idColaborador \n"
                    + " inner join usuario usu on usu.id_usuario=col.id_usuario  \n"
                    + " where usu.alias='" + alias + "' and umod.vigente='True' and umod.Codigo_operacion='" + operacion + "'; \n";

        }
        try {
            List<UsrModInteresOperacion> _usrModfInteresOperacion = jdbcTemplate.query(SQL, new UsrModInterezOperacionMapper());

            if (_usrModfInteresOperacion.size() > 0) {

                result = _usrModfInteresOperacion.size() > 0 ? _usrModfInteresOperacion.get(0).getPorcentajeActual() : -1;

            } else {
                result = -1;
            }

        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            Messagebox.show("ERROR --- GetPorcentajeInterezActual[" + rut + "],Nombre:[" + alias + "]");

        }

        return result;
    }

}
