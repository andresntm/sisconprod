/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.CondonacionesEspeciales;
import siscon.entidades.TipoCondEspecial;

/**
 *
 * @author excosoc
 */
public interface CondEspecialesInterfaz {

    public int insertCondEspecial(CondonacionesEspeciales condEsp);

    public int insertTipCondEspecial(TipoCondEspecial tipCondEsp);
    
    public boolean updateCondEspecial(CondonacionesEspeciales condEsp);
    
    public boolean deleteCondEspecial(CondonacionesEspeciales condEsp);

    public TipoCondEspecial selectTipCondEspecial_X_Id_TipCondEsp(final int id_TipConEsp);

    public CondonacionesEspeciales selectCondEspecial_X_Id_CondEsp(final int id_ConEsp);

    public CondonacionesEspeciales selectCondEspecial_X_Id_Condonacion(final int id_Condonacion);

    public List<CondonacionesEspeciales> listCondEspecial();

    public List<TipoCondEspecial> listTipCondEspecial();

    public List<CondonacionesEspeciales> listCondEspecial_X_Id_TipCondEsp(final int id_TipConEsp);

    public List<TipoCondEspecial> listTipCondEspecial_Usadas();

}
