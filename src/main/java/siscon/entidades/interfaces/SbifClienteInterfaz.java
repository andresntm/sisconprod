/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.GarantiasCliente;
import siscon.entidades.SbifCliente;

/**
 *
 * @author exesilr
 */
public interface SbifClienteInterfaz {
   public List<SbifCliente> SbifCliente(int rut,String usuario);
   public String SumaTotalGarantiasPesos();
}
