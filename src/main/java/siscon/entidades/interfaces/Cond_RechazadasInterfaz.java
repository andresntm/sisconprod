/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Cond_Rechazadas;

/**
 *
 * @author excosoc
 */
public interface Cond_RechazadasInterfaz {

    public int insertRechazo(final Cond_Rechazadas cR);

    public boolean updateRechazo(final Cond_Rechazadas cR);

    public Cond_Rechazadas getRechazo(final int id_Rechazo);

    public List<Cond_Rechazadas> listRechazos();

    public List<Cond_Rechazadas> listRechazos_X_Colaborador(final int id_colaborador);

    public boolean deleteRechazo(final int id_Rechazo);
     public int insertPorcentajeRechazo(final int id_rechazo,final int porcentaje_x);

}
