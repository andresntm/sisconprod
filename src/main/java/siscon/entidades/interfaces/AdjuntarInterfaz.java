/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import siscon.entidades.AdjuntarENC;

/**
 *
 * @author excosoc
 */
public interface AdjuntarInterfaz {

    /**
     *
     * @param enc variable tipo AdjuntarENC
     * @param accion
     * @return List<GlosaENC>
     * accion, tiene 2 valores accion = 1, insertar glosas accion = 2,
     * seleccionar glosas
     *
     */
    public AdjuntarENC api(AdjuntarENC enc, int accion);

}
