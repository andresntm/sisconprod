/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.InformeCond_Ap_Re_Pe;

/**
 *
 * @author excosoc
 */
public interface InformeCond_Ap_Re_PeInterface {

    public List<InformeCond_Ap_Re_Pe> ListInformeCond_Ap_Re_Pe_Pyme();
    public List<InformeCond_Ap_Re_Pe> ListInformeCond_Ap_Re_Pe_Retail();

}
