/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import siscon.entidades.ClienteSiscon;

import java.util.List;

/**
 *
 * @author excosoc
 */
public interface ClienteSisconInterfaz {

    public ClienteSiscon getCliente(int id_cliente);

    public ClienteSiscon getCliente(String rut);

    public ClienteSiscon RegeneraCliente(String rut);

    public int traeIdCedenteCliente(String cedente);

    public List<ClienteSiscon> listClientes();

    public List<ClienteSiscon> listClientes_por_cendete(int id_cedente);

    public int insertCliente(ClienteSiscon cS);

    public int insertCliente_Cendente(ClienteSiscon cS);

    public boolean updateCliente(ClienteSiscon cS);

}
