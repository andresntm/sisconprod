/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.RangoMonetario;

/**
 *
 * @author excosoc
 */
public interface RangoMonetarioInterfaz {

    public int insertRangoMonetario(RangoMonetario rangoMonetario);

    public boolean updateRangoMonetario(RangoMonetario rangoMonetario);

    public RangoMonetario getRangoMonetarioXId(final int id_rangom);

    public List<RangoMonetario> getRangoMonetario();

}
