/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.GesSmall;
import siscon.entidades.OfeSmall;

/**
 *
 * @author exesilr
 */
public interface SmallCoreInterfaz {
   public List<GesSmall> GestionesSmallCore(int rut,String cuenta);
   public List<OfeSmall> OfertasSmallCore(int rut, String cuenta);
   public boolean IsEmptyOferta(int rut, String cuenta);

}
