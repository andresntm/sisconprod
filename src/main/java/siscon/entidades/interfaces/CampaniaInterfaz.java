/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Campania;

/**
 *
 * @author excosoc
 */
public interface CampaniaInterfaz {
    
    public int insertCampania(final Campania campania);

    public boolean updateCampania(final Campania campania);

    public Campania getCampaniaXId(final int id_campania);
    
    public List<Campania> getCampania();
    
    public boolean DeleteCampania(final Campania campania);
    
}
