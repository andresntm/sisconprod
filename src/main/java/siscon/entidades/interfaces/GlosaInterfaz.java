/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.GlosaENC;

/**
 *
 * @author excosoc
 */
public interface GlosaInterfaz {

    /**
     *
     * @param enc variable tipo GlosaENC
     * @param accion
     * @return List<GlosaENC>
     * accion, tiene 2 valores accion = 1, insertar glosas accion = 2,
     * seleccionar glosas
     *
     */
    public GlosaENC api(GlosaENC enc, int accion);

}
