/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.RangoFecha;

/**
 *
 * @author excosoc
 */
public interface RangoFechasInterfaz {

    public int insertRangoFecha(RangoFecha rangoFecha);

    public boolean updateRangoFecha(RangoFecha rangoFecha);

    public RangoFecha getRangoFechaXId(final int id_rangof);

    public List<RangoFecha> getRangoFecha();

}
