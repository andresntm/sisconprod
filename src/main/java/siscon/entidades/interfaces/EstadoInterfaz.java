/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import siscon.entidades.Estado;
import java.util.List;

/**
 *
 * @author excosoc
 */
public interface EstadoInterfaz {

    public Estado getEstado_X_IdEstado(final int id_Estado);

    public Estado getEstado_X_dvCodEstado(final String dvCodEstado);

    public List<Estado> listEstado();
}
