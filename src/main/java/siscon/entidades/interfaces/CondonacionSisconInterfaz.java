/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.CondonacionSiscon;

/**
 *
 * @author excosoc
 */
public interface CondonacionSisconInterfaz {

    public CondonacionSiscon getCondonacion(final int id_condonacion);

    public List<CondonacionSiscon> listCondonacion();

    public List<CondonacionSiscon> listCondonacion_X_idColadorador(final int idColadorador);

    public List<CondonacionSiscon> listCondonacion_X_IdCliente(final int idCliente);

    public List<CondonacionSiscon> listCondonacion_X_IdTipoCondonacion(final int idTipoCondonacion);

    public List<CondonacionSiscon> listCondonacion_X_IdEstado(final int idEstado);

    public List<CondonacionSiscon> listCondonacion_X_IdRegla(final int idRegla);

}
