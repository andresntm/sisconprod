/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Trackin_Estado;
import siscon.entidades.usuario;

/**
 *
 * @author excosoc
 */
public interface Trackin_EstadoInterfaz {

    public int insertTrackin_Estado(final Trackin_Estado tE);

    public boolean updateTrackin_Estado(final Trackin_Estado tE);

    public Trackin_Estado getTrackin_EstadoXId(final int id_trakinestado);

    public List<Trackin_Estado> getTrackin_EstadoXIdCondonacion(final int id_condonacion);

    public List<Trackin_Estado> listTrackin_Estados();

    public boolean DeleteTrackin_Estado(final Trackin_Estado tE);

    public usuario ejecutivoDestinoXCond(int id_condonacion);

    public usuario usuarioOrigen(String alias);

    public boolean updateUsuarioAproTrackin_Estado(final Trackin_Estado tE);

    public int insertTrackin_Estado_sinDestino(final Trackin_Estado tE);

    public boolean updateTrackin_Estado_Ana_Zon_Recept(final Trackin_Estado tE);

    public Trackin_Estado getTrackin_EstadoXcond_Actual(final int id_condonacion);

    public Trackin_Estado getUltimoTrackin_EstadoXcond(final int id_condonacion);
}
