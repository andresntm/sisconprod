/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.EstadoCondonacion;

/**
 *
 * @author excosoc
 */
public interface EstadoCondonacionInterfaz {

    public EstadoCondonacion getEstadoCondonacion(final int id_EstadoCondonacion);

    public List<EstadoCondonacion> listEstadoCondonacion();

    public List<EstadoCondonacion> listEstadoCondonacion_X_IdCondonacion(final int IdCondonacion);

    public List<EstadoCondonacion> listEstadoCondonacion_X_IdEstado(final int IdEstado);

}
