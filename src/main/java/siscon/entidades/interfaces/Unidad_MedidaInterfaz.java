/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Unidad_Medida;

/**
 *
 * @author excosoc
 */
public interface Unidad_MedidaInterfaz {
    
    public int insertUnidad_Medida(final Unidad_Medida unidad_Medida);

    public boolean updateUnidad_Medida(final Unidad_Medida unidad_Medida);

    public Unidad_Medida getUnidad_MedidaXId(final int id_undmed);

    public List<Unidad_Medida> getUnidad_Medida();
    
}
