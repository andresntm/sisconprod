/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Tipo_Rechazo;

/**
 *
 * @author excosoc
 */
public interface Tipo_RechazoInterfaz {

    public int insertTipo_Rechazo(final Tipo_Rechazo tR);

    public boolean updateTipo_Rechazo(final Tipo_Rechazo tR);

    public Tipo_Rechazo getTipo_Rechazo(final int id_TipRechazo);

    public List<Tipo_Rechazo> listTipo_Rechazos();

    public boolean activa_InactivaTipo_Rechazo(final Tipo_Rechazo tR);

}
