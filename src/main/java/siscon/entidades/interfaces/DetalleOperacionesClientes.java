/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import javax.sql.DataSource;
import org.zkoss.chart.model.CategoryModel;
import siscon.entidades.DetalleCliente;

/**
 *
 * @author exesilr
 */
public interface DetalleOperacionesClientes {

    public void setDataSource(DataSource ds);

    public CategoryModel getModelInformes(String nn);

    public boolean insertCliente_condonado_Det(DetalleCliente detCli);

    public boolean insertCliente_condonado_Enc(int rut, char dv, DetalleCliente detCli);

    public List<DetalleCliente> selectCliente_condonado(int rut, char dv);

    public List<DetalleCliente> Cliente(int rut, String user);

    public List<DetalleCliente> Cliente(int rut, String user, final int rechazo);
    public List<DetalleCliente> Cliente(int rut, String user, final int rechazo,String modulo);

    // public CategoryModel getModelInformes (String nn);
    public boolean insertDetSpOperacionesClientCondonado(int id_condonacion, int id_trakingInfoCliente);

}
