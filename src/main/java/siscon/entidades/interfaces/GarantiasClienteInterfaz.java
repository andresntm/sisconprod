/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.GarantiasCliente;

/**
 *
 * @author exesilr
 */
public interface GarantiasClienteInterfaz {
   public List<GarantiasCliente> GarantiasCliente(int rut,String usuario);
   public String SumaTotalGarantiasPesos();
}
