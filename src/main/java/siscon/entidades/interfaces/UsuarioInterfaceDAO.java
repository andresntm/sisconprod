/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import siscon.entidades.UsuarioPermiso;



/**
 *
 * @author exesilr
 */
public interface UsuarioInterfaceDAO {
    	/**login with account and password**/
	public boolean login(String cuenta, String password, String sistema);
	
	/**logout current user**/
	public void logout();
	
	/**get current user credential**/
	public UsuarioPermiso getUsuarioPermisos();
}
