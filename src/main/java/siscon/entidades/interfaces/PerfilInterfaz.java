/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Perfil;
import siscon.entidades.PerfilBancas;

/**
 *
 * @author excosoc
 */
public interface PerfilInterfaz {

    public int insertPerfil(final Perfil perfil);

    public boolean updatePerfil(final Perfil perfil);

    public Perfil getPerfilXId(final int id_perfil);

    public List<Perfil> getPerfil();
    public List<PerfilBancas> getPerfilALLBanca();
    public List<PerfilBancas> getPerfilXBanca(String perfil);

}
