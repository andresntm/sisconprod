/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.ReglasDet;
import siscon.entidades.ReglasEnc;

/**
 *
 * @author exesilr
 */
public interface ReglasInterfaz {
    // public Regla ReglasuSUARIO(int rut);

    public int insertReglasEnc(ReglasEnc reglaEnc);

    public int insertReglasDet(ReglasDet reglaDet);

    public boolean updateReglasEnc(ReglasEnc reglaEnc);

    public boolean updateReglasDet(ReglasDet reglaDet);

    public ReglasEnc getReglasXIdEnc(final int id_regla);

    public List<ReglasDet> getReglasDetXIdEnc(final int id_regla);

    public List<ReglasEnc> getReglasEnc();

    public List<ReglasDet> getReglasDet();

    public boolean inactivaRegla(final int id_Regla);

    public boolean getReglaDetSiNo(int id_Regla);

    public int insertUsrModifRegla(int id_colaborador, int id_relValRegla, int porcenjaAnterior, int porcentajeActual);

    public int insertUsrModifInteres(final int id_colaborador, final String operacion, final float InteresAnterior, final float NuevoInteres, int rutcliente);

    public int insertUsrAbonoCapital(final int id_colaborador, final int rutcliente, final float abono, final float montoMora, final float MontoCapital);

    public int insertUsrModifReglaDecimales(final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual);

    public int insertEditUsrModifReglaDecimales(int condonacion, final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual);
public int insertUsrModifReglaDecimalesCampana(final int id_colaborador, final int id_relValRegla, String porcenjaAnterior, String porcentajeActual,final int id_condonacion);
}
