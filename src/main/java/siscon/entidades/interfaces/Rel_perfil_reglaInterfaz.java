/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Rel_perfil_regla;

/**
 *
 * @author excosoc
 */
public interface Rel_perfil_reglaInterfaz {
    
    public int insertRel_perfil_regla(final Rel_perfil_regla rel_perfil_regla);

    public boolean updateRel_perfil_regla(final Rel_perfil_regla rel_perfil_regla);

    public Rel_perfil_regla getRel_perfil_reglaXId(final int id_perfil_regla);

    public List<Rel_perfil_regla> getRel_perfil_regla();
    
    public List<Rel_perfil_regla> getPerRegXId(int id_campania);
    
}
