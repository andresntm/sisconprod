/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.usuario;



/**
 *
 * @author exesilr
 */
public interface UsuarioDAO {
    
    
    
    
    	/** find user by account
     * @param account
     * @return  **/
	public usuario findUser(String account);
	
	/** update user
     * @param user
     * @return  **/
	public usuario updateUser(usuario user);
        
    public usuario buscarUsuarioRutDi(int rut);

    public usuario buscarUsuarioRutDv(String rut);

    public int buscarColaborador(String cuenta);

    public List<usuario> UsuariosPorPerfil(String CodPerfil);
    public List<usuario> UsuariosSinPerfil();
    public int guardaUsuarioEnPerfilds( final String aliass,final String rut,final String CodPerfil);
    public boolean BorrarUsuarioEnPerfilds(final String aliass, final String rut, final String CodPerfil);
    public usuario buscarUsuarioCuenta(String cuenta);
}
