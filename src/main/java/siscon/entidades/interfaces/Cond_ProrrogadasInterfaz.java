/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Cond_Prorrogadas;

/**
 *
 * @author excosoc
 */
public interface Cond_ProrrogadasInterfaz {

    public int insertProrroga(final Cond_Prorrogadas cP);

    public boolean updateProrroga(final Cond_Prorrogadas cP);

    public Cond_Prorrogadas getProrroga(final int id_Prorroga);

    public Cond_Prorrogadas getProrroga_X_Condonacion(int id_Condonacion);

    public List<Cond_Prorrogadas> listProrrogas();

    public List<Cond_Prorrogadas> listProrrogas_X_Colaborador(final int id_colaborador);

    public List<Cond_Prorrogadas> listProrrogas_X_TipoProrroga(final int id_TipoProrroga);

    public boolean deleteProrroga(final int id_Prorroga);

    public String getAlias_Prorrogador(int id_Condonacion);

    public boolean grabaGestionEjeProrroga(final Cond_Prorrogadas cR);

}
