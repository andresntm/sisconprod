/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import siscon.entidades.Cliente;

/**
 *
 * @author exesilr
 */
public interface ClienteInterfaz {

    public Cliente infocliente(int rut);

    public Cliente infocliente(int rut, String cuenta);

    public int insertDetSpClienteInfoCondonado(int id_condonacion);

    public int GetClienteCondonado(int id_condonacion);

    public String GetRutClienteCondonado(int id_cliente);

    public int GetIsClienteCampana(int rut);

    public String getClienteOferta(int rut);
}
