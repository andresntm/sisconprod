/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Tipo_Prorroga;

/**
 *
 * @author excosoc
 */
public interface Tipo_ProrrogaInterfaz {

    public int insertTipo_Prorroga(final Tipo_Prorroga tP);

    public boolean updateTipo_Prorroga(final Tipo_Prorroga tP);

    public Tipo_Prorroga getTipo_Prorroga(final int id_TipoProrroga);

    public List<Tipo_Prorroga> listTipo_Prorrogas();

    public boolean activa_InactivaTipo_Prorroga(final Tipo_Prorroga tP);

}
