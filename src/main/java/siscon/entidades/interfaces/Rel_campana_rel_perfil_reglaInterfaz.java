/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades.interfaces;

import java.util.List;
import siscon.entidades.Rel_campana_rel_perfil_regla;

/**
 *
 * @author excosoc
 */
public interface Rel_campana_rel_perfil_reglaInterfaz {
    
    public int insertRel_campana_rel_perfil_regla(final Rel_campana_rel_perfil_regla camPerRel);

    public boolean updateRel_campana_rel_perfil_regla(final Rel_campana_rel_perfil_regla camPerRel);

    public Rel_campana_rel_perfil_regla getRel_campana_rel_perfil_reglaXId(final int id);

    public List<Rel_campana_rel_perfil_regla> getRel_campana_rel_perfil_reglas();
    
    public boolean DeleteRel_campana_rel_perfil_regla(final Rel_campana_rel_perfil_regla camPerRel);
    
}
