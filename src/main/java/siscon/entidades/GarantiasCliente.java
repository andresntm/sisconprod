/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class GarantiasCliente {

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSub_tipo() {
        return sub_tipo;
    }

    public void setSub_tipo(String sub_tipo) {
        this.sub_tipo = sub_tipo;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getGarantia() {
        return Garantia;
    }

    public void setGarantia(String Garantia) {
        this.Garantia = Garantia;
    }

    public String getMoneda() {
        return Moneda;
    }

    public void setMoneda(String Moneda) {
        this.Moneda = Moneda;
    }

    public String getFecha_transaccion() {
        return fecha_transaccion;
    }

    public void setFecha_transaccion(String fecha_transaccion) {
        this.fecha_transaccion = fecha_transaccion;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        
        
        this.monto = monto;
        
       this.setMontoFloat(Float.parseFloat(monto));
        
        
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getDireccion_otro() {
        return Direccion_otro;
    }

    public void setDireccion_otro(String Direccion_otro) {
        this.Direccion_otro = Direccion_otro;
    }

    public String getComuna_rol() {
        return comuna_rol;
    }

    public void setComuna_rol(String comuna_rol) {
        this.comuna_rol = comuna_rol;
    }

    public String getRol_manzana() {
        return rol_manzana;
    }

    public void setRol_manzana(String rol_manzana) {
        this.rol_manzana = rol_manzana;
    }

    public String getRol_predio_prop() {
        return rol_predio_prop;
    }

    public void setRol_predio_prop(String rol_predio_prop) {
        this.rol_predio_prop = rol_predio_prop;
    }

    public String getRol_completo() {
        return rol_completo;
    }

    public void setRol_completo(String rol_completo) {
        this.rol_completo = rol_completo;
    }

    public String getAuto_modelo() {
        return auto_modelo;
    }

    public void setAuto_modelo(String auto_modelo) {
        this.auto_modelo = auto_modelo;
    }

    public String getAuto_patente() {
        return auto_patente;
    }

    public void setAuto_patente(String auto_patente) {
        this.auto_patente = auto_patente;
    }

    public GarantiasCliente(String rut, String tipo, String sub_tipo, String nombre_producto, String Garantia, String Moneda, String fecha_transaccion, String monto, String Direccion, String Direccion_otro, String comuna_rol, String rol_manzana, String rol_predio_prop, String rol_completo, String auto_modelo, String auto_patente) {
        this.rut = rut;
        this.tipo = tipo;
        this.sub_tipo = sub_tipo;
        this.nombre_producto = nombre_producto;
        this.Garantia = Garantia;
        this.Moneda = Moneda;
        this.fecha_transaccion = fecha_transaccion;
        this.monto = monto;
        this.Direccion = Direccion;
        this.Direccion_otro = Direccion_otro;
        this.comuna_rol = comuna_rol;
        this.rol_manzana = rol_manzana;
        this.rol_predio_prop = rol_predio_prop;
        this.rol_completo = rol_completo;
        this.auto_modelo = auto_modelo;
        this.auto_patente = auto_patente;
    }

    public GarantiasCliente() {
    }

    
    String rut;
    String tipo;
    String sub_tipo;
    String nombre_producto;
    String Garantia;
    String Moneda;
    String fecha_transaccion;
    String monto;
    String Direccion;
    String Direccion_otro;
    String comuna_rol;
    String rol_manzana;
    String rol_predio_prop;
    String rol_completo;
    String auto_modelo;
    String auto_patente;

    public String getMontoPesos() {
        return MontoPesos;
    }

    public void setMontoPesos(String MontoPesos) {
        this.MontoPesos = MontoPesos;
    }

    public float getMontoFloat() {
        return MontoFloat;
    }

    public void setMontoFloat(float MontoFloat) {
        this.MontoFloat = MontoFloat;
        this.setMontoSF(MontoFloat);
        
    }
    String MontoPesos;
    float MontoFloat;
public  ValorMonto MontoSF;
public  ValorMonto TotalMontoGarantias;



    public ValorMonto getMontoSF() {
        return MontoSF;
    }
    public String getMontoPesoS() {
        return MontoSF.getValorPesos();
    }
    public void setMontoSF(float MontoSF) {
        this.MontoSF=new ValorMonto(MontoSF);
    }
    
    
    
    


}
