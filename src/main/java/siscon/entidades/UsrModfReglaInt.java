/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class UsrModfReglaInt {
    
   private int id_usr_modf;     
   private int PorcentajeActual;
   private int PorcentajeAnterior;
   private float f_PorcentajeActual;
   private float f_PorcentajeAnterior;
    public float getF_PorcentajeActual() {
        return f_PorcentajeActual;
    }

    public void setF_PorcentajeActual(float f_PorcentajeActual) {
        this.f_PorcentajeActual = f_PorcentajeActual;
    }

    public float getF_PorcentajeAnterior() {
        return f_PorcentajeAnterior;
    }

    public void setF_PorcentajeAnterior(float f_PorcentajeAnterior) {
        this.f_PorcentajeAnterior = f_PorcentajeAnterior;
    }

   // String filian;
    
    
    
    public int getPorcentajeActual() {
        return PorcentajeActual;
    }

    public void setPorcentajeActual(int PorcentajeActual) {
        this.PorcentajeActual = PorcentajeActual;
    }

    public int getPorcentajeAnterior() {
        return PorcentajeAnterior;
    }

    public void setPorcentajeAnterior(int PorcentajeAnterior) {
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaInt(int PorcentajeActual, int PorcentajeAnterior) {
        this.PorcentajeActual = PorcentajeActual;
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaInt() {
        
        this.PorcentajeActual=0;
        this.PorcentajeAnterior=0;
        this.id_usr_modf=0;
    }



    public int getId_usr_modf() {
        return id_usr_modf;
    }

    public void setId_usr_modf(int id_usr_modf) {
        this.id_usr_modf = id_usr_modf;
    }

    
    
}
