/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import static siscore.comunes.LogController.SisCorelog;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.function.Consumer;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.implementaciones.ClienteSisconImplements;
import siscon.entidades.interfaces.ClienteSisconInterfaz;

/**
 *
 * @author exesilr
 */
public class ClienteSiscon implements RowMapper<ClienteSiscon> {

    /**
     * @return the regenera_Info
     */
    public boolean isRegenera_Info() {
        return regenera_Info;
    }

    /**
     * @param regenera_Info the regenera_Info to set
     */
    public void setRegenera_Info(boolean regenera_Info) {
        this.regenera_Info = regenera_Info;
    }

    /**
     * @return the fk_id_cedente
     */
    public int getFk_id_cedente() {
        return fk_id_cedente;
    }

    /**
     * @param fk_id_cedente the fk_id_cedente to set
     */
    public void setFk_id_cedente(int fk_id_cedente) {
        this.fk_id_cedente = fk_id_cedente;
    }

    private static final long serialVersionUID = 1L;

    private int id_cliente;
    private String NombreCompleto;
    private String Nombre;
    private String ApellidoPaterno;
    private String ApellidoMaterno;
    private String alias;
    private String rut;
    private int id_usuario;
    private int fk_id_cedente;
    private boolean regenera_Info;
    private ClienteSisconInterfaz cSItz;

    private void inicializa() {
        cSItz = new ClienteSisconImplements();
    }

    public ClienteSiscon() {
        this.regenera_Info = false;

        inicializa();
    }

    public ClienteSiscon(boolean reg) {
        this.regenera_Info = reg;

        inicializa();
    }

    /**
     * el campo regenera_Info se utiliza pasa saber si se buscara en la tabla
     * clientes de siscon o en producción para traer los datos faltantes
     *
     * @param id_cliente
     * @param NombreCompleto
     * @param Nombre
     * @param ApellidoPaterno
     * @param ApellidoMaterno
     * @param alias
     * @param rut
     * @param id_usuario
     * @param fk_id_cedente
     * @param regenera_Info
     */
    public ClienteSiscon(int id_cliente, String NombreCompleto, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String alias, String rut, int id_usuario, int fk_id_cedente, boolean regenera_Info) {
        this.id_cliente = id_cliente;
        this.NombreCompleto = NombreCompleto;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.alias = alias;
        this.rut = rut;
        this.id_usuario = id_usuario;
        this.fk_id_cedente = fk_id_cedente;
        this.regenera_Info = regenera_Info;

        inicializa();
    }

    /**
     * el campo regenera_Info se utiliza pasa saber si se buscara en la tabla
     * clientes de siscon o en producción para traer los datos faltantes
     *
     * @param id_cliente
     * @param NombreCompleto
     * @param Nombre
     * @param ApellidoPaterno
     * @param ApellidoMaterno
     * @param alias
     * @param rut
     * @param id_usuario
     * @param fk_id_cedente
     */
    public ClienteSiscon(int id_cliente, String NombreCompleto, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String alias, String rut, int id_usuario, int fk_id_cedente) {
        this.id_cliente = id_cliente;
        this.NombreCompleto = NombreCompleto;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.alias = alias;
        this.rut = rut;
        this.id_usuario = id_usuario;
        this.fk_id_cedente = fk_id_cedente;
        this.regenera_Info = false;
        inicializa();
    }

    /**
     * el campo regenera_Info se utiliza pasa saber si se buscara en la tabla
     * clientes de siscon o en producción para traer los datos faltantes
     *
     * @param id_cliente
     * @param NombreCompleto
     * @param Nombre
     * @param ApellidoPaterno
     * @param ApellidoMaterno
     * @param alias
     * @param rut
     * @param id_usuario
     * @param regenera_Info
     */
    public ClienteSiscon(int id_cliente, String NombreCompleto, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String alias, String rut, int id_usuario, boolean regenera_Info) {
        this.id_cliente = id_cliente;
        this.NombreCompleto = NombreCompleto;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.alias = alias;
        this.rut = rut;
        this.id_usuario = id_usuario;
        this.regenera_Info = regenera_Info;

        inicializa();
    }

    /**
     * el campo regenera_Info se utiliza pasa saber si se buscara en la tabla
     * clientes de siscon o en producción para traer los datos faltantes
     *
     * @param id_cliente
     * @param NombreCompleto
     * @param Nombre
     * @param ApellidoPaterno
     * @param ApellidoMaterno
     * @param alias
     * @param rut
     * @param id_usuario
     */
    public ClienteSiscon(int id_cliente, String NombreCompleto, String Nombre, String ApellidoPaterno, String ApellidoMaterno, String alias, String rut, int id_usuario) {
        this.id_cliente = id_cliente;
        this.NombreCompleto = NombreCompleto;
        this.Nombre = Nombre;
        this.ApellidoPaterno = ApellidoPaterno;
        this.ApellidoMaterno = ApellidoMaterno;
        this.alias = alias;
        this.rut = rut;
        this.id_usuario = id_usuario;

        regenera_Info = false;
        inicializa();
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String NombreCompleto) {
        this.NombreCompleto = NombreCompleto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String ApellidoPaterno) {
        this.ApellidoPaterno = ApellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String ApellidoMaterno) {
        this.ApellidoMaterno = ApellidoMaterno;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public ClienteSiscon mapRow(ResultSet rs, int rowNum) throws SQLException {
        ClienteSiscon cSiscon = new ClienteSiscon();

        cSiscon.setNombre(rs.getString("NOMBRE"));
        cSiscon.setNombreCompleto(rs.getString("NOMBRES"));
        cSiscon.setApellidoPaterno(rs.getString("APELLIDO_PATERNO"));
        cSiscon.setApellidoMaterno(rs.getString("APELLIDO_MATERNO"));
        cSiscon.setRut(rs.getString("RUT"));

        if (regenera_Info) {
            buscaCedente(rs.getString("CEDENTE"));
            cSiscon.setFk_id_cedente(this.fk_id_cedente);
        } else {
//            cSiscon.setFk_id_cedente(rs.getInt("ID_CEDENTE"));
            cSiscon.setId_cliente(rs.getInt("ID_CLIENTE"));
        }

        return cSiscon;
    }

    public void regeneraCliente() {
        List<ClienteSiscon> lCli = new ArrayList<ClienteSiscon>();
        lCli = listClientes();

        lCli.forEach(new Consumer<ClienteSiscon>() {
            public void accept(ClienteSiscon t) {
                cSItz.RegeneraCliente(t.getRut());
            }

        });

    }

    public List<ClienteSiscon> listClientes() {
        List<ClienteSiscon> lCli = new ArrayList<ClienteSiscon>();

        try {
            lCli = cSItz.listClientes();
        } catch (Exception ex) {
            Messagebox.show("Sr(a). usuario(a), no se pudo cargar la lista de clientes.\nError:" + ex.getMessage(), "Siscon-Administración", Messagebox.OK, Messagebox.EXCLAMATION);
            lCli = null;
        }
        return lCli;
    }

    public List<ClienteSiscon> listClientes_por_cedente() {
        List<ClienteSiscon> lCli = new ArrayList<ClienteSiscon>();

        try {
            lCli = cSItz.listClientes_por_cendete(this.fk_id_cedente);
        } catch (Exception ex) {
            Messagebox.show("Sr(a). usuario(a), no se pudo cargar la lista de clientes por cedente.\nError:" + ex.getMessage(), "Siscon-Administración", Messagebox.OK, Messagebox.EXCLAMATION);
            lCli = null;
        }
        return lCli;
    }

    public void getCliente() {
        ClienteSiscon cS = new ClienteSiscon();
        try {

            cS = cSItz.getCliente(this.id_cliente);

            this.id_cliente = cS.getId_cliente();
            this.NombreCompleto = cS.getNombreCompleto();
            this.Nombre = cS.getNombre();
            this.ApellidoPaterno = cS.getApellidoPaterno();
            this.ApellidoMaterno = cS.getApellidoMaterno();
            this.alias = cS.getAlias();
            this.rut = cS.getRut();
            this.id_usuario = cS.getId_usuario();

        } catch (Exception ex) {
            Messagebox.show("Sr(a). usuario(a), no se pudo cargar el registro del cliente.\nError:" + ex.getMessage(), "Siscon-Administración", Messagebox.OK, Messagebox.EXCLAMATION);

        }

    }

    private void buscaCedente(String cedente) {
        int id_cedente;

        try {
            id_cedente = cSItz.traeIdCedenteCliente(cedente);
        } catch (Exception ex) {
            SisCorelog("No se encontro el cedente.");
            id_cedente = 0;
        }
        this.fk_id_cedente = id_cedente;
    }

}
