/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.Date;

/**
 *
 * @author exesilr
 */
public class usuario {

    /**
     * @return the areaTrabajo
     */
    public String getAreaTrabajo() {
        return areaTrabajo;
    }

    /**
     * @param areaTrabajo the areaTrabajo to set
     */
    public void setAreaTrabajo(String areaTrabajo) {
        this.areaTrabajo = areaTrabajo;
    }

    /**
     * @return the id_colaborador
     */
    public int getId_colaborador() {
        return id_colaborador;
    }

    /**
     * @param id_colaborador the id_colaborador to set
     */
    public void setId_colaborador(int id_colaborador) {
        this.id_colaborador = id_colaborador;
    }

    private static final long serialVersionUID = 1L;

    public void setAccount(String account) {
        this.account = account;
    }

    private String account;
    private String fullName;
    private String password;
    private String email;
    private Date birthday;
    private String country;
    private String bio;
    private String alias;
    private int id_colaborador;
    private String dv_rut;
    private String areaTrabajo;
    private int di_rut;

    public usuario() {
    }

    public usuario(String account, String password, String fullName, String email, String ubic) {
        this.account = account;
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.ubicacion = ubic;
    }

    public usuario(String account, String password, String fullName, String email, String ubic, String alias) {
        this.account = account;
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.ubicacion = ubic;
        this.alias = alias;
    }

    public String getDv_rut() {
        return dv_rut;
    }

    public void setDv_rut(String dv_rut) {
        this.dv_rut = dv_rut;
    }

    public int getDi_rut() {
        return di_rut;
    }

    public void setDi_rut(int di_rut) {
        this.di_rut = di_rut;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    String ubicacion;

    public String getAccount() {
        return account;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((account == null) ? 0 : account.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        usuario other = (usuario) obj;
        if (account == null) {
            if (other.account != null) {
                return false;
            }
        } else if (!account.equals(other.account)) {
            return false;
        }
        return true;
    }

    public static usuario clone(usuario user) {
        try {
            return (usuario) user.clone();
        } catch (CloneNotSupportedException e) {
            //not possible
        }
        return null;
    }
}
