/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;


public class CondReparadas {

    //  private static final long serialVersionUID = 1L;
    //   @Id
    //   @GeneratedValue(strategy = GenerationType.AUTO)
    //   private Long id;
    public CondReparadas(String rut, String nombre, int id_condonacion, String timestap, String dv_estado, String COMENTARIO_RESNA, float MONTO_TOTAL_CONDONADO, float MONTO_TOTAL_RECIBIT, int DI_NUM_OPERS, float MONTO_TOTAL_CAPITAL, int di_fk_idColadorador, String fld_ced) {
        this.rut = rut;
        this.nombre = nombre;
        this.id_condonacion = id_condonacion;
        this.timestap = timestap;
        this.dv_estado = dv_estado;
        this.COMENTARIO_RESNA = COMENTARIO_RESNA;
        this.MONTO_TOTAL_CONDONADO = MONTO_TOTAL_CONDONADO;
        this.MONTO_TOTAL_RECIBIT = MONTO_TOTAL_RECIBIT;
        this.DI_NUM_OPERS = DI_NUM_OPERS;
        this.MONTO_TOTAL_CAPITAL = MONTO_TOTAL_CAPITAL;
        this.di_fk_idColadorador = di_fk_idColadorador;
        this.fld_ced = fld_ced;
    }

    public CondReparadas() {
    }
    String rut;
    String nombre;
    int id_condonacion;
    String timestap;
    String dv_estado;
    String COMENTARIO_RESNA;
    float MONTO_TOTAL_CONDONADO;
    float MONTO_TOTAL_RECIBIT;
    int DI_NUM_OPERS;
    float MONTO_TOTAL_CAPITAL;
    int di_fk_idColadorador;
    String fld_ced;
   
    public int getDi_fk_idColadorador() {
        return di_fk_idColadorador;
    }

    public void setDi_fk_idColadorador(int di_fk_idColadorador) {
        this.di_fk_idColadorador = di_fk_idColadorador;
    }

    public String getFld_ced() {
        return fld_ced;
    }

    public void setFld_ced(String fld_ced) {
        this.fld_ced = fld_ced;
    }


    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getTimestap() {
        return timestap;
    }

    public void setTimestap(String timestap) {
        this.timestap = timestap;
    }

    public String getDv_estado() {
        return dv_estado;
    }

    public void setDv_estado(String dv_estado) {
        this.dv_estado = dv_estado;
    }

    public String getCOMENTARIO_RESNA() {
        return COMENTARIO_RESNA;
    }

    public void setCOMENTARIO_RESNA(String COMENTARIO_RESNA) {
        this.COMENTARIO_RESNA = COMENTARIO_RESNA;
    }

    public float getMONTO_TOTAL_CONDONADO() {
        return MONTO_TOTAL_CONDONADO;
    }

    public void setMONTO_TOTAL_CONDONADO(float MONTO_TOTAL_CONDONADO) {
        this.MONTO_TOTAL_CONDONADO = MONTO_TOTAL_CONDONADO;
    }

    public float getMONTO_TOTAL_RECIBIT() {
        return MONTO_TOTAL_RECIBIT;
    }

    public void setMONTO_TOTAL_RECIBIT(float MONTO_TOTAL_RECIBIT) {
        this.MONTO_TOTAL_RECIBIT = MONTO_TOTAL_RECIBIT;
    }

    public int getDI_NUM_OPERS() {
        return DI_NUM_OPERS;
    }

    public void setDI_NUM_OPERS(int DI_NUM_OPERS) {
        this.DI_NUM_OPERS = DI_NUM_OPERS;
    }

    public float getMONTO_TOTAL_CAPITAL() {
        return MONTO_TOTAL_CAPITAL;
    }

    public void setMONTO_TOTAL_CAPITAL(float MONTO_TOTAL_CAPITAL) {
        this.MONTO_TOTAL_CAPITAL = MONTO_TOTAL_CAPITAL;
    }

   
    
}
