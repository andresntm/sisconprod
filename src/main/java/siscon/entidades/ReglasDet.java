/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class ReglasDet {

    private int id_valor_regla;
    private int id_valor;
    private int id_regla;
    private float porcentaje_condonacion;

    /**
     * @return the id_valor_regla
     */
    public int getId_valor_regla() {
        return id_valor_regla;
    }

    /**
     * @param id_valor_regla the id_valor_regla to set
     */
    public void setId_valor_regla(int id_valor_regla) {
        this.id_valor_regla = id_valor_regla;
    }

    /**
     * @return the id_valor
     */
    public int getId_valor() {
        return id_valor;
    }

    /**
     * @param id_valor the id_valor to set
     */
    public void setId_valor(int id_valor) {
        this.id_valor = id_valor;
    }

    /**
     * @return the id_regla
     */
    public int getId_regla() {
        return id_regla;
    }

    /**
     * @param id_regla the id_regla to set
     */
    public void setId_regla(int id_regla) {
        this.id_regla = id_regla;
    }

    /**
     * @return the porcentaje_condonacion
     */
    public float getPorcentaje_condonacion() {
        return porcentaje_condonacion;
    }

    /**
     * @param porcentaje_condonacion the porcentaje_condonacion to set
     */
    public void setPorcentaje_condonacion(float porcentaje_condonacion) {
        this.porcentaje_condonacion = porcentaje_condonacion;
    }

    public ReglasDet() {
    }

    public ReglasDet(int id_valor_regla,
            int id_valor,
            int id_regla,
            float porcentaje_condonacion) {

        this.id_valor_regla = id_valor_regla;
        this.id_valor = id_valor;
        this.id_regla = id_regla;
        this.porcentaje_condonacion = porcentaje_condonacion;

    }
}
