/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author excosoc
 */
public class GlosaENC {

    /**
     * @return the id_reparo
     */
    public int getId_reparo() {
        return id_reparo;
    }

    /**
     * @param id_reparo the id_reparo to set
     */
    public void setId_reparo(int id_reparo) {
        this.id_reparo = id_reparo;
    }

    /**
     * @return the detalle
     */
    public List<GlosaDET> getDetalle() {
        return detalle;
    }

    /**
     * @param detalle the detalle to set
     */
    public void setDetalle(List<GlosaDET> detalle) {
        this.detalle = detalle;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_client
     */
    public int getId_client() {
        return id_client;
    }

    /**
     * @param id_client the id_client to set
     */
    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    /**
     * @return the id_cond
     */
    public int getId_cond() {
        return id_cond;
    }

    /**
     * @param id_cond the id_cond to set
     */
    public void setId_cond(int id_cond) {
        this.id_cond = id_cond;
    }

    /**
     * @return the cant_glosas
     */
    public int getCant_glosas() {
        return cant_glosas;
    }

    /**
     * @param cant_glosas the cant_glosas to set
     */
    public void setCant_glosas(int cant_glosas) {
        this.cant_glosas = cant_glosas;
    }

    private int id;
    private int id_client;
    private int id_cond;
    private int cant_glosas;
    private int id_reparo;
    private List<GlosaDET> detalle = new ArrayList<GlosaDET>();

    public GlosaENC() {
    }

    /**
     *
     * @param id int
     * @param id_client int
     * @param id_cond int
     * @param cant_glosas int
     * @param detalle List()
     * @param id_reparo int
     */
    public GlosaENC(int id,
            int id_client,
            int id_cond,
            int cant_glosas,
            List<GlosaDET> detalle,
            int id_reparo) {
        this.id = id;
        this.id_client = id_client;
        this.id_cond = id_cond;
        this.cant_glosas = cant_glosas;
        this.detalle = detalle;
        this.id_reparo = id_reparo;

    }

    /**
     *
     * @param id int
     * @param id_client int
     * @param id_cond int
     * @param cant_glosas int
     * @param id_reparo int
     */
    public GlosaENC(int id,
            int id_client,
            int id_cond,
            int cant_glosas,
            int id_reparo) {
        this.id = id;
        this.id_client = id_client;
        this.id_cond = id_cond;
        this.cant_glosas = cant_glosas;
        this.id_reparo = id_reparo;
    }

    /**
     *
     * @param id int
     * @param id_client int
     * @param id_cond int
     * @param cant_glosas int
     */
    public GlosaENC(int id,
            int id_client,
            int id_cond,
            int cant_glosas) {
        this.id = id;
        this.id_client = id_client;
        this.id_cond = id_cond;
        this.cant_glosas = cant_glosas;
    }

}
