/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.RowMapper;
import siscon.Session.TrackingSesion;
import siscon.entidades.implementaciones.CondEspecialesImplements;
import siscon.entidades.interfaces.CondEspecialesInterfaz;

/**
 *
 * @author excosoc
 */
public class TipoCondEspecial implements RowMapper<TipoCondEspecial> {

    /**
     * @return the id_CondEspecial
     */
    public int getId_CondEspecial() {
        return id_CondEspecial;
    }

    /**
     * @param id_CondEspecial the id_CondEspecial to set
     */
    public void setId_CondEspecial(int id_CondEspecial) {
        this.id_CondEspecial = id_CondEspecial;
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Descripcion
     */
    public String getDescripcion() {
        return Descripcion;
    }

    /**
     * @param Descripcion the Descripcion to set
     */
    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    /**
     * @return the Detalle
     */
    public String getDetalle() {
        return Detalle;
    }

    /**
     * @param Detalle the Detalle to set
     */
    public void setDetalle(String Detalle) {
        this.Detalle = Detalle;
    }

    /**
     * @return the FechaIngreso
     */
    public Timestamp getFechaIngreso() {
        return FechaIngreso;
    }

    /**
     * @param FechaIngreso the FechaIngreso to set
     */
    public void setFechaIngreso(Timestamp FechaIngreso) {
        this.FechaIngreso = FechaIngreso;
    }

    private int id_CondEspecial;
    private String Codigo;
    private String Descripcion;
    private String Detalle;
    private Timestamp FechaIngreso;
    private CondEspecialesInterfaz cEI;

    public TipoCondEspecial() {
        this.setcEI();
    }

    public TipoCondEspecial(int id_CondEspecial, String Codigo, String Descripcion, String Detalle, Timestamp FechaIngreso) {
        this.id_CondEspecial = id_CondEspecial;
        this.Codigo = Codigo;
        this.Descripcion = Descripcion;
        this.Detalle = Detalle;
        this.FechaIngreso = FechaIngreso;
        this.setcEI();
    }

    public TipoCondEspecial mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoCondEspecial tipCondEsp = new TipoCondEspecial();

        tipCondEsp.id_CondEspecial = rs.getInt("id_CondEspecial");
        tipCondEsp.Codigo = rs.getString("Codigo");
        tipCondEsp.Descripcion = rs.getString("Descripcion");
        tipCondEsp.Detalle = rs.getString("Detalle");
        tipCondEsp.FechaIngreso = rs.getTimestamp("FechaIngreso");

        return tipCondEsp;
    }

    /**
     * @return the cEI
     */
    public CondEspecialesInterfaz getcEI() {
        return cEI;
    }

    /**
     * @param cEI the cEI to set
     */
    public void setcEI() {
        this.cEI = new CondEspecialesImplements();
    }

    public boolean insertTipoCondEspecial(TipoCondEspecial tCE) {
        boolean ok = false;

        try {
            tCE.setId_CondEspecial(cEI.insertTipCondEspecial(tCE));

            ok = tCE.getId_CondEspecial() != 0;
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            ok = false;
        } finally {
            return ok;
        }

    }

    public TipoCondEspecial getTipoCondEspecial_X_Id(int id) {
        TipoCondEspecial tCE = null;
        try {
            tCE = cEI.selectTipCondEspecial_X_Id_TipCondEsp(id);
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            tCE = null;
        } finally {
            return tCE;
        }

    }

    public List<TipoCondEspecial> listTipoCondEspecial() {
        List<TipoCondEspecial> lTCE = null;
        try {

            lTCE = cEI.listTipCondEspecial();

        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            lTCE = null;
        } finally {
            return lTCE;
        }

    }

    public List<TipoCondEspecial> listTipoCondEspecialEnUso() {
        List<TipoCondEspecial> lTCE = null;
        try {

            lTCE = cEI.listTipCondEspecial_Usadas();

        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            lTCE = null;
        } finally {
            return lTCE;
        }

    }
}
