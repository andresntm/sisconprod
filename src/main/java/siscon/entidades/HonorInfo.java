/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author exesilr
 */
public class HonorInfo {

    
    String operacion_ori;
    String operacion_2;
    String FormulaCalculo;
    String Prejudicial;
    String HonorarioTotal;
    float SaldoIndolutoTotal;
    String SaldoIndolutoTotalPesos;
    String ReglaDeCalculo;
      NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());
      
      
      
        public String getReglaDeCalculo() {
        return ReglaDeCalculo;
    }

    public void setReglaDeCalculo(String ReglaDeCalculo) {
        this.ReglaDeCalculo = ReglaDeCalculo;
    }  
    public float getSaldoIndolutoTotal() {
        return SaldoIndolutoTotal;
    }

    public void setSaldoIndolutoTotal(float SaldoIndolutoTotal) {
        this.SaldoIndolutoTotal = SaldoIndolutoTotal;
        this.SaldoIndolutoTotalPesos=nff.format(SaldoIndolutoTotal).replaceFirst("Ch", "");
    }

    public String getSaldoIndolutoTotalPesos() {
        return SaldoIndolutoTotalPesos;
    }

    public void setSaldoIndolutoTotalPesos(String SaldoIndolutoTotalPesos) {
        this.SaldoIndolutoTotalPesos = SaldoIndolutoTotalPesos;
    }
    
    
    
    
    
    
    
    public String getOperacion_ori() {
        return operacion_ori;
    }

    public void setOperacion_ori(String operacion_ori) {
        this.operacion_ori = operacion_ori;
    }

    public String getOperacion_2() {
        return operacion_2;
    }

    public void setOperacion_2(String operacion_2) {
        this.operacion_2 = operacion_2;
    }

    public String getFormulaCalculo() {
        return FormulaCalculo;
    }

    public void setFormulaCalculo(String FormulaCalculo) {
        this.FormulaCalculo = FormulaCalculo;
    }

    public String getPrejudicial() {
        return Prejudicial;
    }

    public void setPrejudicial(String Prejudicial) {
        this.Prejudicial = Prejudicial;
    }

    public String getHonorarioTotal() {
        return HonorarioTotal;
    }

    public void setHonorarioTotal(String HonorarioTotal) {
        this.HonorarioTotal = HonorarioTotal;
    }






    

    public HonorInfo() {
    }

 
    


}
