/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class SacaBop {

    public ValorMonto HonoJudsobreCondonado;
    private String CapitalARecibirPesos;
    private long HonorarioJuduciaRecibir;
    private String cedente;
    private String operacion;
    private String detori;
    private String TipoCedente;
    private String DetalleCredito;
    private float Mora;
    private float saldoinsoluto;
    private int CuotasMora;
    private int Cuotaspagada;
    private int CuotasPactada;
    private int DiasMora;
    private String Productos;
    private String FechaFencimiento;
    private String FechaCastigo;
    private String MarcaRenegociado;
    private int NumeroDemandas;
    private String MesesCastigo;
    private String Capital;
    private String Interes;
    private String HonorarioJudicial;
    private float HonorarioJuducial;
    private float MontoCondonar;
    private float MontoARecibir;
    private float CapitalCondonado;
    private float CapitalARecibir;
    private float HonorarioJuducialCondonado;
    private float Porcentajecondonar;
    private float valorUF;
    private String MontoCondonarPesos;
    private float InteresCondonado;
    private float InteresARecibir;

    private String InteresCondonadoPesos;
    private String InteresARecibirPesos;
    private String HonorarioJudicialPesos;
    private String HonorarioJudicialCondonadoPesos;
    private String HonorarioJudicialRecibidoPesos;
    private String HonJudCondSobreCapitalPesos;
    private String HonJudRecSobreCapitalPesos;
    private float HONOR_F1;

    public HonorInfo getHonInf() {
        return _HonInf;
    }

    public void setHonInf(HonorInfo _HonInf) {
        this._HonInf = _HonInf;
    }
    public HonorInfo _HonInf;

    public SacaBop() {
        this.HONOR_F1=0;
        _HonInf=new HonorInfo();
    }
    
    
    
    
    public float getHONOR_F1() {
        return HONOR_F1;
    }

    public void setHONOR_F1(float HONOR_F1) {
        this.HONOR_F1 = HONOR_F1;
    }
    
   
    
    
    
    
    
    
    
    public String getHonJudCondSobreCapitalPesos() {
        return HonJudCondSobreCapitalPesos;
    }

    public void setHonJudCondSobreCapitalPesos(String HonJudCondSobreCapitalPesos) {
        this.HonJudCondSobreCapitalPesos = HonJudCondSobreCapitalPesos;
    }

    public String getHonJudRecSobreCapitalPesos() {
        return HonJudRecSobreCapitalPesos;
    }

    public void setHonJudRecSobreCapitalPesos(String HonJudRecSobreCapitalPesos) {
        this.HonJudRecSobreCapitalPesos = HonJudRecSobreCapitalPesos;
    }

    // private  ValorMonto HonoJudsobreCondonado; 

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getDetori() {
        return detori;
    }

    public void setDetori(String detori) {
        this.detori = detori;
    }

    public String getTipoCedente() {
        return TipoCedente;
    }

    public void setTipoCedente(String TipoCedente) {
        this.TipoCedente = TipoCedente;
    }

    public String getDetalleCredito() {
        return DetalleCredito;
    }

    public void setDetalleCredito(String DetalleCredito) {
        this.DetalleCredito = DetalleCredito;
    }

    public float getMora() {
        return Mora;
    }

    public void setMora(long Mora) {
        this.Mora = Mora;
    }

    public float getSaldoinsoluto() {
        return saldoinsoluto;
    }

    public void setSaldoinsoluto(long saldoinsoluto) {
        this.saldoinsoluto = saldoinsoluto;
    }

    public int getCuotasMora() {
        return CuotasMora;
    }

    public void setCuotasMora(int CuotasMora) {
        this.CuotasMora = CuotasMora;
    }

    public int getCuotaspagada() {
        return Cuotaspagada;
    }

    public void setCuotaspagada(int Cuotaspagada) {
        this.Cuotaspagada = Cuotaspagada;
    }

    public int getCuotasPactada() {
        return CuotasPactada;
    }

    public void setCuotasPactada(int CuotasPactada) {
        this.CuotasPactada = CuotasPactada;
    }

    public int getDiasMora() {
        return DiasMora;
    }

    public void setDiasMora(int DiasMora) {
        this.DiasMora = DiasMora;
    }

    public String getProductos() {
        return Productos;
    }

    public void setProductos(String Productos) {
        this.Productos = Productos;
    }

    public String getFechaFencimiento() {
        return FechaFencimiento;
    }

    public void setFechaFencimiento(String FechaFencimiento) {
        this.FechaFencimiento = FechaFencimiento;
    }

    public String getFechaCastigo() {
        return FechaCastigo;
    }

    public void setFechaCastigo(String FechaCastigo) {
        this.FechaCastigo = FechaCastigo;
    }

    public String getMarcaRenegociado() {
        return MarcaRenegociado;
    }

    public void setMarcaRenegociado(String MarcaRenegociado) {
        this.MarcaRenegociado = MarcaRenegociado;
    }

    public int getNumeroDemandas() {
        return NumeroDemandas;
    }

    public void setNumeroDemandas(int NumeroDemandas) {
        this.NumeroDemandas = NumeroDemandas;
    }

    public String getMesesCastigo() {
        return MesesCastigo;
    }

    public void setMesesCastigo(String MesesCastigo) {
        this.MesesCastigo = MesesCastigo;
    }

    public String getCapital() {
        return Capital;
    }

    public void setCapital(String Capital) {
        this.Capital = Capital;
    }

    public String getInteres() {
        return Interes;
    }

    public void setInteres(String Interes) {
        this.Interes = Interes;
    }

    public String getHonorarioJudicial() {
        return HonorarioJudicial;
    }

    public void setHonorarioJudicial(String HonorarioJudicial) {
        this.HonorarioJudicial = HonorarioJudicial;
    }

    public float getHonorarioJuducial() {
        return HonorarioJuducial;
    }

    public void setHonorarioJuducial(float HonorarioJuducial) {
        this.HonorarioJuducial = HonorarioJuducial;
    }

    public float getMontoCondonar() {
        return MontoCondonar;
    }

    public void setMontoCondonar(float MontoCondonar) {
        this.MontoCondonar = MontoCondonar;
    }

    public float getMontoARecibir() {
        return MontoARecibir;
    }

    public void setMontoARecibir(float MontoARecibir) {
        this.MontoARecibir = MontoARecibir;
    }

    public float getCapitalCondonado() {
        return CapitalCondonado;
    }

    public void setCapitalCondonado(float CapitalCondonado) {
        this.CapitalCondonado = CapitalCondonado;
        //return 0;
    }

    public float getCapitalARecibir() {
        return CapitalARecibir;
    }

    public void setCapitalARecibir(float CapitalARecibir) {
        this.CapitalARecibir = CapitalARecibir;
    }

    public float getHonorarioJuducialCondonado() {
        return HonorarioJuducialCondonado;
    }

    public void setHonorarioJuducialCondonado(float HonorarioJuducialCondonado) {
        this.HonorarioJuducialCondonado = HonorarioJuducialCondonado;
    }

    public float getPorcentajecondonar() {
        return Porcentajecondonar;
    }

    public void setPorcentajecondonar(float Porcentajecondonar) {
        this.Porcentajecondonar = Porcentajecondonar;
    }

    public float getValorUF() {
        return valorUF;
    }

    public void setValorUF(long valorUF) {
        this.valorUF = valorUF;
    }

    public ValorMonto getHonorarioJudicial2() {
        return this.HonoJudsobreCondonado;
    }

    public void setHonorarioJudicial2(double monto) {
        this.HonoJudsobreCondonado = new ValorMonto(monto);
    }

    public String getHonorarioJudicial2Pesos() {

        return this.HonoJudsobreCondonado.getValorPesos();

    }

    public double getHonorarioJudicial2Float() {

        return this.HonoJudsobreCondonado.getValor();

    }

    public String getHonorarioJudicialPesos() {
        return HonorarioJudicialPesos;
    }

    public void setHonorarioJudicialPesos(String HonorarioJudicialPesos) {
        this.HonorarioJudicialPesos = HonorarioJudicialPesos;
    }

    public String getHonorarioJudicialCondonadoPesos() {
        return HonorarioJudicialCondonadoPesos;
    }

    public void setHonorarioJudicialCondonadoPesos(String HonorarioJudicialCondonadoPesos) {
        this.HonorarioJudicialCondonadoPesos = HonorarioJudicialCondonadoPesos;
    }

    public String getHonorarioJudicialRecibidoPesos() {
        return HonorarioJudicialRecibidoPesos;
    }

    public void setHonorarioJudicialRecibidoPesos(String HonorarioJudicialRecibidoPesos) {
        this.HonorarioJudicialRecibidoPesos = HonorarioJudicialRecibidoPesos;
    }

    public String getInteresCondonadoPesos() {
        return InteresCondonadoPesos;
    }

    public void setInteresCondonadoPesos(String InteresCondonadoPesos) {
        this.InteresCondonadoPesos = InteresCondonadoPesos;
    }

    public String getInteresARecibirPesos() {
        return InteresARecibirPesos;
    }

    public void setInteresARecibirPesos(String InteresARecibirPesos) {
        this.InteresARecibirPesos = InteresARecibirPesos;
    }

    public String getCapitalARecibirPesos() {
        return CapitalARecibirPesos;
    }

    public void setCapitalARecibirPesos(String CapitalARecibirPesos) {
        this.CapitalARecibirPesos = CapitalARecibirPesos;
    }

    public String getMontoCondonarPesos() {
        return MontoCondonarPesos;
    }

    public void setMontoCondonarPesos(String MontoCondonarPesos) {
        this.MontoCondonarPesos = MontoCondonarPesos;
    }

    public float getInteresCondonado() {
        return InteresCondonado;
    }

    public void setInteresCondonado(long InteresCondonado) {
        this.InteresCondonado = InteresCondonado;
    }

    public float getInteresARecibir() {
        return InteresARecibir;
    }

    public void setInteresARecibir(long InteresARecibir) {
        this.InteresARecibir = InteresARecibir;
    }

    public long getHonorarioJuduciaRecibir() {
        return HonorarioJuduciaRecibir;
    }

    public void setHonorarioJuduciaRecibir(long HonorarioJuduciaRecibir) {
        this.HonorarioJuduciaRecibir = HonorarioJuduciaRecibir;
    }

}
