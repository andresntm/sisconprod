/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class Rel_perfil_regla {

    private int id_perfil_regla;
    private int id_perfil;
    private int id_regla;

    /**
     * @return the id_perfil_regla
     */
    public int getId_perfil_regla() {
        return id_perfil_regla;
    }

    /**
     * @param id_perfil_regla the id_perfil_regla to set
     */
    public void setId_perfil_regla(int id_perfil_regla) {
        this.id_perfil_regla = id_perfil_regla;
    }

    /**
     * @return the id_perfil
     */
    public int getId_perfil() {
        return id_perfil;
    }

    /**
     * @param id_perfil the id_perfil to set
     */
    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    /**
     * @return the id_regla
     */
    public int getId_regla() {
        return id_regla;
    }

    /**
     * @param id_regla the id_regla to set
     */
    public void setId_regla(int id_regla) {
        this.id_regla = id_regla;
    }

    public Rel_perfil_regla() {
    }

    public Rel_perfil_regla(int id_perfil_regla,
            int id_perfil,
            int id_regla) {

        this.id_perfil_regla = id_perfil_regla;
        this.id_perfil = id_perfil;
        this.id_regla = id_regla;

    }

}
