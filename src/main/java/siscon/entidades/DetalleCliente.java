/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class DetalleCliente {

    public DetalleCliente() {
        this.FechaCastigo = "";
        this.detori = "";
    }

    private String cedente;
    private String operacion;
    private String detori;
    private String TipoCedente;
    private String DetalleCredito;
    private long Mora;
    private long saldoinsoluto;
    private int CuotasMora;
    private int Cuotaspagada;
    private int CuotasPactada;
    private int DiasMora;
    private String Productos;
    private String FechaFencimiento;
    private String FechaCastigo;
    private String MarcaRenegociado;
    private int NumeroDemandas;
    private int fk_id_Cli_Cond_Enc;
    private int id_cli_Cond_Det;
    private boolean condona;
    private String FLD_MOR;
    private String FLD_SDO;
    private String FLD_DET_TIP_CRE_MIN;
    private String PAGADAS;
    private String TOTAL_CUOTAS;
    private String NRO_DEMANDAS;
    float honorarioJudicial;
    
    
    
    
    public String getDetori() {
        return detori;
    }

    public void setDetori(String detori) {
        this.detori = detori;
    }

    public String getFLD_MOR() {
        return FLD_MOR;
    }

    public void setFLD_MOR(String FLD_MOR) {
        this.FLD_MOR = FLD_MOR;
    }

    public String getFLD_SDO() {
        return FLD_SDO;
    }

    public void setFLD_SDO(String FLD_SDO) {
        this.FLD_SDO = FLD_SDO;
    }

    public String getFLD_DET_TIP_CRE_MIN() {
        return FLD_DET_TIP_CRE_MIN;
    }

    public void setFLD_DET_TIP_CRE_MIN(String FLD_DET_TIP_CRE_MIN) {
        this.FLD_DET_TIP_CRE_MIN = FLD_DET_TIP_CRE_MIN;
    }

    public String getPAGADAS() {
        return PAGADAS;
    }

    public void setPAGADAS(String PAGADAS) {
        this.PAGADAS = PAGADAS;
    }

    public String getTOTAL_CUOTAS() {
        return TOTAL_CUOTAS;
    }

    public void setTOTAL_CUOTAS(String TOTAL_CUOTAS) {
        this.TOTAL_CUOTAS = TOTAL_CUOTAS;
    }

    public String getNRO_DEMANDAS() {
        return NRO_DEMANDAS;
    }

    public void setNRO_DEMANDAS(String NRO_DEMANDAS) {
        this.NRO_DEMANDAS = NRO_DEMANDAS;
    }

    
    public DetalleCliente(String cedente, String operacion,
            String detori,
            String TipoCedente,
            String DetalleCredito,
            long Mora,
            long saldoinsoluto,
            int CuotasMora,
            int Cuotaspagada,
            int CuotasPactada,
            int DiasMora,
            String Productos,
            String FechaFencimiento,
            String FechaCastigo,
            String MarcaRenegociado,
            int NumeroDemandas,
            String MoraEnPesosChileno,
            String SaldoEnPesosChileno,
            int fk_id_Cli_Cond_Enc,
            int id_cli_Cond_Det) {
        this.cedente = cedente;
        this.operacion = operacion;
        this.detori = detori;
        this.TipoCedente = TipoCedente;
        this.DetalleCredito = DetalleCredito;
        this.Mora = Mora;
        this.saldoinsoluto = saldoinsoluto;
        this.CuotasMora = CuotasMora;
        this.Cuotaspagada = Cuotaspagada;
        this.CuotasPactada = CuotasPactada;
        this.DiasMora = DiasMora;
        this.Productos = Productos;
        this.FechaFencimiento = FechaFencimiento;
        this.FechaCastigo = FechaCastigo;
        this.MarcaRenegociado = MarcaRenegociado;
        this.NumeroDemandas = NumeroDemandas;
        this.MoraEnPesosChileno = MoraEnPesosChileno;
        this.SaldoEnPesosChileno = SaldoEnPesosChileno;
        this.fk_id_Cli_Cond_Enc = fk_id_Cli_Cond_Enc;
        this.id_cli_Cond_Det = id_cli_Cond_Det;
    }
    private String MoraEnPesosChileno;
    private String SaldoEnPesosChileno;

    public String getMoraEnPesosChileno() {
        return MoraEnPesosChileno;
    }

    public void setMoraEnPesosChileno(String MoraEnPesosChileno) {
        this.MoraEnPesosChileno = MoraEnPesosChileno;
    }

    public String getSaldoEnPesosChileno() {
        return SaldoEnPesosChileno;
    }

    public void setSaldoEnPesosChileno(String SaldoEnPesosChileno) {
        this.SaldoEnPesosChileno = SaldoEnPesosChileno;
    }

    /**
     * @return the fk_id_Cli_Cond_Enc
     */
    public int getFk_id_Cli_Cond_Enc() {
        return fk_id_Cli_Cond_Enc;
    }

    /**
     * @param fk_id_Cli_Cond_Enc the fk_id_Cli_Cond_Enc to set
     */
    public void setFk_id_Cli_Cond_Enc(int fk_id_Cli_Cond_Enc) {
        this.fk_id_Cli_Cond_Enc = fk_id_Cli_Cond_Enc;
    }

    /**
     * @return the id_cli_Cond_Det
     */
    public int getId_cli_Cond_Det() {
        return id_cli_Cond_Det;
    }

    /**
     * @param id_cli_Cond_Det the id_cli_Cond_Det to set
     */
    public void setId_cli_Cond_Det(int id_cli_Cond_Det) {
        this.id_cli_Cond_Det = id_cli_Cond_Det;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getOperacionOriginal() {
        return detori;
    }

    public void setOperacionOriginal(String OperacionOriginal) {
        this.detori = OperacionOriginal;
    }

    public String getTipoCedente() {
        return TipoCedente;
    }

    public void setTipoCedente(String TipoCedente) {
        this.TipoCedente = TipoCedente;
    }

    public String getDetalleCredito() {
        return DetalleCredito;
    }

    public void setDetalleCredito(String DetalleCredito) {
        this.DetalleCredito = DetalleCredito;
    }

    public long getMora() {
        return Mora;
    }

    public void setMora(long Mora) {
        this.Mora = Mora;
    }

    public long getSaldoinsoluto() {
        return saldoinsoluto;
    }

    public void setSaldoinsoluto(long saldoinsoluto) {
        this.saldoinsoluto = saldoinsoluto;
    }

    public int getCuotasMora() {
        return CuotasMora;
    }

    public void setCuotasMora(int CuotasMora) {
        this.CuotasMora = CuotasMora;
    }

    public int getCuotaspagada() {
        return Cuotaspagada;
    }

    public void setCuotaspagada(int Cuotaspagada) {
        this.Cuotaspagada = Cuotaspagada;
    }

    public int getCuotasPactada() {
        return CuotasPactada;
    }

    public void setCuotasPactada(int CuotasPactada) {
        this.CuotasPactada = CuotasPactada;
    }

    public int getDiasMora() {
        return DiasMora;
    }

    public void setDiasMora(int DiasMora) {
        this.DiasMora = DiasMora;
    }

    public String getProductos() {
        return Productos;
    }

    public void setProductos(String Productos) {
        this.Productos = Productos;
    }

    public String getFechaFencimiento() {
        return FechaFencimiento;
    }

    public void setFechaFencimiento(String FechaFencimiento) {
        this.FechaFencimiento = FechaFencimiento;
    }

    public String getFechaCastigo() {
        return FechaCastigo;
    }

    public void setFechaCastigo(String FechaCastigo) {
        this.FechaCastigo = FechaCastigo;
    }

    public String getMarcaRenegociado() {
        return MarcaRenegociado;
    }

    public void setMarcaRenegociado(String MarcaRenegociado) {
        this.MarcaRenegociado = MarcaRenegociado;
    }

    public int getNumeroDemandas() {
        return NumeroDemandas;
    }

    public void setNumeroDemandas(int NumeroDemandas) {
        this.NumeroDemandas = NumeroDemandas;
    }

    /**
     * @return the condona
     */
    public boolean isCondona() {
        return condona;
    }

    /**
     * @param condona the condona to set
     */
    public void setCondona(boolean condona) {
        this.condona = condona;
    }

}
