/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author esilves
 */
public class CondonacionesEjecutivos   {

    public String getEjecutivo_condona() {
        return ejecutivo_condona;
    }

    public void setEjecutivo_condona(String ejecutivo_condona) {
        this.ejecutivo_condona = ejecutivo_condona;
    }

    public int getNumero_condonaciones() {
        return numero_condonaciones;
    }

    public void setNumero_condonaciones(int numero_condonaciones) {
        this.numero_condonaciones = numero_condonaciones;
    }

    public String getUltimaCondonacionAplicada() {
        return ultimaCondonacionAplicada;
    }

    public void setUltimaCondonacionAplicada(String ultimaCondonacionAplicada) {
        this.ultimaCondonacionAplicada = ultimaCondonacionAplicada;
    }

    public String getUltimaCondonacionaprovada() {
        return ultimaCondonacionaprovada;
    }

    public void setUltimaCondonacionaprovada(String ultimaCondonacionaprovada) {
        this.ultimaCondonacionaprovada = ultimaCondonacionaprovada;
    }

    public int getTotal_reparos() {
        return total_reparos;
    }

    public void setTotal_reparos(int total_reparos) {
        this.total_reparos = total_reparos;
    }

    public CondonacionesEjecutivos() {
    }

    public CondonacionesEjecutivos(String ejecutivo_condona, int numero_condonaciones, String ultimaCondonacionAplicada, String ultimaCondonacionaprovada, int total_reparos) {
        this.ejecutivo_condona = ejecutivo_condona;
        this.numero_condonaciones = numero_condonaciones;
        this.ultimaCondonacionAplicada = ultimaCondonacionAplicada;
        this.ultimaCondonacionaprovada = ultimaCondonacionaprovada;
        this.total_reparos = total_reparos;
    }


    
    String ejecutivo_condona;
    int numero_condonaciones;
    String ultimaCondonacionAplicada;
    String ultimaCondonacionaprovada;
    int total_reparos;
    
    
}
