/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class CondonacionesAPP {

    public CondonacionesAPP() {
    }

    public CondonacionesAPP(String rut, int id_condonacion, String timestap, String dv_estado, String comentario_resna, float monto_total_recibit, float monto_total_capital, String dv_desc) {
        this.rut = rut;
        this.id_condonacion = id_condonacion;
        this.timestap = timestap;
        this.dv_estado = dv_estado;
        this.comentario_resna = comentario_resna;
        this.monto_total_recibit = monto_total_recibit;
        this.monto_total_capital = monto_total_capital;
        this.dv_desc = dv_desc;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }



    public String getTimestap() {
        return timestap;
    }

    public void setTimestap(String timestap) {
        this.timestap = timestap;
    }

    public String getDv_estado() {
        return dv_estado;
    }

    public void setDv_estado(String dv_estado) {
        this.dv_estado = dv_estado;
    }

    public String getComentario_resna() {
        return comentario_resna;
    }

    public void setComentario_resna(String comentario_resna) {
        this.comentario_resna = comentario_resna;
    }

    public float getMonto_total_recibit() {
        return monto_total_recibit;
    }

    public void setMonto_total_recibit(float monto_total_recibit) {
        this.monto_total_recibit = monto_total_recibit;
    }

    public float getMonto_total_capital() {
        return monto_total_capital;
    }

    public void setMonto_total_capital(float monto_total_capital) {
        this.monto_total_capital = monto_total_capital;
    }

    public String getDv_desc() {
        return dv_desc;
    }

    public void setDv_desc(String dv_desc) {
        this.dv_desc = dv_desc;
    }

String rut;

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }
int id_condonacion;
String timestap;
String dv_estado;
String comentario_resna;
float monto_total_recibit;
float monto_total_capital;
String dv_desc;

 
    


}
