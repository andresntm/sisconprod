/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.Date;
import java.util.List;

/**
 *
 * @author excosoc
 */
public class AdjuntarENC {

    /**
     * @return the aDet
     */
    public List<AdjuntarDET> getaDet() {
        return aDet;
    }

    /**
     * @param aDet the aDet to set
     */
    public void setaDet(List<AdjuntarDET> aDet) {
        this.aDet = aDet;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the fk_idCliente
     */
    public int getFk_idCliente() {
        return fk_idCliente;
    }

    /**
     * @param fk_idCliente the fk_idCliente to set
     */
    public void setFk_idCliente(int fk_idCliente) {
        this.fk_idCliente = fk_idCliente;
    }

    /**
     * @return the fk_idCondonacion
     */
    public int getFk_idCondonacion() {
        return fk_idCondonacion;
    }

    /**
     * @param fk_idCondonacion the fk_idCondonacion to set
     */
    public void setFk_idCondonacion(int fk_idCondonacion) {
        this.fk_idCondonacion = fk_idCondonacion;
    }

    /**
     * @return the registrado
     */
    public Date getRegistrado() {
        return registrado;
    }

    /**
     * @param registrado the registrado to set
     */
    public void setRegistrado(Date registrado) {
        this.registrado = registrado;
    }

    /**
     * @return the tama�o_total
     */
    public String getTama�o_total() {
        return tama�o_total;
    }

    /**
     * @param tama�o_total the tama�o_total to set
     */
    public void setTama�o_total(String tama�o_total) {
        this.tama�o_total = tama�o_total;
    }

    /**
     * @return the numero_archivos
     */
    public int getNumero_archivos() {
        return numero_archivos;
    }

    /**
     * @param numero_archivos the numero_archivos to set
     */
    public void setNumero_archivos(int numero_archivos) {
        this.numero_archivos = numero_archivos;
    }
    /**
     * @return the cli_Rut
     */
    public int getCli_Rut() {
        return cli_Rut;
    }

    /**
     * @param cli_Rut the cli_Rut to set
     */
    public void setCli_Rut(int cli_Rut) {
        this.cli_Rut = cli_Rut;
    }

    /**
     * @return the di_id_reparo
     */
    public int getDi_id_reparo() {
        return di_id_reparo;
    }

    /**
     * @param di_id_reparo the di_id_reparo to set
     */
    public void setDi_id_reparo(int di_id_reparo) {
        this.di_id_reparo = di_id_reparo;
    }
    private int id;

    private int fk_idCliente;
    private int fk_idCondonacion;
    private Date registrado;
    private String tama�o_total;
    private int numero_archivos;
    private List<AdjuntarDET> aDet;
    private int cli_Rut;
    private int di_id_reparo;
    public AdjuntarENC() {
        this.tama�o_total="0,0Mb";
    }

    public AdjuntarENC(int id,
            int fk_idCliente,
            int fk_idCondonacion,
            String tama�o_total,
            int numero_archivos,
            int di_id_reparo    ) {

        this.id = id;
        this.fk_idCliente = fk_idCliente;
        this.fk_idCondonacion = fk_idCondonacion;
        this.tama�o_total = tama�o_total;
        this.numero_archivos = numero_archivos;
        this.di_id_reparo = di_id_reparo;
    }

    public AdjuntarENC(int id,
            int fk_idCliente,
            int fk_idCondonacion,
            String tama�o_total,
            int numero_archivos,
            List<AdjuntarDET> aDet) {

        this.id = id;
        this.fk_idCliente = fk_idCliente;
        this.fk_idCondonacion = fk_idCondonacion;
        this.tama�o_total = tama�o_total;
        this.numero_archivos = numero_archivos;
        this.aDet = aDet;
    }

}
