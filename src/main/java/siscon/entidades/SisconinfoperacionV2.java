/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;


/**
 *
 * @author esilves
 */

public class SisconinfoperacionV2 implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;

    private String nroOperacion;

    private String nroOperacionOri;

    private Integer rutCliente;
 
    private Double capital;
  
    private Double porcentajeCondCap;
  
    private Double interes;
  
    private Double porcentajeCondInt;
   
    private Double honorarios;
 
    private Double porcentajeCondHonor;
   
    private Integer fkIdInfcliente;
   
    private Double capitalRecibe;
   
    private Double interesRecibe;
   
    private Double honorarioRecibe;
   
    private Double capitalCondona;
   
    private Double interesCondona;
   
    private Double honorarioCondona;
   
    private String judicialPrejudicialOperori;
   
    private String judicialPrejudicialOper;
    
     NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public SisconinfoperacionV2() {
    }

    public SisconinfoperacionV2(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNroOperacion() {
        return nroOperacion;
    }

    public void setNroOperacion(String nroOperacion) {
        this.nroOperacion = nroOperacion;
    }

    public String getNroOperacionOri() {
        return nroOperacionOri;
    }

    public void setNroOperacionOri(String nroOperacionOri) {
        this.nroOperacionOri = nroOperacionOri;
    }

    public Integer getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(Integer rutCliente) {
        this.rutCliente = rutCliente;
    }

    public Double getCapital() {
        return capital;
    }

        public String getCapitalS() {
      //  return capital;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(capital).replaceAll(",0*$", "");
    }
    
    
    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public Double getPorcentajeCondCap() {
        return porcentajeCondCap;
    }

    public void setPorcentajeCondCap(Double porcentajeCondCap) {
        this.porcentajeCondCap = porcentajeCondCap;
    }

    public Double getInteres() {
        return interes;
    }
    public String getInteresS() {
      //  return interes;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(interes).replaceAll(",0*$", "");
    }
    public void setInteres(Double interes) {
        this.interes = interes;
    }

    public Double getPorcentajeCondInt() {
        return porcentajeCondInt;
    }

    public void setPorcentajeCondInt(Double porcentajeCondInt) {
        this.porcentajeCondInt = porcentajeCondInt;
    }

    public Double getHonorarios() {
        return honorarios;
    }

    
    
        public String getHonorariosS() {
       // return honorarios;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(honorarios).replaceAll(",0*$", "");
    }
    public void setHonorarios(Double honorarios) {
        this.honorarios = honorarios;
    }

    public Double getPorcentajeCondHonor() {
        return porcentajeCondHonor;
    }

    public void setPorcentajeCondHonor(Double porcentajeCondHonor) {
        this.porcentajeCondHonor = porcentajeCondHonor;
    }

    public Integer getFkIdInfcliente() {
        return fkIdInfcliente;
    }

    public void setFkIdInfcliente(Integer fkIdInfcliente) {
        this.fkIdInfcliente = fkIdInfcliente;
    }

    public Double getCapitalRecibe() {
        return capitalRecibe;
    }

        public String getCapitalRecibeS() {
       // return capitalRecibe;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(capitalRecibe).replaceAll(",0*$", "");
    }
    
    public void setCapitalRecibe(Double capitalRecibe) {
        this.capitalRecibe = capitalRecibe;
    }

    public Double getInteresRecibe() {
        return interesRecibe;
    }

    public String getInteresRecibeS() {
     //   return interesRecibe;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(interesRecibe).replaceAll(",0*$", "");
    }
    public void setInteresRecibe(Double interesRecibe) {
        this.interesRecibe = interesRecibe;
    }

    public Double getHonorarioRecibe() {
        return honorarioRecibe;
    }

    
        public String getHonorarioRecibeS() {
        //return honorarioRecibe;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(honorarioRecibe).replaceAll(",0*$", "");
    }
    
    
    public void setHonorarioRecibe(Double honorarioRecibe) {
        this.honorarioRecibe = honorarioRecibe;
    }

    public Double getCapitalCondona() {
        return capitalCondona;
    }

    
    
    
    public String getCapitalCondonaS() {
       // return capitalCondona;
         return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(capitalCondona).replaceAll(",0*$", "");
    }
    
    public void setCapitalCondona(Double capitalCondona) {
        this.capitalCondona = capitalCondona;
    }

    public Double getInteresCondona() {
        return interesCondona;
    }

    
    
        public String getInteresCondonaS() {
      //  return interesCondona;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(interesCondona).replaceAll(",0*$", "");
    }
    
    public void setInteresCondona(Double interesCondona) {
        this.interesCondona = interesCondona;
    }

    public Double getHonorarioCondona() {
        return honorarioCondona;
    }
    
    
    
        public String getHonorarioCondonaS() {
            return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(honorarioCondona).replaceAll(",0*$", "");
       // return honorarioCondona;
    }

    public void setHonorarioCondona(Double honorarioCondona) {
        this.honorarioCondona = honorarioCondona;
    }

    public String getJudicialPrejudicialOperori() {
        return judicialPrejudicialOperori;
    }

    public void setJudicialPrejudicialOperori(String judicialPrejudicialOperori) {
        this.judicialPrejudicialOperori = judicialPrejudicialOperori;
    }

    public String getJudicialPrejudicialOper() {
        return judicialPrejudicialOper;
    }

    public void setJudicialPrejudicialOper(String judicialPrejudicialOper) {
        this.judicialPrejudicialOper = judicialPrejudicialOper;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SisconinfoperacionV2)) {
            return false;
        }
        SisconinfoperacionV2 other = (SisconinfoperacionV2) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


}
