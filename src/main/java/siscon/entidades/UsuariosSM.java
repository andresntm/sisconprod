/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author esilves
 */
public class UsuariosSM   {

    public UsuariosSM() {
    }
    
    
    
int idUsuario;
int idEstado;
int idTipoUsuario;
String strNombre;
String strApellidoP;
String strApellidoM;
String strUsuario;
String strRut;
String strDv;
String strSexo;
String strContrasena;
String dateFechaNacimiento;
String dateCreado;
String strCedente;
String strCargo;
String strPerfil;
String strRegional;
String fld_nom_asi;
float fld_por_vig;
String fld_lead_sn;
String fld_vig_001;
String fld_red_com_sn;
String fld_listado;
int fld_lis_vel;
String fld_tip_usu_cas;
String fld_vis;

@Value("${some.key:42}")
int fld_nov_suc;
int fld_ID_Padre;
int fld_ID_Padre2;
String Fecha_update;
int ID_padre_informe;
int fld_rut_2dig_des;
int fld_rut_2dig_has;
String ID_INFORMES_PYME;
int COD_EJEC_NOV;
int INF_ONLINE;
int VIS_COL;
int USER_EXT;
String FECHA_CADUCIDAD;
String FECHA_ULTIMO_ACCESO;
String ULTIMA_IP_REGISTRADA;
String URL_ACCESO_SC;
int INTENTOS_INGRESO;
int TEAM_RECL;
int CAMBIA_PASSWORD;
String DOMINIO;
String OLD_USER;
String VIS_SIM;
String POOL;
int fld_ofi_bci;
int ACEPTA_SIM;
int FYP;
int OLD_IDTIPOUSUARIO;
String fld_user_creacion;
int VIS_PR;
String fecha_desactiva;
String issiscon;



    public String getIssiscon() {
        return issiscon;
    }

    public void setIssiscon(String issiscon) {
        this.issiscon = issiscon;
    }


    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public String getStrApellidoP() {
        return strApellidoP;
    }

    public void setStrApellidoP(String strApellidoP) {
        this.strApellidoP = strApellidoP;
    }

    public String getStrApellidoM() {
        return strApellidoM;
    }

    public void setStrApellidoM(String strApellidoM) {
        this.strApellidoM = strApellidoM;
    }

    public String getStrUsuario() {
        return strUsuario;
    }

    public void setStrUsuario(String strUsuario) {
        this.strUsuario = strUsuario;
    }

    public String getStrRut() {
        return strRut;
    }

    public void setStrRut(String strRut) {
        this.strRut = strRut;
    }

    public String getStrDv() {
        return strDv;
    }

    public void setStrDv(String strDv) {
        this.strDv = strDv;
    }

    public String getStrSexo() {
        return strSexo;
    }

    public void setStrSexo(String strSexo) {
        this.strSexo = strSexo;
    }

    public String getStrContrasena() {
        return strContrasena;
    }

    public void setStrContrasena(String strContrasena) {
        this.strContrasena = strContrasena;
    }

    public String getDateFechaNacimiento() {
        return dateFechaNacimiento;
    }

    public void setDateFechaNacimiento(String dateFechaNacimiento) {
        this.dateFechaNacimiento = dateFechaNacimiento;
    }

    public String getDateCreado() {
        return dateCreado;
    }

    public void setDateCreado(String dateCreado) {
        this.dateCreado = dateCreado;
    }

    public String getStrCedente() {
        return strCedente;
    }

    public void setStrCedente(String strCedente) {
        this.strCedente = strCedente;
    }

    public String getStrCargo() {
        return strCargo;
    }

    public void setStrCargo(String strCargo) {
        this.strCargo = strCargo;
    }

    public String getStrPerfil() {
        return strPerfil;
    }

    public void setStrPerfil(String strPerfil) {
        this.strPerfil = strPerfil;
    }

    public String getStrRegional() {
        return strRegional;
    }

    public void setStrRegional(String strRegional) {
        this.strRegional = strRegional;
    }

    public String getFld_nom_asi() {
        return fld_nom_asi;
    }

    public void setFld_nom_asi(String fld_nom_asi) {
        this.fld_nom_asi = fld_nom_asi;
    }

    public float getFld_por_vig() {
        return fld_por_vig;
    }

    public void setFld_por_vig(float fld_por_vig) {
        this.fld_por_vig = fld_por_vig;
    }

    public String getFld_lead_sn() {
        return fld_lead_sn;
    }

    public void setFld_lead_sn(String fld_lead_sn) {
        this.fld_lead_sn = fld_lead_sn;
    }

    public String getFld_vig_001() {
        return fld_vig_001;
    }

    public void setFld_vig_001(String fld_vig_001) {
        this.fld_vig_001 = fld_vig_001;
    }

    public String getFld_red_com_sn() {
        return fld_red_com_sn;
    }

    public void setFld_red_com_sn(String fld_red_com_sn) {
        this.fld_red_com_sn = fld_red_com_sn;
    }

    public String getFld_listado() {
        return fld_listado;
    }

    public void setFld_listado(String fld_listado) {
        this.fld_listado = fld_listado;
    }

    public int getFld_lis_vel() {
        return fld_lis_vel;
    }

    public void setFld_lis_vel(int fld_lis_vel) {
        this.fld_lis_vel = fld_lis_vel;
    }

    public String getFld_tip_usu_cas() {
        return fld_tip_usu_cas;
    }

    public void setFld_tip_usu_cas(String fld_tip_usu_cas) {
        this.fld_tip_usu_cas = fld_tip_usu_cas;
    }

    public String getFld_vis() {
        return fld_vis;
    }

    public void setFld_vis(String fld_vis) {
        this.fld_vis = fld_vis;
    }

    public int getFld_nov_suc() {
        return fld_nov_suc;
    }

    public void setFld_nov_suc(int fld_nov_suc) {
        this.fld_nov_suc = fld_nov_suc;
    }

    public int getFld_ID_Padre() {
        return fld_ID_Padre;
    }

    public void setFld_ID_Padre(int fld_ID_Padre) {
        this.fld_ID_Padre = fld_ID_Padre;
    }

    public int getFld_ID_Padre2() {
        return fld_ID_Padre2;
    }

    public void setFld_ID_Padre2(int fld_ID_Padre2) {
        this.fld_ID_Padre2 = fld_ID_Padre2;
    }

    public String getFecha_update() {
        return Fecha_update;
    }

    public void setFecha_update(String Fecha_update) {
        this.Fecha_update = Fecha_update;
    }

    public int getID_padre_informe() {
        return ID_padre_informe;
    }

    public void setID_padre_informe(int ID_padre_informe) {
        this.ID_padre_informe = ID_padre_informe;
    }

    public int getFld_rut_2dig_des() {
        return fld_rut_2dig_des;
    }

    public void setFld_rut_2dig_des(int fld_rut_2dig_des) {
        this.fld_rut_2dig_des = fld_rut_2dig_des;
    }

    public int getFld_rut_2dig_has() {
        return fld_rut_2dig_has;
    }

    public void setFld_rut_2dig_has(int fld_rut_2dig_has) {
        this.fld_rut_2dig_has = fld_rut_2dig_has;
    }

    public String getID_INFORMES_PYME() {
        return ID_INFORMES_PYME;
    }

    public void setID_INFORMES_PYME(String ID_INFORMES_PYME) {
        this.ID_INFORMES_PYME = ID_INFORMES_PYME;
    }

    public int getCOD_EJEC_NOV() {
        return COD_EJEC_NOV;
    }

    public void setCOD_EJEC_NOV(int COD_EJEC_NOV) {
        this.COD_EJEC_NOV = COD_EJEC_NOV;
    }

    public int getINF_ONLINE() {
        return INF_ONLINE;
    }

    public void setINF_ONLINE(int INF_ONLINE) {
        this.INF_ONLINE = INF_ONLINE;
    }

    public int getVIS_COL() {
        return VIS_COL;
    }

    public void setVIS_COL(int VIS_COL) {
        this.VIS_COL = VIS_COL;
    }

    public int getUSER_EXT() {
        return USER_EXT;
    }

    public void setUSER_EXT(int USER_EXT) {
        this.USER_EXT = USER_EXT;
    }

    public String getFECHA_CADUCIDAD() {
        return FECHA_CADUCIDAD;
    }

    public void setFECHA_CADUCIDAD(String FECHA_CADUCIDAD) {
        this.FECHA_CADUCIDAD = FECHA_CADUCIDAD;
    }

    public String getFECHA_ULTIMO_ACCESO() {
        return FECHA_ULTIMO_ACCESO;
    }

    public void setFECHA_ULTIMO_ACCESO(String FECHA_ULTIMO_ACCESO) {
        this.FECHA_ULTIMO_ACCESO = FECHA_ULTIMO_ACCESO;
    }

    public String getULTIMA_IP_REGISTRADA() {
        return ULTIMA_IP_REGISTRADA;
    }

    public void setULTIMA_IP_REGISTRADA(String ULTIMA_IP_REGISTRADA) {
        this.ULTIMA_IP_REGISTRADA = ULTIMA_IP_REGISTRADA;
    }

    public String getURL_ACCESO_SC() {
        return URL_ACCESO_SC;
    }

    public void setURL_ACCESO_SC(String URL_ACCESO_SC) {
        this.URL_ACCESO_SC = URL_ACCESO_SC;
    }

    public int getINTENTOS_INGRESO() {
        return INTENTOS_INGRESO;
    }

    public void setINTENTOS_INGRESO(int INTENTOS_INGRESO) {
        this.INTENTOS_INGRESO = INTENTOS_INGRESO;
    }

    public int getTEAM_RECL() {
        return TEAM_RECL;
    }

    public void setTEAM_RECL(int TEAM_RECL) {
        this.TEAM_RECL = TEAM_RECL;
    }

    public int getCAMBIA_PASSWORD() {
        return CAMBIA_PASSWORD;
    }

    public void setCAMBIA_PASSWORD(int CAMBIA_PASSWORD) {
        this.CAMBIA_PASSWORD = CAMBIA_PASSWORD;
    }

    public String getDOMINIO() {
        return DOMINIO;
    }

    public void setDOMINIO(String DOMINIO) {
        this.DOMINIO = DOMINIO;
    }

    public String getOLD_USER() {
        return OLD_USER;
    }

    public void setOLD_USER(String OLD_USER) {
        this.OLD_USER = OLD_USER;
    }

    public String getVIS_SIM() {
        return VIS_SIM;
    }

    public void setVIS_SIM(String VIS_SIM) {
        this.VIS_SIM = VIS_SIM;
    }

    public String getPOOL() {
        return POOL;
    }

    public void setPOOL(String POOL) {
        this.POOL = POOL;
    }

    public int getFld_ofi_bci() {
        return fld_ofi_bci;
    }

    public void setFld_ofi_bci(int fld_ofi_bci) {
        this.fld_ofi_bci = fld_ofi_bci;
    }

    public int getACEPTA_SIM() {
        return ACEPTA_SIM;
    }

    public void setACEPTA_SIM(int ACEPTA_SIM) {
        this.ACEPTA_SIM = ACEPTA_SIM;
    }

    public int getFYP() {
        return FYP;
    }

    public void setFYP(int FYP) {
        this.FYP = FYP;
    }

    public int getOLD_IDTIPOUSUARIO() {
        return OLD_IDTIPOUSUARIO;
    }

    public void setOLD_IDTIPOUSUARIO(int OLD_IDTIPOUSUARIO) {
        this.OLD_IDTIPOUSUARIO = OLD_IDTIPOUSUARIO;
    }

    public String getFld_user_creacion() {
        return fld_user_creacion;
    }

    public void setFld_user_creacion(String fld_user_creacion) {
        this.fld_user_creacion = fld_user_creacion;
    }

    public int getVIS_PR() {
        return VIS_PR;
    }

    public void setVIS_PR(int VIS_PR) {
        this.VIS_PR = VIS_PR;
    }

    public String getFecha_desactiva() {
        return fecha_desactiva;
    }

    public void setFecha_desactiva(String fecha_desactiva) {
        this.fecha_desactiva = fecha_desactiva;
    }



    public UsuariosSM(int idUsuario, int idEstado, int idTipoUsuario, String strNombre, String strApellidoP, String strApellidoM, String strUsuario, String strRut, String strDv, String strSexo, String strContrasena, String dateFechaNacimiento, String dateCreado, String strCedente, String strCargo, String strPerfil, String strRegional, String fld_nom_asi, float fld_por_vig, String fld_lead_sn, String fld_vig_001, String fld_red_com_sn, String fld_listado, int fld_lis_vel, String fld_tip_usu_cas, String fld_vis, int fld_nov_suc, int fld_ID_Padre, int fld_ID_Padre2, String Fecha_update, int ID_padre_informe, int fld_rut_2dig_des, int fld_rut_2dig_has, String ID_INFORMES_PYME, int COD_EJEC_NOV, int INF_ONLINE, int VIS_COL, int USER_EXT, String FECHA_CADUCIDAD, String FECHA_ULTIMO_ACCESO, String ULTIMA_IP_REGISTRADA, String URL_ACCESO_SC, int INTENTOS_INGRESO, int TEAM_RECL, int CAMBIA_PASSWORD, String DOMINIO, String OLD_USER, String VIS_SIM, String POOL, int fld_ofi_bci, int ACEPTA_SIM, int FYP, int OLD_IDTIPOUSUARIO, String fld_user_creacion, int VIS_PR, String fecha_desactiva) {
        this.idUsuario = idUsuario;
        this.idEstado = idEstado;
        this.idTipoUsuario = idTipoUsuario;
        this.strNombre = strNombre;
        this.strApellidoP = strApellidoP;
        this.strApellidoM = strApellidoM;
        this.strUsuario = strUsuario;
        this.strRut = strRut;
        this.strDv = strDv;
        this.strSexo = strSexo;
        this.strContrasena = strContrasena;
        this.dateFechaNacimiento = dateFechaNacimiento;
        this.dateCreado = dateCreado;
        this.strCedente = strCedente;
        this.strCargo = strCargo;
        this.strPerfil = strPerfil;
        this.strRegional = strRegional;
        this.fld_nom_asi = fld_nom_asi;
        this.fld_por_vig = fld_por_vig;
        this.fld_lead_sn = fld_lead_sn;
        this.fld_vig_001 = fld_vig_001;
        this.fld_red_com_sn = fld_red_com_sn;
        this.fld_listado = fld_listado;
        this.fld_lis_vel = fld_lis_vel;
        this.fld_tip_usu_cas = fld_tip_usu_cas;
        this.fld_vis = fld_vis;
        this.fld_nov_suc = fld_nov_suc;
        this.fld_ID_Padre = fld_ID_Padre;
        this.fld_ID_Padre2 = fld_ID_Padre2;
        this.Fecha_update = Fecha_update;
        this.ID_padre_informe = ID_padre_informe;
        this.fld_rut_2dig_des = fld_rut_2dig_des;
        this.fld_rut_2dig_has = fld_rut_2dig_has;
        this.ID_INFORMES_PYME = ID_INFORMES_PYME;
        this.COD_EJEC_NOV = COD_EJEC_NOV;
        this.INF_ONLINE = INF_ONLINE;
        this.VIS_COL = VIS_COL;
        this.USER_EXT = USER_EXT;
        this.FECHA_CADUCIDAD = FECHA_CADUCIDAD;
        this.FECHA_ULTIMO_ACCESO = FECHA_ULTIMO_ACCESO;
        this.ULTIMA_IP_REGISTRADA = ULTIMA_IP_REGISTRADA;
        this.URL_ACCESO_SC = URL_ACCESO_SC;
        this.INTENTOS_INGRESO = INTENTOS_INGRESO;
        this.TEAM_RECL = TEAM_RECL;
        this.CAMBIA_PASSWORD = CAMBIA_PASSWORD;
        this.DOMINIO = DOMINIO;
        this.OLD_USER = OLD_USER;
        this.VIS_SIM = VIS_SIM;
        this.POOL = POOL;
        this.fld_ofi_bci = fld_ofi_bci;
        this.ACEPTA_SIM = ACEPTA_SIM;
        this.FYP = FYP;
        this.OLD_IDTIPOUSUARIO = OLD_IDTIPOUSUARIO;
        this.fld_user_creacion = fld_user_creacion;
        this.VIS_PR = VIS_PR;
        this.fecha_desactiva = fecha_desactiva;
    }
    
    
}
