/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

public class Cliente {

    public int id;
    public String nombreCOmpleto;
    public String nombre;
    public String apellidos;
    public String rut;
    public int rutint;
    public int size;
    public String ejecutivo;
    public String oficina;
    public String saldototal;
    public String saldovigente;
    public String saldocastigado;
    public int numerooficina;
    public String NombreOficina;
    public String Banca;
    public int Diamora;
    public String tienejuicio;
    public String cedente;
    public String Gestion;
    public String SaldoTotalMoraPesos;
    public String SumaGridCapitalOperaciones;
    public String SumaGridCapitalOperacionesPesos;
    String fld_cod_banca;
    public long SumaGridCapitalACondonar;
    public String SumaGridCapitalACondonarPesos;
    public long SumaGridCapitalARecibir;
    public String SumaGridCapitalARecibirPesos;
    public long SumaGridInteresTotal;
    public String SumaGridInteresTotalPesos;
    public long SumaGridInteresACondonar;
    public String SumaGridInteresACondonarPesos;
    public long SumaGridInteresARecibir;
    public String SumaGridInteresARecibirPesos;
    public long SumaGridHonorariosTotales;
    public String SumaGridHonorariosTotalesPesos;
    public long SumaGridHonorariosCondonados;
    public String SumaGridHonorariosCondonadosPesos;
    public long SumaGridHonorariosARecibir;
    public String SumaGridHonorariosARecibirPesos;
    String fld_region;

    public String getFld_cod_eje() {
        return fld_cod_eje;
    }

    public void setFld_cod_eje(String fld_cod_eje) {
        this.fld_cod_eje = fld_cod_eje;
    }

    public String getFld_eje_resp() {
        return fld_eje_resp;
    }

    public void setFld_eje_resp(String fld_eje_resp) {
        this.fld_eje_resp = fld_eje_resp;
    }

    public String getFld_cod_eje_resp() {
        return fld_cod_eje_resp;
    }

    public void setFld_cod_eje_resp(String fld_cod_eje_resp) {
        this.fld_cod_eje_resp = fld_cod_eje_resp;
    }

    public String getFld_dia_mor() {
        return fld_dia_mor;
    }

    public void setFld_dia_mor(String fld_dia_mor) {
        this.fld_dia_mor = fld_dia_mor;
    }

    public String getFld_jud() {
        return fld_jud;
    }

    public void setFld_jud(String fld_jud) {
        this.fld_jud = fld_jud;
    }

    public String getFld_ced() {
        return fld_ced;
    }

    public void setFld_ced(String fld_ced) {
        this.fld_ced = fld_ced;
    }

    public String getFld_cae() {
        return fld_cae;
    }

    public void setFld_cae(String fld_cae) {
        this.fld_cae = fld_cae;
    }

    public String getFld_pep() {
        return fld_pep;
    }

    public void setFld_pep(String fld_pep) {
        this.fld_pep = fld_pep;
    }

    public String getFld_cae_pep() {
        return fld_cae_pep;
    }

    public void setFld_cae_pep(String fld_cae_pep) {
        this.fld_cae_pep = fld_cae_pep;
    }

    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public String getFld_sdo_aldia() {
        return fld_sdo_aldia;
    }

    public void setFld_sdo_aldia(String fld_sdo_aldia) {
        this.fld_sdo_aldia = fld_sdo_aldia;
    }

    public String getFld_sdo_morydia() {
        return fld_sdo_morydia;
    }

    public void setFld_sdo_morydia(String fld_sdo_morydia) {
        this.fld_sdo_morydia = fld_sdo_morydia;
    }

    public String getFld_mto_mor() {
        return fld_mto_mor;
    }

    public void setFld_mto_mor(String fld_mto_mor) {
        this.fld_mto_mor = fld_mto_mor;
    }
    String fld_cod_eje;
    String fld_eje_resp;
    String fld_cod_eje_resp;
    String fld_dia_mor;
    String fld_jud;
    String fld_ced;
    String fld_cae;
    String fld_pep;
    String fld_cae_pep;
    String resp;
    String fld_sdo_aldia;
    String fld_sdo_morydia;
    String fld_mto_mor;
    
           

    public String getFld_region() {
        return fld_region;
    }

    public void setFld_region(String fld_region) {
        this.fld_region = fld_region;
    }
   

    public String getFld_cod_banca() {
        return fld_cod_banca;
    }

    public void setFld_cod_banca(String fld_cod_banca) {
        this.fld_cod_banca = fld_cod_banca;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreCOmpleto() {
        return nombreCOmpleto;
    }

    public void setNombreCOmpleto(String nombreCOmpleto) {
        this.nombreCOmpleto = nombreCOmpleto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getRutint() {
        return rutint;
    }

    public void setRutint(int rutint) {
        this.rutint = rutint;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getSaldototal() {
        return saldototal;
    }

    public void setSaldototal(String saldototal) {
        this.saldototal = saldototal;
    }

    public String getSaldovigente() {
        return saldovigente;
    }

    public void setSaldovigente(String saldovigente) {
        this.saldovigente = saldovigente;
    }

    public String getSaldocastigado() {
        return saldocastigado;
    }

    public void setSaldocastigado(String saldocastigado) {
        this.saldocastigado = saldocastigado;
    }

    public int getNumerooficina() {
        return numerooficina;
    }

    public void setNumerooficina(int numerooficina) {
        this.numerooficina = numerooficina;
    }

    public String getNombreOficina() {
        return NombreOficina;
    }

    public void setNombreOficina(String NombreOficina) {
        this.NombreOficina = NombreOficina;
    }

    public String getBanca() {
        return Banca;
    }

    public void setBanca(String Banca) {
        this.Banca = Banca;
    }

    public int getDiamora() {
        return Diamora;
    }

    public void setDiamora(int Diamora) {
        this.Diamora = Diamora;
    }

    public String getTienejuicio() {
        return tienejuicio;
    }

    public void setTienejuicio(String tienejuicio) {
        this.tienejuicio = tienejuicio;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getGestion() {
        return Gestion;
    }

    public void setGestion(String Gestion) {
        this.Gestion = Gestion;
    }

    public Cliente(int id, String nombreCOmpleto, String nombre, String apellidos, String rut, int rutint, int size, String ejecutivo, String oficina, String saldototal, String saldovigente, String saldocastigado, int numerooficina, String NombreOficina, String Banca, int Diamora, String tienejuicio, String cedente, String Gestion) {
        this.id = id;
        this.nombreCOmpleto = nombreCOmpleto;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.rut = rut;
        this.rutint = rutint;
        this.size = size;
        this.ejecutivo = ejecutivo;
        this.oficina = oficina;
        this.saldototal = saldototal;
        this.saldovigente = saldovigente;
        this.saldocastigado = saldocastigado;
        this.numerooficina = numerooficina;
        this.NombreOficina = NombreOficina;
        this.Banca = Banca;
        this.Diamora = Diamora;
        this.tienejuicio = tienejuicio;
        this.cedente = cedente;
        this.Gestion = Gestion;
    }

    public String getSumaGridCapitalOperaciones() {
        return SumaGridCapitalOperaciones;
    }

    public void setSumaGridCapitalOperaciones(String SumaGridCapitalOperaciones) {
        this.SumaGridCapitalOperaciones = SumaGridCapitalOperaciones;
    }

    public String getSumaGridCapitalOperacionesPesos() {
        return SumaGridCapitalOperacionesPesos;
    }

    public void setSumaGridCapitalOperacionesPesos(String SumaGridCapitalOperacionesPesos) {
        this.SumaGridCapitalOperacionesPesos = SumaGridCapitalOperacionesPesos;
    }

    public long getSumaGridCapitalACondonar() {
        return SumaGridCapitalACondonar;
    }

    public void setSumaGridCapitalACondonar(long SumaGridCapitalACondonar) {
        this.SumaGridCapitalACondonar = SumaGridCapitalACondonar;
    }

    public String getSumaGridCapitalACondonarPesos() {
        return SumaGridCapitalACondonarPesos;
    }

    public void setSumaGridCapitalACondonarPesos(String SumaGridCapitalACondonarPesos) {
        this.SumaGridCapitalACondonarPesos = SumaGridCapitalACondonarPesos;
    }

    public long getSumaGridCapitalARecibir() {
        return SumaGridCapitalARecibir;
    }

    public void setSumaGridCapitalARecibir(long SumaGridCapitalARecibir) {
        this.SumaGridCapitalARecibir = SumaGridCapitalARecibir;
    }

    public String getSumaGridCapitalARecibirPesos() {
        return SumaGridCapitalARecibirPesos;
    }

    public void setSumaGridCapitalARecibirPesos(String SumaGridCapitalARecibirPesos) {
        this.SumaGridCapitalARecibirPesos = SumaGridCapitalARecibirPesos;
    }

    public long getSumaGridInteresTotal() {
        return SumaGridInteresTotal;
    }

    public void setSumaGridInteresTotal(long SumaGridInteresTotal) {
        this.SumaGridInteresTotal = SumaGridInteresTotal;
    }

    public String getSumaGridInteresTotalPesos() {
        return SumaGridInteresTotalPesos;
    }

    public void setSumaGridInteresTotalPesos(String SumaGridInteresTotalPesos) {
        this.SumaGridInteresTotalPesos = SumaGridInteresTotalPesos;
    }

    public long getSumaGridInteresACondonar() {
        return SumaGridInteresACondonar;
    }

    public void setSumaGridInteresACondonar(long SumaGridInteresACondonar) {
        this.SumaGridInteresACondonar = SumaGridInteresACondonar;
    }

    public String getSumaGridInteresACondonarPesos() {
        return SumaGridInteresACondonarPesos;
    }

    public void setSumaGridInteresACondonarPesos(String SumaGridInteresACondonarPesos) {
        this.SumaGridInteresACondonarPesos = SumaGridInteresACondonarPesos;
    }

    public long getSumaGridInteresARecibir() {
        return SumaGridInteresARecibir;
    }

    public void setSumaGridInteresARecibir(long SumaGridInteresARecibir) {
        this.SumaGridInteresARecibir = SumaGridInteresARecibir;
    }

    public String getSumaGridInteresARecibirPesos() {
        return SumaGridInteresARecibirPesos;
    }

    public void setSumaGridInteresARecibirPesos(String SumaGridInteresARecibirPesos) {
        this.SumaGridInteresARecibirPesos = SumaGridInteresARecibirPesos;
    }

    public long getSumaGridHonorariosTotales() {
        return SumaGridHonorariosTotales;
    }

    public void setSumaGridHonorariosTotales(long SumaGridHonorariosTotales) {
        this.SumaGridHonorariosTotales = SumaGridHonorariosTotales;
    }

    public String getSumaGridHonorariosTotalesPesos() {
        return SumaGridHonorariosTotalesPesos;
    }

    public void setSumaGridHonorariosTotalesPesos(String SumaGridHonorariosTotalesPesos) {
        this.SumaGridHonorariosTotalesPesos = SumaGridHonorariosTotalesPesos;
    }

    public long getSumaGridHonorariosCondonados() {
        return SumaGridHonorariosCondonados;
    }

    public void setSumaGridHonorariosCondonados(long SumaGridHonorariosCondonados) {
        this.SumaGridHonorariosCondonados = SumaGridHonorariosCondonados;
    }

    public String getSumaGridHonorariosCondonadosPesos() {
        return SumaGridHonorariosCondonadosPesos;
    }

    public void setSumaGridHonorariosCondonadosPesos(String SumaGridHonorariosCondonadosPesos) {
        this.SumaGridHonorariosCondonadosPesos = SumaGridHonorariosCondonadosPesos;
    }

    public long getSumaGridHonorariosARecibir() {
        return SumaGridHonorariosARecibir;
    }

    public void setSumaGridHonorariosARecibir(long SumaGridHonorariosARecibir) {
        this.SumaGridHonorariosARecibir = SumaGridHonorariosARecibir;
    }

    public String getSumaGridHonorariosARecibirPesos() {
        return SumaGridHonorariosARecibirPesos;
    }

    public void setSumaGridHonorariosARecibirPesos(String SumaGridHonorariosARecibirPesos) {
        this.SumaGridHonorariosARecibirPesos = SumaGridHonorariosARecibirPesos;
    }

    public String getSaldoTotalMoraPesos() {
        return SaldoTotalMoraPesos;
    }

    public void setSaldoTotalMoraPesos(String SaldoTotalMoraPesos) {
        this.SaldoTotalMoraPesos = SaldoTotalMoraPesos;
    }

    public Cliente() {
    }

}
