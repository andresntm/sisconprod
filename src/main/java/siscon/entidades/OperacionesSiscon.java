/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 * 
 */
public class OperacionesSiscon {

    public OperacionesSiscon(String tipo_operacion, String codigoOrininalOperacion) {
        this.tipo_operacion = tipo_operacion;
        this.codigoOrininalOperacion = codigoOrininalOperacion;
    }



        String  tipo_operacion;
    String codigoOrininalOperacion;
    
    
    
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    public String getCodigoOrininalOperacion() {
        return codigoOrininalOperacion;
    }

    public void setCodigoOrininalOperacion(String codigoOrininalOperacion) {
        this.codigoOrininalOperacion = codigoOrininalOperacion;
    }

   
    
        @Override
    public String toString() {
        return "OperacionesSiscon{" + "tipo_operacion=" + tipo_operacion + ", codigoOrininalOperacion=" + codigoOrininalOperacion + '}';
    }
    
    
    
}
