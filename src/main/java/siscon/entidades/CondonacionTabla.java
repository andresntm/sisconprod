/**
 * Condonacion.java - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscon.entidades;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author exesilr
 */
public class CondonacionTabla {

    public CondonacionTabla(int id_condonacion, String timestap, String estado, String comentario_resna, float monto_total_condonado, float monto_total_recibit, int di_num_opers, float monto_total_capital, String tipocondonacion, String usuariocondona) {
        this.id_condonacion = id_condonacion;
        this.timestap = timestap;
        this.estado = estado;
        this.comentario_resna = comentario_resna;
        this.monto_total_condonado = monto_total_condonado;
        this.monto_total_recibit = monto_total_recibit;
        this.di_num_opers = di_num_opers;
        this.monto_total_capital = monto_total_capital;
        this.tipocondonacion = tipocondonacion;
        this.usuariocondona = usuariocondona;
        this.regla = "Regla 55";
        this.NumeroDeReparos = 0;
    }

    public CondonacionTabla() {
    }

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getTimestap() {
        return timestap;
    }

    public void setTimestap(String timestap) {
        this.timestap = timestap;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComentario_resna() {
        return comentario_resna;
    }

    public void setComentario_resna(String comentario_resna) {
        this.comentario_resna = comentario_resna;
    }

    public float getMonto_total_condonado() {
        return monto_total_condonado;
    }

    public void setMonto_total_condonado(float monto_total_condonado) {
        this.monto_total_condonado = monto_total_condonado;
    }

    public float getMonto_total_recibit() {
        return monto_total_recibit;
    }

    public void setMonto_total_recibit(float monto_total_recibit) {
        this.monto_total_recibit = monto_total_recibit;
    }

    public int getDi_num_opers() {
        return di_num_opers;
    }

    public void setDi_num_opers(int di_num_opers) {
        this.di_num_opers = di_num_opers;
    }

    public float getMonto_total_capital() {
        return monto_total_capital;
    }

    public void setMonto_total_capital(float monto_total_capital) {
        this.monto_total_capital = monto_total_capital;
    }

    public String getTipocondonacion() {
        return tipocondonacion;
    }

    public void setTipocondonacion(String tipocondonacion) {
        this.tipocondonacion = tipocondonacion;
    }

    public String getUsuariocondona() {
        return usuariocondona;
    }

    public void setUsuariocondona(String usuariocondona) {
        this.usuariocondona = usuariocondona;
    }

    int id_condonacion;
    String timestap;
    String regla;
    String estado;
    String comentario_resna;
    float monto_total_condonado;
    float monto_total_recibit;
    int di_num_opers;
    float monto_total_capital;
    String tipocondonacion;
    String usuariocondona;

    public int getNumeroDeReparos() {
        return NumeroDeReparos;
    }

    public void setNumeroDeReparos(int NumeroDeReparos) {
        this.NumeroDeReparos = NumeroDeReparos;
    }
    int NumeroDeReparos;
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public String getMonto_total_capitalS() {
//        return nf.format(monto_total_capital).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(monto_total_capital).replaceAll(".0*$", "");
    }

    public String getMonto_total_recibitS() {
//        return nf.format(monto_total_recibit).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(monto_total_recibit).replaceAll(".0*$", "");
    }

    public String getMonto_total_condonadoS() {
//        return nf.format(monto_total_condonado).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(monto_total_condonado).replaceAll(".0*$", "");
    }
}
