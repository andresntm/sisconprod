/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class RangoMonetario {

    private int id_rangom;
    private String desc;
    private int monto_ini;
    private int monto_fin;
    private int id_unidad;

    /**
     * @return the id_rangom
     */
    public int getId_rangom() {
        return id_rangom;
    }

    /**
     * @param id_rangom the id_rangom to set
     */
    public void setId_rangom(int id_rangom) {
        this.id_rangom = id_rangom;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the monto_ini
     */
    public int getMonto_ini() {
        return monto_ini;
    }

    /**
     * @param monto_ini the monto_ini to set
     */
    public void setMonto_ini(int monto_ini) {
        this.monto_ini = monto_ini;
    }

    /**
     * @return the monto_fin
     */
    public int getMonto_fin() {
        return monto_fin;
    }

    /**
     * @param monto_fin the monto_fin to set
     */
    public void setMonto_fin(int monto_fin) {
        this.monto_fin = monto_fin;
    }

    /**
     * @return the id_unidad
     */
    public int getId_unidad() {
        return id_unidad;
    }

    /**
     * @param id_unidad the id_unidad to set
     */
    public void setId_unidad(int id_unidad) {
        this.id_unidad = id_unidad;
    }

    public RangoMonetario() {
    }

    public RangoMonetario(int id_rangom,
            String desc,
            int monto_ini,
            int monto_fin,
            int id_unidad) {

        this.id_rangom = id_rangom;
        this.desc = desc;
        this.monto_ini = monto_ini;
        this.monto_fin = monto_fin;
        this.id_unidad = id_unidad;

    }

    public RangoMonetario(int id_rangom) {

        this.id_rangom = id_rangom;

    }

}
