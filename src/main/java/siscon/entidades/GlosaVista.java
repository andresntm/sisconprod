/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class GlosaVista {

    public GlosaVista() {
        	this.NombreColumna="";
    this.Modulo="";
    this.Vista="";
    this.TipoDato="";
    this.Glosario="";
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNombreColumna() {
        return NombreColumna;
    }

    public void setNombreColumna(String NombreColumna) {
        this.NombreColumna = NombreColumna;
    }

    public String getModulo() {
        return Modulo;
    }

    public void setModulo(String Modulo) {
        this.Modulo = Modulo;
    }

    public String getVista() {
        return Vista;
    }

    public void setVista(String Vista) {
        this.Vista = Vista;
    }

    public String getTipoDato() {
        return TipoDato;
    }

    public void setTipoDato(String TipoDato) {
        this.TipoDato = TipoDato;
    }

    public String getGlosario() {
        return Glosario;
    }

    public void setGlosario(String Glosario) {
        this.Glosario = Glosario;
    }

    public GlosaVista(String NombreColumna, String Modulo, String Vista, String TipoDato, String Glosario) {
        this.NombreColumna = NombreColumna;
        this.Modulo = Modulo;
        this.Vista = Vista;
        this.TipoDato = TipoDato;
        this.Glosario = Glosario;
    }
    
    String NombreColumna;
    String Modulo;
    String Vista;
    String TipoDato;
    String Glosario;
    
}
