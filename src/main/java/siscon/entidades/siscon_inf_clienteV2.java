/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class siscon_inf_clienteV2  {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotal_interes_recibe() {
        return total_interes_recibe;
    }

    public void setTotal_interes_recibe(float total_interes_recibe) {
        this.total_interes_recibe = total_interes_recibe;
    }

    public float getTotal_honorarios_recibe() {
        return total_honorarios_recibe;
    }

    public void setTotal_honorarios_recibe(float total_honorarios_recibe) {
        this.total_honorarios_recibe = total_honorarios_recibe;
    }

    public float getTotal_capital_condona() {
        return total_capital_condona;
    }

    public void setTotal_capital_condona(float total_capital_condona) {
        this.total_capital_condona = total_capital_condona;
    }

    public float getTotal_interes_condona() {
        return total_interes_condona;
    }

    public void setTotal_interes_condona(float total_interes_condona) {
        this.total_interes_condona = total_interes_condona;
    }

    public float getTotal_honorarios_condona() {
        return total_honorarios_condona;
    }

    public void setTotal_honorarios_condona(float total_honorarios_condona) {
        this.total_honorarios_condona = total_honorarios_condona;
    }

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getEjecutivo_condona() {
        return ejecutivo_condona;
    }

    public void setEjecutivo_condona(String ejecutivo_condona) {
        this.ejecutivo_condona = ejecutivo_condona;
    }

    public String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    public void setFecha_aprobacion(String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    public String getFecha_aplicacion() {
        return fecha_aplicacion;
    }

    public void setFecha_aplicacion(String fecha_aplicacion) {
        this.fecha_aplicacion = fecha_aplicacion;
    }

    public int getNumero_reparos() {
        return numero_reparos;
    }

    public void setNumero_reparos(int numero_reparos) {
        this.numero_reparos = numero_reparos;
    }

    public String getAprovado_por() {
        return aprovado_por;
    }

    public void setAprovado_por(String aprovado_por) {
        this.aprovado_por = aprovado_por;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public int getNumero_garantias() {
        return numero_garantias;
    }

    public void setNumero_garantias(int numero_garantias) {
        this.numero_garantias = numero_garantias;
    }

    public int getNotificado() {
        return notificado;
    }

    public void setNotificado(int notificado) {
        this.notificado = notificado;
    }

    public float getSgn() {
        return sgn;
    }

    public void setSgn(float sgn) {
        this.sgn = sgn;
    }
    public String getRut_cliente() {
        return rut_cliente;
    }

    public void setRut_cliente(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }
    public siscon_inf_clienteV2(int id, float total_interes_recibe, float total_honorarios_recibe, float total_capital_condona, float total_interes_condona, float total_honorarios_condona, int id_condonacion, String ejecutivo_condona, String fecha_aprobacion, String fecha_aplicacion, int numero_reparos, String aprovado_por, String cedente, int numero_garantias, int notificado, float sgn,String rut_cliente) {
        this.id = id;
        this.total_interes_recibe = total_interes_recibe;
        this.total_honorarios_recibe = total_honorarios_recibe;
        this.total_capital_condona = total_capital_condona;
        this.total_interes_condona = total_interes_condona;
        this.total_honorarios_condona = total_honorarios_condona;
        this.id_condonacion = id_condonacion;
        this.ejecutivo_condona = ejecutivo_condona;
        this.fecha_aprobacion = fecha_aprobacion;
        this.fecha_aplicacion = fecha_aplicacion;
        this.numero_reparos = numero_reparos;
        this.aprovado_por = aprovado_por;
        this.cedente = cedente;
        this.numero_garantias = numero_garantias;
        this.notificado = notificado;
        this.sgn = sgn;
    }

    public siscon_inf_clienteV2() {
    }



    
    
    int  id ;
String rut_cliente;
float  total_interes_recibe ;
float total_honorarios_recibe;
float total_capital_condona ;  
float total_interes_condona;   
float total_honorarios_condona   ;
int id_condonacion             ; 
String ejecutivo_condona     ;   
String fecha_aprobacion     ;    
String fecha_aplicacion    ;    
int numero_reparos        ;  
String aprovado_por       ;      
String cedente        ;         
int numero_garantias   ;        
int notificado        ;          
float sgn          ;              

}
