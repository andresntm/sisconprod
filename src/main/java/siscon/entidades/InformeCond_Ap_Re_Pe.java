/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MvcConfig;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.implementaciones.InformeCond_Ap_Re_PeImplements;
import siscon.entidades.interfaces.InformeCond_Ap_Re_PeInterface;

/**
 *
 * @author excosoc
 */
public class InformeCond_Ap_Re_Pe implements RowMapper<InformeCond_Ap_Re_Pe> {

    /**
     * @return the Pend_cond
     */
    public int getPend_cond() {
        return Pend_cond;
    }

    /**
     * @param Pend_cond the Pend_cond to set
     */
    public void setPend_cond(int Pend_cond) {
        this.Pend_cond = Pend_cond;
    }

    /**
     * @return the Apro_cond
     */
    public int getApro_cond() {
        return Apro_cond;
    }

    /**
     * @param Apro_cond the Apro_cond to set
     */
    public void setApro_cond(int Apro_cond) {
        this.Apro_cond = Apro_cond;
    }

    /**
     * @return the Rech_cond
     */
    public int getRech_cond() {
        return Rech_cond;
    }

    /**
     * @param Rech_cond the Rech_cond to set
     */
    public void setRech_cond(int Rech_cond) {
        this.Rech_cond = Rech_cond;
    }

    /**
     * @return the nombre_usuario
     */
    public String getAlias_usuario() {
        return alias_usuario;
    }

    /**
     * @param nombre_usuario the nombre_usuario to set
     */
    public void setAlias_usuario(String alias_usuario) {
        this.alias_usuario = alias_usuario;
    }

    /**
     * @return the dv_rut
     */
    public String getDv_rut() {
        return dv_rut;
    }

    /**
     * @param dv_rut the dv_rut to set
     */
    public void setDv_rut(String dv_rut) {
        this.dv_rut = dv_rut;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the nombre_jefe
     */
    public String getNombre_jefe() {
        return nombre_jefe;
    }

    /**
     * @param nombre_jefe the nombre_jefe to set
     */
    public void setNombre_jefe(String nombre_jefe) {
        this.nombre_jefe = nombre_jefe;
    }

    /**
     * @return the numero_cond
     */
    public int getNumero_cond() {
        return numero_cond;
    }

    /**
     * @param numero_cond the numero_cond to set
     */
    public void setNumero_cond(int numero_cond) {
        this.numero_cond = numero_cond;
    }

    /**
     * @return the fecha_ultima_cond
     */
    public String getFecha_ultima_cond() {
        return fecha_ultima_cond;
    }

    /**
     * @param fecha_ultima_cond the fecha_ultima_cond to set
     */
    public void setFecha_ultima_cond(String fecha_ultima_cond) {
        this.fecha_ultima_cond = fecha_ultima_cond;
    }

    private String alias_usuario;
    private String dv_rut;
    private String nombres;
    private String nombre_jefe;
    private String rut_jefe;
    private int numero_cond;
    private int Pend_cond;
    private int Apro_cond;
    private int Rech_cond;
    private String fecha_ultima_cond;
    private InformeCond_Ap_Re_PeInterface informeInterface;
    MvcConfig conex;

    private void inicializa() {
        try {
            conex = new MvcConfig();
            informeInterface = new InformeCond_Ap_Re_PeImplements(conex.getDataSourceSisCon());

        } catch (Exception sqlE) {
            Messagebox.show("Error al conectar a la base de datos\nError: " + sqlE.getMessage());
        }

    }

    public InformeCond_Ap_Re_Pe() {
        inicializa();
    }

    public InformeCond_Ap_Re_Pe(String nombre_usuario,
            String dv_rut,
            String nombres,
            String nombre_jefe,
            int numero_cond,
            int Apro_cond,
            int Pend_cond,
            int Rech_cond,
            String fecha_ultima_cond,
            String rut_jefe) {

        inicializa();

        this.alias_usuario = nombre_usuario;
        this.dv_rut = dv_rut;
        this.nombres = nombres;
        this.nombre_jefe = nombre_jefe;
        this.Apro_cond = Apro_cond;
        this.Pend_cond = Pend_cond;
        this.Rech_cond = Rech_cond;
        this.numero_cond = numero_cond;
        this.fecha_ultima_cond = fecha_ultima_cond;
        this.rut_jefe = rut_jefe;
    }

    public List<InformeCond_Ap_Re_Pe> list_Informe_Pyme() {

        List<InformeCond_Ap_Re_Pe> lInforme = new ArrayList<InformeCond_Ap_Re_Pe>();
        lInforme = informeInterface.ListInformeCond_Ap_Re_Pe_Pyme();
        return lInforme;

    }
    public List<InformeCond_Ap_Re_Pe> list_Informe_Retail() {

        List<InformeCond_Ap_Re_Pe> lInforme = new ArrayList<InformeCond_Ap_Re_Pe>();
        lInforme = informeInterface.ListInformeCond_Ap_Re_Pe_Retail();
        return lInforme;

    }
    
    @Override
    public InformeCond_Ap_Re_Pe mapRow(ResultSet rs, int rowNum) throws SQLException {
        InformeCond_Ap_Re_Pe informe = new InformeCond_Ap_Re_Pe();

        informe.setAlias_usuario(rs.getString("alias_usuario"));
        informe.setDv_rut(rs.getString("dv_rut"));
        informe.setNombres(rs.getString("nombres"));
        informe.setRut_jefe(rs.getString("rut_jefe"));
        informe.setNombre_jefe(rs.getString("nombre_jefe"));
        informe.setApro_cond(rs.getInt("Apro_cond"));
        informe.setPend_cond(rs.getInt("Pend_cond"));
        informe.setRech_cond(rs.getInt("Rech_cond"));
        informe.setNumero_cond(rs.getInt("numero_cond"));
        informe.setFecha_ultima_cond(rs.getString("fecha_ultima_cond"));

        return informe;
    }

    /**
     * @return the rut_jefe
     */
    public String getRut_jefe() {
        return rut_jefe;
    }

    /**
     * @param rut_jefe the rut_jefe to set
     */
    public void setRut_jefe(String rut_jefe) {
        this.rut_jefe = rut_jefe;
    }

}
