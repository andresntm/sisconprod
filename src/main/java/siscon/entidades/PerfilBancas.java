/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class PerfilBancas {

    int id_perfil;
    String Descripcion;
    String CodPerfil;
    String Bancas;
    
    public String getBancas() {
        return Bancas;
    }

    public void setBancas(String Bancas) {
        this.Bancas = Bancas;
    }


    public PerfilBancas(int id_perfil, String Descripcion, String CodPerfil, String Bancas) {
        this.id_perfil = id_perfil;
        this.Descripcion = Descripcion;
        this.CodPerfil = CodPerfil;
        this.Bancas = Bancas;
    }
    
    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    
    


    public PerfilBancas() {
    }
    
       public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getCodPerfil() {
        return CodPerfil;
    }

    public void setCodPerfil(String CodPerfil) {
        this.CodPerfil = CodPerfil;
    }


}
