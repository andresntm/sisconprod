/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author exesilr
 */

public class ValorMonto {
    
    private  double valor=0.0;
    private  float valorF=0;
    private String valorUf;

    public double getValor() {
        return valor;
    }

    public String getValorPesos() {
        return valorPesos;
    }

    public String getValorDolar() {
        return valorDolar;
    }

    public String getValorenuf() {
        return valorenuf;
    }

    private final String valorPesos;
    private final String valorDolar;
    private final String valorenuf;

    private final double ValordelaUf=26000;
    private final double ValordelDolar=625;
    NumberFormat nff = NumberFormat.getCurrencyInstance(Locale.getDefault());
  
  
  
    public ValorMonto(double valorongreso) 
    {
        this.valor = valorongreso;
        this.valorPesos=nff.format(valorongreso).replaceFirst("Ch", "");
        this.valorDolar= "$$$$"+Double.toString(ValordelDolar * valorongreso);
        this.valorenuf="UF"+Double.toString(ValordelaUf * valorongreso);
    }
    
        public ValorMonto(float valorongreso) 
        {
        this.valorF = valorongreso;
        this.valorPesos=nff.format(valorongreso).replaceFirst("Ch", "");
        this.valorDolar= "$$$$"+Double.toString(ValordelDolar * valorongreso);
        this.valorenuf="UF"+Double.toString(ValordelaUf * valorongreso);
    }
    
    
    
}
