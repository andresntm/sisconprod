/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class RangoFecha {

    private int id_rangof;
    private String desc;
    private int rango_ini;
    private int rango_fin;
    private String unidad;

    /**
     * @return the id_rangof
     */
    public int getId_rangof() {
        return id_rangof;
    }

    /**
     * @param id_rangof the id_rangof to set
     */
    public void setId_rangof(int id_rangof) {
        this.id_rangof = id_rangof;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the rango_ini
     */
    public int getRango_ini() {
        return rango_ini;
    }

    /**
     * @param rango_ini the rango_ini to set
     */
    public void setRango_ini(int rango_ini) {
        this.rango_ini = rango_ini;
    }

    /**
     * @return the rango_fin
     */
    public int getRango_fin() {
        return rango_fin;
    }

    /**
     * @param rango_fin the rango_fin to set
     */
    public void setRango_fin(int rango_fin) {
        this.rango_fin = rango_fin;
    }

    /**
     * @return the unidad
     */
    public String getUnidad() {
        return unidad;
    }

    /**
     * @param unidad the unidad to set
     */
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public RangoFecha() {
    }

    private RangoFecha(int id_rangof,
            String desc,
            int rango_ini,
            int rango_fin,
            String unidad) {

        this.id_rangof = id_rangof;
        this.desc = desc;
        this.rango_ini = rango_ini;
        this.rango_fin = rango_fin;
        this.unidad = unidad;
    }

    RangoFecha(int id_rangof) {

        this.id_rangof = id_rangof;
    }

}
