/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MvcConfig;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import siscon.entidades.implementaciones.Tipo_ProrrogaImplements;
import siscon.entidades.interfaces.Tipo_ProrrogaInterfaz;
import static siscore.comunes.LogController.SisCorelog;

/**
 *
 * @author excosoc
 */
public class Tipo_Prorroga implements RowMapper<Tipo_Prorroga> {

    /**
     * @return the id_TipoProrroga
     */
    public int getId_TipoProrroga() {
        return id_TipoProrroga;
    }

    /**
     * @param id_TipoProrroga the id_TipoProrroga to set
     */
    public void setId_TipoProrroga(int id_TipoProrroga) {
        this.id_TipoProrroga = id_TipoProrroga;
    }

    /**
     * @return the Detalle
     */
    public String getDetalle() {
        return Detalle;
    }

    /**
     * @param Detalle the Detalle to set
     */
    public void setDetalle(String Detalle) {
        this.Detalle = Detalle;
    }

    /**
     * @return the fecIngreso
     */
    public Date getFecIngreso() {
        return fecIngreso;
    }

    /**
     * @param fecIngreso the fecIngreso to set
     */
    public void setFecIngreso(Date fecIngreso) {
        this.fecIngreso = fecIngreso;
    }

    /**
     * @return the bit_Activo
     */
    public boolean isBit_Activo() {
        return bit_Activo;
    }

    /**
     * @param bit_Activo the bit_Activo to set
     */
    public void setBit_Activo(boolean bit_Activo) {
        this.bit_Activo = bit_Activo;
    }

    private int id_TipoProrroga;
    private String Detalle;
    private Date fecIngreso;
    private boolean bit_Activo;
    private MvcConfig conex;
    private Tipo_ProrrogaInterfaz tPI;

    private void inicializa() {
        try {
            conex = new MvcConfig();
            tPI = new Tipo_ProrrogaImplements(conex.getDataSource());
        } catch (SQLException ex) {
            SisCorelog("Error al cargar la entidad Tipo_Prorroga");
        }
    }

    public Tipo_Prorroga() {
        inicializa();
    }

    public Tipo_Prorroga(int id_TipoProrroga, String Detalle, Date fecIngreso, boolean bit_Activo) {
        this.id_TipoProrroga = id_TipoProrroga;
        this.Detalle = Detalle;
        this.fecIngreso = fecIngreso;
        this.bit_Activo = bit_Activo;
        inicializa();
    }

    public Tipo_Prorroga mapRow(ResultSet rs, int rowNum) throws SQLException {
        Tipo_Prorroga tP = new Tipo_Prorroga();

        tP.setId_TipoProrroga(rs.getInt("id_TipoProrroga"));
        tP.setDetalle(rs.getString("Detalle"));
        tP.setFecIngreso(rs.getDate("fecIngreso"));
        tP.setBit_Activo(rs.getBoolean("bit_Activo"));

        return tP;
    }

    public Tipo_Prorroga insert() {
        int id = 0;
        try {
            id = tPI.insertTipo_Prorroga(this);

            this.setId_TipoProrroga(id);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden grabar los datos del tipo de prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al insertar Error:" + ex.getMessage() + ", datos:" + this.toString());
            id = 0;
        }
        return this;

    }

    public boolean actualiza() {
        boolean up = false;
        try {

            up = tPI.updateTipo_Prorroga(this);
        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden actualizar los datos del tipo de prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al actualizar Error:" + ex.getMessage() + ", datos:" + this.toString());
            up = false;
        }

        return up;
    }

    public boolean activa_Inactiva() {
        boolean activ = false;
        try {

            activ = tPI.activa_InactivaTipo_Prorroga(this);

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden " + (this.isBit_Activo() ? "activar" : "inavilitar") + " los datos del tipo de prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al " + (this.isBit_Activo() ? "activar" : "inavilitar") + " Error:" + ex.getMessage() + ", datos:" + this.toString());
            activ = false;
        }

        return activ;
    }

    public void get(final int id_TipoProrroga) {
        Tipo_Prorroga tP = new Tipo_Prorroga();

        try {

            tP = tPI.getTipo_Prorroga(id_TipoProrroga);

            this.id_TipoProrroga = tP.id_TipoProrroga;
            this.Detalle = tP.Detalle;
            this.fecIngreso = tP.fecIngreso;
            this.bit_Activo = tP.bit_Activo;

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se pueden extraer el registro de la base datos", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al traer registro Error:" + ex.getMessage() + ", datos:" + this.toString());
            tP = null;
        }

    }

    public List<Tipo_Prorroga> listAll() {
        List<Tipo_Prorroga> lTR = new ArrayList<Tipo_Prorroga>();

        try {

            lTR = tPI.listTipo_Prorrogas();

        } catch (Exception ex) {
            Messagebox.show("Sr(a) ususario(a), no se puedenen listar todos los registros de tipo de prorroga", "Siscon-Admin", Messagebox.OK, Messagebox.EXCLAMATION);
            SisCorelog("Error al listar Error:" + ex.getMessage() + ", datos:" + this.toString());
            lTR = null;
        }

        return lTR;
    }

}
