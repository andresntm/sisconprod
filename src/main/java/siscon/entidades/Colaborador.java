/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author esilvestre
 */
public class Colaborador {

    
    
    int _idColaborador;
    String _rut;
    String Nombre1;
    String Nombre2;
    String apellido1;
    String apellido2;
    usuario _usuario;
    int id_filial;
    int id_adreTrabajo;
    String Nombres;
    String Apellidos;
    String fechaIngreso;
    String CorreoCorporativo;
    int id_usuario;


    int fj_idJefe;
    
    
    public Colaborador(int _idColaborador, String _rut, String Nombre1, String Nombre2, String apellido1, String apellido2, usuario _usuario) {
        this._idColaborador = _idColaborador;
        this._rut = _rut;
        this.Nombre1 = Nombre1;
        this.Nombre2 = Nombre2;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this._usuario = _usuario;
    }

    public Colaborador(int _idColaborador, String _rut, String Nombre1, String Nombre2, String apellido1, String apellido2, usuario _usuario, int id_filial, int id_adreTrabajo, String Nombres, String Apellidos, String fechaIngreso, String CorreoCorporativo) {
        this._idColaborador = _idColaborador;
        this._rut = _rut;
        this.Nombre1 = Nombre1;
        this.Nombre2 = Nombre2;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this._usuario = _usuario;
        this.id_filial = id_filial;
        this.id_adreTrabajo = id_adreTrabajo;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.fechaIngreso = fechaIngreso;
        this.CorreoCorporativo = CorreoCorporativo;
    }
    
    
    
    
        public int getFj_idJefe() {
        return fj_idJefe;
    }

    public void setFj_idJefe(int fj_idJefe) {
        this.fj_idJefe = fj_idJefe;
    }
    
    public int getIdColaborador() {
        return _idColaborador;
    }

    public void setIdColaborador(int _idColaborador) {
        this._idColaborador = _idColaborador;
    }

    public String getRut() {
        return _rut;
    }

    public void setRut(String _rut) {
        this._rut = _rut;
    }

    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String Nombre1) {
        this.Nombre1 = Nombre1;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String Nombre2) {
        this.Nombre2 = Nombre2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public usuario getUsuario() {
        return _usuario;
    }

    public void setUsuario(usuario _usuario) {
        this._usuario = _usuario;
    }

    public Colaborador() {
        this._usuario=new usuario();
    }

    
    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }
    
    
    
    
    public int getId_filial() {
        return id_filial;
    }

    public void setId_filial(int id_filial) {
        this.id_filial = id_filial;
    }

    public int getId_adreTrabajo() {
        return id_adreTrabajo;
    }

    public void setId_adreTrabajo(int id_adreTrabajo) {
        this.id_adreTrabajo = id_adreTrabajo;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getCorreoCorporativo() {
        return CorreoCorporativo;
    }

    public void setCorreoCorporativo(String CorreoCorporativo) {
        this.CorreoCorporativo = CorreoCorporativo;
    }
    

    
    
}
