/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class JudicialCliente {

    public JudicialCliente(int rut_cliente, String cedente, String operacion, String tipo_operacion, String abogado, String estudio, String contralor, String fecha_asignacion, String fecha_recepcion, String fecha_ingreso_demanda, String fecha_notificacion, String fecha_remate, String fecha_salida, String fecha_ultima_gestion, String rol, String tribunal, String etapa_juicio, String estado_juicio, String fecha_incautacion) {
        this.rut_cliente = rut_cliente;
        this.cedente = cedente;
        this.operacion = operacion;
        this.tipo_operacion = tipo_operacion;
        this.abogado = abogado;
        this.estudio = estudio;
        this.contralor = contralor;
        this.fecha_asignacion = fecha_asignacion;
        this.fecha_recepcion = fecha_recepcion;
        this.fecha_ingreso_demanda = fecha_ingreso_demanda;
        this.fecha_notificacion = fecha_notificacion;
        this.fecha_remate = fecha_remate;
        this.fecha_salida = fecha_salida;
        this.fecha_ultima_gestion = fecha_ultima_gestion;
        this.rol = rol;
        this.tribunal = tribunal;
        this.etapa_juicio = etapa_juicio;
        this.estado_juicio = estado_juicio;
        this.fecha_incautacion = fecha_incautacion;
    }
        private int rut_cliente;
        private String cedente;
	private String operacion;
        private String tipo_operacion;
        private String abogado;
        private String estudio;
        private String contralor;
        private String fecha_asignacion;
        private String fecha_recepcion;
        private String fecha_ingreso_demanda;
        private String fecha_notificacion;
        private String fecha_remate;
        private String fecha_salida;
        private String fecha_ultima_gestion;
        private String rol;
        private String tribunal;
        private String etapa_juicio;
        private String estado_juicio;
        private String fecha_incautacion;
    
     public JudicialCliente(){}

    public int getRut_cliente() {
        return rut_cliente;
    }

    public void setRut_cliente(int rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getTipo_operacion() {
        return tipo_operacion;
    }

    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    public String getAbogado() {
        return abogado;
    }

    public void setAbogado(String abogado) {
        this.abogado = abogado;
    }

    public String getEstudio() {
        return estudio;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public String getContralor() {
        return contralor;
    }

    public void setContralor(String contralor) {
        this.contralor = contralor;
    }

    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setFecha_asignacion(String fecha_asignacion) {
        this.fecha_asignacion = fecha_asignacion;
    }

    public String getFecha_recepcion() {
        return fecha_recepcion;
    }

    public void setFecha_recepcion(String fecha_recepcion) {
        this.fecha_recepcion = fecha_recepcion;
    }

    public String getFecha_ingreso_demanda() {
        return fecha_ingreso_demanda;
    }

    public void setFecha_ingreso_demanda(String fecha_ingreso_demanda) {
        this.fecha_ingreso_demanda = fecha_ingreso_demanda;
    }

    public String getFecha_notificacion() {
        return fecha_notificacion;
    }

    public void setFecha_notificacion(String fecha_notificacion) {
        this.fecha_notificacion = fecha_notificacion;
    }

    public String getFecha_remate() {
        return fecha_remate;
    }

    public void setFecha_remate(String fecha_remate) {
        this.fecha_remate = fecha_remate;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public String getFecha_ultima_gestion() {
        return fecha_ultima_gestion;
    }

    public void setFecha_ultima_gestion(String fecha_ultima_gestion) {
        this.fecha_ultima_gestion = fecha_ultima_gestion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getTribunal() {
        return tribunal;
    }

    public void setTribunal(String tribunal) {
        this.tribunal = tribunal;
    }

    public String getEtapa_juicio() {
        return etapa_juicio;
    }

    public void setEtapa_juicio(String etapa_juicio) {
        this.etapa_juicio = etapa_juicio;
    }

    public String getFecha_incautacion() {
        return fecha_incautacion;
    }

    public void setFecha_incautacion(String fecha_incautacion) {
        this.fecha_incautacion = fecha_incautacion;
    }

    public String getEstado_juicio() {
        return estado_juicio;
    }

    public void setEstado_juicio(String estado_juicio) {
        this.estado_juicio = estado_juicio;
    }

        
        
        
}

