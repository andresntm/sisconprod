/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class GraficoTPcondonaDataGrid {


    String Fecha;
    double tiempo;
    double tiempoespeado;
    String tiempoFMTminutos;
    String tiempoespeadoFMTminutos;
    public String getTiempoFMTminutos() {
        return tiempoFMTminutos;
    }

    public String getTiempoespeadoFMTminutos() {
        return tiempoespeadoFMTminutos;
    }

    
    
        public GraficoTPcondonaDataGrid(String Fecha, double tiempo, double tiempoespeado) {
        this.Fecha = Fecha;
        this.tiempo = tiempo;
         this.tiempoFMTminutos=tiempo+" Minutos";
        this.tiempoespeado = tiempoespeado;
        this.tiempoespeadoFMTminutos=tiempoespeado+ " Minutos";
    }
    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public double getTiempo() {
        return tiempo;
    }

    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
        this.tiempoFMTminutos=tiempo+" Minutos";
    }

    public double getTiempoespeado() {
        return tiempoespeado;
        
    }

    public void setTiempoespeado(double tiempoespeado) {
        this.tiempoespeado = tiempoespeado;
        this.tiempoespeadoFMTminutos=tiempoespeado+ " Minutos";
    }
    
    

    
}
