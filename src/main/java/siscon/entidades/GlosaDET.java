/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class GlosaDET {

    /**
     * @return the id_user
     */
    public int getId_user() {
        return id_user;
    }

    /**
     * @param id_user the id_user to set
     */
    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    /**
     * @return the modulo
     */
    public String getModulo() {
        return modulo;
    }

    /**
     * @param modulo the modulo to set
     */
    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_usrGlosa
     */
    public int getId_usrGlosa() {
        return id_usrGlosa;
    }

    /**
     * @param id_usrGlosa the id_usrGlosa to set
     */
    public void setId_usrGlosa(int id_usrGlosa) {
        this.id_usrGlosa = id_usrGlosa;
    }

    /**
     * @return the edit
     */
    public boolean isEdit() {
        return edit;
    }

    /**
     * @param edit the edit to set
     */
    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    /**
     * @return the glosa
     */
    public String getGlosa() {
        return glosa;
    }

    /**
     * @param glosa the glosa to set
     */
    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    private int id;
    private int id_usrGlosa;
    private String glosa;
    private String user;
    private int id_user;
    private boolean edit;
    private String modulo;

    public GlosaDET() {
    }

    public GlosaDET(int id,
            int id_usrGlosa,
            String glosa,
            String user,
            boolean edit,
            String modulo) {

        this.id = id;
        this.id_usrGlosa = id_usrGlosa;
        this.glosa = glosa;
        this.user = user;
        this.edit = edit;
        this.modulo = modulo;

    }

}
