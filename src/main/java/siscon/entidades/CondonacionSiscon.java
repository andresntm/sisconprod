/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.jdbc.core.RowMapper;
import org.zkoss.zul.Messagebox;
import java.util.List;
import java.util.ArrayList;
import siscon.entidades.implementaciones.CondonacionSisconImplement;
import siscon.entidades.interfaces.CondonacionSisconInterfaz;

/**
 *
 * @author excosoc
 */
public class CondonacionSiscon implements RowMapper<CondonacionSiscon> {

    /**
     * @return the cliS
     */
    public ClienteSiscon getCliS() {
        return cliS;
    }

    /**
     * @param cliS the cliS to set
     */
    public void setCliS(ClienteSiscon cliS) {
        this.cliS = cliS;
    }

    /**
     * @return the est
     */
    public Estado getEst() {
        return est;
    }

    /**
     * @param est the est to set
     */
    public void setEst(Estado est) {
        this.est = est;
    }

    /**
     * @return the id_condonacion
     */
    public int getId_condonacion() {
        return id_condonacion;
    }

    /**
     * @param id_condonacion the id_condonacion to set
     */
    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    /**
     * @return the timestap
     */
    public Timestamp getTimestap() {
        return timestap;
    }

    /**
     * @param timestap the timestap to set
     */
    public void setTimestap(Timestamp timestap) {
        this.timestap = timestap;
    }

    /**
     * @return the id_regla
     */
    public int getId_regla() {
        return id_regla;
    }

    /**
     * @param id_regla the id_regla to set
     */
    public void setId_regla(int id_regla) {
        this.id_regla = id_regla;
    }

    /**
     * @return the id_estado
     */
    public int getId_estado() {
        return id_estado;
    }

    /**
     * @param id_estado the id_estado to set
     */
    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }

    /**
     * @return the comentario_resna
     */
    public String getComentario_resna() {
        return comentario_resna;
    }

    /**
     * @param comentario_resna the comentario_resna to set
     */
    public void setComentario_resna(String comentario_resna) {
        this.comentario_resna = comentario_resna;
    }

    /**
     * @return the monto_total_condonado
     */
    public float getMonto_total_condonado() {
        return monto_total_condonado;
    }

    /**
     * @param monto_total_condonado the monto_total_condonado to set
     */
    public void setMonto_total_condonado(float monto_total_condonado) {
        this.monto_total_condonado = monto_total_condonado;
    }

    /**
     * @return the monto_total_recibit
     */
    public float getMonto_total_recibit() {
        return monto_total_recibit;
    }

    /**
     * @param monto_total_recibit the monto_total_recibit to set
     */
    public void setMonto_total_recibit(float monto_total_recibit) {
        this.monto_total_recibit = monto_total_recibit;
    }

    /**
     * @return the di_num_opers
     */
    public int getDi_num_opers() {
        return di_num_opers;
    }

    /**
     * @param di_num_opers the di_num_opers to set
     */
    public void setDi_num_opers(int di_num_opers) {
        this.di_num_opers = di_num_opers;
    }

    /**
     * @return the monto_total_capital
     */
    public float getMonto_total_capital() {
        return monto_total_capital;
    }

    /**
     * @param monto_total_capital the monto_total_capital to set
     */
    public void setMonto_total_capital(float monto_total_capital) {
        this.monto_total_capital = monto_total_capital;
    }

    /**
     * @return the di_fk_tipocondonacion
     */
    public int getDi_fk_tipocondonacion() {
        return di_fk_tipocondonacion;
    }

    /**
     * @param di_fk_tipocondonacion the di_fk_tipocondonacion to set
     */
    public void setDi_fk_tipocondonacion(int di_fk_tipocondonacion) {
        this.di_fk_tipocondonacion = di_fk_tipocondonacion;
    }

    /**
     * @return the di_fk_idColadorador
     */
    public int getDi_fk_idColadorador() {
        return di_fk_idColadorador;
    }

    /**
     * @param di_fk_idColadorador the di_fk_idColadorador to set
     */
    public void setDi_fk_idColadorador(int di_fk_idColadorador) {
        this.di_fk_idColadorador = di_fk_idColadorador;
    }

    /**
     * @return the di_fk_IdCliente
     */
    public int getDi_fk_IdCliente() {
        return di_fk_IdCliente;
    }

    /**
     * @param di_fk_IdCliente the di_fk_IdCliente to set
     */
    public void setDi_fk_IdCliente(int di_fk_IdCliente) {
        this.di_fk_IdCliente = di_fk_IdCliente;
    }

    /**
     * @return the monto_condonado_capital
     */
    public float getMonto_condonado_capital() {
        return monto_condonado_capital;
    }

    /**
     * @param monto_condonado_capital the monto_condonado_capital to set
     */
    public void setMonto_condonado_capital(float monto_condonado_capital) {
        this.monto_condonado_capital = monto_condonado_capital;
    }

    /**
     * @return the monto_condonado_recibe
     */
    public float getMonto_condonado_recibe() {
        return monto_condonado_recibe;
    }

    /**
     * @param monto_condonado_recibe the monto_condonado_recibe to set
     */
    public void setMonto_condonado_recibe(float monto_condonado_recibe) {
        this.monto_condonado_recibe = monto_condonado_recibe;
    }

    /**
     * @return the monto_honorario_capital
     */
    public float getMonto_honorario_capital() {
        return monto_honorario_capital;
    }

    /**
     * @param monto_honorario_capital the monto_honorario_capital to set
     */
    public void setMonto_honorario_capital(float monto_honorario_capital) {
        this.monto_honorario_capital = monto_honorario_capital;
    }

    /**
     * @return the monto_honorario_capitalRecibe
     */
    public float getMonto_honorario_capitalRecibe() {
        return monto_honorario_capitalRecibe;
    }

    /**
     * @param monto_honorario_capitalRecibe the monto_honorario_capitalRecibe to
     * set
     */
    public void setMonto_honorario_capitalRecibe(float monto_honorario_capitalRecibe) {
        this.monto_honorario_capitalRecibe = monto_honorario_capitalRecibe;
    }

//    /**
//     * @return the monto_condonado_honorario
//     */
//    public float getMonto_condonado_honorario() {
//        return monto_condonado_honorario;
//    }
//
//    /**
//     * @param monto_condonado_honorario the monto_condonado_honorario to set
//     */
//    public void setMonto_condonado_honorario(float monto_condonado_honorario) {
//        this.monto_condonado_honorario = monto_condonado_honorario;
//    }
//
//    /**
//     * @return the monto_VDE_SGN
//     */
//    public float getMonto_VDE_SGN() {
//        return monto_VDE_SGN;
//    }
//
//    /**
//     * @param monto_VDE_SGN the monto_VDE_SGN to set
//     */
//    public void setMonto_VDE_SGN(float monto_VDE_SGN) {
//        this.monto_VDE_SGN = monto_VDE_SGN;
//    }

    private int id_condonacion;
    private Timestamp timestap;
    private int id_regla;
    private int id_estado;
    private String comentario_resna;
    private float monto_total_condonado;
    private float monto_total_recibit;
    private int di_num_opers;
    private float monto_total_capital;
    private int di_fk_tipocondonacion;
    private int di_fk_idColadorador;
    private int di_fk_IdCliente;
    private float monto_condonado_capital;
    private float monto_condonado_recibe;
    private float monto_honorario_capital;
    private float monto_honorario_capitalRecibe;
//    private float monto_condonado_honorario;
//    private float monto_VDE_SGN;
    private CondonacionSisconInterfaz cSIntz;
    private ClienteSiscon cliS;
    private Estado est;

    private void inicializa() {
        cSIntz = new CondonacionSisconImplement();
        setCliS(new ClienteSiscon());
        setEst(new Estado());
    }

    public CondonacionSiscon() {
        inicializa();
    }

    public CondonacionSiscon(int id_condonacion, Timestamp timestap, int id_regla, int id_estado, String comentario_resna, float monto_total_condonado, float monto_total_recibit, int di_num_opers, float monto_total_capital, int di_fk_tipocondonacion, int di_fk_idColadorador, int di_fk_IdCliente, float monto_condonado_capital, float monto_condonado_recibe, float monto_honorario_capital, float monto_honorario_capitalRecibe, float monto_condonado_honorario, float monto_VDE_SGN) {
        this.id_condonacion = id_condonacion;
        this.timestap = timestap;
        this.id_regla = id_regla;
        this.id_estado = id_estado;
        this.comentario_resna = comentario_resna;
        this.monto_total_condonado = monto_total_condonado;
        this.monto_total_recibit = monto_total_recibit;
        this.di_num_opers = di_num_opers;
        this.monto_total_capital = monto_total_capital;
        this.di_fk_tipocondonacion = di_fk_tipocondonacion;
        this.di_fk_idColadorador = di_fk_idColadorador;
        this.di_fk_IdCliente = di_fk_IdCliente;
        this.monto_condonado_capital = monto_condonado_capital;
        this.monto_condonado_recibe = monto_condonado_recibe;
        this.monto_honorario_capital = monto_honorario_capital;
        this.monto_honorario_capitalRecibe = monto_honorario_capitalRecibe;
//        this.monto_condonado_honorario = monto_condonado_honorario;
//        this.monto_VDE_SGN = monto_VDE_SGN;
        inicializa();
    }

    public CondonacionSiscon mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {

            this.id_condonacion = rs.getInt("id_condonacion");
            this.timestap = rs.getTimestamp("timestap");
            this.id_regla = rs.getInt("id_regla");
            this.id_estado = rs.getInt("id_estado");
            this.comentario_resna = rs.getString("comentario_resna");
            this.monto_total_condonado = rs.getFloat("monto_total_condonado");
            this.monto_total_recibit = rs.getFloat("monto_total_recibit");
            this.di_num_opers = rs.getInt("di_num_opers");
            this.monto_total_capital = rs.getFloat("monto_total_capital");
            this.di_fk_tipocondonacion = rs.getInt("di_fk_tipocondonacion");
            this.di_fk_idColadorador = rs.getInt("di_fk_idColadorador");
            this.di_fk_IdCliente = rs.getInt("di_fk_IdCliente");
            this.monto_condonado_capital = rs.getFloat("monto_condonado_capital");
            this.monto_condonado_recibe = rs.getFloat("monto_condonado_recibe");
            this.monto_honorario_capital = rs.getFloat("monto_honorario_capital");
            this.monto_honorario_capitalRecibe = rs.getFloat("monto_honorario_capitalRecibe");
//            this.monto_condonado_honorario = rs.getFloat("monto_condonado_honorario");
//            this.monto_VDE_SGN = rs.getFloat("monto_VDE_SGN");

            //carga objectos heredados
            this.getCliS().setId_cliente(this.di_fk_IdCliente);
            this.getCliS().getCliente();

            this.getEst().setDi_id_Estado(this.id_estado);
            this.getEst().getEstado_X_IdEstado();

        } catch (Exception ex) {
            Messagebox.show("Sr(a) usuario(a), no se pueden enlazar los datos de la condonación.", "Admin-Siscon", Messagebox.OK, Messagebox.ERROR);
        }

        return this;
    }

    public void getCondonacion() {
        CondonacionSiscon cS = new CondonacionSiscon();
        cS = cSIntz.getCondonacion(this.id_condonacion);

        try {
            this.id_condonacion = cS.getId_condonacion();
            this.timestap = cS.getTimestap();
            this.id_regla = cS.getId_regla();
            this.id_estado = cS.getId_estado();
            this.comentario_resna = cS.getComentario_resna();
            this.monto_total_condonado = cS.getMonto_total_condonado();
            this.monto_total_recibit = cS.getMonto_total_recibit();
            this.di_num_opers = cS.getDi_num_opers();
            this.monto_total_capital = cS.getMonto_total_capital();
            this.di_fk_tipocondonacion = cS.getDi_fk_tipocondonacion();
            this.di_fk_idColadorador = cS.getDi_fk_idColadorador();
            this.di_fk_IdCliente = cS.getDi_fk_IdCliente();
            this.monto_condonado_capital = cS.getMonto_condonado_capital();
            this.monto_condonado_recibe = cS.getMonto_condonado_recibe();
            this.monto_honorario_capital = cS.getMonto_honorario_capital();
            this.monto_honorario_capitalRecibe = cS.getMonto_honorario_capitalRecibe();
//            this.monto_condonado_honorario = cS.getMonto_condonado_honorario();
//            this.monto_VDE_SGN = cS.getMonto_VDE_SGN();
            this.setCliS(cS.getCliS());
            this.est = cS.getEst();
        } catch (Exception ex) {
            Messagebox.show("Sr(a) usuario(a), el sistema no puede traer la condonación solicitada.", "Admin-Siscon", Messagebox.OK, Messagebox.ERROR);
        }
    }

    public List<CondonacionSiscon> listConds() {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion();

        return lCS;
    }

    public List<CondonacionSiscon> listConds_IdColaborador(final int IdColaborador) {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion_X_idColadorador(IdColaborador);

        return lCS;
    }

    public List<CondonacionSiscon> listConds_IdCliente(final int IdCliente) {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion_X_IdCliente(IdCliente);

        return lCS;
    }

    public List<CondonacionSiscon> listConds_IdEstado(final int IdEstado) {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion_X_IdEstado(IdEstado);

        return lCS;
    }

    public List<CondonacionSiscon> listConds_IdRegla(final int IdRegla) {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion_X_IdRegla(IdRegla);

        return lCS;
    }

    public List<CondonacionSiscon> listConds_IdTipoCondonacion(final int IdTipoCondonacion) {
        List<CondonacionSiscon> lCS = new ArrayList<CondonacionSiscon>();

        lCS = cSIntz.listCondonacion_X_IdTipoCondonacion(IdTipoCondonacion);

        return lCS;
    }

}
