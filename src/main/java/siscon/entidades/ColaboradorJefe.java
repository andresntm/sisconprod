/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author esilvestre
 */
public class ColaboradorJefe extends Colaborador {

    List<Colaborador> _colaboradorACargo;


    usuario _usuario;
    int myid_jefe;
    public ColaboradorJefe(List<Colaborador> _colaboradorACargo) {
        this._colaboradorACargo = _colaboradorACargo;
    }

    public ColaboradorJefe() {
        this._colaboradorACargo = new ArrayList<Colaborador>();
        this._usuario=new usuario();
        this.myid_jefe=0;
    }

    public List<Colaborador> getColaboradorACargo() {
        return _colaboradorACargo;
    }

    public void setColaboradorACargo(List<Colaborador> _colaboradorACargo) {
        this._colaboradorACargo = _colaboradorACargo;
    }

    
    
        public usuario getUsuario() {
        return _usuario;
    }

    public void setUsuario(usuario _usuario) {
        this._usuario = _usuario;
    }

    public int getMyId_jefe() {
        return myid_jefe;
    }

    public void setMyId_jefe(int id_jefe) {
        this.myid_jefe = id_jefe;
    }
    
    
}
