/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exaesan
 */
public class DetalleClienteExcel {
    
     private String cedenteHono;
    private String operacionHono;
    private String detoriHono;
    private String TipoCedenteHono;
    private String DetalleCreditoHono;
    private long MoraHono;
    private long saldoinsolutoHono;
    private int CuotasMoraHono;
    private int CuotaspagadaHono;
    private int CuotasPactadaHono;
    private int DiasMoraHono;
    private String ProductosHono;
    private String FechaFencimientoHono;
    private String FechaCastigoHono;
    private String MarcaRenegociadoHono;
    private int NumeroDemandasHono;
    private int fk_id_Cli_Cond_Enc;
    private int id_cli_Cond_Det;
    private boolean condona;
    private String FLD_MOR;
    private String FLD_SDO;
    private String FLD_DET_TIP_CRE_MIN;
    private String PAGADAS;
    private String TOTAL_CUOTAS;
    private String NRO_DEMANDAS;
    float honorarioJudicialHono;
    
    
    //sacabop
    
    public ValorMonto HonoJudsobreCondonado;
    private String CapitalARecibirPesos;
    private long HonorarioJuduciaRecibir;
    private String cedente;
    private String operacion;
    private String detori;
    private String TipoCedente;
    private String DetalleCredito;
    private float Mora;
    private float saldoinsoluto;
    private int CuotasMora;
    private int Cuotaspagada;
    private int CuotasPactada;
    private int DiasMora;
    private String Productos;
    private String FechaFencimiento;
    private String FechaCastigo;
    private String MarcaRenegociado;
    private int NumeroDemandas;
    private String MesesCastigo;
    private String Capital;
    private String Interes;
    private String HonorarioJudicial;
    private float HonorarioJuducial;
    private float MontoCondonar;
    private float MontoARecibir;
    private float CapitalCondonado;
    private float CapitalARecibir;
    private float HonorarioJuducialCondonado;
    private float Porcentajecondonar;
    private float valorUF;
    private String MontoCondonarPesos;
    private float InteresCondonado;
    private float InteresARecibir;

    private String InteresCondonadoPesos;
    private String InteresARecibirPesos;
    private String HonorarioJudicialPesos;
    private String HonorarioJudicialCondonadoPesos;
    private String HonorarioJudicialRecibidoPesos;
    private String HonJudCondSobreCapitalPesos;
    private String HonJudRecSobreCapitalPesos;
    private float HONOR_F1;
    private String MoraEnPesosChileno;
    private String SaldoEnPesosChileno;

    public String getCedenteHono() {
        return cedenteHono;
    }

    public void setCedenteHono(String cedenteHono) {
        this.cedenteHono = cedenteHono;
    }

    public String getOperacionHono() {
        return operacionHono;
    }

    public void setOperacionHono(String operacionHono) {
        this.operacionHono = operacionHono;
    }

    public String getDetoriHono() {
        return detoriHono;
    }

    public void setDetoriHono(String detoriHono) {
        this.detoriHono = detoriHono;
    }

    public String getTipoCedenteHono() {
        return TipoCedenteHono;
    }

    public void setTipoCedenteHono(String TipoCedenteHono) {
        this.TipoCedenteHono = TipoCedenteHono;
    }

    public String getDetalleCreditoHono() {
        return DetalleCreditoHono;
    }

    public void setDetalleCreditoHono(String DetalleCreditoHono) {
        this.DetalleCreditoHono = DetalleCreditoHono;
    }

    public long getMoraHono() {
        return MoraHono;
    }

    public void setMoraHono(long MoraHono) {
        this.MoraHono = MoraHono;
    }

    public long getSaldoinsolutoHono() {
        return saldoinsolutoHono;
    }

    public void setSaldoinsolutoHono(long saldoinsolutoHono) {
        this.saldoinsolutoHono = saldoinsolutoHono;
    }

    public int getCuotasMoraHono() {
        return CuotasMoraHono;
    }

    public void setCuotasMoraHono(int CuotasMoraHono) {
        this.CuotasMoraHono = CuotasMoraHono;
    }

    public int getCuotaspagadaHono() {
        return CuotaspagadaHono;
    }

    public void setCuotaspagadaHono(int CuotaspagadaHono) {
        this.CuotaspagadaHono = CuotaspagadaHono;
    }

    public int getCuotasPactadaHono() {
        return CuotasPactadaHono;
    }

    public void setCuotasPactadaHono(int CuotasPactadaHono) {
        this.CuotasPactadaHono = CuotasPactadaHono;
    }

    public int getDiasMoraHono() {
        return DiasMoraHono;
    }

    public void setDiasMoraHono(int DiasMoraHono) {
        this.DiasMoraHono = DiasMoraHono;
    }

    public String getProductosHono() {
        return ProductosHono;
    }

    public void setProductosHono(String ProductosHono) {
        this.ProductosHono = ProductosHono;
    }

    public String getFechaFencimientoHono() {
        return FechaFencimientoHono;
    }

    public void setFechaFencimientoHono(String FechaFencimientoHono) {
        this.FechaFencimientoHono = FechaFencimientoHono;
    }

    public String getFechaCastigoHono() {
        return FechaCastigoHono;
    }

    public void setFechaCastigoHono(String FechaCastigoHono) {
        this.FechaCastigoHono = FechaCastigoHono;
    }

    public String getMarcaRenegociadoHono() {
        return MarcaRenegociadoHono;
    }

    public void setMarcaRenegociadoHono(String MarcaRenegociadoHono) {
        this.MarcaRenegociadoHono = MarcaRenegociadoHono;
    }

    public int getNumeroDemandasHono() {
        return NumeroDemandasHono;
    }

    public void setNumeroDemandasHono(int NumeroDemandasHono) {
        this.NumeroDemandasHono = NumeroDemandasHono;
    }

    public int getFk_id_Cli_Cond_Enc() {
        return fk_id_Cli_Cond_Enc;
    }

    public void setFk_id_Cli_Cond_Enc(int fk_id_Cli_Cond_Enc) {
        this.fk_id_Cli_Cond_Enc = fk_id_Cli_Cond_Enc;
    }

    public int getId_cli_Cond_Det() {
        return id_cli_Cond_Det;
    }

    public void setId_cli_Cond_Det(int id_cli_Cond_Det) {
        this.id_cli_Cond_Det = id_cli_Cond_Det;
    }

    public boolean isCondona() {
        return condona;
    }

    public void setCondona(boolean condona) {
        this.condona = condona;
    }

    public String getFLD_MOR() {
        return FLD_MOR;
    }

    public void setFLD_MOR(String FLD_MOR) {
        this.FLD_MOR = FLD_MOR;
    }

    public String getFLD_SDO() {
        return FLD_SDO;
    }

    public void setFLD_SDO(String FLD_SDO) {
        this.FLD_SDO = FLD_SDO;
    }

    public String getFLD_DET_TIP_CRE_MIN() {
        return FLD_DET_TIP_CRE_MIN;
    }

    public void setFLD_DET_TIP_CRE_MIN(String FLD_DET_TIP_CRE_MIN) {
        this.FLD_DET_TIP_CRE_MIN = FLD_DET_TIP_CRE_MIN;
    }

    public String getPAGADAS() {
        return PAGADAS;
    }

    public void setPAGADAS(String PAGADAS) {
        this.PAGADAS = PAGADAS;
    }

    public String getTOTAL_CUOTAS() {
        return TOTAL_CUOTAS;
    }

    public void setTOTAL_CUOTAS(String TOTAL_CUOTAS) {
        this.TOTAL_CUOTAS = TOTAL_CUOTAS;
    }

    public String getNRO_DEMANDAS() {
        return NRO_DEMANDAS;
    }

    public void setNRO_DEMANDAS(String NRO_DEMANDAS) {
        this.NRO_DEMANDAS = NRO_DEMANDAS;
    }

    public float getHonorarioJudicialHono() {
        return honorarioJudicialHono;
    }

    public void setHonorarioJudicialHono(float honorarioJudicialHono) {
        this.honorarioJudicialHono = honorarioJudicialHono;
    }

    public ValorMonto getHonoJudsobreCondonado() {
        return HonoJudsobreCondonado;
    }

    public void setHonoJudsobreCondonado(ValorMonto HonoJudsobreCondonado) {
        this.HonoJudsobreCondonado = HonoJudsobreCondonado;
    }

    public String getCapitalARecibirPesos() {
        return CapitalARecibirPesos;
    }

    public void setCapitalARecibirPesos(String CapitalARecibirPesos) {
        this.CapitalARecibirPesos = CapitalARecibirPesos;
    }

    public long getHonorarioJuduciaRecibir() {
        return HonorarioJuduciaRecibir;
    }

    public void setHonorarioJuduciaRecibir(long HonorarioJuduciaRecibir) {
        this.HonorarioJuduciaRecibir = HonorarioJuduciaRecibir;
    }

    public String getCedente() {
        return cedente;
    }

    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getDetori() {
        return detori;
    }

    public void setDetori(String detori) {
        this.detori = detori;
    }

    public String getTipoCedente() {
        return TipoCedente;
    }

    public void setTipoCedente(String TipoCedente) {
        this.TipoCedente = TipoCedente;
    }

    public String getDetalleCredito() {
        return DetalleCredito;
    }

    public void setDetalleCredito(String DetalleCredito) {
        this.DetalleCredito = DetalleCredito;
    }

    public float getMora() {
        return Mora;
    }

    public void setMora(float Mora) {
        this.Mora = Mora;
    }

    public float getSaldoinsoluto() {
        return saldoinsoluto;
    }

    public void setSaldoinsoluto(float saldoinsoluto) {
        this.saldoinsoluto = saldoinsoluto;
    }

    public int getCuotasMora() {
        return CuotasMora;
    }

    public void setCuotasMora(int CuotasMora) {
        this.CuotasMora = CuotasMora;
    }

    public int getCuotaspagada() {
        return Cuotaspagada;
    }

    public void setCuotaspagada(int Cuotaspagada) {
        this.Cuotaspagada = Cuotaspagada;
    }

    public int getCuotasPactada() {
        return CuotasPactada;
    }

    public void setCuotasPactada(int CuotasPactada) {
        this.CuotasPactada = CuotasPactada;
    }

    public int getDiasMora() {
        return DiasMora;
    }

    public void setDiasMora(int DiasMora) {
        this.DiasMora = DiasMora;
    }

    public String getProductos() {
        return Productos;
    }

    public void setProductos(String Productos) {
        this.Productos = Productos;
    }

    public String getFechaFencimiento() {
        return FechaFencimiento;
    }

    public void setFechaFencimiento(String FechaFencimiento) {
        this.FechaFencimiento = FechaFencimiento;
    }

    public String getFechaCastigo() {
        return FechaCastigo;
    }

    public void setFechaCastigo(String FechaCastigo) {
        this.FechaCastigo = FechaCastigo;
    }

    public String getMarcaRenegociado() {
        return MarcaRenegociado;
    }

    public void setMarcaRenegociado(String MarcaRenegociado) {
        this.MarcaRenegociado = MarcaRenegociado;
    }

    public int getNumeroDemandas() {
        return NumeroDemandas;
    }

    public void setNumeroDemandas(int NumeroDemandas) {
        this.NumeroDemandas = NumeroDemandas;
    }

    public String getMesesCastigo() {
        return MesesCastigo;
    }

    public void setMesesCastigo(String MesesCastigo) {
        this.MesesCastigo = MesesCastigo;
    }

    public String getCapital() {
        return Capital;
    }

    public void setCapital(String Capital) {
        this.Capital = Capital;
    }

    public String getInteres() {
        return Interes;
    }

    public void setInteres(String Interes) {
        this.Interes = Interes;
    }

    public String getHonorarioJudicial() {
        return HonorarioJudicial;
    }

    public void setHonorarioJudicial(String HonorarioJudicial) {
        this.HonorarioJudicial = HonorarioJudicial;
    }

    public float getHonorarioJuducial() {
        return HonorarioJuducial;
    }

    public void setHonorarioJuducial(float HonorarioJuducial) {
        this.HonorarioJuducial = HonorarioJuducial;
    }

    public float getMontoCondonar() {
        return MontoCondonar;
    }

    public void setMontoCondonar(float MontoCondonar) {
        this.MontoCondonar = MontoCondonar;
    }

    public float getMontoARecibir() {
        return MontoARecibir;
    }

    public void setMontoARecibir(float MontoARecibir) {
        this.MontoARecibir = MontoARecibir;
    }

    public float getCapitalCondonado() {
        return CapitalCondonado;
    }

    public void setCapitalCondonado(float CapitalCondonado) {
        this.CapitalCondonado = CapitalCondonado;
    }

    public float getCapitalARecibir() {
        return CapitalARecibir;
    }

    public void setCapitalARecibir(float CapitalARecibir) {
        this.CapitalARecibir = CapitalARecibir;
    }

    public float getHonorarioJuducialCondonado() {
        return HonorarioJuducialCondonado;
    }

    public void setHonorarioJuducialCondonado(float HonorarioJuducialCondonado) {
        this.HonorarioJuducialCondonado = HonorarioJuducialCondonado;
    }

    public float getPorcentajecondonar() {
        return Porcentajecondonar;
    }

    public void setPorcentajecondonar(float Porcentajecondonar) {
        this.Porcentajecondonar = Porcentajecondonar;
    }

    public float getValorUF() {
        return valorUF;
    }

    public void setValorUF(float valorUF) {
        this.valorUF = valorUF;
    }

    public String getMontoCondonarPesos() {
        return MontoCondonarPesos;
    }

    public void setMontoCondonarPesos(String MontoCondonarPesos) {
        this.MontoCondonarPesos = MontoCondonarPesos;
    }

    public float getInteresCondonado() {
        return InteresCondonado;
    }

    public void setInteresCondonado(float InteresCondonado) {
        this.InteresCondonado = InteresCondonado;
    }

    public float getInteresARecibir() {
        return InteresARecibir;
    }

    public void setInteresARecibir(float InteresARecibir) {
        this.InteresARecibir = InteresARecibir;
    }

    public String getInteresCondonadoPesos() {
        return InteresCondonadoPesos;
    }

    public void setInteresCondonadoPesos(String InteresCondonadoPesos) {
        this.InteresCondonadoPesos = InteresCondonadoPesos;
    }

    public String getInteresARecibirPesos() {
        return InteresARecibirPesos;
    }

    public void setInteresARecibirPesos(String InteresARecibirPesos) {
        this.InteresARecibirPesos = InteresARecibirPesos;
    }

    public String getHonorarioJudicialPesos() {
        return HonorarioJudicialPesos;
    }

    public void setHonorarioJudicialPesos(String HonorarioJudicialPesos) {
        this.HonorarioJudicialPesos = HonorarioJudicialPesos;
    }

    public String getHonorarioJudicialCondonadoPesos() {
        return HonorarioJudicialCondonadoPesos;
    }

    public void setHonorarioJudicialCondonadoPesos(String HonorarioJudicialCondonadoPesos) {
        this.HonorarioJudicialCondonadoPesos = HonorarioJudicialCondonadoPesos;
    }

    public String getHonorarioJudicialRecibidoPesos() {
        return HonorarioJudicialRecibidoPesos;
    }

    public void setHonorarioJudicialRecibidoPesos(String HonorarioJudicialRecibidoPesos) {
        this.HonorarioJudicialRecibidoPesos = HonorarioJudicialRecibidoPesos;
    }

    public String getHonJudCondSobreCapitalPesos() {
        return HonJudCondSobreCapitalPesos;
    }

    public void setHonJudCondSobreCapitalPesos(String HonJudCondSobreCapitalPesos) {
        this.HonJudCondSobreCapitalPesos = HonJudCondSobreCapitalPesos;
    }

    public String getHonJudRecSobreCapitalPesos() {
        return HonJudRecSobreCapitalPesos;
    }

    public void setHonJudRecSobreCapitalPesos(String HonJudRecSobreCapitalPesos) {
        this.HonJudRecSobreCapitalPesos = HonJudRecSobreCapitalPesos;
    }

    public float getHONOR_F1() {
        return HONOR_F1;
    }

    public void setHONOR_F1(float HONOR_F1) {
        this.HONOR_F1 = HONOR_F1;
    }
    
    public String getMoraEnPesosChileno() {
        return MoraEnPesosChileno;
    }

    public void setMoraEnPesosChileno(String MoraEnPesosChileno) {
        this.MoraEnPesosChileno = MoraEnPesosChileno;
    }
    
    public String getSaldoEnPesosChileno() {
        return SaldoEnPesosChileno;
    }

    public void setSaldoEnPesosChileno(String SaldoEnPesosChileno) {
        this.SaldoEnPesosChileno = SaldoEnPesosChileno;
    }
    
    
}
