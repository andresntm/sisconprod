/**
 * Condonacion.jgetVDE_SGNSava - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscon.entidades;

import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author exesilr
 */
public class ResumenCondonacionesInforme {

    public ResumenCondonacionesInforme() {
    }

    String Demora_segundos;
    String Demora_horas;
    String Demora_dias;
    String tiempo_aprobacion;
    String tiempo_aplicacion;
    String Aprovado_Por;
    String numero_oficina;
    String desc_oficina;
    String Minima_Fecha_Castigo;

    String fld_ced;
    String id_condonacion;
    String Ejecutivo_Origen;
    String monto_total_recibit;
    String SALDO_TOTAL;
    String Porcentaje_COND_CAP;
    String SALDO_CONDONADO;
    String Porcentaje_PAGO_CAP;
    String SALDO_PAGO;
    String INTERES;
    String Porcentaje_COND_INT;
    String INT_CONDONADO;
    String Porcentaje_PAGO_INT;
    String INT_PAGADO;
    String Porcentaje_PAGO_HON;
    String HON_PAGO;
    String MONTO_A_RECIBIR;
    String VDE_SGN;
    String PROVICION;
    String MONTO_TOTAL_PAGO;

    public String getRUT_CLIENTE() {
        return RUT_CLIENTE;
    }

    public void setRUT_CLIENTE(String RUT_CLIENTE) {
        this.RUT_CLIENTE = RUT_CLIENTE;
    }
    String RUT_CLIENTE;
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public String getKPI() {
        return KPI;
    }

    public void setKPI(String KPI) {
        this.KPI = KPI;
    }
    String KPI;

    public ResumenCondonacionesInforme(String Demora_segundos, String Demora_horas, String Demora_dias, String tiempo_aprobacion, String tiempo_aplicacion, String Aprovado_Por, String numero_oficina, String desc_oficina, String Minima_Fecha_Castigo, String monto_total_recibit, String fld_ced, String id_condonacion, String Ejecutivo_Origen, String SALDO_TOTAL, String Porcentaje_COND_CAP, String SALDO_CONDONADO, String Porcentaje_PAGO_CAP, String SALDO_PAGO, String INTERES, String Porcentaje_COND_INT, String INT_CONDONADO, String Porcentaje_PAGO_INT, String INT_PAGADO, String Porcentaje_PAGO_HON, String HON_PAGO, String MONTO_A_RECIBIR, String VDE_SGN, String PROVICION, String MONTO_TOTAL_PAGO) {
        this.Demora_segundos = Demora_segundos;
        this.Demora_horas = Demora_horas;
        this.Demora_dias = Demora_dias;
        this.tiempo_aprobacion = tiempo_aprobacion;
        this.tiempo_aplicacion = tiempo_aplicacion;
        this.Aprovado_Por = Aprovado_Por;
        this.numero_oficina = numero_oficina;
        this.desc_oficina = desc_oficina;
        this.Minima_Fecha_Castigo = Minima_Fecha_Castigo;
        this.monto_total_recibit = monto_total_recibit;
        this.fld_ced = fld_ced;
        this.id_condonacion = id_condonacion;
        this.Ejecutivo_Origen = Ejecutivo_Origen;
        this.SALDO_TOTAL = SALDO_TOTAL;
        this.Porcentaje_COND_CAP = Porcentaje_COND_CAP;
        this.SALDO_CONDONADO = SALDO_CONDONADO;
        this.Porcentaje_PAGO_CAP = Porcentaje_PAGO_CAP;
        this.SALDO_PAGO = SALDO_PAGO;
        this.INTERES = INTERES;
        this.Porcentaje_COND_INT = Porcentaje_COND_INT;
        this.INT_CONDONADO = INT_CONDONADO;
        this.Porcentaje_PAGO_INT = Porcentaje_PAGO_INT;
        this.INT_PAGADO = INT_PAGADO;
        this.Porcentaje_PAGO_HON = Porcentaje_PAGO_HON;
        this.HON_PAGO = HON_PAGO;
        this.MONTO_A_RECIBIR = MONTO_A_RECIBIR;
        this.VDE_SGN = VDE_SGN;
        this.PROVICION = PROVICION;
        this.MONTO_TOTAL_PAGO = MONTO_TOTAL_PAGO;
    }

    public String getDemora_segundos() {
        return Demora_segundos;
    }

    public void setDemora_segundos(String Demora_segundos) {
        this.Demora_segundos = Demora_segundos;
    }

    public String getDemora_horas() {
        return Demora_horas;
    }

    public void setDemora_horas(String Demora_horas) {
        this.Demora_horas = Demora_horas;
    }

    public String getDemora_dias() {
        return Demora_dias;
    }

    public void setDemora_dias(String Demora_dias) {
        this.Demora_dias = Demora_dias;
    }

    public String getTiempo_aprobacion() {
        return tiempo_aprobacion;
    }

    public void setTiempo_aprobacion(String tiempo_aprobacion) {
        this.tiempo_aprobacion = tiempo_aprobacion;
    }

    public String getTiempo_aplicacion() {
        return tiempo_aplicacion;
    }

    public void setTiempo_aplicacion(String tiempo_aplicacion) {
        this.tiempo_aplicacion = tiempo_aplicacion;
    }

    public String getAprovado_Por() {
        return Aprovado_Por;
    }

    public void setAprovado_Por(String Aprovado_Por) {
        this.Aprovado_Por = Aprovado_Por;
    }

    public String getNumero_oficina() {
        return numero_oficina;
    }

    public void setNumero_oficina(String numero_oficina) {
        this.numero_oficina = numero_oficina;
    }

    public String getDesc_oficina() {
        return desc_oficina;
    }

    public void setDesc_oficina(String desc_oficina) {
        this.desc_oficina = desc_oficina;
    }

    public String getMinima_Fecha_Castigo() {
        return Minima_Fecha_Castigo;
    }

    public void setMinima_Fecha_Castigo(String Minima_Fecha_Castigo) {
        this.Minima_Fecha_Castigo = Minima_Fecha_Castigo;
    }

    public String getMonto_total_recibit() {
        return monto_total_recibit;
    }

    public String getMonto_total_recibitS() {
//        return nf.format(monto_total_capital).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(Float.parseFloat(monto_total_recibit)).replaceAll(",0*$", "");
    }

    public void setMonto_total_recibit(String monto_total_recibit) {
        this.monto_total_recibit = monto_total_recibit;
    }

    public String getFld_ced() {
        return fld_ced;
    }

    public void setFld_ced(String fld_ced) {
        this.fld_ced = fld_ced;
    }

    public String getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(String id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getEjecutivo_Origen() {
        return Ejecutivo_Origen;
    }

    public void setEjecutivo_Origen(String Ejecutivo_Origen) {
        this.Ejecutivo_Origen = Ejecutivo_Origen;
    }

    public String getSALDO_TOTAL() {
        return SALDO_TOTAL;
    }

    public String getSALDO_TOTALS() {
        // return ;
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(Float.parseFloat(SALDO_TOTAL)).replaceAll(",0*$", "");
    }

    public void setSALDO_TOTAL(String SALDO_TOTAL) {
        this.SALDO_TOTAL = SALDO_TOTAL;
    }

    public String getPorcentaje_COND_CAP() {
        return Porcentaje_COND_CAP;
    }

    public void setPorcentaje_COND_CAP(String Porcentaje_COND_CAP) {
        this.Porcentaje_COND_CAP = Porcentaje_COND_CAP;
    }

    public String getSALDO_CONDONADO() {
        return SALDO_CONDONADO;
    }

    public void setSALDO_CONDONADO(String SALDO_CONDONADO) {
        this.SALDO_CONDONADO = SALDO_CONDONADO;
    }

    public String getPorcentaje_PAGO_CAP() {
        return Porcentaje_PAGO_CAP;
    }

    public void setPorcentaje_PAGO_CAP(String Porcentaje_PAGO_CAP) {
        this.Porcentaje_PAGO_CAP = Porcentaje_PAGO_CAP;
    }

    public String getSALDO_PAGO() {
        return SALDO_PAGO;
    }

    public void setSALDO_PAGO(String SALDO_PAGO) {
        this.SALDO_PAGO = SALDO_PAGO;
    }

    public String getINTERES() {
        return INTERES;
    }

    public void setINTERES(String INTERES) {
        this.INTERES = INTERES;
    }

    public String getPorcentaje_COND_INT() {
        return Porcentaje_COND_INT;
    }

    public void setPorcentaje_COND_INT(String Porcentaje_COND_INT) {
        this.Porcentaje_COND_INT = Porcentaje_COND_INT;
    }

    public String getINT_CONDONADO() {
        return INT_CONDONADO;
    }

    public void setINT_CONDONADO(String INT_CONDONADO) {
        this.INT_CONDONADO = INT_CONDONADO;
    }

    public String getPorcentaje_PAGO_INT() {
        return Porcentaje_PAGO_INT;
    }

    public void setPorcentaje_PAGO_INT(String Porcentaje_PAGO_INT) {
        this.Porcentaje_PAGO_INT = Porcentaje_PAGO_INT;
    }

    public String getINT_PAGADO() {
        return INT_PAGADO;
    }

    public void setINT_PAGADO(String INT_PAGADO) {
        this.INT_PAGADO = INT_PAGADO;
    }

    public String getPorcentaje_PAGO_HON() {
        return Porcentaje_PAGO_HON;
    }

    public void setPorcentaje_PAGO_HON(String Porcentaje_PAGO_HON) {
        this.Porcentaje_PAGO_HON = Porcentaje_PAGO_HON;
    }

    public String getHON_PAGO() {
        return HON_PAGO;
    }

    public void setHON_PAGO(String HON_PAGO) {
        this.HON_PAGO = HON_PAGO;
    }

    public String getMONTO_A_RECIBIR() {
        return MONTO_A_RECIBIR;
    }

    public void setMONTO_A_RECIBIR(String MONTO_A_RECIBIR) {
        this.MONTO_A_RECIBIR = MONTO_A_RECIBIR;
    }

    public String getVDE_SGN() {
        return VDE_SGN;
    }

    public String getVDE_SGNS() {
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(Float.parseFloat(VDE_SGN)).replaceAll(",0*$", "");

    }

    public void setVDE_SGN(String VDE_SGN) {
        this.VDE_SGN = VDE_SGN;
    }

    public String getPROVICION() {
        return PROVICION;
    }

    public String getPROVICIONS() {
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(Float.parseFloat(PROVICION)).replaceAll(",0*$", "");

    }

    public void setPROVICION(String PROVICION) {
        this.PROVICION = PROVICION;
    }

    public String getMONTO_TOTAL_PAGO() {
        return MONTO_TOTAL_PAGO;
    }

    public String getMONTO_TOTAL_PAGOS() {
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(Float.parseFloat(MONTO_TOTAL_PAGO)).replaceAll(",0*$", "");
        // return ;
    }

    public void setMONTO_TOTAL_PAGO(String MONTO_TOTAL_PAGO) {
        this.MONTO_TOTAL_PAGO = MONTO_TOTAL_PAGO;
    }

}
