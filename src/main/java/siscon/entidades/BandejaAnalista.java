/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class BandejaAnalista {
String fecha;
String rut;
String responsable;
String hora_de_llegada;
String hora_de_salida;
String tiempo_de_respuesta;
String monto_a_recibir;
String respuesta;
String ejecutivo;
String aprobacion;
String resena;
     public BandejaAnalista(String fecha, String rut, String responsable, String hora_de_llegada, String hora_de_salida, String tiempo_de_respuesta, String monto_a_recibir, String respuesta, String ejecutivo, String aprobacion, String resena) {
        this.fecha = fecha;
        this.rut = rut;
        this.responsable = responsable;
        this.hora_de_llegada = hora_de_llegada;
        this.hora_de_salida = hora_de_salida;
        this.tiempo_de_respuesta = tiempo_de_respuesta;
        this.monto_a_recibir = monto_a_recibir;
        this.respuesta = respuesta;
        this.ejecutivo = ejecutivo;
        this.aprobacion = aprobacion;
        this.resena = resena;
    }
           
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getHora_de_llegada() {
        return hora_de_llegada;
    }

    public void setHora_de_llegada(String hora_de_llegada) {
        this.hora_de_llegada = hora_de_llegada;
    }

    public String getHora_de_salida() {
        return hora_de_salida;
    }

    public void setHora_de_salida(String hora_de_salida) {
        this.hora_de_salida = hora_de_salida;
    }

    public String getTiempo_de_respuesta() {
        return tiempo_de_respuesta;
    }

    public void setTiempo_de_respuesta(String tiempo_de_respuesta) {
        this.tiempo_de_respuesta = tiempo_de_respuesta;
    }

    public String getMonto_a_recibir() {
        return monto_a_recibir;
    }

    public void setMonto_a_recibir(String monto_a_recibir) {
        this.monto_a_recibir = monto_a_recibir;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public String getResena() {
        return resena;
    }

    public void setResena(String resena) {
        this.resena = resena;
    }


}
