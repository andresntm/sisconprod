/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class UsrModfReglaCapital {



    
    

   int is_usr_modif;
   private int PorcentajeActual;
   private int PorcentajeAnterior;
   private float f_PorcentajeActual;
   private float f_PorcentajeAnterior;
   
   
   
    public float getF_PorcentajeActual() {
        return f_PorcentajeActual;
    }

    public void setF_PorcentajeActual(float f_PorcentajeActual) {
        this.f_PorcentajeActual = f_PorcentajeActual;
    }

    public float getF_PorcentajeAnterior() {
        return f_PorcentajeAnterior;
    }

    public void setF_PorcentajeAnterior(float f_PorcentajeAnterior) {
        this.f_PorcentajeAnterior = f_PorcentajeAnterior;
    }
    
   // String filian;
    
    
    public int getPorcentajeActual() {
        return PorcentajeActual;
    }

    public void setPorcentajeActual(int PorcentajeActual) {
        this.PorcentajeActual = PorcentajeActual;
    }

    public int getPorcentajeAnterior() {
        return PorcentajeAnterior;
    }

    public void setPorcentajeAnterior(int PorcentajeAnterior) {
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaCapital(int PorcentajeActual, int PorcentajeAnterior) {
        this.PorcentajeActual = PorcentajeActual;
        this.PorcentajeAnterior = PorcentajeAnterior;
    }

    public UsrModfReglaCapital() {
        
        this.PorcentajeActual=0;
        this.PorcentajeAnterior=0;
        this.f_PorcentajeActual=0;
        this.f_PorcentajeAnterior=0;
        this.setIs_usr_modif(0);
    }
    public int getIs_usr_modif() {
        return is_usr_modif;
    }

    public void setIs_usr_modif(int is_usr_modif) {
        this.is_usr_modif = is_usr_modif;
    }
    
    
}
