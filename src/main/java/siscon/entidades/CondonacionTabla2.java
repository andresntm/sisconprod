/**
 * Condonacion.java - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscon.entidades;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * @author exesilr
 */
public class CondonacionTabla2 {

    /**
     * @return the fecha_Cambio_Estado
     */
    public Timestamp getFecha_Cambio_Estado() {
        return fecha_Cambio_Estado;
    }

    /**
     * @return the fecha_Cambio_Estado
     */
    public String getFecha_Cambio_Estado_formateado() {
        String retorno = null;
        if (fecha_Cambio_Estado == null) {
            retorno = "Sin datos";
        } else {
            retorno = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fecha_Cambio_Estado);
        }
        return retorno;
    }

    /**
     * @param fecha_Cambio_Estado the fecha_Cambio_Estado to set
     */
    public void setFecha_Cambio_Estado(Timestamp fecha_Cambio_Estado) {
        this.fecha_Cambio_Estado = fecha_Cambio_Estado;
    }

    /**
     * @return the tipo_prorroga
     */
    public String getTipo_prorroga() {
        return tipo_prorroga;
    }

    /**
     * @param tipo_prorroga the tipo_prorroga to set
     */
    public void setTipo_prorroga(String tipo_prorroga) {
        this.tipo_prorroga = tipo_prorroga;
    }

    /**
     * @return the fec_prorroga
     */
    public Timestamp getFec_prorroga() {
        return fec_prorroga;
    }

    public String getFec_prorroga_Formateado() {
            return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fec_prorroga);
    }

    /**
     * @param fec_prorroga the fec_prorroga to set
     */
    public void setFec_prorroga(Timestamp fec_prorroga) {
        this.fec_prorroga = fec_prorroga;
    }

    /**
     * @return the tipo_rechazo
     */
    public String getTipo_rechazo() {
        return tipo_rechazo;
    }

    /**
     * @param tipo_rechazo the tipo_rechazo to set
     */
    public void setTipo_rechazo(String tipo_rechazo) {
        this.tipo_rechazo = tipo_rechazo;
    }

    /**
     * @return the fec_rechazo
     */
    public Timestamp getFec_rechazo() {
        return fec_rechazo;
    }

    /**
     * @return the fec_rechazo
     */
    public String getFec_rechazo_Formateado() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fec_rechazo);
    }

    /**
     * @param fec_rechazo the fec_rechazo to set
     */
    public void setFec_rechazo(Timestamp fec_rechazo) {
        this.fec_rechazo = fec_rechazo;
    }

    /**
     * @return the cedente
     */
    public String getCedente() {
        return cedente;
    }

    /**
     * @param cedente the cedente to set
     */
    public void setCedente(String cedente) {
        this.cedente = cedente;
    }

    public CondonacionTabla2(int id_condonacion, String timestap, String estado, String comentario_resna, float monto_total_condonado, float monto_total_recibit, int di_num_opers, float monto_total_capital, String tipocondonacion, String usuariocondona, String _rutCliente) {
        this.id_condonacion = id_condonacion;
        this.timestap = timestap;
        this.estado = estado;
        this.comentario_resna = comentario_resna;
        this.monto_total_condonado = monto_total_condonado;
        this.monto_total_recibit = monto_total_recibit;
        this.di_num_opers = di_num_opers;
        this.monto_total_capital = monto_total_capital;
        this.tipocondonacion = tipocondonacion;
        this.usuariocondona = usuariocondona;
        this.regla = "Regla 55";
        this.num_reparos = 0;
        this.RutcCliente = _rutCliente;
    }

    public CondonacionTabla2() {
    }

    public int getId_condonacion() {
        return id_condonacion;
    }

    public void setId_condonacion(int id_condonacion) {
        this.id_condonacion = id_condonacion;
    }

    public String getTimestap() {
        return timestap;
    }

    public void setTimestap(String timestap) {
        this.timestap = timestap;
    }

    public String getRegla() {
        return regla;
    }

    public void setRegla(String regla) {
        this.regla = regla;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getComentario_resna() {
        return comentario_resna;
    }

    public void setComentario_resna(String comentario_resna) {
        this.comentario_resna = comentario_resna;
    }

    public float getMonto_total_condonado() {
        return monto_total_condonado;
    }

    public void setMonto_total_condonado(float monto_total_condonado) {
        this.monto_total_condonado = monto_total_condonado;
    }

    public float getMonto_total_recibit() {
        return monto_total_recibit;
    }

    public void setMonto_total_recibit(float monto_total_recibit) {
        this.monto_total_recibit = monto_total_recibit;
    }

    public int getDi_num_opers() {
        return di_num_opers;
    }

    public void setDi_num_opers(int di_num_opers) {
        this.di_num_opers = di_num_opers;
    }

    public float getMonto_total_capital() {
        return monto_total_capital;
    }

    public void setMonto_total_capital(float monto_total_capital) {
        this.monto_total_capital = monto_total_capital;
    }

    public String getTipocondonacion() {
        return tipocondonacion;
    }

    public void setTipocondonacion(String tipocondonacion) {
        this.tipocondonacion = tipocondonacion;
    }

    public String getUsuariocondona() {
        return usuariocondona;
    }

    public void setUsuariocondona(String usuariocondona) {
        this.usuariocondona = usuariocondona;
    }

    public int getNum_reparos() {
        return num_reparos;
    }

    public void setNum_reparos(int num_reparos) {
        this.num_reparos = num_reparos;
    }

    int num_reparos;
    int id_condonacion;
    String timestap;

    String regla;
    String estado;
    String comentario_resna;
    float monto_total_condonado;
    float monto_total_recibit;
    int di_num_opers;
    float monto_total_capital;
    String tipocondonacion;
    private String cedente;
    String usuariocondona;
    //Cosorio rechazos y prorrogas
    private String tipo_rechazo;
    private Timestamp fec_rechazo;
    private String tipo_prorroga;
    private Timestamp fec_prorroga;
    private Timestamp fecha_Cambio_Estado;

    public String getRutcCliente() {
        return RutcCliente;
    }

    public void setRutcCliente(String RutcCliente) {
        this.RutcCliente = RutcCliente;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }

    public void setNombreCliente(String NombreCliente) {
        this.NombreCliente = NombreCliente;
    }
    String RutcCliente;
    String NombreCliente;
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public String getMonto_total_capitalS() {
//        return nf.format(monto_total_capital).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(monto_total_capital).replaceAll(",0*$", "");
    }

    public String getMonto_total_recibitS() {
//        return nf.format(monto_total_recibit).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(monto_total_recibit).replaceAll(",0*$", "");
    }

    public String getMonto_total_condonadoS() {
//        return nf.format(monto_total_condonado).replaceFirst("Ch", "");
        return NumberFormat.getCurrencyInstance(new Locale("es", "CL")).format(monto_total_condonado).replaceAll(",0*$", "");
    }
}
