/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.util.List;
import org.zkoss.zul.ListModelList;
import siscon.entidades.Perfil;
import siscon.entidades.RangoFecha;

/**
 *
 * @author excosoc
 */
public class ReglaVista {

    private int id_regla;
    private String descripcion;
    private boolean activo;
    private String classActivo;
    private int porCap;
    private int porInt;
    private int porHon;
    private int rangoF;
    private int rangoM;
    private ListModelList<RangoFecha> lRangoF;
    private ListModelList<RangoMonetario> lRangoM;
    private ListModelList<ReglaVista> lReglaVistaTotal;

    public ReglaVista() {
    }

    public ReglaVista(int id_regla,
            String descripcion,
            boolean activo,
            int porCap,
            int porInt,
            int porHon,
            int rangoF,
            int rangoM,
            List<RangoFecha> lRangoF,
            List<RangoMonetario> lRangoM) {

        this.id_regla = id_regla;
        this.descripcion = descripcion;
        this.activo = activo;
        this.porCap = porCap;
        this.porInt = porInt;
        this.porHon = porHon;
        this.rangoF = rangoF;
        this.rangoM = rangoM;
        this.lRangoF = new ListModelList(lRangoF);

        setClassActivo();

        for (RangoFecha ranF : lRangoF) {
            if (ranF.getId_rangof() == rangoF) {
                this.lRangoF.addSelection(ranF);
            }
        }

        this.lRangoM = new ListModelList(lRangoM);

        for (RangoMonetario ranM : lRangoM) {
            if (ranM.getId_rangom() == rangoM) {
                this.lRangoM.addSelection(ranM);
            }
        }

        this.lReglaVistaTotal = new ListModelList(lRangoM);

        for (RangoMonetario ranM : lRangoM) {
            if (ranM.getId_rangom() == rangoM) {
                this.lRangoM.addSelection(ranM);
            }
        }

    }

    public ReglaVista(int id_regla,
            String descripcion,
            boolean activo,
            int porCap,
            int porInt,
            int porHon,
            int rangoF,
            int rangoM,
            List<RangoFecha> lRangoF,
            List<RangoMonetario> lRangoM,
            List<ReglaVista> lReglaVistaTotal) {

        this.id_regla = id_regla;
        this.descripcion = descripcion;
        this.activo = activo;
        this.porCap = porCap;
        this.porInt = porInt;
        this.porHon = porHon;
        this.rangoF = rangoF;
        this.rangoM = rangoM;
        this.lRangoF = new ListModelList(lRangoF);

        setClassActivo();

        for (RangoFecha ranF : lRangoF) {
            if (ranF.getId_rangof() == rangoF) {
                this.lRangoF.addSelection(ranF);
            }
        }

        this.lRangoM = new ListModelList(lRangoM);

        for (RangoMonetario ranM : lRangoM) {
            if (ranM.getId_rangom() == rangoM) {
                this.lRangoM.addSelection(ranM);
            }
        }

        this.lReglaVistaTotal = new ListModelList(lReglaVistaTotal);

        for (ReglaVista relV : lReglaVistaTotal) {
            if (relV.getId_regla() == id_regla) {
                this.lReglaVistaTotal.addSelection(relV);
            }
        }

    }

    public ReglaVista(int id_regla,
            String descripcion,
            int porCap,
            int porInt,
            int porHon) {

        this.id_regla = id_regla;
        this.descripcion = descripcion;
        this.porCap = porCap;
        this.porInt = porInt;
        this.porHon = porHon;
    }

    /**
     * @return the id_regla
     */
    public int getId_regla() {
        return id_regla;
    }

    /**
     * @param id_regla the id_regla to set
     */
    public void setId_regla(int id_regla) {
        this.id_regla = id_regla;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the porCap
     */
    public int getPorCap() {
        return porCap;
    }

    /**
     * @param porCap the porCap to set
     */
    public void setPorCap(int porCap) {
        this.porCap = porCap;
    }

    /**
     * @return the porInt
     */
    public int getPorInt() {
        return porInt;
    }

    /**
     * @param porInt the porInt to set
     */
    public void setPorInt(int porInt) {
        this.porInt = porInt;
    }

    /**
     * @return the porHon
     */
    public int getPorHon() {
        return porHon;
    }

    /**
     * @param porHon the porHon to set
     */
    public void setPorHon(int porHon) {
        this.porHon = porHon;
    }

    /**
     * @return the rangoF
     */
    public int getRangoF() {
        return rangoF;
    }

    /**
     * @param rangoF the rangoF to set
     */
    public void setRangoF(int rangoF) {
        this.rangoF = rangoF;
    }

    /**
     * @return the rangoM
     */
    public int getRangoM() {
        return rangoM;
    }

    /**
     * @param rangoM the rangoM to set
     */
    public void setRangoM(int rangoM) {
        this.rangoM = rangoM;
    }

    /**
     * @return the lRangoF
     */
    public ListModelList<RangoFecha> getlRangoF() {
        return lRangoF;
    }

    /**
     * @param lRangoF the lRangoF to set
     */
    public void setlRangoF(ListModelList<RangoFecha> lRangoF) {
        this.lRangoF = lRangoF;
    }

    /**
     * @return the lRangoM
     */
    public ListModelList<RangoMonetario> getlRangoM() {
        return lRangoM;
    }

    /**
     * @param lRangoM the lRangoM to set
     */
    public void setlRangoM(ListModelList<RangoMonetario> lRangoM) {
        this.lRangoM = lRangoM;
    }

    /**
     * @return the activo
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @return the classActivo
     */
    public String getClassActivo() {
        return classActivo;
    }

    /**
     * @param classActivo the classActivo to set
     */
    public void setClassActivo() {
        if (activo) {
            this.classActivo = "z-icon-check-circle-o activo";
        } else {
            this.classActivo = "z-icon-times-circle-o noActivo";
        }
    }

    /**
     * @return the lReglaVistaTotal
     */
    public ListModelList<ReglaVista> getlReglaVistaTotal() {
        return lReglaVistaTotal;
    }

    /**
     * @param lReglaVistaTotal the lReglaVistaTotal to set
     */
    public void setlReglaVistaTotal(ListModelList<ReglaVista> lReglaVistaTotal) {
        this.lReglaVistaTotal = lReglaVistaTotal;
    }
}
