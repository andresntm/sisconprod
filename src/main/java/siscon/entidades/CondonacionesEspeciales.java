/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import config.MetodosGenerales;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.RowMapper;
import siscon.Session.TrackingSesion;
import siscon.entidades.implementaciones.CondEspecialesImplements;
import siscon.entidades.interfaces.CondEspecialesInterfaz;

/**
 *
 * @author excosoc
 */
public class CondonacionesEspeciales implements RowMapper<CondonacionesEspeciales>, Serializable {
      private MetodosGenerales metodo;

    /**
     * @return the tCE
     */
    public TipoCondEspecial gettCE() {
        return tCE;
    }

    /**
     * @param tCE the tCE to set
     */
    public void settCE(TipoCondEspecial tCE) {
        this.tCE = tCE;
    }

    /**
     * @return the NumCuotas
     */
    public int getNumCuotas() {
        return NumCuotas;
    }

    /**
     * @param NumCuotas the NumCuotas to set
     */
    public void setNumCuotas(int NumCuotas) {
        this.NumCuotas = NumCuotas;
    }

    /**
     * @return the id_Cond_Esp
     */
    public int getId_Cond_Esp() {
        return id_Cond_Esp;
    }

    /**
     * @param id_Cond_Esp the id_Cond_Esp to set
     */
    public void setId_Cond_Esp(int id_Cond_Esp) {
        this.id_Cond_Esp = id_Cond_Esp;
    }

    /**
     * @return the id_TipoEspecial
     */
    public int getId_TipoEspecial() {
        return id_TipoEspecial;
    }

    /**
     * @param id_TipoEspecial the id_TipoEspecial to set
     */
    public void setId_TipoEspecial(int id_TipoEspecial) {
        this.id_TipoEspecial = id_TipoEspecial;
    }

    /**
     * @return the Id_Condonacion
     */
    public int getId_Condonacion() {
        return Id_Condonacion;
    }

    /**
     * @param Id_Condonacion the Id_Condonacion to set
     */
    public void setId_Condonacion(int Id_Condonacion) {
        this.Id_Condonacion = Id_Condonacion;
    }

    /**
     * @return the FechaIngreso
     */
    public Timestamp getFechaIngreso() {
        return FechaIngreso;
    }

    /**
     * @param FechaIngreso the FechaIngreso to set
     */
    public void setFechaIngreso(Timestamp FechaIngreso) {
        this.FechaIngreso = FechaIngreso;
    }

    private int id_Cond_Esp;
    private int id_TipoEspecial;
    private int Id_Condonacion;
    private Timestamp FechaIngreso;
    private int NumCuotas;
    private CondEspecialesInterfaz cEI;
    private TipoCondEspecial tCE;
    private int id_Provision;
    private boolean activo;

    public CondonacionesEspeciales() {
         metodo = new MetodosGenerales();
             id_Cond_Esp=0;
     id_TipoEspecial=0;
     Id_Condonacion=0;
     Date date = new Date();
DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
String strDate = dateFormat.format(date);
     FechaIngreso= metodo.ConvStringDate(strDate);
    NumCuotas=0;
            id_Provision=0;
            activo=false;
        this.setcEI();
        
    }

    public CondonacionesEspeciales(int id_Cond_Esp, int id_TipoEspecial, int Id_Condonacion, Timestamp FechaIngreso) {
        this.id_Cond_Esp = id_Cond_Esp;
        this.id_TipoEspecial = id_TipoEspecial;
        this.Id_Condonacion = Id_Condonacion;
        this.FechaIngreso = FechaIngreso;
        this.setcEI();
    }

    public CondonacionesEspeciales mapRow(ResultSet rs, int rowNum) throws SQLException {
        CondonacionesEspeciales condEspecial = new CondonacionesEspeciales();

        condEspecial.id_Cond_Esp = rs.getInt("id_Cond_Esp");
        condEspecial.id_TipoEspecial = rs.getInt("id_TipoEspecial");
        condEspecial.Id_Condonacion = rs.getInt("Id_Condonacion");
        condEspecial.FechaIngreso = rs.getTimestamp("FechaIngreso");
        condEspecial.NumCuotas = rs.getInt("NumCuotas");
        condEspecial.id_Provision = rs.getInt("id_Provision");
        condEspecial.activo = rs.getBoolean("activo");
        condEspecial.settCE(new TipoCondEspecial());
        condEspecial.gettCE().getTipoCondEspecial_X_Id(condEspecial.getId_TipoEspecial());
        return condEspecial;

    }

    public CondonacionesEspeciales(int id_Cond_Esp, int id_TipoEspecial, int Id_Condonacion, Timestamp FechaIngreso, int NumCuotas) {
        this.id_Cond_Esp = id_Cond_Esp;
        this.id_TipoEspecial = id_TipoEspecial;
        this.Id_Condonacion = Id_Condonacion;
        this.FechaIngreso = FechaIngreso;
        this.NumCuotas = NumCuotas;
    }

    /**
     * @return the cEI
     */
    public CondEspecialesInterfaz getcEI() {
        return cEI;
    }

    public void setcEI() {
        this.cEI = new CondEspecialesImplements();
    }

    public boolean insertCondEspecial(CondonacionesEspeciales cE) {
        boolean ok = false;

        try {
            cE.setId_Cond_Esp(cEI.insertCondEspecial(cE));

            ok = cE.getId_Cond_Esp() != 0;
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            ok = false;
        } finally {

            return ok;
        }
    }

    public boolean deleteLogicoCondEsp(int id_condEsp) {
        boolean ok = false;

        try {
            this.setId_Cond_Esp(id_condEsp);
            ok = cEI.deleteCondEspecial(this);
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            ok = false;
        } finally {
            return ok;
        }
    }

    public CondonacionesEspeciales getCondonacionesEspeciales_X_IdCond(int id_Cond) {
        CondonacionesEspeciales cE = null;
        try {
            cE = cEI.selectCondEspecial_X_Id_Condonacion(id_Cond);
            tCE = new TipoCondEspecial();

            if (cE != null) {
                if (cE.getId_TipoEspecial() > 0) {
                    cE.settCE(tCE.getTipoCondEspecial_X_Id(cE.getId_TipoEspecial()));
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            cE = null;
        } finally {

            return cE;
        }

    }

    public CondonacionesEspeciales getCondonacionesEspeciales_X_Id(int id) {
        CondonacionesEspeciales cE = null;
        try {
            cE = cEI.selectCondEspecial_X_Id_CondEsp(id);
            tCE = new TipoCondEspecial();
            tCE = tCE.getTipoCondEspecial_X_Id(cE.getId_TipoEspecial());
        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            cE = null;
        } finally {

            return cE;
        }

    }

    public List<CondonacionesEspeciales> listCondonacionesEspeciales() {
        List<CondonacionesEspeciales> lCE = null;
        try {
            lCE = cEI.listCondEspecial();

        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            lCE = null;
        } finally {

            return lCE;
        }
    }

    public List<CondonacionesEspeciales> listCondonacionesEspeciales_X_Id_TipoCondEsp(int id_tipCondEsp) {
        List<CondonacionesEspeciales> lCE = null;
        try {
            lCE = cEI.listCondEspecial_X_Id_TipCondEsp(id_tipCondEsp);

        } catch (Exception ex) {
            Logger.getLogger(TrackingSesion.class.getName()).log(Level.WARNING, ex.getMessage(), ex);
            System.out.println(ex.getMessage());
            lCE = null;
        } finally {

            return lCE;
        }
    }

    /**
     * @return the id_Provision
     */
    public int getId_Provision() {
        return id_Provision;
    }

    /**
     * @param id_Provision the id_Provision to set
     */
    public void setId_Provision(int id_Provision) {
        this.id_Provision = id_Provision;
    }

    /**
     * @return the activo
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

}
