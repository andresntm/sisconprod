/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author excosoc
 */
public class ReportesApllicados implements RowMapper<ReportesApllicados> {

    private int id;
    private int oficina;
    private String nombre_deudor;
    private String rut_cliente;
    private String operacion;
    private Timestamp fecha_castigo;
    private Timestamp fecha_aprobacion;
    private String aprobado_por;
    private String presentada_por;
    private String aca_normaliza;
    private int monto_recibido;
    private int capital;
    private int vd;
    private int intereses;
    private int acumulado;
    private int solo_capital;
    private int total_capital;
    private int total_intercentro;
    private int monto_condonado_capital;
    private int monto_condonado_interes;
    private int total_condonado;
    private int porcentaje_capital;
    private int porcentaje_interes;
    private int honorario_gastos;
    private int costas;
    private String ejecutivo_normaliza;
    private Timestamp ddt_fechaIngreso;

    public ReportesApllicados() {
    }

    public ReportesApllicados(int id,
            int oficina,
            String nombre_deudor,
            String rut_cliente,
            String operacion,
            Timestamp fecha_castigo,
            Timestamp fecha_aprobacion,
            String aprobado_por,
            String presentada_por,
            String aca_normaliza,
            int monto_recibido,
            int capital,
            int vd,
            int intereses,
            int acumulado,
            int solo_capital,
            int total_capital,
            int total_intercentro,
            int monto_condonado_capital,
            int monto_condonado_interes,
            int total_condonado,
            int porcentaje_capital,
            int porcentaje_interes,
            int honorario_gastos,
            int costas,
            String ejecutivo_normaliza,
            Timestamp ddt_fechaIngreso) {

        this.id = id;
        this.oficina = oficina;
        this.nombre_deudor = nombre_deudor;
        this.rut_cliente = rut_cliente;
        this.operacion = operacion;
        this.fecha_castigo = fecha_castigo;
        this.fecha_aprobacion = fecha_aprobacion;
        this.aprobado_por = aprobado_por;
        this.presentada_por = presentada_por;
        this.aca_normaliza = aca_normaliza;
        this.monto_recibido = monto_recibido;
        this.capital = capital;
        this.vd = vd;
        this.intereses = intereses;
        this.acumulado = acumulado;
        this.solo_capital = solo_capital;
        this.total_capital = total_capital;
        this.total_intercentro = total_intercentro;
        this.monto_condonado_capital = monto_condonado_capital;
        this.monto_condonado_interes = monto_condonado_interes;
        this.total_condonado = total_condonado;
        this.porcentaje_capital = porcentaje_capital;
        this.porcentaje_interes = porcentaje_interes;
        this.honorario_gastos = honorario_gastos;
        this.costas = costas;
        this.ejecutivo_normaliza = ejecutivo_normaliza;
        this.ddt_fechaIngreso = ddt_fechaIngreso;
    }

    @Override
    public ReportesApllicados mapRow(ResultSet rs, int rowNum) throws SQLException {
        
        this.id = rs.getInt("id");
        this.oficina = rs.getInt("oficina");
        this.nombre_deudor = rs.getString("nombre_deudor");
        this.rut_cliente = rs.getString("rut_cliente");
        this.operacion = rs.getString("operacion");
        this.fecha_castigo = rs.getTimestamp("fecha_castigo");
        this.fecha_aprobacion = rs.getTimestamp("fecha_aprobacion");
        this.aprobado_por = rs.getString("aprobado_por");
        this.presentada_por = rs.getString("presentada_por");
        this.aca_normaliza = rs.getString("aca_normaliza");
        this.monto_recibido = rs.getInt("monto_recibido");
        this.capital = rs.getInt("capital");
        this.vd = rs.getInt("vd");
        this.intereses = rs.getInt("intereses");
        this.acumulado = rs.getInt("acumulado");
        this.solo_capital = rs.getInt("solo_capital");
        this.total_capital = rs.getInt("total_capital");
        this.total_intercentro = rs.getInt("total_intercentro");
        this.monto_condonado_capital = rs.getInt("monto_condonado_capital");
        this.monto_condonado_interes = rs.getInt("monto_condonado_interes");
        this.total_condonado = rs.getInt("total_condonado");
        this.porcentaje_capital = rs.getInt("porcentaje_capital");
        this.porcentaje_interes = rs.getInt("porcentaje_interes");
        this.honorario_gastos = rs.getInt("honorario_gastos");
        this.costas = rs.getInt("costas");
        this.ejecutivo_normaliza = rs.getString("ejecutivo_normaliza");
        this.ddt_fechaIngreso = rs.getTimestamp("ddt_fechaIngreso");
        
        return this;
    }
}
