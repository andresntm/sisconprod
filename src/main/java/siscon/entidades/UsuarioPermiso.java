/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import siscore.entidades.Sistema;

/**
 *
 * @author exesilr
 */
public class UsuarioPermiso implements Serializable {



    private static final long serialVersionUID = 1L;

    String cuenta;
    String nombre;
    int id_colaborador;
    int di_rut;
    private String areaTrab;
    //Perfil per=new Perfil();
    Set<String> perfiles = new HashSet<String>();

    public Set<Perfil> getPerfiless() {
        return perfiless;
    }
    Set<Perfil> perfiless = new HashSet<Perfil>();
    AreaTrabajo area;
    // List <Banca> bancas = new List <Banca>();
    List<Banca> bancas = new ArrayList<Banca>();
    List<Sistema> _sistema = new ArrayList<Sistema>();

    
    
    
    public AreaTrabajo getArea() {
        return area;
    }

    public void setArea(AreaTrabajo area) {
        this.area = area;
    }

    public int getId_colaborador() {
        return id_colaborador;
    }

    public void setId_colaborador(int id_colaborador) {
        this.id_colaborador = id_colaborador;
    }

    public UsuarioPermiso(String cuenta, String nombre) {
        this.cuenta = cuenta;
        this.nombre = nombre;
    }

    public UsuarioPermiso(String cuenta, String nombre, int di_rut) {
        this.cuenta = cuenta;
        this.nombre = nombre;
        this.di_rut = di_rut;
    }

    public UsuarioPermiso() {
        this.cuenta = "invitado";
        this.nombre = "invitado";
        perfiles.add("invitado");
    }

    public int getDi_rut() {
        return di_rut;
    }

    public void setDi_rut(int di_rut) {
        this.di_rut = di_rut;
    }

    public boolean isInvitado() {
        return hasRole("invitado") || "invitado".equals(cuenta);
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean hasRole(String role) {
        return perfiles.contains(role);
    }

    public void addRole(String role) {
        perfiles.add(role);
    }

    public boolean TienePerfil(Perfil perfil) {
        return perfiless.contains(perfil);
    }

    public void agregarPerfil(Perfil perfil) {
        perfiless.add(perfil);
    }

    public void agregarBanca(Banca _banca) {
        bancas.add(_banca);
    }

    public List<Banca> getBanca() {
        return this.bancas;
    }

    public void agregarSistema(Sistema _sis) {
        _sistema.add(_sis);
    }

    public List<Sistema> getSistema() {
        return this._sistema;
    }

    
    
        /**
     * @return the areaTrab
     */
    public String getAreaTrab() {
        return areaTrab;
    }

    /**
     * @param areaTrab the areaTrab to set
     */
    public void setAreaTrab(String areaTrab) {
        this.areaTrab = areaTrab;
    }
    
    
}
