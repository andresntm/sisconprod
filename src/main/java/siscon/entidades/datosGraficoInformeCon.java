/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.chart.model.CategoryModel;
import org.zkoss.chart.model.DefaultCategoryModel;

/**
 *
 * @author exesilr
 */
public class datosGraficoInformeCon {



    private static final CategoryModel model;
  private static List <GraficoTPcondonaDataGrid> DataGridList= new ArrayList<GraficoTPcondonaDataGrid>();

    static {
        model = new DefaultCategoryModel();

        try {
        model.setValue("TiempoP", "01-12-2016", convertMinute("01-12-2016","0:15:40"));
        
        model.setValue("TiempoP", "02-12-2016", convertMinute("02-12-2016","0:12:25"));
        model.setValue("TiempoP", "05-12-2016", convertMinute("05-12-2016","0:13:14"));
        model.setValue("TiempoP", "06-12-2016", convertMinute("06-12-2016","0:13:50"));
        model.setValue("TiempoP", "07-12-2016", convertMinute("07-12-2016","0:17:05"));
        model.setValue("TiempoP", "09-12-2016", convertMinute("09-12-2016","0:11:20"));
        model.setValue("TiempoP", "12-12-2016", convertMinute("12-12-2016","0:19:28"));
        model.setValue("TiempoP", "13-12-2016", convertMinute("13-12-2016","0:15:34"));
        model.setValue("TiempoP", "14-12-2016", convertMinute("14-12-2016","0:16:12"));
        model.setValue("TiempoP", "15-12-2016", convertMinute("15-12-2016","0:12:00"));
        model.setValue("TiempoP", "16-12-2016", convertMinute("16-12-2016","0:16:13"));
        model.setValue("TiempoP", "19-12-2016", convertMinute("19-12-2016","0:13:13"));
        model.setValue("TiempoP", "20-12-2016", convertMinute("20-12-2016","0:23:00"));
        model.setValue("TiempoP", "21-12-2016", convertMinute("21-12-2016","0:15:30"));
        model.setValue("TiempoP", "22-12-2016", convertMinute("22-12-2016","0:20:00"));
        model.setValue("TiempoP", "23-12-2016", convertMinute("23-12-2016","0:13:16"));
        model.setValue("TiempoP", "26-12-2016", convertMinute("26-12-2016","0:18:14"));
        model.setValue("TiempoP", "27-12-2016", convertMinute("27-12-2016","0:14:30"));
        model.setValue("TiempoP", "28-12-2016", convertMinute("28-12-2016","0:13:54"));
        model.setValue("TiempoP", "29-12-2016", convertMinute("29-12-2016","0:14:17"));
        model.setValue("TiempoP", "30-12-2016", convertMinute("30-12-2016","0:10:43"));
        
        
        model.setValue("TiempoEsperado", "01-12-2016", convertMinute("01-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "02-12-2016", convertMinute("02-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "05-12-2016", convertMinute("05-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "06-12-2016", convertMinute("06-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "07-12-2016", convertMinute("07-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "09-12-2016", convertMinute("09-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "12-12-2016", convertMinute("12-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "13-12-2016", convertMinute("13-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "14-12-2016", convertMinute("14-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "15-12-2016", convertMinute("15-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "16-12-2016", convertMinute("16-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "19-12-2016", convertMinute("19-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "20-12-2016", convertMinute("20-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "21-12-2016", convertMinute("21-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "22-12-2016", convertMinute("22-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "23-12-2016", convertMinute("23-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "26-12-2016", convertMinute("26-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "27-12-2016", convertMinute("27-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "28-12-2016", convertMinute("28-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "29-12-2016", convertMinute("29-12-2016","0:30:00"));
        model.setValue("TiempoEsperado", "30-12-2016", convertMinute("30-12-2016","0:30:00"));
        
        } catch (ParseException ex) {
            Logger.getLogger(datosGraficoInformeCon.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public datosGraficoInformeCon() {
        
        
                                if (!DataGridList.isEmpty())
			DataGridList.clear();
    }
    public static CategoryModel getCategoryModel() {

        return model;
    }
    public static List <GraficoTPcondonaDataGrid> getGridDataList() {

        return DataGridList;
    }

    private static  double convertMinute(String fecha,String hora) throws ParseException {
        
           
        
        
        double diffminutos = 0;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        d1 = format.parse("26-01-2017 " + hora);
        d2 = format.parse("26-01-2017 00:00:00");
        double diff = d1.getTime() - d2.getTime();
        diffminutos = (double) (diff / (60 * 1000)); //diffmin = (int) (diff / (60 * 1000));
       DataGridList.add(new GraficoTPcondonaDataGrid(fecha,(double)diffminutos, (double) 30.0));
        return diffminutos;

    }

}
