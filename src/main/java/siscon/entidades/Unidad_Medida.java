/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

/**
 *
 * @author excosoc
 */
public class Unidad_Medida {

    private int id_undmed;
    private String descripcion;
    private String simbolo;

    /**
     * @return the id_undmed
     */
    public int getId_undmed() {
        return id_undmed;
    }

    /**
     * @param id_undmed the id_undmed to set
     */
    public void setId_undmed(int id_undmed) {
        this.id_undmed = id_undmed;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the simbolo
     */
    public String getSimbolo() {
        return simbolo;
    }

    /**
     * @param simbolo the simbolo to set
     */
    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public Unidad_Medida() {
    }

    public Unidad_Medida(int id_undmed,
            String descripcion,
            String simbolo) {

        this.id_undmed = id_undmed;
        this.descripcion = descripcion;
        this.simbolo = simbolo;

    }

}
