/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

public class PorcentajesXYZSimulador {

    public PorcentajesXYZSimulador(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    
    public float  x;
    public float  y;
    public float  z;
    
    
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }


   


    public PorcentajesXYZSimulador() {
    }

}
