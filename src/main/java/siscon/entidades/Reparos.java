/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.entidades;

public class Reparos {

    public int di_idReparo;
    public int di_fk_IdCondonacion;
    public String dv_MotivoReparo;
    public String dv_GlosaReparo;
    public String fecha_reparo;
    public String usuario;
    public String colaborador_nombres;

    public String getCunetaOrigen() {
        return CunetaOrigen;
    }

    public void setCunetaOrigen(String CunetaOrigen) {
        this.CunetaOrigen = CunetaOrigen;
    }

    public String CunetaOrigen;

    public String getColaborador_nombres() {
        return colaborador_nombres;
    }

    public void setColaborador_nombres(String colaborador_nombres) {
        this.colaborador_nombres = colaborador_nombres;
    }

    public String getCodArea() {
        return codArea;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getFecha_reparo() {
        return fecha_reparo;
    }

    public void setFecha_reparo(String fecha_reparo) {
        this.fecha_reparo = fecha_reparo;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }
    public String codArea;

    public int getDi_idReparo() {
        return di_idReparo;
    }

    public void setDi_idReparo(int di_idReparo) {
        this.di_idReparo = di_idReparo;
    }

    public int getDi_fk_IdCondonacion() {
        return di_fk_IdCondonacion;
    }

    public void setDi_fk_IdCondonacion(int di_fk_IdCondonacion) {
        this.di_fk_IdCondonacion = di_fk_IdCondonacion;
    }

    public String getDv_MotivoReparo() {
        return dv_MotivoReparo;
    }

    public void setDv_MotivoReparo(String dv_MotivoReparo) {
        this.dv_MotivoReparo = dv_MotivoReparo;
    }

    public String getDv_GlosaReparo() {
        return dv_GlosaReparo;
    }

    public void setDv_GlosaReparo(String dv_GlosaReparo) {
        this.dv_GlosaReparo = dv_GlosaReparo;
    }

    public Reparos(int di_idReparo, int di_fk_IdCondonacion, String dv_MotivoReparo, String dv_GlosaReparo) {
        this.di_idReparo = di_idReparo;
        this.di_fk_IdCondonacion = di_fk_IdCondonacion;
        this.dv_MotivoReparo = dv_MotivoReparo;
        this.dv_GlosaReparo = dv_GlosaReparo;
    }

    public Reparos() {
    }

}
