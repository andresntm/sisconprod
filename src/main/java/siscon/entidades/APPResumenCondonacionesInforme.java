/**
 * Condonacion.java - clase para el manejo de informacion de copndonacion.
 *
 * @author Eric Silvestre
 * @version 1.0
 * @see Automobile
 */
package siscon.entidades;

/**
 *
 * @author exesilr
 */
public class APPResumenCondonacionesInforme {

    public APPResumenCondonacionesInforme() {
    }

    public APPResumenCondonacionesInforme(String total_rut, String TOTAL_CASTIGADO, String TOTAL_VDE_SGN, String TOTAL_RECIBIDO_1, String TOTAL_RECIBIDO_2, String TOTAL_TOTAL_RECIBE, String TOTAL_CAPITAL_CONDONADO, String TOTAL_INTERES_CONDONADO, String TOTAL_HONORARIO_RECIBE, String TOTAL_COSTAS_PROVISION) {
        this.total_rut = total_rut;
        this.TOTAL_CASTIGADO = TOTAL_CASTIGADO;
        this.TOTAL_VDE_SGN = TOTAL_VDE_SGN;
        this.TOTAL_RECIBIDO_1 = TOTAL_RECIBIDO_1;
        this.TOTAL_RECIBIDO_2 = TOTAL_RECIBIDO_2;
        this.TOTAL_TOTAL_RECIBE = TOTAL_TOTAL_RECIBE;
        this.TOTAL_CAPITAL_CONDONADO = TOTAL_CAPITAL_CONDONADO;
        this.TOTAL_INTERES_CONDONADO = TOTAL_INTERES_CONDONADO;
        this.TOTAL_HONORARIO_RECIBE = TOTAL_HONORARIO_RECIBE;
        this.TOTAL_COSTAS_PROVISION = TOTAL_COSTAS_PROVISION;
    }

    public String getTotal_rut() {
        return total_rut;
    }

    public void setTotal_rut(String total_rut) {
        this.total_rut = total_rut;
    }

    public String getTOTAL_CASTIGADO() {
        return TOTAL_CASTIGADO;
    }

    public void setTOTAL_CASTIGADO(String TOTAL_CASTIGADO) {
        this.TOTAL_CASTIGADO = TOTAL_CASTIGADO;
    }

    public String getTOTAL_VDE_SGN() {
        return TOTAL_VDE_SGN;
    }

    public void setTOTAL_VDE_SGN(String TOTAL_VDE_SGN) {
        this.TOTAL_VDE_SGN = TOTAL_VDE_SGN;
    }

    public String getTOTAL_RECIBIDO_1() {
        return TOTAL_RECIBIDO_1;
    }

    public void setTOTAL_RECIBIDO_1(String TOTAL_RECIBIDO_1) {
        this.TOTAL_RECIBIDO_1 = TOTAL_RECIBIDO_1;
    }

    public String getTOTAL_RECIBIDO_2() {
        return TOTAL_RECIBIDO_2;
    }

    public void setTOTAL_RECIBIDO_2(String TOTAL_RECIBIDO_2) {
        this.TOTAL_RECIBIDO_2 = TOTAL_RECIBIDO_2;
    }

    public String getTOTAL_TOTAL_RECIBE() {
        return TOTAL_TOTAL_RECIBE;
    }

    public void setTOTAL_TOTAL_RECIBE(String TOTAL_TOTAL_RECIBE) {
        this.TOTAL_TOTAL_RECIBE = TOTAL_TOTAL_RECIBE;
    }

    public String getTOTAL_CAPITAL_CONDONADO() {
        return TOTAL_CAPITAL_CONDONADO;
    }

    public void setTOTAL_CAPITAL_CONDONADO(String TOTAL_CAPITAL_CONDONADO) {
        this.TOTAL_CAPITAL_CONDONADO = TOTAL_CAPITAL_CONDONADO;
    }

    public String getTOTAL_INTERES_CONDONADO() {
        return TOTAL_INTERES_CONDONADO;
    }

    public void setTOTAL_INTERES_CONDONADO(String TOTAL_INTERES_CONDONADO) {
        this.TOTAL_INTERES_CONDONADO = TOTAL_INTERES_CONDONADO;
    }

    public String getTOTAL_HONORARIO_RECIBE() {
        return TOTAL_HONORARIO_RECIBE;
    }

    public void setTOTAL_HONORARIO_RECIBE(String TOTAL_HONORARIO_RECIBE) {
        this.TOTAL_HONORARIO_RECIBE = TOTAL_HONORARIO_RECIBE;
    }

    public String getTOTAL_COSTAS_PROVISION() {
        return TOTAL_COSTAS_PROVISION;
    }

    public void setTOTAL_COSTAS_PROVISION(String TOTAL_COSTAS_PROVISION) {
        this.TOTAL_COSTAS_PROVISION = TOTAL_COSTAS_PROVISION;
    }

    String total_rut;	
String TOTAL_CASTIGADO;	
String TOTAL_VDE_SGN;	
String TOTAL_RECIBIDO_1;	
String TOTAL_RECIBIDO_2;	
String TOTAL_TOTAL_RECIBE;	
String TOTAL_CAPITAL_CONDONADO;	
String TOTAL_INTERES_CONDONADO;	
String TOTAL_HONORARIO_RECIBE;	
String TOTAL_COSTAS_PROVISION;


}
