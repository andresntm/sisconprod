/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.graficos;

import org.zkoss.chart.Charts;
import org.zkoss.chart.Legend;
import org.zkoss.chart.PlotLine;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;
import siscon.entidades.BandejaAnalista;
import siscon.entidades.GraficoTPcondonaDataGrid;
import siscon.entidades.datosGraficoInformeCon;
import siscon.entidades.datosgraficos;

/**
 *
 * @author exesilr
 */
public class InfConTiempoRespuesta extends SelectorComposer<Window> {

    @Wire
    Charts chart;

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);
        System.out.print("\n\n\n\t\t--- {###" + Sessions.getCurrent() + "####} ---\n\n\n");
        chart.setModel(datosGraficoInformeCon.getCategoryModel());
        chart.getTitle().setX(-20);
        chart.getSubtitle().setX(-20);
        chart.getYAxis().setTitle("Dias del Mes");
        PlotLine plotLine = new PlotLine();
        plotLine.setValue(0);
        plotLine.setWidth(1);
        plotLine.setColor("#808080");
        chart.getYAxis().addPlotLine(plotLine);
        chart.getTooltip().setValueSuffix("Tiempo Promedio Respuesta");
        Legend legend = chart.getLegend();
        legend.setLayout("vertical");
        legend.setAlign("right");
        legend.setVerticalAlign("middle");
        legend.setBorderWidth(0);

    }
}
