/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMappers;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.GlosaVista;
/**
 *
 * @author exesilr
 */
public class VistaGlosaRowMapper implements RowMapper{
    
    
    	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		GlosaVista glosa_vista = new GlosaVista();
		glosa_vista.setNombreColumna(rs.getString("nombre_columna"));
		glosa_vista.setTipoDato(rs.getString("tipo_dato"));
		glosa_vista.setGlosario(rs.getString("glosario"));
		return glosa_vista;
	
        
        }
}
