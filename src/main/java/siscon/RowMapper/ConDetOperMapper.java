/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.ConDetOper;


/**
 *
 * @author exesilr
 */
public class ConDetOperMapper implements RowMapper<ConDetOper>{
 
    
            public ConDetOper mapRow(ResultSet rs, int rowNum) throws SQLException {
        ConDetOper area = new ConDetOper();
        
        
        
        area.setOpracionOriginal(rs.getString("operacion"));
        area.setEstadoOperacion(rs.getString("estadoOperacion"));
       // area.setFilian(rs.getString("filial"));
       // area.setId_colaborador(rs.getInt("id"));

        return area;
    }
    
}
