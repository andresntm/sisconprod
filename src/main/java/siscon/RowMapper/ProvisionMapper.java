/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrProvision;


/**
 *
 * @author exesilr
 */
public class ProvisionMapper implements RowMapper<UsrProvision>{
 
    
            public UsrProvision mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrProvision prov = new UsrProvision();
        
        
        
        prov.setId(rs.getInt("id"));
        prov.setFk_idColaborador(rs.getInt("fk_idColaborador"));
        prov.setRut_cliente(rs.getString("rut_cliente"));
        prov.setNfk_idCondonacion(rs.getInt("nfk_idCondonacion"));
        prov.setRegistrado(rs.getString("registrado"));
        prov.setDv_desc(rs.getString("dv_desc"));
        prov.setMonto(rs.getFloat("monto"));
        prov.setFk_idTipoProvision(rs.getInt("fk_idTipoProvision"));
        prov.setVigente(rs.getBoolean("vigente"));
        prov.setVigentex(rs.getInt("vigente"));
        prov.setTdesc(rs.getString("tdsc"));


        return prov;
    }
    
}
