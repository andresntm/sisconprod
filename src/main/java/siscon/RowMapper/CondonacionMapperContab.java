/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.BandejaCondonacionesInforme;

/**
 *
 * @author excosoc
 */
public class CondonacionMapperContab implements RowMapper<BandejaCondonacionesInforme>{
    
     public BandejaCondonacionesInforme mapRow(ResultSet rs, int rowNum) throws SQLException {
        BandejaCondonacionesInforme con = new BandejaCondonacionesInforme();
        con.setId_condonacion(rs.getInt("id_condonacion"));
        con.setTimestap(rs.getString("timestap"));
        con.setRegla(rs.getString("regla"));
        con.setEstado(rs.getString("dv_estado"));
        con.setComentario_resna(rs.getString("comentario_resna"));
        con.setMonto_total_condonado(rs.getFloat("monto_total_condonado"));
        con.setMonto_total_recibit(rs.getFloat("monto_total_recibit"));
        con.setDi_num_opers(rs.getInt("di_num_opers"));
        con.setMonto_total_capital(rs.getFloat("monto_total_capital"));
        con.setTipocondonacion(rs.getString("dv_desc"));
        con.setUsuariocondona(rs.getString("di_rut"));
        con.setRutcCliente(rs.getString("rut"));
        con.setNombreCliente(rs.getString("nombre"));
        con.setNum_reparos(rs.getInt("num_reparos"));
        con.setCedente(rs.getString("fld_ced"));
        con.setNumero_oficina(rs.getInt("numero_oficina"));
        con.setDesc_oficina(rs.getString("desc_oficina"));
        return con;
    }
    
}
