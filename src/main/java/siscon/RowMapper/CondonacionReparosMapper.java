/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.CondReparadas;

/**
 *
 * @author esilves
 */
public class CondonacionReparosMapper implements RowMapper<CondReparadas> {

    public CondReparadas mapRow(ResultSet rs, int rowNum) throws SQLException {
        CondReparadas con = new CondReparadas();
        con.setRut(rs.getString("rut"));
        con.setNombre(rs.getString("nombre"));
        con.setId_condonacion(rs.getInt("id_condonacion"));
        con.setTimestap(rs.getString("timestap"));
        con.setDv_estado(rs.getString("dv_estado"));
        con.setCOMENTARIO_RESNA(rs.getString("COMENTARIO_RESNA"));
        con.setMONTO_TOTAL_CAPITAL(rs.getFloat("MONTO_TOTAL_CONDONADO"));
        con.setMONTO_TOTAL_RECIBIT(rs.getFloat("MONTO_TOTAL_RECIBIT"));
        con.setDI_NUM_OPERS(rs.getInt("DI_NUM_OPERS"));
        con.setMONTO_TOTAL_CAPITAL(rs.getFloat("MONTO_TOTAL_CAPITAL"));
        con.setDi_fk_idColadorador(rs.getInt("di_fk_idColadorador"));
        con.setFld_ced(rs.getString("fld_ced"));
        
        
        
        
      //  con.setId_condonacion(rs.getInt("ID_CONDONACION"));
       // con.setTimestap(rs.getString("TIMESTAP"));

        return con;
    }

}
