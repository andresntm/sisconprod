/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.AreaTrabajo;


/**
 *
 * @author exesilr
 */
public class AreaTrabajoMapper implements RowMapper<AreaTrabajo>{
 
    
            public AreaTrabajo mapRow(ResultSet rs, int rowNum) throws SQLException {
        AreaTrabajo area = new AreaTrabajo();
        
        area.setCodigo(rs.getString("cod"));
        area.setDesccripcion(rs.getString("descripcion"));
        area.setFilian(rs.getString("filial"));
        area.setId_colaborador(rs.getInt("id"));

        return area;
    }
    
}
