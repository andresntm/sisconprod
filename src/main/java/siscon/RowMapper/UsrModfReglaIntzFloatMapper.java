/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrModfReglaInt;


/**
 *
 * @author exesilr
 */
public class UsrModfReglaIntzFloatMapper implements RowMapper<UsrModfReglaInt>{
 
    
            public UsrModfReglaInt mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrModfReglaInt _usrmodfregla = new UsrModfReglaInt();
        
        _usrmodfregla.setId_usr_modf(rs.getInt("id"));
        _usrmodfregla.setF_PorcentajeActual(rs.getFloat("PorcentajeActual_Alldecimal"));
        _usrmodfregla.setF_PorcentajeAnterior(rs.getFloat("PorcentajeActual_Alldecimal"));


        return _usrmodfregla;
    }
    
}
