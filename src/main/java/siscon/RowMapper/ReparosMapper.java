/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Reparos;

/**
 *
 * @author exesilr
 */
public class ReparosMapper implements RowMapper<Reparos> {

    public Reparos mapRow(ResultSet rs, int rowNum) throws SQLException {
        Reparos rep = new Reparos();

        rep.setDi_idReparo(rs.getInt("di_idReparo"));
        rep.setDi_fk_IdCondonacion(rs.getInt("di_fk_IdCondonacion"));
        rep.setDv_MotivoReparo(rs.getString("dv_MotivoReparo"));
        rep.setDv_GlosaReparo(rs.getString("dv_GlosaReparo"));
        rep.setFecha_reparo(rs.getString("registrado"));
        rep.setUsuario(rs.getString("alias"));
        rep.setColaborador_nombres(rs.getString("nombres"));
        rep.setCodArea(rs.getString("cod"));
        rep.setCunetaOrigen(rs.getString("cuenta"));
//        area.setCodigo(rs.getString("cod"));
//        area.setDesccripcion(rs.getString("descripcion"));
//        area.setFilian(rs.getString("filial"));
//        area.setId_colaborador(rs.getInt("id"));

        return rep;
    }

}
