/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.Regla;


/**
 *
 * @author exesilr
 */
public class ReglaPorUsuarioMapper implements RowMapper<Regla>{
 
    
            public Regla mapRow(ResultSet rs, int rowNum) throws SQLException {
        Regla _regla = new Regla();
        
        _regla.setIdRegla(rs.getInt("id_valor_regla"));
        _regla.setPerfil(rs.getString("Perfil"));
        _regla.setDescRegla(rs.getString("DescRegla"));
        _regla.setRanfoFInicio(rs.getInt("RanfoFInicio"));
        _regla.setRangoFFin(rs.getInt("RangoFFin"));
        _regla.setUndMedF(rs.getString("UndMedF"));
        _regla.setRangoMontoI(rs.getFloat("RangoMontoI"));
        _regla.setRangoMontoM(rs.getFloat("RangoMontoM"));
        _regla.setDesTipoValor(rs.getString("DesTipoValor"));
        _regla.setPorcentajeCondonacion100(rs.getInt("PorcentajeCondonacion100"));
       // area.setCodigo(rs.getString("cod"));
       // area.setDesccripcion(rs.getString("descripcion"));
       // area.setFilian(rs.getString("filial"));

        return _regla;
    }
    
}
