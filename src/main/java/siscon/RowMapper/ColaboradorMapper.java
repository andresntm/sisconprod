/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Colaborador;


/**
 *
 * @author exesilr
 */
public class ColaboradorMapper implements RowMapper<Colaborador>{
 
    
            public Colaborador mapRow(ResultSet rs, int rowNum) throws SQLException {
        Colaborador _cola = new Colaborador();
        
        _cola.setIdColaborador(rs.getInt("id"));
        _cola.setNombres(rs.getString("nombres"));
        _cola.setId_usuario(rs.getInt("id_usuario"));
        _cola.setApellidos(rs.getString("dv_apellidos"));
        _cola.setId_filial(rs.getInt("fk_id_filial"));
        _cola.setId_adreTrabajo(rs.getInt("fk_di_id_AreaTrabajo"));
        _cola.setNombre1(rs.getString("dv_nombre1"));
        _cola.setNombre2(rs.getString("dv_nombre2"));
        _cola.setApellido1(rs.getString("dv_apellidoPaterno"));
        _cola.setApellido2(rs.getString("dv_apellidoMaterno"));
        
        _cola.setFechaIngreso(rs.getString("ddt_fechaIngreso"));
        _cola.setCorreoCorporativo(rs.getString("dv_correoCoprporativo"));

        return _cola;
    }
    
}
