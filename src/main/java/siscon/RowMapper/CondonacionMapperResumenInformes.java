/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.APPResumenCondonacionesInforme;
import siscon.entidades.ResumenCondonacionesInforme;

/**
 *
 * @author excosoc
 */
public class CondonacionMapperResumenInformes implements RowMapper<APPResumenCondonacionesInforme>{
    
     public APPResumenCondonacionesInforme mapRow(ResultSet rs, int rowNum) throws SQLException {
        APPResumenCondonacionesInforme con = new APPResumenCondonacionesInforme();
        
        
        con.setTotal_rut(rs.getString("total_rut"));
        con.setTOTAL_CASTIGADO(rs.getString("TOTAL_CASTIGADO"));
        con.setTOTAL_VDE_SGN(rs.getString("TOTAL_VDE_SGN"));
        con.setTOTAL_RECIBIDO_1(rs.getString("TOTAL_RECIBIDO_1"));
        con.setTOTAL_RECIBIDO_2(rs.getString("TOTAL_RECIBIDO_2"));
        con.setTOTAL_TOTAL_RECIBE(rs.getString("TOTAL_TOTAL_RECIBE"));
        con.setTOTAL_CAPITAL_CONDONADO(rs.getString("TOTAL_CAPITAL_CONDONADO"));
        con.setTOTAL_INTERES_CONDONADO(rs.getString("TOTAL_INTERES_CONDONADO"));
        con.setTOTAL_HONORARIO_RECIBE(rs.getString("TOTAL_HONORARIO_RECIBE"));
        con.setTOTAL_COSTAS_PROVISION(rs.getString("TOTAL_COSTAS_PROVISION"));
        return con;
    }
    
}
