/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.TipoProvision;


/**
 *
 * @author exesilr
 */
public class TipoProvisionMapper implements RowMapper<TipoProvision>{
 
    
            public TipoProvision mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoProvision area = new TipoProvision();
        
        area.setDi_id(rs.getInt("di_id"));
        area.setDv_codtipocon(rs.getString("dv_codtipocon"));
        area.setDv_desc(rs.getString("dv_desc"));
        area.setRegistrado(rs.getString("registrado"));
        //area.setCodigo(rs.getString("cod"));
       // area.setDesccripcion(rs.getString("descripcion"));
       // area.setFilian(rs.getString("filial"));
       // area.setId_colaborador(rs.getInt("id"));

        return area;
    }
    
}
