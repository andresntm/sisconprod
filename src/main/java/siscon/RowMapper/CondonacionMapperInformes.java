/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.ResumenCondonacionesInforme;

/**
 *
 * @author excosoc
 */
public class CondonacionMapperInformes implements RowMapper<ResumenCondonacionesInforme>{
    
     public ResumenCondonacionesInforme mapRow(ResultSet rs, int rowNum) throws SQLException {
        ResumenCondonacionesInforme con = new ResumenCondonacionesInforme();
        
        
        con.setDemora_segundos(rs.getString("Demora-segundos"));
        con.setDemora_horas(rs.getString("Demora-horas"));
        con.setDemora_dias(rs.getString("Demora-dias"));
        con.setTiempo_aprobacion(rs.getString("tiempo_aprobacion"));
        con.setTiempo_aplicacion(rs.getString("tiempo_aplicacion"));
        con.setAprovado_Por(rs.getString("Aprovado_Por"));
        con.setNumero_oficina(rs.getString("numero_oficina"));
        con.setDesc_oficina(rs.getString("desc_oficina"));
        con.setMinima_Fecha_Castigo(rs.getString("Minima Fecha Castigo"));
        con.setMonto_total_recibit(rs.getString("monto_total_recibit"));
        con.setFld_ced(rs.getString("fld_ced"));
        con.setId_condonacion(rs.getString("id_condonacion"));
        con.setEjecutivo_Origen(rs.getString("Ejecutivo Origen"));
        con.setSALDO_TOTAL(rs.getString("SALDO_TOTAL"));
        con.setPorcentaje_COND_CAP(rs.getString("%_COND_CAP"));
        con.setSALDO_CONDONADO(rs.getString("SALDO_CONDONADO"));
        con.setPorcentaje_PAGO_CAP(rs.getString("%_PAGO_CAP"));
        con.setSALDO_PAGO(rs.getString("SALDO_PAGO"));
        con.setINTERES(rs.getString("INTERES"));
        con.setPorcentaje_COND_INT(rs.getString("%_COND_INT"));
        con.setINT_CONDONADO(rs.getString("INT_CONDONADO"));
        con.setPorcentaje_PAGO_INT(rs.getString("%_PAGO_INT"));
        con.setINT_PAGADO(rs.getString("INT_PAGADO"));
        con.setPorcentaje_PAGO_HON(rs.getString("%_PAGO_HON"));
        con.setHON_PAGO(rs.getString("HON_PAGO"));
        con.setMONTO_A_RECIBIR(rs.getString("MONTO_A_RECIBIR"));
        con.setVDE_SGN(rs.getString("VDE_SGN"));
        con.setPROVICION(rs.getString("PROVICION"));
        con.setMONTO_TOTAL_PAGO(rs.getString("MONTO_TOTAL_PAGO"));
        con.setRUT_CLIENTE(rs.getString("rut_cliente"));
        con.setKPI(rs.getString("KPI"));
//        con.setId_condonacion(rs.getInt("id_condonacion"));
//        con.setTimestap(rs.getString("timestap"));
//        con.setRegla(rs.getString("regla"));
//        con.setEstado(rs.getString("dv_estado"));
//        con.setComentario_resna(rs.getString("comentario_resna"));
//        con.setMonto_total_condonado(rs.getFloat("monto_total_condonado"));
//        con.setMonto_total_recibit(rs.getFloat("monto_total_recibit"));
//        con.setDi_num_opers(rs.getInt("di_num_opers"));
//        con.setMonto_total_capital(rs.getFloat("monto_total_capital"));
//        con.setTipocondonacion(rs.getString("dv_desc"));
//        con.setUsuariocondona(rs.getString("di_rut"));
//        con.setRutcCliente(rs.getString("rut"));
//        con.setNombreCliente(rs.getString("nombre"));
//        con.setNum_reparos(rs.getInt("num_reparos"));
//        con.setCedente(rs.getString("fld_ced"));
//        con.setNumero_oficina(rs.getInt("numero_oficina"));
//        con.setDesc_oficina(rs.getString("desc_oficina"));
        return con;
    }
    
}
