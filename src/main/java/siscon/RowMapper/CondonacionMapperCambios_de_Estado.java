/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.CondonacionTabla2;

/**
 *
 * @author esilves
 */
public class CondonacionMapperCambios_de_Estado implements RowMapper<CondonacionTabla2> {

    public CondonacionTabla2 mapRow(ResultSet rs, int rowNum) throws SQLException {
        CondonacionTabla2 con = new CondonacionTabla2();
        con.setId_condonacion(rs.getInt("ID_CONDONACION"));
        con.setTimestap(rs.getString("TIMESTAP"));
        con.setRegla(rs.getString("REGLA"));
        con.setEstado(rs.getString("DV_ESTADO"));
        con.setComentario_resna(rs.getString("COMENTARIO_RESNA"));
        con.setMonto_total_condonado(rs.getFloat("MONTO_TOTAL_CONDONADO"));
        con.setMonto_total_recibit(rs.getFloat("MONTO_TOTAL_RECIBIT"));
        con.setDi_num_opers(rs.getInt("DI_NUM_OPERS"));
        con.setMonto_total_capital(rs.getFloat("MONTO_TOTAL_CAPITAL"));
        con.setTipocondonacion(rs.getString("DV_DESC"));
        con.setUsuariocondona(rs.getString("DI_RUT"));
        con.setRutcCliente(rs.getString("RUT"));
        con.setNombreCliente(rs.getString("NOMBRE"));
        con.setNum_reparos(rs.getInt("NUM_REPAROS"));
        con.setCedente(rs.getString("FLD_CED"));
        con.setFecha_Cambio_Estado(rs.getTimestamp("FECHA_CAMBIO_ESTADO"));
        return con;
    }

}
