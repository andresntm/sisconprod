/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrModfReglaHonorario;


/**
 *
 * @author exesilr
 */
public class UsrModfReglaHonorariolMapper implements RowMapper<UsrModfReglaHonorario>{
 
    
        public UsrModfReglaHonorario mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrModfReglaHonorario _usrmodfregla = new UsrModfReglaHonorario();
        
        _usrmodfregla.setPorcentajeActual(rs.getInt("PorcentajeActual"));
        _usrmodfregla.setPorcentajeAnterior(rs.getInt("PorcentajeAnterior"));


        return _usrmodfregla;
    }
    
}
