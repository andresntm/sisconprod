/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Perfil;


/**
 *
 * @author exesilr
 */
public class PerfilMapper implements RowMapper<Perfil>{
            public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
        Perfil perfil = new Perfil();
        perfil.setId_perfil(rs.getInt("id_perfil"));
        perfil.setDescripcion(rs.getString("descripcion"));
        perfil.setCodPerfil(rs.getString("dv_CodPerfil"));
        perfil.setId_jefe(rs.getInt("id_jefe"));

        return perfil;
    }
}
