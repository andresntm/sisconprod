/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrModInteresOperacion;


/**
 *
 * @author exesilr
 */
public class UsrModInterezOperacionMapper implements RowMapper<UsrModInteresOperacion>{
 
    
            public UsrModInteresOperacion mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrModInteresOperacion _usrmodfregla = new UsrModInteresOperacion();
        
        _usrmodfregla.setPorcentajeActual(rs.getInt("valor_actual"));
        _usrmodfregla.setPorcentajeAnterior(rs.getInt("valor_Anteriro"));


        return _usrmodfregla;
    }
    
}
