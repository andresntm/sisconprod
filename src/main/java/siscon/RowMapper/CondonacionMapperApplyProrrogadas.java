/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.CondonacionTabla2;

/**
 *
 * @author esilves
 */
public class CondonacionMapperApplyProrrogadas implements RowMapper<CondonacionTabla2> {

    public CondonacionTabla2 mapRow(ResultSet rs, int rowNum) throws SQLException {
        CondonacionTabla2 con = new CondonacionTabla2();
        con.setId_condonacion(rs.getInt("id_condonacion"));
        con.setTimestap(rs.getString("timestap"));
        con.setRegla(rs.getString("regla"));
        con.setEstado(rs.getString("dv_estado"));
        con.setComentario_resna(rs.getString("comentario_resna"));
        con.setMonto_total_condonado(rs.getFloat("monto_total_condonado"));
        con.setMonto_total_recibit(rs.getFloat("monto_total_recibit"));
        con.setDi_num_opers(rs.getInt("di_num_opers"));
        con.setMonto_total_capital(rs.getFloat("monto_total_capital"));
        con.setTipocondonacion(rs.getString("dv_desc"));
        con.setUsuariocondona(rs.getString("di_rut"));
        con.setRutcCliente(rs.getString("rut"));
        con.setCedente(rs.getString("fld_ced"));
        con.setNombreCliente(rs.getString("nombre"));
        con.setNum_reparos(rs.getInt("num_reparos"));
        con.setTipo_prorroga(rs.getString("tipo_prorroga"));
        con.setFec_prorroga(rs.getTimestamp("fec_prorroga"));
        return con;
    }

}
