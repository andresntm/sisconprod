/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrModfReglaHonorario;


/**
 *
 * @author exesilr
 */
public class UsrModfReglaHonorarioFloatMapper implements RowMapper<UsrModfReglaHonorario>{
 
    
        public UsrModfReglaHonorario mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrModfReglaHonorario _usrmodfregla = new UsrModfReglaHonorario();
        
        _usrmodfregla.setId_usr_modif(rs.getInt("id"));
        _usrmodfregla.setF_PorcentajeActual(rs.getFloat("PorcentajeActual_Alldecimal"));
        _usrmodfregla.setF_PorcentajeAnterior(rs.getFloat("PorcentajeActual_Alldecimal"));


        return _usrmodfregla;
    }
    
}
