/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.usuario;

/**
 *
 * @author exesilr
 */
public class UsuarioMapper implements RowMapper<usuario> {

    public usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
        usuario usr = new usuario();
        usr.setAlias(rs.getString("alias"));
        usr.setEmail(rs.getString("dv_email"));
        usr.setPassword(rs.getString("dv_password"));
        usr.setDv_rut(rs.getString("dv_rut"));
        usr.setDi_rut(rs.getInt("di_rut"));
        usr.setAccount(rs.getString("alias"));
        usr.setFullName(rs.getString("alias"));

        if (rs.getString("id_condonacion") != null) {
            if (rs.getInt("id_condonacion") == 0) {
                usr.setId_colaborador(0);
            } else {
                usr.setId_colaborador(rs.getInt("id_condonacion"));
            }
        } else {
            usr.setAreaTrabajo(null);
        }
        if (rs.getString("AREATRAB") != null) {
            if (rs.getString("AREATRAB").isEmpty()) {
                usr.setAreaTrabajo(null);
            } else {
                usr.setAreaTrabajo(rs.getString("AREATRAB"));
            }
        } else {
            usr.setAreaTrabajo(null);
        }

        return usr;
    }
}
