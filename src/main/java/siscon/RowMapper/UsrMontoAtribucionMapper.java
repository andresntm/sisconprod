/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.UsrMontoAtribucion;


/**
 *
 * @author exesilr
 */
public class UsrMontoAtribucionMapper implements RowMapper<UsrMontoAtribucion>{
 
    
            public UsrMontoAtribucion mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsrMontoAtribucion _umatr = new UsrMontoAtribucion();
        
        _umatr.setAlias(rs.getString("alias"));
        _umatr.setGlosaAtribucion(rs.getString("glosa_atribucion"));
        _umatr.setMonto_minimo(rs.getFloat("monto_minimo"));
        _umatr.setMonto_maximo(rs.getFloat("monto_maximo"));
        return _umatr;
    }
    
}
