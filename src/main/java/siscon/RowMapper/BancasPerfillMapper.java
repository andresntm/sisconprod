/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Banca;


/**
 *
 * @author exesilr
 */
public class BancasPerfillMapper implements RowMapper<Banca>{
            public Banca mapRow(ResultSet rs, int rowNum) throws SQLException {
        Banca _banca = new Banca();
           _banca.setId(rs.getInt("id"));
           _banca.setCod_banca(rs.getString("cod_banca"));
           _banca.setDescripcion(rs.getString("descripcion"));

        return _banca;
    }
}
