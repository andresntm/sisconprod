/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import siscon.entidades.Colaborador;
import siscon.entidades.ColaboradorJefe;
import siscon.entidades.usuario;


/**
 *
 * @author exesilr
 */
public class ColaboradorJefeMapper implements RowMapper<ColaboradorJefe>{
 
    
            public ColaboradorJefe mapRow(ResultSet rs, int rowNum) throws SQLException {
        ColaboradorJefe _cola = new ColaboradorJefe();
        
        
        
        _cola.setMyId_jefe(rs.getInt("MyIdJefe"));
        _cola.setIdColaborador(rs.getInt("id"));
        _cola.setNombres(rs.getString("nombres"));
        _cola.setId_usuario(rs.getInt("id_usuario"));
        _cola.setApellidos(rs.getString("dv_apellidos"));
        _cola.setId_filial(rs.getInt("fk_id_filial"));
        _cola.setId_adreTrabajo(rs.getInt("fk_di_id_AreaTrabajo"));
        _cola.setNombre1(rs.getString("dv_nombre1"));
        _cola.setNombre2(rs.getString("dv_nombre2"));
        _cola.setApellido1(rs.getString("dv_apellidoPaterno"));
        _cola.setApellido2(rs.getString("dv_apellidoMaterno"));
        _cola.setFechaIngreso(rs.getString("ddt_fechaIngreso"));
        _cola.setCorreoCorporativo(rs.getString("dv_correoCoprporativo"));
        _cola.setFj_idJefe(rs.getInt("fk_idFeje"));
        usuario _usr=new usuario();
        _usr.setAlias(rs.getString("alias"));
        _usr.setDi_rut(rs.getInt("di_rut"));
        _usr.setDv_rut(rs.getString("dv_rut"));
        _usr.setEmail(rs.getString("dv_email"));
        _cola.setUsuario(_usr);
       // _cola.u
        
        
        return _cola;
    }
    
}
