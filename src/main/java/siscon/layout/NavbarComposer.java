/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package siscon.layout;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zul.*;
//import org.zkoss.zul.Popup;
import org.zkoss.zkspringmvc.annotation.*;
public class NavbarComposer extends SelectorComposer<Component> {
	
	@Wire 
	A atask, anoti, amsg;
         
	@Wire 
	Div divnav; 
        	@Wire 
	Div kkk; 
                
                
              
   
    public ComponentInfo doBeforeCompose(Page page, Component parent,ComponentInfo compInfo) {
     
        System.out.println("Debut Executiion doBeforeCompose");
        //initTimeDropdown(page);
// prepare model data
        //initCalendarModel(page);
       // hasPE = WebApps.getFeature("pe");
      //  page.setAttribute("hasPE", hasPE);
     // divnav.setStyle("background:#FFFFFF");
     //kkk.setStyle("background:#FFFFFF");
        System.out.println("Fin Executiion doBeforeCompose");
        
        
//         kkk.setStyle("background:#FFFFFF");
        return super.doBeforeCompose(page, parent, compInfo);
        //return ; 
    }
	  // El metodo doAfterCompose se encarga de enviar las acciones,metodos y eventos desde el controlador java hasta el componente Zk
	public void doAfterCompose(Component comp) throws Exception {
            
		super.doAfterCompose(comp);
             //   kkk.setStyle("background:#FFFFFF");
                divnav.setStyle("background:#d9534f");
	} 
        
          
	@Listen("onOpen = #taskpp")
	 public void toggleTaskPopup(OpenEvent event) {
            // divnav.setAttribute("style","background: #DF013A");
          //   divnav.getAttributes();
         // divnav.getat
         divnav.setStyle("background:#DF013A");
             //Messagebox.show(kkk.getAttributes().toString()+kkk.getStyle().toString()); 
		toggleOpenClass(event.isOpen(), atask);
               
	}
	
	@Listen("onOpen = #notipp")
	public void toggleNotiPopup(OpenEvent event) {
		toggleOpenClass(event.isOpen(), anoti);
	}
	
	@Listen("onOpen = #msgpp")
	public void toggleMsgPopup(OpenEvent event) {
		toggleOpenClass(event.isOpen(), amsg);
	}

	// Toggle open class to the component
	public void toggleOpenClass(Boolean open, Component component) {
		HtmlBasedComponent comp = (HtmlBasedComponent) component;
		String scls = comp.getSclass();
		if (open) {
			comp.setSclass(scls + " open");
		} else {
			comp.setSclass(scls.replace(" open", ""));
		}
	}
}