/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author excosoc
 */
public class MetodosGenerales {

    Session session = Sessions.getCurrent();

    //varibles locales
    int validaDet;
    int validaSegDet;
    //Variables cantidad de hitos
    int[] Entradas;
    int[] EntradasErr;
    String[] Bandeja2;
    int TotalEntradas;
    private SimpleJdbcCall execSp;
    private DataSource data;

    
    
    
        public MetodosGenerales() {
    }

    public boolean validarRut(String rut) {

        boolean validacion = false;

        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            String rutAux = rut.substring(0, rut.length() - 1);
            String dv = rut.substring(rut.length() - 1, rut.length());
            int cantidad = rutAux.length();
            int factor = 2;
            int suma = 0;
            String verificador = "";

            for (int i = cantidad; i > 0; i--) {
                if (factor > 7) {
                    factor = 2;
                }
                suma += (Integer.parseInt(rut.substring((i - 1), i))) * factor;
                factor++;

            }

            verificador = String.valueOf(11 - suma % 11);
            if (verificador.equals(dv)) {
                validacion = true;
            } else {
                if ((verificador.equals("10")) && (dv.toLowerCase().equals("k"))) {
                    validacion = true;
                } else {
                    if ((verificador.equals("11") && dv.equals("0"))) {
                        validacion = true;
                    } else {
                        validacion = false;
                    }
                }
            }
            return validacion;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    public char CalculaDv(int rut) {

        char dvCorrecto = ' ';

        try {
            int cantidad = Integer.toString(rut).length();
            int factor = 2;
            int suma = 0;
            String rutV = "";
            rutV = Integer.toString(rut);
            String verificador = "";

            for (int i = cantidad; i > 0; i--) {
                if (factor > 7) {
                    factor = 2;
                }
                suma += (Integer.parseInt(rutV.substring((i - 1), i))) * factor;
                factor++;

            }

            verificador = String.valueOf(11 - suma % 11);

            if ((verificador.equals("10"))) {
                dvCorrecto = 'k';
            } else {
                if ((verificador.equals("11"))) {
                    dvCorrecto = '0';
                } else {
                    dvCorrecto = verificador.charAt(0);
                }
            }

            return dvCorrecto;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }

        return dvCorrecto;
    }

    public static void main(String args[]) throws IOException {
        FileInputStream file = new FileInputStream(new File("C:\\prueb_excel.xls"));

        // Crear el objeto que tendra el libro de Excel	
        HSSFWorkbook workbook = new HSSFWorkbook(file);

        /*	
	 * Obtenemos la primera pesta�a a la que se quiera procesar indicando el indice.
	 * Una vez obtenida la hoja excel con las filas que se quieren leer obtenemos el iterator
	 * que nos permite recorrer cada una de las filas que contiene.
         */
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        Row row;

        // Recorremos todas las filas para mostrar el contenido de cada celda	
        while (rowIterator.hasNext()) {
            row = rowIterator.next();

            // Obtenemos el iterator que permite recorres todas las celdas de una fila
            Iterator<Cell> cellIterator = row.cellIterator();
            Cell celda;

            while (cellIterator.hasNext()) {
                celda = cellIterator.next();
                // Dependiendo del formato de la celda el valor se debe mostrar como String, Fecha, boolean, entero...	
                switch (celda.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        if (HSSFDateUtil.isCellDateFormatted(celda)) {
                            System.out.println(celda.getDateCellValue());
                        } else {
                            System.out.println(celda.getNumericCellValue());
                        }
                        System.out.println(celda.getNumericCellValue());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        System.out.println(celda.getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        System.out.println(celda.getBooleanCellValue());
                        break;
                }
            }
        }
        // cerramos el libro excel
        //workbook.close();

    }

    public int[] getEntradas() {
        return Entradas;
    }

    public void setEntradas(int[] Entradas) {
        this.Entradas = Entradas;
    }

    public int[] getEntradasErr() {
        return EntradasErr;
    }

    public void setEntradasErr(int[] EntradasErr) {
        this.EntradasErr = EntradasErr;
    }

    public String[] getBandeja2() {
        return Bandeja2;
    }

    public void setBandeja2(String[] Bandeja2) {
        this.Bandeja2 = Bandeja2;
    }

    public int getTotalEntradas() {
        return TotalEntradas;
    }

    public void setTotalEntradas(int TotalEntradas) {
        this.TotalEntradas = TotalEntradas;
    }

    ////////////////////////////////////////////////////////////////////////////
    //metodos de ejecuci�n de SP con o sin parametros (out/int)
    public void SPSParam(String SP) throws SQLException {
        //Ejecuta SPs sin parametros
        data = new MvcConfig().getDataSourceSisBoj();
        try {
            execSp = new SimpleJdbcCall(data).withProcedureName(SP);
            SqlParameterSource in = new MapSqlParameterSource();
            Map out = execSp.execute(in);
        } catch (Exception err) {
            Messagebox.show(err.toString());
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //Cargas de parametros generales SysBoj
    public void CargaEntradas() {
        int i = 0;
        setEntradas(new int[10]);
        setBandeja2(new String[10]);
        setEntradasErr(new int[10]);
        setTotalEntradas(0);
        try {
            //ConsuComer
            getEntradas()[0] = 0;
            getBandeja2()[0] = "Con y Com";
            getEntradasErr()[0] = 0;
            //Cae
            getEntradas()[1] = 0;
            getBandeja2()[1] = "Chip";
            getEntradasErr()[1] = 0;
            //Chip
            getEntradas()[2] = 30;
            getBandeja2()[2] = "Cae";
            getEntradasErr()[2] = 0;
            //Lir
            getEntradas()[3] = 0;
            getBandeja2()[3] = "TCR";
            getEntradasErr()[3] = 0;
            //PepPlus
            getEntradas()[4] = 0;
            getBandeja2()[4] = "Avenimientos";
            getEntradasErr()[4] = 0;
            //Tcr
            getEntradas()[5] = 0;
            getBandeja2()[5] = "Comex";
            getEntradasErr()[5] = 0;
            //Comex
            getEntradas()[6] = 0;
            getBandeja2()[6] = "Lineas";
            getEntradasErr()[6] = 0;
            //Lineas
            getEntradas()[7] = 0;
            getBandeja2()[7] = "Pep-Plus";
            getEntradasErr()[7] = 0;
            //GGEE
            getEntradas()[8] = 0;
            getBandeja2()[8] = "GGEE";
            getEntradasErr()[8] = 0;

            for (i = 0; i <= (getEntradas().length - 1); i++) {
                setTotalEntradas(getTotalEntradas() + getEntradas()[i]);
            }

            getEntradas()[9] = getTotalEntradas();
            setTotalEntradas(0);

            for (i = 0; i <= (getEntradasErr().length - 1); i++) {
                setTotalEntradas(getTotalEntradas() + getEntradasErr()[i]);
            }

            getEntradasErr()[9] = getTotalEntradas();

            session.setAttribute("HitIndex", getEntradas());
            session.setAttribute("EntradasErr", getEntradasErr());
            session.setAttribute("Bandeja2", getBandeja2());
        } catch (Exception e) {
        }
        //return Integer.toString(TotalEntradas);
    }

    //Compositores de paginas
    //Genera encabezados paginas 
    public void Encabezados(String ruta, String titulo, String subTitulo) {
        session.setAttribute("ruta", ruta);
        session.setAttribute("tituloPag", titulo);
        session.setAttribute("subTituloPag", subTitulo);
    }

    //Metodo que genera alertas
    public void AlertasBoj(String texto) {
        session.setAttribute("PagAlert", "AlertasBoj.zul");
        session.setAttribute("alertas", texto);
    }

    //Genera generico titulos paginas
    public void ContenidosPag(String url) {
        session.setAttribute("PagLlenado", url);
    }

    public void ContenidosPag(String icono, String tituloPanel, String url) {
        session.setAttribute("Bandeja", tituloPanel);
        session.setAttribute("Icono", icono);
        session.setAttribute("PagLlenado", url);

        ValidaDetalles();

        if (validaDet != -1) {
            session.removeAttribute("PagDetalle");
        }

        if (validaSegDet != -1) {
            session.removeAttribute("PagSegDetalle");
        }
    }

    public void ContenidosPag(String icono, String tituloPanel, String url, String ulr2) {
        session.setAttribute("Bandeja", tituloPanel);
        session.setAttribute("Icono", icono);
        session.setAttribute("PagLlenado", url);

        if ((!ulr2.equals(null)) && (!ulr2.equals(""))) {
            session.setAttribute("PagDetalle", ulr2);
        }

        ValidaDetalles();

        if (validaSegDet != -1) {
            session.removeAttribute("PagSegDetalle");
        }
    }

    public void ContenidosPag(String icono, String tituloPanel, String url, String ulr2, String ulr3) {
        session.setAttribute("Bandeja", tituloPanel);
        session.setAttribute("Icono", icono);
        session.setAttribute("PagLlenado", url);

        if ((ulr2.equals(null)) && (!ulr2.equals(""))) {
            session.setAttribute("PagDetalle", ulr2);
        }

        if ((ulr3.equals(null)) && (!ulr3.equals(""))) {
            session.setAttribute("PagSegDetalle", ulr3);
        }
    }

    private void ValidaDetalles() {
        validaDet = session.getAttributes().toString().indexOf("PagDetalle");
        validaSegDet = session.getAttributes().toString().indexOf("PagSegDetalle");
    }

    //convetimos String a fecha
    public Timestamp ConvStringDate(String date) {
        Timestamp retornaDate = null;
        DateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateConvert;

        try {
            dateConvert = (Date) formato.parse(date);
            retornaDate = new Timestamp(dateConvert.getTime());

        } catch (ParseException ex) {
            Messagebox.show(ex.toString());

        }

        return retornaDate;
    }
    ////////////////////////////////////////////////////////////////////////////

    /**
     *
     * @param caracter
     * @param Id
     * @return Retorna Codigo lote
     */
    public String ConvertId_Into_CodString(String caracter, int Id) {
        String codFormat;
        codFormat = "";

        try {
            codFormat = caracter;
            //Los codigos son con forma de 4 digitos ##### si no tiene valor agrega un 0 eje: 0001
            //ademas concatena 
            codFormat += String.format("%05d", Id);
        } catch (Exception ex) {
            Messagebox.show("Sr(a). Usuario(a), ha ocurrido un error en la codificaci�n. " + ex.toString());
        }
        return codFormat;
    }

    public int analizaDctos(String dv_CTipOper) {
        //proceso de calculo tipo documento mediante tioaux operaci�n, retorna Id Tipo Documento
        int id_TipDcto = 0;
        
        
        
        return id_TipDcto;
    }




    /**
     * Permite convertir un String en fecha (Date).
     *
     * @param fecha Cadena de fecha dd/MM/yyyy
     * @return Objeto Date
     */
    public Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
            System.out.println(ex);
        }
        return fechaDate;
    }
    

    public boolean like(String toBeCompare, String by) {
        if (by != null) {
            if (toBeCompare != null) {
                if (by.startsWith("%") && by.endsWith("%")) {
                    int index = toBeCompare.toLowerCase().indexOf(by.replace("%", "").toLowerCase());
                    if (index < 0) {
                        return false;
                    } else {
                        return true;
                    }
                } else if (by.startsWith("%")) {
                    return toBeCompare.endsWith(by.replace("%", ""));
                } else if (by.endsWith("%")) {
                    return toBeCompare.startsWith(by.replace("%", ""));
                } else {
                    return toBeCompare.equals(by.replace("%", ""));
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }



}
