/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.*;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.select.*;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;
import javax.print.DocFlavor;
import javax.print.SimpleDoc;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import sisboj.entidades.BandejaCustodia;
import sisboj.entidades.BandejaDetalle;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Lote;
import sisboj.entidades.Operaciones;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.LoteJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import siscon.entidades.AreaTrabajo;
import siscon.entidades.UsuarioPermiso;

import siscore.pdf.PDFTableGenerator;
import siscore.pdf.Table;
import siscore.pdf.TableBuilder;

/**
 *
 * @author esilvestre
 */
public class SolicitudesEnvioBojController extends SelectorComposer<Window> {

    @Wire
    private Window resultWin;
    @Wire
    Image _idCodeBar;
    @Wire
    Textbox txt_CodeQR;
    @Wire
    Textbox txt_CodeBar;
    @Wire
    Button closeButtonPDF;
    @Wire
    Label id_solicitud;
    @Wire
    Iframe iframe;
    @Wire
    BitMatrix OtroMix;
    @Wire
    Button ButtonEnvioBoj;
    PDStream pss;

    private final Session session = Sessions.getCurrent();
    private List<BandejaCustodia> listSol;
    private final SolicitudJDBC conexSol;
    private LoteJDBC _newLote;
    private DetSolicitudJDBC conexDetSol;
    private DetSolicitud _detsol;
    private List<BandejaDetalle> listDet = new ArrayList<BandejaDetalle>();
    private ListModelList<BandejaCustodia> bandejacustodia;
    private ListModelList<BandejaDetalle> DetSolModelList;
    private List<Documento> _listDOc = new ArrayList<Documento>();
    private ListModelList<Documento> _listModelDOc;
    private DocumentoJDBC _conListDOc;
    private Window window;
    private final OperacionesJDBC _op;
    private Operaciones ope = null;
    private TipDctoJDBC tipodocumentos;
    private int numerodocsOperActual = 0;
    private String codBarLL = "";
    private boolean loteImpreso = false;
    private EventQueue eq;
    @Wire
    QRCodeWriter qrCodeWriter = null;
    Code128Writer code128Writer = null;

    BufferedImage bufferedImage128 = null;
    @Wire
    BitMatrix encode = null;
    BitMatrix encodeCode128 = null;
    //private int id_solicitud = 0;
    private int id_solicitudLocal;
    @Wire
    Textbox _idDecodeQrCode;
    ///Informacion del Usuario

    String UsuarioCustodia;
    String CodeTemp;

    byte[] byteStream;

    // Define los valores del PDF
    // Page configuration
    private static final PDRectangle PAGE_SIZE = PDPage.PAGE_SIZE_A4;
    private static final float MARGIN = 50;
    private static final boolean IS_LANDSCAPE = false;

    // Font configuration
    private static final PDFont TEXT_FONT = PDType1Font.HELVETICA;
    private static final float FONT_SIZE = 12;

    // Table configuration
    private static final float ROW_HEIGHT = 15;
    private static final float CELL_MARGIN = 6;

    public SolicitudesEnvioBojController() throws SQLException {
        this.id_solicitudLocal = 0;
        conexSol = new MvcConfig().solicitudJDBC();
        _op = new MvcConfig().operacionesJDBCTemplate();
        _newLote = new MvcConfig().loteJDBC();

    }

    @Override
    public void doAfterCompose(Window comp) throws Exception {
        super.doAfterCompose(comp);

        ///////////////////////////////////////////////////////////////////////////////
        Session sess = Sessions.getCurrent();
        UsuarioPermiso permisos = (UsuarioPermiso) sess.getAttribute("UsuarioPermisos");
        String Account = permisos.getCuenta();//user.getAccount();
        UsuarioCustodia = ((AreaTrabajo) permisos.getArea()).getCodigo();
        ///////////////////////////////////////////////////////////////////////////////

        final Execution exec = Executions.getCurrent();

        id_solicitudLocal = (Integer) exec.getArg().get("solicitud");
        id_solicitud.setValue(exec.getArg().get("solicitud").toString());

        //id_solicitudLocal = Integer.parseInt(id_solicitud.getValue());
        if (UsuarioCustodia == "TODAS") {
            UsuarioCustodia = "%";
        }
        /// cust ={1,2,3}  indica la pesta�a 
        int cust = Integer.parseInt(session.getAttribute("custodias").toString());

        CodeTemp = GeneraCodigoLoteBoj();
        qrCodeWriter = new QRCodeWriter();
        code128Writer = new Code128Writer();
        String myWeb = "Hello World!";
        int width = 150;
        int height = 150;
        String fileType = "png";
        BufferedImage bufferedImage = null;

        BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB); // aux implementation
        Graphics2D g2d = img.createGraphics();
        Font font = new Font("Times", Font.PLAIN, 11);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        int textWidth = fm.stringWidth(myWeb);
        int textHeight = fm.getHeight();
        g2d.dispose();
        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        g2d = img.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2d.setFont(font);
        fm = g2d.getFontMetrics();
        g2d.setColor(Color.WHITE);
        g2d.drawString(myWeb, Math.round(Math.floor((width - textWidth) / 2)) - 2, height - fm.getAscent());
        g2d.dispose();
        //  BitMatrix byteMatrix=null;
        //String qrInputText = "email"+"_"+"password";
        //String finalData = Uri.encode(qrInputText,"utf-8");

        Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        BitMatrix bitMatrix = code128Writer.encode(myWeb, BarcodeFormat.CODE_128, width, height - textHeight - (2 * fm.getAscent()), hintMap);

        try {

            // encode=
            // BitMatrix  hhhh=  new EAN13Writer().encode("Hola Soy QA Code Decodificado", BarcodeFormat.EAN_13, width, height);
            encode = qrCodeWriter.encode(CodeTemp, BarcodeFormat.QR_CODE, width, height);
            encodeCode128 = new Code128Writer().encode(CodeTemp, BarcodeFormat.CODE_128, 60, 40, null);
            //  byteMatrix= qrCodeWriter.encode(myWeb, BarcodeFormat.CODABAR, width, height);//encode(myWeb, BarcodeFormat.QR_CODE, width, height);
            bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            bufferedImage.createGraphics();

            Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, width, height);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (encode.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            bufferedImage128 = MatrixToImageWriter.toBufferedImage(encodeCode128);

            // Make the BufferedImage that are to hold the Code128
            int matrixWidth = bitMatrix.getWidth();
            int matrixHeight = bitMatrix.getHeight();

            Graphics2D graphics2 = (Graphics2D) img.getGraphics();
            graphics2.setColor(Color.WHITE);
            for (int i = 0; i < matrixWidth; i++) {
                for (int j = 0; j < matrixHeight; j++) {
                    if (bitMatrix.get(i, j)) {
                        graphics2.fillRect(i, j + fm.getAscent(), 1, 1);
                    }
                }
            }

            System.out.println("Success CodeQR...");

        } catch (WriterException ex) {
            Logger.getLogger(SolicitudesEnvioBojController.class.getName()).log(Level.SEVERE, null, ex);
        }

        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        LuminanceSource source2 = new BufferedImageLuminanceSource(bufferedImage128);
        BinaryBitmap binaryBitmap2 = new BinaryBitmap(new HybridBinarizer(source2));
        Result qrCodeResult2 = new MultiFormatReader().decode(binaryBitmap2);
        //_idCodeBar.setContent(bufferedImage);
        //_idCodeBar2.setContent(bufferedImage128);
        //_idDecodeQrCode.setValue(qrCodeResult.getText());
//        txt_CodeBarLL.setValue(qrCodeResult2.getText());
        codBarLL = qrCodeResult2.getText();
//        //idCodeBar.setContent(bufferedImage);
//        _idCodeBar2.setContent(bufferedImage128);
        // this.txt_CodeQR.setValue(qrCodeResult.getText());
        //_idCodeBar2.setv
        PDF3();

    }

//    public void updateDetalleGrid(int id_sol) {
//
//        conexDetSol = new MvcConfig().detSolicitudJDBC();
//        this.listDet = conexDetSol.listDetSolicitud(id_sol);
//        DetSolModelList = new ListModelList<BandejaDetalle>(listDet);
//        grid_DetSol.setModel(DetSolModelList);
//        grid_DetSol.setVisible(false);
//        grid_DetSol.setVisible(true);
//
//    }
    public String GeneraCodigoLoteBoj() {
        String nn;
        // Creacion de Lote BOJ   idLote+NumeroDocs+Producto+CustodiaOrigen

        // obtencion de lote
        Lote jj = new Lote();
        int aditivo = 10000;
        int actualLote = _newLote.getIdNewtLote();
        int nuevoLote = actualLote + 1;

        //normalizando
        nuevoLote = nuevoLote + aditivo;

        nn = "LOT" + nuevoLote + "SOL" + this.id_solicitudLocal;

        //nn="KKJDKJD";
        return nn;

    }

    // este metodo Permite Refrescar de Forma Arcaida, Forzando la ejecucion del boton por comando
//    @Listen("onClick = #btnrefresh")
//    public void refresh() {
//        _divFridDoc.setVisible(true);
//        //this.updateGridDoc();
//    }
//
    @Listen("onClick = #closeButton")
    public void close() {
        resultWin.detach();
    }

    // @Listen("onClick = #closeButtonPDF")
    public void PDF_ori() throws IOException, WriterException, PrintException, COSVisitorException, SQLException {
        // resultWin.detach();
        BitMatrix bitMatrixXXXX;
        PDDocument document = new PDDocument();

        int line = 0;
        PDFont font = PDType1Font.HELVETICA;
        PDFont fontMono = PDType1Font.COURIER;

        PDPage emptyPage = null;
        emptyPage = new PDPage();
        PDRectangle rect = emptyPage.getMediaBox();
        document.addPage(emptyPage);
        PDPageContentStream content = new PDPageContentStream(document, emptyPage);
        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLUE);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
        content.drawString("APL IPHONE 5C BLUE 16GB KIT");
        content.endText();

        //// agregando codigos de los Documentos del Lote
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(1);
        String CompiladoNumeros = "";
        int count = 0;
        for (final BandejaDetalle db : listDet) {
            //  CompiladoNumeros=CompiladoNumeros+"##"+db.getnOperacion();

            content.beginText();
            content.setFont(fontMono, 20);
            content.setNonStrokingColor(Color.BLUE);
            content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
            content.drawString(db.getnOperacion());
            content.endText();

            bitMatrixXXXX = new Code128Writer().encode(db.getnOperacion(), BarcodeFormat.CODE_128, 150, 80, null);
            BufferedImage buffImg = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
            PDXObjectImage ximage = new PDPixelMap(document, buffImg);
            content.drawXObject(ximage, 150, rect.getHeight() - 50 * (++line), 150, 50);

            count = count + 1;
            // if(count==8){content.close();}
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        pss = new PDStream(document);
        // document.close();

        // PDStream contents = emptyPage.getContents();
        String path = "C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf";
        File f = new File(path);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            iframe.setContent(mymedia);
        }

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            if (printService.getName().equals("Xerox Global Print Driver PCL6")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());

        }

        InputStream is = pss.createInputStream();

        Messagebox.show("is" + is.toString());

        byteStream = out.toByteArray();
        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        FileInputStream fis = new FileInputStream("C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf");

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        //  printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());
        fis.close();

    }

    //@Listen("onClick = #closeButtonPDF")
    public void PDF() throws IOException, WriterException, PrintException, COSVisitorException, SQLException {
        // resultWin.detach();
        BitMatrix bitMatrixXXXX;
        PDDocument document = new PDDocument();
        PDPage emptyPage = null;
        int line = 0;
        PDFont font = PDType1Font.HELVETICA;
        PDFont fontMono = PDType1Font.COURIER;

        // emptyPage = new PDPage(PDPage.);
        emptyPage = new PDPage();
        PDRectangle rect = emptyPage.getMediaBox();
        // emptyPage.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));
        document.addPage(emptyPage);
        document.save("EmptyPage.pdf");
//PDFRenderer renderer = new PDFRenderer(document);
//BufferedImage img = renderer.renderImage(12 - 1, 4f);
//ImageIOUtil.writeImage(img, new File(RESULT_FOLDER, "ENG-US_NMATSCJ-1.103-0330-page12cropped.jpg").getAbsolutePath(), 300);

        PDPageContentStream content = new PDPageContentStream(document, emptyPage);
        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLUE);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
        content.drawString("Gu�a despacho Documentos");
        content.endText();

        content.beginText();
        content.setFont(fontMono, 20);
        content.setNonStrokingColor(Color.BLUE);
        content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
        content.drawString(GeneraCodigoLoteBoj());
        content.endText();

        bitMatrixXXXX = new Code128Writer().encode(GeneraCodigoLoteBoj(), BarcodeFormat.CODE_128, 150, 80, null);
        BufferedImage buffImgt = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
        PDXObjectImage ximaget = new PDPixelMap(document, buffImgt);
        content.drawXObject(ximaget, 150, rect.getHeight() - 50 * (++line), 150, 50);

        //// agregando codigos de los Documentos del Lote
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_solicitudLocal);
        String CompiladoNumeros = "";

        for (final BandejaDetalle db : listDet) {
            //  CompiladoNumeros=CompiladoNumeros+"##"+db.getnOperacion();

            content.beginText();
            content.setFont(fontMono, 20);
            content.setNonStrokingColor(Color.BLUE);
            content.moveTextPositionByAmount(100, rect.getHeight() - 50 * (++line));
            content.drawString(db.getnOperacion());
            content.endText();

            bitMatrixXXXX = new Code128Writer().encode(db.getnOperacion(), BarcodeFormat.CODE_128, 150, 80, null);
            BufferedImage buffImg = MatrixToImageWriter.toBufferedImage(bitMatrixXXXX);
            PDXObjectImage ximage = new PDPixelMap(document, buffImg);
            content.drawXObject(ximage, 150, rect.getHeight() - 50 * (++line), 150, 50);

        }

        content.close();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        pss = new PDStream(document);
        // document.close();

        // PDStream contents = emptyPage.getContents();
        String path = "C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf";
        File f = new File(path);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            iframe.setContent(mymedia);
        }

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            if (printService.getName().equals("Xerox Global Print Driver PCL6")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());

        }

        InputStream is = pss.createInputStream();

        Messagebox.show("is" + is.toString());

        final byte[] byteStream = out.toByteArray();
        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        FileInputStream fis = new FileInputStream("C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf");

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());

        fis.close();

    }

    //@Listen("onClick = #closeButtonPDF2")
    public void PDF3() throws IOException, WriterException, PrintException, COSVisitorException, SQLException {

        PDDocument document = new PDDocument();
        PDFTableGenerator nn = new PDFTableGenerator();
        document = nn.generatePDFdoc(createContent());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        byteStream = out.toByteArray();
        pss = new PDStream(document);
        AMedia mymedia = null;
        mymedia = new AMedia("EmptyPage.pdf", "pdf", "application/pdf", out.toByteArray());
        if (mymedia != null) {
            iframe.setContent(mymedia);
        }

    }

    public Table createContent() throws SQLException {
        // Total size of columns must not be greater than table width.
        List<siscore.pdf.Column> columns = new ArrayList<siscore.pdf.Column>();
        String[][] content2 = null;
        columns.add(new siscore.pdf.Column("N Operacion", 90));
        columns.add(new siscore.pdf.Column("Rut", 90));
        columns.add(new siscore.pdf.Column("Nombre", 220));
        columns.add(new siscore.pdf.Column("id Det lote", 40));
        //columns.add(new siscore.pdf.Column("Nombre", 200));
        conexDetSol = new MvcConfig().detSolicitudJDBC();
        this.listDet = conexDetSol.listDetSolicitud(id_solicitudLocal);
        int counttt = listDet.size();
        String[][] coupleArray = new String[counttt][];
        int jjj = 0;
        for (BandejaDetalle db : listDet) {

            // for (int i = 0; i < listDet.size(); i++) {
            //  String[] nn=new String[] {"aa","fff", "dddd"};
            // content2 = nn;
            coupleArray[jjj] = new String[]{db.getnOperacion(), db.getRutCompleto(), db.getNombrecomp(), Integer.toString(db.getIdDetSol())};

            //}
            jjj = jjj + 1;
        }

        String[][] content = {
            {"FirstName", "LastName", "fakemail@mock.com", "12345", "yes"},
            {"FirstName", "LastName", "fakemail@mock.com", "12345", "yes"},
            {"FirstName", "LastName", "fakemail@mock.com", "12345", "yes"}
        };

        // Messagebox.show("Content["+Arrays.toString(content)+"]content2["+Arrays.toString(coupleArray)+"]");
        float tableHeight = IS_LANDSCAPE ? PAGE_SIZE.getWidth() - (2 * MARGIN) : PAGE_SIZE.getHeight() - (2 * MARGIN);
        //float CELL_MARGIN = 0;

        Table table = new TableBuilder().setCellMargin(CELL_MARGIN).setColumns(columns).setContent(coupleArray).setHeight(tableHeight)
                .setNumberOfRows(coupleArray.length)
                .setRowHeight(ROW_HEIGHT)
                .setMargin(MARGIN)
                .setPageSize(PAGE_SIZE)
                .setLandscape(IS_LANDSCAPE)
                .setTextFont(TEXT_FONT)
                .setFontSize(FONT_SIZE)
                .build();
        table.setCodlote(GeneraCodigoLoteBoj());
        return table;
    }

    @Listen("onClick = #ButtonEnvioBoj")
    public void EnviarBOJ() {

        try {
            //revisa si se imprimio el documento antes de continuar y le da la opci�n al usuario de imprimir antes de continuar
            if (!loteImpreso) {
                Messagebox.show("Sr(a) Usuario(a), el Lote a despachar no se encuentra impreso."
                        + "\n�desea imprimirlo antes de continuar?.", "Despacho de Lotes.", Messagebox.YES | Messagebox.NO, Messagebox.EXCLAMATION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            //imprime y continua
                            try {
                                Imprimir2();
                                DespachaLote();
                            } catch (IOException ex) {
                                Logger.getLogger(SolicitudesEnvioBojController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (WriterException ex) {
                                Logger.getLogger(SolicitudesEnvioBojController.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (PrintException ex) {
                                Logger.getLogger(SolicitudesEnvioBojController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } else if (Messagebox.ON_NO.equals(e.getName())) {
                            //continua sin imprimir
                            DespachaLote();
                        }
                    }
                }
                );
            }

        } catch (Exception ex) {
            Messagebox.show("Sr(a) Usuario(a), ocurrio un problema al enviar a Boj el lote.\n" + ex.toString());
        }
    }

    private void DespachaLote() {
        try {
            ProcesoLotes nuevoLote = new ProcesoLotes();
            if (!nuevoLote.ProcesoBojResepcion(id_solicitudLocal, codBarLL)) {
                Messagebox.show("Error No se Pudo Crear, Intente mas Tarde");
                resultWin.detach();
                return;
            }

            if (conexSol.EnviarBojSolicitud(id_solicitudLocal)) {

                // generar Lote Boj
                // Messagebox.show("IncludePagPrin["+capturawin.getParent().getParent().getFellows().toString()+"]");
//            Include inc = ((Include) capturawin.getParent().getParent().getFellow("PagPrin"));
//            inc.setSrc(null);
//            inc.setSrc("cargaHitos_1/pagCont.zul");
                Messagebox.show("Se Envio correctamente la Solicitud ", "Enviar Solicitud", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {

                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        }
                    }
                }
                );
            } else {
                Messagebox.show("Problemas al Agregar el Documento ", "Guardar Documento", Messagebox.OK | Messagebox.CANCEL, Messagebox.ERROR,
                        new org.zkoss.zk.ui.event.EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            Executions.sendRedirect("/sisboj/index");
                        } else if (Messagebox.ON_CANCEL.equals(e.getName())) {
                        }
                    }
                }
                );

            }
            Messagebox.show("Generando Envio Lote Numero:[" + CodeTemp + "]");

            eq = EventQueues.lookup("interactive", EventQueues.DESKTOP, false);
            eq.publish(new Event("onButtonClick", ButtonEnvioBoj, null));

            resultWin.detach();

        } catch (Exception ex) {
            Messagebox.show(ex.toString());
        }
    }

    @Listen("onClick = #Imprimir")
    public void Imprimir() throws IOException, WriterException, PrintException {

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            if (printService.getName().equals("Xerox Global Print Driver PCL6")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());

        }

        InputStream is = pss.createInputStream();

        Messagebox.show("is" + is.toString());
        FileInputStream fis = new FileInputStream("C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf");

        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        printJob.print(pdfDoc, new HashPrintRequestAttributeSet());

        fis.close();

    }

    @Listen("onClick = #id_impGuiadespacho")
    public void Imprimir2() throws IOException, WriterException, PrintException {

        PrintService[] ps = PrintServiceLookup.lookupPrintServices(null, null);//deleted patts and flavor
        if (ps.length == 0) {
            throw new IllegalStateException("No Printer found");
        }
        PrintService myService = null;
        for (PrintService printService : ps) {
            if (printService.getName().equals("Xerox Global Print Driver PCL6")) {
                myService = printService;
                break;
            }

        }
        if (myService == null) {
            throw new IllegalStateException("Printer not found");
        } else {

            Messagebox.show("Ya encontre el Drivers" + myService.getName().toString());
            loteImpreso = true;
        }

        InputStream is = pss.createInputStream();

        Messagebox.show("is" + is.toString());

        Doc documentToBePrinted = new SimpleDoc(new ByteArrayInputStream(byteStream), DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        //FileInputStream fis = new FileInputStream("C:/Desarrollo/PES_DE_CONDONACION_CASTIGO.pdf");
        Doc pdfDoc = new SimpleDoc(is, DocFlavor.INPUT_STREAM.AUTOSENSE, null);

        DocPrintJob printJob = myService.createPrintJob();

        printJob.print(documentToBePrinted, new HashPrintRequestAttributeSet());

        // fis.close();
    }

}
