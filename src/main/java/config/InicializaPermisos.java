package config;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.SQLException;
import java.util.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;
import siscon.entidades.UsuarioPermiso;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;


/**
 *
 * @author esilves
 */
public class InicializaPermisos implements Initiator {
	 UsuarioPermisoJDBC usrPermiso;

    public InicializaPermisos() throws SQLException {
        this.usrPermiso = new MvcConfig().usuarioPermisoJDBC();
    }
	public void doInit(Page page, Map<String, Object> args) throws Exception {
		
		UsuarioPermiso permiso=usrPermiso.getUsuarioPermisos();
		if(permiso==null || permiso.isInvitado()){
			Executions.sendRedirect("/Login");
		}
	}
}
