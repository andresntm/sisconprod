package config;

import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import org.zkoss.zkspringmvc.ZKUrlBasedViewResolver;
import org.zkoss.zkspringmvc.ZKView;
import org.zkoss.zkspringmvc.config.ZKWebMvcConfigurerAdapter;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;


/*los excludes y filter del final de linea son para no errar en nombres de clases iguales para distintos packages*/
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"siscore", "org.zkoss.zkspringmvc.demo", "sisboj", "siscon"})
public class MvcConfigProd extends ZKWebMvcConfigurerAdapter {

    private String IpSql = "";// "localhost:1433";
    private String DveloperServer = "161.131.247.54";
    private String DeveloperPort = "1433";
    private String LucyServer = "161.131.247.54";
    private String LucyPort = "1433";
    private String ddbbSql = "SysCore_Desarrollo";
    private String UserSql = "SysCore";
    private String PassUserSql = "syscore1";

    
    
            
      public MvcConfigProd(){
        //carga parametros del sistema
        parmCond = new ParametrosCond();
    }  
    private ParametrosCond parmCond;


   /**
     * @return the parmCond
     */
    public ParametrosCond getParmCond() {
        return parmCond;
    }

    /**
     * @param parmCond the parmCond to set
     */
    public void setParmCond(ParametrosCond parmCond) {
        this.parmCond = parmCond;
    }


    
    @Bean
    public ViewResolver getViewResolver() {
        ZKUrlBasedViewResolver resolver = new ZKUrlBasedViewResolver();
        resolver.setViewClass(new ZKView().getClass());
        // System.err.print( "\n\n\n\t\t--- {###"+resolver.getApplicationContext()+ Sessions.getCurrent()+"####} ---\n\n\n" );
        resolver.setPrefix("/WEB-INF/");
        resolver.setSuffix("");

        return resolver;
    }

    /*
     * Configure ResourceHandlers to serve static resources like CSS/ Javascript etc...
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("*.css").addResourceLocations("/css/*");
        registry.addResourceHandler("*.js").addResourceLocations("/js/*");
        registry.addResourceHandler("*.dsp").addResourceLocations("/dsp/*");
        registry.addResourceHandler("*.jpg").addResourceLocations("/jpg/*");
        registry.addResourceHandler("*.png").addResourceLocations("/png/*");
        registry.addResourceHandler("*.svg").addResourceLocations("/svg/*");
        registry.addResourceHandler("*.woff").addResourceLocations("/fonts/*");
        registry.addResourceHandler("*.woff2").addResourceLocations("/fonts/*");
        registry.addResourceHandler("*.eot").addResourceLocations("/fonts/*");
    }

    @Bean(name = "datasource")
    public DataSource getDataSource() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        //dataSource.setUrl("jdbc:jtds:sqlserver://161.131.230.219:1433/SysCon");
        //dataSource.setUsername("siscon");
       // dataSource.setPassword("password");
        dataSource.setUrl("jdbc:jtds:sqlserver://161.131.247.54:1433/SysCon");
        dataSource.setUsername("siscore");
       dataSource.setPassword("siscorebci");
        return dataSource;
    }

    @Bean(name = "datasourceProduccion")
    public DataSource getDataSourceProduccion() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

        dataSource.setUrl("jdbc:jtds:sqlserver://161.131.247.54:1433/IN_CBZA");
        
        dataSource.setUsername("siscore");
        dataSource.setPassword("siscorebci");
//dataSource.setUrl("jdbc:jtds:sqlserver://161.131.230.219:1433/SmallCore");
      //  dataSource.setUsername("siscon");
       // dataSource.setPassword("password");
        return dataSource;
    }
    
    
    
    
    
    
    
/*
    @Bean(name = "datasourceLucy")
    public DataSource getDataSourceLucy() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

        dataSource.setUrl("jdbc:jtds:sqlserver://161.131.230.214:1433/IN_CBZA");
        dataSource.setUsername("webnza");
        dataSource.setPassword("webnza10");

        return dataSource;
    }
*/
    
    
        @Bean(name = "datasourceLucy")
    public DataSource getDataSourceLucy() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

        dataSource.setUrl("jdbc:jtds:sqlserver://161.131.247.54:1433/IN_CBZA");
        dataSource.setUsername("siscore");
        dataSource.setPassword("siscorebci");

        return dataSource;
    }
    
    
   // @Bean(name = "contactodatasource")
   // public ContactoDAO getContactoDAO() throws SQLException {
   //     return new ContactoDAOImpl(getDataSource());
   // }

    @Bean(name = "usuarioPermisoJDBC")
    public UsuarioPermisoJDBC usuarioPermisoJDBC() throws SQLException {
        UsuarioPermisoJDBC sol = new UsuarioPermisoJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisCon());

        return sol;
    }

    @Bean(name = "getDataSourceSisCon")
    public DriverManagerDataSource getDataSourceSisCon() {
        IpSql = this.DveloperServer + ":" + this.DeveloperPort;
        ddbbSql = "SysCon";
        //ddbbSql = "SysCore_QA";
        UserSql = "siscore";
        PassUserSql = "siscorebci";

        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://" + IpSql + ";databaseName=" + ddbbSql);
        dataSource.setUsername(UserSql);
        dataSource.setPassword(PassUserSql);

        return dataSource;
    }

}
