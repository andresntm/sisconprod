/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

/**
 *
 * @author excosoc
 */
public class ParametrosCond {

    
    private int id_x100Capital;
    private int id_x100Interes;
    private int id_x100Honorario;
    
    public ParametrosCond(){
        
        this.id_x100Capital = 6;
        this.id_x100Interes = 4;
        this.id_x100Honorario = 11;
        
    }
    
    /**
     * @return the id_x100Capital
     */
    public int getId_x100Capital() {
        return id_x100Capital;
    }

    /**
     * @param id_x100Capital the id_x100Capital to set
     */
    public void setId_x100Capital(int id_x100Capital) {
        this.id_x100Capital = id_x100Capital;
    }

    /**
     * @return the id_x100Interes
     */
    public int getId_x100Interes() {
        return id_x100Interes;
    }

    /**
     * @param id_x100Interes the id_x100Interes to set
     */
    public void setId_x100Interes(int id_x100Interes) {
        this.id_x100Interes = id_x100Interes;
    }

    /**
     * @return the id_x100Honorario
     */
    public int getId_x100Honorario() {
        return id_x100Honorario;
    }

    /**
     * @param id_x100Honorario the id_x100Honorario to set
     */
    public void setId_x100Honorario(int id_x100Honorario) {
        this.id_x100Honorario = id_x100Honorario;
    }
}
