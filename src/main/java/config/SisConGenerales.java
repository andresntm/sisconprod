/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.sql.DataSource;
//import net.objecthunter.exp4j.ExpressionBuilder;
//import net.objecthunter.exp4j.Expression;
import org.mariuszgromada.math.mxparser.Expression;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import siscon.entidades.Condonacion;
import siscon.entidades.DetalleCliente;
import siscon.entidades.HornorariosJudicialesCliente;
import siscon.entidades.implementaciones.GeneralAppJDBC;
import siscon.entidades.implementaciones.OperacionJDBC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import siscon.entidades.ResultadoSp;
import static siscore.comunes.LogController.SisCorelog;
//import scala.tools.nsc.interpreter.IMain;
//import scala.tools.nsc.settings.MutableSettings.BooleanSetting;
/**
 *
 * @author esilves
 */
public class SisConGenerales {

    OperacionJDBC _operJDBC;
    MvcConfig mmmm = new MvcConfig();
    GeneralAppJDBC _ggJDBC;
    float ufAplica;
    float UfDia;
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall jdbcCall;
    private static final Logger logger =  LoggerFactory.getLogger(SisConGenerales.class);
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.getDefault());

    public SisConGenerales(OperacionJDBC _operJDBC) throws SQLException {
        _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
        _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());

        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;

    }
    public SisConGenerales() throws SQLException {
        _operJDBC = new OperacionJDBC(mmmm.getDataSourceLucy());
        _ggJDBC = new GeneralAppJDBC(mmmm.getDataSourceProduccion());

        this.UfDia = (_ggJDBC.GetUfHoy() == 0) ? 26600 : _ggJDBC.GetUfHoy();
        this.ufAplica = this.UfDia == 0 ? 24600 : this.UfDia;

    }
    //
    //
    //    recibe ;
    //    A) list de operaciones y     
    //    B) el % condonacion ingresado
    //    C) rutCliente Formateado
    //
    public ListModelList<HornorariosJudicialesCliente> calculaHonorariosJudiciales(ListModelList<DetalleCliente> ListDetOperModel, float PorcentajeCondonaCapital,float PorcentajeCondonaHonorarios, int RutClienteFormateado) {
      //  ListModelList<> ii = null;
       ListModelList<HornorariosJudicialesCliente> ii = new ListModelList<HornorariosJudicialesCliente>();
        String oper = "NULL";
        float capital_condonado = 0;

        float porcentaje_condonacion = (float) PorcentajeCondonaCapital;

        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setOperacion1(ListDetOperModel.get(i).getOperacionOriginal());
                temp.setOperacion2(ListDetOperModel.get(i).getOperacion());
                temp.setRutcliente(RutClienteFormateado);
                temp.setPorcentajeCapital(porcentaje_condonacion);
                
                
                
                capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion ;
                temp.setCapital((float) ListDetOperModel.get(i).getSaldoinsoluto());
                temp.setCapitalCondonado(capital_condonado);
                
                float porcentaje_honorjud = 0;
                float honorariojud = 0;
                float honorariojud2 = 0;
                float honorariojud_sobrecondonado = 0;
                float Honor2 = 0;
                float capital_a_recibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;
                 
                temp.setCapital_arecibir(capital_a_recibir);
                // 
                //  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;

                    float c = Math.round((float) this.ufAplica);
                    float a = Math.round((float) capital_a_recibir);
                    float b = Math.round(((float) 10 * c));

                    if (a <= b) {

                        honorariojud_sobrecondonado = Math.round(a * 0.09);
                        //hs=   Math.round(Math.round((float) ca*(1-p1)) * 0.09);
                        
                        
                        // calculo de P1
                        // MT - VDES -p1*ca= ca*(1-p1) * 0.09
                        //                 =(ca - p1*ca)  *0.09
                        //                 = 0.09* ca - 0.09*p1*ca
                        // MT - VDES -p1*ca = 0.09* ca - 0.09*p1*ca
                        //MT -VDES   = 0.09 * ca (-0.09*ca * p1) + (p1*ca)
                        //MT-VDES-0.09*ca =p1(ca-0.09*ca)
                        
                        
                        //  Honor2=a * (float)00.9;

                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud_sobrecondonado = Math.round(hh + ww3);
                        //
                       // HS = Math.round(Math.round((Math.round(((float) 10 * (float) this.ufAplica)) * 0.09)) + Math.round(Math.round(Math.round((Math.round((float) (float) ca*(1-p1)) - Math.round(((float) 10 * (float) this.ufAplica))))) * (float) 0.06));
                      
                       

                       // hs= 10  * this.ufAplica *0.09  +  ca *( 1 -p1) - 10 * this.ufAplica * (float) 0.06 ;
                        
                        //Honor2=(b*0.09) + (a - b) * c * 0.06;

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;
                             
                        
                        
                      //  HS = (10 * this.ufAplica  * 0.09)   + ( 40 * this.ufAplica  * 0.06 ) +  ( ca * (1-p1)  - 50 * this.ufAplica  ) * 0.03
                      // HS =  10*0.09 * this.ufAplica + 40*0.06 * this.ufAplica  +  0.03*ca * (1- p1) - 50*0.03 * this.ufAplica
                        
                        
                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) capital_a_recibir;
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud_sobrecondonado = (float) capital_a_recibir * (float) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir && capital_a_recibir <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) capital_a_recibir;
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);

                        // honorariojud_sobrecondonado = (unff) + ( capital- ((float)10 * uf)) * uf * ptje2;
                        honorariojud_sobrecondonado = Math.round(hh + ww3);
                    } else if ((float) capital_a_recibir > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) capital_a_recibir;
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

         
                float honorarioJudCondonado2 = (float) honorariojud_sobrecondonado * (float) PorcentajeCondonaHonorarios;
                float honorarioJudReibido2 = (float) honorariojud_sobrecondonado - (float) honorarioJudCondonado2;
                temp.setHonorario_condona(honorarioJudCondonado2);
                temp.setHonorario_recibe(honorarioJudReibido2);
                ii.add(temp);

                
                
                
            }
        }
        
        
        
       
        return ii;
    }

    
    
    
    
    
    
        //
    //
    //    recibe ;
    //    A) list de operaciones y     
    //    B) el % condonacion ingresado
    //    B.1) se agrega  MontoTptal   VDES para el calculo de 
    //    C) rutCliente Formateado
    //
    public ListModelList<HornorariosJudicialesCliente> calculaHonorariosJudicialesV2(ListModelList<DetalleCliente> ListDetOperModel, float PorcentajeCondonaCapital,float PorcentajeCondonaHonorarios, int RutClienteFormateado,float MT,float VDES) {
      //  ListModelList<> ii = null;
       ListModelList<HornorariosJudicialesCliente> ii = new ListModelList<HornorariosJudicialesCliente>();
        String oper = "NULL";
        float capital_condonado = 0;

        float porcentaje_condonacion = (float) PorcentajeCondonaCapital;
       
        
        
        // total saldo insoluto
        
        float total_insoluto=0;
         for (int i = 0; i < ListDetOperModel.getSize(); i++) {
         total_insoluto=total_insoluto +  (float) ListDetOperModel.get(i).getSaldoinsoluto();
         
         
         }
        
        
        
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                oper = ListDetOperModel.get(i).getOperacion();
                temp.setOperacion1(ListDetOperModel.get(i).getOperacionOriginal());
                temp.setOperacion2(ListDetOperModel.get(i).getOperacion());
                temp.setRutcliente(RutClienteFormateado);
                temp.setPorcentajeCapital(porcentaje_condonacion);

                capital_condonado = (float) ListDetOperModel.get(i).getSaldoinsoluto() * (float) porcentaje_condonacion;
                temp.setCapital((float) ListDetOperModel.get(i).getSaldoinsoluto());
                temp.setCapitalCondonado(capital_condonado);

                float porcentaje_honorjud = 0;
                float honorariojud = 0;
                float honorariojud2 = 0;
                float honorariojud_sobrecondonado = 0;
                float Honor2 = 0;
                float capital_a_recibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;

                float p1_1 = 0;
                float p1_2 = 0;
                float p1_3 = 0;
                float pn1 = (float) 0.09;
                float pn2 = (float) 0.06;
                float pn3 = (float) 0.03;
                float ca = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                // float MT =

                temp.setCapital_arecibir(capital_a_recibir);

                float c = Math.round((float) this.ufAplica);
                float a = Math.round((float) capital_a_recibir);
                float b = Math.round(((float) 10 * c));

                // p1_1 = (MT - VDES - (pn1 * ca)) / (ca - pn1 * ca);
                // p1_1 =    (MT - VDES) / ( pn1 * ca + ca ) ;
                p1_1 = (MT - VDES) / (pn1 * total_insoluto + total_insoluto);
                temp.setHONOR_F1(p1_1*pn1*total_insoluto);
                float c_recibe1 = total_insoluto * p1_1;

                Messagebox.show("Insoluto :" + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-1:[" + p1_1 + "]  CRECIBE:[" + nf.format(c_recibe1).replaceFirst("Ch", "") + "]");

                //    p1_2 = (MT - VDES - pn1 * 10 * this.ufAplica + 10 * this.ufAplica * pn2 + ca * pn2) / (ca - pn2 * ca);
                p1_2 = (MT - VDES - 10 * this.ufAplica * pn1 + pn2 * 10 * this.ufAplica) / (total_insoluto + pn2 * total_insoluto);

                float c_recibe2 = total_insoluto * p1_2;
                Messagebox.show("Insoluto : " + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-2:[" + p1_2 + "] CRECIBE:[" + nf.format(c_recibe2).replaceFirst("Ch", "") + "]");

                //   p1_3 = MT - VDES - 10 * this.ufAplica * pn1 - 40 * this.ufAplica * pn2 - pn3 * ca - pn3 * 50 * this.ufAplica / (ca - pn3 * ca);
                p1_3 = (MT - VDES - 10 * this.ufAplica * pn1 - 40 * this.ufAplica * pn2 - 50 * this.ufAplica * pn3) / (total_insoluto * pn3 + total_insoluto);

                float c_recibe3 = total_insoluto * p1_3;
                Messagebox.show("insoluto :" + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-3:[" + p1_3 + "] CRECIBE:[" + nf.format(c_recibe3).replaceFirst("Ch", "") + "]");

                float honors = 0;
                for (int iii = 0; iii < ListDetOperModel.getSize(); iii++) {
                    honors = honors + ((float) ListDetOperModel.get(i).getSaldoinsoluto() * p1_1);

                }
                float honors2 = 0;
                for (int iiii = 0; iiii < ListDetOperModel.getSize(); iiii++) {
                    honors2 = honors2 + ((float) ListDetOperModel.get(i).getSaldoinsoluto() * p1_2);

                }

                float honors3 = 0;
                for (int iiiii = 0; iiiii < ListDetOperModel.getSize(); iiiii++) {
                    honors3 = honors3 + ((float) ListDetOperModel.get(i).getSaldoinsoluto() * p1_3);

                }

                Messagebox.show("honors3 :[" + nf.format(honors3).replaceFirst("Ch", "") + "]  suma : [" + nf.format(honors3 + VDES).replaceFirst("Ch", "") + "]  honors:[" + nf.format(honors).replaceFirst("Ch", "") + "]   suma  : [" + nf.format(honors + VDES).replaceFirst("Ch", "") + "]   honors2:[" + nf.format(honors2).replaceFirst("Ch", "") + "]    suma  : [" + nf.format(honors2 + VDES).replaceFirst("Ch", "") + "]");

                a = c_recibe3;

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    porcentaje_honorjud = (float) 0.15;



                    if (a <= b) {

                        honorariojud_sobrecondonado = Math.round(a * 0.09);
                        //hs=   Math.round(Math.round((float) ca*(1-p1)) * 0.09);
                        
                        
                        // calculo de P1  ERROR
                        // MT - VDES -p1*ca= ca*(1-p1) * 0.09
                        //                 =(ca - p1*ca)  *0.09
                        //                 = 0.09* ca - 0.09*p1*ca
                        // MT - VDES -p1*ca = 0.09* ca - 0.09*p1*ca
                        //MT -VDES   = 0.09 * ca (-0.09*ca * p1) + (p1*ca)
                        //MT-VDES-0.09*ca =p1(ca-0.09*ca)
                        
                        //  MT-VDES-0.09*ca/(ca-0.09*ca)=p1
                        
      
                        
                        /////$################
                        
                        
                        
                                                // calculo de P1
                        // MT - VDES -p1*ca= p1*ca * 0.09
                        //         MT - VDES=  p1*ca *0.09 +p1*ca=
                        //         MT - VDES = p1 ( 0.09ca + ca ) 
                        // MT - VDES / ( 0.09ca + ca )  = p1 
                        // MT - VDES -p1*ca = 0.09* ca - 0.09*p1*ca
                        //MT -VDES   = 0.09 * ca (-0.09*ca * p1) + (p1*ca)
                        //MT-VDES-0.09*ca =p1(ca-0.09*ca)
                        
                        //  MT-VDES-0.09*ca/(ca-0.09*ca)=p1
                        
                        
                        
                        
                        
                       //p1 = (MT-VDES-pn1*ca)/(ca-pn1*ca);
                        
                       // Messagebox.show("P1-1:["+p1+"]");
                        
                        //  Honor2=a * (float)00.9;

                    } else if ((b) < a && a <= ((float) 50 * c)) {

                        float hh = Math.round((b * 0.09));
                        float amenosb = Math.round((a - b));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.06);

                        honorariojud_sobrecondonado = Math.round(hh + ww3);
                        
                        
                        //hs=hh + ww3
                        //hs=b * 0.09 + ww * (float) 0.06
                        //hs=b * 0.09 + ww * (float) 0.06
                        //hs=b * 0.09 + amenosb * (float) 0.06
                        //hs=b * 0.09 + (a - b) * (float) 0.06
                        
                        //hs=b * 0.09 + (p1*ca - 10 * this.ufAplica) * (float) 0.06
                        
                     //   MT-VDES-p1*ca =b * 0.09 + (p1*ca - 10 * this.ufAplica) * (float) 0.06
                   //  MT-VDES - b * 0.09 + 0.06*10 * this.ufAplica = p1*ca +  p1*ca*0.06
                  //   MT-VDES - b * 0.09 + 0.06*10 * this.ufAplica=  p1(ca + 0.06 ca) 
                  //      p1=MT-VDES - b * 0.09 + 0.06*10 * this.ufAplica/(ca + 0.06 ca) 
                        
                        
                        
                        //hs=pn1* 10 * this.ufAplica + (ca(1-p1) - 10 * this.ufAplica)*pn2
                        //hs=pn1* 10 * this.ufAplica + ca(1-p1)*pn2 -10*this.ufAplica*pn2
                        //MT-VDES-p1*ca = pn1* 10 * this.ufAplica + ca(1-p1)*pn2 -10*this.ufAplica*pn2
                        //MT-VDES-p1*ca = pn1* 10 * this.ufAplica + ca*pn21-capn2p1 -10*this.ufAplica*pn2
                        //MT-VDES -pn1* 10 * this.ufAplica +10*this.ufAplica*pn2+ ca*pn21= -capn2p1 +p1*ca
                       
                         //MT-VDES -pn1* 10 * this.ufAplica +10*this.ufAplica*pn2+ ca*pn21=p1(ca-pn2ca) 
                        
                       //  p1 = (MT-VDES -pn1* 10 * this.ufAplica +10*this.ufAplica*pn2+ ca*pn2)/(ca-pn2*ca);
                         
                         
                         
                   // Messagebox.show("P1-1:["+p1+"]");
                         
                         
                       // HS = Math.round(Math.round((Math.round(((float) 10 * (float) this.ufAplica)) * 0.09)) + Math.round(Math.round(Math.round((Math.round((float) (float) ca*(1-p1)) - Math.round(((float) 10 * (float) this.ufAplica))))) * (float) 0.06));
                      
                       

                       // hs= 10  * this.ufAplica *0.09  +  ca *( 1 -p1) - 10 * this.ufAplica * (float) 0.06 ;
                      // MT-VDES-p1*ca= HS=F(p1*ca)=10  * this.ufAplica *0.09  +  ca *( 1 -p1) - 10 * this.ufAplica * (float) 0.06 ;
                       
                      // MT-VDES-p1*ca=10*this.ufAplica *0.09 + ca *( 1 -p1) -10 * this.ufAplica * (float) 0.06 ;
                       
                      // MT-VDES-p1*ca=10*this.ufAplica*pn1+ca*(1-p1)-10*this.ufAplica*pn2;
                      // MT-VDES-10*this.ufAplica*pn1+10*this.ufAplica*pn2=ca*(1-p1)+p1*ca
                      // MT-VDES-10*this.ufAplica*pn1+10*this.ufAplica*pn2= ca-ca*p1+p1*ca      
                      
                      
                      
                        //Honor2=(b*0.09) + (a - b) * c * 0.06;

                    } else if (a > ((float) 50 * c)) {
                        float uno = (float) ((float) 10 * c) * (float) 0.09;
                        float dos = (float) (40 * c * (float) 0.06);
                        float tes_part1 = a;
                        float tres_part2 = ((float) (50 * c));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;
                             
                        
                        
                        //HS= uno + dos + tres;
                        //HS =10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + (tes_part1 -tres_part2) * pn3
                        
                        //HS =10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + (ca*p1 - 50 * this.ufAplica) * pn3
                        
                        
                    //    HS=10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + ca*p1*pn3 - 50 * this.ufAplica* pn3
                        
                      //  MT-VDES-p1*ca =10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + ca*p1*pn3 - 50 * this.ufAplica* pn3
                      
                      
                    //  MT-VDES- 10*this.ufAplica*pn1 -40*this.ufAplica*pn2 - 50 * this.ufAplica* pn3 = ca*p1pn3 + p1*ca 
                              
                  //    MT-VDES- 10*this.ufAplica*pn1 -40*this.ufAplica*pn2 - 50 * this.ufAplica* pn3 = p1(ca*pn3 + *ca)
                      //        p1_3= MT-VDES- 10*this.ufAplica*pn1 -40*this.ufAplica*pn2 - 50 * this.ufAplica* pn3/(ca*pn3 + *ca)
                      
                      
                      
                      
                        
                        
                        //HS= 10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + (ca - ca*p1 - 50 * this.ufAplica) * pn3
                        //HS = 10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + pn3*ca - pn3*ca*p1 - pn3*50*this.ufAplica
                        //MT-VDES-p1*ca =10*this.ufAplica*pn1 + 40*this.ufAplica*pn2 + pn3*ca - pn3*ca*p1 - pn3*50*this.ufAplica
                        //MT-VDES-10*this.ufAplica*pn1-40*this.ufAplica*pn2-pn3*ca-pn3*50*this.ufAplica = - pn3*ca*p1 +p1*ca
                       // MT-VDES-10*this.ufAplica*pn1-40*this.ufAplica*pn2-pn3*ca-pn3*50*this.ufAplica=p1(ca - pn3*ca);
                        
                      //  p1_3 = MT-VDES-10*this.ufAplica*pn1-40*this.ufAplica*pn2-pn3*ca-pn3*50*this.ufAplica/(ca - pn3*ca);
                        
                        
                      //  HS = (10 * this.ufAplica  * 0.09)   + ( 40 * this.ufAplica  * 0.06 ) +  ( ca * (1-p1)  - 50 * this.ufAplica  ) * 0.03
                      // HS =  10*0.09 * this.ufAplica + 40*0.06 * this.ufAplica  +  0.03*ca * (1- p1) - 50*0.03 * this.ufAplica
                        
                      
                      
                      
                        
                    }

                } else {

                    porcentaje_honorjud = (float) 0.50;

                    float capital2 = (float) capital_a_recibir;
                    float rango0_500 = ((float) 500 * (float) this.ufAplica);

                    if (capital2 <= rango0_500) {

                        honorariojud_sobrecondonado = (float) capital_a_recibir * (float) 0.15;

                    } else if ((500 * this.ufAplica) < capital_a_recibir && capital_a_recibir <= (3000 * this.ufAplica)) {

                        float uf = (float) this.ufAplica;
                        float capital = (float) capital_a_recibir;
                        float ptje2 = (float) 0.05;
                        float unff = ((float) 500 * uf);

                        float hh = Math.round((unff * 0.15));
                        float amenosb = Math.round((capital - unff));
                        float ww = Math.round(amenosb);
                        float ww3 = Math.round(ww * (float) 0.05);

                        // honorariojud_sobrecondonado = (unff) + ( capital- ((float)10 * uf)) * uf * ptje2;
                        honorariojud_sobrecondonado = Math.round(hh + ww3);
                    } else if ((float) capital_a_recibir > ((float) 3000 * (float) this.ufAplica)) {
                        float uno = (float) (500 * (float) this.ufAplica) * (float) 0.15;
                        float dos = (float) (2500 * (float) this.ufAplica * (float) 0.06);
                        float tes_part1 = (float) capital_a_recibir;
                        float tres_part2 = ((float) (3000 * this.ufAplica));
                        float tres = ((float) tes_part1 - (float) tres_part2) * (float) 0.03;
                        honorariojud_sobrecondonado = uno + dos + tres;

                    }

                }

         
                float honorarioJudCondonado2 = (float) honorariojud_sobrecondonado * (float) PorcentajeCondonaHonorarios;
                float honorarioJudReibido2 = (float) honorariojud_sobrecondonado - (float) honorarioJudCondonado2;
                temp.setHonorario_condona(honorarioJudCondonado2);
                temp.setHonorario_recibe(honorarioJudReibido2);
                ii.add(temp);

                
                
                
            }
        }
        
        
        
       
        return ii;
    }

    
    
    
        
        //
    //
    //    recibe    ;  solo monto total campaña y interes=0 y honor =100%
    //    A) list de operaciones y     
    //    B) el % condonacion ingresado
    //    B.1) se agrega  MontoTptal   VDES para el calculo de 
    //    C) rutCliente Formateado
    //
    public float calculaHonorariosJudicialesV3(ListModelList<DetalleCliente> ListDetOperModel, int RutClienteFormateado,float MT,float VDES, Condonacion condonacion) {
      //  ListModelList<> ii = null;
       ListModelList<HornorariosJudicialesCliente> ii = new ListModelList<HornorariosJudicialesCliente>();
        String oper = "NULL";
        float capital_condonado = 0;
        float reorno = 0;

     
       
        
        
        // total saldo insoluto
        
        float total_insoluto=0;
         for (int i = 0; i < ListDetOperModel.getSize(); i++) {
         total_insoluto=total_insoluto +  (float) ListDetOperModel.get(i).getSaldoinsoluto();
         
         
         }
         

                
                
          //      saldo1*p1_1+saldo2*p1_1+saldo3*p1h_1 = p1_1(s1+S2+S3+S4) == saldo insoluto nuevo
                
         
         //s1 saldo operacion1
         //h1 honorario operacion1
        // FORMULA del porcentaje  x% * (s1+s2+s3...sn)  + h1+h2+h3+hn
        //    x% * total_insoluto + h1+h2+h3+hn= MontoTotalCampaña
        
       //   if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
        /**************************************************************
                float porcentaje_honorjud = 0;
                float honorariojud = 0;
                float honorariojud2 = 0;
                float honorariojud_sobrecondonado = 0;
                float Honor2 = 0;
              //  float capital_a_recibir = (float) ListDetOperModel.get(i).getSaldoinsoluto() - (float) capital_condonado;

  
                // 
                //p1_1*(ca - (pn1 * ca))= MT- VDE(s) -(pn1*ca)
                // p1_1 = (MT - VDES - (pn1 * ca)) / (ca - pn1 * ca);
                // p1_1 =    (MT - VDES) / ( pn1 * ca + ca ) ;
                // remplazar total_insoluto=p1_1(s1+S2+S3+S4) 
                //   p1_1 = (MT - VDES) / (pn1 * (p1_1)total_insoluto + (p1_1)total_insoluto);
                //p1_1 = (MT - VDES) /  (p1_1)(pn1*total_insoluto + total_insoluto);
                //p1_1 * (p1_1) =(MT - VDES) /  (pn1*total_insoluto + total_insoluto);
               // p1_1_1 * p1_1_1=(MT - VDES) / (pn1 * total_insoluto + total_insoluto);
                //p1_1_1^2=(MT - VDES) / (pn1 * total_insoluto + total_insoluto);
                p1_1_1=(float) Math.sqrt((MT - VDES) / (pn1 * total_insoluto + total_insoluto));
                
                
                p1_1 = (MT - VDES) / (pn1 * total_insoluto + total_insoluto);
               // temp.setHONOR_F1(p1_1*pn1*total_insoluto);
                float c_recibe1 = total_insoluto * p1_1_1;
  float c_recibe1_1 = total_insoluto * p1_1;
                Messagebox.show("Insoluto :" + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-1:[" + p1_1 + "] p1_1_1 :"+p1_1_1+" CRECIBE_1:[" + nf.format(c_recibe1_1).replaceFirst("Ch", "") + "]"+" CRECIBE:[" + nf.format(c_recibe1).replaceFirst("Ch", "") + "]");

                //    p1_2 = (MT - VDES - pn1 * 10 * this.ufAplica + 10 * this.ufAplica * pn2 + ca * pn2) / (ca - pn2 * ca);
                p1_2 = (MT - VDES - 10 * this.ufAplica * pn1 + pn2 * 10 * this.ufAplica) / (total_insoluto + pn2 * total_insoluto);
p1_2_2 =(float) Math.sqrt( (MT - VDES - 10 * this.ufAplica * pn1 + pn2 * 10 * this.ufAplica) / (total_insoluto + pn2 * total_insoluto));
                float c_recibe2 = total_insoluto * p1_2;
                 float c_recibe2_2 = total_insoluto * p1_2_2;
                Messagebox.show("Insoluto : " + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-2:[" + p1_2 + "] p1_2_2:"+p1_2_2+"CRECIBE_2:[" + nf.format(c_recibe2_2).replaceFirst("Ch", "") + "]"+"CRECIBE:[" + nf.format(c_recibe2).replaceFirst("Ch", "") + "]");

                //   p1_3 = MT - VDES - 10 * this.ufAplica * pn1 - 40 * this.ufAplica * pn2 - pn3 * ca - pn3 * 50 * this.ufAplica / (ca - pn3 * ca);
                p1_3 = (MT - VDES - 10 * this.ufAplica * pn1 - 40 * this.ufAplica * pn2 - 50 * this.ufAplica * pn3) / (total_insoluto * pn3 + total_insoluto);
  //p1_3_3 = (float) Math.sqrt( (MT - VDES - 10 * this.ufAplica * pn1 - 40 * this.ufAplica * pn2 - 50 * this.ufAplica * pn3) / (total_insoluto * pn3 + total_insoluto));
  
  //(MT - VDES)=total_insoluto(1-p1_3_3)(1+pn3) + 2*this.ufAplica+(10*pn1+40*pn2-50*pn3);
  //(MT - VDES)=(total_insoluto - p1_3_3*total_insoluto)(1+pn3) +  2*this.ufAplica+(10*pn1+40*pn2-50*pn3);

   //(MT - VDES)=((1+pn3)*total_insoluto - p1_3_3*total_insoluto * (1+pn3)) +  2*this.ufAplica+(10*pn1+40*pn2-50*pn3);
 //  ((MT - VDES) - (1+pn3)*total_insoluto - 2*this.ufAplica+(10*pn1+40*pn2-50*pn3)) = -p1_3_3*total_insoluto * (1+pn3)
   
 p1_3_3  = - ((MT - VDES) - (1+pn3)*total_insoluto - 2*this.ufAplica+(10*pn1+40*pn2-50*pn3)) / total_insoluto * (1+pn3);
 
 
           
     //p1_3_3 =1- (((MT - VDES) - 2*this.ufAplica+(10*pn1+40*pn2-50*pn3))/total_insoluto*(1+pn3));
  // nuevo calculo bueno
  
  // MT -VDES-(Ca1(1-x) + 
  
  
  
  reorno=p1_3_3*100;
                float c_recibe3 = total_insoluto * p1_3;
                 float c_recibe3_3 = total_insoluto * p1_3_3;
                Messagebox.show("total_insoluto:["+total_insoluto+"](MT - VDES) :["+(MT - VDES)+ "](1+pn3)*total_insoluto :[" +(1+pn3)*total_insoluto+ "]2*this.ufAplica+(10*pn1+40*pn2-50*pn3)) :[" +2*this.ufAplica+(10*pn1+40*pn2-50*pn3)+ "]2*this.ufAplica :["+2*this.ufAplica+"] (10*pn1+40*pn2-50*pn3) : ["+(10*pn1+40*pn2-50*pn3)+"]");
                Messagebox.show("insoluto :" + nf.format(total_insoluto).replaceFirst("Ch", "") + "P1-3:[" + p1_3 + "]  p1_3_3 #######:"+p1_3_3+"CRECIBE_3:[" + nf.format(c_recibe3_3).replaceFirst("Ch", "") + "]"+"CRECIBE:[" + nf.format(c_recibe3).replaceFirst("Ch", "") + "]");

                
                
                
                
                
                
                
                
                float honors = 0;
                for (int iii = 0; iii < ListDetOperModel.getSize(); iii++) {
                    honors = honors + ((float) ListDetOperModel.get(iii).getSaldoinsoluto() * p1_1);

                }
                float honors2 = 0;
                for (int iiii = 0; iiii < ListDetOperModel.getSize(); iiii++) {
                    honors2 = honors2 + ((float) ListDetOperModel.get(iiii).getSaldoinsoluto() * p1_2);

                }

                float honors3 = 0;
                for (int iiiii = 0; iiiii < ListDetOperModel.getSize(); iiiii++) {
                    honors3 = honors3 + ((float) ListDetOperModel.get(iiiii).getSaldoinsoluto() * p1_3);

                }

                Messagebox.show("honors3 :[" + nf.format(honors3).replaceFirst("Ch", "") + "]  suma : [" + nf.format(honors3 + VDES).replaceFirst("Ch", "") + "]  honors:[" + nf.format(honors).replaceFirst("Ch", "") + "]   suma  : [" + nf.format(honors + VDES).replaceFirst("Ch", "") + "]   honors2:[" + nf.format(honors2).replaceFirst("Ch", "") + "]    suma  : [" + nf.format(honors2 + VDES).replaceFirst("Ch", "") + "]");

                a = c_recibe3;

                */
                   float p1_1;      //porcentaje teorico 1
                p1_1 = 0;
                float p1_1_1;      //porcentaje teorico 1
                                p1_1_1 = 0;
                float p1_2 = 0;      // porcentaje teorico 2
                float p1_2_2 = 0;      // porcentaje teorico 2
                float p1_3 = 0;      // porcentaje teorico 3
                float p1_3_3 = 0;      // porcentaje teorico 3
                float pn1 = (float) 0.09;  // prejudicial 0.9
                float pn2 = (float) 0.06;  // prejudidial 0.6
                float pn3 = (float) 0.03;  // prejudicial 0.3
               // float ca = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                // float MT =

                //temp.setCapital_arecibir(capital_a_recibir);

                float c = Math.round((float) this.ufAplica);
                float a ;//= Math.round((float) capital_a_recibir);
                float b = Math.round(((float) 10 * c));

		float pn4 = (float)  0.15;  // judicial 0.15
		float pn5 = (float)  0.05;  // judicial 0.05
		    
                float rango0_500 = ((float) 500 * c);
                float rango500_3000 = ((float) 3000 * c);
                   
       float HS=0;
        
        
   /*    ExpressionParser parser = new SpelExpressionParser();
int two = parser.parseExpression("1 + 1").getValue(Integer.class); // 2 
double twentyFour = parser.parseExpression("2.0 * 3e0 * 4").getValue(Double.class); //24.0
    */   
      String  HSi1;
      String  HSi2;
      String  HSi3;
      String  HSi4;
      String  HSi5;
      String  HSi6;
      
               for (int i = 0; i < ListDetOperModel.getSize(); i++) {
              HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 0;
                String Xii;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
                temp.setOperacion1(ListDetOperModel.get(i).getOperacion());
                temp.setOperacion2(ListDetOperModel.get(i).getOperacionOriginal());
                temp.setSaldoInsoluto(ListDetOperModel.get(i).getSaldoinsoluto());
                float HSi = 0;
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) 
                {

                        HSi1 =Float.toString(Ci*pn1) + "- Xi*"+Float.toString(Ci*pn1);
                        temp.setHONOR_F1S(HSi1);
                        HSi2 =Float.toString(b*pn1 + Ci*pn2)+ "- Xi*"+Float.toString(Ci * pn2) +"-"+Float.toString(b * pn2);
                         temp.setHONOR_F2S(HSi2);
                        HSi3 =Float.toString(b*pn1+40*c*pn2+Ci*pn3) + "- Xi*"+Float.toString(Ci * pn3) + "-" + Float.toString(50 *c*pn3);
                         temp.setHONOR_F3S(HSi3);
                   

                } else 
                {

                  
                        HSi4 =Float.toString( Ci * pn4)+ "-"+"Xi*"+Float.toString(Ci * pn4);
                        temp.setHONOR_F1S(HSi4);
                        HSi5 =Float.toString( c * pn4 + pn5 * Ci)+ "- Xi * "+Float.toString(pn5* Ci) +"-"+Float.toString( pn5 * c) ;
                        temp.setHONOR_F2S(HSi5);
                        HSi6 =Float.toString(500*c*pn4 + 2500*c*pn2 + pn3*Ci)+ "- Xi* " + Float.toString(pn3* Ci)+"-" +Float.toString(pn3*3000*c);
                        temp.setHONOR_F3S(HSi6);
                

                }
                
                /// op1 sin  op2 con juicio 
                //1   MT -VDE(s) = (Ci1 - Xi * Ci1)  +  Ci1 * pn1 - Xi * Ci1 * pn1    +   (Ci2 - Xi * Ci2) + Ci2 * pn4 - Xi * Ci2 * pn4 
                
                

            }
            ii.add(temp);
        }
       
       
           float honotTeorico=0;    
         ////*/ Fase 2 calcularemos el teorico y luego cuando es el condonado aproximado respecto del monto campaña y total teorico
         for (int i = 0; i < ListDetOperModel.getSize(); i++) {
                float HSi = 0;
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 1;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
             
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) 
                {
                    HSi = (float) 0.15;
                    if ((Ci - Xi * Ci) <= b) {    // b=$279.534

                        HSi = Ci * pn1 - Xi * Ci * pn1;

                    } else if ((b) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= ((float) 50 * c)) {
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * -b * pn2;
                    } else if ((Ci - Xi * Ci) > ((float) 50 * c)) {
                        HSi = b * pn1 + 40 * c * pn2 + Ci * pn3 - Xi * Ci * pn3 - 50 * c * pn3;
                    }

                } else {

                    if ((Ci - Xi * Ci) <= rango0_500) {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                    } else if ((rango0_500) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= (rango500_3000)) {
                        HSi = c * pn4 + pn5 * Ci - pn5 * Xi * Ci - pn5 * c;
                    } else if ((Ci - Xi * Ci) > rango500_3000) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                    }

                }
                

            }
            honotTeorico=honotTeorico+HSi;
        }
         
         
         float porcentaje_teorico = (100- (MT*100/(total_insoluto + VDES +honotTeorico)))/100 ;
         
        
         
               
        
         
         /// AHora calculo el X%, teniendo el conomimiento del intervalo al cual debe pertenecer el x
         
             String  HSi11;
      String  HSi22;
      String  HSi33;
      String  HSi44;
      String  HSi55;
      String  HSi66;  
       String Super_Formula = "";  
         
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
                float HSi = 0;
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 0;
                float Xi0 = porcentaje_teorico;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
             
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion
      float tempppp=0;
//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) 
                {
                    HSi = (float) 0.15;
                    if (Xi0>=1 -b/Ci) {    //(Ci - Xi0 * Ci) <= b  b=$279.534  Ci - Xi0 * Ci <= b=== -Xi0=  b-Ci/Ci=>   Xi0=Ci/Ci -b/Ci ==>  Xi0<=1 -b/Ci

                        HSi = Ci * pn1 - Xi * Ci * pn1;
                         Super_Formula =Super_Formula+"+"+Float.toString(Ci*pn1) + "- Xi*"+Float.toString(Ci*pn1);
                       // temp.setHONOR_F1S(HSi1);

                    } else if ( 1 -b/Ci >Xi0  &&  Xi0 >= 1-(50*c/Ci) ) 
                    {  // (Ci - Xi0 * Ci) <= 50*c ==>  50c-Ci/Ci ==>Xi0 <= 1-50c/Ci
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * - b*pn2;
                    //    HSI  = 10*uf*0.09 + 0.06*Ci - 0.06*Xi*Ci- 0.06*10*uf;
                        
                        
                         Super_Formula =Super_Formula+"+"+Float.toString(b*pn1 + Ci*pn2)+ "- Xi*"+Float.toString(Ci * pn2) +"-"+Float.toString(b * pn2);
                    } else if ( Xi0 < 1-(50*c/Ci)) 
                    {
                        HSi = b*pn1  + 40*c*pn2 + Ci*pn3 -Xi*Ci*pn3 - 50*c*pn3;
                        
                      //  HDI = 10*uf*0.09 +  40*uf*0.06 +0.03*(Ci-XiCi) - 0.03*50*uf
                    //  HDI =10*uf*0.09 + 40*uf*0.06 +0.03*Ci-0.03*XiCi - 0.03*50*uf 
                        
                        Super_Formula =Super_Formula+"+"+Float.toString(b*pn1+40*c*pn2+Ci*pn3) + "- Xi*"+Float.toString(Ci * pn3) + "-" + Float.toString(50 *c*pn3);
                    }

                } else {

                    if (Xi0>=1-(500*c/Ci))  //Xi0<=1-(500*c/Ci)
                    {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                     //   HSI = 0.15*Ci - 0.15 * Xi*Ci
                        
                        
                        
                         Super_Formula =Super_Formula+"+"+Float.toString( Ci * pn4)+ "-"+"Xi*"+Float.toString(Ci * pn4);
                    } else if (Xi0<1-(500*c/Ci) && Xi0>= 1- 3000*c/Ci) //Xi0<= 1- 3000*c/Ci
                    {
                        
                        
                        tempppp= c*pn4 + pn5*Ci - pn5*Xi*Ci - pn5*c;
                      //  HSi = c*pn4 + pn5*Ci - pn5*Xi*Ci - pn5*c;
                        HSi=(float)(500*this.ufAplica* 0.15 + Ci*0.05- Xi*Ci*0.05 - 500*this.ufAplica*0.05);
                       // HSI=500*this.ufAplica* 0.15 + Ci*0.05- Xi*Ci*0.05 - 500*this.ufAplica*0.05
                        
                         Super_Formula =Super_Formula+"+"+Float.toString(500*c*pn4 + pn5*Ci)+ "- Xi * "+Float.toString(pn5* Ci) +"-"+Float.toString(500*pn5*c) ;
                         
                    } else if (Xi0 < 1- 3000*c/Ci) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                      //  HSi = 500 *this.ufAplica*0.15 + 2500 *this.ufAplica*0.06 + Ci*0.03-CiXi*0.03 - 3000 * this.ufAplica*0.03
                        
                        
                         Super_Formula =Super_Formula +"+"+Float.toString(500*c*pn4 + 2500*c*pn2 + pn3*Ci)+ "- Xi* " + Float.toString(pn3* Ci)+"-" +Float.toString(pn3*3000*c);
                    }

                }
                

            }
          //  honotTeorico=honotTeorico+HSi;
        }
         
         
               
               
               
       //// este es el original
       /*
        
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 0;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
                float HSi = 0;
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    HSi = (float) 0.15;
                    if ((Ci - Xi * Ci) <= b) {    // b=$279.534

                        HSi = Ci * pn1 - Xi * Ci * pn1;

                    } else if ((b) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= ((float) 50 * c)) {
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * -b * pn2;
                    } else if ((Ci - Xi * Ci) > ((float) 50 * c)) {
                        HSi = b * pn1 + 40 * c * pn2 + Ci * pn3 - Xi * Ci * pn3 - 50 * c * pn3;
                    }

                } else {

                    if ((Ci - Xi * Ci) <= rango0_500) {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                    } else if ((rango0_500) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= (rango500_3000)) {
                        HSi = c * pn4 + pn5 * Ci - pn5 * Xi * Ci - pn5 * c;
                    } else if ((Ci - Xi * Ci) > rango500_3000) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                    }

                }

            }
        }*/
       
       
 /*  ScriptEngine engine = new ScriptEngineManager().getEngineByName("scala");
   
   
     String testScript3 = "a+5";
     try{
        engine.eval(testScript3);
     }catch(ScriptException e){
     
     
     }
     */
     
 

/*
try
{
ExecutorService exec = Executors.newFixedThreadPool(1);
Expression e = new ExpressionBuilder("3log(y)/(x+1)");//.variables("x", "y").build().setVariable("x", 2.3).setVariable("y", 3.14);

Future<Double> future = e.evaluateAsync(exec);
double result = future.get();
}catch (Exception e){


}*/

// float porcentaje_real = (100- (MT*100/(total_insoluto + VDES +honotPractico)))/100 ;  


/*
double result = new ExpressionBuilder("2cos(xy)")
        .variables("x","y")
        .build()
        .setVariable("x", 0.5d)
        .setVariable("y", 0.25d)
        .evaluate();
//assertEquals(2d * Math.cos(0.5d * 0.25d), result, 0d);

float pt2=0;
//double honotPractico = new ExpressionBuilder(100- (MT*100/(total_insoluto + VDES + Super_Formula)) / 100).variables("x","y").build().setVariable("Xi", 0.5d)    // .setVariable("y", 0.25d)        .evaluate();
assertEquals(2d * Math.cos(0.5d * 0.25d), result, 0d);*/

Expression e = new Expression("solve( 2*x - 4, x, 0, 10 )");
//mXparser.consolePrintln("Res: " +  + " = " + e.calculate() );
String value=e.getExpressionString();
double value2 =e.calculate();



Expression e2 = new Expression("solve("+total_insoluto+"-"+MT+"+"+VDES+"-Xi*"+total_insoluto+""+Super_Formula+", Xi, 0, 1 )");
Expression e22 = new Expression("solve("+MT+"-"+VDES+"-"+total_insoluto+"+Xi*"+total_insoluto+"-("+Super_Formula+"), Xi, 0, 1 )");
//mXparser.consolePrintln("Res: " +  + " = " + e.calculate() );
String value22=e2.getExpressionString();
float value23 =(float) e2.calculate();


String value222=e22.getExpressionString();
double value24 =e22.calculate();   
    // ((BooleanSetting)(((IMain)engine).settings().usejavacp()))      .value_$eq(true);
   
     //  Expression e = new Expression("( 2 + 3/4 + sin(pi) )/2");
//double v = e.calculate();
 //      Function f = new Function("f(x) = sin(x + pi)/2 + 1");
//Expression e = new Expression("f(pi)", f);
//mXparser.consolePrintln("Res: " + e.getExpressionString() + " = " + e.calculate());
       
  // BigDecimal result = null;
   
   //result = new Expression("not(x<7 || sqrt(max(x,9)) <= 3))").with("x","22.9").eval();
      //  double twentyFour = parser.parseExpression("2.0 * 3e0 * 4").getValue(Double.class); //24.0
//   float porcentaje_real = (100- (MT*100/(total_insoluto + VDES +honotPractico)))/100 ;        
      //  Xi = (100- (MT*100/(total_insoluto + VDES +(1545Xi+545454Xi)))/100 ;  
        
        
      // reorno='50,5';
               
                   
               
               return (float)value23*100;
    }

  
    
    
    
    
    
    
    
    
            //
    //
    //    recibe    ;  solo monto ingresadoCliente y interes=0 y honor =100%
    //    A) list de operaciones y     
    //    B) el % condonacion ingresado
    //    B.1) se agrega  MontoTptal   VDES para el calculo de 
    //    C) rutCliente Formateado
    //
    public float calculaHonorariosJudicialesV4(ListModelList<DetalleCliente> ListDetOperModel, int RutClienteFormateado,float MT,float VDES) {
      //  ListModelList<> ii = null;
       ListModelList<HornorariosJudicialesCliente> ii = new ListModelList<HornorariosJudicialesCliente>();
        String oper = "NULL";
        float capital_condonado = 0;
        float reorno = 0;

     
       
        
        
        // total saldo insoluto
        
        float total_insoluto=0;
         for (int i = 0; i < ListDetOperModel.getSize(); i++) {
         total_insoluto=total_insoluto +  (float) ListDetOperModel.get(i).getSaldoinsoluto();
         
         
         }

                   float p1_1;      //porcentaje teorico 1
                p1_1 = 0;
                float p1_1_1;      //porcentaje teorico 1
                                p1_1_1 = 0;
                float p1_2 = 0;      // porcentaje teorico 2
                float p1_2_2 = 0;      // porcentaje teorico 2
                float p1_3 = 0;      // porcentaje teorico 3
                float p1_3_3 = 0;      // porcentaje teorico 3
                float pn1 = (float) 0.09;  // prejudicial 0.9
                float pn2 = (float) 0.06;  // prejudidial 0.6
                float pn3 = (float) 0.03;  // prejudicial 0.3
               // float ca = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                // float MT =

                //temp.setCapital_arecibir(capital_a_recibir);

                float c = Math.round((float) this.ufAplica);
                float a ;//= Math.round((float) capital_a_recibir);
                float b = Math.round(((float) 10 * c));

		float pn4 = (float)  0.15;  // judicial 0.15
		float pn5 = (float)  0.05;  // judicial 0.05
		    
                float rango0_500 = ((float) 500 * c);
                float rango500_3000 = ((float) 3000 * c);
                   

       
       
           float honotTeorico=0;    
         ////*/ Fase 2 calcularemos el teorico y luego cuando es el condonado aproximado respecto del monto campaña y total teorico
         for (int i = 0; i < ListDetOperModel.getSize(); i++) {
                float HSi = 0;
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 1;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
             
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) 
                {
                    HSi = (float) 0.15;
                    if ((Ci - Xi * Ci) <= b) {    // b=$279.534

                        HSi = Ci * pn1 - Xi * Ci * pn1;

                    } else if ((b) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= ((float) 50 * c)) {
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * -b * pn2;
                    } else if ((Ci - Xi * Ci) > ((float) 50 * c)) {
                        HSi = b * pn1 + 40 * c * pn2 + Ci * pn3 - Xi * Ci * pn3 - 50 * c * pn3;
                    }

                } else {

                    if ((Ci - Xi * Ci) <= rango0_500) {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                    } else if ((rango0_500) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= (rango500_3000)) {
                        HSi = c * pn4 + pn5 * Ci - pn5 * Xi * Ci - pn5 * c;
                    } else if ((Ci - Xi * Ci) > rango500_3000) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                    }

                }
                

            }
            honotTeorico=honotTeorico+HSi;
        }
         
         
         float porcentaje_teorico = (100- (MT*100/(total_insoluto + VDES +honotTeorico)))/100 ;
         
        
         
               
        
         
         /// AHora calculo el X%, teniendo el conomimiento del intervalo al cual debe pertenecer el x
         
             String  HSi11;
      String  HSi22;
      String  HSi33;
      String  HSi44;
      String  HSi55;
      String  HSi66;  
       String Super_Formula = "";  
         
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
                float HSi = 0;
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 0;
                float Xi0 = porcentaje_teorico;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
             
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion
      float tempppp=0;
//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) 
                {
                    HSi = (float) 0.15;
                    if (Xi0>=1 -b/Ci) {    //(Ci - Xi0 * Ci) <= b  b=$279.534  Ci - Xi0 * Ci <= b=== -Xi0=  b-Ci/Ci=>   Xi0=Ci/Ci -b/Ci ==>  Xi0<=1 -b/Ci

                        HSi = Ci * pn1 - Xi * Ci * pn1;
                         Super_Formula =Super_Formula+"+"+Float.toString(Ci*pn1) + "- Xi*"+Float.toString(Ci*pn1);
                       // temp.setHONOR_F1S(HSi1);

                    } else if ( 1 -b/Ci >Xi0  &&  Xi0 >= 1-(50*c/Ci) ) 
                    {  // (Ci - Xi0 * Ci) <= 50*c ==>  50c-Ci/Ci ==>Xi0 <= 1-50c/Ci
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * - b*pn2;
                    //    HSI  = 10*uf*0.09 + 0.06*Ci - 0.06*Xi*Ci- 0.06*10*uf;
                        
                        
                         Super_Formula =Super_Formula+"+"+Float.toString(b*pn1 + Ci*pn2)+ "- Xi*"+Float.toString(Ci * pn2) +"-"+Float.toString(b * pn2);
                    } else if ( Xi0 < 1-(50*c/Ci)) 
                    {
                        HSi = b*pn1  + 40*c*pn2 + Ci*pn3 -Xi*Ci*pn3 - 50*c*pn3;
                        
                      //  HDI = 10*uf*0.09 +  40*uf*0.06 +0.03*(Ci-XiCi) - 0.03*50*uf
                    //  HDI =10*uf*0.09 + 40*uf*0.06 +0.03*Ci-0.03*XiCi - 0.03*50*uf 
                        
                        Super_Formula =Super_Formula+"+"+Float.toString(b*pn1+40*c*pn2+Ci*pn3) + "- Xi*"+Float.toString(Ci * pn3) + "-" + Float.toString(50 *c*pn3);
                    }

                } else {

                    if (Xi0>=1-(500*c/Ci))  //Xi0<=1-(500*c/Ci)
                    {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                     //   HSI = 0.15*Ci - 0.15 * Xi*Ci
                        
                        
                        
                         Super_Formula =Super_Formula+"+"+Float.toString( Ci * pn4)+ "-"+"Xi*"+Float.toString(Ci * pn4);
                    } else if (Xi0<1-(500*c/Ci) && Xi0>= 1- 3000*c/Ci) //Xi0<= 1- 3000*c/Ci
                    {
                        
                        
                        tempppp= c*pn4 + pn5*Ci - pn5*Xi*Ci - pn5*c;
                      //  HSi = c*pn4 + pn5*Ci - pn5*Xi*Ci - pn5*c;
                        HSi=(float)(500*this.ufAplica* 0.15 + Ci*0.05- Xi*Ci*0.05 - 500*this.ufAplica*0.05);
                       // HSI=500*this.ufAplica* 0.15 + Ci*0.05- Xi*Ci*0.05 - 500*this.ufAplica*0.05
                        
                         Super_Formula =Super_Formula+"+"+Float.toString(500*c*pn4 + pn5*Ci)+ "- Xi * "+Float.toString(pn5* Ci) +"-"+Float.toString(500*pn5*c) ;
                         
                    } else if (Xi0 < 1- 3000*c/Ci) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                      //  HSi = 500 *this.ufAplica*0.15 + 2500 *this.ufAplica*0.06 + Ci*0.03-CiXi*0.03 - 3000 * this.ufAplica*0.03
                        
                        
                         Super_Formula =Super_Formula +"+"+Float.toString(500*c*pn4 + 2500*c*pn2 + pn3*Ci)+ "- Xi* " + Float.toString(pn3* Ci)+"-" +Float.toString(pn3*3000*c);
                    }

                }
                

            }
          //  honotTeorico=honotTeorico+HSi;
        }
         
         
               
               
               
       //// este es el original
       /*
        
        for (int i = 0; i < ListDetOperModel.getSize(); i++) {
            //  HornorariosJudicialesCliente temp = new HornorariosJudicialesCliente();
            if (ListDetOperModel.get(i) != null) {
                float Xi = 0;
                oper = ListDetOperModel.get(i).getOperacion();      //operacion actual
                float HSi = 0;
                float Ci = (float) ListDetOperModel.get(i).getSaldoinsoluto();
                float Ri = (float) (Ci - Xi * Ci); // Ci= insoluto de cada Operacion

//  Calculo 2 del HONORARIO
                if (!this._operJDBC.TieneRol(RutClienteFormateado, ListDetOperModel.get(i).getOperacionOriginal())) {
                    HSi = (float) 0.15;
                    if ((Ci - Xi * Ci) <= b) {    // b=$279.534

                        HSi = Ci * pn1 - Xi * Ci * pn1;

                    } else if ((b) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= ((float) 50 * c)) {
                        HSi = b * pn1 + Ci * pn2 - Xi * Ci * pn2 * -b * pn2;
                    } else if ((Ci - Xi * Ci) > ((float) 50 * c)) {
                        HSi = b * pn1 + 40 * c * pn2 + Ci * pn3 - Xi * Ci * pn3 - 50 * c * pn3;
                    }

                } else {

                    if ((Ci - Xi * Ci) <= rango0_500) {
                        HSi = Ci * pn4 - Xi * Ci * pn4;
                    } else if ((rango0_500) < (Ci - Xi * Ci) && (Ci - Xi * Ci) <= (rango500_3000)) {
                        HSi = c * pn4 + pn5 * Ci - pn5 * Xi * Ci - pn5 * c;
                    } else if ((Ci - Xi * Ci) > rango500_3000) //   (Ci-Xi*Ci) >3000 * c ==   Xi > (Ci-3000+c)/Ci
                    {
                        HSi = 500 * c * pn4 + 2500 * c * pn2 + pn3 * Ci - pn3 * Xi * Ci - pn3 * 3000 * c;
                    }

                }

            }
        }*/
       
       
 /*  ScriptEngine engine = new ScriptEngineManager().getEngineByName("scala");
   
   
     String testScript3 = "a+5";
     try{
        engine.eval(testScript3);
     }catch(ScriptException e){
     
     
     }
     */
     
 

/*
try
{
ExecutorService exec = Executors.newFixedThreadPool(1);
Expression e = new ExpressionBuilder("3log(y)/(x+1)");//.variables("x", "y").build().setVariable("x", 2.3).setVariable("y", 3.14);

Future<Double> future = e.evaluateAsync(exec);
double result = future.get();
}catch (Exception e){


}*/

// float porcentaje_real = (100- (MT*100/(total_insoluto + VDES +honotPractico)))/100 ;  


/*
double result = new ExpressionBuilder("2cos(xy)")
        .variables("x","y")
        .build()
        .setVariable("x", 0.5d)
        .setVariable("y", 0.25d)
        .evaluate();
//assertEquals(2d * Math.cos(0.5d * 0.25d), result, 0d);

float pt2=0;
//double honotPractico = new ExpressionBuilder(100- (MT*100/(total_insoluto + VDES + Super_Formula)) / 100).variables("x","y").build().setVariable("Xi", 0.5d)    // .setVariable("y", 0.25d)        .evaluate();
assertEquals(2d * Math.cos(0.5d * 0.25d), result, 0d);*/

Expression e = new Expression("solve( 2*x - 4, x, 0, 10 )");
//mXparser.consolePrintln("Res: " +  + " = " + e.calculate() );
String value=e.getExpressionString();
double value2 =e.calculate();



Expression e2 = new Expression("solve("+total_insoluto+"-"+MT+"+"+VDES+"-Xi*"+total_insoluto+""+Super_Formula+", Xi, 0, 1 )");
Expression e22 = new Expression("solve("+MT+"-"+VDES+"-"+total_insoluto+"+Xi*"+total_insoluto+"-("+Super_Formula+"), Xi, 0, 1 )");
//mXparser.consolePrintln("Res: " +  + " = " + e.calculate() );
String value22=e2.getExpressionString();
float value23 =(float) e2.calculate();


String value222=e22.getExpressionString();
double value24 =e22.calculate();   
    // ((BooleanSetting)(((IMain)engine).settings().usejavacp()))      .value_$eq(true);
   
     //  Expression e = new Expression("( 2 + 3/4 + sin(pi) )/2");
//double v = e.calculate();
 //      Function f = new Function("f(x) = sin(x + pi)/2 + 1");
//Expression e = new Expression("f(pi)", f);
//mXparser.consolePrintln("Res: " + e.getExpressionString() + " = " + e.calculate());
       
  // BigDecimal result = null;
   
   //result = new Expression("not(x<7 || sqrt(max(x,9)) <= 3))").with("x","22.9").eval();
      //  double twentyFour = parser.parseExpression("2.0 * 3e0 * 4").getValue(Double.class); //24.0
//   float porcentaje_real = (100- (MT*100/(total_insoluto + VDES +honotPractico)))/100 ;        
      //  Xi = (100- (MT*100/(total_insoluto + VDES +(1545Xi+545454Xi)))/100 ;  
        
        
      // reorno='50,5';
               
                   
               
               return (float)value23*100;
    }
    
    
    
    
    public float PesoTofloat(String recibePeso){
    float entregafloat=0;
           //String jjx = valueneg.getValue();
         recibePeso = recibePeso.replace("$", "");
         recibePeso = recibePeso.replace(".", "");
    
    entregafloat=Float.parseFloat(recibePeso);
    
    
    
    return entregafloat;
    }

    
    
    public boolean is_NovedadMontosNiOperaciones(int rut,String cuenta) {
        
        boolean result = false;
        JdbcTemplate jdbcTemplateObject;
        DataSource _internodataSource = null;
        ResultadoSp infocliente = null;
        // verificar si la infoprmacion d las tracking mora y tracking ope estan distinta a las de produccion
        DataSource dataSource;
        float morahoySisCon = 0;
        float morahoyProduccion = 0;
        int numero_operaciones_SisCon = 0;
        int numero_operaciones_SMallCore = 0;
        float sumaOperacionesTotal_SisCon = 0;
        float sumaOperacionesTotal_SmallCore = 0;

        int id_Cli = 7;
        String rutt = "jj";
        boolean existe = false;
        String SQL;
        List<ResultadoSp> dddddd = null;

        try {

            dataSource = mmmm.getDataSource();
          
              jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("sp_siscon_is_NovedadMontosNiOperaciones"). returningResultSet("rs", new ParameterizedRowMapper<ResultadoSp>() {

                    public ResultadoSp mapRow(ResultSet rs, int i) throws SQLException {

                        ResultadoSp ainfocliente = new ResultadoSp();
                        ainfocliente.setResp(rs.getString("resp"));


                        //ainfocliente.setSaldoTotalMoraPesos(nf.format(rs.getLong("fld_sdo_tot")).replaceFirst("Ch", ""));

                        return ainfocliente;

                    }
                ;
        });

            SqlParameterSource in = new MapSqlParameterSource().addValue("rut", rut);

            Map<String, Object> out = jdbcCall.execute(in);

            dddddd = (List<ResultadoSp>) out.get("rs");
            infocliente = (ResultadoSp) dddddd.get(0);

            
            result = infocliente.getResp().equals("1")? false:true;

        } catch (DataAccessException e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al buscar la id de cliente para la condonación: " + Integer.toString(id_condonacion) + " adjuntar\nError: " + e.toString());
            result = false;
        } catch (Exception e) {
            // Messagebox.show("Sr(a) Usuario(a), hubo un problema al cargar datos enc adjuntar\nError: " + e.toString());
            result = false;
        }

        return result;
    }
    
    public Boolean is_Ejecutivo(String cuenta) throws SQLException {
          DataSource dataSource;
        /*La consulta retorna 3 valores
        0 = cliente nuevo (retorna error, en error se captura 0, puede evaluar).
        1 = cliente existe (sistema no permite evaluación).        
        2 = cliente rechazado(puede volver a evaluar).
         */
        dataSource = mmmm.getDataSource();
        jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select   CASE\n" +
"    WHEN\n" +
"      at.cod = 'EJESICON'\n" +
"        THEN\n" +
"          1\n" +
"\n" +
"    ELSE\n" +
"      0\n" +
"  END as result from colaborador col \n" +
"                   inner join usuario us  on us.id_usuario=col.id_usuario\n" +
"				   inner join AreaTrabajo at on at.id=col.fk_di_id_AreaTrabajo\n" +
"				   where us.alias='"+cuenta+"'";
        int isEjecutivo = 0;

        try {
            isEjecutivo = jdbcTemplate.queryForInt(sql);
            
        } catch (DataAccessException ex) {

            SisCorelog("Query :[" + sql + "] estado [:"+cuenta+"] estado2 ["+dataSource.toString()+"]" + ex.getMessage());

            if (ex.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
                isEjecutivo = 0;
            }
        }

        SisCorelog("Query :[" + sql + "]");
        return isEjecutivo >0;
    }
    
}
