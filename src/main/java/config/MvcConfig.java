package config;

import configuracion.GeneralConf;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import org.zkoss.zkspringmvc.ZKUrlBasedViewResolver;
import org.zkoss.zkspringmvc.ZKView;
import org.zkoss.zkspringmvc.config.ZKWebMvcConfigurerAdapter;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DevolucionJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.LoteJDBC;
import sisboj.entidades.implementaciones.OperacionesJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;
import sisboj.entidades.implementaciones.TblHitosJDBC;
import sisboj.entidades.implementaciones.TipDctoJDBC;
import sisboj.entidades.implementaciones.DirLdapJDBC;
import sisboj.entidades.implementaciones.JerarquiaSolJDBC;
import sisboj.entidades.implementaciones.ProdTipoDocJDBC;
import sisboj.entidades.implementaciones.ProductosJDBC;
import sisboj.entidades.implementaciones.MotivoDevolucionJDBC;
import sisboj.entidades.implementaciones.SetDocumentoJDBC;
import sisboj.entidades.implementaciones.TipoDocumentoJDBC;
import sisboj.entidades.implementaciones.UbicacionJDBC;
import sisboj.entidades.implementaciones.UsuariosLdapJDBC;
import siscon.entidades.implementaciones.UsuarioPermisoJDBC;
import static siscore.comunes.LogController.SisCorelog;

/*los excludes y filter del final de linea son para no errar en nombres de clases iguales para distintos packages*/
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"siscore", "sisboj", "siscon", "sisprojects", "sisoper", "siscob"})
public class MvcConfig extends ZKWebMvcConfigurerAdapter {

    private String IpSql = "";// "localhost:1433";
    private String DondeEstoy = "";// "localhost:1433";
    private final String DveloperServer = "localhost";
    private final String DeveloperPort = "1433";
    private final String LucyServer = "localhost";
    private final String LucyPort = "1433";
    private String ddbbSql = "SysCore_Desarrollo";
    private String UserSql = "SysCore";
    private String PassUserSql = "syscore1";
    String internalIPs = "192\\.168\\.X\\.X" + "|";
    String My_ProduccionIP = "10\\.0\\.2\\.X" + "|" + "127\\.0\\.0\\.X" + "|";
    String MyDesarrollo = "161\\.131\\.189\\.X" + "|" + "161\\.131\\.230\\.X" + "|";
    String MyQA = "161\\.131\\.219\\.X" + "|";
    String QAProduccion = "161\\.131\\.189\\.X" + "|";
    String Produccion = "172\\.16\\.26\\.X" + "|" + "172\\.16\\.27\\.X" + "|";
    String host_source_prod = "";
    String db_source_prod = "";
    String db_source_prodCob = "";
    String IpMatch = "X\\.X\\.X\\.X" + "|";
    String host_source_lucy = "";
    String db_source_lucy = "";

    String usr_source_prod = "";
    String psw_source_prod = "";
    String usr_source_lucy = "";
    String psw_source_lucy = "";
    String host_source = "";
    String host_sourceCob = "";
    String db_source = "";
    String db_sourceCob = "";
    String usr_source_siscon = "";
    String psw_source_siscon = "";
    String usr_source = "";
    String psw_source = "";
    String host_source_syscon = "";
    String db_source_syscon = "";

    String host_source_sisboj = "";
    String db_source_sisboj = "";
    String usr_source_sisboj = "";
    String psw_source_sisboj = "";
    int notebook2 = 1;
    String hostt = "";

    String ipCertificacion = "172.16.109.91";//"10.68.2.22";// antes 161.131.230.219

    String Preproduccion;

    public String getAmbienteActual() {
        return AmbienteActual;
    }

    public void setAmbienteActual(String AmbienteActual) {
        this.AmbienteActual = AmbienteActual;
    }
    // String AmbienteActual = "Desarrollo";
    String AmbienteActual = "";
    private ParametrosCond parmCond;
    GeneralConf _conf;
    String host;
    String host2;
    String host3;
    InetAddress[] hostx;
    //public  MotorValidacionJDBC _valida;

    /**
     * @return the parmCond
     */
    public ParametrosCond getParmCond() {

        return parmCond;
    }

    /**
     * @param parmCond the parmCond to set
     */
    public void setParmCond(ParametrosCond parmCond) {
        this.parmCond = parmCond;
    }

    public MvcConfig() throws SQLException {

        //carga parametros del sistema
        parmCond = new ParametrosCond();

        String ip;
        Pattern ippatern = Pattern.compile("^(?:" + IpMatch.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Matcher mm;
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    ip = addr.getHostAddress();
                    String Name = iface.getDisplayName();
                    if (!Name.matches("(?i).*VirtualBox.*")) {
                        mm = ippatern.matcher(ip);
                        if (mm.matches()) {
                            host = ip;
                        }

                    }
                    System.out.println(Name + " " + ip);
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }

        //this.setAmbienteActual("PreProduccion");
         this.setAmbienteActual("Produccion");
        // this.setAmbienteActual("Desarrollo");
        if (notebook2 == 1) {

            if (this.getAmbienteActual().equals("PreProduccion")) {
                hostt = "161.131.189.197"; // CertificacionBanco }
                this.Preproduccion = "SysConProdIntegracion";
            } else if (this.getAmbienteActual().equals("Produccion")) {
                hostt = "172.16.26.42";
                this.Preproduccion = "SysConProd";
            } else if (this.getAmbienteActual().equals("Desarrollo")) {
                hostt = "192.168.56.1";
                this.Preproduccion = "SysConProd";
            }
        } else {
            hostt = "localhost";
        }

        
        /*  try {

             
              host = InetAddress.getLocalHost().getHostAddress();
             hostx = InetAddress.getAllByName(host);

          } catch (UnknownHostException ex) {
              System.out.println("ERR" + ex.getMessage());
              host = "localhost";
          }*/
        //Pattern p = Pattern.compile("^(?:"+internalIPs.replaceAll("X", "(?:\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])")+")$");      7
        Pattern p = Pattern.compile("^(?:" + internalIPs.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern Developer = Pattern.compile("^(?:" + MyDesarrollo.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern My_Produccion = Pattern.compile("^(?:" + My_ProduccionIP.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");
        Pattern AltaDisponibilidad = Pattern.compile("^(?:" + Produccion.replaceAll("X", "(?:\\\\d{1,2}|1\\\\d{2}|2[0-4]\\\\d|25[0-5])") + ")$");

        System.out.println("HOST:===> " + hostt);

        Matcher m = p.matcher(hostt);
        Matcher m2dev = Developer.matcher(hostt);
        Matcher m3Prod = My_Produccion.matcher(hostt);

        Matcher MatcherAltaDisponibilidad = AltaDisponibilidad.matcher(hostt);

        SisCorelog("MccConfig: [" + hostt + "]");

        if (m.matches()) {     //// house Instance



            this.DondeEstoy = hostt;
            this.host_source_prod = hostt;
            this.db_source_prod = this.Preproduccion;
            this.db_source_prodCob = "SysCob";
            this.host_source_lucy = hostt;
            this.db_source_lucy = this.Preproduccion;
            this.usr_source_prod = "siscon";
            this.psw_source_prod = "password";
            this.db_source = this.Preproduccion;
            this.db_sourceCob = "SysCob";
            this.host_source = hostt;
            //  this.host_source = "";
            this.usr_source_lucy = "siscon";
            this.psw_source_lucy = "password";
            this.db_source_syscon = this.Preproduccion;    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
            this.host_source_syscon = hostt;
            this.usr_source_siscon = "siscon";
            this.psw_source_siscon = "password";

            this.usr_source = "siscon";
            this.psw_source = "password";

        } else if (m2dev.matches()) {    // Local Work Instance    mi PC 1 
            //this.host_source_prod = "161.131.230.214";
            this.host_source_prod = "172.16.33.176";
            this.db_source_prod = "SysConProd";
            this.db_sourceCob = "SysCob";
            this.host_source_lucy = "172.16.33.176";
            this.db_source_lucy = "IN_CBZA";
            this.usr_source_prod = "siscore";
            this.psw_source_prod = "siscorebci";
            if (this.getAmbienteActual().equals("Desarrollo")) {
                this.db_source = this.Preproduccion;           ///cambiar por SysCon para Mi PC Work  Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source = this.ipCertificacion;
                this.db_source_syscon = this.Preproduccion;    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
                this.db_sourceCob = "SysCob";
                this.host_source_syscon = this.ipCertificacion;
                this.usr_source_siscon = "siscon";
                this.psw_source_siscon = "password";
                this.usr_source = "siscon";
                this.psw_source = "password";
                this.db_source_sisboj = "SysBoj";
            } else if (this.getAmbienteActual().equals("Produccion")) {
                this.db_source = "SysConProd";           ///cambiar por SysCon para Mi PC Work  Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source = "172.16.33.176";
                this.db_sourceCob = "SysCob";
                this.db_source_syscon = "SysConProd";    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source_syscon = "172.16.33.176";
                this.usr_source_siscon = "siscore";
                this.psw_source_siscon = "siscorebci";
                this.usr_source = "siscore";
                this.psw_source = "siscorebci";
                this.host_source_sisboj = "172.16.33.176";
                this.db_source_sisboj = "SysBoj";
                this.usr_source_sisboj = "siscore";
                this.psw_source_sisboj = "siscorebci";

            } else if (this.getAmbienteActual().equals("PreProduccion")) {  //2
                this.db_source = this.Preproduccion;           ///cambiar por SysCon para Mi PC Work  Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source = this.ipCertificacion;
                this.db_sourceCob = "SysCob";
                this.db_source_syscon = this.Preproduccion;    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source_syscon = this.ipCertificacion;
                this.db_source_sisboj = "SysBoj";            
                
                
                
                //usuarios
                this.usr_source_siscon = "siscon";
                this.psw_source_siscon = "password";
                this.usr_source = "siscon";
                this.psw_source = "password";

                this.usr_source_sisboj = "siscon";
                this.psw_source_sisboj = "password";
                System.out.println("ipCertificacion#####################################################################################################:Host Produccion [" + host_source_prod + "]    db_source :[" + db_source + "]");
                System.out.println("ipCertificacion#####################################################################################################:Host Certificacion [" + db_source_syscon + "]    db_source_certificacion :[" + host_source_syscon + "]");
            } else if (this.getAmbienteActual().equals("PreProduccionDataProd")) {
                this.db_source = "SysConPreProd";           ///cambiar por SysCon para Mi PC Work  Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source = this.ipCertificacion;
                this.db_sourceCob = "SysCob";
                this.db_source_syscon = "SysConPreProd";    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
                this.host_source_syscon = this.ipCertificacion;
                this.usr_source_siscon = "siscon";
                this.psw_source_siscon = "password";
                this.usr_source = "siscon";
                this.psw_source = "password";

            }

            this.usr_source_lucy = "siscore";
            this.psw_source_lucy = "siscorebci";

            // Desarrollo
            this.host_source_sisboj = this.ipCertificacion;

            /*Conexion BD Desarrollo*/
//            this.db_source_sisboj = "SysBoj";
            //this.db_source_sisboj = "SysBoj_20201";//no corre
            /*Conexion BD Certificacion*/
            this.db_source_sisboj = "SysBojCert";
            this.usr_source_sisboj = "siscon";
            this.psw_source_sisboj = "password";

            // Produccion
//            this.host_source_sisboj = "172.16.33.176";
//            this.db_source_sisboj = "SysBoj";
//            this.usr_source_sisboj = "siscore";
//            this.psw_source_sisboj = "siscorebci";
        } else if (m3Prod.matches()) {  //Production Instance CERTIFICACION para sisboj 

            //this.host_source_prod = "172.16.33.176";
            this.host_source_prod = "10.68.2.22";
            this.db_source_prod = "SysConProd";
            // this.host_source_lucy = "172.16.33.176";
            this.host_source_lucy = "10.68.2.22";
            this.db_source_lucy = "IN_CBZA";
            this.usr_source_lucy = "siscore";
            this.psw_source_lucy = "siscorebci";
            this.usr_source_prod = "siscore";
            this.psw_source_prod = "siscorebci";
//                this.db_source = "SysCon";
//                this.host_source = "10.68.2.22";
//                this.db_source_syscon = "SysCon";
//                this.host_source_syscon = "10.68.2.22";
//                this.usr_source_siscon = "siscon";
//                this.psw_source_siscon = "password";
//
//                this.usr_source = "siscon";
//                this.psw_source = "password";

            this.db_source = this.Preproduccion;           ///cambiar por SysCon para Mi PC Work  Desarrollo es SysConDev y Certificacion es SysCon
            this.host_source = this.ipCertificacion;
            this.db_source_syscon = this.Preproduccion;    ///cambiar por SysCon para Mi PC Work Desarrollo es SysConDev y Certificacion es SysCon
            this.host_source_syscon = this.ipCertificacion;
            this.usr_source_siscon = "siscon";
            this.psw_source_siscon = "password";
            this.usr_source = "siscon";
            this.psw_source = "password";
            // this.host_source_sisboj = "172.16.33.176";
            this.host_source_sisboj = "10.68.2.22";
            this.db_source_sisboj = "SysBojCert";
            this.usr_source_sisboj = "siscon";
            this.psw_source_sisboj = "password";

            // this.DondeEstoy = "161.131.230.214";
        } else if (MatcherAltaDisponibilidad.matches()) {   // Alta Disponibilidad Instance

            this.host_source_prod = "172.16.33.176";
            this.db_source_prod = "SysConProd";
            this.host_source_lucy = "172.16.33.176";
            this.db_source_lucy = "IN_CBZA";
            this.usr_source_lucy = "siscore";
            this.psw_source_lucy = "siscorebci";
            this.usr_source_prod = "siscore";
            this.psw_source_prod = "siscorebci";
            this.db_source = "SysConProd";
            this.host_source = "172.16.33.176";
            this.db_source_syscon = "SysConProd";
            this.host_source_syscon = "172.16.33.176";
            this.usr_source_siscon = "siscore";
            this.psw_source_siscon = "siscorebci";
            this.usr_source = "siscore";
            this.psw_source = "siscorebci";
            // this.DondeEstoy = "161.131.230.214";
            this.host_source_sisboj = "172.16.33.176";
            this.db_source_sisboj = "SysBoj";

            this.usr_source_sisboj = "siscore";
            this.psw_source_sisboj = "siscorebci";

        }

        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&Configuracion host_source_prod:[" + host_source_prod + "]    db_source :[" + db_source + "]");
    }

    @Bean
    public ViewResolver getViewResolver() {
        ZKUrlBasedViewResolver resolver = new ZKUrlBasedViewResolver();
        resolver.setViewClass(new ZKView().getClass());
        // System.err.print( "\n\n\n\t\t--- {###"+resolver.getApplicationContext()+ Sessions.getCurrent()+"####} ---\n\n\n" );
        resolver.setPrefix("/WEB-INF/");
        resolver.setSuffix("");

        return resolver;
    }

    /*
     * Configure ResourceHandlers to serve static resources like CSS/ Javascript etc...
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("*.css").addResourceLocations("/css/*");
        registry.addResourceHandler("*.js").addResourceLocations("/js/*");
        registry.addResourceHandler("*.dsp").addResourceLocations("/dsp/*");
        registry.addResourceHandler("*.jpg").addResourceLocations("/jpg/*");
        registry.addResourceHandler("*.png").addResourceLocations("/png/*");
        registry.addResourceHandler("*.svg").addResourceLocations("/svg/*");
        registry.addResourceHandler("*.woff").addResourceLocations("/fonts/*");
        registry.addResourceHandler("*.woff2").addResourceLocations("/fonts/*");
        registry.addResourceHandler("*.eot").addResourceLocations("/fonts/*");

    }

    @Bean(name = "datasource")
    public DataSource getDataSource() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source + ":1433/" + db_source + "");
        dataSource.setUsername(this.usr_source);
        dataSource.setPassword(this.psw_source);

        return dataSource;
    }

    @Bean(name = "datasource2")
    public DataSource getDataSource2() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source + ":1433/SysConProdIntegracion2");
        dataSource.setUsername(this.usr_source);
        dataSource.setPassword(this.psw_source);

        return dataSource;
    }

    @Bean(name = "datasourceCob")
    public DataSource getDataSourceCob() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source + ":1433/" + db_sourceCob + "");
        dataSource.setUsername(this.usr_source);
        dataSource.setPassword(this.psw_source);

        return dataSource;
    }

    @Bean(name = "datasourceSisBoj")
    public DataSource getDataSourceSisBoj() throws SQLException {

        //host_source_sisboj="10.68.2.22";
        //usr_source_sisboj="siscon";
        //psw_source_sisboj="password";
        //db_source_sisboj="SysBoj";
        System.out.println("host_source_sisboj-> " + host_source_sisboj);
        System.out.println("db_source_sisboj-> " + db_source_sisboj);
        System.out.println("usr_source_sisboj-> " + usr_source_sisboj);
        System.out.println("psw_source_sisboj-> " + psw_source_sisboj);

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source_sisboj + ":1433/" + db_source_sisboj + "");

        dataSource.setUsername(this.usr_source_sisboj);
        dataSource.setPassword(this.psw_source_sisboj);

        return dataSource;
    }

    @Bean(name = "datasourceProduccion")
    public DataSource getDataSourceProduccion() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source_prod + ":1433/" + db_source_prod + "");
        dataSource.setUsername(usr_source_prod);
        dataSource.setPassword(psw_source_prod);
        return dataSource;
    }

    @Bean(name = "datasourceProduccionCob")
    public DataSource getDataSourceProduccionCob() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");
        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source_prod + ":1433/" + db_source_prodCob + "");
        dataSource.setUsername(usr_source_prod);
        dataSource.setPassword(psw_source_prod);
        return dataSource;
    }

    @Bean(name = "datasourceLucy")
    public DataSource getDataSourceLucy() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("net.sourceforge.jtds.jdbc.Driver");

        dataSource.setUrl("jdbc:jtds:sqlserver://" + host_source_lucy + ":1433/" + db_source_lucy + "");

        dataSource.setUsername(this.usr_source_lucy);
        dataSource.setPassword(this.psw_source_lucy);

        return dataSource;
    }

    @Bean(name = "usuarioPermisoJDBC")
    public UsuarioPermisoJDBC usuarioPermisoJDBC() throws SQLException {
        UsuarioPermisoJDBC sol = new UsuarioPermisoJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisCon());

        return sol;
    }

    @Bean(name = "getDataSourceSisCon")
    public DriverManagerDataSource getDataSourceSisCon() {
        IpSql = this.DveloperServer + ":" + this.DeveloperPort;
        ddbbSql = "SysCon";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://" + host_source_syscon + ";databaseName=" + db_source_syscon);
        dataSource.setUsername(this.usr_source_siscon);
        dataSource.setPassword(this.psw_source_siscon);
        return dataSource;
    }

    @Bean(name = "operacionesJDBCTemplate")
    public OperacionesJDBC operacionesJDBCTemplate() throws SQLException {
        OperacionesJDBC opJdbc = new OperacionesJDBC();
        opJdbc.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return opJdbc;
    }

    @Bean(name = "detSolicitudJDBC")
    public DetSolicitudJDBC detSolicitudJDBC() throws SQLException {
        DetSolicitudJDBC detHit = new DetSolicitudJDBC();
        detHit.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return detHit;
    }

    @Bean(name = "solicitudJDBC")
    public SolicitudJDBC solicitudJDBC() throws SQLException {
        SolicitudJDBC sol = new SolicitudJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sol;
    }

    @Bean(name = "documentosJDBC")
    public DocumentoJDBC documentosJDBC() throws SQLException {
        DocumentoJDBC sol = new DocumentoJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sol;
    }

    @Bean(name = "loteJDBC")
    public LoteJDBC loteJDBC() throws SQLException {
        LoteJDBC sol = new LoteJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sol;
    }

    @Bean(name = "tipDctoJDBC")
    public TipDctoJDBC tipDctoJDBC() throws SQLException {
        TipDctoJDBC sol = new TipDctoJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sol;
    }

    @Bean(name = "estadoJDBC")
    public EstadoJDBC estadoJDBC() throws SQLException {
        EstadoJDBC est = new EstadoJDBC();
        est.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return est;
    }

    @Bean(name = "productosJDBC")
    public ProductosJDBC productosJDBC() throws SQLException {
        ProductosJDBC prod = new ProductosJDBC();
        prod.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return prod;
    }

    @Bean(name = "tipoDocumentoJDBC")
    public TipoDocumentoJDBC tipoDocumentoJDBC() throws SQLException {
        TipoDocumentoJDBC td = new TipoDocumentoJDBC();
        td.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return td;
    }

    /**
     * ProdTipoDocJDBC
     *
     * @return
     * @throws SQLException
     */
    @Bean(name = "prodTipoDocJDBC")
    public ProdTipoDocJDBC prodTipoDocJDBC() throws SQLException {
        ProdTipoDocJDBC ptd = new ProdTipoDocJDBC();
        ptd.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return ptd;
    }

    /**
     * UbicacionJDBC
     *
     * @return
     * @throws SQLException
     */
    @Bean(name = "ubicacionJDBC")
    public UbicacionJDBC ubicacionJDBC() throws SQLException {
        UbicacionJDBC ubi = new UbicacionJDBC();
        ubi.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return ubi;
    }

    @Bean(name = "usuariosLdapJDBC")
    public UsuariosLdapJDBC usuariosLdapJDBC() throws SQLException {
        UsuariosLdapJDBC usuLdap = new UsuariosLdapJDBC();
        usuLdap.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return usuLdap;
    }

    // MYSQL CONECTION
    @Bean(name = "datasourceProduccionMYSQL")
    public DataSource getDataSourceProduccionMYSQL() throws SQLException {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://192.168.0.14:3306/SisCon");
        dataSource.setUsername("root");
        dataSource.setPassword("password");
        return dataSource;
    }

    @Bean(name = "getDataSourceSisCore")
    public DriverManagerDataSource getDataSourceSisCore() {
        IpSql = this.DveloperServer + ":" + this.DeveloperPort;
        ddbbSql = "SysCon";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        dataSource.setUrl("jdbc:sqlserver://" + this.ipCertificacion + ":1433;databaseName=SysBoj");
        dataSource.setUsername("siscon");
        dataSource.setPassword("password");
        return dataSource;
    }

    @Bean(name = "tblHitosJDBCDEV")
    public TblHitosJDBC tblHitosJDBCDEV() throws SQLException {
        TblHitosJDBC tblh = new TblHitosJDBC();
        tblh.setDataSource(new MvcConfig().getDataSourceSisCore());
        return tblh;
    }

    @Bean(name = "tblHitosJDBC")
    public TblHitosJDBC tblHitosJDBC() throws SQLException {
        TblHitosJDBC tblh = new TblHitosJDBC();
        tblh.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return tblh;
    }

    @Bean(name = "dirLdapJDBC")
    public DirLdapJDBC dirLdapJDBC() throws SQLException {
        DirLdapJDBC dldap = new DirLdapJDBC();
        dldap.setDataSource(new MvcConfig().getDataSourceSisCore());
        return dldap;
    }

    @Bean(name = "jerarquiaSolJDBC")
    public JerarquiaSolJDBC jerarquiaSolJDBC() throws SQLException {
        JerarquiaSolJDBC sol = new JerarquiaSolJDBC();
        sol.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sol;
    }

    @Bean(name = "motivoDevolucionJDBC")
    public MotivoDevolucionJDBC motivoDevolucionJDBC() throws SQLException {
        MotivoDevolucionJDBC md = new MotivoDevolucionJDBC();
        md.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return md;
    }

    @Bean(name = "setDocumentoJDBC")
    public SetDocumentoJDBC setDocumentoJDBC() throws SQLException {
        SetDocumentoJDBC sd = new SetDocumentoJDBC();
        sd.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return sd;
    }

    @Bean(name = "devolucionJDBC")
    public DevolucionJDBC devolucionJDBC() throws SQLException {
        DevolucionJDBC dev = new DevolucionJDBC();
        dev.setDataSource(new MvcConfig().getDataSourceSisBoj());
        return dev;
    }
}
