/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.Messagebox;
import sisboj.entidades.DetLote;
import sisboj.entidades.DetSolicitud;
import sisboj.entidades.Documento;
import sisboj.entidades.Estado;
import sisboj.entidades.Lote;
import sisboj.entidades.Solicitud;
import sisboj.entidades.implementaciones.DetLoteJDBC;
import sisboj.entidades.implementaciones.DetSolicitudJDBC;
import sisboj.entidades.implementaciones.DocumentoJDBC;
import sisboj.entidades.implementaciones.EstadoJDBC;
import sisboj.entidades.implementaciones.GenericJDBC;
import sisboj.entidades.implementaciones.LoteJDBC;
import sisboj.entidades.implementaciones.SolicitudJDBC;


/**
 *
 * @author excosoc
 */
public class ProcesoLotes {

    //Variables
    private LoteJDBC SQLLote;
    private List<Lote> LLote;
    private Lote lote;
    private DetLoteJDBC SQLDetLote;
    private List<DetLote> LDetLote;
    private DetLote detLote;
    private EstadoJDBC SQLEstado;
    private Estado estadoEncabezado;
    private Estado estadoDetalle;
    private Solicitud solicitud;
    private List<Solicitud> lSolicitud;
    private SolicitudJDBC SQLSolicitud;
    private DetSolicitud detSolicitud;
    private List<DetSolicitud> lDetSolicitud;
    private DetSolicitudJDBC SQLDetSolicitud;
    private GenericJDBC SQLgenerico;
    private MetodosGenerales metodo;
     private DocumentoJDBC _conListDOc; 
     private List<Documento> _listDOc;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////Proceso/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Actualiza los lotes para asignarles el estado de en boj
    public boolean ProcesoBojResepcion(int di_IdSolicitud,String CodLote) throws SQLException {
        //Declaraciones
        boolean ok;
        int di_IdLote;
        String dv_CodBarra;
        int di_IdUbi;
        int di_fk_IdEstado;
        String ddt_FechaCreacion;
        String dv_GlosaLote;
        int di_CantDctos;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ok = false;
        //Crea instancias
        lSolicitud = new ArrayList<Solicitud>();
        detSolicitud = new DetSolicitud();
        lDetSolicitud = new ArrayList<DetSolicitud>();
        LLote = new ArrayList<Lote>();
        LDetLote = new ArrayList<DetLote>();
        solicitud = new Solicitud();
        metodo =new MetodosGenerales();
        
        SQLLote = new LoteJDBC();
        SQLEstado = new EstadoJDBC();
        SQLDetLote = new DetLoteJDBC();
        SQLDetSolicitud = new DetSolicitudJDBC();
        SQLSolicitud =  new SolicitudJDBC();
        SQLgenerico = new GenericJDBC();
        
        //crea jdbctemplate para separar las conexiones a SQL
        SQLLote.setJdbc();
        SQLEstado.setJdbc();
        SQLDetLote.setJdbc();
        SQLSolicitud.setJdbc();
        SQLDetSolicitud.setJdbc();
        SQLgenerico.setJdbc();
       _conListDOc= new MvcConfig().documentosJDBC();
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            lote= new Lote();
        
        try{
            //Obtenemos la Id de Estado para iniciar proceso
            estadoEncabezado = SQLEstado.getEstadoXNombre("Enviado", "Solicitud", "Custodia");

            //lo primero es resivir los Solicitudes
            if(di_IdSolicitud == 0){
                lSolicitud = SQLSolicitud.getSolicitudXEstado(estadoEncabezado.getDi_IdEstado()); //<--- 214 = lote despachado
            }else if (di_IdSolicitud != 0){
                lSolicitud.add(SQLSolicitud.getSolicitud(di_IdSolicitud)); //<--- busca la solicitud especifica
            }
            
            //Obtenemos la Id de Estado para el encabezado del lote
            estadoEncabezado = SQLEstado.getEstadoXNombre("En Transito", "Lote", "Back Office");
            //Obtenemos la Id de Estado para el detalle del lote
            estadoDetalle   = SQLEstado.getEstadoXNombre("En Transito", "Detalle Lote", "Back Office");
            
            for (Solicitud recorreSol : lSolicitud) {               
                
                
                int id  = recorreSol.getDi_IdSolicitud();
                int cantopers= SQLDetSolicitud.getCantOperDetSol(id);
                
                lote.setDi_CantOper(cantopers); //Cantidad de operaciones
                lote.setDv_CodBarra(CodLote);                                                                //Codigo de barra
                lote.setDi_IdUbi_Salida(recorreSol.getDi_fk_IdUbi());                                   //ubicación desde la que se despacha (custodia)
                lote.setDi_fk_IdEstado(estadoEncabezado.getDi_IdEstado());                              //estadoEncabezado del lote
                lote.setDdt_FechaCreacion(SQLgenerico.getDateNow());                                    //fecha de creacion del lote
                lote.setDv_GlosaLote("No Hay Glosa");                                                               //glosa del lote
                lote.setDi_CantDctos(recorreSol.getDi_CantDctosSolicitados());                          //cantidad de documentos para el lote
                
                //Inserta el lote y retorna la Id 
               int idLoreReturn= SQLLote.insertLoteCustodiaConIdSol(lote,di_IdSolicitud);
               if(idLoreReturn<0){
               Messagebox.show("Error al Insertar el LOTE");
               return false;
               }
                       lote.setDi_IdLote(idLoreReturn);
                
                //Genera Codigo de lote
                lote.setDv_CodLote(""); //se genera solo o se puede asignar
                
                //Obtenemos el detalle del Solicitud
                lDetSolicitud = SQLDetSolicitud.getDetSolicitudXSolicitud(recorreSol.getDi_IdSolicitud());
                
                detLote=new DetLote();
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //Recorremos el Detalle del Solicitud para mostrar en pantalla
                for (DetSolicitud recorreDetSol : lDetSolicitud) {
                    
                    detLote.setDi_fk_IdLote(lote.getDi_IdLote());               //Id del lote en generación
                    detLote.setDi_fk_IdDcto(recorreDetSol.getDi_fk_IdDcto());   //id del Dcto que se esta insertando
                    int idOperacion=recorreDetSol.getDi_fk_IdOper();
                    detLote.setDi_fk_IdOper(idOperacion);   //Id de la operacion relacionada al Dcto

                    
                    detLote.setDi_fk_IdDetSol(recorreDetSol.getDi_IdDetSol());  //Id del detella de solicitud del cual fue creado el lote
                    detLote.setDi_fk_IdEstado(estadoDetalle.getDi_IdEstado());  //estado (Despachado)
                    
                int respDetLote=    SQLDetLote.insert(detLote);
                    if(respDetLote>0){
                                        _listDOc=null;
                    _listDOc  = _conListDOc.listDocumento(idOperacion);
                     for (Documento cccc : _listDOc) {
                         int primaryKeyDocumento=cccc.getDi_IdDcto();
                     _conListDOc.ActualizaEstadoDocumento(primaryKeyDocumento, 160);
                    SQLDetLote.insertDetLoteDocument(respDetLote,primaryKeyDocumento )  ;                                                          // Insertar Relacion Documento DetLote
                     }
                     
                    }else {return false;}
                    
                    
                    
                }
            }
            
            ok = true;
        }catch(Exception ex){
            Messagebox.show("Sr(a) Usuario(a), ha ocurrido un error al cargar los lote de las custodias \n Error: " + ex.toString());
            ok = false;
        }
        return ok;
    }

}