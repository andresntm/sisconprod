
package org.zkoss.zkspringmvc.demo.mvvm;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author jumperchen
 */
@Controller ("mvvmIndexController")

@RequestMapping(value = {"/mvvm/*"})
//@SessionAttributes({"booksVM"})
public class IndexController {   
	@RequestMapping(value = {"index"}, method = RequestMethod.GET)
	public String index(ModelMap model) {

		return "Index/index.zul";
	}
	/*@RequestMapping(value = {"bootstrap.min.css","WEB-INF/Index/bootstrap.min.css","/WEB-INF/Index/bootstrap.min.css"}, method = RequestMethod.GET)
	public String css(ModelMap model) {

		return "Index/bootstrap.min.css";
	}
*/

}
