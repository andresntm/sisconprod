package RedNeuronal;




/**

 *

 * @author esilvestre

 */

public class Neuron1 {

    double activacionu;

    double [] entradas;

    double [] pesos;

    double errorNodo;

    

    static Sigmoide sigmoide;

    public Neuron1(int nentradas){

    pesos=new double[nentradas];

    entradas=new double[nentradas];

    sigmoide=new Sigmoide();

    errorNodo=0;

    }

    

    

    public void activacion(){

    double suma=0;

    

    for(int i=0;i<entradas.length;i++){

    suma+=entradas[i]*pesos[i];

    

    }

    activacionu=sigmoide.function(suma);

    

    

    }

    

    public void verEntradas(){

    System.out.println("*****Entradas**********");

    for(int i=0;i<entradas.length;i++){

        

    System.out.println(entradas[i]);

    }

    

    }

    

        public void verPesos(){

    System.out.println("*****Pesos**********");

        for(int i=0;i<pesos.length;i++){

        

    System.out.println(pesos[i]);

    }

    

    }

        

        

        

        

        

    public double    getActivacion(){

    

    return (activacionu);

    

    } 

    

    

   public void verActivacion(){

   System.out.println("*****Activacion**********");

   System.out.println(""+activacionu);

   }

   

   public double getError(){

   

   return(errorNodo);

   

   }

   

   public void setError(double error){

   errorNodo=error;

   }

}