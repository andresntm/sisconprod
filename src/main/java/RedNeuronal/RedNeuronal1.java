/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RedNeuronal;

/**
 *
 * @author esilves
 */
public class RedNeuronal1 {
    int nodosCapaK;
    int entCapak;
    int nodosCapaJ;
    int entCapaj;
    
    
    double L=0.5;//factor aprendizaje
    double [] entradas;
    Neuron1 [] neuronK,neuronJ;
    static double balance=-1;
     
    
    public RedNeuronal1(int nodosCapaK,int nodosCapaJ,int entCapaK,int entCapaj){
    this.nodosCapaK=nodosCapaK;
    this.nodosCapaJ=nodosCapaJ;
    this.entCapak=entCapaK;
    this.entCapaj=entCapaj;
    
    }
    
    public void crearRed(){
    
        
        // se crea la primera capa
    neuronK=new Neuron1[nodosCapaK];
    for(int k=0;k<nodosCapaK;k++){
    neuronK[k]=new Neuron1(entCapak);
    }
    
    
    // se crea la segunda capa
    
            // se crea la primera capa
    neuronJ=new Neuron1[nodosCapaJ];
    for(int j=0;j<nodosCapaJ;j++){
    neuronJ[j]=new Neuron1(entCapaj);
    }
    
    
    }
  
    
    public void inicialiaPesos(){
    
    for(int k=0;k<nodosCapaK;k++){
    
            for(int i=0;i<entCapak;i++){
                   neuronK[k].pesos[i]=Math.random();

            }
    
    }
    
    
        for(int k=0;k<nodosCapaJ;k++){
    
            for(int i=0;i<entCapaj;i++){
                   neuronK[k].pesos[i]=Math.random();

            }
    
    }
    
    
    }//inicialiaPesos
    
    
    public void entradasJ(double capaI[]){
    
    for(int j=0;j<nodosCapaJ;j++){
        for(int i=0;i<capaI.length;i++){
            
            neuronJ[j].entradas[i] = capaI[i];
        
        
        }
    
    }
    
    }//entradasJ()
   
 
   public void activacionJ(){
    for(int j=0 ;j < nodosCapaJ;j++){
    
    neuronJ[j].activacion();
    
    
    }
    
    
    }//activacionJ()
    
    
        public void entradasK(){
int j;
    for(int k=0;k<nodosCapaK;k++){
        for( j=0;j<nodosCapaJ;j++){
            
            neuronK[k].entradas[j] = neuronJ[j].getActivacion();
        
        
        }
    neuronK[k].entradas[j]=balance;
    }
    
    }//entradasK()

    
    
       public void activacionK(){
    for(int k=0 ;k < nodosCapaK;k++){
    
    neuronK[k].activacion();
    
    
    }
    
    
    }//activacionK()
       
   
       
       
       public void errorCapaK(double apreder[][],int epoca){
       
           double errorl=0;
           for(int k=0;k<nodosCapaK;k++){
           errorl=(apreder[k][epoca]-neuronK[k].getActivacion()) * neuronK[k].getActivacion()*(1-neuronK[k].getActivacion());
           neuronK[k].setError(errorl);
           }
       
       
       }//errorCapaK(double apreder[][],int epoca)
       
       
       
       public void pesos_JK(){
       
       for(int k=0;k<nodosCapaK;k++){
       for(int i =0; i<entCapak;i++){
       
           neuronK[k].pesos[i] += (L*neuronK[k].getError()*neuronK[k].entradas[i]);
       
       
       }
       
       }
       
       }///pesos_JK
       
       
       
       
       public void errorCapaJ(){
       
       double suma;double errj;
       
       
       for(int j=0;j<nodosCapaJ;j++){
       
       suma=0;
       for(int k=0;k<nodosCapaK;k++){
       
       suma +=(neuronK[k].getError()*neuronK[k].pesos[j]);
            
       }
       
       errj=(neuronJ[j].getActivacion()*(1-neuronJ[j].getActivacion())*suma);
       neuronJ[j].setError(errj);
       }
       
       
       
       }////errorCapaJ
       
       
       
       
       public void pesos_IJ(){
       
       for(int j=0;j<nodosCapaJ;j++){
       for(int i=0;i<entCapaj;j++){
       neuronJ[j].pesos[i] += L*neuronJ[j].getError()*neuronJ[j].entradas[i];
       
       }
       
       
       }
       
       }/////pesos_IJ
       
   
      public double getEerrorK(){
      double sumae=0;
      for(int j=0;j<nodosCapaK;j++){
      
      sumae+=neuronK[j].getError();
      
      }
      return(Math.abs(sumae));
      
      } ////getEerrorK
       
    
      
      
      public double getActivacionK(int neuron){
      if(neuronK[neuron].getActivacion()>=0.5)
          return(1);
      else return(0);
      
      
      }//////getActivacionK
      
      
      
      public void verPesos_IJ(){
      
      
      
      
      }
      
      
      
      
      
     
}
