USE [SysBoj]
GO
/****** Object:  StoredProcedure [dbo].[SP_INHIBIDOR_HO]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INHIBIDOR_HO] (@RUT NVARCHAR(max))
AS

declare @ruts nvarchar(max)
set @ruts = @rut


BEGIN

	IF @RUT <> ''
	 select   @RUTs  as CONTENIDO


	Select
CASE WHEN pcjbci_prod..sd_estadosDocumentos.ed_estadoDocumento  = 'Ingresado' THEN 'NO' 
     WHEN pcjbci_prod..sd_estadosDocumentos.ed_estadoDocumento  = 'SALIDA' THEN 'SI'
	 WHEN pcjbci_prod..sd_estadosDocumentos.ed_estadoDocumento  = ' ' THEN 'SI'
	 WHEN pcjbci_prod..sd_estadosDocumentos.ed_estadoDocumento  is null THEN 'SI'
	 END AS 'PUEDO INHIBIR x doc_trans?' ,
 
pcjbci_prod..sd_documentos.do_codigointerno 'Código Único', 
pcjbci_prod..tg_custodias.cu_custodia 'Custodia Origen', 
replace(convert(varchar(10),pcjbci_prod..sd_documentos.do_fechaingreso, 103),'/','-') 'Fecha Ingreso uapc',
pcjbci_prod..sd_documentos.cl_rut,
pcjbci_prod..sd_documentos.cl_cliente 'Nombre Deudor', 
pcjbci_prod..sd_identifSola7.is_identifSola7 'IdentifSola7', 
pcjbci_prod..op_operaciones.op_codoperacion 'Operación original', 
pcjbci_prod..sd_documentos.do_folioDoc 'Folio doc.', 
pcjbci_prod..sd_estadosDocumentos.ed_estadoDocumento 'Estado documento', 
pcjbci_prod..sd_documentos.do_descrip 'Descripción', 
responsable.pe_nombrecompleto 'Responsable',
sd_DocumentosEstadosAsignaciones.dea_documentoestadoasignacion 'Estado asignación', 
op_monto 'Monto Op.'
FROM pcjbci_prod..or_subbancas 
RIGHT OUTER JOIN pcjbci_prod..tg_personas AS responsable 
RIGHT OUTER JOIN pcjbci_prod..sd_DocumentosEstadosAsignaciones 
RIGHT OUTER JOIN pcjbci_prod..sd_documentos ON sd_DocumentosEstadosAsignaciones.id_documentoestadoasignacion = sd_documentos.id_documentoestadoasignacion 
ON responsable.id_persona = sd_documentos.id_Responsable 
LEFT OUTER JOIN pcjbci_prod..op_operaciones ON sd_documentos.id_operacion = op_operaciones.id_operacion 
LEFT OUTER JOIN pcjbci_prod..tg_personas ON sd_documentos.id_persCliente = tg_personas.id_persona 
LEFT OUTER JOIN pcjbci_prod..tg_custodias ON sd_documentos.id_custodia = tg_custodias.id_custodia 
LEFT OUTER JOIN pcjbci_prod..or_oficinas ON sd_documentos.id_oficina = or_oficinas.id_oficina 
LEFT OUTER JOIN pcjbci_prod..sd_tiposdearchivos 
INNER JOIN pcjbci_prod..sd_loteos ON sd_tiposdearchivos.id_tipodearchivo = sd_loteos.id_tipodearchivo 
INNER JOIN pcjbci_prod..or_organizacion ON sd_tiposdearchivos.id_organizacion = or_organizacion.id_organizacion 
ON sd_documentos.id_loteo = sd_loteos.id_loteo 
LEFT OUTER JOIN pcjbci_prod..tg_custodias AS tg_custodias_1 ON sd_documentos.id_custodiaConuser = tg_custodias_1.id_custodia 
LEFT OUTER JOIN pcjbci_prod..sd_tiposmov ON sd_documentos.id_tipomov = sd_tiposmov.id_tipomov 
LEFT OUTER JOIN pcjbci_prod..ub_regiones ON sd_documentos.id_region = ub_regiones.id_region 
LEFT OUTER JOIN pcjbci_prod..sd_productos ON sd_documentos.id_producto = sd_productos.id_producto 
LEFT OUTER JOIN pcjbci_prod..sd_identifSola7 ON sd_documentos.id_identifSola7 = sd_identifSola7.id_identifSola7 
ON or_subbancas.id_subbanca = sd_documentos.id_subbanca 
LEFT OUTER JOIN pcjbci_prod..sd_estadosDocumentos ON sd_documentos.id_estadoDocumento = sd_estadosDocumentos.id_estadoDocumento 
LEFT OUTER JOIN pcjbci_prod..sd_tiposSalidas ON sd_documentos.id_tipoSalida = sd_tiposSalidas.id_tipoSalida 
LEFT OUTER JOIN pcjbci_prod..sd_motivo ON sd_documentos.id_motivo = sd_motivo.id_motivo 
where pcjbci_prod..sd_documentos.cl_rut = @RUTs

select 
CASE WHEN JU.AB_ABOGADO =' ' then 'SI'
     WHEN JU.AB_ABOGADO <>''  and OPSA.os_fechasalida is NOT null then 'SI'
	 WHEN JU.AB_ABOGADO ='sin abogado' then 'SI' 
	 WHEN JU.AB_ABOGADO  is null then 'SI'
	 when JU.AB_ABOGADO <> '' THEN 'NO' 

	 END AS 'PUEDO INHIBIR?',os_fechasalida,ju_fechaasig,JU.AB_ABOGADO,
	 op.op_codoperacion,*
 from pcjbci_prod..op_operaciones as op
 left join  PCJBCI_PROD..ju_juicios AS JU 
 on ltrim(rtrim(op.cl_rut)) = ltrim(rtrim(ju.cl_rut))
 left  join PCJBCI_PROD..op_operacionessalidas as opsa
 on op.id_operacion = opsa.id_operacion
 where ltrim(rtrim(op.cl_rut )) = @RUTs

select
ie.ine_inhibicionestado,
i.in_fechadesde,
i.in_fechahasta,
im.im_inhibicionmotivo,
p.pe_rut,
p.pe_nombrecompleto,

case when id_perssolicita = 825625 then 'HUMBERTO'
     when id_perssolicita = 286710 then 'CRISTIAN'
end AS INHIBIDOR,
*
from pcjbci_prod..sd_inhibidos as i
inner join pcjbci_prod..tg_personas as p
on i.id_perscliente = p.id_persona 
inner join pcjbci_prod..sd_inhibicionesestados as ie
on ie.id_inhibicionestado = i.id_inhibicionestado
inner join pcjbci_prod..sd_inhibicionesmotivos as im
on im.id_inhibicionmotivo = i.id_inhibicionmotivo
where p.pe_rut = @RUTs

END

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_ACTUALIZA_INTER_ENTRADA]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_SISBOJ_ACTUALIZA_INTER_ENTRADA]
AS
     DECLARE @RUT INT= 0, @DV CHAR(1)= '';
     BEGIN
         DECLARE CLIENTE CURSOR
         FOR
             SELECT TSIE.ORIG_RUT,
                    TSIE.ORIG_DV
             FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  LEFT OUTER JOIN DBO.TB_SYS_PERSONA TSP ON TSIE.ORIG_RUT = TSP.DI_RUT
                                                            AND TSIE.ORIG_DV = TSP.DC_DIGVER
             WHERE TSP.DI_RUT IS NULL;
         OPEN CLIENTE;
         FETCH NEXT FROM CLIENTE INTO @RUT, @DV;
         WHILE(@@FETCH_STATUS = 0)
             BEGIN
                 EXEC SP_SISBOJ_ADDCLIENTENEW
                      @RUT = @RUT,
                      @DIGVER = @DV;
                 FETCH NEXT FROM CLIENTE INTO @RUT, @DV;
             END;
         IF(CURSOR_STATUS('GLOBAL', 'CLIENTE') >= -1)
             BEGIN
                 CLOSE CLIENTE;
                 DEALLOCATE CLIENTE;
         END;

	    -------------------------------------------------

         BEGIN
             UPDATE DBO.TB_SYS_INTER_ENTRADA
               SET
                   DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE,
                   DI_FK_IDPRODUC = PRO.DI_IDPRODUC
             FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN TB_SYS_PERSONA PER ON PER.DI_RUT = TSIE.ORIG_RUT
                                                   AND PER.DC_DIGVER = TSIE.ORIG_DV
                  INNER JOIN TB_SYS_CLIENTE CLI ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA
                  INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DV_NOMPRODUC = TSIE.ORIG_PRODUCTO;
         END;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_ACUALIZA_TIOAUX]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_ACUALIZA_TIOAUX]
AS
     BEGIN
	
     /* 
	   SE ANALIZA PRODUCCION PARA SABER SI SE A CREADO ALGUN NUEVO TIOAUX (NOVA,BCI), 
	   SI EXISTE UNO NUEVO LO INGRESA A LA TABLA
	*/

         BEGIN TRY
             INSERT INTO TB_SYS_TIPOOPERACION
             (dv_IdTipOpe,
              dv_DetTipOperBOJ
             )
                    SELECT CONVERT(VARCHAR, RTMSP.SUBPROD_ID) CODIGO,
                           SUBPROD_NOMBRE NOMBRE
                    FROM IN_CBZA..RIESGO_TABMAE_SUBPRODUCTO RTMSP
                         LEFT JOIN TB_SYS_TIPOOPERACION TSTO ON TSTO.DV_IDTIPOPE = RTMSP.SUBPROD_ID
                    WHERE TSTO.DV_IDTIPOPE IS NULL
                    UNION ALL
                    SELECT CONVERT(VARCHAR, RTMP.PRODUCTO_ID),
                           RTMP.NOMBRE_PRODUCTO
                    FROM IN_CBZA..RIESGONOVA_TABMAE_PRODUCTOS RTMP
                         LEFT JOIN TB_SYS_TIPOOPERACION TSTO ON TSTO.DV_IDTIPOPE = CONVERT(VARCHAR, RTMP.PRODUCTO_ID)
                    WHERE TSTO.DV_IDTIPOPE IS NULL;
         END TRY
         BEGIN CATCH
	
	   --ERROR AL CREAR TIOAUX EN BOJ
/* 
		SELECT
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage
	*/


         END CATCH;
     END;





GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_ADDCLIENTENEW]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


creaTE PROCEDURE [dbo].[SP_SISBOJ_ADDCLIENTENEW]
(@RUT    INT,
 @DIGVER CHAR
)
AS
     BEGIN
	
	--SI NO EXISTE COMO PERSONA LA AGREGA
         IF(NOT EXISTS
           (
               SELECT DI_IDPERSONA
               FROM TB_SYS_PERSONA
               WHERE DI_RUT = @RUT
                     AND DC_DIGVER = @DIGVER
           ))
             BEGIN
                 EXEC SP_SISBOJ_ADDPERSONANEW
                      @DI_RUT = @RUT,
                      @DC_DIGVER = @DIGVER;
         END;
             ELSE
         IF(NOT EXISTS
           (
               SELECT DI_IDPERSONA
               FROM TB_SYS_PERSONA
               WHERE DI_RUT = @RUT
           ))
             BEGIN
                 EXEC SP_SISBOJ_ADDPERSONANEW
                      @DI_RUT = @RUT,
                      @DC_DIGVER = @DIGVER;
         END;

	--SI EL CLIENTE NO EXISTE EN LA TABLA CLIENTES
         IF(NOT EXISTS
           (
               SELECT DI_IDCLIENTE
               FROM TB_SYS_PERSONA PER
                    INNER JOIN TB_SYS_CLIENTE CLI ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA
               WHERE PER.DI_RUT = @RUT
                     AND PER.DC_DIGVER = @DIGVER
           ))
             BEGIN

		--SI NO EXISTE LO CREA
                 INSERT INTO TB_SYS_CLIENTE(DI_FK_IDPERSONA,DI_FK_RUT)
                        SELECT DI_IDPERSONA,DI_RUT
                        FROM TB_SYS_PERSONA
                        WHERE DI_RUT = @RUT
                              AND DC_DIGVER = @DIGVER;
         END;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_ADDPERSONANEW]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[SP_SISBOJ_ADDPERSONANEW]
(@DI_RUT    INT  = 0,
 @DC_DIGVER CHAR = ''
)
AS
     DECLARE @RETORNO VARCHAR(150);
     BEGIN
	---------------------------
	--BUSCAMOS PERSONAS NUEVAS-
	---------------------------
         BEGIN TRY
		--DROP TABLE #PERSONANUEVA
             IF((@DI_RUT IS NOT NULL)
                AND (@DC_DIGVER IS NOT NULL))
                 BEGIN
                     IF EXISTS
                     (
                         SELECT DI_IDPERSONA
                         FROM TB_SYS_PERSONA
                         WHERE DI_RUT = @DI_RUT
                               AND DC_DIGVER = @DC_DIGVER
                     )
                         BEGIN
                             --SET @RETORNO = 'LA PERSONA YA EXISTE.';
                             RETURN;
                     END;
                         ELSE
                     IF EXISTS
                     (
                         SELECT DI_IDPERSONA
                         FROM TB_SYS_PERSONA
                         WHERE DI_RUT = @DI_RUT
                     )
                         BEGIN
                             RETURN;
                     END;
                         ELSE
                         BEGIN
                             INSERT INTO TB_SYS_PERSONA
                             (DI_RUT,
                              DC_DIGVER,
                              DV_NOMBRE,
                              DV_APEPATER,
                              DV_APEMATER,
                              DDT_FECHNAC,
                              DC_SEXO,
                              DC_TIPOPER,
                              DV_EMAIL
                             )
                             SELECT RUT,
                                    DV,
                                    IIF((NOMBRES IS NULL)
                                        OR (NOMBRES = ''), IIF((RAZ_SOC = '')
                                                               OR (RAZ_SOC IS NULL), IIF((APEPAT = '')
                                                                                         OR (APEPAT IS NULL), 'SIN NOMBRE', LTRIM(RTRIM(UPPER(APEPAT)))), LTRIM(RTRIM(UPPER(RAZ_SOC)))), LTRIM(RTRIM(UPPER(NOMBRES)))) NOMBRES,
                                    IIF((APEPAT IS NULL)
                                        OR (APEPAT = ''), NULL, IIF((NOMBRES IS NULL)
                                                                    OR (NOMBRES = ''), NULL, APEPAT)) APEPAT,
                                    IIF((APEMAT IS NULL)
                                        OR (APEMAT = ''), NULL, IIF((NOMBRES IS NULL)
                                                                    OR (NOMBRES = ''), NULL, APEMAT)) APEMAT,
                                    FECNAC,
                                    SEXO,
                                    INDTIPOPER,
                                    EMAIL
                             FROM IN_CBZA..IN_DBC DBC
                             WHERE RUT = @DI_RUT
                                   AND DV = @DC_DIGVER;
                             --SET @RETORNO = 'LA PERSONA CON RUT: '+CONVERT(VARCHAR, @DI_RUT)+'-'+CONVERT(VARCHAR, @DC_DIGVER)+' FUE CREADO CON EXITO.';
                     END;
             END
		--SI SE EJECUTA SIN PARAMETROS SE GENERAN TODAS LAS PERSONAS QUE FALTAN;
                 ELSE
                 BEGIN
                     INSERT INTO TB_SYS_PERSONA
                     (DI_RUT,
                      DC_DIGVER,
                      DV_NOMBRE,
                      DV_APEPATER,
                      DV_APEMATER,
                      DDT_FECHNAC,
                      DC_SEXO,
                      DC_TIPOPER,
                      DV_EMAIL
                     )
                            SELECT RUT,
                                   DV,
                                   NOMBRES,
                                   APEPAT,
                                   APEMAT,
                                   FECNAC,
                                   SEXO,
                                   INDTIPOPER,
                                   EMAIL
                            FROM IN_CBZA..IN_DBC DBC
                            WHERE(DBC.NOMBRES IS NOT NULL
                                  AND LTRIM(DBC.NOMBRES) <> '');
                     --SET @RETORNO = 'PERSONAS CREADAS.';
             END;
         END TRY
         BEGIN CATCH
             RETURN;
             --SET @RETORNO = 'ERROR AL EJECUTAR EL SP ERROR N°'+CONVERT(VARCHAR, ERROR_NUMBER())+' '+CONVERT(VARCHAR, ERROR_MESSAGE())+'.';
         END CATCH;
         --SELECT @RETORNO;
     END;



GO
/****** Object:  StoredProcedure [dbo].[sp_sisboj_band_custodia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_sisboj_band_custodia]
AS



SELECT  hito.OP_ORIG,hito.OPERACION_2,his_10.dv_CodOperacion,his_10.dv_CodOperRelacionada from ( select *  FROM in_cbza.dbo.tbl_hito_jud_bci_historico where fecha_ejecucion in(select max(fecha_ejecucion)  FROM in_cbza.dbo.tbl_hito_jud_bci_historico)  ) hito 

inner  join (select distinct * from  [dbo].[tb_Sys_Inter_Entrada_hist] where ddt_fecIngreso=convert(datetime,'2019-07-17 10:30:53.880',101)) his_10 on his_10.dv_CodOperRelacionada=hito.OPERACION_2



--- Problema a Resolver ---


--HITO 1   SALIDA     ---HITO2         ---- HITO 3
-- x1                     x1
-- x2-------x2
-- x3                     x3
-- x4-------x4
-- x5                     x5
--                        x7 
--                        x8
--                        x9
----------------------------------------
--resumen       
--       x2,x4       salieron por pago
--	   x1,x3,x5    permaneces en el HITO
--	   x7,x8,x9    Son operaciones nuevas en hito





select convert(datetime,'2019-07-17 10:30:53.880',101)

    -- END;

GO
/****** Object:  StoredProcedure [dbo].[sp_sisboj_band_custodia_v2]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_sisboj_band_custodia_v2]
AS




--- Problema a Resolver ---


--HITO 1   SALIDA     ---HITO2         ---- HITO 3
-- x1                     x1
-- x2-------x2
-- x3                     x3
-- x4-------x4
-- x5                     x5
--                        x7 
--                        x8
--                        x9
----------------------------------------
--resumen       
--       x2,x4       salieron por pago
--	   x1,x3,x5    permaneces en el HITO
--	   x7,x8,x9    Son operaciones nuevas en hito
--delete from nombretabla
delete from [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones] ;  


declare @ultimoHito varchar(15);
declare @ultimoHitoBoj varchar(15);
declare @PenultimoHitoBoj varchar(15);
select @ultimoHitoBoj=CONVERT(date,max (fecha_ejecucion)) FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]

select @ultimoHito=max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	where tabla='tbl_hito_jud_bci_historico'

select @PenultimoHitoBoj=CONVERT(date,max (fecha_ejecucion)) FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico] where CONVERT(date,fecha_ejecucion) < @ultimoHito
--select CONVERT(date,max (fecha_ejecucion))'MyHito' FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]

--select max(fecha_solicitud)'Produccion'	from in_cbza.dbo.vw_operaciones_hito	where tabla='tbl_hito_jud_bci_historico'
select @ultimoHito '@ultimoHito',@ultimoHitoBoj '@ultimoHitoBoj', @PenultimoHitoBoj '@PenultimoHitoBoj'

INSERT INTO [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones]  ([operaciones],[estado],[registrado]) 


--/// pendiente

select PEN.operacion, 'PENDIENTE' 'estado',getdate()  'registrado'  from (
select * from (select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF
inner join  ( select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@PenultimoHitoBoj) AS FF2 on  FF.operacion=FF2.OP_ORIG
) as PEN
 UNION 
--Salida del Hito 
select SALI.OP_ORIG, 'CON_SALIDA' 'estado',getdate()  'registrado'  from (
select * from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@PenultimoHitoBoj
 ) AS FF2 
 LEFT join  (select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion=FF2.OP_ORIG)
 where FF.operacion is null
 ) as SALI
  UNION 
   --Nuevo EN el HITO 
 select NUEVO.operacion, 'NUEVO' 'estado',getdate() 'registrado' from (
select * from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@PenultimoHitoBoj
 ) AS FF2 
 RIGHT join  (
 select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion = FF2.OP_ORIG)
 where FF2.OP_ORIG is null
  ) as NUEVO




GO
/****** Object:  StoredProcedure [dbo].[sp_sisboj_band_custodia_v3]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_sisboj_band_custodia_v3]
AS




--- Problema a Resolver ---


--HITO 1   SALIDA     ---HITO2         ---- HITO 3
-- x1                     x1
-- x2-------x2
-- x3                     x3
-- x4-------x4
-- x5                     x5
--                        x7 
--                        x8
--                        x9
----------------------------------------
--resumen       
--       x2,x4       salieron por pago
--	   x1,x3,x5    permaneces en el HITO
--	   x7,x8,x9    Son operaciones nuevas en hito
declare @ultimoHito varchar(15);
declare @ultimoHitoBoj varchar(15);

select @ultimoHitoBoj=CONVERT(date,max (fecha_ejecucion)) FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]

select @ultimoHito=max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	where tabla='tbl_hito_jud_bci_historico'

--select max(fecha_solicitud) from in_cbza.dbo.vw_operaciones_hito  where tabla='tbl_hito_jud_bci_historico'


--select max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	
--select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico'



--/// pendiente
select PEN.operacion, 'PENDIENTE' 'estado' from (
select * from (select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF
inner join  ( select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj) AS FF2 on  FF.operacion=FF2.OP_ORIG
) as PEN

 UNION ALL
--Salida del Hito 
select SALI.operacion, 'CON_SALIDA' 'estado' from (
select * from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj
 ) AS FF2 
 LEFT join  (select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion=FF2.OP_ORIG)
 where FF.operacion is null
 ) as SALI

 UNION ALL


 --Nuevo EN el HITO 
 select NUEVO.operacion, 'NUEVO' 'estado' from (
select * from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj
 ) AS FF2 
 RIGHT join  (
 select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion != FF2.OP_ORIG)
 where FF2.OP_ORIG is null
  ) as NUEVO
--select * from in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud='2019-08-12' and tabla = 'tbl_hito_jud_bci_historico'



--select max(fecha_solicitud) from in_cbza.dbo.vw_operaciones_hito  where tabla='tbl_hito_jud_bci_historico'-- and fecha_solicitud='2019-08-13'
--select max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	

--select convert(datetime,'2019-07-17 10:30:53.880',101)

    -- END;




GO
/****** Object:  StoredProcedure [dbo].[sp_sisboj_band_custodia_v4]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_sisboj_band_custodia_v4]
AS




--- Problema a Resolver ---


--HITO 1   SALIDA     ---HITO2         ---- HITO 3
-- x1                     x1
-- x2-------x2
-- x3                     x3
-- x4-------x4
-- x5                     x5
--                        x7 
--                        x8
--                        x9
----------------------------------------
--resumen       
--       x2,x4       salieron por pago
--	   x1,x3,x5    permaneces en el HITO
--	   x7,x8,x9    Son operaciones nuevas en hito
declare @ultimoHito varchar(15);
declare @ultimoHitoBoj varchar(15);

select @ultimoHitoBoj=CONVERT(date,max (fecha_ejecucion)) FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]

select @ultimoHito=max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	where tabla='tbl_hito_jud_bci_historico'

--select max(fecha_solicitud) from in_cbza.dbo.vw_operaciones_hito  where tabla='tbl_hito_jud_bci_historico'


--select max(fecha_solicitud)	from in_cbza.dbo.vw_operaciones_hito	
--select operacion from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico'



--/// pendiente
select PEN.* from (
select FF.* from (select * from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF
inner join  ( select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj) AS FF2 on  FF.operacion=FF2.OP_ORIG
) as PEN

 UNION ALL
--Salida del Hito 
select SALI.* from (
select FF.* from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj
 ) AS FF2 
 LEFT join  (select * from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion=FF2.OP_ORIG)
 where FF.operacion is null
 ) as SALI

 UNION ALL
  --Nuevo EN el HITO 
 select NUEVO.* from (
select FF.* from (
 select OP_ORIG FROM [SysBoj].[dbo].[sisboj_tbl_hito_jud_bci_historico]  where  CONVERT(date,(fecha_ejecucion))  =@ultimoHitoBoj
 ) AS FF2 
 RIGHT join  (
 select * from   in_cbza.dbo.vw_operaciones_hito	where fecha_solicitud=@ultimoHito and tabla = 'tbl_hito_jud_bci_historico')  as FF on (FF.operacion != FF2.OP_ORIG)
 where FF2.OP_ORIG is null
  ) as NUEVO


GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_CARGA_HITO]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_CARGA_HITO]
AS
     DECLARE @IDPRODUCTO INT, @IDHITO INT;
     BEGIN

		  --VALIDAMOS QUE EL HITO NO ESTE INGRESADO YA
         DECLARE OPER CURSOR
         FOR
             SELECT DISTINCT
                    (TSIE.DI_FK_IDPRODUC)
             FROM TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
             ORDER BY TSIE.DI_FK_IDPRODUC;
         OPEN OPER;

		--RECOREMOS EL CURSOR PARA GENERAR LA DATA EN EL HITO
         FETCH NEXT FROM OPER INTO @IDPRODUCTO;
         WHILE(@@FETCH_STATUS = 0)
             BEGIN
                 INSERT INTO [DBO].[TB_SYS_HITO]
                 (DI_FK_IDTIPOHITO,
                  DV_CODHITO,
                  DI_FK_IDESTADO
                 )
                 VALUES
                 (2,
                  NULL,
                  110
                 );

			  --------------------------------
                 SET @IDHITO = @@IDENTITY;
			  --------------------------------
                 INSERT INTO TB_SYS_DETHITO
                 ([DI_FK_IDPRODUC],
                  [DI_IDHITO],
                  [DI_FK_IDOPE],
                  [DI_FK_IDUBI],
                  [DI_PRIORIDAD],
                  [DV_SOLICITUDORIG],
                  [DI_FK_IDESTADO]
                 )
                        SELECT TSP.DI_IDPRODUC,
                               @IDHITO,
                               TSO.DI_IDOPERACION,
                               TSU.DI_IDUBICACION,
                               TSIE.DI_PRIORIDAD,
                               TSIE.DV_SOLICITUDORIG,
                               IIF(TSDH.DI_FK_IDOPE IS NULL, 120, 121)
                        FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                             INNER JOIN DBO.TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
                             INNER JOIN TB_SYS_UBICACION TSU ON TSU.DV_DETUBI = CASE
                                                                                    WHEN(TSIE.ORIG_UBICACION = '')
                                                                                    THEN 'SIN UBICACION'
                                                                                    ELSE TSIE.ORIG_UBICACION
                                                                                END
                             INNER JOIN TB_SYS_PRODUCTO TSP ON TSP.DI_IDPRODUC = TSO.DI_FK_IDPRODUC
                             LEFT OUTER JOIN TB_SYS_DETHITO TSDH ON TSO.DI_IDOPERACION = TSDH.DI_FK_IDOPE
                        WHERE TSIE.DI_FK_IDPRODUC = @IDPRODUCTO
                              AND TSO.BIT_NUEVO = 1;
                 FETCH NEXT FROM oper INTO @IDPRODUCTO;
             END;
         IF(CURSOR_STATUS('GLOBAL', 'OPER') >= -1)
             BEGIN
                 CLOSE OPER;
                 DEALLOCATE OPER;
         END;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_CARGA_INICIAL]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_CARGA_INICIAL]
AS
     BEGIN
         BEGIN TRY
	   
		  /*CARGA DATA DE PRODUCCION A INTERFAZ*/

             EXEC SP_SISBOJ_HITO_COM_CON_NOVA_BCI;
	   
		  /*ANALIZA SI CLIENTE EXISTE, DE NO EXISTIR LO CREA
		  ACTUALIZA INFORMACIÓN ADICIONANDO DATOS EXTRAS DE BOJ*/

             EXEC SP_SISBOJ_ACTUALIZA_INTER_ENTRADA;
         END TRY
         BEGIN CATCH
	   	   	   		 SELECT 'ERROR AL CARGAR LA INTERFAZ';
                     RETURN; 
		  /* ERROR AL CARGAR LA INTERFAZ*/

         END CATCH;
	    -----------------------------------------
	    -----------------------------------------
         BEGIN TRY
	   
		  /*RESPALDA OPERACIONES A ACTUALIZAR*/

             EXEC SP_SISBOJ_HIST_OPER_UPT;

             UPDATE DBO.TB_SYS_OPERACION
               SET
                   [DI_FK_IDCLIENTE] = TSIE.DI_FK_IDCLIENTE,
                   [DI_FK_IDPRODUC] = TSIE.DI_FK_IDPRODUC,
                   [DI_IDESTADO] = 101,
                   [DI_DIASMORA] = TSIE.DI_DIASMORA,
                   [DM_SALDOINSOLUTO] = TSIE.DM_SALDOINSOLUTO,
                   [DF_SALDOINSOUF] = TSIE.DF_SALDOINSOUF,
                   [DV_TIPOMONEDA] = TSIE.DV_TIPOMONEDA,
                   [DV_FK_IDTIPOPE] = TSIE.DV_FK_IDTIPOPE,
                   [DI_FK_IDOFICINA] = TSIE.DI_FK_IDOFICINA,
                   [DI_FK_IDCEDENTE] = TSIE.DI_FK_IDCEDENTE,
                   [DV_FK_TIPREGION] = TSIE.DV_FK_TIPREGION,
                   [DDT_FECHVENCACT] = TSIE.DDT_FECHVENCACT,
                   [DDT_FECHCURSE] = TSIE.DDT_FECHCURSE,
                   [DDT_FECHULTIMOPAGO] = TSIE.DDT_FECHULTIMOPAGO,
                   [DV_FK_IDBANCA] = TSIE.DV_FK_IDBANCA,
                   [DV_APODOBANCA] = TSIE.DV_APODOBANCA,
                   [DV_GARANTIA] = TSIE.DV_GARANTIA,
                   [DV_ORIGEN] = TSIE.DV_ORIGEN,
                   [DF_DTDD00] = TSIE.DF_DTDD00,
                   [DV_RENENZA] = TSIE.DV_RENENZA,
                   [DV_EJECUTIVORENENZA] = TSIE.DV_EJECUTIVORENENZA,
                   [DI_SUCURSALNOVA] = TSIE.DI_SUCURSALNOVA,
                   [DI_CANALVENTANOVA] = TSIE.DI_CANALVENTANOVA,
                   [DV_CODTIPCREDITONOVA] = TSIE.DV_CODTIPCREDITONOVA,
                   [DV_4012NOVA] = TSIE.DV_4012NOVA,
                   [DI_FASENOVA] = TSIE.DI_FASENOVA,
                   [DV_CODEBCI] = TSIE.DV_CODEBCI,
                   [DV_CODEGLOSABCI] = TSIE.DV_CODEGLOSABCI,
                   [DDT_FECHAMODIF] = GETDATE(),
			    [DV_CODOPERRELACIONADA] = TSIE.DV_CODOPERRELACIONADA,
                   [BIT_NUEVO] = 1
             FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN DBO.TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION;

             UPDATE DBO.TB_SYS_OPERACION
               SET
                   [BIT_NUEVO] = 0
             FROM DBO.TB_SYS_OPERACION TSO
                  LEFT OUTER JOIN DBO.TB_SYS_INTER_ENTRADA TSIE ON TSO.DV_CODOPERACION = TSIE.DV_CODOPERACION
             WHERE TSIE.DV_CODOPERACION IS NULL;

         END TRY
         BEGIN CATCH
	   	   		 SELECT 'ERROR AL ACTUALIZAR OPERACION';
                     RETURN; 
		  /* ERROR AL ACTUALIZAR OPERACION */

         END CATCH;
	    -----------------------------------------
	    -----------------------------------------
         BEGIN TRY
             INSERT INTO DBO.TB_SYS_OPERACION
             (DV_CODOPERACION,
              DV_CODOPERRELACIONADA,
              DI_FK_IDCLIENTE,
              DI_FK_IDPRODUC,
              DI_IDESTADO,
              DI_DIASMORA,
              DM_SALDOINSOLUTO,
              DF_SALDOINSOUF,
              DV_TIPOMONEDA,
              DV_FK_IDTIPOPE,
              DI_FK_IDOFICINA,
              DI_FK_IDCEDENTE,
              DV_FK_TIPREGION,
              DDT_FECHVENCACT,
              DDT_FECHCURSE,
              DDT_FECHULTIMOPAGO,
              DV_FK_IDBANCA,
              DV_APODOBANCA,
              DV_GARANTIA,
              DV_ORIGEN,
              DF_DTDD00,
              DV_RENENZA,
              DV_EJECUTIVORENENZA,
              DI_SUCURSALNOVA,
              DI_CANALVENTANOVA,
              DV_CODTIPCREDITONOVA,
              DV_4012NOVA,
              DI_FASENOVA,
              DV_CODEBCI,
              DV_CODEGLOSABCI,
              BIT_NUEVO
             )
                    SELECT TSIE.DV_CODOPERACION,
                           TSIE.DV_CODOPERRELACIONADA,
                           TSIE.DI_FK_IDCLIENTE,
                           TSIE.DI_FK_IDPRODUC,
                           101 DI_IDESTADO,
                           TSIE.DI_DIASMORA,
                           TSIE.DM_SALDOINSOLUTO,
                           TSIE.DF_SALDOINSOUF,
                           TSIE.DV_TIPOMONEDA,
                           TSIE.DV_FK_IDTIPOPE,
                           TSIE.DI_FK_IDOFICINA,
                           TSIE.DI_FK_IDCEDENTE,
                           TSIE.DV_FK_TIPREGION,
                           TSIE.DDT_FECHVENCACT,
                           TSIE.DDT_FECHCURSE,
                           TSIE.DDT_FECHULTIMOPAGO,
                           TSIE.DV_FK_IDBANCA,
                           TSIE.DV_APODOBANCA,
                           TSIE.DV_GARANTIA,
                           TSIE.DV_ORIGEN,
                           TSIE.DF_DTDD00,
                           TSIE.DV_RENENZA,
                           TSIE.DV_EJECUTIVORENENZA,
                           TSIE.DI_SUCURSALNOVA,
                           TSIE.DI_CANALVENTANOVA,
                           TSIE.DV_CODTIPCREDITONOVA,
                           TSIE.DV_4012NOVA,
                           TSIE.DI_FASENOVA,
                           TSIE.DV_CODEBCI,
                           TSIE.DV_CODEGLOSABCI,
                           1
                    FROM DBO.TB_SYS_INTER_ENTRADA TSIE LEFT JOIN TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
                    WHERE((TSIE.DI_FK_IDCLIENTE IS NOT NULL)
                          OR (TSIE.DI_FK_IDPRODUC IS NOT NULL))
					 AND TSO.DV_CODOPERACION IS NULL;
         END TRY
         BEGIN CATCH
	   		 SELECT 'ERROR AL AGREGAR OPERACIONES A BOJ';
                     RETURN;   
		  /* ERROR AL AGREGAR OPERACIONES A BOJ*/

         END CATCH;
	    -----------------------------------------
	    -----------------------------------------
         BEGIN TRY
             EXEC SP_SISBOJ_CARGA_HITO;
         END TRY
         BEGIN CATCH
	
		 SELECT 'ERROR AL INSERTAR EL HITO A BOJ';
                     RETURN;   
		  /* ERROR AL INSERTAR EL HITO A BOJ*/


         END CATCH;
	   -- -----------------------------------------
	   -- -----------------------------------------
         BEGIN TRY
             EXEC SP_SISBOJ_GEN_SOLICITUD;
         END TRY
         BEGIN CATCH
	
	 SELECT 'ERROR AL INSERTAR EL SOLICITUD A BOJ';
                     RETURN;     
		  /* ERROR AL INSERTAR EL SOLICITUD A BOJ*/


         END CATCH;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_CARGA_INICIALALL]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_CARGA_INICIALALL]
AS
     BEGIN
        
	   



	   		  /*CARGA DATA DE PRODUCCION A INTERFAZ*/

  DECLARE @SOLICITUD_BCI VARCHAR(50), @SOLICITUD_NOVA VARCHAR(50),@SCRIPT VARCHAR(MAX);
    
        BEGIN  TRY
            IF(( SELECT  COUNT(*)     FROM       DBO.TB_SYS_INTER_ENTRADA TSIE )> 0)  -- contamos la cantidad de operaciones en la tabla de enterfaza de entrada
                BEGIN
				-- si existen registros en la tabla de interfaz de entrada los borro

			    INSERT INTO 
				 DBO.TB_SYS_INTER_ENTRADA_HIST
                ( di_IdOperInterEntr
				, DV_CODOPERACION
                , DV_CODOPERRELACIONADA
                , DI_FK_IDCLIENTE
                , DI_FK_IDPRODUC
                , DI_DIASMORA
                , DM_SALDOINSOLUTO
                , DF_SALDOINSOUF
                , DV_TIPOMONEDA
                , DV_FK_IDTIPOPE
                , DI_FK_IDOFICINA
                , DI_FK_IDCEDENTE
                , DV_FK_TIPREGION
                , DDT_FECHVENCACT
                , DDT_FECHCURSE
                , DDT_FECHULTIMOPAGO
                , DV_FK_IDBANCA
                , DV_APODOBANCA
                , DV_GARANTIA
                , DV_ORIGEN
                , DF_DTDD00
                , DV_RENENZA
                , DV_EJECUTIVORENENZA
                , DI_SUCURSALNOVA
                , DI_CANALVENTANOVA
                , DV_CODTIPCREDITONOVA
                , DV_4012NOVA
                , DI_FASENOVA
                , DV_CODEBCI
                , DV_CODEGLOSABCI
                , DDT_FECHAINGRESO
                , DI_CANTDCTOSESPERADOS
                , DI_PRIORIDAD
                , DV_PRIORIDADPYME
                , DV_SOLICITUDORIG
                , DI_IDUBICACION
                , ORIG_UBICACION
                , ORIG_PRODUCTO
                , ORIG_RUT
                , ORIG_DV
                , DDT_FECINGRESO
           ) select  * from DBO.TB_SYS_INTER_ENTRADA;

                    DELETE  FROM   DBO.TB_SYS_INTER_ENTRADA  ;
                    
                    DBCC CHECKIDENT(TB_SYS_INTER_ENTRADA, RESEED, 0);   --- con este comando reinicio el contador de la tabla
                END;
            END TRY
            BEGIN
                CATCH
                /*
                SELECT             ERROR_NUMBER() AS ERRORNUMBER,             ERROR_SEVERITY() AS ERRORSEVERITY,               ERROR_STATE() AS ERRORSTATE,                ERROR_PROCEDURE() AS ERRORPROCEDURE,                ERROR_LINE() AS ERRORLINE,                ERROR_MESSAGE() AS ERRORMESSAGE
                */
            END CATCH;





            BEGIN   TRY
                ----------------------------------------------------
                ---------------------------------------------------- 	
				--- crea notacion para la nueva solicitud  BCI
                SET @SOLICITUD_BCI = 'HITO(CON-COM) '+CONVERT(VARCHAR(15), (SELECT  MAX(SUBSTRING(RTRIM(TBL_HITO_JUD_BCI_HISTORICO.SOLICITUD), 14, 15) + 1) FROM  IN_CBZA..TBL_HITO_JUD_BCI_HISTORICO WHERE  TBL_HITO_JUD_BCI_HISTORICO.HITO_ESPECIAL = 0  ) );
                ------------------------------------------------------
                ------------------------------------------------------
				---Crea Notacion para la nueva solicitudHITO NOVA

                SET @SOLICITUD_NOVA = 'HITO CONSUMO '+CONVERT(VARCHAR(15), (SELECT  MAX(SUBSTRING(RTRIM(SOLICITUD), 14, 16) + 1) FROM IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO WHERE  FECHA_EJECUCION =(SELECT MAX(FECHA_EJECUCION) FROM IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO )));
                ----------------------------------------------------
                ----------------------------------------------------
    INSERT INTO DBO.TB_SYS_INTER_ENTRADA
           ( DV_CODOPERACION
                , DV_CODOPERRELACIONADA
                , DI_FK_IDCLIENTE
                , DI_FK_IDPRODUC
                , DI_DIASMORA
                , DM_SALDOINSOLUTO
                , DF_SALDOINSOUF
                , DV_TIPOMONEDA
                , DV_FK_IDTIPOPE
                , DI_FK_IDOFICINA
                , DI_FK_IDCEDENTE
                , DV_FK_TIPREGION
                , DDT_FECHVENCACT
                , DDT_FECHCURSE
                , DDT_FECHULTIMOPAGO
                , DV_FK_IDBANCA
                , DV_APODOBANCA
                , DV_GARANTIA
                , DV_ORIGEN
                , DF_DTDD00
                , DV_RENENZA
                , DV_EJECUTIVORENENZA
                , DI_SUCURSALNOVA
                , DI_CANALVENTANOVA
                , DV_CODTIPCREDITONOVA
                , DV_4012NOVA
                , DI_FASENOVA
                , DV_CODEBCI
                , DV_CODEGLOSABCI
                , DDT_FECHAINGRESO
                , DI_CANTDCTOSESPERADOS
                , DI_PRIORIDAD
                , DV_PRIORIDADPYME
                , DV_SOLICITUDORIG
                , DI_IDUBICACION
                , ORIG_UBICACION
                , ORIG_PRODUCTO
                , ORIG_RUT
                , ORIG_DV
                , DDT_FECINGRESO
           )
    select
           HBCC.OP_ORIG
         , HBCC.OPERACION_2
         , NULL
         , NULL
         , HBCC.DIAS_MORA
         , HBCC.MONTO
         , HBCC.MONTO/26550'UF'
         , 'CLP'
         , HBCC.TIPO_OP
         , isnull( ( select top 1 sisbojof.di_IdOficina from tb_Sys_Oficina sisbojof  where  sisbojof.di_IdOficina= HBCC.N_OF),0) 
         , '1'
         , iif(HBCC.REG ='','SR',HBCC.REG) REG
         , CONVERT(Datetime, HBCC.FAN , 120) -----datetime
         , CONVERT(Datetime, HBCC.FCURSE , 120)---datetime
         , CONVERT(Datetime, getdate() , 120)   --datetime
         , HBCC.BANCA
         , HBCC.NOM_BANCA
         , NULL
         , 'DEMJUD'
         , 0
         , HBCC.RENEG_POR_NZA
         , HBCC.EJECUTIVO_NEG
         , NULL
         , NULL
         , NULL
         , NULL
         , NULL
         , HBCC.CODE
         , HBCC.GLOSA
         , NULL
         , NULL
         , HBCC.PRIORIDAD
         , HBCC.PRIORIDAD_PYME
         ,  @SOLICITUD_BCI
         , NULL
         , HBCC.UBICACION
         , HBCC.PRODUCTO
         , HBCC.RUT
         , HBCC.DV
         , getdate() --datetime
    from
           IN_CBZA..tbl_hito_jud_bci_historico HBCC
		    where HBCC.Fecha_ejecucion = (Select MAX(Fecha_ejecucion)From  IN_CBZA..tbl_hito_jud_bci_historico)
           --  where HBCC.OP_ORIG='D39300120658'






 INSERT INTO [dbo].[sisboj_tbl_hito_jud_bci_historico]
           ([di_Iddecarga]
           ,[UBICACION]
           ,[OFICINA]
           ,[N_OF]
           ,[REG]
           ,[OP_ORIG]
           ,[OPERACION_2]
           ,[TIPO_OP]
           ,[MONTO]
           ,[BANCA]
           ,[RUT_COMPLETO]
           ,[RUT]
           ,[DV]
           ,[NOMBRE]
           ,[FAN]
           ,[FCURSE]
           ,[DIAS_MORA]
           ,[CODE]
           ,[GLOSA]
           ,[RENEG_POR_NZA]
           ,[EJECUTIVO_NEG]
           ,[PRIORIDAD]
           ,[NOM_BANCA]
           ,[PRODUCTO]
           ,[SOLICITUD]
           ,[nombre_region]
           ,[Fecha_ejecucion]
           ,[PRIORIDAD_PYME]
           ,[USUARIO_GENERA]
           ,[hito_especial]
           ,[acelerado]
		    ,[fec_carga_boj])
    select
(select  isnull(max(di_iddecarga),0)+1 'id' from sisboj_tbl_hito_jud_bci_historico),HBCC.*,getdate()
    from
           IN_CBZA..tbl_hito_jud_bci_historico HBCC
		    where HBCC.Fecha_ejecucion = (Select MAX(Fecha_ejecucion)From  IN_CBZA..tbl_hito_jud_bci_historico)
      





END TRY 

BEGIN CATCH
    SELECT           ERROR_NUMBER()    AS ErrorNumber         , ERROR_SEVERITY()  AS ErrorSeverity         , ERROR_STATE()     AS ErrorState         , ERROR_PROCEDURE() AS ErrorProcedure         , ERROR_LINE()      AS ErrorLine         , ERROR_MESSAGE()   AS ErrorMessage    ;

END CATCH;
	   
		  /*ANALIZA SI CLIENTE EXISTE, DE NO EXISTIR LO CREA
		  ACTUALIZA INFORMACIÓN ADICIONANDO DATOS EXTRAS DE BOJ*/
		   BEGIN TRY
            --------------
            DECLARE @RUT INT= 0, @DV CHAR(1)= '';
     
         DECLARE CLIENTEX CURSOR
         FOR
             SELECT TSIE.ORIG_RUT,  TSIE.ORIG_DV FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  LEFT OUTER JOIN DBO.TB_SYS_PERSONA TSP ON TSIE.ORIG_RUT = TSP.DI_RUT   AND TSIE.ORIG_DV = TSP.DC_DIGVER
             WHERE TSP.DI_RUT IS NULL;

         OPEN CLIENTEX;
         FETCH NEXT FROM CLIENTEX INTO @RUT, @DV;
         WHILE(@@FETCH_STATUS = 0)
             BEGIN
                 EXEC SP_SISBOJ_ADDCLIENTENEW
                      @RUT = @RUT,
                      @DIGVER = @DV;
                 FETCH NEXT FROM CLIENTEX INTO @RUT, @DV;
             END;
         IF(CURSOR_STATUS('GLOBAL', 'CLIENTEX') >= -1)
             BEGIN
                 CLOSE CLIENTEX;
                 DEALLOCATE CLIENTEX;
         END;

	    -------------------------------------------------

         BEGIN
             UPDATE DBO.TB_SYS_INTER_ENTRADA
               SET
                   DI_FK_IDCLIENTE = CLI.DI_IDCLIENTE,
                   DI_FK_IDPRODUC = PRO.DI_IDPRODUC

             FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN TB_SYS_PERSONA PER ON PER.DI_RUT = TSIE.ORIG_RUT
                                                   AND PER.DC_DIGVER = TSIE.ORIG_DV
                  INNER JOIN TB_SYS_CLIENTE CLI ON CLI.DI_FK_IDPERSONA = PER.DI_IDPERSONA
                  INNER JOIN TB_SYS_PRODUCTO PRO ON PRO.DV_NOMPRODUC = TSIE.ORIG_PRODUCTO;
         END;



			-------------------
         END TRY
         BEGIN CATCH
	   	   	   		 SELECT 'ERROR AL CARGAR LA INTERFAZ';
					   SELECT           ERROR_NUMBER()    AS ErrorNumber         , ERROR_SEVERITY()  AS ErrorSeverity         , ERROR_STATE()     AS ErrorState         , ERROR_PROCEDURE() AS ErrorProcedure         , ERROR_LINE()      AS ErrorLine         , ERROR_MESSAGE()   AS ErrorMessage    ;
                     RETURN; 
		  /* ERROR AL CARGAR LA INTERFAZ*/

         END CATCH;











	    -----------------------------------------
















	    -----------------------------------------






         BEGIN TRY
	   
		  /*RESPALDA OPERACIONES A ACTUALIZAR*/

             --- respalda en tabla historica las  operaciones que etan en operacion
			       IF((SELECT COUNT(di_IdOperacion) FROM TB_SYS_OPERACION) > 0)
             BEGIN
	   
/*  
		  PROCESO QUE RESPALDA LAS OPERACIONES QUE CAEN OTRA VEZ A BOJ
		  DE ESTA FORMA SE MANTIENE EL REGISTRO ANTERIOR EN EL HISTORICO 
	   */

                 INSERT INTO TB_SYS_OPERACION_HIST
                 (di_IdOperacion,
                  dv_CodOperacion,
                  dv_CodOperRelacionada,
                  di_fk_IdCliente,
                  di_fk_IdProduc,
                  di_IdEstado,
                  di_DiasMora,
                  dm_SaldoInsoluto,
                  df_SaldoInsoUF,
                  dv_TipoMoneda,
                  dv_fk_IdTipOpe,
                  di_fk_IdOficina,
                  di_fk_IdCedente,
                  dv_fk_TipRegion,
                  ddt_FechVencAct,
                  ddt_FechCurse,
                  ddt_FechUltimoPago,
                  dv_fk_IdBanca,
                  dv_ApodoBanca,
                  dv_Garantia,
                  dv_Origen,
                  df_DtdD00,
                  dv_ReneNza,
                  dv_EjecutivoReneNza,
                  di_SucursalNova,
                  di_CanalVentaNova,
                  dv_CodTipCreditoNova,
                  dv_4012Nova,
                  di_FaseNova,
                  dv_CodeBci,
                  dv_CodeGlosaBci,
                  ddt_FechaIngresoBoj
                 )
                        SELECT TSO.di_IdOperacion,
                               TSO.dv_CodOperacion,
                               TSO.dv_CodOperRelacionada,
                               TSO.di_fk_IdCliente,
                               TSO.di_fk_IdProduc,
                               TSO.di_IdEstado,
                               TSO.di_DiasMora,
                               TSO.dm_SaldoInsoluto,
                               TSO.df_SaldoInsoUF,
                               TSO.dv_TipoMoneda,
                               TSO.dv_fk_IdTipOpe,
                               TSO.di_fk_IdOficina,
                               TSO.di_fk_IdCedente,
                               TSO.dv_fk_TipRegion,
                               TSO.ddt_FechVencAct,
                               TSO.ddt_FechCurse,
                               TSO.ddt_FechUltimoPago,
                               TSO.dv_fk_IdBanca,
                               TSO.dv_ApodoBanca,
                               TSO.dv_Garantia,
                               TSO.dv_Origen,
                               TSO.df_DtdD00,
                               TSO.dv_ReneNza,
                               TSO.dv_EjecutivoReneNza,
                               TSO.di_SucursalNova,
                               TSO.di_CanalVentaNova,
                               TSO.dv_CodTipCreditoNova,
                               TSO.dv_4012Nova,
                               TSO.di_FaseNova,
                               TSO.dv_CodeBci,
                               TSO.dv_CodeGlosaBci,
                               TSO.ddt_FechaIngreso
                        FROM TB_SYS_OPERACION TSO
                             INNER JOIN TB_SYS_INTER_ENTRADA TSIE ON TSO.dv_CodOperacion = TSIE.dv_CodOperacion;
         END;


             UPDATE DBO.TB_SYS_OPERACION
               SET
                   [DI_FK_IDCLIENTE] = TSIE.DI_FK_IDCLIENTE,
                   [DI_FK_IDPRODUC] = TSIE.DI_FK_IDPRODUC,
                   [DI_IDESTADO] = 101,
                   [DI_DIASMORA] = TSIE.DI_DIASMORA,
                   [DM_SALDOINSOLUTO] = TSIE.DM_SALDOINSOLUTO,
                   [DF_SALDOINSOUF] = TSIE.DF_SALDOINSOUF,
                   [DV_TIPOMONEDA] = TSIE.DV_TIPOMONEDA,
                   [DV_FK_IDTIPOPE] = TSIE.DV_FK_IDTIPOPE,
                   [DI_FK_IDOFICINA] = TSIE.DI_FK_IDOFICINA,
                   [DI_FK_IDCEDENTE] = TSIE.DI_FK_IDCEDENTE,
                   [DV_FK_TIPREGION] = TSIE.DV_FK_TIPREGION,
                   [DDT_FECHVENCACT] = TSIE.DDT_FECHVENCACT,
                   [DDT_FECHCURSE] = TSIE.DDT_FECHCURSE,
                   [DDT_FECHULTIMOPAGO] = TSIE.DDT_FECHULTIMOPAGO,
                   [DV_FK_IDBANCA] = TSIE.DV_FK_IDBANCA,
                   [DV_APODOBANCA] = TSIE.DV_APODOBANCA,
                   [DV_GARANTIA] = TSIE.DV_GARANTIA,
                   [DV_ORIGEN] = TSIE.DV_ORIGEN,
                   [DF_DTDD00] = TSIE.DF_DTDD00,
                   [DV_RENENZA] = TSIE.DV_RENENZA,
                   [DV_EJECUTIVORENENZA] = TSIE.DV_EJECUTIVORENENZA,
                   [DI_SUCURSALNOVA] = TSIE.DI_SUCURSALNOVA,
                   [DI_CANALVENTANOVA] = TSIE.DI_CANALVENTANOVA,
                   [DV_CODTIPCREDITONOVA] = TSIE.DV_CODTIPCREDITONOVA,
                   [DV_4012NOVA] = TSIE.DV_4012NOVA,
                   [DI_FASENOVA] = TSIE.DI_FASENOVA,
                   [DV_CODEBCI] = TSIE.DV_CODEBCI,
                   [DV_CODEGLOSABCI] = TSIE.DV_CODEGLOSABCI,
                   [DDT_FECHAMODIF] = GETDATE(),
			    [DV_CODOPERRELACIONADA] = TSIE.DV_CODOPERRELACIONADA,
                   [BIT_NUEVO] = 1
             FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN DBO.TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION;

             UPDATE DBO.TB_SYS_OPERACION SET  [BIT_NUEVO] = 0
             FROM DBO.TB_SYS_OPERACION TSO
                  LEFT OUTER JOIN DBO.TB_SYS_INTER_ENTRADA TSIE ON TSO.DV_CODOPERACION = TSIE.DV_CODOPERACION
                       WHERE TSIE.DV_CODOPERACION IS NULL;

         END TRY
         BEGIN CATCH
	   	   		 SELECT 'ERROR AL ACTUALIZAR OPERACION';
                     RETURN; 
		  /* ERROR AL ACTUALIZAR OPERACION */

         END CATCH;
	    -----------------------------------------
	    -----------------------------------------


		-- despues de cargar las operaciones desde las tablas del (HITO)in_cbza en TB_SYS_INTER_ENTRADA,
		-- copiamos las que no existen en la siguiente tabla TB_SYS_OPERACION 
		-- es decir inserta operaciones
         BEGIN TRY
             INSERT INTO DBO.TB_SYS_OPERACION
             (DV_CODOPERACION,
              DV_CODOPERRELACIONADA,
              DI_FK_IDCLIENTE,
              DI_FK_IDPRODUC,
              DI_IDESTADO,
              DI_DIASMORA,
              DM_SALDOINSOLUTO,
              DF_SALDOINSOUF,
              DV_TIPOMONEDA,
              DV_FK_IDTIPOPE,
              DI_FK_IDOFICINA,
              DI_FK_IDCEDENTE,
              DV_FK_TIPREGION,
              DDT_FECHVENCACT,
              DDT_FECHCURSE,
              DDT_FECHULTIMOPAGO,
              DV_FK_IDBANCA,
              DV_APODOBANCA,
              DV_GARANTIA,
              DV_ORIGEN,
              DF_DTDD00,
              DV_RENENZA,
              DV_EJECUTIVORENENZA,
              DI_SUCURSALNOVA,
              DI_CANALVENTANOVA,
              DV_CODTIPCREDITONOVA,
              DV_4012NOVA,
              DI_FASENOVA,
              DV_CODEBCI,
              DV_CODEGLOSABCI,
              BIT_NUEVO
             )
                    SELECT TSIE.DV_CODOPERACION,
                           TSIE.DV_CODOPERRELACIONADA,
                           TSIE.DI_FK_IDCLIENTE,
                           TSIE.DI_FK_IDPRODUC,
                           101 DI_IDESTADO,
                           TSIE.DI_DIASMORA,
                           TSIE.DM_SALDOINSOLUTO,
                           TSIE.DF_SALDOINSOUF,
                           TSIE.DV_TIPOMONEDA,
                           TSIE.DV_FK_IDTIPOPE,
                           TSIE.DI_FK_IDOFICINA,
                           TSIE.DI_FK_IDCEDENTE,
                           TSIE.DV_FK_TIPREGION,
                           TSIE.DDT_FECHVENCACT,
                           TSIE.DDT_FECHCURSE,
                           TSIE.DDT_FECHULTIMOPAGO,
                           TSIE.DV_FK_IDBANCA,
                           TSIE.DV_APODOBANCA,
                           TSIE.DV_GARANTIA,
                           TSIE.DV_ORIGEN,
                           TSIE.DF_DTDD00,
                           TSIE.DV_RENENZA,
                           TSIE.DV_EJECUTIVORENENZA,
                           TSIE.DI_SUCURSALNOVA,
                           TSIE.DI_CANALVENTANOVA,
                           TSIE.DV_CODTIPCREDITONOVA,
                           TSIE.DV_4012NOVA,
                           TSIE.DI_FASENOVA,
                           TSIE.DV_CODEBCI,
                           TSIE.DV_CODEGLOSABCI,
                           1
                    FROM DBO.TB_SYS_INTER_ENTRADA TSIE LEFT JOIN TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
                    WHERE((TSIE.DI_FK_IDCLIENTE IS NOT NULL) OR (TSIE.DI_FK_IDPRODUC IS NOT NULL)) AND TSO.DV_CODOPERACION IS NULL;
         END TRY
         BEGIN CATCH
	   		 SELECT 'ERROR AL AGREGAR OPERACIONES A BOJ';
                     RETURN;   
		  /* ERROR AL AGREGAR OPERACIONES A BOJ*/

         END CATCH;
	    -----------------------------------------
	    -----------------------------------------
         BEGIN TRY
             --- crea el hito BOJ

			 		  DECLARE @IDPRODUCTO INT, @IDHITO INT;
         DECLARE OPER CURSOR
         FOR
             SELECT DISTINCT
                    (TSIE.DI_FK_IDPRODUC)
             FROM TB_SYS_INTER_ENTRADA TSIE
                  INNER JOIN TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
             ORDER BY TSIE.DI_FK_IDPRODUC;
         OPEN OPER;

		--RECOREMOS EL CURSOR PARA GENERAR LA DATA EN EL HITO
         FETCH NEXT FROM OPER INTO @IDPRODUCTO;
         WHILE(@@FETCH_STATUS = 0)
             BEGIN
                 INSERT INTO [DBO].[TB_SYS_HITO]  (DI_FK_IDTIPOHITO, DV_CODHITO, DI_FK_IDESTADO)  VALUES(2,NULL, 110);

			  --------------------------------
                 SET @IDHITO = @@IDENTITY;
			  --------------------------------
                 INSERT INTO TB_SYS_DETHITO  ([DI_FK_IDPRODUC],[DI_IDHITO], [DI_FK_IDOPE],[DI_FK_IDUBI],[DI_PRIORIDAD], [DV_SOLICITUDORIG],[DI_FK_IDESTADO] )
                        SELECT TSP.DI_IDPRODUC,@IDHITO,TSO.DI_IDOPERACION,TSU.DI_IDUBICACION,TSIE.DI_PRIORIDAD,TSIE.DV_SOLICITUDORIG,IIF(TSDH.DI_FK_IDOPE IS NULL, 120, 121)
                        FROM DBO.TB_SYS_INTER_ENTRADA TSIE
                             INNER JOIN DBO.TB_SYS_OPERACION TSO ON TSIE.DV_CODOPERACION = TSO.DV_CODOPERACION
                             INNER JOIN TB_SYS_UBICACION TSU ON TSU.DV_DETUBI = CASE
                                                                                    WHEN(TSIE.ORIG_UBICACION = '')
                                                                                    THEN 'SIN UBICACION'
                                                                                    ELSE TSIE.ORIG_UBICACION
                                                                                END
                             INNER JOIN TB_SYS_PRODUCTO TSP ON TSP.DI_IDPRODUC = TSO.DI_FK_IDPRODUC
                             LEFT OUTER JOIN TB_SYS_DETHITO TSDH ON TSO.DI_IDOPERACION = TSDH.DI_FK_IDOPE
                                                      WHERE TSIE.DI_FK_IDPRODUC = @IDPRODUCTO AND TSO.BIT_NUEVO = 1;
                 FETCH NEXT FROM oper INTO @IDPRODUCTO;
             END;
         IF(CURSOR_STATUS('GLOBAL', 'OPER') >= -1)
             BEGIN
                 CLOSE OPER;
                 DEALLOCATE OPER;
         END;

         END TRY
         BEGIN CATCH
	
		 SELECT 'ERROR AL INSERTAR EL HITO A BOJ';
                     RETURN;   
		  /* ERROR AL INSERTAR EL HITO A BOJ*/


         END CATCH;
	   -- -----------------------------------------
	   -- -----------------------------------------
         BEGIN TRY
          --   EXEC SP_SISBOJ_GEN_SOLICITUD;     --- genera una nueva solictud de pedidos con encabezados detalles

		           BEGIN TRY
             INSERT INTO DBO.TB_SYS_SOLICITUD
             (
	       --DI_IDSOLICITUD - THIS COLUMN VALUE IS AUTO-GENERATED
             DI_FK_IDUBI,
             DI_FK_IDHITO,
             DI_FK_IDESTADO,
	       --DDT_FECHACREACION,
             DI_CANTDCTOSSOLICITADOS
             )
                    SELECT TSDH.DI_FK_IDUBI,
                           TSDH.DI_IDHITO,
                           130,
                           COUNT(1)
                    FROM TB_SYS_DETHITO TSDH
                         LEFT OUTER JOIN DBO.TB_SYS_DETSOLICITUD TSDS ON TSDH.DI_FK_IDOPE = TSDS.DI_FK_IDOPER
                    WHERE TSDS.DI_FK_IDOPER IS NULL
                    GROUP BY TSDH.DI_FK_IDUBI,
                             TSDH.DI_IDHITO;
         END TRY
         BEGIN CATCH
		   
/*ERROR AL INGRESAR ENCABEZADO SOLICITUD*/

             RETURN;
         END CATCH;
--         BEGIN TRY
--             INSERT INTO DBO.TB_SYS_DOCUMENTO
--             (
--		   --DI_IDDCTO - THIS COLUMN VALUE IS AUTO-GENERATED
--             DI_FK_IDUBI,
--             DI_FK_TIPODCTO,
--		   --DDT_FECHASALIDABOV,
--             DI_FK_IDESTADO,
--             DV_CODBARRA,
--             DI_FK_IDOPER,
--             DB_CHECKOK,
--             DV_GLOSA,
--             DB_APODERADOBANCO
--             )
--                    SELECT

--				-- DI_IDDCTO - INT
--                    IIF(TSD.DI_FK_IDUBI IS NOT NULL, TSD.DI_FK_IDUBI, TSDH.DI_FK_IDUBI) DI_FK_IDUBI, -- DI_FK_IDUBI - INT
--                    TSTD.DI_IDTIPDCTO, -- DI_FK_TIPODCTO - INT
--				--'2017-10-05 10:46:27', -- DDT_FECHASALIDABOV - DATETIME
--                    179 DI_FK_IDESTADO, -- DI_FK_IDESTADO - INT
--                    NULL DV_CODBARRA, -- DV_CODBARRA - VARCHAR
--                    TSDH.DI_FK_IDOPE, -- DI_FK_IDOPER - INT
--                    0 DB_CHECKOK, -- DB_CHECKOK - BIT
--                    NULL DV_GLOSA, -- DV_GLOSA - VARCHAR
--                    0 DB_APODERADOBANCO -- DB_APODERADOBANCO - BIT
       
--                    FROM TB_SYS_DETHITO TSDH
--                         INNER JOIN TB_SYS_TIPODCTO TSTD ON CASE
--                                                                WHEN(DI_FK_IDPRODUC = 3
--                                                                     OR DI_FK_IDPRODUC = 4)
--                                                                THEN 80
--                                                                ELSE 80
--                                                            END = TSTD.DI_IDTIPDCTOPJ
--                         LEFT JOIN TB_SYS_DOCUMENTO TSD ON TSDH.DI_FK_IDOPE = TSD.DI_FK_IDOPER
--                                                           AND TSTD.DI_IDTIPDCTO = TSD.DI_FK_TIPODCTO;
--         END TRY
--         BEGIN CATCH
		   
--/*ERROR AL INSERTAR DOCUMENTO*/

--             RETURN;
--         END CATCH;
         BEGIN TRY
             INSERT INTO DBO.TB_SYS_DETSOLICITUD
             (
    --DI_IDDETSOL - THIS COLUMN VALUE IS AUTO-GENERATED
             DI_FK_IDSOLICITUD,
             DI_FK_IDUBI,
             DI_FK_IDDETHITO,
             DI_FK_IDDCTO,
             DI_FK_IDOPER,
             DI_FK_IDESTADO
    --DDT_FECHAINGRESO
             )
                    SELECT TSS.DI_IDSOLICITUD,
                           TSS.DI_FK_IDUBI,
                           MAX(TSDH.DI_IDDETHITO) DI_IDDETHITO,
                           NULL,
                           TSDH.DI_FK_IDOPE,
                           141 DI_FK_IDESTADO
                    FROM DBO.TB_SYS_SOLICITUD TSS
                         INNER JOIN tb_sys_hito tsh ON TSS.di_fk_IdHito = tsh.di_IdHito
                         INNER JOIN TB_SYS_DETHITO TSDH ON TSS.DI_FK_IDHITO = TSDH.DI_IDHITO
                                                           AND TSS.DI_FK_IDUBI = TSDH.DI_FK_IDUBI
                         INNER JOIN TB_SYS_OPERACION TSO ON TSDH.DI_FK_IDOPE = TSO.DI_IDOPERACION
                         LEFT OUTER JOIN DBO.TB_SYS_DETSOLICITUD TSDS ON TSO.DI_IDOPERACION = TSDS.DI_FK_IDOPER
                    WHERE tsh.di_fk_IdEstado = 110
                          AND TSO.BIT_NUEVO = 1
                          AND TSDS.DI_FK_IDOPER IS NULL
                    GROUP BY TSS.DI_IDSOLICITUD,
                             TSS.DI_FK_IDUBI,
                             TSDH.DI_FK_IDOPE;
		  ---- actualiza estado del hito
             UPDATE tb_sys_hito
               SET
                   tb_sys_hito.di_fk_IdEstado = 111
             WHERE tb_sys_hito.di_IdHito IN
             (
                 SELECT di_fk_IdHito
                 FROM dbo.tb_Sys_Solicitud
             );
         END TRY
         BEGIN CATCH
		  
		/*ERROR AL INSERTAR DETALLE SOLICITUD*/

             RETURN;
         END CATCH;

         END TRY
         BEGIN CATCH;
	
	              SELECT 'ERROR AL INSERTAR EL SOLICITUD A BOJ';
                     RETURN;     
		  /* ERROR AL INSERTAR EL SOLICITUD A BOJ*/
         END CATCH;


     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_GEN_SOLICITUD]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_GEN_SOLICITUD]
AS
     BEGIN
         BEGIN TRY
             INSERT INTO DBO.TB_SYS_SOLICITUD
             (
	       --DI_IDSOLICITUD - THIS COLUMN VALUE IS AUTO-GENERATED
             DI_FK_IDUBI,
             DI_FK_IDHITO,
             DI_FK_IDESTADO,
	       --DDT_FECHACREACION,
             DI_CANTDCTOSSOLICITADOS
             )
                    SELECT TSDH.DI_FK_IDUBI,
                           TSDH.DI_IDHITO,
                           130,
                           COUNT(1)
                    FROM TB_SYS_DETHITO TSDH
                         LEFT OUTER JOIN DBO.TB_SYS_DETSOLICITUD TSDS ON TSDH.DI_FK_IDOPE = TSDS.DI_FK_IDOPER
                    WHERE TSDS.DI_FK_IDOPER IS NULL
                    GROUP BY TSDH.DI_FK_IDUBI,
                             TSDH.DI_IDHITO;
         END TRY
         BEGIN CATCH
		   
/*ERROR AL INGRESAR ENCABEZADO SOLICITUD*/

             RETURN;
         END CATCH;
--         BEGIN TRY
--             INSERT INTO DBO.TB_SYS_DOCUMENTO
--             (
--		   --DI_IDDCTO - THIS COLUMN VALUE IS AUTO-GENERATED
--             DI_FK_IDUBI,
--             DI_FK_TIPODCTO,
--		   --DDT_FECHASALIDABOV,
--             DI_FK_IDESTADO,
--             DV_CODBARRA,
--             DI_FK_IDOPER,
--             DB_CHECKOK,
--             DV_GLOSA,
--             DB_APODERADOBANCO
--             )
--                    SELECT

--				-- DI_IDDCTO - INT
--                    IIF(TSD.DI_FK_IDUBI IS NOT NULL, TSD.DI_FK_IDUBI, TSDH.DI_FK_IDUBI) DI_FK_IDUBI, -- DI_FK_IDUBI - INT
--                    TSTD.DI_IDTIPDCTO, -- DI_FK_TIPODCTO - INT
--				--'2017-10-05 10:46:27', -- DDT_FECHASALIDABOV - DATETIME
--                    179 DI_FK_IDESTADO, -- DI_FK_IDESTADO - INT
--                    NULL DV_CODBARRA, -- DV_CODBARRA - VARCHAR
--                    TSDH.DI_FK_IDOPE, -- DI_FK_IDOPER - INT
--                    0 DB_CHECKOK, -- DB_CHECKOK - BIT
--                    NULL DV_GLOSA, -- DV_GLOSA - VARCHAR
--                    0 DB_APODERADOBANCO -- DB_APODERADOBANCO - BIT
       
--                    FROM TB_SYS_DETHITO TSDH
--                         INNER JOIN TB_SYS_TIPODCTO TSTD ON CASE
--                                                                WHEN(DI_FK_IDPRODUC = 3
--                                                                     OR DI_FK_IDPRODUC = 4)
--                                                                THEN 80
--                                                                ELSE 80
--                                                            END = TSTD.DI_IDTIPDCTOPJ
--                         LEFT JOIN TB_SYS_DOCUMENTO TSD ON TSDH.DI_FK_IDOPE = TSD.DI_FK_IDOPER
--                                                           AND TSTD.DI_IDTIPDCTO = TSD.DI_FK_TIPODCTO;
--         END TRY
--         BEGIN CATCH
		   
--/*ERROR AL INSERTAR DOCUMENTO*/

--             RETURN;
--         END CATCH;
         BEGIN TRY
             INSERT INTO DBO.TB_SYS_DETSOLICITUD
             (
    --DI_IDDETSOL - THIS COLUMN VALUE IS AUTO-GENERATED
             DI_FK_IDSOLICITUD,
             DI_FK_IDUBI,
             DI_FK_IDDETHITO,
             DI_FK_IDDCTO,
             DI_FK_IDOPER,
             DI_FK_IDESTADO
    --DDT_FECHAINGRESO
             )
                    SELECT TSS.DI_IDSOLICITUD,
                           TSS.DI_FK_IDUBI,
                           MAX(TSDH.DI_IDDETHITO) DI_IDDETHITO,
                           NULL,
                           TSDH.DI_FK_IDOPE,
                           141 DI_FK_IDESTADO
                    FROM DBO.TB_SYS_SOLICITUD TSS
                         INNER JOIN tb_sys_hito tsh ON TSS.di_fk_IdHito = tsh.di_IdHito
                         INNER JOIN TB_SYS_DETHITO TSDH ON TSS.DI_FK_IDHITO = TSDH.DI_IDHITO
                                                           AND TSS.DI_FK_IDUBI = TSDH.DI_FK_IDUBI
                         INNER JOIN TB_SYS_OPERACION TSO ON TSDH.DI_FK_IDOPE = TSO.DI_IDOPERACION
                         LEFT OUTER JOIN DBO.TB_SYS_DETSOLICITUD TSDS ON TSO.DI_IDOPERACION = TSDS.DI_FK_IDOPER
                    WHERE tsh.di_fk_IdEstado = 110
                          AND TSO.BIT_NUEVO = 1
                          AND TSDS.DI_FK_IDOPER IS NULL
                    GROUP BY TSS.DI_IDSOLICITUD,
                             TSS.DI_FK_IDUBI,
                             TSDH.DI_FK_IDOPE;
		  ---- actualiza estado del hito
             UPDATE tb_sys_hito
               SET
                   tb_sys_hito.di_fk_IdEstado = 111
             WHERE tb_sys_hito.di_IdHito IN
             (
                 SELECT di_fk_IdHito
                 FROM dbo.tb_Sys_Solicitud
             );
         END TRY
         BEGIN CATCH
		  
		/*ERROR AL INSERTAR DETALLE SOLICITUD*/

             RETURN;
         END CATCH;
     END;


         

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_HIST_OPER_UPT]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_HIST_OPER_UPT]
AS
     BEGIN
         IF(
           (
               SELECT COUNT(di_IdOperacion)
               FROM TB_SYS_OPERACION
           ) > 0)
             BEGIN
	   
/*  
		  PROCESO QUE RESPALDA LAS OPERACIONES QUE CAEN OTRA VEZ A BOJ
		  DE ESTA FORMA SE MANTIENE EL REGISTRO ANTERIOR EN EL HISTORICO 
	   */

                 INSERT INTO TB_SYS_OPERACION_HIST
                 (di_IdOperacion,
                  dv_CodOperacion,
                  dv_CodOperRelacionada,
                  di_fk_IdCliente,
                  di_fk_IdProduc,
                  di_IdEstado,
                  di_DiasMora,
                  dm_SaldoInsoluto,
                  df_SaldoInsoUF,
                  dv_TipoMoneda,
                  dv_fk_IdTipOpe,
                  di_fk_IdOficina,
                  di_fk_IdCedente,
                  dv_fk_TipRegion,
                  ddt_FechVencAct,
                  ddt_FechCurse,
                  ddt_FechUltimoPago,
                  dv_fk_IdBanca,
                  dv_ApodoBanca,
                  dv_Garantia,
                  dv_Origen,
                  df_DtdD00,
                  dv_ReneNza,
                  dv_EjecutivoReneNza,
                  di_SucursalNova,
                  di_CanalVentaNova,
                  dv_CodTipCreditoNova,
                  dv_4012Nova,
                  di_FaseNova,
                  dv_CodeBci,
                  dv_CodeGlosaBci,
                  ddt_FechaIngresoBoj
                 )
                        SELECT TSO.di_IdOperacion,
                               TSO.dv_CodOperacion,
                               TSO.dv_CodOperRelacionada,
                               TSO.di_fk_IdCliente,
                               TSO.di_fk_IdProduc,
                               TSO.di_IdEstado,
                               TSO.di_DiasMora,
                               TSO.dm_SaldoInsoluto,
                               TSO.df_SaldoInsoUF,
                               TSO.dv_TipoMoneda,
                               TSO.dv_fk_IdTipOpe,
                               TSO.di_fk_IdOficina,
                               TSO.di_fk_IdCedente,
                               TSO.dv_fk_TipRegion,
                               TSO.ddt_FechVencAct,
                               TSO.ddt_FechCurse,
                               TSO.ddt_FechUltimoPago,
                               TSO.dv_fk_IdBanca,
                               TSO.dv_ApodoBanca,
                               TSO.dv_Garantia,
                               TSO.dv_Origen,
                               TSO.df_DtdD00,
                               TSO.dv_ReneNza,
                               TSO.dv_EjecutivoReneNza,
                               TSO.di_SucursalNova,
                               TSO.di_CanalVentaNova,
                               TSO.dv_CodTipCreditoNova,
                               TSO.dv_4012Nova,
                               TSO.di_FaseNova,
                               TSO.dv_CodeBci,
                               TSO.dv_CodeGlosaBci,
                               TSO.ddt_FechaIngreso
                        FROM TB_SYS_OPERACION TSO
                             INNER JOIN TB_SYS_INTER_ENTRADA TSIE ON TSO.dv_CodOperacion = TSIE.dv_CodOperacion;
         END;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI]
AS
    DECLARE
        @SOLICITUD_BCI VARCHAR(50), @SOLICITUD_NOVA VARCHAR(50), @SCRIPT VARCHAR(MAX);
    BEGIN
        BEGIN
            TRY
            IF(
                (
                       SELECT
                              COUNT(*)
                       FROM
                              DBO.TB_SYS_INTER_ENTRADA TSIE
                )
                > 0)
                BEGIN
                    DELETE
                    FROM
                           DBO.TB_SYS_INTER_ENTRADA
                    ;
                    
                    DBCC CHECKIDENT(TB_SYS_INTER_ENTRADA, RESEED, 0);
                END;
            END TRY
            BEGIN
                CATCH
                /*
                SELECT
                ERROR_NUMBER() AS ERRORNUMBER,
                ERROR_SEVERITY() AS ERRORSEVERITY,
                ERROR_STATE() AS ERRORSTATE,
                ERROR_PROCEDURE() AS ERRORPROCEDURE,
                ERROR_LINE() AS ERRORLINE,
                ERROR_MESSAGE() AS ERRORMESSAGE
                */
            END CATCH;
            BEGIN
                TRY
                ----------------------------------------------------
                ---------------------------------------------------- 	
                SET @SOLICITUD_BCI = 'HITO(CON-COM) '+CONVERT(VARCHAR(15), (
                       SELECT
                              MAX(SUBSTRING(RTRIM(TBL_HITO_JUD_BCI_HISTORICO.SOLICITUD), 14, 15) + 1)
                       FROM
                              IN_CBZA..TBL_HITO_JUD_BCI_HISTORICO
                       WHERE
                              TBL_HITO_JUD_BCI_HISTORICO.HITO_ESPECIAL = 0
                )
                );
                ------------------------------------------------------
                ------------------------------------------------------
                SET @SOLICITUD_NOVA = 'HITO CONSUMO '+CONVERT(VARCHAR(15), (
                       SELECT
                              MAX(SUBSTRING(RTRIM(SOLICITUD), 14, 16) + 1)
                       FROM
                              IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                       WHERE
                              FECHA_EJECUCION =
                              (
                                     SELECT
                                            MAX(FECHA_EJECUCION)
                                     FROM
                                            IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                              )
                )
                );
                ----------------------------------------------------
                ----------------------------------------------------
    INSERT INTO DBO.TB_SYS_INTER_ENTRADA
           ( DV_CODOPERACION
                , DV_CODOPERRELACIONADA
                , DI_FK_IDCLIENTE
                , DI_FK_IDPRODUC
                , DI_DIASMORA
                , DM_SALDOINSOLUTO
                , DF_SALDOINSOUF
                , DV_TIPOMONEDA
                , DV_FK_IDTIPOPE
                , DI_FK_IDOFICINA
                , DI_FK_IDCEDENTE
                , DV_FK_TIPREGION
                , DDT_FECHVENCACT
                , DDT_FECHCURSE
                , DDT_FECHULTIMOPAGO
                , DV_FK_IDBANCA
                , DV_APODOBANCA
                , DV_GARANTIA
                , DV_ORIGEN
                , DF_DTDD00
                , DV_RENENZA
                , DV_EJECUTIVORENENZA
                , DI_SUCURSALNOVA
                , DI_CANALVENTANOVA
                , DV_CODTIPCREDITONOVA
                , DV_4012NOVA
                , DI_FASENOVA
                , DV_CODEBCI
                , DV_CODEGLOSABCI
                , DDT_FECHAINGRESO
                , DI_CANTDCTOSESPERADOS
                , DI_PRIORIDAD
                , DV_PRIORIDADPYME
                , DV_SOLICITUDORIG
                , DI_IDUBICACION
                , ORIG_UBICACION
                , ORIG_PRODUCTO
                , ORIG_RUT
                , ORIG_DV
                , DDT_FECINGRESO
           )
    select
           HBCC.OP_ORIG
         , HBCC.OPERACION_2
         , NULL
         , NULL
         , HBCC.DIAS_MORA
         , HBCC.MONTO
         , HBCC.MONTO/26550'UF'
         , 'CLP'
         , HBCC.TIPO_OP
         , HBCC.N_OF
         , '1'
         , iif(HBCC.REG ='','SR',HBCC.REG) REG
         , CONVERT(Datetime, HBCC.FAN , 120) -----datetime
         , CONVERT(Datetime, HBCC.FCURSE , 120)---datetime
         , CONVERT(Datetime, getdate() , 120)   --datetime
         , HBCC.BANCA
         , HBCC.NOM_BANCA
         , NULL
         , 'DEMJUD'
         , 0
         , HBCC.RENEG_POR_NZA
         , HBCC.EJECUTIVO_NEG
         , NULL
         , NULL
         , NULL
         , NULL
         , NULL
         , HBCC.CODE
         , HBCC.GLOSA
         , NULL
         , NULL
         , HBCC.PRIORIDAD
         , HBCC.PRIORIDAD_PYME
         ,  @SOLICITUD_BCI
         , NULL
         , HBCC.UBICACION
         , HBCC.PRODUCTO
         , HBCC.RUT
         , HBCC.DV
         , getdate() --datetime
    from
           IN_CBZA..tbl_hito_jud_bci_historico HBCC
		    where HBCC.Fecha_ejecucion = (Select MAX(Fecha_ejecucion)From  IN_CBZA..tbl_hito_jud_bci_historico)
           --  where HBCC.OP_ORIG='D39300120658'
END TRY BEGIN CATCH
    SELECT
           ERROR_NUMBER()    AS ErrorNumber
         , ERROR_SEVERITY()  AS ErrorSeverity
         , ERROR_STATE()     AS ErrorState
         , ERROR_PROCEDURE() AS ErrorProcedure
         , ERROR_LINE()      AS ErrorLine
         , ERROR_MESSAGE()   AS ErrorMessage
    ;

END CATCH;
                ;
            
            END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI_NUEVO]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI_NUEVO]
AS
    DECLARE
        @SOLICITUD_BCI VARCHAR(50), @SOLICITUD_NOVA VARCHAR(50), @SCRIPT VARCHAR(MAX);
    BEGIN
        BEGIN
            TRY
            IF(
                (
                       SELECT
                              COUNT(*)
                       FROM
                              DBO.TB_SYS_INTER_ENTRADA TSIE
                )
                > 0)
                BEGIN
                    DELETE
                    FROM
                           DBO.TB_SYS_INTER_ENTRADA
                    ;
                    
                    DBCC CHECKIDENT(TB_SYS_INTER_ENTRADA, RESEED, 0);
                END;
            END TRY
            BEGIN
                CATCH
                /*
                SELECT
                ERROR_NUMBER() AS ERRORNUMBER,
                ERROR_SEVERITY() AS ERRORSEVERITY,
                ERROR_STATE() AS ERRORSTATE,
                ERROR_PROCEDURE() AS ERRORPROCEDURE,
                ERROR_LINE() AS ERRORLINE,
                ERROR_MESSAGE() AS ERRORMESSAGE
                */
            END CATCH;
            BEGIN
                TRY
                ----------------------------------------------------
                ---------------------------------------------------- 	
                SET @SOLICITUD_BCI = 'HITO(CON-COM) '+CONVERT(VARCHAR(15), (
                       SELECT
                              MAX(SUBSTRING(RTRIM(TBL_HITO_JUD_BCI_HISTORICO.SOLICITUD), 14, 15) + 1)
                       FROM
                              IN_CBZA..TBL_HITO_JUD_BCI_HISTORICO
                       WHERE
                              TBL_HITO_JUD_BCI_HISTORICO.HITO_ESPECIAL = 0
                )
                );
                ------------------------------------------------------
                ------------------------------------------------------
                SET @SOLICITUD_NOVA = 'HITO CONSUMO '+CONVERT(VARCHAR(15), (
                       SELECT
                              MAX(SUBSTRING(RTRIM(SOLICITUD), 14, 16) + 1)
                       FROM
                              IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                       WHERE
                              FECHA_EJECUCION =
                              (
                                     SELECT
                                            MAX(FECHA_EJECUCION)
                                     FROM
                                            IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                              )
                )
                );
                ----------------------------------------------------
                ----------------------------------------------------
    INSERT INTO DBO.TB_SYS_INTER_ENTRADA
           ( DV_CODOPERACION
                , DV_CODOPERRELACIONADA
                , DI_FK_IDCLIENTE
                , DI_FK_IDPRODUC
                , DI_DIASMORA
                , DM_SALDOINSOLUTO
                , DF_SALDOINSOUF
                , DV_TIPOMONEDA
                , DV_FK_IDTIPOPE
                , DI_FK_IDOFICINA
                , DI_FK_IDCEDENTE
                , DV_FK_TIPREGION
                , DDT_FECHVENCACT
                , DDT_FECHCURSE
                , DDT_FECHULTIMOPAGO
                , DV_FK_IDBANCA
                , DV_APODOBANCA
                , DV_GARANTIA
                , DV_ORIGEN
                , DF_DTDD00
                , DV_RENENZA
                , DV_EJECUTIVORENENZA
                , DI_SUCURSALNOVA
                , DI_CANALVENTANOVA
                , DV_CODTIPCREDITONOVA
                , DV_4012NOVA
                , DI_FASENOVA
                , DV_CODEBCI
                , DV_CODEGLOSABCI
                , DDT_FECHAINGRESO
                , DI_CANTDCTOSESPERADOS
                , DI_PRIORIDAD
                , DV_PRIORIDADPYME
                , DV_SOLICITUDORIG
                , DI_IDUBICACION
                , ORIG_UBICACION
                , ORIG_PRODUCTO
                , ORIG_RUT
                , ORIG_DV
                , DDT_FECINGRESO
           )
    select
           HBCC.OP_ORIG
         , HBCC.OPERACION_2
         , NULL
         , NULL
         , HBCC.DIAS_MORA
         , HBCC.MONTO
         , HBCC.MONTO/26550'UF'
         , 'CLP'
         , HBCC.TIPO_OP
         , HBCC.N_OF
         , '1'
         , HBCC.REG
         , CONVERT(Datetime, HBCC.FAN , 120) -----datetime
         , CONVERT(Datetime, HBCC.FCURSE , 120)---datetime
         , CONVERT(Datetime, getdate() , 120)   --datetime
         , HBCC.BANCA
         , HBCC.NOM_BANCA
         , NULL
         , 'DEMJUD'
         , 0
         , HBCC.RENEG_POR_NZA
         , HBCC.EJECUTIVO_NEG
         , NULL
         , NULL
         , NULL
         , NULL
         , NULL
         , HBCC.CODE
         , HBCC.GLOSA
         , NULL
         , NULL
         , HBCC.PRIORIDAD
         , HBCC.PRIORIDAD_PYME
         , 'HITO CONSUMO-454'
         , NULL
         , HBCC.UBICACION
         , HBCC.PRODUCTO
         , HBCC.RUT
         , HBCC.DV
         , getdate() --datetime
    from
           IN_CBZA..tbl_hito_jud_bci_historico HBCC
           --  where HBCC.OP_ORIG='D39300120658'
END TRY BEGIN CATCH
    SELECT
           ERROR_NUMBER()    AS ErrorNumber
         , ERROR_SEVERITY()  AS ErrorSeverity
         , ERROR_STATE()     AS ErrorState
         , ERROR_PROCEDURE() AS ErrorProcedure
         , ERROR_LINE()      AS ErrorLine
         , ERROR_MESSAGE()   AS ErrorMessage
    ;

END CATCH;
                ;
            
            END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI_old]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_SISBOJ_HITO_COM_CON_NOVA_BCI_old]
AS
     DECLARE @SOLICITUD_BCI VARCHAR(50), @SOLICITUD_NOVA VARCHAR(50), @SCRIPT VARCHAR(MAX);
     BEGIN
         BEGIN TRY
             IF(
               (
                   SELECT COUNT(*)
                   FROM DBO.TB_SYS_INTER_ENTRADA TSIE
               ) > 0)
                 BEGIN
                     DELETE FROM DBO.TB_SYS_INTER_ENTRADA;
                     DBCC CHECKIDENT(TB_SYS_INTER_ENTRADA, RESEED, 0);
             END;
         END TRY
         BEGIN CATCH
	

/* 
		SELECT
			ERROR_NUMBER() AS ERRORNUMBER,
			ERROR_SEVERITY() AS ERRORSEVERITY,
			ERROR_STATE() AS ERRORSTATE,
			ERROR_PROCEDURE() AS ERRORPROCEDURE,
			ERROR_LINE() AS ERRORLINE,
			ERROR_MESSAGE() AS ERRORMESSAGE
	*/


         END CATCH;
         BEGIN TRY
		  ----------------------------------------------------
		  ----------------------------------------------------		  
             SET @SOLICITUD_BCI = 'HITO(CON-COM) '+CONVERT(VARCHAR(15),
                                                          (
                                                              SELECT MAX(SUBSTRING(RTRIM(TBL_HITO_JUD_BCI_HISTORICO.SOLICITUD), 14, 15) + 1)
                                                              FROM IN_CBZA..TBL_HITO_JUD_BCI_HISTORICO
                                                              WHERE TBL_HITO_JUD_BCI_HISTORICO.HITO_ESPECIAL = 0
                                                          ));
		  ------------------------------------------------------
		  ------------------------------------------------------
             SET @SOLICITUD_NOVA = 'HITO CONSUMO '+CONVERT(VARCHAR(15),
                                                          (
                                                              SELECT MAX(SUBSTRING(RTRIM(SOLICITUD), 14, 16) + 1)
                                                              FROM IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                                                              WHERE FECHA_EJECUCION =
                                                              (
                                                                  SELECT MAX(FECHA_EJECUCION)
                                                                  FROM IN_CBZA..TBL_HITO_JUD_NOVA_HISTORICO
                                                              )
                                                          ));
		  ----------------------------------------------------
		  ----------------------------------------------------
             INSERT INTO DBO.TB_SYS_INTER_ENTRADA
             (DV_CODOPERACION,
              DV_CODOPERRELACIONADA,
              DI_FK_IDCLIENTE,
              DI_FK_IDPRODUC,
              DI_DIASMORA,
              DM_SALDOINSOLUTO,
              DF_SALDOINSOUF,
              DV_TIPOMONEDA,
              DV_FK_IDTIPOPE,
              DI_FK_IDOFICINA,
              DI_FK_IDCEDENTE,
              DV_FK_TIPREGION,
              DDT_FECHVENCACT,
              DDT_FECHCURSE,
              DDT_FECHULTIMOPAGO,
              DV_FK_IDBANCA,
              DV_APODOBANCA,
              DV_GARANTIA,
              DV_ORIGEN,
              DF_DTDD00,
              DV_RENENZA,
              DV_EJECUTIVORENENZA,
              DI_SUCURSALNOVA,
              DI_CANALVENTANOVA,
              DV_CODTIPCREDITONOVA,
              DV_4012NOVA,
              DI_FASENOVA,
              DV_CODEBCI,
              DV_CODEGLOSABCI,
              DDT_FECHAINGRESO,
              DI_CANTDCTOSESPERADOS,
              DI_PRIORIDAD,
              DV_PRIORIDADPYME,
              DV_SOLICITUDORIG,
              DI_IDUBICACION,
              ORIG_UBICACION,
              ORIG_PRODUCTO,
              ORIG_RUT,
              ORIG_DV
           --,DDT_FECINGRESO
             )
                    SELECT DV_CODOPERACION = BBR.NUM_ORIGINAL,
                           DV_CODOPERRELACIONADA = BBR.OPERACION,
                           DI_FK_IDCLIENTE = NULL,
                           DI_FK_IDPRODUC = NULL,
                           DI_DIASMORA = BBR.DIAS_MORA_ACT,
                           DM_SALDOINSOLUTO = BBR.SALDO$_ACT,
                           DF_SALDOINSOUF = NULL,
                           DV_TIPOMONEDA = 'CLP',
                           DV_FK_IDTIPOPE = BBR.TIOAUX,
                           DI_FK_IDOFICINA = BBR.N_OF,
                           DI_FK_IDCEDENTE = 1, --BCI
                           DV_FK_TIPREGION = IIF(BBR.TIOAUX LIKE 'PAP%', 'RM', IIF(BBR.REG = ''
                                                                                   OR BBR.REG = NULL, 'SR', BBR.REG)),
                           DDT_FECHVENCACT = BBR.FECHA_VCTO_ACT,
                           DDT_FECHCURSE = MIN(CONVERT(DATETIME, EFA.ACCOUNT_OPEN_DT, 120)),
                           DDT_FECHULTIMOPAGO = NULL,
                           DV_FK_IDBANCA = BBR.SUBBANCA,
                           DV_APODOBANCA = IIF(LEFT(SUBBANCA, 2) <> 'PM', 'RETAIL', 'PYME'),
                           DV_GARANTIA = NULL,
                           DV_ORIGEN = 'DEMJUD',
                           DF_DTDD00 = 0,
                           DV_RENENZA = IIF(TDC.FLD_OPE IS NOT NULL, 'SI', 'NO'),
                           DV_EJECUTIVORENENZA = BBR.EJECUTIVO_NEG,
                           DI_SUCURSALNOVA = NULL,
                           DI_CANALVENTANOVA = NULL,
                           DV_CODTIPCREDITONOVA = NULL,
                           DV_4012NOVA = NULL,
                           DI_FASENOVA = NULL,
                           DV_CODEBCI = IIF(BBR.CODE IS NULL
                                            AND BBR.E1 = 'DEMANDA CONSUMO', '150-DOC-PAG', BBR.CODE),
                           DV_CODEGLOSABCI = IIF(BBR.CODE IS NULL
                                                 AND E1 = 'DEMANDA CONSUMO', 'PAGARE', BBR.GLOSA),
                           DDT_FECHAINGRESO = NULL,
                           DI_CANTDCTOSESPERADOS = NULL,
                           DI_PRIORIDAD = IIF(BBR.SALDO$_ACT > 10000000, 1, 2),
                           DI_PRIORIDADPYME = IIF(BBR.SALDO$_ACT >= 50000000, 'ALERTA:$50M', 'BAJO $50M'),
                           DV_SOLICITUDORIG = @SOLICITUD_BCI,
                           DI_IDUBICACION = NULL,
                           ORIG_UBICACION = CASE
                                                WHEN BBR.NOMBRE_OFICINA = 'TBANC'
                                                THEN 'TBANC'
                                                WHEN BBR.REG = 'RM'
                                                     OR BBR.TIOAUX LIKE 'PAP%'
                                                THEN 'UCC'
                                                WHEN BBR.REG = 'RE'
                                                THEN 'SUCURSAL RE'
                                                WHEN BBR.REG IS NULL
                                                THEN 'SUCURSAL RE'
                                                WHEN BBR.N_OF = 617
                                                THEN 'SUCURSAL RE'
                                                ELSE 'SUCURSAL RE'
                                            END,
                           ORIG_PRODUCTO = CASE
                                               WHEN BBR.E1 = 'DEMANDA COMERCIAL'
                                               THEN 'COMERCIAL'
                                               WHEN BBR.E1 = 'DEMANDA CONSUMO'
                                               THEN 'CONSUMO'
                                               WHEN BBR.MINDETALLE_TIOAUX = 'CONSUMO'
                                                    AND E1 = 'DEMANDA PYME'
                                               THEN 'CONSUMO'
                                               ELSE 'COMERCIAL'
                                           END,
                           ORIG_RUT = BBR.RUT_SOLO,
                           ORIG_DV = BBR.DV
                    FROM IN_CBZA..BCI_RSO_new BBR
                         --LEFT OUTER JOIN IN_CBZA..TB_SEG_SOLICITUDES TSS ON TSS.OP_ORIGINAL = BBR.NUM_ORIGINAL
                         LEFT OUTER JOIN IN_CBZA..TBL_COLABORADORES_VIG TCV ON BBR.RUT_SOLO = TCV.RUT_SOLO
                         LEFT OUTER JOIN IN_CBZA..TBL_MARCAS_EXCLUSION_CLIENTES TMEC ON(TMEC.COLABORADOR = 1
                                                                                        OR TMEC.PEP = 1
                                                                                        OR TMEC.FALLECIDO = 1
                                                                                        OR TMEC.FRAUDE = 1
                                                                                        OR TMEC.CLIENTE_CCEE = 1
                                                                                        OR TMEC.LIR = 1
                                                                                        OR TMEC.CLIENTE_CUENTA_ESPECIAL = 1
                                                                                        OR INHIBICION_PREJUDICIAL = 1
                                                                                        OR SUSPENCION_JUDICIAL_PCJ = 1)
                                                                                       AND BBR.RUT_SOLO = TMEC.RUT_SOLO
                         LEFT OUTER JOIN IN_CBZA..TBL_ASIG_CCEE TAC ON BBR.RUT_SOLO = TAC.RUT_SOLO
                         LEFT OUTER JOIN IN_CBZA..JU_JUDICIAL JJ ON JJ.FECHA_ASIGNACION IS NOT NULL
                                                                    AND BBR.NUM_ORIGINAL = JJ.OPERACION
                         LEFT OUTER JOIN IN_CBZA..TB_SEG_SOLICITUDES_RESP_20150311 TSSR2 ON BBR.NUM_ORIGINAL = TSSR2.OP_ORIGINAL
                         LEFT OUTER JOIN IN_CBZA..TBL_DES_CRE TDC ON TDC.FLD_GLO_DES_CRE = 'RENEG X NORMALIZA'
                                                                     AND BBR.NUM_ORIGINAL = TDC.FLD_OPE
                         LEFT OUTER JOIN IN_CBZA..EH_FECHA_APERTURA EFA ON BBR.NUM_ORIGINAL = EFA.ACCOUNT_NUM
                         LEFT OUTER JOIN IN_CBZA..TBL_JUD_ACELERACION AS ACE ON BBR.NUM_ORIGINAL = ACE.OPERACION
                         --INNER JOIN IN_CBZA..TBL_OPE_CAMB_OFI AS TOCO ON BBR.NUM_ORIGINAL = TOCO.OPE_NUEVA
                         --INNER JOIN IN_CBZA..TBL_OFICINA AS TOF ON SUBSTRING(TOCO.OPE_ANTIGUA, 2, 3) = TOF.COD_OFICINA
                         --                                          AND TOF.CEDENTE = 'BCI'
                         INNER JOIN
                    (
                        SELECT SUM(BBR.SALDO$_ACT) SALDO$_ACT,
                               BBR.RUT_SOLO
                        FROM IN_CBZA..BCI_RSO BBR
                        GROUP BY RUT_SOLO
                    ) AS BBR_CLI ON BBR.RUT_SOLO = BBR_CLI.RUT_SOLO
                    WHERE(BBR.E1 = 'DEMANDA PYME'
                          OR BBR.E1 = 'DEMANDA COMERCIAL'
                          OR BBR.E1 = 'DEMANDA CONSUMO')
                         --AND TSS.OP_ORIGINAL IS NULL
                         AND TCV.RUT_SOLO IS NULL
                         AND TMEC.RUT_SOLO IS NULL
                         AND TAC.RUT_SOLO IS NULL
                         AND JJ.OPERACION IS NULL
                         AND BBR.DIAS_MORA_ACT <= 320
                         AND TSSR2.OP_ORIGINAL IS NULL
                         --AND BBR.NUM_ORIGINAL NOT IN('D13499966402', 'D03399935220', 'D01305026918') --NOTA:OPERACIONES INFORMADAS EN LA MORA Y ESTAN AL DÍA
				    --AND BBR.TIOAUX NOT IN('COM306', 'COM302', 'COM308', 'COM304', 'CON007', 'CON012', 'CON016', 'CON310', 'COM318', 'CON509', 'CON614', 'CON730', 'CON740', 'CON731', 'COM732', 'CON739', 'CON735')
                         AND BBR.NUM_ORIGINAL NOT IN
                    (
                        --SELECT THJBH.OP_ORIG
                        --FROM IN_CBZA..TBL_HITO_JUD_BCI_HISTORICO THJBH
                        --WHERE YEAR(CONVERT(DATE, THJBH.FECHA_EJECUCION)) * 100 + MONTH(CONVERT(DATE, THJBH.FECHA_EJECUCION)) = YEAR(CONVERT(DATE, GETDATE())) * 100 + MONTH(CONVERT(DATE, GETDATE()))
                        --UNION --20171207 VT SE AGREGA TABLA DE EXCLUSIÓN DE OPERACIONES, ESTAS OPERACIONES TIENEN ALGUN PROBLEMA SISTEMICO POR EJEMPLO CON EL ROV

                        SELECT OPERACION_ORIGINAL
                        FROM IN_CBZA..TBL_JUD_EXCLUSION_OPERACIONES
                    )
                         OR (ACE.NOMBRE_HITO IS NULL
                             AND ACE.ID_HITO = 1
                             AND ACE.CEDENTE = 'BCI'
                             AND ACE.COD_USUARIO_ANULA IS NULL)
                    GROUP BY BBR.NUM_ORIGINAL,
                             BBR.OPERACION,
                             BBR.DIAS_MORA_ACT,
                             BBR.SALDO$_ACT,
                             BBR.TIOAUX,
                             BBR.N_OF,
                             BBR.REG,
                             BBR.FECHA_VCTO_ACT,
                             BBR.SUBBANCA,
                             BBR.EJECUTIVO_NEG,
                             TDC.FLD_OPE,
                             BBR.CODE,
                             BBR.E1,
                             BBR.GLOSA,
                             BBR.SALDO$_ACT,
                             BBR.NOMBRE_OFICINA,
                             BBR.MINDETALLE_TIOAUX,
                             BBR.RUT_SOLO,
                             BBR.DV
                             --TOF.CEDENTE,
                             --TOF.COD_TIPO_REGION,
                             --TOF.COD_OFICINA

		  ------------------------------------------------------
                    UNION ALL
		  ------------------------------------------------------

                    SELECT DV_CODOPERACION = TMN.OPERACION_ORIGINAL,
                           DV_CODOPERRELACIONADA = NULL,
                           DI_FK_IDCLIENTE = NULL,
                           DI_FK_IDPRODUC = 4, --CONSUMO
                           DI_DIASMORA = MAX(TMN.DIAS_MORA),
                           DM_SALDOINSOLUTO = SUM(TMN.SALDO_INSOLUTO),
                           DF_SALDOINSOUF = NULL,
                           DV_TIPOMONEDA = 'CLP',
                           DV_FK_IDTIPOPE = CASE
                                                WHEN LEFT(TMN.OPERACION_ORIGINAL, 1) <> 0
                                                THEN LEFT(TMN.OPERACION_ORIGINAL, 3)
                                                WHEN LEFT(TMN.OPERACION_ORIGINAL, 2) = '00'
                                                THEN SUBSTRING(TMN.OPERACION_ORIGINAL, 3, 3)
                                                ELSE SUBSTRING(TMN.OPERACION_ORIGINAL, 2, 3)
                                            END,
                           DI_FK_IDOFICINA = IIF(JONC.NOF_NUEVA IS NOT NULL, JONC.NOF_NUEVA, CONVERT(INT, SUBSTRING(TMN.OPERACION_ORIGINAL, 5, 2))),
                           DI_FK_IDCEDENTE = 2, --NOVA
                           DV_FK_TIPREGION = NULL,
                           DDT_FECHVENCACT = NULL,
                           DDT_FECHCURSE = NULL,
                           DDT_FECHULTIMOPAGO = MAX(CONVERT(DATETIME, TMN.FEC_ULTP, 120)),
                           DV_FK_IDBANCA = 'PBM',
                           DV_APODOBANCA = NULL,
                           DV_GARANTIA = IIF(GN2.RUT_CLI IS NOT NULL, 'S', 'N'),
                           DV_ORIGEN = 'DEMJUD',
                           DF_DTDD00 = 1,
                           DV_RENENZA = NULL,
                           DV_EJECUTIVORENENZA = NULL,
                           DI_SUCURSALNOVA = CONVERT(INT, SUBSTRING(TMN.OPERACION_ORIGINAL, 5, 2)),
                           DI_CANALVENTANOVA = ISNULL(MIN(CONVERT(INT, OVJI.CANAL_VENTA)), 0),
                           DV_CODTIPCREDITONOVA = ISNULL(MAX(SC.TIPO_CRED), 'NR'),
                           DV_4012NOVA = 'N',
                           DI_FASENOVA = TTM.COD_TRAMO_MORA,
                           DV_CODEBCI = NULL,
                           DV_CODEGLOSABCI = NULL,
                           DDT_FECHAINGRESO = NULL,
                           DI_CANTDCTOSESPERADOS = NULL,
                           DI_PRIORIDAD = IIF(SUM(TMN.SALDO_INSOLUTO) >= 10000000, 1, 2),
                           DI_PRIORIDADPYME = NULL,
                           DV_SOLICITUDORIG = @SOLICITUD_NOVA,
                           DI_IDUBICACION = NULL,
                           ORIG_UBICACION = 'NOVA',
                           ORIG_PRODUCTO = 'CONSUMO',
                           ORIG_RUT = TMN.RUT_CLIENTE,
                           ORIG_DV = TMN.DV
                    FROM
                    (
                        SELECT MAX(TMN.DIAS_MORA) AS DIAS_MORA,
                               TMN.FEC_ULTP,
                               TMN.SALDO_INSOLUTO,
                               TMN.RUT_CLIENTE,
                               TMN.DV,
                               TMN.OPERACION_ORIGINAL
                        FROM IN_CBZA..TBL_MORA_NOVA TMN
                             LEFT OUTER JOIN
                        (
                            SELECT OPERACION,
                                   RUT_CLIENTE,
                                   OPEL8 = RIGHT(OPERACION, 8),
                                   FECHA_SALIDA,
                                   ROL
                            FROM IN_CBZA..JU_JUDICIAL JJ
                            WHERE((ORGANIZACION = 'NOVA'
                                   AND (ABOGADO NOT LIKE '%SIN ABO%'
                                        OR ABOGADO NOT LIKE '%DEVUELTO TEMPORAL CODIGO%')
                                   OR (ABOGADO LIKE('%DEVUELTO TEMPORAL CODIGO UNICO()%')))
                                  AND (JJ.ROL IS NOT NULL
                                       OR JJ.FECHA_SALIDA IS NULL))
                        ) JJ ON(RIGHT(TMN.OPERACION_ORIGINAL, 8) = RIGHT(JJ.OPERACION, 8)
                                AND TMN.RUT_CLIENTE = JJ.RUT_CLIENTE)
                             LEFT OUTER JOIN IN_CBZA..TB_SEG_SOLICITUDES TSS ON TMN.OPERACION_ORIGINAL = TSS.OP_ORIGINAL
                        WHERE JJ.RUT_CLIENTE IS NULL
                              AND TMN.COD_PRODUCTO = 'CRD'
                              AND TMN.DIAS_MORA BETWEEN 90 AND 320
                              AND TMN.SALDO_INSOLUTO >= 500000
                              AND TMN.MARCA_GARANTIA_ESTATAL = 'N'--DESCRIPCION <> 'FOGAPE'
                              AND TSS.OP_ORIGINAL IS NULL
                        GROUP BY TMN.FEC_ULTP,
                                 TMN.SALDO_INSOLUTO,
                                 TMN.RUT_CLIENTE,
                                 TMN.DV,
                                 TMN.OPERACION_ORIGINAL
                    ) TMN
                    LEFT OUTER JOIN IN_CBZA..IN_DBC ID ON TMN.RUT_CLIENTE = ID.RUT
                    INNER JOIN IN_CBZA..TBL_TRAMO_MORA TTM ON TIPO_CAR = 'CON'
                                                              AND TMN.DIAS_MORA BETWEEN TTM.DIA_INI AND TTM.DIA_FIN
                    --LEFT OUTER JOIN IN_CBZA..TB_SEG_SOLICITUDES TSS ON TMN.OPERACION_ORIGINAL = TSS.OP_ORIGINAL
                    LEFT OUTER JOIN IN_CBZA..TBL_COLABORADORES_VIG TCV ON TMN.RUT_CLIENTE = TCV.RUT_SOLO
                    LEFT OUTER JOIN IN_CBZA..TBL_MARCAS_EXCLUSION_CLIENTES TMEC ON(TMEC.COLABORADOR = 1
                                                                                   OR TMEC.PEP = 1
                                                                                   OR TMEC.FALLECIDO = 1
                                                                                   OR TMEC.FRAUDE = 1
                                                                                   OR TMEC.CLIENTE_CCEE = 1
                                                                                   OR TMEC.LIR = 1
                                                                                   OR TMEC.CLIENTE_CUENTA_ESPECIAL = 1)
                                                                                  AND TMN.RUT_CLIENTE = TMEC.RUT_SOLO
                    LEFT OUTER JOIN IN_CBZA..TBL_ASIG_CCEE TAC ON TMN.RUT_CLIENTE = TAC.RUT_SOLO
                    LEFT OUTER JOIN IN_CBZA..JU_JUDICIAL JJ ON TMN.OPERACION_ORIGINAL = JJ.OPERACION
                    LEFT OUTER JOIN IN_CBZA..TB_SEG_SOLICITUDES_RESP_20150311 TSR2 ON TMN.OPERACION_ORIGINAL = TSR2.OP_ORIGINAL
                    --LEFT OUTER JOIN IN_CBZA..TBL_HITO_JUD_NOVA_TCR_HISTORICO THJNTH ON YEAR(CONVERT(DATE, THJNTH.FECHA_EJECUCION)) * 100 + MONTH(CONVERT(DATE, THJNTH.FECHA_EJECUCION)) = YEAR(CONVERT(DATE, GETDATE())) * 100 + MONTH(CONVERT(DATE, GETDATE()))
                    --                                                                   AND TMN.OPERACION_ORIGINAL = THJNTH.OPERACION
                    LEFT OUTER JOIN IN_CBZA..SISCOB_CALLCENTER SC ON TMN.RUT_CLIENTE = CONVERT(INT, SC.RUT_CLIENTE)
                    LEFT OUTER JOIN IN_CBZA..OPERA_VIG_JRI_IFRS OVJI ON TMN.OPERACION_ORIGINAL = OVJI.NUMERO_OPERACION
                    LEFT JOIN IN_CBZA..GTIAS_NOVA_201309 GN2 ON TMN.RUT_CLIENTE = GN2.RUT_CLI
                    LEFT JOIN IN_CBZA..JG_OF_NOVA_CERRADAS JONC ON CONVERT(INT, SUBSTRING(TMN.OPERACION_ORIGINAL, 5, 2)) = JONC.NOF
                    WHERE /*TSS.OP_ORIGINAL IS NULL
                          AND*/ TCV.RUT_SOLO IS NULL
                          AND TMEC.RUT_SOLO IS NULL
                          AND TAC.RUT_SOLO IS NULL
                          AND JJ.OPERACION IS NULL
                          AND TSR2.OP_ORIGINAL IS NULL
                          --AND THJNTH.OPERACION IS NULL
                    GROUP BY TMN.RUT_CLIENTE,
                             TMN.DV,
                             TMN.OPERACION_ORIGINAL,
                             TTM.COD_TRAMO_MORA,
                             GN2.RUT_CLI,
                             JONC.NOF_NUEVA
                    HAVING MAX(TMN.DIAS_MORA) <= 320;
         END TRY
         BEGIN CATCH
	    

/* 
		    SELECT
			    ERROR_NUMBER() AS ERRORNUMBER,
			    ERROR_SEVERITY() AS ERRORSEVERITY,
			    ERROR_STATE() AS ERRORSTATE,
			    ERROR_PROCEDURE() AS ERRORPROCEDURE,
			    ERROR_LINE() AS ERRORLINE,
			    ERROR_MESSAGE() AS ERRORMESSAGE
	    */


         END CATCH;
     END;

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_LIMPIA_TABLAS_FLUJO]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_LIMPIA_TABLAS_FLUJO]
AS
BEGIN
   
    --LIMPIA SOLICITUDES
    DELETE FROM DBO.TB_SYS_DETSOLICITUD;
    DBCC CHECKIDENT(TB_SYS_DETSOLICITUD, RESEED, 0);

    DELETE FROM DBO.TB_SYS_SOLICITUD;
    DBCC CHECKIDENT(TB_SYS_SOLICITUD, RESEED, 0);
    ----------------------------------------------------
    ----------------------------------------------------
    ----------------------------------------------------
    --LIMPIA HITOS
    DELETE FROM DBO.TB_SYS_DETHITO;
    DBCC CHECKIDENT(TB_SYS_DETHITO, RESEED, 0);

    DELETE FROM DBO.TB_SYS_HITO;
    DBCC CHECKIDENT(TB_SYS_HITO, RESEED, 0);
    ----------------------------------------------------
    ----------------------------------------------------
    ----------------------------------------------------
    --LIMPIA OPERACIONES 
    DELETE FROM DBO.TB_SYS_OPERACION;
    DBCC CHECKIDENT(TB_SYS_OPERACION, RESEED, 0);
    ----------------------------------------------------
    ----------------------------------------------------
    ----------------------------------------------------
    --LIMPIA INTERFAZ ENTRADA HITO
    DELETE FROM DBO.TB_SYS_INTER_ENTRADA;
    DBCC CHECKIDENT(TB_SYS_INTER_ENTRADA, RESEED, 0);
    ----------------------------------------------------
    ----------------------------------------------------
    ----------------------------------------------------
    --LIMPIA DOCUMENTOS  
    --DELETE FROM DBO.TB_SYS_DOCUMENTO;
    --DBCC CHECKIDENT(TB_SYS_DOCUMENTO, RESEED, 0);


END

GO
/****** Object:  StoredProcedure [dbo].[SP_SISBOJ_REVISA_OFICINAS]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SISBOJ_REVISA_OFICINAS]
AS
         BEGIN
             BEGIN TRY
                 BEGIN TRANSACTION INSERTOFICINA;
                 INSERT INTO dbo.tb_Sys_Oficina
(di_Idcedente,
 di_IdOficina,
 dv_Oficina,
 dv_OficinaPrin,
 dv_RegNormaliza,
 dv_RegBanco,
 dv_JefeRegNormaliza,
 dv_fk_IdTipReg,
 dv_IdJefeRegNorma,
 dv_JefeResponsable,
 di_IdOficinaMype,
 dv_OficinaMype,
 dv_RegionMype
)
                        SELECT TSC.di_IdCedente,
                               TOF.cod_oficina,
                               TOF.oficina,
                               TOF.oficina_principal,
                               TOF.region_normaliza,
                               TOF.region_banco,
                               TOF.jefe_region_normaliza,
                               TOF.cod_tipo_region,
                               TOF.cod_jefe_reg_normaliza,
                               TOF.jefe_responsable,
                               TOF.cod_oficina_mype,
                               TOF.oficina_mype,
                               TOF.region_mype
                        FROM IN_CBZA..TBL_OFICINA TOF
                             INNER JOIN TB_SYS_CEDENTES TSC ON TOF.cedente = TSC.dv_Cedente
                             LEFT OUTER JOIN TB_SYS_OFICINA TSO ON TOF.cod_oficina = TSO.di_IdOficina
                                                                   AND TSC.di_IdCedente = TSO.di_Idcedente
                        WHERE TSO.di_IdOficina IS NULL
                              AND TOF.cod_oficina IS NOT NULL;
                 COMMIT TRANSACTION INSERTOFICINA;
             END TRY
             BEGIN CATCH
                 ROLLBACK TRANSACTION INSERTOFICINA;
		  --ERROR AL INSERTAR OFICINAS NUEVAS AL MODELO
/* 
		SELECT
			ERROR_NUMBER() AS ErrorNumber,
			ERROR_SEVERITY() AS ErrorSeverity,
			ERROR_STATE() AS ErrorState,
			ERROR_PROCEDURE() AS ErrorProcedure,
			ERROR_LINE() AS ErrorLine,
			ERROR_MESSAGE() AS ErrorMessage
	*/


             END CATCH;
         END;


GO
/****** Object:  StoredProcedure [dbo].[SP_SYSBOJ_OBTIENE_INTERFAZJP]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_SYSBOJ_OBTIENE_INTERFAZJP]
AS
     BEGIN
         BEGIN TRY
             SELECT tsipd.STR_CodOperacion,
                    tsipd.INT_IdOficina,
                    tsipd.STR_Oficina,
                    tsipd.STR_Region,
                    tsipd.STR_Operacion,
                    tsipd.STR_TipOperacion,
                    tstm.STR_TipoMoneda,
                    tsipd.STR_RutCompleto,
                    tsipd.STR_NombreCliente,
                    tsipd.STR_Garantia,
                    tsipd.INT_SaldoInsoluto,
                    tsipd.DDT_FechaVencimiento,
                    tsipd.STR_Banca,
                    tsipd.DDT_FechaCarga,
                    tsipd.DDT_FechaCurse,
                    tsipd.STR_OperacionOriginal,
                    tsipd.STR_Ejecutivo,
                    tsipd.STR_Dtd,
                    tsipd.STR_Solicitud,
                    tsipd.INT_Prioridad,
                    tsipd.STR_EstOperacion,
                    tsipd.STR_RutAbogado,
                    tsipd.STR_RutContralor,
                    tsipd.DDT_FechaAsig,
                    tsipd.DDT_FechaEntrega,
                    tsipd.DDT_FechaRecep,
                    tsipd.STR_Cedente,
                    tsipd.STR_MultiJuicio,
                    tsipd.INT_Monto,
                    tstm.STR_TipoMoneda,
                    tsipd.DDT_FecPrimVenCuot,
                    tsipd.INT_CuotasPagadas,
                    tsipd.INT_CuotasInpagas,
                    tsipd.INT_SaldoCapital,
                    tsipd.INT_Interes,
                    tsipd.INT_IdOficina,
                    tsipd.INT_SaldoCapitalInsoluto,
                    tsipd.INT_MoraTotalImpaga,
                    tsipd.INT_PlazoPactado,
                    tsipd.FLO_TasaSpread,
                    tsipd.DDT_FechaCurse,
                    tsipd.DDT_FechaPrimVencImpago,
                    tsipd.STR_Comuna,
                    tsipd.INT_MontoEnPesos
             FROM dbo.tb_Sys_InterfazPJ_Enc tsipe
                  INNER JOIN dbo.tb_Sys_InterfazPJ_Det tsipd ON tsipd.INT_IdInterfaz_Enc = tsipe.INT_IdInterfaz_Enc
                  INNER JOIN dbo.tb_Sys_TipoMoneda tstm ON tstm.INT_IdTipoMoneda = tsipd.INT_IdTipoMoneda;
         END TRY
         BEGIN CATCH
             SELECT 1 AS ERROR,
                    ERROR_MESSAGE() AS ErrorMensaje,
                    ERROR_NUMBER() AS ErrorNumero;
         END CATCH;
     END;


GO
/****** Object:  UserDefinedFunction [dbo].[FN_CED]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- SELECT DBO.FN_CED(16409327)

Create FUNCTION [dbo].[FN_CED]	(@RUT INT) RETURNS NVARCHAR(30) AS

BEGIN
	DECLARE @CEDENTE NVARCHAR(30)
		
					
/*	SET @CEDENTE = (SELECT	DISTINCT TOP 1
										CASE WHEN (	LEFT	(COD_BANCA,1)		='P'	
													AND		LEFT(COD_BANCA,2)	!='PM' 
													AND		COD_BANCA			!= 'PBM' 
													AND		(OFICINA				NOT LIKE '%NOVA%'
																OR OFICINA IS NULL)
													)
												THEN 'BCI'
											WHEN	(COD_BANCA IN ('EM', 'EMQ','EQ'))		
												THEN 'EMPRESA' 
											WHEN	(LEFT(COD_BANCA,2)='PM')				
												THEN 'PYME'
											WHEN	(COD_BANCA = 'PBM')					
												THEN 
														CASE
															WHEN (SELECT TOP 1 1 FROM TBL_MALLA_MYPE WHERE FLD_RUT_CLIENTE = @RUT AND FLD_PER = (SELECT MAX(FLD_PER) FROM TBL_MALLA_MYPE)) = 1 THEN 'MYPE'
															ELSE 'NOVA'
														END
											WHEN	(OFICINA  LIKE '%NOVA%')				
												THEN 
													CASE
														WHEN (SELECT TOP 1 1 FROM TBL_MALLA_MYPE WHERE FLD_RUT_CLIENTE = @RUT AND FLD_PER = (SELECT MAX(FLD_PER) FROM TBL_MALLA_MYPE)) = 1 THEN 'MYPE'
														ELSE 'NOVA'
													END
										END
					FROM	(
								SELECT 
										LTRIM(RTRIM(COD_BANCA))		COD_BANCA
										,LTRIM(RTRIM(OFICINA))		OFICINA
								FROM	IN_DBC (NOLOCK)
								WHERE	RUT = @RUT
									
							)CED
                    )*/

	SET @CEDENTE =	(
						SELECT	CEDENTE
						FROM	IN_CBZA.dbo.TBL_CEDENTE_CLIENTE (NOLOCK)
						WHERE	RUT_CLIENTE = @RUT
					)

	RETURN @CEDENTE

END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_dv]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create function [dbo].[fn_dv]
(
   @numrut varchar(30)
)
RETURNS char(1)
AS 
BEGIN

   Declare @pos int,
           @factor int,
           @total int,
           @dv int,
           @dvp char(1),
           @fld_rut_cli varchar(30)

	SELECT @factor = 2,
          @total = 0,
          @dvp = '',          

		  @fld_rut_cli = @numrut
   select @pos = len(@fld_rut_cli)
   while @pos > 0
   begin
	   select @total  = @total + substring(@fld_rut_cli, @pos, 1) * @factor
	   select @pos    = @pos - 1
           , @factor = @factor + 1
      if @factor > 7 select @factor = 2
   end
   select @dv = 11 - (@total % 11)
   return (case @dv
             when 10 then 'K'
             when 11 then '0'
             else CONVERT(char(1), @dv)
           end)
END






GO
/****** Object:  UserDefinedFunction [dbo].[ObtenerDigitoVerificador]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[ObtenerDigitoVerificador]
(
	@RUT INTEGER
 )
 RETURNS VARCHAR(1)
 
 AS
 BEGIN
 
 DECLARE @DV VARCHAR(1)
 DECLARE @RUTAUX INTEGER
 DECLARE @DIGITO INTEGER
 DECLARE @CONTADOR INTEGER
 DECLARE @MULTIPLO INTEGER
 DECLARE @ACUMULADOR INTEGER
 
 
 SET @CONTADOR = 2;
 SET @ACUMULADOR = 0;
 SET @MULTIPLO = 0;
 
	WHILE(@RUT!=0)
		BEGIN
 
			SET @MULTIPLO = (@RUT % 10) * @CONTADOR;
			SET @ACUMULADOR = @ACUMULADOR + @MULTIPLO;
			SET @RUT = @RUT / 10;
			SET @CONTADOR = @CONTADOR + 1;
			IF(@CONTADOR = 8)
			BEGIN
				SET @CONTADOR = 2;
			END;
		END;
 
	SET @DIGITO = 11 - (@ACUMULADOR % 11);
 
	SET @DV = LTRIM(RTRIM(CONVERT(VARCHAR(2),@DIGITO)));
 
	IF(@DIGITO = 10)
	BEGIN
		SET @DV = 'K';
	END;
 
	IF(@DIGITO = 11)
	BEGIN
		SET @DV = '0';
	END;
 
RETURN @DV
 
END
 



GO
/****** Object:  Table [dbo].[areatrabajo_custodia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[areatrabajo_custodia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_custodia] [int] NULL,
	[fk_id_areatrabajo] [int] NULL,
 CONSTRAINT [PK_areatrabajo_custodia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Base$]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base$](
	[UBICACION] [nvarchar](255) NULL,
	[OFICINA] [nvarchar](255) NULL,
	[N_OF] [float] NULL,
	[REG] [nvarchar](255) NULL,
	[OP_ORIG] [nvarchar](255) NULL,
	[OPERACION_2] [nvarchar](255) NULL,
	[TIPO_OP] [nvarchar](255) NULL,
	[MONTO] [float] NULL,
	[BANCA] [nvarchar](255) NULL,
	[RUT_COMPLETO] [nvarchar](255) NULL,
	[RUT] [float] NULL,
	[DV] [nvarchar](255) NULL,
	[NOMBRE] [nvarchar](255) NULL,
	[FAN] [datetime] NULL,
	[FCURSE] [datetime] NULL,
	[DIAS_MORA] [float] NULL,
	[CODE] [nvarchar](255) NULL,
	[GLOSA] [nvarchar](255) NULL,
	[RENEG_POR_NZA] [nvarchar](255) NULL,
	[EJECUTIVO_NEG] [nvarchar](255) NULL,
	[PRIORIDAD] [float] NULL,
	[NOM_BANCA] [nvarchar](255) NULL,
	[PRODUCTO] [nvarchar](255) NULL,
	[SOLICITUD] [float] NULL,
	[NOMBRE REGION] [nvarchar](255) NULL,
	[ACELERADO] [nvarchar](255) NULL,
	[FECHA DE ASIGNACION] [nvarchar](255) NULL,
	[FECHA DE RECEPCION] [nvarchar](255) NULL,
	[ABOGADO] [nvarchar](255) NULL,
	[ESTADO] [nvarchar](255) NULL,
	[OBSERVACION] [nvarchar](255) NULL,
	[ESTADO2] [nvarchar](255) NULL,
	[FECHA SOLICITUD] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DB_SMALL_REGIONES]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DB_SMALL_REGIONES](
	[INT_IDREGION] [int] IDENTITY(1,1) NOT NULL,
	[INT_CODREGION] [int] NOT NULL,
	[STR_REGION] [varchar](100) NULL,
 CONSTRAINT [PK_DB_SMALL_REGIONES] PRIMARY KEY CLUSTERED 
(
	[INT_IDREGION] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DEVOLUCION$]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DEVOLUCION$](
	[Operación Original] [nvarchar](255) NULL,
	[Fecha Registro] [datetime] NULL,
	[Custodia Origen] [nvarchar](255) NULL,
	[Custodia Destino] [nvarchar](255) NULL,
	[Usuario] [nvarchar](255) NULL,
	[Tipo Movimiento] [nvarchar](255) NULL,
	[Tipo Salida] [nvarchar](255) NULL,
	[Motivo] [nvarchar](255) NULL,
	[Lote] [float] NULL,
	[Id Pagaré] [float] NULL,
	[Operación Original1] [nvarchar](255) NULL,
	[Organización] [nvarchar](255) NULL,
	[Código único] [nvarchar](255) NULL,
	[Estado Documento] [nvarchar](255) NULL,
	[Rut] [nvarchar](255) NULL,
	[Nombre] [nvarchar](255) NULL,
	[folio_doc] [nvarchar](255) NULL,
	[Oficina] [nvarchar](255) NULL,
	[Region] [nvarchar](255) NULL,
	[Producto] [nvarchar](255) NULL,
	[Identif] [float] NULL,
	[Descripcion] [nvarchar](255) NULL,
	[Subbanca] [nvarchar](255) NULL,
	[Fecha Asignacion] [datetime] NULL,
	[Monto] [float] NULL,
	[Moneda] [nvarchar](255) NULL,
	[Abogado] [nvarchar](255) NULL,
	[Fecha Recepcion] [datetime] NULL,
	[Fecha Salida] [datetime] NULL,
	[Motivo Salida] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hist_pruebas]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hist_pruebas](
	[id_ope_hist] [varchar](10) NULL,
	[hist_des] [varchar](30) NULL,
	[hist_fecha_ejecucion] [varchar](10) NULL,
	[hist_numero_hito] [varchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hito_pruebas]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hito_pruebas](
	[id_ope_hito] [varchar](10) NULL,
	[hito_des] [varchar](30) NULL,
	[fecha_ejecucion] [varchar](10) NULL,
	[hito_numero_hito] [varchar](10) NULL,
	[estado_op] [varchar](1) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].['NOMINA ASIGNACION$']    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].['NOMINA ASIGNACION$'](
	[Operación Vencida] [nvarchar](255) NULL,
	[Pagare] [float] NULL,
	[Org#] [nvarchar](255) NULL,
	[Oficina] [nvarchar](255) NULL,
	[RUT_Deudor] [nvarchar](255) NULL,
	[Nombre Deudor] [nvarchar](255) NULL,
	[Tipo Operación] [nvarchar](255) NULL,
	[Operación Vencida1] [nvarchar](255) NULL,
	[Fecha de Asignación (FABOG)] [datetime] NULL,
	[Cód# Abogado] [nvarchar](255) NULL,
	[Fecha 1er Vcto#] [datetime] NULL,
	[Segmento] [nvarchar](255) NULL,
	[Monto] [float] NULL,
	[Moneda] [nvarchar](255) NULL,
	[Nombre Aval] [nvarchar](255) NULL,
	[RUT Aval] [nvarchar](255) NULL,
	[Garantía] [nvarchar](255) NULL,
	[Tipo Garantía] [nvarchar](255) NULL,
	[Fogape] [nvarchar](255) NULL,
	[Corfo] [nvarchar](255) NULL,
	[Operación Original] [nvarchar](255) NULL,
	[Deuda Directa] [float] NULL,
	[Fecha de Recepción] [datetime] NULL,
	[Fecha Visado] [datetime] NULL,
	[Provincia] [nvarchar](255) NULL,
	[Tipo Mandato] [nvarchar](255) NULL,
	[Fecha Mandato] [nvarchar](255) NULL,
	[Fecha Protesto] [nvarchar](255) NULL,
	[Fecha Curse] [datetime] NULL,
	[F30] [nvarchar](255) NULL,
	[F31] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].['NOMINA DOCUMENTO$']    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].['NOMINA DOCUMENTO$'](
	[Oficina] [float] NULL,
	[Region] [nvarchar](255) NULL,
	[Producto] [nvarchar](255) NULL,
	[TIPOCRED] [float] NULL,
	[Identif] [float] NULL,
	[op_Orig] [nvarchar](255) NULL,
	[Operacion] [float] NULL,
	[Subbanca] [nvarchar](255) NULL,
	[folio_Doc] [nvarchar](255) NULL,
	[Rut] [nvarchar](255) NULL,
	[Nombre] [nvarchar](255) NULL,
	[fing_uapc] [datetime] NULL,
	[Estado_Doc] [nvarchar](255) NULL,
	[Descrip] [nvarchar](255) NULL,
	[organizacion] [nvarchar](255) NULL,
	[Fecha Arqueo] [datetime] NULL,
	[Código único] [nvarchar](255) NULL,
	[ING_DEV] [nvarchar](255) NULL,
	[resp_lote_uapc] [nvarchar](255) NULL,
	[Usuario] [nvarchar](255) NULL,
	[Tipo Salida] [nvarchar](255) NULL,
	[Lote] [float] NULL,
	[Creado En Proceso] [nvarchar](255) NULL,
	[Fecha Carga] [datetime] NULL,
	[F25] [nvarchar](255) NULL,
	[Fecha Mandato] [datetime] NULL,
	[Fecha Protesto] [datetime] NULL,
	[Fecha Curse] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RESUMEN$]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RESUMEN$](
	[F1] [float] NULL,
	[F2] [nvarchar](255) NULL,
	[Valores] [float] NULL,
	[F4] [money] NULL,
	[F5] [money] NULL,
	[Cuenta de OP_ORIG] [float] NULL,
	[F7] [nvarchar](255) NULL,
	[ESTADO] [float] NULL,
	[F9] [float] NULL,
	[F10] [float] NULL,
	[F11] [nvarchar](255) NULL,
	[F12] [nvarchar](255) NULL,
	[F13] [nvarchar](255) NULL,
	[Valores1] [float] NULL,
	[F15] [money] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sisboj_tbl_hito_jud_bci_historico]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sisboj_tbl_hito_jud_bci_historico](
	[di_respaldo] [int] IDENTITY(1,1) NOT NULL,
	[di_Iddecarga] [int] NULL,
	[UBICACION] [varchar](11) NOT NULL,
	[OFICINA] [varchar](100) NULL,
	[N_OF] [int] NULL,
	[REG] [varchar](3) NULL,
	[OP_ORIG] [varchar](15) NULL,
	[OPERACION_2] [char](12) NULL,
	[TIPO_OP] [varchar](20) NOT NULL,
	[MONTO] [float] NULL,
	[BANCA] [nvarchar](255) NULL,
	[RUT_COMPLETO] [nvarchar](10) NULL,
	[RUT] [bigint] NULL,
	[DV] [nvarchar](1) NULL,
	[NOMBRE] [nvarchar](255) NULL,
	[FAN] [smalldatetime] NULL,
	[FCURSE] [char](10) NOT NULL,
	[DIAS_MORA] [int] NULL,
	[CODE] [varchar](50) NULL,
	[GLOSA] [varchar](50) NULL,
	[RENEG_POR_NZA] [varchar](2) NOT NULL,
	[EJECUTIVO_NEG] [char](10) NULL,
	[PRIORIDAD] [int] NOT NULL,
	[NOM_BANCA] [varchar](6) NOT NULL,
	[PRODUCTO] [varchar](9) NOT NULL,
	[SOLICITUD] [varchar](200) NULL,
	[nombre_region] [varchar](100) NULL,
	[Fecha_ejecucion] [smalldatetime] NULL,
	[PRIORIDAD_PYME] [varchar](100) NULL,
	[USUARIO_GENERA] [varchar](200) NULL,
	[hito_especial] [int] NULL,
	[acelerado] [int] NULL,
	[fec_carga_boj] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_di_respaldo] PRIMARY KEY CLUSTERED 
(
	[di_respaldo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Abogado]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_Abogado](
	[di_IdAbogado] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdUsuario] [int] NOT NULL,
	[di_fk_IdZona] [int] NOT NULL,
	[di_fk_Id_EstJur] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Abogado_di_IdAbogado] PRIMARY KEY CLUSTERED 
(
	[di_IdAbogado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Banca]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Banca](
	[dv_idBanca] [varchar](5) NOT NULL,
	[dv_Banca] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tb_Sys_Banca] PRIMARY KEY CLUSTERED 
(
	[dv_idBanca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Carga]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_Carga](
	[di_idCarga] [int] IDENTITY(1,1) NOT NULL,
	[ddt_FechaCarga] [datetime] NULL,
	[di_NumeroOperacion] [int] NULL,
	[di_NumeroRuts] [int] NULL,
	[di_fk_IdCargaRciRso] [int] NULL,
	[ddt_FechaRegistro] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_Carga] PRIMARY KEY CLUSTERED 
(
	[di_idCarga] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_CargaConfigProducto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_CargaConfigProducto](
	[di_IdCargaConfigProd] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdCarga] [int] NULL,
	[di_fk_IdConfProd] [int] NULL,
 CONSTRAINT [PK_tb_Sys_CargaConfigProducto] PRIMARY KEY CLUSTERED 
(
	[di_IdCargaConfigProd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Cargo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Cargo](
	[di_IdCargo] [int] NOT NULL,
	[dv_Cargo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdCargo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Cedentes]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Cedentes](
	[di_IdCedente] [int] IDENTITY(1,1) NOT NULL,
	[dv_Cedente] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdCedente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Cenco]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Cenco](
	[di_IdCenco] [int] NOT NULL,
	[dv_Cenco] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdCenco] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Cliente]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_Cliente](
	[di_IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdPersona] [int] NOT NULL,
	[di_fk_Rut] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Cliente_di_IdCliente] PRIMARY KEY CLUSTERED 
(
	[di_IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Comunas]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Comunas](
	[di_IdComunas] [int] NOT NULL,
	[dv_Comunas] [varchar](100) NULL,
	[di_fk_IdProvincia] [int] NOT NULL,
	[di_fk_IdRegion] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Comunas] PRIMARY KEY CLUSTERED 
(
	[di_IdComunas] ASC,
	[di_fk_IdProvincia] ASC,
	[di_fk_IdRegion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_ConfigCarga]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_ConfigCarga](
	[di_IdConfig] [int] IDENTITY(1,1) NOT NULL,
	[di_PeriodoDias] [int] NULL,
	[dv_undMed] [varchar](12) NULL,
	[ddt_horaCargaCron] [time](7) NULL,
	[dv_desc] [varchar](50) NULL,
	[dv_glosa] [varchar](50) NULL,
	[ddt_fechaCreacion] [datetime] NULL,
	[ddt_fActualia] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_ConfigCarga] PRIMARY KEY CLUSTERED 
(
	[di_IdConfig] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_ConfigProducto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_ConfigProducto](
	[di_IdConfProd] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdConfig] [int] NULL,
	[di_fk_IdProducto] [int] NULL,
	[bool_flag_activo] [bit] NULL,
 CONSTRAINT [PK_tb_Sys_ConfigProducto] PRIMARY KEY CLUSTERED 
(
	[di_IdConfProd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_ContrAcesso]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_ContrAcesso](
	[di_IdAcceso] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdUsuario] [int] NULL,
	[ddt_FecReg] [datetime] NULL,
	[dv_DircIpUser] [varchar](40) NULL,
	[dbit_EstadoLog] [bit] NULL,
 CONSTRAINT [PK_tb_Sys_ContrAcesso_di_IdAcceso] PRIMARY KEY CLUSTERED 
(
	[di_IdAcceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_ContratoPagare]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_ContratoPagare](
	[di_IdContrPag] [int] IDENTITY(1,1) NOT NULL,
	[dv_ContrPag] [varchar](50) NULL,
	[di_fk_IdTipoDcto] [int] NULL,
	[di_IdentiBoj] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdContrPag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_ContVistAcces]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_ContVistAcces](
	[di_IdContVistAcces] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdVistaControl] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_ContVistAcces_di_IdContVistAcces] PRIMARY KEY CLUSTERED 
(
	[di_IdContVistAcces] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sys_custodia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_sys_custodia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dv_cod_custodia] [varchar](15) NULL,
	[dv_descripcion] [varchar](80) NULL,
 CONSTRAINT [PK_tb_sys_custodia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Dependencia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Dependencia](
	[di_IdDependencia] [int] NOT NULL,
	[dv_Dependencia] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdDependencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_DetHito]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_DetHito](
	[di_IdDetHito] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdProduc] [int] NOT NULL,
	[di_IdHito] [int] NOT NULL,
	[di_fk_IdOpe] [int] NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_Prioridad] [int] NOT NULL,
	[dv_PrioridadPyme] [varchar](50) NOT NULL,
	[ddt_fechaCreacion] [datetime] NOT NULL,
	[dv_SolicitudOrig] [varchar](150) NULL,
	[di_fk_IdEstado] [int] NULL,
 CONSTRAINT [PK_tb_Sys_DetHito] PRIMARY KEY CLUSTERED 
(
	[di_IdDetHito] ASC,
	[di_fk_IdOpe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_DetLotDoc]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_DetLotDoc](
	[di_IdDetLotDoc] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdDcto] [int] NULL,
	[di_fk_IdDetLote] [int] NULL,
 CONSTRAINT [PK_tb_Sys_DetLotDoc] PRIMARY KEY CLUSTERED 
(
	[di_IdDetLotDoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_DetLote]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_DetLote](
	[di_IdDetLote] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdLote] [int] NOT NULL,
	[di_fk_IdOper] [int] NOT NULL,
	[di_fk_IdDetSol] [int] NOT NULL,
	[di_fk_IdEstado] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_DetLote_di_IdDetLote] PRIMARY KEY CLUSTERED 
(
	[di_IdDetLote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_DetSetDocs]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_DetSetDocs](
	[di_IdDetSetDoc] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdSetDoc] [int] NULL,
	[di_fk_IdTipDcto] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_DetSolicitud]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_DetSolicitud](
	[di_IdDetSol] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdSolicitud] [int] NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_fk_IdDetHito] [int] NOT NULL,
	[di_fk_IdDcto] [int] NULL,
	[di_fk_IdOper] [int] NULL,
	[di_fk_IdEstado] [int] NULL,
	[ddt_fechaIngreso] [datetime] NULL,
	[dv_EstadoActual] [varchar](200) NULL,
 CONSTRAINT [PK_tb_Sys_DetSolicitud_di_IdDetSol] PRIMARY KEY CLUSTERED 
(
	[di_IdDetSol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_sys_DetSolicitud_Hist]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_DetSolicitud_Hist](
	[di_idDetSolHist] [int] NOT NULL,
	[di_idDetSol] [int] NOT NULL,
	[di_fk_IdSolicitud] [int] NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_fk_IdDetHito] [int] NOT NULL,
	[di_fk_IdDcto] [int] NULL,
	[di_fk_IdOper] [int] NULL,
	[di_fk_IdEstado] [int] NULL,
	[ddt_fechaIngreso_DetSol] [datetime] NOT NULL,
	[ddt_fechaRespaldo] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_sys_DetSolicitud_Hist_1] PRIMARY KEY CLUSTERED 
(
	[di_idDetSolHist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_DetTipoDcto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_DetTipoDcto](
	[di_IdDetTipDcto] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdTipDcto] [int] NOT NULL,
	[dv_DetTipDcto] [varchar](50) NOT NULL,
	[ddt_FecIngreso] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_DetTipoDcto] PRIMARY KEY CLUSTERED 
(
	[di_IdDetTipDcto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Devolucion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Devolucion](
	[di_IdDevolucion] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdOper] [int] NOT NULL,
	[di_fk_IdTipoDev] [int] NOT NULL,
	[di_fk_IdMotivoDev] [int] NOT NULL,
	[di_fk_IdSolOrigen] [int] NOT NULL,
	[di_fk_IdSolActual] [int] NOT NULL,
	[di_fk_IdEstadoDev] [int] NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[dv_Observacion] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_DireccionesPer]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_DireccionesPer](
	[di_IdDireccion] [int] IDENTITY(1,1) NOT NULL,
	[dv_Direccion] [varchar](250) NULL,
	[di_fk_IdComuna] [int] NULL,
	[di_fk_IdProvincia] [int] NULL,
	[di_fk_IdRegion] [int] NULL,
	[di_fk_IdPersona] [int] NOT NULL,
	[di_fk_Rut] [int] NOT NULL,
	[di_fk_IdFono] [int] NULL,
 CONSTRAINT [PK_tb_Sys_DireccionesPer] PRIMARY KEY CLUSTERED 
(
	[di_IdDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Documento]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Documento](
	[di_IdDcto] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_fk_TipoDcto] [int] NOT NULL,
	[ddt_FechaSalidaBov] [datetime] NULL,
	[di_fk_IdEstado] [int] NOT NULL,
	[dv_CodBarra] [varchar](150) NULL,
	[di_fk_IdOper] [int] NOT NULL,
	[db_checkOk] [bit] NOT NULL,
	[dv_Glosa] [varchar](200) NULL,
	[db_ApoderadoBanco] [bit] NULL,
	[ddt_FechaSolicitud] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Documento] PRIMARY KEY CLUSTERED 
(
	[di_IdDcto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Empresa]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Empresa](
	[di_IdEmpresa] [int] NOT NULL,
	[dv_Empresa] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Estado]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Estado](
	[di_IdEstado] [int] NOT NULL,
	[dv_NomEstado] [varchar](75) NULL,
	[dv_DetEstado] [varchar](100) NULL,
	[dv_Lugar] [varchar](50) NULL,
	[bit_Activo] [bit] NULL,
	[dv_CodEstado] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_Estado_di_IdEstado] PRIMARY KEY CLUSTERED 
(
	[di_IdEstado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_EstadoDev]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_EstadoDev](
	[di_IdEstadoDev] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomEstadoDev] [varchar](50) NOT NULL,
	[dv_CodEstadoDev] [varchar](20) NOT NULL,
	[dv_Descripcion] [varchar](200) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[bit_Activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_EstInterfazPJ]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_EstInterfazPJ](
	[INT_IdEstInter] [int] IDENTITY(1,1) NOT NULL,
	[INT_IdEstado] [int] NOT NULL,
	[INT_IdestInterEnc] [int] NOT NULL,
	[DDT_FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_EstInterfazPJ] PRIMARY KEY CLUSTERED 
(
	[INT_IdEstInter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_EstudioJuridico]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_EstudioJuridico](
	[di_IdEstJur] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdZona] [int] NOT NULL,
	[dv_NomEstJur] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_EstudioJuridico] PRIMARY KEY CLUSTERED 
(
	[di_IdEstJur] ASC,
	[di_fk_IdZona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Fondo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Fondo](
	[di_IdFondo] [int] NOT NULL,
	[dv_Fondo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdFondo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Gerencia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Gerencia](
	[di_IdGerencia] [int] NOT NULL,
	[dv_Gerencia] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdGerencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Hito]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_Hito](
	[di_IdHito] [int] IDENTITY(1,1) NOT NULL,
	[ddt_FechaCreacion] [datetime] NOT NULL,
	[di_fk_IdTipoHito] [int] NOT NULL,
	[dv_CodHito] [nchar](20) NULL,
	[di_fk_IdEstado] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Hito_di_IdHito] PRIMARY KEY CLUSTERED 
(
	[di_IdHito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_HitoProducto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_HitoProducto](
	[di_IdhitProd] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdHito] [int] NULL,
	[di_fk_IdProducto] [int] NULL,
	[di_fk_di_IdCargaConfigProd] [int] NULL,
	[di_CantidadDocumentos] [int] NULL,
 CONSTRAINT [PK_tb_Sys_HitoProducto] PRIMARY KEY CLUSTERED 
(
	[di_IdhitProd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Inter_Entrada]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Inter_Entrada](
	[di_IdOperInterEntr] [int] IDENTITY(1,1) NOT NULL,
	[dv_CodOperacion] [varchar](15) NULL,
	[dv_CodOperRelacionada] [varchar](15) NULL,
	[di_fk_IdCliente] [int] NULL,
	[di_fk_IdProduc] [int] NULL,
	[di_DiasMora] [int] NULL,
	[dm_SaldoInsoluto] [money] NULL,
	[df_SaldoInsoUF] [float] NULL,
	[dv_TipoMoneda] [varchar](6) NULL,
	[dv_fk_IdTipOpe] [varchar](6) NULL,
	[di_fk_IdOficina] [int] NULL,
	[di_fk_IdCedente] [int] NULL,
	[dv_fk_TipRegion] [varchar](3) NULL,
	[ddt_FechVencAct] [datetime] NULL,
	[ddt_FechCurse] [datetime] NULL,
	[ddt_FechUltimoPago] [datetime] NULL,
	[dv_fk_IdBanca] [varchar](5) NULL,
	[dv_ApodoBanca] [varchar](6) NULL,
	[dv_Garantia] [varchar](2) NULL,
	[dv_Origen] [varchar](6) NULL,
	[df_DtdD00] [int] NULL,
	[dv_ReneNza] [varchar](2) NULL,
	[dv_EjecutivoReneNza] [varchar](20) NULL,
	[di_SucursalNova] [int] NULL,
	[di_CanalVentaNova] [int] NULL,
	[dv_CodTipCreditoNova] [varchar](5) NULL,
	[dv_4012Nova] [varchar](1) NULL,
	[di_FaseNova] [int] NULL,
	[dv_CodeBci] [varchar](50) NULL,
	[dv_CodeGlosaBci] [varchar](50) NULL,
	[ddt_FechaIngreso] [int] NULL,
	[di_CantDctosEsperados] [int] NULL,
	[di_Prioridad] [int] NULL,
	[dv_PrioridadPyme] [varchar](50) NULL,
	[dv_SolicitudOrig] [varchar](150) NULL,
	[di_IdUbicacion] [int] NULL,
	[orig_ubicacion] [varchar](11) NULL,
	[orig_producto] [varchar](9) NULL,
	[orig_rut] [int] NULL,
	[orig_dv] [char](10) NULL,
	[ddt_fecIngreso] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Inter_Entrada] PRIMARY KEY CLUSTERED 
(
	[di_IdOperInterEntr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Inter_Entrada_hist]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Inter_Entrada_hist](
	[di_IdOperInterEntr_hist] [int] IDENTITY(1,1) NOT NULL,
	[di_IdOperInterEntr] [int] NOT NULL,
	[dv_CodOperacion] [varchar](15) NULL,
	[dv_CodOperRelacionada] [varchar](15) NULL,
	[di_fk_IdCliente] [int] NULL,
	[di_fk_IdProduc] [int] NULL,
	[di_DiasMora] [int] NULL,
	[dm_SaldoInsoluto] [money] NULL,
	[df_SaldoInsoUF] [float] NULL,
	[dv_TipoMoneda] [varchar](6) NULL,
	[dv_fk_IdTipOpe] [varchar](6) NULL,
	[di_fk_IdOficina] [int] NULL,
	[di_fk_IdCedente] [int] NULL,
	[dv_fk_TipRegion] [varchar](3) NULL,
	[ddt_FechVencAct] [datetime] NULL,
	[ddt_FechCurse] [datetime] NULL,
	[ddt_FechUltimoPago] [datetime] NULL,
	[dv_fk_IdBanca] [varchar](5) NULL,
	[dv_ApodoBanca] [varchar](6) NULL,
	[dv_Garantia] [varchar](2) NULL,
	[dv_Origen] [varchar](6) NULL,
	[df_DtdD00] [int] NULL,
	[dv_ReneNza] [varchar](2) NULL,
	[dv_EjecutivoReneNza] [varchar](20) NULL,
	[di_SucursalNova] [int] NULL,
	[di_CanalVentaNova] [int] NULL,
	[dv_CodTipCreditoNova] [varchar](5) NULL,
	[dv_4012Nova] [varchar](1) NULL,
	[di_FaseNova] [int] NULL,
	[dv_CodeBci] [varchar](50) NULL,
	[dv_CodeGlosaBci] [varchar](50) NULL,
	[ddt_FechaIngreso] [int] NULL,
	[di_CantDctosEsperados] [int] NULL,
	[di_Prioridad] [int] NULL,
	[dv_PrioridadPyme] [varchar](50) NULL,
	[dv_SolicitudOrig] [varchar](150) NULL,
	[di_IdUbicacion] [int] NULL,
	[orig_ubicacion] [varchar](11) NULL,
	[orig_producto] [varchar](9) NULL,
	[orig_rut] [int] NULL,
	[orig_dv] [char](10) NULL,
	[ddt_fecIngreso] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Inter_Entradahis] PRIMARY KEY CLUSTERED 
(
	[di_IdOperInterEntr_hist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_InterfazPJ_Det]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_InterfazPJ_Det](
	[INT_IdInterfaz_Det] [int] IDENTITY(1,1) NOT NULL,
	[INT_IdInterfaz_Enc] [int] NOT NULL,
	[STR_CodOperacion] [varchar](12) NOT NULL,
	[INT_IdOficina] [int] NOT NULL,
	[STR_Oficina] [varchar](50) NULL,
	[STR_Region] [varchar](12) NULL,
	[STR_Operacion] [varchar](12) NOT NULL,
	[STR_TipOperacion] [varchar](10) NOT NULL,
	[STR_RutCompleto] [varchar](20) NOT NULL,
	[STR_NombreCliente] [varchar](500) NOT NULL,
	[STR_Garantia] [varchar](500) NULL,
	[INT_SaldoInsoluto] [int] NOT NULL,
	[DDT_FechaVencimiento] [date] NOT NULL,
	[STR_Banca] [varchar](50) NOT NULL,
	[DDT_FechaCarga] [datetime] NOT NULL,
	[DDT_FechaCurse] [date] NOT NULL,
	[STR_OperacionOriginal] [varchar](12) NOT NULL,
	[STR_Ejecutivo] [varchar](150) NULL,
	[STR_Dtd] [varchar](50) NULL,
	[STR_Solicitud] [varchar](10) NULL,
	[INT_Prioridad] [int] NULL,
	[STR_EstOperacion] [varchar](50) NOT NULL,
	[STR_RutAbogado] [varchar](10) NOT NULL,
	[STR_RutContralor] [varchar](10) NOT NULL,
	[DDT_FechaAsig] [date] NOT NULL,
	[DDT_FechaEntrega] [date] NOT NULL,
	[DDT_FechaRecep] [date] NOT NULL,
	[STR_Cedente] [varchar](10) NOT NULL,
	[STR_MultiJuicio] [varchar](2) NOT NULL,
	[INT_Monto] [int] NULL,
	[INT_IdTipoMoneda] [int] NULL,
	[DDT_FecPrimVenCuot] [date] NOT NULL,
	[INT_CuotasPagadas] [int] NOT NULL,
	[INT_CuotasInpagas] [int] NOT NULL,
	[INT_SaldoCapital] [int] NOT NULL,
	[INT_Interes] [int] NOT NULL,
	[INT_SaldoCapitalInsoluto] [int] NOT NULL,
	[INT_MoraTotalImpaga] [int] NOT NULL,
	[INT_PlazoPactado] [int] NOT NULL,
	[FLO_TasaSpread] [float] NOT NULL,
	[DDT_FechaPrimVencImpago] [date] NOT NULL,
	[STR_Comuna] [varchar](30) NOT NULL,
	[INT_MontoEnPesos] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_InterfazPJ_Det] PRIMARY KEY CLUSTERED 
(
	[INT_IdInterfaz_Det] ASC,
	[INT_IdInterfaz_Enc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_InterfazPJ_Enc]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_InterfazPJ_Enc](
	[INT_IdInterfaz_Enc] [int] IDENTITY(1,1) NOT NULL,
	[DDT_FechaCarga] [timestamp] NULL,
	[INT_CantRegCargados] [int] NULL,
 CONSTRAINT [PK_tb_Sys_InterfazPJ_Enc] PRIMARY KEY CLUSTERED 
(
	[INT_IdInterfaz_Enc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_JerarquiaSol]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_JerarquiaSol](
	[di_IdJerarquia] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdSolicitudPadre] [int] NOT NULL,
	[di_fk_IdSolicitudHijo] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Lote]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Lote](
	[di_IdLote] [int] IDENTITY(1,1) NOT NULL,
	[dv_CodLote] [varchar](10) NOT NULL,
	[dv_CodBarra] [varchar](150) NOT NULL,
	[di_IdUbi_Salida] [int] NOT NULL,
	[di_fk_IdEstado] [int] NOT NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[dv_GlosaLote] [varchar](200) NULL,
	[di_CantDctos] [int] NOT NULL,
	[di_IdUbi_Llegada] [int] NULL,
	[dv_CodQR] [nchar](10) NULL,
	[di_fk_IdSolicitud] [int] NULL,
	[di_CantOper] [int] NULL,
 CONSTRAINT [PK_tb_Sys_Lote] PRIMARY KEY CLUSTERED 
(
	[di_IdLote] ASC,
	[dv_CodLote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Modulo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Modulo](
	[di_IdModulo] [int] IDENTITY(1,1) NOT NULL,
	[dv_Codigo] [varchar](10) NULL,
	[dv_Desc] [nchar](10) NULL,
 CONSTRAINT [PK_tb_Sys_Modulo] PRIMARY KEY CLUSTERED 
(
	[di_IdModulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Modulo_AccesoTipoReparo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_Modulo_AccesoTipoReparo](
	[di_IdMod_AccTipRep] [int] NULL,
	[di_fk_IdTipoReparo] [int] NULL,
	[di_fk_IdModulo] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_MotivoDevolucion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_MotivoDevolucion](
	[di_IdMotivoDev] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomMotivoDev] [varchar](50) NOT NULL,
	[dv_CodMotivoDev] [varchar](20) NOT NULL,
	[di_fk_TipoDev] [int] NOT NULL,
	[dv_Descripcion] [varchar](200) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[bit_Activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_MotivoReparo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_MotivoReparo](
	[di_IdReparo] [int] IDENTITY(1,1) NOT NULL,
	[dv_CodReparo] [varchar](12) NULL,
	[dv_DescReparo] [varchar](50) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[dbt_Activo] [bit] NOT NULL,
	[dbt_Eliminado] [bit] NOT NULL,
	[di_fk_IdTipoReparo] [int] NULL,
 CONSTRAINT [PK_tb_Sys_MotivoReparo] PRIMARY KEY CLUSTERED 
(
	[di_IdReparo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_MotivoReparoDocumento]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_MotivoReparoDocumento](
	[di_IdReparoDoc] [int] IDENTITY(1,1) NOT NULL,
	[dv_lugar_reparo] [varchar](25) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[di_fk_IdDoc] [int] NULL,
	[di_fk_IdMotReparo] [int] NULL,
 CONSTRAINT [PK_tb_Sys_MotivoReparoDocumento] PRIMARY KEY CLUSTERED 
(
	[di_IdReparoDoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_MotivoReparoLote]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_MotivoReparoLote](
	[di_IdReparoLote] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdLote] [int] NULL,
	[di_fk_IdReparo] [int] NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[dv_fk_codLote] [varchar](10) NULL,
	[dv_lugar_reparo] [varchar](25) NULL,
 CONSTRAINT [PK_tb_Sys_MotivoReparoLote] PRIMARY KEY CLUSTERED 
(
	[di_IdReparoLote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_MotRepDetSol]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_MotRepDetSol](
	[di_IdMotRepDetSol] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdDetSol] [int] NULL,
	[di_fk_IdReparo] [int] NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_MotRepDetSol] PRIMARY KEY CLUSTERED 
(
	[di_IdMotRepDetSol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Oficina]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Oficina](
	[di_Idcedente] [int] NOT NULL,
	[di_IdOficina] [int] NOT NULL,
	[dv_Oficina] [varchar](150) NULL,
	[dv_OficinaPrin] [varchar](150) NULL,
	[dv_RegNormaliza] [varchar](150) NULL,
	[dv_RegBanco] [varchar](150) NULL,
	[dv_JefeRegNormaliza] [varchar](150) NULL,
	[dv_fk_IdTipReg] [varchar](3) NOT NULL,
	[dv_IdJefeRegNorma] [nvarchar](15) NULL,
	[dv_JefeResponsable] [nvarchar](150) NULL,
	[di_IdOficinaMype] [int] NULL,
	[dv_OficinaMype] [nvarchar](150) NULL,
	[dv_RegionMype] [nvarchar](150) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_oficina] PRIMARY KEY CLUSTERED 
(
	[di_Idcedente] ASC,
	[di_IdOficina] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Operacion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Operacion](
	[di_IdOperacion] [int] IDENTITY(1,1) NOT NULL,
	[dv_CodOperacion] [varchar](15) NOT NULL,
	[dv_CodOperRelacionada] [varchar](15) NULL,
	[di_fk_IdCliente] [int] NOT NULL,
	[di_fk_IdProduc] [int] NOT NULL,
	[di_IdEstado] [int] NULL,
	[di_DiasMora] [int] NULL,
	[dm_SaldoInsoluto] [money] NULL,
	[df_SaldoInsoUF] [float] NULL,
	[dv_TipoMoneda] [varchar](6) NULL,
	[dv_fk_IdTipOpe] [varchar](6) NULL,
	[di_fk_IdOficina] [int] NULL,
	[di_fk_IdCedente] [int] NULL,
	[dv_fk_TipRegion] [varchar](3) NULL,
	[ddt_FechVencAct] [datetime] NULL,
	[ddt_FechCurse] [datetime] NULL,
	[ddt_FechUltimoPago] [datetime] NULL,
	[dv_fk_IdBanca] [varchar](5) NULL,
	[dv_ApodoBanca] [varchar](6) NULL,
	[dv_Garantia] [varchar](2) NULL,
	[dv_Origen] [varchar](6) NULL,
	[df_DtdD00] [float] NULL,
	[dv_ReneNza] [varchar](2) NULL,
	[dv_EjecutivoReneNza] [varchar](20) NULL,
	[di_SucursalNova] [int] NULL,
	[di_CanalVentaNova] [int] NULL,
	[dv_CodTipCreditoNova] [varchar](5) NULL,
	[dv_4012Nova] [varchar](1) NULL,
	[di_FaseNova] [int] NULL,
	[dv_CodeBci] [varchar](50) NULL,
	[dv_CodeGlosaBci] [varchar](50) NULL,
	[ddt_FechaIngreso] [datetime] NULL,
	[ddt_FechaModif] [datetime] NULL,
	[bit_Nuevo] [bit] NULL,
 CONSTRAINT [PK_tb_Sys_Operacion] PRIMARY KEY CLUSTERED 
(
	[di_IdOperacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Operacion_Hist]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Operacion_Hist](
	[di_IdOperacion_Hist] [int] IDENTITY(1,1) NOT NULL,
	[di_IdOperacion] [int] NULL,
	[dv_CodOperacion] [varchar](15) NOT NULL,
	[dv_CodOperRelacionada] [varchar](15) NULL,
	[di_fk_IdCliente] [int] NOT NULL,
	[di_fk_IdProduc] [int] NOT NULL,
	[di_IdEstado] [int] NULL,
	[di_DiasMora] [int] NULL,
	[dm_SaldoInsoluto] [money] NULL,
	[df_SaldoInsoUF] [float] NULL,
	[dv_TipoMoneda] [varchar](6) NULL,
	[dv_fk_IdTipOpe] [varchar](6) NULL,
	[di_fk_IdOficina] [int] NULL,
	[di_fk_IdCedente] [int] NULL,
	[dv_fk_TipRegion] [varchar](3) NULL,
	[ddt_FechVencAct] [datetime] NULL,
	[ddt_FechCurse] [datetime] NULL,
	[ddt_FechUltimoPago] [datetime] NULL,
	[dv_fk_IdBanca] [varchar](5) NULL,
	[dv_ApodoBanca] [varchar](6) NULL,
	[dv_Garantia] [varchar](2) NULL,
	[dv_Origen] [varchar](6) NULL,
	[df_DtdD00] [float] NULL,
	[dv_ReneNza] [varchar](2) NULL,
	[dv_EjecutivoReneNza] [varchar](20) NULL,
	[di_SucursalNova] [int] NULL,
	[di_CanalVentaNova] [int] NULL,
	[dv_CodTipCreditoNova] [varchar](5) NULL,
	[dv_4012Nova] [varchar](1) NULL,
	[di_FaseNova] [int] NULL,
	[dv_CodeBci] [varchar](50) NULL,
	[dv_CodeGlosaBci] [varchar](50) NULL,
	[ddt_FechaIngresoBoj] [datetime] NULL,
	[ddt_FechaRespaldo] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_Operacion_Hist] PRIMARY KEY CLUSTERED 
(
	[di_IdOperacion_Hist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_OperacionesInhibidas]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_OperacionesInhibidas](
	[di_idInhibicion] [int] IDENTITY(1,1) NOT NULL,
	[ddt_fechaIngreso] [datetime] NOT NULL,
	[di_fk_idOpe] [int] NOT NULL,
	[dv_Detalle] [varchar](250) NOT NULL,
	[di_DiasInhibicion] [int] NOT NULL,
	[di_fk_idTipInhibicion] [int] NOT NULL,
	[bit_ativo] [bit] NOT NULL,
 CONSTRAINT [PK_tb_Sys_OperacionesInhibidas] PRIMARY KEY CLUSTERED 
(
	[di_idInhibicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_PerAcces]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Sys_PerAcces](
	[di_IdPerAcces] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdPerfil] [int] NOT NULL,
	[di_IdContVistAcces] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_PerAcces] PRIMARY KEY CLUSTERED 
(
	[di_IdPerAcces] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Perfiles]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Perfiles](
	[di_IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomPerfil] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_Perfiles_di_IdPerfil] PRIMARY KEY CLUSTERED 
(
	[di_IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Persona]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Persona](
	[di_IdPersona] [int] IDENTITY(1,1) NOT NULL,
	[di_Rut] [int] NOT NULL,
	[dc_DigVer] [char](1) NOT NULL,
	[dv_Nombre] [varchar](100) NULL,
	[dv_ApePater] [varchar](50) NULL,
	[dv_ApeMater] [varchar](50) NULL,
	[ddt_FechNac] [date] NULL,
	[dc_Sexo] [char](1) NULL,
	[dc_TipoPer] [char](1) NULL,
	[dv_Email] [varchar](80) NULL,
	[dv_Fallecido] [varchar](2) NULL,
 CONSTRAINT [PK_tb_Sys_Persona] PRIMARY KEY CLUSTERED 
(
	[di_IdPersona] ASC,
	[di_Rut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Posicion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Posicion](
	[di_IdPosicion] [int] NOT NULL,
	[dv_Posicion] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[di_IdPosicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Producto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Producto](
	[di_IdProduc] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomProduc] [varchar](75) NULL,
	[dv_CodProducto] [varchar](10) NULL,
	[dv_Activo] [bit] NULL,
 CONSTRAINT [PK_tb_Sys_Producto_di_IdProducto] PRIMARY KEY CLUSTERED 
(
	[di_IdProduc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_sys_ProductoTipoDocumento]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_ProductoTipoDocumento](
	[id_prodtipdoc] [int] IDENTITY(1,1) NOT NULL,
	[fk_idProducto] [int] NULL,
	[fk_idTipoDocto] [int] NULL,
	[registrado] [datetime] NULL,
	[bit_Activo] [bit] NULL,
	[obligatorio] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Provincias]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Provincias](
	[di_IdProvincia] [int] NOT NULL,
	[dv_Provincia] [varchar](100) NULL,
	[di_fk_IdRegion] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_Provincias] PRIMARY KEY CLUSTERED 
(
	[di_IdProvincia] ASC,
	[di_fk_IdRegion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Regiones]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Regiones](
	[di_IdRegion] [int] NOT NULL,
	[dv_Region] [varchar](100) NULL,
 CONSTRAINT [PK_tb_Sys_Regiones_di_IdRegion] PRIMARY KEY CLUSTERED 
(
	[di_IdRegion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_SetDocs]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_SetDocs](
	[di_IdSetDoc] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomSetDoc] [varchar](20) NULL,
	[ddt_FechaCrea] [datetime] NULL,
	[ddt_FechaMod] [datetime] NULL,
	[dv_Descripcion] [varchar](100) NULL,
	[di_fk_IdUbi] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Solicitud]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Solicitud](
	[di_IdSolicitud] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_fk_IdHito] [int] NOT NULL,
	[di_fk_IdEstado] [int] NOT NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[di_CantDctosSolicitados] [int] NULL,
	[dv_CodeBar] [varchar](30) NULL,
	[ddt_FechaCierre] [datetime] NULL,
	[ddt_FechaEnvio] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_Solicitud] PRIMARY KEY CLUSTERED 
(
	[di_IdSolicitud] ASC,
	[di_fk_IdUbi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_sys_Solicitud_Hist]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_Solicitud_Hist](
	[di_idSolicitudHist] [int] NOT NULL,
	[di_IdSolicitud] [int] NOT NULL,
	[di_fk_IdUbi] [int] NOT NULL,
	[di_fk_IdHito] [int] NOT NULL,
	[di_fk_IdEstado] [int] NOT NULL,
	[ddt_FechaCreacion] [datetime] NOT NULL,
	[di_CantDctosSolicitados] [int] NULL,
	[ddt_FechaRespaldo] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_sys_Solicitud_Hist] PRIMARY KEY CLUSTERED 
(
	[di_idSolicitudHist] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_TelefonosPer]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TelefonosPer](
	[di_IdFono] [int] IDENTITY(1,1) NOT NULL,
	[di_Fono] [varchar](20) NULL,
	[di_fk_IdPersona] [int] NOT NULL,
	[di_fk_Rut] [int] NOT NULL,
 CONSTRAINT [PK_tb_Sys_TelefonosPer] PRIMARY KEY CLUSTERED 
(
	[di_IdFono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones](
	[id] [int] NOT NULL,
	[operaciones] [varchar](50) NULL,
	[estado] [varchar](50) NULL,
	[registrado] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones.]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_tempCuntodiaEspadoOperaciones.](
	[id] [int] NOT NULL,
	[operaciones] [varchar](50) NULL,
	[estado] [varchar](50) NULL,
	[registrado] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipInhibicion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipInhibicion](
	[di_idTipInhibicion] [int] IDENTITY(1,1) NOT NULL,
	[ddt_fechaIngreso] [datetime] NOT NULL,
	[dv_Detalle] [varchar](250) NOT NULL,
	[bit_activo] [bit] NOT NULL,
 CONSTRAINT [PK_tb_Sys_TipInhibicion] PRIMARY KEY CLUSTERED 
(
	[di_idTipInhibicion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoDcto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoDcto](
	[di_IdTipDcto] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomTipDcto] [varchar](50) NULL,
	[di_IdTipDctoPJ] [int] NULL,
	[ddt_FecIngreso] [datetime] NOT NULL,
	[bit_Activo] [bit] NOT NULL,
 CONSTRAINT [PK_tb_Sys_TipoDcto_di_IdTipDcto] PRIMARY KEY CLUSTERED 
(
	[di_IdTipDcto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoDevolucion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoDevolucion](
	[di_IdTipoDev] [int] IDENTITY(1,1) NOT NULL,
	[dv_NomTipoDev] [varchar](50) NOT NULL,
	[dv_CodTipoDev] [varchar](20) NOT NULL,
	[dv_Descripcion] [varchar](200) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaActualiza] [datetime] NULL,
	[bit_Activo] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoHito]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoHito](
	[di_idTipoHito] [int] IDENTITY(1,1) NOT NULL,
	[dv_CodHito] [varchar](12) NULL,
	[dv_dsc] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_TipoHito] PRIMARY KEY CLUSTERED 
(
	[di_idTipoHito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoMoneda]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoMoneda](
	[INT_IdTipoMoneda] [int] IDENTITY(1,1) NOT NULL,
	[STR_TipoMoneda] [varchar](50) NULL,
	[DDT_FechaCreacion] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_TipoMoneda] PRIMARY KEY CLUSTERED 
(
	[INT_IdTipoMoneda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoOperacion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoOperacion](
	[id_IdTipoOper] [int] IDENTITY(1,1) NOT NULL,
	[dv_IdTipOpe] [varchar](6) NOT NULL,
	[dv_DetTipOperBOJ] [varchar](150) NULL,
	[di_CantDctosEsp] [int] NULL,
	[ddt_FecIngreso] [datetime] NOT NULL,
	[bit_Activo] [bit] NOT NULL,
 CONSTRAINT [PK_tb_Sys_TipoOperacion] PRIMARY KEY CLUSTERED 
(
	[dv_IdTipOpe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipoReparo]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipoReparo](
	[di_IdTipoReparo] [int] IDENTITY(1,1) NOT NULL,
	[dv_Descripcion] [varchar](15) NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[ddt_FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_tb_Sys_TipoReparo] PRIMARY KEY CLUSTERED 
(
	[di_IdTipoReparo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TipRegion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TipRegion](
	[di_IdTipReg] [varchar](3) NOT NULL,
	[dv_TipRego] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_TipRegion] PRIMARY KEY CLUSTERED 
(
	[di_IdTipReg] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_TOper_DetTDcto]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_TOper_DetTDcto](
	[di_IdRelacion] [int] IDENTITY(1,1) NOT NULL,
	[dv_fk_IdTipOper] [varchar](6) NOT NULL,
	[di_fk_IdDetTipDcto] [int] NOT NULL,
	[ddt_fecIngreso] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_Sys_TOper_DetTDcto] PRIMARY KEY CLUSTERED 
(
	[di_IdRelacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Traking]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Traking](
	[di_IdTracking] [int] IDENTITY(1,1) NOT NULL,
	[ddt_fechaIngreso] [datetime] NOT NULL,
	[dv_CodOper] [varchar](15) NULL,
	[di_rut] [int] NOT NULL,
	[dc_digVer] [char](1) NOT NULL,
	[dv_NombreCliente] [varchar](200) NULL,
	[di_IdDcto] [int] NULL,
	[dv_TipoDcto] [varchar](8) NULL,
	[di_IdUbiOrigen] [int] NULL,
	[di_IdUserOrigen] [int] NULL,
	[ddt_fechaInicioTraking] [datetime] NULL,
	[di_IdUbiDestino] [int] NULL,
	[di_IdUserReceptor] [int] NULL,
	[ddt_FechaTerminoTraking] [datetime] NULL,
 CONSTRAINT [PK__tb_Sys_T__477120526AE13606] PRIMARY KEY CLUSTERED 
(
	[di_IdTracking] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_sys_TrakingAcciones]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_TrakingAcciones](
	[di_IdTrakingAc] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tb_sys_TrakingAcciones_1] PRIMARY KEY CLUSTERED 
(
	[di_IdTrakingAc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_sys_TrakingEstado]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_TrakingEstado](
	[di_IdTrakingEst] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tb_sys_TrakingEstado] PRIMARY KEY CLUSTERED 
(
	[di_IdTrakingEst] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Transacciones]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Transacciones](
	[di_IdTrans] [int] IDENTITY(1,1) NOT NULL,
	[dv_TablaAfectada] [varchar](60) NULL,
	[dv_Accion] [varchar](50) NULL,
	[ddt_FechaReg] [datetime] NULL,
	[dv_Usuario] [varchar](50) NULL,
	[dv_Glosa] [varchar](150) NULL,
 CONSTRAINT [PK_Transacciones_di_IdTrans] PRIMARY KEY CLUSTERED 
(
	[di_IdTrans] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Ubicacion]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Ubicacion](
	[di_IdUbicacion] [int] IDENTITY(1,1) NOT NULL,
	[dv_DetUbi] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_Ubicacion_di_IdUbicacion] PRIMARY KEY CLUSTERED 
(
	[di_IdUbicacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_sys_ubicacion_custodia]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_sys_ubicacion_custodia](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fk_id_ubicacion] [int] NULL,
	[fk_id_custodia] [int] NULL,
 CONSTRAINT [PK_tb_sys_ubicacion_custodia] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_Sys_Usuario]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Usuario](
	[di_IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[di_fk_IdPersona] [int] NOT NULL,
	[di_fk_Rut] [int] NOT NULL,
	[dv_AliasUser] [varchar](50) NULL,
	[dv_PassUser] [varchar](50) NULL,
	[di_fk_IdPerAcces] [int] NULL,
	[di_IdUsuarioJefe] [int] NULL,
	[ddt_FechaCreacion] [datetime] NULL,
	[di_fk_IdCenco] [int] NULL,
	[di_fk_IdEmpresa] [int] NULL,
	[di_fk_IdGerencia] [int] NULL,
	[di_fk_IdFondo] [int] NULL,
	[di_fk_IdDependencia] [int] NULL,
	[di_fk_IdPosicion] [int] NULL,
	[di_fk_IdCargo] [int] NULL,
	[dv_UserActivo] [varchar](2) NULL,
 CONSTRAINT [PK_tb_Sys_Usuario_di_IdUsuario] PRIMARY KEY CLUSTERED 
(
	[di_IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_VistaControl]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_VistaControl](
	[di_IDVistaControl] [int] IDENTITY(1,1) NOT NULL,
	[dv_IndexVista] [varchar](50) NOT NULL,
	[dv_IndexControl] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tb_Sys_VistaControl _di_IDVistaControl] PRIMARY KEY CLUSTERED 
(
	[di_IDVistaControl] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Sys_Zona]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Sys_Zona](
	[di_IdZona] [int] NOT NULL,
	[dv_NomZona] [varchar](50) NULL,
 CONSTRAINT [PK_tb_Sys_Zona_di_IdZona] PRIMARY KEY CLUSTERED 
(
	[di_IdZona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_usuarios_ldap]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_usuarios_ldap](
	[id] [int] NOT NULL,
	[ip] [nvarchar](50) NULL,
	[usuario] [nvarchar](20) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_webnza_Cedentes]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_webnza_Cedentes](
	[fld_IdCedente] [int] IDENTITY(1,1) NOT NULL,
	[fld_Cedente] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_webnza_Cedentes] PRIMARY KEY CLUSTERED 
(
	[fld_IdCedente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_webnza_Informes]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_webnza_Informes](
	[fld_IdInforme] [int] IDENTITY(1,1) NOT NULL,
	[fld_Infome] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_webnza_Informes] PRIMARY KEY CLUSTERED 
(
	[fld_IdInforme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_webnza_NombreRegionalesInf]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_webnza_NombreRegionalesInf](
	[fld_IdNomRegInforme] [int] IDENTITY(1,1) NOT NULL,
	[fld_NomRegInforme] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tbl_webnza_ReginalesInformes] PRIMARY KEY CLUSTERED 
(
	[fld_IdNomRegInforme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_webnza_Regionales]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_webnza_Regionales](
	[fld_IdRegional] [int] IDENTITY(1,1) NOT NULL,
	[fld_Regional] [varchar](50) NOT NULL,
	[fld_fk_IdCedente] [int] NOT NULL,
 CONSTRAINT [PK_tbl_webnza_Regionales] PRIMARY KEY CLUSTERED 
(
	[fld_IdRegional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_webnza_RegionalesInformes]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_webnza_RegionalesInformes](
	[fld_IdNomRegInf] [int] IDENTITY(1,1) NOT NULL,
	[fld_fk_IdRegional] [int] NOT NULL,
	[fld_fk_IdInforme] [int] NOT NULL,
	[fld_fk_IdNomRegInforme] [int] NOT NULL,
 CONSTRAINT [PK_tbl_webnza_Reg_equals_Informe] PRIMARY KEY CLUSTERED 
(
	[fld_IdNomRegInf] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp_identif_pagar]    Script Date: 09/01/2020 12:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_identif_pagar](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cod_producto] [varchar](100) NULL,
	[descripcion_prod] [varchar](100) NULL,
	[total] [varchar](100) NULL,
	[identif] [varchar](100) NULL,
	[descripcion_identif] [varchar](100) NULL,
	[cod_identif_sola7] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] ADD  CONSTRAINT [DF_tb_Sys_DetHito_di_Prioridad]  DEFAULT ((0)) FOR [di_Prioridad]
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] ADD  CONSTRAINT [DF_tb_Sys_DetHito_di_PrioridadPyme]  DEFAULT ((0)) FOR [dv_PrioridadPyme]
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] ADD  CONSTRAINT [DF_tb_Sys_DetHito_ddt_fechaCreacion]  DEFAULT (getdate()) FOR [ddt_fechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_DetSolicitud] ADD  CONSTRAINT [DF_tb_Sys_DetSolicitud_ddt_fechaIngreso]  DEFAULT (getdate()) FOR [ddt_fechaIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_DetSolicitud] ADD  DEFAULT ('Sin Informacion') FOR [dv_EstadoActual]
GO
ALTER TABLE [dbo].[tb_sys_DetSolicitud_Hist] ADD  CONSTRAINT [DF_tb_sys_detSolicitud_Hist_ddt_fechaIngreso]  DEFAULT (getdate()) FOR [ddt_fechaIngreso_DetSol]
GO
ALTER TABLE [dbo].[tb_sys_DetSolicitud_Hist] ADD  CONSTRAINT [DF_tb_sys_DetSolicitud_Hist_ddt_fechaRespaldo_1]  DEFAULT (getdate()) FOR [ddt_fechaRespaldo]
GO
ALTER TABLE [dbo].[tb_Sys_DetTipoDcto] ADD  CONSTRAINT [DF_tb_Sys_DetTipoDcto_ddt_FecIngreso]  DEFAULT (getdate()) FOR [ddt_FecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_Devolucion] ADD  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_Documento] ADD  CONSTRAINT [DF_tb_Sys_Documento_ddt_FechaSolicitud]  DEFAULT (getdate()) FOR [ddt_FechaSolicitud]
GO
ALTER TABLE [dbo].[tb_Sys_Estado] ADD  DEFAULT ((1)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_EstadoDev] ADD  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_EstadoDev] ADD  DEFAULT ((0)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_EstInterfazPJ] ADD  CONSTRAINT [DF_tb_Sys_EstInterfazPJ_DDT_FechaCreacion]  DEFAULT (getdate()) FOR [DDT_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_Hito] ADD  CONSTRAINT [DF_tb_Sys_Hito_ddt_FechaCreacion]  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_Inter_Entrada] ADD  CONSTRAINT [DF_tb_Sys_Inicio_ddt_fecIngreso]  DEFAULT (getdate()) FOR [ddt_fecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_Inter_Entrada_hist] ADD  CONSTRAINT [DF_tb_Sys_Inicio_ddt_fecIngresohis]  DEFAULT (getdate()) FOR [ddt_fecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_MotivoDevolucion] ADD  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_MotivoDevolucion] ADD  DEFAULT ((0)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_Oficina] ADD  CONSTRAINT [DF_tb_Sys_Oficina_ddt_FechaCreacion]  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] ADD  CONSTRAINT [DF_tb_Sys_Operacion_ddt_FechaIngreso]  DEFAULT (getdate()) FOR [ddt_FechaIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion_Hist] ADD  CONSTRAINT [DF_tb_Sys_Operacion_Hist_ddt_FechaRespaldo]  DEFAULT (getdate()) FOR [ddt_FechaRespaldo]
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas] ADD  CONSTRAINT [DF_tb_Sys_OperacionesInhibidas_ddt_fechaIngreso]  DEFAULT (getdate()) FOR [ddt_fechaIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas] ADD  CONSTRAINT [DF_tb_Sys_OperacionesInhibidas_bit_ativo]  DEFAULT ((1)) FOR [bit_ativo]
GO
ALTER TABLE [dbo].[tb_sys_ProductoTipoDocumento] ADD  CONSTRAINT [DF_tb_sys_ProductoTipoDocumento_registrado]  DEFAULT (getdate()) FOR [registrado]
GO
ALTER TABLE [dbo].[tb_Sys_SetDocs] ADD  DEFAULT (getdate()) FOR [ddt_FechaCrea]
GO
ALTER TABLE [dbo].[tb_Sys_SetDocs] ADD  DEFAULT (NULL) FOR [ddt_FechaMod]
GO
ALTER TABLE [dbo].[tb_Sys_Solicitud] ADD  CONSTRAINT [DF_tb_Sys_Solicitud_ddt_FechaCreacion]  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_Solicitud] ADD  CONSTRAINT [DF_tb_Sys_Solicitud_di_CantDctosSolicitados]  DEFAULT ((0)) FOR [di_CantDctosSolicitados]
GO
ALTER TABLE [dbo].[tb_Sys_Solicitud] ADD  DEFAULT ('N') FOR [dv_CodeBar]
GO
ALTER TABLE [dbo].[tb_sys_Solicitud_Hist] ADD  CONSTRAINT [DF_tb_sys_Solicitud_Hist_ddt_FechaCreacion]  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_sys_Solicitud_Hist] ADD  CONSTRAINT [DF_tb_sys_Solicitud_Hist_di_CantDctosSolicitados]  DEFAULT ((0)) FOR [di_CantDctosSolicitados]
GO
ALTER TABLE [dbo].[tb_sys_Solicitud_Hist] ADD  CONSTRAINT [DF_tb_sys_Solicitud_Hist_ddt_FechaRespaldo]  DEFAULT (getdate()) FOR [ddt_FechaRespaldo]
GO
ALTER TABLE [dbo].[tb_Sys_TipInhibicion] ADD  CONSTRAINT [DF_tb_Sys_TipInhibicion_ddt_fechaIngreso]  DEFAULT (getdate()) FOR [ddt_fechaIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_TipInhibicion] ADD  CONSTRAINT [DF_tb_Sys_TipInhibicion_bit_activo]  DEFAULT ((1)) FOR [bit_activo]
GO
ALTER TABLE [dbo].[tb_Sys_TipoDcto] ADD  CONSTRAINT [DF_tb_Sys_TipoDcto_ddt_FecIngreso]  DEFAULT (getdate()) FOR [ddt_FecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_TipoDcto] ADD  CONSTRAINT [DF_tb_Sys_TipoDcto_bit_Activo]  DEFAULT ((0)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_TipoDevolucion] ADD  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tb_Sys_TipoDevolucion] ADD  DEFAULT ((0)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_TipoOperacion] ADD  CONSTRAINT [DF__tb_Sys_Ti__di_Ca__3D2915A8]  DEFAULT ((0)) FOR [di_CantDctosEsp]
GO
ALTER TABLE [dbo].[tb_Sys_TipoOperacion] ADD  CONSTRAINT [DF_tb_Sys_TipoOperacion_ddt_FecIngreso]  DEFAULT (getdate()) FOR [ddt_FecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_TipoOperacion] ADD  CONSTRAINT [DF_tb_Sys_TipoOperacion_bit_Activo]  DEFAULT ((1)) FOR [bit_Activo]
GO
ALTER TABLE [dbo].[tb_Sys_TOper_DetTDcto] ADD  CONSTRAINT [DF_tb_Sys_TOper_DetTDcto_ddt_fecIngreso]  DEFAULT (getdate()) FOR [ddt_fecIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_Traking] ADD  CONSTRAINT [DF_tb_Sys_Traking_ddt_fechaIngreso]  DEFAULT (getdate()) FOR [ddt_fechaIngreso]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] ADD  CONSTRAINT [DF_tb_Sys_Usuario_ddt_FechaCreacion]  DEFAULT (getdate()) FOR [ddt_FechaCreacion]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] ADD  DEFAULT ((0)) FOR [fld_fk_IdRegional]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] ADD  DEFAULT ((0)) FOR [fld_fk_IdInforme]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] ADD  DEFAULT ((0)) FOR [fld_fk_IdNomRegInforme]
GO
ALTER TABLE [dbo].[areatrabajo_custodia]  WITH CHECK ADD  CONSTRAINT [FK_areatrabajo_custodia_tb_sys_custodia] FOREIGN KEY([fk_id_custodia])
REFERENCES [dbo].[tb_sys_custodia] ([id])
GO
ALTER TABLE [dbo].[areatrabajo_custodia] CHECK CONSTRAINT [FK_areatrabajo_custodia_tb_sys_custodia]
GO
ALTER TABLE [dbo].[tb_Sys_Abogado]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Abogado] FOREIGN KEY([di_fk_Id_EstJur], [di_fk_IdZona])
REFERENCES [dbo].[tb_Sys_EstudioJuridico] ([di_IdEstJur], [di_fk_IdZona])
GO
ALTER TABLE [dbo].[tb_Sys_Abogado] CHECK CONSTRAINT [FK_tb_Sys_Abogado]
GO
ALTER TABLE [dbo].[tb_Sys_Abogado]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Abogado_tb_Sys_Usuario_di_IdUsuario] FOREIGN KEY([di_fk_IdUsuario])
REFERENCES [dbo].[tb_Sys_Usuario] ([di_IdUsuario])
GO
ALTER TABLE [dbo].[tb_Sys_Abogado] CHECK CONSTRAINT [FK_tb_Sys_Abogado_tb_Sys_Usuario_di_IdUsuario]
GO
ALTER TABLE [dbo].[tb_Sys_CargaConfigProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_CargaConfigProducto_tb_Sys_Carga] FOREIGN KEY([di_fk_IdCarga])
REFERENCES [dbo].[tb_Sys_Carga] ([di_idCarga])
GO
ALTER TABLE [dbo].[tb_Sys_CargaConfigProducto] CHECK CONSTRAINT [FK_tb_Sys_CargaConfigProducto_tb_Sys_Carga]
GO
ALTER TABLE [dbo].[tb_Sys_CargaConfigProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_CargaConfigProducto_tb_Sys_ConfigProducto] FOREIGN KEY([di_fk_IdConfProd])
REFERENCES [dbo].[tb_Sys_ConfigProducto] ([di_IdConfProd])
GO
ALTER TABLE [dbo].[tb_Sys_CargaConfigProducto] CHECK CONSTRAINT [FK_tb_Sys_CargaConfigProducto_tb_Sys_ConfigProducto]
GO
ALTER TABLE [dbo].[tb_Sys_Cliente]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Cliente_tb_Sys_Persona] FOREIGN KEY([di_fk_IdPersona], [di_fk_Rut])
REFERENCES [dbo].[tb_Sys_Persona] ([di_IdPersona], [di_Rut])
GO
ALTER TABLE [dbo].[tb_Sys_Cliente] CHECK CONSTRAINT [FK_tb_Sys_Cliente_tb_Sys_Persona]
GO
ALTER TABLE [dbo].[tb_Sys_Comunas]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Comunas] FOREIGN KEY([di_fk_IdProvincia], [di_fk_IdRegion])
REFERENCES [dbo].[tb_Sys_Provincias] ([di_IdProvincia], [di_fk_IdRegion])
GO
ALTER TABLE [dbo].[tb_Sys_Comunas] CHECK CONSTRAINT [FK_tb_Sys_Comunas]
GO
ALTER TABLE [dbo].[tb_Sys_ConfigProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_ConfigProducto_tb_Sys_ConfigCarga] FOREIGN KEY([di_fk_IdConfig])
REFERENCES [dbo].[tb_Sys_ConfigCarga] ([di_IdConfig])
GO
ALTER TABLE [dbo].[tb_Sys_ConfigProducto] CHECK CONSTRAINT [FK_tb_Sys_ConfigProducto_tb_Sys_ConfigCarga]
GO
ALTER TABLE [dbo].[tb_Sys_ConfigProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_ConfigProducto_tb_Sys_Producto] FOREIGN KEY([di_fk_IdProducto])
REFERENCES [dbo].[tb_Sys_Producto] ([di_IdProduc])
GO
ALTER TABLE [dbo].[tb_Sys_ConfigProducto] CHECK CONSTRAINT [FK_tb_Sys_ConfigProducto_tb_Sys_Producto]
GO
ALTER TABLE [dbo].[tb_Sys_ContrAcesso]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_ContrAcesso_tb_Sys_Usuario_di_IdUsuario] FOREIGN KEY([di_fk_IdUsuario])
REFERENCES [dbo].[tb_Sys_Usuario] ([di_IdUsuario])
GO
ALTER TABLE [dbo].[tb_Sys_ContrAcesso] CHECK CONSTRAINT [FK_tb_Sys_ContrAcesso_tb_Sys_Usuario_di_IdUsuario]
GO
ALTER TABLE [dbo].[tb_Sys_ContVistAcces]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_ContVistAcces_tb_Sys_VistaControl _di_IDVistaControl] FOREIGN KEY([di_fk_IdVistaControl])
REFERENCES [dbo].[tb_Sys_VistaControl] ([di_IDVistaControl])
GO
ALTER TABLE [dbo].[tb_Sys_ContVistAcces] CHECK CONSTRAINT [FK_tb_Sys_ContVistAcces_tb_Sys_VistaControl _di_IDVistaControl]
GO
ALTER TABLE [dbo].[tb_Sys_DetHito]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Hito_di_IdHito] FOREIGN KEY([di_IdHito])
REFERENCES [dbo].[tb_Sys_Hito] ([di_IdHito])
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] CHECK CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Hito_di_IdHito]
GO
ALTER TABLE [dbo].[tb_Sys_DetHito]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Operacion_di_IdOperacion] FOREIGN KEY([di_fk_IdOpe])
REFERENCES [dbo].[tb_Sys_Operacion] ([di_IdOperacion])
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] CHECK CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Operacion_di_IdOperacion]
GO
ALTER TABLE [dbo].[tb_Sys_DetHito]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Ubicacion_di_IdUbicacion] FOREIGN KEY([di_fk_IdUbi])
REFERENCES [dbo].[tb_Sys_Ubicacion] ([di_IdUbicacion])
GO
ALTER TABLE [dbo].[tb_Sys_DetHito] CHECK CONSTRAINT [FK_tb_Sys_DetHito_tb_Sys_Ubicacion_di_IdUbicacion]
GO
ALTER TABLE [dbo].[tb_Sys_DetLotDoc]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetLotDoc_tb_Sys_DetLote] FOREIGN KEY([di_fk_IdDetLote])
REFERENCES [dbo].[tb_Sys_DetLote] ([di_IdDetLote])
GO
ALTER TABLE [dbo].[tb_Sys_DetLotDoc] CHECK CONSTRAINT [FK_tb_Sys_DetLotDoc_tb_Sys_DetLote]
GO
ALTER TABLE [dbo].[tb_Sys_DetLotDoc]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetLotDoc_tb_Sys_Documento] FOREIGN KEY([di_fk_IdDcto])
REFERENCES [dbo].[tb_Sys_Documento] ([di_IdDcto])
GO
ALTER TABLE [dbo].[tb_Sys_DetLotDoc] CHECK CONSTRAINT [FK_tb_Sys_DetLotDoc_tb_Sys_Documento]
GO
ALTER TABLE [dbo].[tb_Sys_DetLote]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetLote_tb_Sys_DetSolicitud_di_IdDetSol] FOREIGN KEY([di_fk_IdDetSol])
REFERENCES [dbo].[tb_Sys_DetSolicitud] ([di_IdDetSol])
GO
ALTER TABLE [dbo].[tb_Sys_DetLote] CHECK CONSTRAINT [FK_tb_Sys_DetLote_tb_Sys_DetSolicitud_di_IdDetSol]
GO
ALTER TABLE [dbo].[tb_Sys_DetLote]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetLote_tb_Sys_Estado_di_IdEstado] FOREIGN KEY([di_fk_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_DetLote] CHECK CONSTRAINT [FK_tb_Sys_DetLote_tb_Sys_Estado_di_IdEstado]
GO
ALTER TABLE [dbo].[tb_Sys_DetTipoDcto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DetTipoDcto_tb_Sys_TipoDcto] FOREIGN KEY([di_fk_IdTipDcto])
REFERENCES [dbo].[tb_Sys_TipoDcto] ([di_IdTipDcto])
GO
ALTER TABLE [dbo].[tb_Sys_DetTipoDcto] CHECK CONSTRAINT [FK_tb_Sys_DetTipoDcto_tb_Sys_TipoDcto]
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DireccionesPer_tb_Sys_Persona] FOREIGN KEY([di_fk_IdPersona], [di_fk_Rut])
REFERENCES [dbo].[tb_Sys_Persona] ([di_IdPersona], [di_Rut])
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer] CHECK CONSTRAINT [FK_tb_Sys_DireccionesPer_tb_Sys_Persona]
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DireccionesPer_tb_Sys_TelefonosPer_di_IdFono] FOREIGN KEY([di_fk_IdFono])
REFERENCES [dbo].[tb_Sys_TelefonosPer] ([di_IdFono])
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer] CHECK CONSTRAINT [FK_tb_Sys_DireccionesPer_tb_Sys_TelefonosPer_di_IdFono]
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_DireccionesPer2] FOREIGN KEY([di_fk_IdComuna], [di_fk_IdProvincia], [di_fk_IdRegion])
REFERENCES [dbo].[tb_Sys_Comunas] ([di_IdComunas], [di_fk_IdProvincia], [di_fk_IdRegion])
GO
ALTER TABLE [dbo].[tb_Sys_DireccionesPer] CHECK CONSTRAINT [FK_tb_Sys_DireccionesPer2]
GO
ALTER TABLE [dbo].[tb_Sys_Documento]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Estado_di_IdEstado] FOREIGN KEY([di_fk_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_Documento] CHECK CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Estado_di_IdEstado]
GO
ALTER TABLE [dbo].[tb_Sys_Documento]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Operacion_di_IdOperacion] FOREIGN KEY([di_fk_IdOper])
REFERENCES [dbo].[tb_Sys_Operacion] ([di_IdOperacion])
GO
ALTER TABLE [dbo].[tb_Sys_Documento] CHECK CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Operacion_di_IdOperacion]
GO
ALTER TABLE [dbo].[tb_Sys_Documento]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Ubicacion_di_IdUbicacion] FOREIGN KEY([di_fk_IdUbi])
REFERENCES [dbo].[tb_Sys_Ubicacion] ([di_IdUbicacion])
GO
ALTER TABLE [dbo].[tb_Sys_Documento] CHECK CONSTRAINT [FK_tb_Sys_Documento_tb_Sys_Ubicacion_di_IdUbicacion]
GO
ALTER TABLE [dbo].[tb_Sys_EstInterfazPJ]  WITH CHECK ADD  CONSTRAINT [FK_EstInterPJ_Estado] FOREIGN KEY([INT_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_EstInterfazPJ] CHECK CONSTRAINT [FK_EstInterPJ_Estado]
GO
ALTER TABLE [dbo].[tb_Sys_EstInterfazPJ]  WITH CHECK ADD  CONSTRAINT [FK_EstInterPJ_InterPJ_Enc] FOREIGN KEY([INT_IdestInterEnc])
REFERENCES [dbo].[tb_Sys_InterfazPJ_Enc] ([INT_IdInterfaz_Enc])
GO
ALTER TABLE [dbo].[tb_Sys_EstInterfazPJ] CHECK CONSTRAINT [FK_EstInterPJ_InterPJ_Enc]
GO
ALTER TABLE [dbo].[tb_Sys_EstudioJuridico]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_EstudioJuridico_tb_Sys_Zona_di_IdZona] FOREIGN KEY([di_fk_IdZona])
REFERENCES [dbo].[tb_Sys_Zona] ([di_IdZona])
GO
ALTER TABLE [dbo].[tb_Sys_EstudioJuridico] CHECK CONSTRAINT [FK_tb_Sys_EstudioJuridico_tb_Sys_Zona_di_IdZona]
GO
ALTER TABLE [dbo].[tb_Sys_Hito]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Hito_tb_Sys_Estado_di_IdEstado] FOREIGN KEY([di_fk_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_Hito] CHECK CONSTRAINT [FK_tb_Sys_Hito_tb_Sys_Estado_di_IdEstado]
GO
ALTER TABLE [dbo].[tb_Sys_Hito]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Hito_tb_Sys_TipoHito] FOREIGN KEY([di_fk_IdTipoHito])
REFERENCES [dbo].[tb_Sys_TipoHito] ([di_idTipoHito])
GO
ALTER TABLE [dbo].[tb_Sys_Hito] CHECK CONSTRAINT [FK_tb_Sys_Hito_tb_Sys_TipoHito]
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_CargaConfigProducto] FOREIGN KEY([di_fk_di_IdCargaConfigProd])
REFERENCES [dbo].[tb_Sys_CargaConfigProducto] ([di_IdCargaConfigProd])
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto] CHECK CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_CargaConfigProducto]
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_Hito] FOREIGN KEY([di_fk_IdHito])
REFERENCES [dbo].[tb_Sys_Hito] ([di_IdHito])
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto] CHECK CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_Hito]
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_Producto] FOREIGN KEY([di_fk_IdProducto])
REFERENCES [dbo].[tb_Sys_Producto] ([di_IdProduc])
GO
ALTER TABLE [dbo].[tb_Sys_HitoProducto] CHECK CONSTRAINT [FK_tb_Sys_HitoProducto_tb_Sys_Producto]
GO
ALTER TABLE [dbo].[tb_Sys_InterfazPJ_Det]  WITH CHECK ADD  CONSTRAINT [FK_InterPJ_Det_TipoMoneda] FOREIGN KEY([INT_IdTipoMoneda])
REFERENCES [dbo].[tb_Sys_TipoMoneda] ([INT_IdTipoMoneda])
GO
ALTER TABLE [dbo].[tb_Sys_InterfazPJ_Det] CHECK CONSTRAINT [FK_InterPJ_Det_TipoMoneda]
GO
ALTER TABLE [dbo].[tb_Sys_InterfazPJ_Det]  WITH CHECK ADD  CONSTRAINT [FKInterPJ_Det_InterPJ_Enc] FOREIGN KEY([INT_IdInterfaz_Enc])
REFERENCES [dbo].[tb_Sys_InterfazPJ_Enc] ([INT_IdInterfaz_Enc])
GO
ALTER TABLE [dbo].[tb_Sys_InterfazPJ_Det] CHECK CONSTRAINT [FKInterPJ_Det_InterPJ_Enc]
GO
ALTER TABLE [dbo].[tb_Sys_Lote]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Lote] FOREIGN KEY([di_IdUbi_Salida])
REFERENCES [dbo].[tb_Sys_Ubicacion] ([di_IdUbicacion])
GO
ALTER TABLE [dbo].[tb_Sys_Lote] CHECK CONSTRAINT [FK_tb_Sys_Lote]
GO
ALTER TABLE [dbo].[tb_Sys_Lote]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Lote_tb_Sys_Estado_di_IdEstado] FOREIGN KEY([di_fk_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_Lote] CHECK CONSTRAINT [FK_tb_Sys_Lote_tb_Sys_Estado_di_IdEstado]
GO
ALTER TABLE [dbo].[tb_Sys_Modulo_AccesoTipoReparo]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Modulo_AccesoTipoReparo_tb_Sys_Modulo] FOREIGN KEY([di_fk_IdModulo])
REFERENCES [dbo].[tb_Sys_Modulo] ([di_IdModulo])
GO
ALTER TABLE [dbo].[tb_Sys_Modulo_AccesoTipoReparo] CHECK CONSTRAINT [FK_tb_Sys_Modulo_AccesoTipoReparo_tb_Sys_Modulo]
GO
ALTER TABLE [dbo].[tb_Sys_MotivoReparoLote]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_MotivoReparoLote_tb_Sys_Lote] FOREIGN KEY([di_fk_IdLote], [dv_fk_codLote])
REFERENCES [dbo].[tb_Sys_Lote] ([di_IdLote], [dv_CodLote])
GO
ALTER TABLE [dbo].[tb_Sys_MotivoReparoLote] CHECK CONSTRAINT [FK_tb_Sys_MotivoReparoLote_tb_Sys_Lote]
GO
ALTER TABLE [dbo].[tb_Sys_MotRepDetSol]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_MotRepDetSol_tb_Sys_DetSolicitud] FOREIGN KEY([di_fk_IdDetSol])
REFERENCES [dbo].[tb_Sys_DetSolicitud] ([di_IdDetSol])
GO
ALTER TABLE [dbo].[tb_Sys_MotRepDetSol] CHECK CONSTRAINT [FK_tb_Sys_MotRepDetSol_tb_Sys_DetSolicitud]
GO
ALTER TABLE [dbo].[tb_Sys_Oficina]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_oficina_tb_Sys_Cedentes] FOREIGN KEY([di_Idcedente])
REFERENCES [dbo].[tb_Sys_Cedentes] ([di_IdCedente])
GO
ALTER TABLE [dbo].[tb_Sys_Oficina] CHECK CONSTRAINT [FK_tb_Sys_oficina_tb_Sys_Cedentes]
GO
ALTER TABLE [dbo].[tb_Sys_Oficina]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_oficina_tb_Sys_TipRegion] FOREIGN KEY([dv_fk_IdTipReg])
REFERENCES [dbo].[tb_Sys_TipRegion] ([di_IdTipReg])
GO
ALTER TABLE [dbo].[tb_Sys_Oficina] CHECK CONSTRAINT [FK_tb_Sys_oficina_tb_Sys_TipRegion]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Banca] FOREIGN KEY([dv_fk_IdBanca])
REFERENCES [dbo].[tb_Sys_Banca] ([dv_idBanca])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Banca]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Cliente_di_IdCliente] FOREIGN KEY([di_fk_IdCliente])
REFERENCES [dbo].[tb_Sys_Cliente] ([di_IdCliente])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Cliente_di_IdCliente]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Estado_di_IdEstado] FOREIGN KEY([di_IdEstado])
REFERENCES [dbo].[tb_Sys_Estado] ([di_IdEstado])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Estado_di_IdEstado]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_oficina] FOREIGN KEY([di_fk_IdCedente], [di_fk_IdOficina])
REFERENCES [dbo].[tb_Sys_Oficina] ([di_Idcedente], [di_IdOficina])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_oficina]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Producto_di_IdProduc] FOREIGN KEY([di_fk_IdProduc])
REFERENCES [dbo].[tb_Sys_Producto] ([di_IdProduc])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_Producto_di_IdProduc]
GO
ALTER TABLE [dbo].[tb_Sys_Operacion]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_TipRegion] FOREIGN KEY([dv_fk_TipRegion])
REFERENCES [dbo].[tb_Sys_TipRegion] ([di_IdTipReg])
GO
ALTER TABLE [dbo].[tb_Sys_Operacion] CHECK CONSTRAINT [FK_tb_Sys_Operacion_tb_Sys_TipRegion]
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_OperacionesInhibidas_tb_Sys_Operacion] FOREIGN KEY([di_fk_idOpe])
REFERENCES [dbo].[tb_Sys_Operacion] ([di_IdOperacion])
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas] CHECK CONSTRAINT [FK_tb_Sys_OperacionesInhibidas_tb_Sys_Operacion]
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_OperacionesInhibidas_tb_Sys_TipInhibicion] FOREIGN KEY([di_fk_idTipInhibicion])
REFERENCES [dbo].[tb_Sys_TipInhibicion] ([di_idTipInhibicion])
GO
ALTER TABLE [dbo].[tb_Sys_OperacionesInhibidas] CHECK CONSTRAINT [FK_tb_Sys_OperacionesInhibidas_tb_Sys_TipInhibicion]
GO
ALTER TABLE [dbo].[tb_Sys_PerAcces]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_PerAcces] FOREIGN KEY([di_fk_IdPerfil])
REFERENCES [dbo].[tb_Sys_Perfiles] ([di_IdPerfil])
GO
ALTER TABLE [dbo].[tb_Sys_PerAcces] CHECK CONSTRAINT [FK_tb_Sys_PerAcces]
GO
ALTER TABLE [dbo].[tb_Sys_PerAcces]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_PerAcces2] FOREIGN KEY([di_IdContVistAcces])
REFERENCES [dbo].[tb_Sys_ContVistAcces] ([di_IdContVistAcces])
GO
ALTER TABLE [dbo].[tb_Sys_PerAcces] CHECK CONSTRAINT [FK_tb_Sys_PerAcces2]
GO
ALTER TABLE [dbo].[tb_Sys_Provincias]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Provincias_tb_Sys_Regiones_di_IdRegion] FOREIGN KEY([di_fk_IdRegion])
REFERENCES [dbo].[tb_Sys_Regiones] ([di_IdRegion])
GO
ALTER TABLE [dbo].[tb_Sys_Provincias] CHECK CONSTRAINT [FK_tb_Sys_Provincias_tb_Sys_Regiones_di_IdRegion]
GO
ALTER TABLE [dbo].[tb_Sys_TelefonosPer]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_TelefonosPer_tb_Sys_Persona] FOREIGN KEY([di_fk_IdPersona], [di_fk_Rut])
REFERENCES [dbo].[tb_Sys_Persona] ([di_IdPersona], [di_Rut])
GO
ALTER TABLE [dbo].[tb_Sys_TelefonosPer] CHECK CONSTRAINT [FK_tb_Sys_TelefonosPer_tb_Sys_Persona]
GO
ALTER TABLE [dbo].[tb_Sys_TOper_DetTDcto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_TOper_DetTDcto_tb_Sys_DetTipoDcto] FOREIGN KEY([di_fk_IdDetTipDcto])
REFERENCES [dbo].[tb_Sys_DetTipoDcto] ([di_IdDetTipDcto])
GO
ALTER TABLE [dbo].[tb_Sys_TOper_DetTDcto] CHECK CONSTRAINT [FK_tb_Sys_TOper_DetTDcto_tb_Sys_DetTipoDcto]
GO
ALTER TABLE [dbo].[tb_Sys_TOper_DetTDcto]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_TOper_DetTDcto_tb_Sys_TipoOperacion] FOREIGN KEY([dv_fk_IdTipOper])
REFERENCES [dbo].[tb_Sys_TipoOperacion] ([dv_IdTipOpe])
GO
ALTER TABLE [dbo].[tb_Sys_TOper_DetTDcto] CHECK CONSTRAINT [FK_tb_Sys_TOper_DetTDcto_tb_Sys_TipoOperacion]
GO
ALTER TABLE [dbo].[tb_sys_ubicacion_custodia]  WITH CHECK ADD  CONSTRAINT [FK_tb_sys_ubicacion_custodia_tb_sys_custodia] FOREIGN KEY([fk_id_custodia])
REFERENCES [dbo].[tb_sys_custodia] ([id])
GO
ALTER TABLE [dbo].[tb_sys_ubicacion_custodia] CHECK CONSTRAINT [FK_tb_sys_ubicacion_custodia_tb_sys_custodia]
GO
ALTER TABLE [dbo].[tb_sys_ubicacion_custodia]  WITH CHECK ADD  CONSTRAINT [FK_tb_sys_ubicacion_custodia_tb_Sys_Ubicacion] FOREIGN KEY([fk_id_custodia])
REFERENCES [dbo].[tb_Sys_Ubicacion] ([di_IdUbicacion])
GO
ALTER TABLE [dbo].[tb_sys_ubicacion_custodia] CHECK CONSTRAINT [FK_tb_sys_ubicacion_custodia_tb_Sys_Ubicacion]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Cargo_di_IdCargo] FOREIGN KEY([di_fk_IdCargo])
REFERENCES [dbo].[tb_Sys_Cargo] ([di_IdCargo])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Cargo_di_IdCargo]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Cenco_di_IdCenco] FOREIGN KEY([di_fk_IdCenco])
REFERENCES [dbo].[tb_Sys_Cenco] ([di_IdCenco])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Cenco_di_IdCenco]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Dependencia_di_IdDependencia] FOREIGN KEY([di_fk_IdDependencia])
REFERENCES [dbo].[tb_Sys_Dependencia] ([di_IdDependencia])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Dependencia_di_IdDependencia]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Empresa_di_IdEmpresa] FOREIGN KEY([di_fk_IdEmpresa])
REFERENCES [dbo].[tb_Sys_Empresa] ([di_IdEmpresa])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Empresa_di_IdEmpresa]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Fondo_di_IdFondo] FOREIGN KEY([di_fk_IdFondo])
REFERENCES [dbo].[tb_Sys_Fondo] ([di_IdFondo])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Fondo_di_IdFondo]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Gerencia_di_IdGerencia] FOREIGN KEY([di_fk_IdGerencia])
REFERENCES [dbo].[tb_Sys_Gerencia] ([di_IdGerencia])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Gerencia_di_IdGerencia]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Persona] FOREIGN KEY([di_fk_IdPersona], [di_fk_Rut])
REFERENCES [dbo].[tb_Sys_Persona] ([di_IdPersona], [di_Rut])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Persona]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Posicion_di_IdPosicion] FOREIGN KEY([di_fk_IdPosicion])
REFERENCES [dbo].[tb_Sys_Posicion] ([di_IdPosicion])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Posicion_di_IdPosicion]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Usuario_di_IdUsuario] FOREIGN KEY([di_IdUsuarioJefe])
REFERENCES [dbo].[tb_Sys_Usuario] ([di_IdUsuario])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario_tb_Sys_Usuario_di_IdUsuario]
GO
ALTER TABLE [dbo].[tb_Sys_Usuario]  WITH CHECK ADD  CONSTRAINT [FK_tb_Sys_Usuario2] FOREIGN KEY([di_fk_IdPerAcces])
REFERENCES [dbo].[tb_Sys_PerAcces] ([di_IdPerAcces])
GO
ALTER TABLE [dbo].[tb_Sys_Usuario] CHECK CONSTRAINT [FK_tb_Sys_Usuario2]
GO
ALTER TABLE [dbo].[tbl_webnza_Regionales]  WITH CHECK ADD  CONSTRAINT [FK_tbl_webnza_Regionales_tbl_webnza_Cedentes_fld_IdCedente] FOREIGN KEY([fld_fk_IdCedente])
REFERENCES [dbo].[tbl_webnza_Cedentes] ([fld_IdCedente])
GO
ALTER TABLE [dbo].[tbl_webnza_Regionales] CHECK CONSTRAINT [FK_tbl_webnza_Regionales_tbl_webnza_Cedentes_fld_IdCedente]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes]  WITH CHECK ADD  CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_Informes_fld_IdInforme] FOREIGN KEY([fld_fk_IdInforme])
REFERENCES [dbo].[tbl_webnza_Informes] ([fld_IdInforme])
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] CHECK CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_Informes_fld_IdInforme]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes]  WITH CHECK ADD  CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_ReginalesInformes_fld_IdNomRegInforme] FOREIGN KEY([fld_fk_IdNomRegInforme])
REFERENCES [dbo].[tbl_webnza_NombreRegionalesInf] ([fld_IdNomRegInforme])
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] CHECK CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_ReginalesInformes_fld_IdNomRegInforme]
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes]  WITH CHECK ADD  CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_Regionales_fld_IdRegional] FOREIGN KEY([fld_fk_IdRegional])
REFERENCES [dbo].[tbl_webnza_Regionales] ([fld_IdRegional])
GO
ALTER TABLE [dbo].[tbl_webnza_RegionalesInformes] CHECK CONSTRAINT [FK_tbl_webnza_Reg_equals_Informe_tbl_webnza_Regionales_fld_IdRegional]
GO
